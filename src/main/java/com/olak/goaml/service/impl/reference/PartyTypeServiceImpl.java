package com.olak.goaml.service.impl.reference;
import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

import com.olak.goaml.dao.reference.RPartyTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RPartyTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RPartyTypeMapper;
import com.olak.goaml.models.reference.RPartyType;
import com.olak.goaml.service.reference.PartyTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PartyTypeServiceImpl implements PartyTypeService {

    private final RStatusDao rStatusDao;
    private final RPartyTypeDao rPartyTypeDao;
    private final RPartyTypeMapper rPartyTypeMapper;
    private final SearchNFilter searchNFilter;

    public PartyTypeServiceImpl(RStatusDao rStatusDao, RPartyTypeDao rPartyTypeDao, RPartyTypeMapper rPartyTypeMapper, SearchNFilter searchNFilter) {
        this.rStatusDao = rStatusDao;
        this.rPartyTypeDao = rPartyTypeDao;
        this.rPartyTypeMapper = rPartyTypeMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RPartyTypeDto>> getAllPartyTypes() {
        log.info("Inside PartyTypeServiceImpl: getAllPartyTypes method");
        List<RPartyType> resultList = rPartyTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rPartyTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RPartyTypeDto>>> getPartyTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside PartyTypeServiceImpl: getPartyTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RPartyType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RPartyTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rPartyTypeMapper.listToDto((List<RPartyType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RPartyTypeDto> addPartyType(RPartyTypeDto partyTypeDto) {
        log.info("Inside PartyTypeServiceImpl: addPartyType method");

        try {
            if (partyTypeDto.getPartyTypeCode() == null || partyTypeDto.getPartyTypeCode().isEmpty()) {
                throw new BadRequestAlertException("PartyType code is required.", ENTITY_NAME, "error");
            }

            Optional<RPartyType> existingEntity = rPartyTypeDao.findByPartyTypeCodeAndIsActive(partyTypeDto.getPartyTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("PartyType already exists with code: " + partyTypeDto.getPartyTypeCode(), ENTITY_NAME, "idexists");
            }

            if (partyTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(partyTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "idinvalid");
            }

            partyTypeDto.setIsActive(true);
            partyTypeDto.setStatusId(1);

            RPartyType entity = rPartyTypeMapper.toEntity(partyTypeDto);
            entity.setRStatus(rStatusDao.findById(partyTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "idinvalid")));
            entity = rPartyTypeDao.save(entity);

            return new ResponseEntity<>(rPartyTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding PartyType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RPartyTypeDto> getPartyTypeById(Integer id) {
        log.info("Inside PartyTypeServiceImpl: getPartyTypeById method");

        try {
            Optional<RPartyType> entity = rPartyTypeDao.findByPartyTypeIdAndIsActive(id, true);
            RPartyTypeDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("PartyType not found by ID: " + id, ENTITY_NAME, "idNotFound");
            } else {
                responseDto = rPartyTypeMapper.toDto(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting PartyType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RPartyTypeDto> updatePartyTypeById(Integer id, RPartyTypeDto partyTypeDto) {
        log.info("Inside PartyTypeServiceImpl: updatePartyTypeById method");

        Optional<RPartyType> existingEntity = rPartyTypeDao.findById(id);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("Invalid party type ID", ENTITY_NAME, "idInvalid");
        }

        if (!id.equals(partyTypeDto.getPartyTypeId())) {
            throw new BadRequestAlertException("PartyType ID mismatch.", ENTITY_NAME, "error");
        }

        if (partyTypeDto.getPartyTypeCode() == null || partyTypeDto.getPartyTypeCode().isEmpty()) {
            throw new BadRequestAlertException("PartyType Code is required.", ENTITY_NAME, "error");
        }

        if (rPartyTypeDao.existsByPartyTypeCodeAndIsActiveAndPartyTypeIdNot(partyTypeDto.getPartyTypeCode(), true, id)) {
            throw new BadRequestAlertException("PartyType Code is duplicate with another record.", ENTITY_NAME, "partyTypeCodeExists");
        }

        if (partyTypeDto.getStatusId() == null) {
            throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
        }

        if (!rStatusDao.existsById(partyTypeDto.getStatusId())) {
            throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
        }

        RPartyType entity = existingEntity.get();
        entity.setPartyTypeCode(partyTypeDto.getPartyTypeCode());
        entity.setDescription(partyTypeDto.getDescription());
        entity.setIsActive(partyTypeDto.getIsActive());
        entity.setRStatus(rStatusDao.findById(partyTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID.", ENTITY_NAME, "error")));

        try {
            entity = rPartyTypeDao.save(entity);
            return new ResponseEntity<>(rPartyTypeMapper.toDto(entity), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while updating PartyType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RPartyTypeDto> deletePartyTypeById(Integer id) {
        log.info("Inside PartyTypeServiceImpl: deletePartyTypeById method");

        try {
            Optional<RPartyType> resultOp = rPartyTypeDao.findByPartyTypeIdAndIsActive(id, true);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Party type not found by ID: " + id, ENTITY_NAME, "idNotFound");
            } else {
                RPartyType existingEntity = resultOp.get();

                existingEntity.setIsActive(false);
                RPartyTypeDto responseDto = rPartyTypeMapper.toDto(rPartyTypeDao.save(existingEntity));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting Party type: {}", e.getMessage());
            throw new BadRequestAlertException("Party type could not be deleted by ID: " + id, ENTITY_NAME, "error");
        }
    }
}
