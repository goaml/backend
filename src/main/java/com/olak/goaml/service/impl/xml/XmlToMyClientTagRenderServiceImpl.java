package com.olak.goaml.service.impl.xml;

import com.olak.goaml.dto.xml.transaction.TToMyClientDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.xml.transaction.TToMyClient;
import com.olak.goaml.service.xml.*;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.xmlDto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlToMyClientTagRenderServiceImpl implements XmlToMyClientTagRenderService {

    private final XmlFCurrencyTagRenderService xmlFCurrencyTagRenderService;
    private final XmlPersonMyClientTagRenderService xmlPersonMyClientTagRenderService;
    private final XmlAccountMyClientTagRenderService xmlAccountMyClientTagRenderService;
    private final XmlEntityMyClientTagRenderService xmlEntityMyClientTagRenderService;

    public XmlToMyClientTagRenderServiceImpl(XmlFCurrencyTagRenderService xmlFCurrencyTagRenderService, XmlPersonMyClientTagRenderService xmlPersonMyClientTagRenderService, XmlAccountMyClientTagRenderService xmlAccountMyClientTagRenderService, XmlEntityMyClientTagRenderService xmlEntityMyClientTagRenderService) {
        this.xmlFCurrencyTagRenderService = xmlFCurrencyTagRenderService;
        this.xmlPersonMyClientTagRenderService = xmlPersonMyClientTagRenderService;
        this.xmlAccountMyClientTagRenderService = xmlAccountMyClientTagRenderService;
        this.xmlEntityMyClientTagRenderService = xmlEntityMyClientTagRenderService;
    }

    @Override
    public xmlTToMyClientDto getRenderedTToMyClient(List<TToMyClient> toMyClientList, String scenarioName) {

        try{
            xmlTToMyClientDto xmlToMyClientDto = new xmlTToMyClientDto();

            if (toMyClientList != null && !toMyClientList.isEmpty()) {
                for (TToMyClient toMyClientDto : toMyClientList) {
//                TToMyClientDto toMyClientDto = new TToMyClientDto();

                    xmlToMyClientDto.setToFundsCode(toMyClientDto.getRFundsType().getFundTypeCode());
                    xmlToMyClientDto.setToFundsComment(toMyClientDto.getToFundsComment());

                    if (EnumTagVerifier.isTagValid("T_TO_MY_CLIENT_TO_FOREIGN_CURRENCY", scenarioName)) {
                        if (toMyClientDto.getToForeignCurrencyId() != null)
                            xmlToMyClientDto.setToForeignCurrency(xmlFCurrencyTagRenderService.getRenderedFCurrency(toMyClientDto.getToForeignCurrencyId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_TO_MY_CLIENT_TO_ACCOUNT", scenarioName)) {
                        if (toMyClientDto.getToAccountMyClientId() != null)
                            xmlToMyClientDto.setToAccount(xmlAccountMyClientTagRenderService.getRenderedAccount(toMyClientDto.getToAccountMyClientId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_TO_MY_CLIENT_TO_PERSON", scenarioName)) {
                        if (toMyClientDto.getToPersonMyClientId() != null)
                            xmlToMyClientDto.setToPerson(xmlPersonMyClientTagRenderService.getRenderedPerson(toMyClientDto.getToPersonMyClientId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_TO_MY_CLIENT_TO_ENTITY", scenarioName)) {
                        if (toMyClientDto.getToEntityMyClientId() != null)
                            xmlToMyClientDto.setToEntity(xmlEntityMyClientTagRenderService.getRenderedEntity(toMyClientDto.getToEntityMyClientId(), scenarioName));
                    }

                    xmlToMyClientDto.setToCountry(toMyClientDto.getRCountryCodes().getCountryCodeCode());


                }
            }

            return xmlToMyClientDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedTToMyClient: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
