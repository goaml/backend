package com.olak.goaml.service.impl;

import com.olak.goaml.dao.master.RptCodeDao;
import com.olak.goaml.dao.transaction.DailyTranDetailTableTempDao;
import com.olak.goaml.dao.xml.transaction.TTransactionDao;
import com.olak.goaml.dto.miscellaneous.TranRequestDto;
import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TTransactionMapper;
import com.olak.goaml.models.master.RptCode;
import com.olak.goaml.models.transaction.DailyTranDetailTableTemp;
import com.olak.goaml.service.SaveDailyTranToTableService;
import com.olak.goaml.userMGT.dto.other.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@Slf4j
public class SaveDailyTranToTableServiceImpl implements SaveDailyTranToTableService {

    private final DailyTranDetailTableTempDao dailyTranDetailTableTempDao;
    private final TTransactionDao tTransactionDao;
    private final TTransactionMapper tTransactionMapper;
    private final RptCodeDao rptCodeDao;

    public SaveDailyTranToTableServiceImpl(DailyTranDetailTableTempDao dailyTranDetailTableTempDao, TTransactionDao tTransactionDao, TTransactionMapper tTransactionMapper, RptCodeDao rptCodeDao) {
        this.dailyTranDetailTableTempDao = dailyTranDetailTableTempDao;
        this.tTransactionDao = tTransactionDao;
        this.tTransactionMapper = tTransactionMapper;
        this.rptCodeDao = rptCodeDao;
    }

//    @Override
//    public ResponseDto saveDailyTranToTable(TranRequestDto tranRequestDto) {
//        int processedCount = 0;
//
//        try (Stream<DailyTranDetailTableTemp> stream = dailyTranDetailTableTempDao.findAllByTranDateBetween(tranRequestDto.getStartDate(), tranRequestDto.getEndDate())) {
//            // delete all data
//            tTransactionDao.deleteAll();
//            log.info("Existing data are cleared");
//            List<RptCode> rptCodeList = rptCodeDao.findAll();
//            processedCount = (int) stream.peek(record -> {
//                try {
//                    TTransactionDto tTransactionDto = new TTransactionDto();
////                System.out.println(record.getTranAmt());
//                    tTransactionDto.setAmountLocal(record.getTranAmt());
//                    tTransactionDto.setDatePosting(record.getPstdDate());
//                    tTransactionDto.setDateTransaction(record.getTranDate());
//                    tTransactionDto.setInternalRefNumber(record.getRefNum());
//                    tTransactionDto.setIsActive(true);
//                    tTransactionDto.setLateDeposit(false);
//                    tTransactionDto.setTransactionNumber(record.getTranId());
//                    tTransactionDto.setTransmodeComment(record.getPartTranType());
//                    tTransactionDto.setValueDate(record.getValueDate());
//                    tTransactionDto.setStatusId(1);
//                    tTransactionDto.setReportId(1);
//                    tTransactionDto.setConductionTypeId(2);
//                    tTransactionDto.setRptCode(record.getRptCode());
//                    tTransactionDto.setTransactionDescription(record.getTranParticular());
//                    tTransactionDto.setTransactionLocation(record.getBrCode() != null ? record.getBrCode() : "N/A");
//                    if (record.getRptCode() != null) {
//                        tTransactionDto.setTrxNo(rptCodeList.stream().filter(rptCode -> rptCode.getRptCodeName().equals(record.getRptCode())).findFirst().get().getGoAmlTrxCodes().getTransactionCodeName());
//                    } else {
//                        tTransactionDto.setTrxNo("N/A");
//                    }
//                    tTransactionDao.save(tTransactionMapper.toEntity(tTransactionDto));
//                    log.info("tran saved: {}", tTransactionDto.getTransactionNumber());
//                } catch (Exception e) {
//                    // Handle the specific exception here
//                    System.err.println("Error processing record: " + record + " - " + e.getMessage());
//                    throw new BadRequestAlertException(e.getMessage(), "Error", "Error");
//                }
//            }).count();
//            return new ResponseDto("success", "count: "+processedCount, "Success");
//        } catch (Exception e) {
//            System.err.println("Error processing records: " + e.getMessage());
//            throw new BadRequestAlertException(e.getMessage(), "Error", "Error");
//        }
//    }

//    @Transactional
//    @Override
//    public ResponseDto saveDailyTranToTable(TranRequestDto tranRequestDto) {
//        log.info("Request to save daily transaction to table");
//        try {
//            // list all data given date range
//            Stream<DailyTranDetailTableTemp> dailyTranDetailTableTemps;
//            if (tranRequestDto.getRtpCode().isEmpty()) {
//                dailyTranDetailTableTemps = dailyTranDetailTableTempDao.findAllByTranDateBetween(tranRequestDto.getStartDate(), tranRequestDto.getEndDate());
//            } else {
//                dailyTranDetailTableTemps = dailyTranDetailTableTempDao.findAllByTranDateBetweenAndRptCodeIn(tranRequestDto.getStartDate(), tranRequestDto.getEndDate(), tranRequestDto.getRtpCode());
//            }
//
//            // delete all data
//            tTransactionDao.deleteAll();
//            log.info("Existing data are cleared");
//
//            List<RptCode> rptCodeList = rptCodeDao.findAll();
//
//            // save to transaction table
//            dailyTranDetailTableTemps.forEach(dailyTranDetail -> {
//                TTransactionDto tTransactionDto = new TTransactionDto();
//                tTransactionDto.setAmountLocal(dailyTranDetail.getTranAmt());
//                tTransactionDto.setDatePosting(dailyTranDetail.getPstdDate());
//                tTransactionDto.setDateTransaction(dailyTranDetail.getTranDate());
//                tTransactionDto.setInternalRefNumber(dailyTranDetail.getRefNum());
//                tTransactionDto.setIsActive(true);
//                tTransactionDto.setLateDeposit(false);
//                tTransactionDto.setTransactionNumber(dailyTranDetail.getTranId());
//                tTransactionDto.setTransmodeComment(dailyTranDetail.getPartTranType());
//                tTransactionDto.setValueDate(dailyTranDetail.getValueDate());
//                tTransactionDto.setStatusId(1);
//                tTransactionDto.setReportId(1);
//                tTransactionDto.setConductionTypeId(2);
//                tTransactionDto.setRptCode(dailyTranDetail.getRptCode());
//                tTransactionDto.setTransactionDescription(dailyTranDetail.getTranParticular());
//                tTransactionDto.setTransactionLocation(dailyTranDetail.getBrCode() != null ? dailyTranDetail.getBrCode() : "N/A");
//                if (dailyTranDetail.getRptCode() != null) {
//                    tTransactionDto.setTrxNo(rptCodeList.stream().filter(rptCode -> rptCode.getRptCodeName().equals(dailyTranDetail.getRptCode())).findFirst().get().getGoAmlTrxCodes().getTransactionCodeName());
//                } else {
//                    tTransactionDto.setTrxNo("N/A");
//                }
//                tTransactionDao.save(tTransactionMapper.toEntity(tTransactionDto));
//                log.info("tran saved: {}", tTransactionDto.getTransactionNumber());
//            });
//
//            return new ResponseDto("success", "count: "+0, "Success");
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.error("Error: {}", e.getMessage());
//            throw new BadRequestAlertException(e.getMessage(), "Error", "Error");
//        }
//    }

//    @Transactional
//    @Override
//    public ResponseDto saveDailyTranToTable(TranRequestDto tranRequestDto) {
//        log.info("Request to save daily transaction to table");
//        int processedCount = 0;
//
//        try (Stream<DailyTranDetailTableTemp> dailyTranDetailTableTemps = getDailyTranDetailStream(tranRequestDto)) {
//            // delete all data
//            tTransactionDao.deleteAll();
//            log.info("Existing data are cleared");
//
//            List<RptCode> rptCodeList = rptCodeDao.findAll();
//
//            // save to transaction table
//            processedCount = (int) dailyTranDetailTableTemps.peek(dailyTranDetail -> {
//                TTransactionDto tTransactionDto = createTTransactionDto(dailyTranDetail, rptCodeList);
//                tTransactionDao.save(tTransactionMapper.toEntity(tTransactionDto));
//                log.info("tran saved: {}", tTransactionDto.getTransactionNumber());
//            }).count();
//
//            return new ResponseDto("success", "count: " + processedCount, "Success");
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.error("Error: {}", e.getMessage());
//            throw new BadRequestAlertException(e.getMessage(), "Error", "Error");
//        }
//    }
//
//    private Stream<DailyTranDetailTableTemp> getDailyTranDetailStream(TranRequestDto tranRequestDto) {
//        if (tranRequestDto.getRtpCode().isEmpty()) {
//            return dailyTranDetailTableTempDao.findAllByTranDateBetween(tranRequestDto.getStartDate(), tranRequestDto.getEndDate());
//        } else {
//            return dailyTranDetailTableTempDao.findAllByTranDateBetweenAndRptCodeIn(tranRequestDto.getStartDate(), tranRequestDto.getEndDate(), tranRequestDto.getRtpCode());
//        }
//    }
//
//    private TTransactionDto createTTransactionDto(DailyTranDetailTableTemp dailyTranDetail, List<RptCode> rptCodeList) {
//        TTransactionDto tTransactionDto = new TTransactionDto();
//        tTransactionDto.setAmountLocal(dailyTranDetail.getTranAmt());
//        tTransactionDto.setDatePosting(dailyTranDetail.getPstdDate());
//        tTransactionDto.setDateTransaction(dailyTranDetail.getTranDate());
//        tTransactionDto.setInternalRefNumber(dailyTranDetail.getRefNum());
//        tTransactionDto.setIsActive(true);
//        tTransactionDto.setLateDeposit(false);
//        tTransactionDto.setTransactionNumber(dailyTranDetail.getTranId());
//        tTransactionDto.setTransmodeComment(dailyTranDetail.getPartTranType());
//        tTransactionDto.setValueDate(dailyTranDetail.getValueDate());
//        tTransactionDto.setStatusId(1);
//        tTransactionDto.setReportId(1);
//        tTransactionDto.setConductionTypeId(2);
//        tTransactionDto.setRptCode(dailyTranDetail.getRptCode() != null ? dailyTranDetail.getRptCode() : "N/A");
//        tTransactionDto.setTransactionDescription(dailyTranDetail.getTranParticular());
//        tTransactionDto.setTransactionLocation(dailyTranDetail.getBrCode() != null ? dailyTranDetail.getBrCode() : "N/A");
//        if (dailyTranDetail.getRptCode() != null) {
//            Optional<RptCode> optionalRptCode = rptCodeList.stream()
//                    .filter(rptCode -> rptCode.getRptCodeName().equalsIgnoreCase(dailyTranDetail.getRptCode()))
//                    .findFirst();
//            tTransactionDto.setTrxNo(optionalRptCode.map(rptCode -> rptCode.getGoAmlTrxCodes().getTransactionCodeName()).orElse("N/A"));
//        } else {
//            tTransactionDto.setTrxNo("N/A");
//        }
//        return tTransactionDto;
//    }

    @Transactional
    @Override
    public ResponseDto saveDailyTranToTable(TranRequestDto tranRequestDto) {
        log.info("Request to save daily transaction to table");
        int processedCount = 0;

        try {
            // delete all data
            tTransactionDao.deleteAll();
            log.info("Existing data are cleared");

            List<RptCode> rptCodeList = rptCodeDao.findAll();

            Pageable pageable = PageRequest.of(0, 100);
            Page<DailyTranDetailTableTemp> page;
            do {
                page = getDailyTranDetailPage(tranRequestDto, pageable);
                for (DailyTranDetailTableTemp dailyTranDetail : page) {
                    TTransactionDto tTransactionDto = createTTransactionDto(dailyTranDetail, rptCodeList);
                    tTransactionDao.save(tTransactionMapper.toEntity(tTransactionDto));
                    log.info("tran saved: {}", tTransactionDto.getTransactionNumber());
                    processedCount++;
                }

                pageable = page.nextPageable();

                if (!page.hasNext()) {
                    log.info("No more content to process");
                    break;
                }
            } while (page.hasNext());

            return new ResponseDto("success", "count: " + processedCount, "Success");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "Error", "Error");
        }
    }

    private Page<DailyTranDetailTableTemp> getDailyTranDetailPage(TranRequestDto tranRequestDto, Pageable pageable) {
        if (tranRequestDto.getRtpCode().isEmpty()) {
            return dailyTranDetailTableTempDao.findAllByTranDateBetween(tranRequestDto.getStartDate(), tranRequestDto.getEndDate(), pageable);
        } else {
            return dailyTranDetailTableTempDao.findAllByTranDateBetweenAndRptCodeIn(tranRequestDto.getStartDate(), tranRequestDto.getEndDate(), tranRequestDto.getRtpCode(), pageable);
        }
    }

    private TTransactionDto createTTransactionDto(DailyTranDetailTableTemp dailyTranDetail, List<RptCode> rptCodeList) {
        TTransactionDto tTransactionDto = new TTransactionDto();
        tTransactionDto.setAmountLocal(dailyTranDetail.getTranAmt());
        tTransactionDto.setDatePosting(dailyTranDetail.getPstdDate());
        tTransactionDto.setDateTransaction(dailyTranDetail.getTranDate());
        tTransactionDto.setInternalRefNumber(dailyTranDetail.getRefNum());
        tTransactionDto.setIsActive(true);
        tTransactionDto.setLateDeposit(false);
        tTransactionDto.setTransactionNumber(dailyTranDetail.getTranId());
        tTransactionDto.setTransmodeComment(dailyTranDetail.getPartTranType());
        tTransactionDto.setValueDate(dailyTranDetail.getValueDate());
        tTransactionDto.setStatusId(1);
        tTransactionDto.setReportId(1);
        tTransactionDto.setConductionTypeId(2);
        tTransactionDto.setRptCode(dailyTranDetail.getRptCode() != null ? dailyTranDetail.getRptCode() : "N/A");
        tTransactionDto.setTransactionDescription(dailyTranDetail.getTranParticular());
        tTransactionDto.setTransactionLocation(dailyTranDetail.getBrCode() != null ? dailyTranDetail.getBrCode() : "N/A");
        if (dailyTranDetail.getRptCode() != null) {
            Optional<RptCode> optionalRptCode = rptCodeList.stream()
                    .filter(rptCode -> rptCode.getRptCodeName().equalsIgnoreCase(dailyTranDetail.getRptCode()))
                    .findFirst();
            tTransactionDto.setTrxNo(optionalRptCode.map(rptCode -> rptCode.getGoAmlTrxCodes().getTransactionCodeName()).orElse("N/A"));
        } else {
            tTransactionDto.setTrxNo("N/A");
        }
        return tTransactionDto;
    }
}
