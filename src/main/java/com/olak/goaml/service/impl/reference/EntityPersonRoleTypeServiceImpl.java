package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.reference.REntityPersonRoleTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.REntityPersonRoleTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.REntityPersonRoleTypeMapper;
import com.olak.goaml.models.reference.REntityPersonRoleType;
import com.olak.goaml.service.reference.EntityPersonRoleTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class EntityPersonRoleTypeServiceImpl implements EntityPersonRoleTypeService {

    private final REntityPersonRoleTypeDao entityPersonRoleTypeDao;
    private final RStatusDao statusDao;
    private final REntityPersonRoleTypeMapper entityPersonRoleTypeMapper;
    private final SearchNFilter searchNFilter;

    private final RStatusDao rStatusDao;

    public EntityPersonRoleTypeServiceImpl(RStatusDao rStatusDao, REntityPersonRoleTypeDao entityPersonRoleTypeDao, RStatusDao statusDao, REntityPersonRoleTypeMapper entityPersonRoleTypeMapper, SearchNFilter searchNFilter) {
        this.entityPersonRoleTypeDao = entityPersonRoleTypeDao;
        this.statusDao = statusDao;
        this.entityPersonRoleTypeMapper = entityPersonRoleTypeMapper;
        this.searchNFilter = searchNFilter;
        this.rStatusDao = rStatusDao;
    }

    @Override
    public ResponseEntity<List<REntityPersonRoleTypeDto>> getAllEntityPersonRoleTypes() {
        log.info("Inside EntityPersonRoleTypeService: getAllEntityPersonRoleTypes method");
        List<REntityPersonRoleType> resultList = entityPersonRoleTypeDao.findByIsActive(true);
        return new ResponseEntity<>(entityPersonRoleTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<REntityPersonRoleTypeDto>>> getEntityPersonRoleTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside EntityPersonRoleTypeService: getEntityPersonRoleTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(REntityPersonRoleType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());

        ApiResponseDto<List<REntityPersonRoleTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(entityPersonRoleTypeMapper.listToDto((List<REntityPersonRoleType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<REntityPersonRoleTypeDto> getEntityPersonRoleTypeById(Integer id) {
        log.info("Inside EntityPersonRoleTypeService: getEntityPersonRoleTypeById method");

        try {
            Optional<REntityPersonRoleType> resultOp = entityPersonRoleTypeDao.findByEntityPersonRoleTypeIdAndIsActive(id, true);
            REntityPersonRoleTypeDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("EntityPersonRoleType not found by ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = entityPersonRoleTypeMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting EntityPersonRoleType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<REntityPersonRoleTypeDto> addEntityPersonRoleType(REntityPersonRoleTypeDto entityPersonRoleTypeDto) {
        log.info("Inside EntityPersonRoleTypeService: addEntityPersonRoleType method");

        try {
            if (entityPersonRoleTypeDto.getEntityPersonRoleTypeCode() == null || entityPersonRoleTypeDto.getEntityPersonRoleTypeCode().isEmpty()) {
                throw new BadRequestAlertException("EntityPersonRoleType code is required.", ENTITY_NAME, "error");
            }

            Optional<REntityPersonRoleType> existingEntity = entityPersonRoleTypeDao.findByEntityPersonRoleTypeCodeAndIsActive(entityPersonRoleTypeDto.getEntityPersonRoleTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("EntityPersonRoleType already exists with code: " + entityPersonRoleTypeDto.getEntityPersonRoleTypeCode(), ENTITY_NAME, "error");
            }

            if (entityPersonRoleTypeDto.getDescription() == null || entityPersonRoleTypeDto.getDescription().isEmpty()) {
                throw new BadRequestAlertException("EntityPersonRoleType description is required.", ENTITY_NAME, "error");
            }

            if (entityPersonRoleTypeDto.getStatusId() == null || !statusDao.existsById(entityPersonRoleTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
            }

            entityPersonRoleTypeDto.setIsActive(true);
            entityPersonRoleTypeDto.setStatusId(1);



            REntityPersonRoleType entity = entityPersonRoleTypeMapper.toEntity(entityPersonRoleTypeDto);
            entity.setRStatus(statusDao.findById(entityPersonRoleTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "error")));
            entity = entityPersonRoleTypeDao.save(entity);

            return new ResponseEntity<>(entityPersonRoleTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding EntityPersonRoleType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<REntityPersonRoleTypeDto> updateEntityPersonRoleTypeById(Integer id, REntityPersonRoleTypeDto entityPersonRoleTypeDto) {
        log.info("Inside EntityPersonRoleTypeService: updateEntityPersonRoleTypeById method");

        if (!id.equals(entityPersonRoleTypeDto.getEntityPersonRoleTypeId())) {
            throw new BadRequestAlertException("EntityPersonRoleType ID mismatch.", ENTITY_NAME, "error");
        }

        if (entityPersonRoleTypeDto.getEntityPersonRoleTypeCode() == null || entityPersonRoleTypeDto.getEntityPersonRoleTypeCode().isEmpty()) {
            throw new BadRequestAlertException("EntityPersonRoleType Code is required.", ENTITY_NAME, "error");
        }

        if (entityPersonRoleTypeDao.existsByEntityPersonRoleTypeCodeAndIsActiveAndEntityPersonRoleTypeIdNot(entityPersonRoleTypeDto.getEntityPersonRoleTypeCode(), true, id)) {
            throw new BadRequestAlertException("EntityPersonRoleType Code is duplicate with another record.", ENTITY_NAME, "error");
        }

        if (entityPersonRoleTypeDto.getStatusId() == null) {
            throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
        }

        if (!rStatusDao.existsById(entityPersonRoleTypeDto.getStatusId())) {
            throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
        }

        Optional<REntityPersonRoleType> existingEntityOp = entityPersonRoleTypeDao.findByEntityPersonRoleTypeIdAndIsActive(id, true);
        if (!existingEntityOp.isPresent()) {
            throw new BadRequestAlertException("EntityPersonRoleType not found by ID: " + id, ENTITY_NAME, "error");
        } else {
            REntityPersonRoleType existingEntity = existingEntityOp.get();

            existingEntity.setEntityPersonRoleTypeCode(entityPersonRoleTypeDto.getEntityPersonRoleTypeCode());
            existingEntity.setDescription(entityPersonRoleTypeDto.getDescription());
            existingEntity.setIsActive(entityPersonRoleTypeDto.getIsActive());
            existingEntity.setRStatus(rStatusDao.findById(entityPersonRoleTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error")));

            try {
                existingEntity = entityPersonRoleTypeDao.save(existingEntity);
                return new ResponseEntity<>(entityPersonRoleTypeMapper.toDto(existingEntity), HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while updating EntityPersonRoleType: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }


    @Override
    public ResponseEntity<REntityPersonRoleTypeDto> deleteEntityPersonRoleTypeById(Integer id) {
        log.info("Inside EntityPersonRoleTypeService: deleteEntityPersonRoleTypeById method");

        try {
            Optional<REntityPersonRoleType> resultOp = entityPersonRoleTypeDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("EntityPersonRoleType not found by ID: " + id, ENTITY_NAME, "idinvalid");
            } else {
                REntityPersonRoleType existingEntity = resultOp.get();
                existingEntity.setIsActive(false);
                REntityPersonRoleTypeDto responseDto = entityPersonRoleTypeMapper.toDto(entityPersonRoleTypeDao.save(existingEntity));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting EntityPersonRoleType: {}", e.getMessage());
            throw new BadRequestAlertException("EntityPersonRoleType could not be deleted by ID: " + id, ENTITY_NAME, "deleteerror");
        }
    }

}
