package com.olak.goaml.service.impl.reference;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.olak.goaml.dao.reference.RConductionTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RConductionTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RConductionTypeMapper;
import com.olak.goaml.models.reference.RConductionType;
import com.olak.goaml.service.reference.ConductionTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ConductionTypeServiceImpl implements ConductionTypeService {

    private final RStatusDao rStatusDao;
    private final RConductionTypeDao rConductionTypeDao;
    private final RConductionTypeMapper rConductionTypeMapper;
    private final SearchNFilter searchNFilter;

    public ConductionTypeServiceImpl(RStatusDao rStatusDao, RConductionTypeDao rConductionTypeDao, RConductionTypeMapper rConductionTypeMapper, SearchNFilter searchNFilter){
        this.rStatusDao = rStatusDao;
        this.rConductionTypeDao = rConductionTypeDao;
        this.rConductionTypeMapper = rConductionTypeMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RConductionTypeDto>> getAllConductionTypes() {
        log.info("Inside ConductionTypeService: getAllConductionTypes method");
        List<RConductionType> resultList = rConductionTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rConductionTypeMapper.listToDto(resultList), HttpStatus.OK);
    }



    @Override
    public ResponseEntity<ApiResponseDto<List<RConductionTypeDto>>> getConductionTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside ConductionTypeService: getConductionTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RConductionType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RConductionTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rConductionTypeMapper.listToDto((List<RConductionType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RConductionTypeDto> addConductionType(RConductionTypeDto conductionTypeDto) {
        log.info("Inside ConductionTypeService: addConductionType method");

        try {
            if (conductionTypeDto.getConductionTypeCode() == null || conductionTypeDto.getConductionTypeCode().isEmpty()) {
                throw new BadRequestAlertException("ConductionType code is required.", ENTITY_NAME, "error");
            }

            Optional<RConductionType> existingEntity = rConductionTypeDao.findByConductionTypeCodeAndIsActive(conductionTypeDto.getConductionTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("ConductionType already exists with code: " + conductionTypeDto.getConductionTypeCode(), ENTITY_NAME, "idexists");
            }

            if (conductionTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(conductionTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "idinvalid");
            }

            conductionTypeDto.setIsActive(true);
             conductionTypeDto.setStatusId(1);

            RConductionType entity = rConductionTypeMapper.toEntity(conductionTypeDto);
            entity.setRStatus(rStatusDao.findById(conductionTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "idinvalid")));
            entity = rConductionTypeDao.save(entity);

            return new ResponseEntity<>(rConductionTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding ConductionType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RConductionTypeDto> getConductionTypeById(Integer id) {
        log.info("Inside ConductionTypeService: getConductionTypeById method");

        try {
            Optional<RConductionType> entity = rConductionTypeDao.findByConductionTypeIdAndIsActive(id, true);
            RConductionTypeDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("ConductionType not found by ID: " + id, ENTITY_NAME, "idinvalid");
            } else {
                responseDto = rConductionTypeMapper.toDto(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting ConductionType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RConductionTypeDto> updateConductionTypeById(Integer id, RConductionTypeDto conductionTypeDto) {
        log.info("Inside ConductionTypeService: updateConductionTypeById method");

        Optional<RConductionType> existingEntity = rConductionTypeDao.findByConductionTypeIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("Invalid conduction type ID", ENTITY_NAME, "idinvalid");
        }

        if (!id.equals(conductionTypeDto.getConductionTypeId())) {
            throw new BadRequestAlertException("ConductionType id mismatch.", ENTITY_NAME, "error");
        }

        if (conductionTypeDto.getConductionTypeCode() == null || conductionTypeDto.getConductionTypeCode().isEmpty()) {
            throw new BadRequestAlertException("ConductionType Code is required.", ENTITY_NAME, "error");
        }

        if (rConductionTypeDao.existsByConductionTypeCodeAndIsActiveAndConductionTypeIdNot(conductionTypeDto.getConductionTypeCode(), true, id)) {
            throw new BadRequestAlertException("ConductionType Code is duplicate with another record.", ENTITY_NAME, "error");
        }

        if (conductionTypeDto.getStatusId() == null) {
            throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
        }

        if (!rStatusDao.existsById(conductionTypeDto.getStatusId())) {
            throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
        }

        RConductionType entity = existingEntity.get();
        entity.setConductionTypeCode(conductionTypeDto.getConductionTypeCode());
        entity.setDescription(conductionTypeDto.getDescription());
        entity.setIsActive(conductionTypeDto.getIsActive());
        entity.setRStatus(rStatusDao.findById(conductionTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID.", ENTITY_NAME, "error")));

        try {
            entity = rConductionTypeDao.save(entity);
            return new ResponseEntity<>(rConductionTypeMapper.toDto(entity), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while updating ConductionType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RConductionTypeDto> deleteConductionTypeById(Integer id) {
        log.info("Inside ConductionTypeService: deleteConductionTypeById method");

        try {
            Optional<RConductionType> resultOp = rConductionTypeDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("ConductionType not found by ID: " + id, ENTITY_NAME, "error");
            } else {
                RConductionType existingEntity = resultOp.get();

                existingEntity.setIsActive(false);
                RConductionTypeDto responseDto = rConductionTypeMapper.toDto(rConductionTypeDao.save(existingEntity));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting ConductionType: {}", e.getMessage());
            throw new BadRequestAlertException("ConductionType could not be deleted by ID: " + id, ENTITY_NAME, "error");
        }
    }

}



