package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.reference.RCurrencyDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RCurrencyDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RCurrencyMapper;
import com.olak.goaml.models.reference.RCurrency;
import com.olak.goaml.service.reference.CurrencyService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class CurrencyServiceImpl implements CurrencyService {

    private final RStatusDao rStatusDao;
    private final RCurrencyDao rCurrencyDao;
    private final RCurrencyMapper rCurrencyMapper;
    private final SearchNFilter searchNFilter;

    public CurrencyServiceImpl(RStatusDao rStatusDao, RCurrencyDao rCurrencyDao, RCurrencyMapper rCurrencyMapper, SearchNFilter searchNFilter) {
        this.rStatusDao = rStatusDao;
        this.rCurrencyDao = rCurrencyDao;
        this.rCurrencyMapper = rCurrencyMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RCurrencyDto>> getAllCurrencies() {
        log.info("Inside CurrencyService: getAllCurrencies method");
        List<RCurrency> resultList = rCurrencyDao.findByIsActive(true);
        return new ResponseEntity<>(rCurrencyMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RCurrencyDto>>> getCurrenciesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside CurrencyService: getCurrenciesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RCurrency.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RCurrencyDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rCurrencyMapper.listToDto((List<RCurrency>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RCurrencyDto> addCurrency(RCurrencyDto currencyDto) {
        log.info("Inside CurrencyService: addCurrency method");

        try {
            if (currencyDto.getCurrenciesCode() == null || currencyDto.getCurrenciesCode().isEmpty()) {
                throw new BadRequestAlertException("Currency code is required.", ENTITY_NAME, "error");
            }

            Optional<RCurrency> existingEntity = rCurrencyDao.findByCurrenciesCodeAndIsActive(currencyDto.getCurrenciesCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("Currency already exists with code: " + currencyDto.getCurrenciesCode(), ENTITY_NAME, "idexists");
            }

            if (currencyDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(currencyDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "idinvalid");
            }

            currencyDto.setIsActive(true);

            RCurrency entity = rCurrencyMapper.toEntity(currencyDto);
            entity.setRStatus(rStatusDao.findById(currencyDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "idinvalid")));
            entity = rCurrencyDao.save(entity);

            return new ResponseEntity<>(rCurrencyMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding Currency: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RCurrencyDto> getCurrencyById(Integer id) {
        log.info("Inside CurrencyService: getCurrencyById method");

        try {
            Optional<RCurrency> entity = rCurrencyDao.findByCurrenciesIdAndIsActive(id, true);
            RCurrencyDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("Currency not found by ID: " + id, ENTITY_NAME, "idinvalid");
            } else {
                responseDto = rCurrencyMapper.toDto(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting Currency details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RCurrencyDto> updateCurrencyById(Integer id, RCurrencyDto currencyDto) {
        log.info("Inside CurrencyService: updateCurrencyById method");

        Optional<RCurrency> existingEntity = rCurrencyDao.findByCurrenciesIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("Invalid currency ID", ENTITY_NAME, "idinvalid");
        }

        if (!id.equals(currencyDto.getCurrenciesId())) {
            throw new BadRequestAlertException("Currency ID mismatch.", ENTITY_NAME, "error");
        }

        if (currencyDto.getCurrenciesCode() == null || currencyDto.getCurrenciesCode().isEmpty()) {
            throw new BadRequestAlertException("Currency Code is required.", ENTITY_NAME, "error");
        }

        if (rCurrencyDao.existsByCurrenciesCodeAndIsActiveAndCurrenciesIdNot(currencyDto.getCurrenciesCode(), true, id)) {
            throw new BadRequestAlertException("Currency Code is duplicate with another record.", ENTITY_NAME, "error");
        }

        if (currencyDto.getStatusId() == null) {
            throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
        }

        if (!rStatusDao.existsById(currencyDto.getStatusId())) {
            throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
        }

        RCurrency entity = existingEntity.get();
        entity.setCurrenciesCode(currencyDto.getCurrenciesCode());
        entity.setDescription(currencyDto.getDescription());
        entity.setIsActive(currencyDto.getIsActive());
        entity.setRStatus(rStatusDao.findById(currencyDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID.", ENTITY_NAME, "error")));

        try {
            entity = rCurrencyDao.save(entity);
            return new ResponseEntity<>(rCurrencyMapper.toDto(entity), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while updating Currency: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RCurrencyDto> deleteCurrencyById(Integer id) {
        log.info("Inside CurrencyService: deleteCurrencyById method");

        Optional<RCurrency> existingEntity = rCurrencyDao.findByCurrenciesIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("Invalid currency ID", ENTITY_NAME, "idinvalid");
        }

        RCurrency entity = existingEntity.get();
        entity.setIsActive(false);

        try {
            entity = rCurrencyDao.save(entity);
            return new ResponseEntity<>(rCurrencyMapper.toDto(entity), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting Currency: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
