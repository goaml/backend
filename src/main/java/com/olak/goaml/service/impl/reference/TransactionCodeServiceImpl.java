package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.master.TransactionCodesDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.master.TransactionCodesDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.parameter.TransactionCodesMapper;
import com.olak.goaml.models.master.TransactionCodes;
import com.olak.goaml.service.reference.TransactionCodeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TransactionCodeServiceImpl implements TransactionCodeService {

    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final TransactionCodesDao transactionCodesDao;
    private final TransactionCodesMapper transactionCodesMapper;

    public TransactionCodeServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, TransactionCodesDao transactionCodesDao, TransactionCodesMapper transactionCodesMapper) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.transactionCodesDao = transactionCodesDao;
        this.transactionCodesMapper = transactionCodesMapper;
    }

    @Override
    public ResponseEntity<List<TransactionCodesDto>> getAllTransactionCodes() {
        log.info("Inside TransactionCodesService: getAllTransactionCodes method");
        List<TransactionCodes> resultList = transactionCodesDao.findByIsActive(true);
        return new ResponseEntity<>(transactionCodesMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TransactionCodesDto>>> getTransactionCodesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside NatureTypeService: getNatureTypeWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(TransactionCodes.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<TransactionCodesDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(transactionCodesMapper.listToDto((List<TransactionCodes>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<TransactionCodesDto> addTransactionCodes(TransactionCodesDto transactionCodesDto) {
        log.info("Inside TransactionCodesService: addTransactionCodes method");

        try {
            if (transactionCodesDto.getTransactionCodeName() == null || transactionCodesDto.getTransactionCodeName().isEmpty())
                throw new BadRequestAlertException("TransactionCodes name is required: ", ENTITY_NAME, "error");

            Optional<TransactionCodes> resultOp = transactionCodesDao.findByTransactionCodeNameAndIsActive(transactionCodesDto.getTransactionCodeName(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("TransactionCodes already exists by TransactionCodes name: " + transactionCodesDto.getTransactionCodeName(), ENTITY_NAME, "error");

            if (transactionCodesDto.getTransactionCodeDescription() == null || transactionCodesDto.getTransactionCodeDescription().isEmpty())
                throw new BadRequestAlertException("TransactionCodes Description is required: ", ENTITY_NAME, "error");

            Optional<TransactionCodes> resultOp1 = transactionCodesDao.findByTransactionCodeDescriptionAndIsActive(transactionCodesDto.getTransactionCodeDescription(), true);
            if (resultOp1.isPresent())
                throw new BadRequestAlertException("TransactionCodes already exists by TransactionCodes Description: " + transactionCodesDto.getTransactionCodeDescription(), ENTITY_NAME, "error");

//            if (transactionCodesDto.getStatusId() == null)
            transactionCodesDto.setStatusId(1);

            transactionCodesDto.setIsActive(true);

//            if (!rStatusDao.existsById(transactionCodesDto.getStatusId()))
//                throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

            TransactionCodesDto saveDtoRes = transactionCodesMapper.toDto(transactionCodesDao.save(transactionCodesMapper.toEntity(transactionCodesDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add TransactionCodes: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<TransactionCodesDto> getTransactionCodesById(Long id) {
        log.info("Inside TransactionCodesService: getTransactionCodesById method");

        try {
            Optional<TransactionCodes> resultOp = transactionCodesDao.findByTransactionCodeIdAndIsActive(id, true);
            TransactionCodesDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("TransactionCodes not found by TransactionCodes ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = transactionCodesMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get TransactionCodes details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<TransactionCodesDto> updateTransactionCodesById(Long id, TransactionCodesDto transactionCodesDto) {
        log.info("Inside TransactionCodesService: updateTransactionCodesById method");

        // Validate
        if (id != transactionCodesDto.getTransactionCodeId())
            throw new BadRequestAlertException("TransactionCodes id mismatch.", ENTITY_NAME, "error");

        if (transactionCodesDto.getTransactionCodeName() == null || transactionCodesDto.getTransactionCodeName().isEmpty())
            throw new BadRequestAlertException("TransactionCodes Name is required: ", ENTITY_NAME, "error");

        if (transactionCodesDao.existsByTransactionCodeNameAndIsActiveAndTransactionCodeIdNot(transactionCodesDto.getTransactionCodeName(), true, id))
            throw new BadRequestAlertException("TransactionCodes Name is duplicate with another recode: ", ENTITY_NAME, "error");

//        if (transactionCodesDto.getStatusId() == null)
//            throw new BadRequestAlertException("Status Can Not Be Empty: ", ENTITY_NAME, "error");
//
//        if (!rStatusDao.existsById(transactionCodesDto.getStatusId()))
//            throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

        Optional<TransactionCodes> resultOp = transactionCodesDao.findByTransactionCodeIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("TransactionCodes not found by TransactionCodes ID: " + id, ENTITY_NAME, "error");
        } else {
            TransactionCodes existingRes = resultOp.get();
            transactionCodesDto.setIsActive(existingRes.getIsActive());
            transactionCodesDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                TransactionCodesDto responseDto = transactionCodesMapper.toDto(transactionCodesDao.save(transactionCodesMapper.toEntity(transactionCodesDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update TransactionCodes by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<TransactionCodesDto> deleteTransactionCodesById(Long id) {
        log.info("Inside TransactionCodesService: deleteTransactionCodesById method");

        try {
            Optional<TransactionCodes> resultOp = transactionCodesDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("TransactionCodes not found by TransactionCodes ID: " + id, ENTITY_NAME, "error");
            } else {
                TransactionCodes existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                TransactionCodesDto responseDto = transactionCodesMapper.toDto(existingRes);
                responseDto = transactionCodesMapper.toDto(transactionCodesDao.save(transactionCodesMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete TransactionCodes: {}", e.getMessage());
            throw new BadRequestAlertException("TransactionCodes con not delete by TransactionCodes ID: " + id, ENTITY_NAME, "error");
        }
    }
}
