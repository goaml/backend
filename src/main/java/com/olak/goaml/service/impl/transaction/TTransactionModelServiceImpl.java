package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TTransactionDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TTransactionMapper;
import com.olak.goaml.models.xml.transaction.TTransaction;
import com.olak.goaml.service.transaction.TTransactionModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TTransactionModelServiceImpl implements TTransactionModelService {
    private final TTransactionDao tTransactionDao;
    private final TTransactionMapper tTransactionMapper;
    private final FilterUtility<TTransaction> filterUtility;

    public TTransactionModelServiceImpl(TTransactionDao tTransactionDao, TTransactionMapper tTransactionMapper) {
        this.tTransactionDao = tTransactionDao;
        this.filterUtility = new FilterUtility<>(tTransactionDao);
        this.tTransactionMapper = tTransactionMapper;
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TTransactionDto>>> getAllTransactionDetails(Integer page, Integer perPage, String search, String sort, String direction, String rptCode, String trxNo) {
        log.info("Inside TReportModelService: getAllReportDetails method");
        try {

            List<FilterCriteria<TTransaction>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("transactionId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (rptCode != null && !rptCode.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("rptCode"), rptCode)
                );
            }

            if (trxNo != null && !trxNo.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("trxNo"), trxNo)
                );
            }

            // pageable
            Page<TTransaction> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TTransactionDto> content = tTransactionMapper.listToDto(pageableData.getContent());

            // response
            ApiResponseDto<List<TTransactionDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
