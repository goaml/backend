package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.master.MultiPartyRoleDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.master.MultiPartyRoleDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.parameter.MultiPartyRoleMapper;
import com.olak.goaml.models.master.MultiPartyRole;
import com.olak.goaml.service.reference.MultiPartyRoleService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class MultiPartyRoleServiceImpl implements MultiPartyRoleService {

    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final MultiPartyRoleDao multiPartyRoleDao;
    private final MultiPartyRoleMapper multiPartyRoleMapper;

    public MultiPartyRoleServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, MultiPartyRoleDao multiPartyRoleDao, MultiPartyRoleMapper multiPartyRoleMapper) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.multiPartyRoleDao = multiPartyRoleDao;
        this.multiPartyRoleMapper = multiPartyRoleMapper;
    }

    @Override
    public ResponseEntity<List<MultiPartyRoleDto>> getAllMultiPartyRole() {
        log.info("Inside MultiPartyRoleService: getAllMultiPartyRole method");
        List<MultiPartyRole> resultList = multiPartyRoleDao.findByIsActive(true);
        return new ResponseEntity<>(multiPartyRoleMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<MultiPartyRoleDto>>> getMultiPartyRoleWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside NatureTypeService: getNatureTypeWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(MultiPartyRole.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<MultiPartyRoleDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(multiPartyRoleMapper.listToDto((List<MultiPartyRole>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<MultiPartyRoleDto> addMultiPartyRole(MultiPartyRoleDto multiPartyRoleDto) {
        log.info("Inside MultiPartyRoleService: addMultiPartyRole method");

        try {
            if (multiPartyRoleDto.getMultiPartyRoleName() == null || multiPartyRoleDto.getMultiPartyRoleName().isEmpty())
                throw new BadRequestAlertException("MultiPartyRole name is required: ", ENTITY_NAME, "error");

            Optional<MultiPartyRole> resultOp = multiPartyRoleDao.findByMultiPartyRoleNameAndIsActive(multiPartyRoleDto.getMultiPartyRoleName(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("MultiPartyRole already exists by MultiPartyRole name: " + multiPartyRoleDto.getMultiPartyRoleName(), ENTITY_NAME, "error");

            if (multiPartyRoleDto.getMultiPartyRoleDescription() == null || multiPartyRoleDto.getMultiPartyRoleDescription().isEmpty())
                throw new BadRequestAlertException("MultiPartyRole Description is required: ", ENTITY_NAME, "error");

            Optional<MultiPartyRole> resultOp1 = multiPartyRoleDao.findByMultiPartyRoleDescriptionAndIsActive(multiPartyRoleDto.getMultiPartyRoleDescription(), true);
            if (resultOp1.isPresent())
                throw new BadRequestAlertException("MultiPartyRole already exists by MultiPartyRole Description: " + multiPartyRoleDto.getMultiPartyRoleDescription(), ENTITY_NAME, "error");

//            if (multiPartyRoleDto.getStatusId() == null)
                multiPartyRoleDto.setStatusId(1);

            multiPartyRoleDto.setIsActive(true);

//            if (!rStatusDao.existsById(multiPartyRoleDto.getStatusId()))
//                throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

            MultiPartyRoleDto saveDtoRes = multiPartyRoleMapper.toDto(multiPartyRoleDao.save(multiPartyRoleMapper.toEntity(multiPartyRoleDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add MultiPartyRole: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<MultiPartyRoleDto> getMultiPartyRole(Long id) {
        log.info("Inside MultiPartyRoleService: getMultiPartyRoleById method");

        try {
            Optional<MultiPartyRole> resultOp = multiPartyRoleDao.findByMultiPartyRoleIdAndIsActive(id, true);
            MultiPartyRoleDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("MultiPartyRole not found by MultiPartyRole ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = multiPartyRoleMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get MultiPartyRole details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<MultiPartyRoleDto> updateMultiPartyRoleById(Long id, MultiPartyRoleDto multiPartyRoleDto) {
        log.info("Inside MultiPartyRoleService: updateMultiPartyRoleById method");

        // Validate
        if (id != multiPartyRoleDto.getMultiPartyRoleId())
            throw new BadRequestAlertException("MultiPartyRole id mismatch.", ENTITY_NAME, "error");

        if (multiPartyRoleDto.getMultiPartyRoleName() == null || multiPartyRoleDto.getMultiPartyRoleName().isEmpty())
            throw new BadRequestAlertException("MultiPartyRole Name is required: ", ENTITY_NAME, "error");

        if (multiPartyRoleDao.existsByMultiPartyRoleNameAndIsActiveAndMultiPartyRoleIdNot(multiPartyRoleDto.getMultiPartyRoleName(), true, id))
            throw new BadRequestAlertException("MultiPartyRole Name is duplicate with another recode: ", ENTITY_NAME, "error");

//        if (multiPartyRoleDto.getStatusId() == null)
//            throw new BadRequestAlertException("Status Can Not Be Empty: ", ENTITY_NAME, "error");
//
//        if (!rStatusDao.existsById(multiPartyRoleDto.getStatusId()))
//            throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

        Optional<MultiPartyRole> resultOp = multiPartyRoleDao.findByMultiPartyRoleIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("MultiPartyRole not found by MultiPartyRole ID: " + id, ENTITY_NAME, "error");
        } else {
            MultiPartyRole existingRes = resultOp.get();
            multiPartyRoleDto.setIsActive(existingRes.getIsActive());
            multiPartyRoleDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                MultiPartyRoleDto responseDto = multiPartyRoleMapper.toDto(multiPartyRoleDao.save(multiPartyRoleMapper.toEntity(multiPartyRoleDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update MultiPartyRole by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<MultiPartyRoleDto> deleteMultiPartyRoleById(Long id) {
        log.info("Inside MultiPartyRoleService: deleteMultiPartyRoleById method");

        try {
            Optional<MultiPartyRole> resultOp = multiPartyRoleDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("MultiPartyRole not found by MultiPartyRole ID: " + id, ENTITY_NAME, "error");
            } else {
                MultiPartyRole existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                MultiPartyRoleDto responseDto = multiPartyRoleMapper.toDto(existingRes);
                responseDto = multiPartyRoleMapper.toDto(multiPartyRoleDao.save(multiPartyRoleMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete MultiPartyRole: {}", e.getMessage());
            throw new BadRequestAlertException("MultiPartyRole con not delete by MultiPartyRole ID: " + id, ENTITY_NAME, "error");
        }
    }
}
