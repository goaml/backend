package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.reference.REntityLegalFormTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.reference.REntityLegalFormTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.REntityLegalFormTypeMapper;
import com.olak.goaml.models.reference.REntityLegalFormType;
import com.olak.goaml.service.reference.EntityLegalFormTypeService;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class EntityLegalFormTypeServiceImpl implements EntityLegalFormTypeService {

    private final REntityLegalFormTypeDao rEntityLegalFormTypeDao;
    private final SearchNFilter searchNFilter;
    private final REntityLegalFormTypeMapper rEntityLegalFormTypeMapper;

    public EntityLegalFormTypeServiceImpl(REntityLegalFormTypeDao rEntityLegalFormTypeDao, SearchNFilter searchNFilter, REntityLegalFormTypeMapper rEntityLegalFormTypeMapper) {
        this.rEntityLegalFormTypeDao = rEntityLegalFormTypeDao;
        this.searchNFilter = searchNFilter;
        this.rEntityLegalFormTypeMapper = rEntityLegalFormTypeMapper;
    }

    @Override
    public ResponseEntity<List<REntityLegalFormTypeDto>> getAllEntityLegalFormTypes() {
        log.info("Inside EntityLegalFormTypeService: getAllEntityLegalFormTypes method");
        List<REntityLegalFormType> entities = rEntityLegalFormTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rEntityLegalFormTypeMapper.listToDto(entities), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<REntityLegalFormTypeDto>>> getEntityLegalFormTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside EntityLegalFormTypeService: getEntityLegalFormTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(REntityLegalFormType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<REntityLegalFormTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rEntityLegalFormTypeMapper.listToDto((List<REntityLegalFormType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<REntityLegalFormTypeDto> getEntityLegalFormTypeById(Integer id) {
        log.info("Inside EntityLegalFormTypeService: getEntityLegalFormTypeById method");

        try {
            Optional<REntityLegalFormType> resultOp = rEntityLegalFormTypeDao.findByEntityLegalFormTypeIdAndIsActive(id, true);
            REntityLegalFormTypeDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Entity Legal Form Type not found by ID: " + id, ENTITY_NAME, "notfound");
            } else {
                responseDto = rEntityLegalFormTypeMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting Entity Legal Form Type details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<REntityLegalFormTypeDto> addEntityLegalFormType(REntityLegalFormTypeDto entityLegalFormTypeDto) {
        log.info("Inside EntityLegalFormTypeService: addEntityLegalFormType method");

        try {
            if (entityLegalFormTypeDto.getEntityLegalFormTypeCode() == null || entityLegalFormTypeDto.getEntityLegalFormTypeCode().isEmpty())
                throw new BadRequestAlertException("Entity Legal Form Type code is required.", ENTITY_NAME, "error");

            Optional<REntityLegalFormType> existingEntity = rEntityLegalFormTypeDao.findByEntityLegalFormTypeCodeAndIsActive(entityLegalFormTypeDto.getEntityLegalFormTypeCode(), true);
            if (existingEntity.isPresent())
                throw new BadRequestAlertException("Entity Legal Form Type already exists with code: " + entityLegalFormTypeDto.getEntityLegalFormTypeCode(), ENTITY_NAME, "error");

            if (entityLegalFormTypeDto.getDescription() == null || entityLegalFormTypeDto.getDescription().isEmpty())
                throw new BadRequestAlertException("Description is required.", ENTITY_NAME, "error");

            entityLegalFormTypeDto.setIsActive(true);
            entityLegalFormTypeDto.setStatusId(1);


            REntityLegalFormType entity = rEntityLegalFormTypeMapper.toEntity(entityLegalFormTypeDto);
            REntityLegalFormType savedEntity = rEntityLegalFormTypeDao.save(entity);
            REntityLegalFormTypeDto responseDto = rEntityLegalFormTypeMapper.toDto(savedEntity);

            return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding Entity Legal Form Type: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }






    @Override
    public ResponseEntity<REntityLegalFormTypeDto> updateEntityLegalFormTypeById(Integer id, REntityLegalFormTypeDto entityLegalFormTypeDto) {
        log.info("Inside EntityLegalFormTypeService: updateEntityLegalFormTypeById method");

        if (!id.equals(entityLegalFormTypeDto.getEntityLegalFormTypeId()))
            throw new BadRequestAlertException("Entity Legal Form Type ID mismatch.", ENTITY_NAME, "error");

        if (entityLegalFormTypeDto.getEntityLegalFormTypeCode() == null || entityLegalFormTypeDto.getEntityLegalFormTypeCode().isEmpty())
            throw new BadRequestAlertException("Entity Legal Form Type code is required.", ENTITY_NAME, "error");

        if (rEntityLegalFormTypeDao.existsByEntityLegalFormTypeCodeAndIsActiveAndEntityLegalFormTypeIdNot(entityLegalFormTypeDto.getEntityLegalFormTypeCode(), true, id))
            throw new BadRequestAlertException("Entity Legal Form Type code is duplicate with another record.", ENTITY_NAME, "error");

        Optional<REntityLegalFormType> resultOp = rEntityLegalFormTypeDao.findByEntityLegalFormTypeIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("Entity Legal Form Type not found by ID: " + id, ENTITY_NAME, "error");
        } else {
            REntityLegalFormType existingEntity = resultOp.get();
            entityLegalFormTypeDto.setIsActive(existingEntity.getIsActive());

            try {
                REntityLegalFormType updatedEntity = rEntityLegalFormTypeMapper.toEntity(entityLegalFormTypeDto);
                updatedEntity.setEntityLegalFormTypeId(existingEntity.getEntityLegalFormTypeId());

                REntityLegalFormTypeDto responseDto = rEntityLegalFormTypeMapper.toDto(rEntityLegalFormTypeDao.save(updatedEntity));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while updating Entity Legal Form Type by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }


    @Override
    public ResponseEntity<REntityLegalFormTypeDto> deleteEntityLegalFormTypeById(Integer id) {
        log.info("Inside EntityLegalFormTypeService: deleteEntityLegalFormTypeById method");

        try {
            Optional<REntityLegalFormType> resultOp = rEntityLegalFormTypeDao.findByEntityLegalFormTypeIdAndIsActive(id, true);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Entity Legal Form Type not found by ID: " + id, "entityLegalFormType", "error");
            } else {
                REntityLegalFormType existingEntity = resultOp.get();
                existingEntity.setIsActive(false);
                REntityLegalFormTypeDto responseDto = rEntityLegalFormTypeMapper.toDto(existingEntity);
                responseDto = rEntityLegalFormTypeMapper.toDto(rEntityLegalFormTypeDao.save(rEntityLegalFormTypeMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting Entity Legal Form Type: {}", e.getMessage());
            throw new BadRequestAlertException("Entity Legal Form Type cannot be deleted by ID: " + id, "entityLegalFormType", "error");
        }
    }

}
