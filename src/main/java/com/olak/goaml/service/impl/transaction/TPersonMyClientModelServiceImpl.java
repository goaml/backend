package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TPersonMyClientDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RCountryCodesDto;
import com.olak.goaml.dto.reference.RGenderTypeDto;
import com.olak.goaml.dto.xml.transaction.TPersonMyClientDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TPersonMyClientMapper;
import com.olak.goaml.models.xml.transaction.TPersonMyClient;
import com.olak.goaml.service.reference.GenderTypeService;
import com.olak.goaml.service.reference.RCountryCodesService;
import com.olak.goaml.service.transaction.TPersonMyClientModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TPersonMyClientModelServiceImpl implements TPersonMyClientModelService {

    private final TPersonMyClientDao tPersonMyClientDao;
    private final TPersonMyClientMapper tPersonMyClientMapper;
    private final FilterUtility<TPersonMyClient> filterUtility;
    private final GenderTypeService genderTypeService;
    private final RCountryCodesService countryCodesService;

    public TPersonMyClientModelServiceImpl(TPersonMyClientDao tPersonMyClientDao, TPersonMyClientMapper tPersonMyClientMapper, GenderTypeService genderTypeService, RCountryCodesService countryCodesService) {
        this.tPersonMyClientDao = tPersonMyClientDao;
        this.filterUtility = new FilterUtility<>(tPersonMyClientDao);
        this.tPersonMyClientMapper = tPersonMyClientMapper;
        this.genderTypeService = genderTypeService;
        this.countryCodesService = countryCodesService;
    }

    @Override
    public TPersonMyClient getPersonMyClientById(Integer id, Integer clientType) {
        log.info("Inside TPersonMyClientModelService: getPersonMyClientById method");
        try {
            Optional<TPersonMyClient> resultOp = tPersonMyClientDao.findByPersonMyClientIdAndRClientType_ClientTypeIdAndIsActive(id, clientType, true);
            TPersonMyClient response = resultOp.orElse(null);
            return response;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getPersonMyClientById: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public List<TPersonMyClient> getDirectorListByPersonId(Integer id) {
        log.info("Inside TPersonMyClientModelService: getDirectorListByPersonId method");
        try {
            List<TPersonMyClient> personMyClientList = new ArrayList<>();
            Optional<TPersonMyClient> resultOp = tPersonMyClientDao.findByPersonMyClientIdAndIsActive(id, true);
            if (resultOp.isPresent()) {
                TPersonMyClient personMyClient = resultOp.get();
                personMyClientList.add(personMyClient);
            }
            return personMyClientList;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getDirectorListByPersonId: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TPersonMyClientDto>>> getAllPersonMyClientDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer personMyClientId) {
        log.info("Inside TPersonMyClientModelServiceImpl: getAllPersonMyClientDetails method");
        try {

            List<FilterCriteria<TPersonMyClient>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("personMyClientId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (personMyClientId != null && personMyClientId != 0) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("personMyClientId"), personMyClientId)
                );
            }


            // pageable
            Page<TPersonMyClient> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TPersonMyClientDto> content = tPersonMyClientMapper.listToDto(pageableData.getContent());

            content.forEach(contentItem -> {
                if (contentItem.getGenderTypeId() != null){
                    ResponseEntity<RGenderTypeDto> genderType = genderTypeService.getGenderTypeById(contentItem.getGenderTypeId());
                    contentItem.setRGenderType(genderType.getBody());
                }
                if (contentItem.getPassportCountryId() != null) {
                    ResponseEntity<RCountryCodesDto> RCountryCodes = countryCodesService.getCountryCodeById(contentItem.getPassportCountryId());
                    contentItem.setPassportCountry(RCountryCodes.getBody());
                }
                if (contentItem.getNationalityCountryId() != null) {
                    ResponseEntity<RCountryCodesDto> RCountryCodes = countryCodesService.getCountryCodeById(contentItem.getNationalityCountryId());
                    contentItem.setNationalityCountry(RCountryCodes.getBody());
                }
                if (contentItem.getNationalityCountry2Id() != null) {
                    ResponseEntity<RCountryCodesDto> RCountryCodes = countryCodesService.getCountryCodeById(contentItem.getNationalityCountry2Id());
                    contentItem.setNationalityCountry2(RCountryCodes.getBody());
                }
                if (contentItem.getNationalityCountry3Id() != null) {
                    ResponseEntity<RCountryCodesDto> RCountryCodes = countryCodesService.getCountryCodeById(contentItem.getNationalityCountry3Id());
                    contentItem.setNationalityCountry3(RCountryCodes.getBody());
                }
                if (contentItem.getResidenceCountryId() != null) {
                    ResponseEntity<RCountryCodesDto> RCountryCodes = countryCodesService.getCountryCodeById(contentItem.getResidenceCountryId());
                    contentItem.setResidenceCountry(RCountryCodes.getBody());
                }
            });

            // response
            ApiResponseDto<List<TPersonMyClientDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
