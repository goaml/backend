package com.olak.goaml.service.impl.reference;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.olak.goaml.dao.reference.RGenderTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RGenderTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RGenderTypeMapper;
import com.olak.goaml.models.reference.RGenderType;
import com.olak.goaml.service.reference.GenderTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class GenderTypeServiceImpl implements GenderTypeService {

    private final RStatusDao rStatusDao;
    private final RGenderTypeDao rGenderTypeDao;
    private final RGenderTypeMapper rGenderTypeMapper;
    private final SearchNFilter searchNFilter;

    public GenderTypeServiceImpl(RStatusDao rStatusDao, RGenderTypeDao rGenderTypeDao, RGenderTypeMapper rGenderTypeMapper, SearchNFilter searchNFilter) {
        this.rStatusDao = rStatusDao;
        this.rGenderTypeDao = rGenderTypeDao;
        this.rGenderTypeMapper = rGenderTypeMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RGenderTypeDto>> getAllGenderTypes() {
        log.info("Inside GenderTypeService: getAllGenderTypes method");
        List<RGenderType> resultList = rGenderTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rGenderTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RGenderTypeDto>>> getGenderTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside GenderTypeService: getGenderTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RGenderType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RGenderTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rGenderTypeMapper.listToDto((List<RGenderType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RGenderTypeDto> addGenderType(RGenderTypeDto genderTypeDto) {
        log.info("Inside GenderTypeService: addGenderType method");

        try {
            if (genderTypeDto.getGenderTypeCode() == null || genderTypeDto.getGenderTypeCode().isEmpty()) {
                throw new BadRequestAlertException("GenderType code is required.", ENTITY_NAME, "error");
            }

            Optional<RGenderType> existingEntity = rGenderTypeDao.findByGenderTypeCodeAndIsActive(genderTypeDto.getGenderTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("GenderType already exists with code: " + genderTypeDto.getGenderTypeCode(), ENTITY_NAME, "idexists");
            }

            if (genderTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(genderTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "idinvalid");
            }

            genderTypeDto.setIsActive(true);
            genderTypeDto.setStatusId(1);

            RGenderType entity = rGenderTypeMapper.toEntity(genderTypeDto);
            entity.setRStatus(rStatusDao.findById(genderTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "idinvalid")));
            entity = rGenderTypeDao.save(entity);

            return new ResponseEntity<>(rGenderTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding GenderType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "GenderType", "error");
        }
    }

    @Override
    public ResponseEntity<RGenderTypeDto> getGenderTypeById(Integer id) {
        log.info("Inside GenderTypeService: getGenderTypeById method");

        try {
            Optional<RGenderType> entity = rGenderTypeDao.findByGenderTypeIdAndIsActive(id, true);
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("GenderType not found by ID: " + id, ENTITY_NAME, "idinvalid");
            }

            RGenderTypeDto responseDto = rGenderTypeMapper.toDto(entity.get());
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting GenderType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RGenderTypeDto> updateGenderTypeById(Integer id, RGenderTypeDto genderTypeDto) {
        log.info("Inside GenderTypeService: updateGenderTypeById method");

        try {
            Optional<RGenderType> existingEntity = rGenderTypeDao.findByGenderTypeIdAndIsActive(id, true);
            if (!existingEntity.isPresent()) {
                throw new BadRequestAlertException("Invalid gender type ID", ENTITY_NAME, "idinvalid");
            }

            if (!id.equals(genderTypeDto.getGenderTypeId())) {
                throw new BadRequestAlertException("GenderType id mismatch.", ENTITY_NAME, "error");
            }

            if (genderTypeDto.getGenderTypeCode() == null || genderTypeDto.getGenderTypeCode().isEmpty()) {
                throw new BadRequestAlertException("GenderType Code is required.", ENTITY_NAME, "error");
            }

            if (rGenderTypeDao.existsByGenderTypeCodeAndIsActiveAndGenderTypeIdNot(genderTypeDto.getGenderTypeCode(), true, id)) {
                throw new BadRequestAlertException("GenderType Code is duplicate with another record.", ENTITY_NAME, "error");
            }

            if (genderTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(genderTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
            }

            RGenderType entity = existingEntity.get();
            entity.setGenderTypeCode(genderTypeDto.getGenderTypeCode());
            entity.setDescription(genderTypeDto.getDescription());
            entity.setRStatus(rStatusDao.findById(genderTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "idinvalid")));

            return new ResponseEntity<>(rGenderTypeMapper.toDto(rGenderTypeDao.save(entity)), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while updating GenderType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RGenderTypeDto> deleteGenderTypeById(Integer id) {
        log.info("Inside GenderTypeService: deleteGenderTypeById method");

        try {
            Optional<RGenderType> resultOp = rGenderTypeDao.findByGenderTypeIdAndIsActive(id, true);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("GenderType not found by ID: " + id, ENTITY_NAME, "idinvalid");
            } else {
                RGenderType existingEntity = resultOp.get();

                existingEntity.setIsActive(false);
                RGenderTypeDto responseDto = rGenderTypeMapper.toDto(rGenderTypeDao.save(existingEntity));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting GenderType: {}", e.getMessage());
            throw new BadRequestAlertException("GenderType could not be deleted by ID: " + id, ENTITY_NAME, "error");
        }
    }

}
