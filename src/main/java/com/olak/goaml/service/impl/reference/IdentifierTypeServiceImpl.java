package com.olak.goaml.service.impl.reference;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dao.reference.RIdentifierTypeDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RIdentifierTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RIdentifierTypeMapper;
import com.olak.goaml.models.reference.RIdentifierType;
import com.olak.goaml.service.reference.IdentifierTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;

import lombok.extern.slf4j.Slf4j;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class IdentifierTypeServiceImpl implements IdentifierTypeService {

    private final RStatusDao rStatusDao;
    private final RIdentifierTypeDao rIdentifierTypeDao;
    private final RIdentifierTypeMapper rIdentifierTypeMapper;
    private final SearchNFilter searchNFilter;

    public IdentifierTypeServiceImpl(RStatusDao rStatusDao, RIdentifierTypeDao rIdentifierTypeDao, RIdentifierTypeMapper rIdentifierTypeMapper, SearchNFilter searchNFilter){
        this.rStatusDao = rStatusDao;
        this.rIdentifierTypeDao = rIdentifierTypeDao;
        this.rIdentifierTypeMapper = rIdentifierTypeMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RIdentifierTypeDto>> getAllIdentifierTypes() {
        log.info("Inside IdentifierTypeService: getAllIdentifierTypes method");
        List<RIdentifierType> resultList = rIdentifierTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rIdentifierTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RIdentifierTypeDto>>> getIdentifierTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside IdentifierTypeService: getIdentifierTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RIdentifierType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RIdentifierTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rIdentifierTypeMapper.listToDto((List<RIdentifierType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RIdentifierTypeDto> addIdentifierType(RIdentifierTypeDto identifierTypeDto) {
        log.info("Inside IdentifierTypeService: addIdentifierType method");

        try {
            if (identifierTypeDto.getIdentifierTypeCode() == null || identifierTypeDto.getIdentifierTypeCode().isEmpty()) {
                throw new BadRequestAlertException("IdentifierType code is required.", "IdentifierType", "error");
            }

            Optional<RIdentifierType> existingEntity = rIdentifierTypeDao.findByIdentifierTypeCodeAndIsActive(identifierTypeDto.getIdentifierTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("IdentifierType already exists with code: " + identifierTypeDto.getIdentifierTypeCode(), "IdentifierType", "error");
            }

            if (identifierTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", "IdentifierType", "error");
            }

            if (!rStatusDao.existsById(identifierTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", "IdentifierType", "error");
            }

            identifierTypeDto.setIsActive(true);
            identifierTypeDto.setStatusId(1);

            RIdentifierType entity = rIdentifierTypeMapper.toEntity(identifierTypeDto);
            entity.setRStatus(rStatusDao.findById(identifierTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", "IdentifierType", "invalidStatusId")));
            entity = rIdentifierTypeDao.save(entity);

            return new ResponseEntity<>(rIdentifierTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding IdentifierType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "IdentifierType", "error");
        }
    }


    @Override
    public ResponseEntity<RIdentifierTypeDto> getIdentifierTypeById(Integer id) {
        log.info("Inside IdentifierTypeService: getIdentifierTypeById method");

        try {
            Optional<RIdentifierType> entity = rIdentifierTypeDao.findByIdentifierTypeIdAndIsActive(id, true);
            RIdentifierTypeDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("IdentifierType not found by ID: " + id, "IdentifierType", "error");
            } else {
                responseDto = rIdentifierTypeMapper.toDto(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting IdentifierType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "IdentifierType", "error");
        }
    }


    @Override
    public ResponseEntity<RIdentifierTypeDto> updateIdentifierTypeById(Integer id, RIdentifierTypeDto identifierTypeDto) {
        log.info("Inside IdentifierTypeService: updateIdentifierTypeById method");


        if (!id.equals(identifierTypeDto.getIdentifierTypeId())) {
            throw new BadRequestAlertException("IdentifierType id mismatch.", "IdentifierType", "error");
        }


        if (identifierTypeDto.getIdentifierTypeCode() == null || identifierTypeDto.getIdentifierTypeCode().isEmpty()) {
            throw new BadRequestAlertException("IdentifierType Code is required.", "IdentifierType", "error");
        }


        if (rIdentifierTypeDao.existsByIdentifierTypeCodeAndIsActiveAndIdentifierTypeIdNot(identifierTypeDto.getIdentifierTypeCode(), true, id)) {
            throw new BadRequestAlertException("IdentifierType Code is duplicate with another record.", "IdentifierType", "error");
        }


        if (identifierTypeDto.getStatusId() == null) {
            throw new BadRequestAlertException("Status ID is required.", "IdentifierType", "error");
        }

        if (!rStatusDao.existsById(identifierTypeDto.getStatusId())) {
            throw new BadRequestAlertException("Invalid Status ID.", "IdentifierType", "error");
        }


        Optional<RIdentifierType> existingEntity = rIdentifierTypeDao.findByIdentifierTypeIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("IdentifierType not found by ID: " + id, "IdentifierType", "error");
        } else {

            RIdentifierType entity = existingEntity.get();
            entity.setIdentifierTypeCode(identifierTypeDto.getIdentifierTypeCode());
            entity.setDescription(identifierTypeDto.getDescription());
            entity.setIsActive(identifierTypeDto.getIsActive());
            entity.setRStatus(rStatusDao.findById(identifierTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid Status ID.", "IdentifierType", "error")));

            try {

                entity = rIdentifierTypeDao.save(entity);

                return new ResponseEntity<>(rIdentifierTypeMapper.toDto(entity), HttpStatus.OK);
            } catch (Exception e) {

                e.printStackTrace();
                log.error("Error while updating IdentifierType: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), "IdentifierType", "error");
            }
        }
    }


    @Override
    public ResponseEntity<RIdentifierTypeDto> deleteIdentifierTypeById(Integer id) {
        log.info("Inside IdentifierTypeService: deleteIdentifierTypeById method");

        try {

            Optional<RIdentifierType> existingEntity = rIdentifierTypeDao.findByIdentifierTypeIdAndIsActive(id, true);
            if (!existingEntity.isPresent()) {
                throw new BadRequestAlertException("Invalid identifier type ID", ENTITY_NAME, "idinvalid");
            }


            RIdentifierType entity = existingEntity.get();
            entity.setIsActive(false);
            entity = rIdentifierTypeDao.save(entity);


            return new ResponseEntity<>(rIdentifierTypeMapper.toDto(entity), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting IdentifierType: {}", e.getMessage());
            throw new BadRequestAlertException("IdentifierType could not be deleted", ENTITY_NAME, "deleteerror");
        }
    }

}
