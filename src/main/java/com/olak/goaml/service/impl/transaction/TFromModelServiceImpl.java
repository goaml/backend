package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TFromDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.xml.transaction.TFromDto;
import com.olak.goaml.dto.xml.transaction.TFromMyClientDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TFromMapper;
import com.olak.goaml.models.xml.transaction.TFrom;
import com.olak.goaml.models.xml.transaction.TFromMyClient;
import com.olak.goaml.service.transaction.TFromModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TFromModelServiceImpl implements TFromModelService {

    private final FilterUtility<TFrom> filterUtility;
    private final TFromDao tFromDao;
    private final TFromMapper tFromMapper;
    public TFromModelServiceImpl(TFromDao tFromDao,TFromMapper tFromMapper) {
        this.filterUtility = new FilterUtility<>(tFromDao);
        this.tFromDao = tFromDao;
        this.tFromMapper = tFromMapper;
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TFromDto>>> getAllFromDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer trxId) {
        log.info("Inside TFromModelServiceImpl: getAllFromDetails method");
        try {

            List<FilterCriteria<TFrom>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("fromId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (trxId != null && trxId != 0) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("TTransaction").get("transactionId"), trxId)
                );
            }

            // pageable
            Page<TFrom> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TFromDto> content = tFromMapper.listToDto(pageableData.getContent());

            // response
            ApiResponseDto<List<TFromDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
