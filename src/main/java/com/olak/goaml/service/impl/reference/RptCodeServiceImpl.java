package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.master.GoAmlTrxCodeDao;
import com.olak.goaml.dao.master.RptCodeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.master.RptCodeDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.parameter.RptCodeMapper;
import com.olak.goaml.models.master.RptCode;
import com.olak.goaml.service.reference.RptCodeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class RptCodeServiceImpl implements RptCodeService {

    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final RptCodeDao rptCodeDao;
    private final RptCodeMapper rptCodeMapper;
    private final GoAmlTrxCodeDao goAmlTrxCodeDao;

    public RptCodeServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, RptCodeDao rptCodeDao, RptCodeMapper rptCodeMapper, GoAmlTrxCodeDao goAmlTrxCodeDao) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.rptCodeDao = rptCodeDao;
        this.rptCodeMapper = rptCodeMapper;
        this.goAmlTrxCodeDao = goAmlTrxCodeDao;
    }


    @Override
    public ResponseEntity<List<RptCodeDto>> getAllRptCode() {
        log.info("Inside RptCodeService: getAllRptCode method");
        List<RptCode> resultList = rptCodeDao.findByIsActive(true);
        return new ResponseEntity<>(rptCodeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RptCodeDto>>> getRptCodeWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside NatureTypeService: getNatureTypeWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RptCode.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RptCodeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rptCodeMapper.listToDto((List<RptCode>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RptCodeDto> addRptCode(RptCodeDto rptCodeDto) {
        log.info("Inside RptCodeService: addRptCode method");

        try {
            if (rptCodeDto.getRptCodeName() == null || rptCodeDto.getRptCodeName().isEmpty())
                throw new BadRequestAlertException("RptCode name is required: ", ENTITY_NAME, "error");

            Optional<RptCode> resultOp = rptCodeDao.findByRptCodeNameAndIsActive(rptCodeDto.getRptCodeName(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("RptCode already exists by RptCode name: " + rptCodeDto.getRptCodeName(), ENTITY_NAME, "error");

            if (rptCodeDto.getRptCodeDescription() == null || rptCodeDto.getRptCodeDescription().isEmpty())
                throw new BadRequestAlertException("RptCode Description is required: ", ENTITY_NAME, "error");

            Optional<RptCode> resultOp1 = rptCodeDao.findByRptCodeDescriptionAndIsActive(rptCodeDto.getRptCodeDescription(), true);
            if (resultOp1.isPresent())
                throw new BadRequestAlertException("RptCode already exists by RptCode Description: " + rptCodeDto.getRptCodeDescription(), ENTITY_NAME, "error");

            if (rptCodeDto.getGoamlTrxCodeId() == null)
                throw new BadRequestAlertException("GoAmlTrxCode id is required: ", ENTITY_NAME, "error");

            if (!goAmlTrxCodeDao.existsById(rptCodeDto.getGoamlTrxCodeId()))
                throw new BadRequestAlertException("GoAmlTrxCode id is invalid ", ENTITY_NAME, "error");


//            if (rptCodeDto.getStatusId() == null)
            rptCodeDto.setStatusId(1);

            rptCodeDto.setIsActive(true);

//            if (!rStatusDao.existsById(rptCodeDto.getStatusId()))
//                throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

            RptCodeDto saveDtoRes = rptCodeMapper.toDto(rptCodeDao.save(rptCodeMapper.toEntity(rptCodeDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add RptCode: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RptCodeDto> getRptCodeById(Long id) {
        log.info("Inside RptCodeService: getRptCodeById method");

        try {
            Optional<RptCode> resultOp = rptCodeDao.findByRptCodeIdAndIsActive(id, true);
            RptCodeDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("RptCode not found by RptCode ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = rptCodeMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get RptCode details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RptCodeDto> updateRptCodeById(Long id, RptCodeDto rptCodeDto) {
        log.info("Inside RptCodeService: updateRptCodeById method");

        // Validate
        if (id != rptCodeDto.getRptCodeId())
            throw new BadRequestAlertException("RptCode id mismatch.", ENTITY_NAME, "error");

        if (rptCodeDto.getRptCodeName() == null || rptCodeDto.getRptCodeName().isEmpty())
            throw new BadRequestAlertException("RptCode Name is required: ", ENTITY_NAME, "error");

        if (rptCodeDao.existsByRptCodeNameAndIsActiveAndRptCodeIdNot(rptCodeDto.getRptCodeName(), true, id))
            throw new BadRequestAlertException("RptCode Name is duplicate with another recode: ", ENTITY_NAME, "error");

        if (rptCodeDto.getGoamlTrxCodeId() == null)
            throw new BadRequestAlertException("GoAmlTrxCode id is required: ", ENTITY_NAME, "error");

        if (!goAmlTrxCodeDao.existsById(rptCodeDto.getGoamlTrxCodeId()))
            throw new BadRequestAlertException("GoAmlTrxCode id is invalid ", ENTITY_NAME, "error");

//        if (rptCodeDto.getStatusId() == null)
//            throw new BadRequestAlertException("Status Can Not Be Empty: ", ENTITY_NAME, "error");
//
//        if (!rStatusDao.existsById(rptCodeDto.getStatusId()))
//            throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

        Optional<RptCode> resultOp = rptCodeDao.findByRptCodeIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("RptCode not found by RptCode ID: " + id, ENTITY_NAME, "error");
        } else {
            RptCode existingRes = resultOp.get();
            rptCodeDto.setIsActive(existingRes.getIsActive());
            rptCodeDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                RptCodeDto responseDto = rptCodeMapper.toDto(rptCodeDao.save(rptCodeMapper.toEntity(rptCodeDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update RptCode by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<RptCodeDto> deleteRptCodeById(Long id) {
        log.info("Inside RptCodeService: deleteRptCodeById method");

        try {
            Optional<RptCode> resultOp = rptCodeDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("RptCode not found by RptCode ID: " + id, ENTITY_NAME, "error");
            } else {
                RptCode existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                RptCodeDto responseDto = rptCodeMapper.toDto(existingRes);
                responseDto = rptCodeMapper.toDto(rptCodeDao.save(rptCodeMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete RptCode: {}", e.getMessage());
            throw new BadRequestAlertException("RptCode con not delete by RptCode ID: " + id, ENTITY_NAME, "error");
        }
    }
}
