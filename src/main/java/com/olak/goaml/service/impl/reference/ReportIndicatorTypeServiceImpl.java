package com.olak.goaml.service.impl.reference;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.olak.goaml.dao.reference.RReportIndicatorTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RReportIndicatorTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RReportIndicatorTypeMapper;
import com.olak.goaml.models.reference.RReportIndicatorType;
import com.olak.goaml.service.reference.ReportIndicatorTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ReportIndicatorTypeServiceImpl implements ReportIndicatorTypeService {

    private final RStatusDao rStatusDao;
    private final RReportIndicatorTypeDao rReportIndicatorTypeDao;
    private final RReportIndicatorTypeMapper rReportIndicatorTypeMapper;
    private final SearchNFilter searchNFilter;

    public ReportIndicatorTypeServiceImpl(RStatusDao rStatusDao, RReportIndicatorTypeDao rReportIndicatorTypeDao,
                                          RReportIndicatorTypeMapper rReportIndicatorTypeMapper, SearchNFilter searchNFilter) {
        this.rStatusDao = rStatusDao;
        this.rReportIndicatorTypeDao = rReportIndicatorTypeDao;
        this.rReportIndicatorTypeMapper = rReportIndicatorTypeMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RReportIndicatorTypeDto>> getAllReportIndicatorTypes() {
        log.info("Inside ReportIndicatorTypeService: getAllReportIndicatorTypes method");
        List<RReportIndicatorType> resultList = rReportIndicatorTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rReportIndicatorTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RReportIndicatorTypeDto>>> getReportIndicatorTypesWithPagination(
            Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside ReportIndicatorTypeService: getReportIndicatorTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RReportIndicatorType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RReportIndicatorTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rReportIndicatorTypeMapper.listToDto((List<RReportIndicatorType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RReportIndicatorTypeDto> addReportIndicatorType(RReportIndicatorTypeDto reportIndicatorTypeDto) {
        log.info("Inside ReportIndicatorTypeService: addReportIndicatorType method");

        try {
            if (reportIndicatorTypeDto.getReportIndicatorTypeCode() == null || reportIndicatorTypeDto.getReportIndicatorTypeCode().isEmpty()) {
                throw new BadRequestAlertException("IndicatorType code is required.", ENTITY_NAME, "error");
            }

            Optional<RReportIndicatorType> existingEntity = rReportIndicatorTypeDao.findByReportIndicatorTypeCodeAndIsActive(reportIndicatorTypeDto.getReportIndicatorTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("Report IndicatorType already exists with code: " + reportIndicatorTypeDto.getReportIndicatorTypeCode(), ENTITY_NAME, "idexists");
            }

            if (reportIndicatorTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(reportIndicatorTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "idinvalid");
            }

            reportIndicatorTypeDto.setIsActive(true);
            reportIndicatorTypeDto.setStatusId(1);

            RReportIndicatorType entity = rReportIndicatorTypeMapper.toEntity(reportIndicatorTypeDto);
            entity.setRStatus(rStatusDao.findById(reportIndicatorTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "idinvalid")));
            entity = rReportIndicatorTypeDao.save(entity);

            return new ResponseEntity<>(rReportIndicatorTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding ReportIndicatorType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RReportIndicatorTypeDto> getReportIndicatorTypeById(Integer id) {
        log.info("Inside ReportIndicatorTypeService: getReportIndicatorTypeById method");

        try {
            Optional<RReportIndicatorType> entity = rReportIndicatorTypeDao.findByReportIndicatorTypeIdAndIsActive(id, true);
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("Report IndicatorType not found by ID: " + id, ENTITY_NAME, "idinvalid");
            } else {
                RReportIndicatorTypeDto responseDto = rReportIndicatorTypeMapper.toDto(entity.get());
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting ReportIndicatorType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RReportIndicatorTypeDto> updateReportIndicatorTypeById(Integer id, RReportIndicatorTypeDto reportIndicatorTypeDto) {
        log.info("Inside ReportIndicatorTypeService: updateReportIndicatorTypeById method");

        try {
            Optional<RReportIndicatorType> existingEntity = rReportIndicatorTypeDao.findByReportIndicatorTypeIdAndIsActive(id, true);
            if (!existingEntity.isPresent()) {
                throw new BadRequestAlertException("Invalid report indicator type ID", ENTITY_NAME, "idinvalid");
            }

            if (!id.equals(reportIndicatorTypeDto.getReportIndicatorTypeId())) {
                throw new BadRequestAlertException("ReportIndicatorType id mismatch.", ENTITY_NAME, "error");
            }

            if (reportIndicatorTypeDto.getReportIndicatorTypeCode() == null || reportIndicatorTypeDto.getReportIndicatorTypeCode().isEmpty()) {
                throw new BadRequestAlertException("ReportIndicatorType Code is required.", ENTITY_NAME, "error");
            }

            if (rReportIndicatorTypeDao.existsByReportIndicatorTypeCodeAndIsActiveAndReportIndicatorTypeIdNot(reportIndicatorTypeDto.getReportIndicatorTypeCode(), true, id)) {
                throw new BadRequestAlertException("ReportIndicatorType Code is duplicate with another record.", ENTITY_NAME, "error");
            }

            if (reportIndicatorTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(reportIndicatorTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
            }

            RReportIndicatorType entity = existingEntity.get();
            entity.setReportIndicatorTypeCode(reportIndicatorTypeDto.getReportIndicatorTypeCode());
            entity.setDescription(reportIndicatorTypeDto.getDescription());
            entity.setIsActive(reportIndicatorTypeDto.getIsActive());
            entity.setRStatus(rStatusDao.findById(reportIndicatorTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID.", ENTITY_NAME, "error")));

            try {
                entity = rReportIndicatorTypeDao.save(entity);
                return new ResponseEntity<>(rReportIndicatorTypeMapper.toDto(entity), HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while updating ReportIndicatorType: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while updating ReportIndicatorType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RReportIndicatorTypeDto> deleteReportIndicatorTypeById(Integer id) {
        log.info("Inside ReportIndicatorTypeService: deleteReportIndicatorTypeById method");

        try {
            Optional<RReportIndicatorType> resultOp = rReportIndicatorTypeDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("ReportIndicatorType not found by ID: " + id, ENTITY_NAME, "error");
            } else {
                RReportIndicatorType existingEntity = resultOp.get();

                existingEntity.setIsActive(false);
                RReportIndicatorTypeDto responseDto = rReportIndicatorTypeMapper.toDto(rReportIndicatorTypeDao.save(existingEntity));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting ReportIndicatorType: {}", e.getMessage());
            throw new BadRequestAlertException("ReportIndicatorType could not be deleted by ID: " + id, ENTITY_NAME, "error");
        }
    }
}
