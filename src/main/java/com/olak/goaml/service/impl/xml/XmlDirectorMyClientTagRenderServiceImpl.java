package com.olak.goaml.service.impl.xml;

import com.olak.goaml.dao.reference.RCountryCodesDao;
import com.olak.goaml.dao.reference.RGenderTypeDao;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.reference.RCountryCodes;
import com.olak.goaml.models.reference.RGenderType;
import com.olak.goaml.models.xml.transaction.TPersonMyClient;
import com.olak.goaml.service.transaction.TPersonMyClientModelService;
import com.olak.goaml.service.xml.XMLAddressTagRenderService;
import com.olak.goaml.service.xml.XMLPhoneTagRenderService;
import com.olak.goaml.service.xml.XmlDirectorMyClientTagRenderService;
import com.olak.goaml.service.xml.XmlPIdentificationTagRenderService;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlDirectorMyClientTagRenderServiceImpl implements XmlDirectorMyClientTagRenderService {

    private final XMLAddressTagRenderService xmlAddressTagRenderService;
    private final XMLPhoneTagRenderService xmlPhoneTagRenderService;
    private final XmlPIdentificationTagRenderService xmlPIdentificationTagRenderService;
    private final TPersonMyClientModelService tPersonMyClientModelService;
    private final RGenderTypeDao rGenderTypeDao;
    private final RCountryCodesDao rCountryCodesDao;

    public XmlDirectorMyClientTagRenderServiceImpl(XMLAddressTagRenderService xmlAddressTagRenderService, XMLPhoneTagRenderService xmlPhoneTagRenderService, XmlPIdentificationTagRenderService xmlPIdentificationTagRenderService, TPersonMyClientModelService tPersonMyClientModelService, RGenderTypeDao rGenderTypeDao, RCountryCodesDao rCountryCodesDao) {
        this.xmlAddressTagRenderService = xmlAddressTagRenderService;
        this.xmlPhoneTagRenderService = xmlPhoneTagRenderService;
        this.xmlPIdentificationTagRenderService = xmlPIdentificationTagRenderService;
        this.tPersonMyClientModelService = tPersonMyClientModelService;
        this.rGenderTypeDao = rGenderTypeDao;
        this.rCountryCodesDao = rCountryCodesDao;
    }

    @Override
    public List<xmlEntityMyClientDirectorIdDto> getRenderedDirectorList(Integer id, String scenarioName) {


        try{
            List<xmlEntityMyClientDirectorIdDto> xmlEntityMyClientDirectorIdDtoList = new ArrayList<>();

            List<TPersonMyClient> personMyClientDtoList = tPersonMyClientModelService.getDirectorListByPersonId(id);

            if (personMyClientDtoList != null && !personMyClientDtoList.isEmpty()) {
                for (TPersonMyClient personMyClientDto : personMyClientDtoList) {

                    xmlEntityMyClientDirectorIdDto xmlPersonMyClientDto = new xmlEntityMyClientDirectorIdDto();

                    if (personMyClientDto.getGenderTypeId() != null) {
                        Optional<RGenderType> rGenderTypeOp = rGenderTypeDao.findByGenderTypeIdAndIsActive(personMyClientDto.getGenderTypeId(), true);
                        rGenderTypeOp.ifPresent(rGenderType -> xmlPersonMyClientDto.setGender(rGenderType.getGenderTypeCode()));
                    }

                    xmlPersonMyClientDto.setTitle(personMyClientDto.getTitle());
                    xmlPersonMyClientDto.setFirstName(personMyClientDto.getFirstName());
//                    xmlPersonMyClientDto.setMiddleName(personMyClientDto.getMiddleName());
//                    xmlPersonMyClientDto.setPrefix(personMyClientDto.getPrefix());
                    xmlPersonMyClientDto.setLastName(personMyClientDto.getLastName());

                    if (personMyClientDto.getBirthdate() != null) {
                        Date birthDate = personMyClientDto.getBirthdate();
                        xmlPersonMyClientDto.setBirthdate(XmlUtils.convertDateToLocalDate(birthDate).atTime(0, 0, 0));
                    }

//                    xmlPersonMyClientDto.setBirthPlace(personMyClientDto.getBirthPlace());
//                    xmlPersonMyClientDto.setMothersName(personMyClientDto.getMothersName());
//                    xmlPersonMyClientDto.setAlias(personMyClientDto.getAlias());
                    xmlPersonMyClientDto.setSsn(personMyClientDto.getSsn());
                    xmlPersonMyClientDto.setPassportNumber(personMyClientDto.getPassportNumber());

                    setCountryCode(personMyClientDto.getPassportCountryId(), xmlPersonMyClientDto::setPassportCountry);
//            xmlPersonMyClientDto.setPassportCountry(personMyClientDto.getRCountryCodes1().getCountryCodeCode());

                    xmlPersonMyClientDto.setIdNumber(personMyClientDto.getIdNumber());


                    setCountryCode(personMyClientDto.getNationalityCountryId(), xmlPersonMyClientDto::setNationality1);
                    setCountryCode(personMyClientDto.getNationalityCountry2Id(), xmlPersonMyClientDto::setNationality2);
                    setCountryCode(personMyClientDto.getNationalityCountry3Id(), xmlPersonMyClientDto::setNationality3);
                    setCountryCode(personMyClientDto.getResidenceCountryId(), xmlPersonMyClientDto::setResidence);

//            xmlPersonMyClientDto.setNationality1(personMyClientDto.getRCountryCodes2().getCountryCodeCode());
//            xmlPersonMyClientDto.setNationality2(personMyClientDto.getRCountryCodes3().getCountryCodeCode());
//            xmlPersonMyClientDto.setNationality3(personMyClientDto.getRCountryCodes4().getCountryCodeCode());
//            xmlPersonMyClientDto.setResidence(personMyClientDto.getRCountryCodes5().getCountryCodeCode());

                    if (EnumTagVerifier.isTagValid("PERSON_MY_CLIENT_PHONES_PHONE", scenarioName)) {
                        if (personMyClientDto.getPhoneId() != null)
                            xmlPersonMyClientDto.setPhones(xmlPhoneTagRenderService.getRenderedPhoneList(personMyClientDto.getPhoneId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("PERSON_MY_CLIENT_ADDRESSES_ADDRESS", scenarioName)) {
                        if (personMyClientDto.getAddressId() != null)
                            xmlPersonMyClientDto.setAddresses(xmlAddressTagRenderService.getRenderedAddressList(personMyClientDto.getAddressId(), scenarioName));
                    }

                    List<String> emailList = new ArrayList<>();
                    emailList.add(personMyClientDto.getEmail());
                    xmlPersonMyClientDto.setEmail(emailList);

                    xmlPersonMyClientDto.setOccupation(personMyClientDto.getOccupation());
//                    xmlPersonMyClientDto.setEmployerName(personMyClientDto.getEmployerName());
//
//                    if (EnumTagVerifier.isTagValid("PERSON_MY_CLIENT_EMPLOYER_ADDRESS_ID", scenarioName)) {
//                        if (personMyClientDto.getEmployerAddressId() != null)
//                            xmlPersonMyClientDto.setEmployerAddressId(xmlAddressTagRenderService.getRenderedAddress(personMyClientDto.getEmployerAddressId(), scenarioName));
//                    }
//
//                    if (EnumTagVerifier.isTagValid("PERSON_MY_CLIENT_EMPLOYER_PHONE_ID", scenarioName)) {
//                        if (personMyClientDto.getEmployerPhoneId() != null)
//                            xmlPersonMyClientDto.setEmployerPhoneId(xmlPhoneTagRenderService.getRenderedPhone(personMyClientDto.getEmployerPhoneId(), scenarioName));
//                    }
//
//                    if (EnumTagVerifier.isTagValid("PERSON_MY_CLIENT_IDENTIFICATION", scenarioName)) {
//                        if (personMyClientDto.getPersonIdentificationId() != null)
//                            xmlPersonMyClientDto.setIdentification(xmlPIdentificationTagRenderService.getRenderedIdentificationList(personMyClientDto.getPersonIdentificationId(), scenarioName));
//                    }
//
//
//                    xmlPersonMyClientDto.setDeceased(false);
//
//                    if (xmlPersonMyClientDto.getDeceased()) {
//                        if (personMyClientDto.getDeceasedDate() != null) {
//                            Date deceasedDate = personMyClientDto.getDeceasedDate();
//                            xmlPersonMyClientDto.setDateDeceased(XmlUtils.convertDateToLocalDate(deceasedDate).atTime(0, 0, 0));
//                        }
//                    }
//
//                    xmlPersonMyClientDto.setTaxNumber(personMyClientDto.getTaxNumber());
//                    xmlPersonMyClientDto.setTaxRegNumber(personMyClientDto.getTaxRegNumber());
//                    xmlPersonMyClientDto.setSourceOfWealth(personMyClientDto.getSourceOfWealth());
                    xmlPersonMyClientDto.setComments(personMyClientDto.getComments());

                    xmlPersonMyClientDto.setRole("DRCT");

                    xmlEntityMyClientDirectorIdDtoList.add(xmlPersonMyClientDto);
                }
            }

            if (xmlEntityMyClientDirectorIdDtoList.isEmpty()) {
                xmlEntityMyClientDirectorIdDtoList = null;
            }


            return xmlEntityMyClientDirectorIdDtoList;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedDirectorList: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    private void setCountryCode(Integer countryId, Consumer<String> setter) {
        if (countryId != null) {
            Optional<RCountryCodes> countryOp = rCountryCodesDao.findByCountryCodeIdAndIsActive(countryId, true);
            countryOp.ifPresent(RCountryCode -> setter.accept(RCountryCode.getCountryCodeCode()));
        }
    }
}
