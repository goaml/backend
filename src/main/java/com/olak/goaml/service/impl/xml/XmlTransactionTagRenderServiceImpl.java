package com.olak.goaml.service.impl.xml;

import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TTransactionMapper;
import com.olak.goaml.models.xml.transaction.TTransaction;
import com.olak.goaml.service.xml.*;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.xmlDto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlTransactionTagRenderServiceImpl implements XmlTransactionTagRenderService {

    private final XmlFromMyClientTagRenderService xmlFromMyClientTagRenderService;
    private final XmlFromTagRenderService xmlFromTagRenderService;
    private final XmlToMyClientTagRenderService xmlToMyClientTagRenderService;
    private final XmlToTagRenderService xmlToTagRenderService;
    private final XmlPartyTagRenderService xmlPartyTagRenderService;
    private final TTransactionMapper tTransactionMapper;

    public XmlTransactionTagRenderServiceImpl(XmlFromMyClientTagRenderService xmlFromMyClientTagRenderService, XmlFromTagRenderService xmlFromTagRenderService, XmlToMyClientTagRenderService xmlToMyClientTagRenderService, XmlToTagRenderService xmlToTagRenderService, XmlPartyTagRenderService xmlPartyTagRenderService, TTransactionMapper tTransactionMapper) {
        this.xmlFromMyClientTagRenderService = xmlFromMyClientTagRenderService;
        this.xmlFromTagRenderService = xmlFromTagRenderService;
        this.xmlToMyClientTagRenderService = xmlToMyClientTagRenderService;
        this.xmlToTagRenderService = xmlToTagRenderService;
        this.xmlPartyTagRenderService = xmlPartyTagRenderService;
        this.tTransactionMapper = tTransactionMapper;
    }


    @Override
    public xmlTransactionDto getRenderedTransaction(TTransaction transaction) {
        log.info("Inside XmlTransactionTagRenderService: getRenderedTransaction method");
        try {
            xmlTransactionDto xmlTransactionDto = new xmlTransactionDto();
            if (transaction != null) {
//            System.out.println("xmlTransactionDto: " + transaction);

                xmlTransactionDto.setTransactionnumber(transaction.getTransactionNumber());
                xmlTransactionDto.setInternalRefNumber(transaction.getInternalRefNumber());
                xmlTransactionDto.setTransactionLocation(transaction.getTransactionLocation());
                xmlTransactionDto.setTransactionDescription(transaction.getTransactionDescription());

                if (transaction.getDateTransaction() != null)
                    xmlTransactionDto.setDateTransaction(transaction.getDateTransaction().atTime(0, 0, 0));

                if (transaction.getLateDeposit().booleanValue()) {
                    xmlLateDepositDto xmlLateDepositDto = new xmlLateDepositDto();
                    xmlLateDepositDto.setValue(transaction.getLateDeposit());
                    xmlTransactionDto.setLateDeposit(xmlLateDepositDto);
                }

//            if (transaction.getDatePosting() != null)
//                xmlTransactionDto.setDatePosting(transaction.getDatePosting().atTime(0, 0, 0));

                if (transaction.getValueDate() != null)
                    xmlTransactionDto.setValueDate(transaction.getValueDate().atTime(0, 0, 0));

                xmlTransactionDto.setTransmodeCode(transaction.getRConductionType().getConductionTypeCode());
                xmlTransactionDto.setTransmodeComment(transaction.getTransmodeComment());
                xmlTransactionDto.setAmountLocal(transaction.getAmountLocal());


                if (EnumTagVerifier.isTagValid("TRANSACTION_INVOLVED_PARTIES", transaction.getTrxNo())) {
                    if (transaction.getTParties() != null && !transaction.getTParties().isEmpty())
                        xmlTransactionDto.setInvolvedParties(xmlPartyTagRenderService.getRenderedParty(transaction.getTParties(), transaction.getTrxNo()));
                }

                if (EnumTagVerifier.isTagValid("T_FROM_MY_CLIENT", transaction.getTrxNo())) {
                    if (transaction.getTFromMyClients() != null && !transaction.getTFromMyClients().isEmpty())
                        xmlTransactionDto.setTFromMyClient(xmlFromMyClientTagRenderService.getRenderedTFromMyClient(transaction.getTFromMyClients(), transaction.getTrxNo()));
                }

                if (EnumTagVerifier.isTagValid("T_FROM", transaction.getTrxNo())) {
                    if (transaction.getTFroms() != null && !transaction.getTFroms().isEmpty())
                        xmlTransactionDto.setTFrom(xmlFromTagRenderService.getRenderedTFrom(transaction.getTFroms(), transaction.getTrxNo()));
                }

                if (EnumTagVerifier.isTagValid("T_TO_MY_CLIENT", transaction.getTrxNo())) {
                    if (transaction.getTToMyClients() != null && !transaction.getTToMyClients().isEmpty())
                        xmlTransactionDto.setTToMyClient(xmlToMyClientTagRenderService.getRenderedTToMyClient(transaction.getTToMyClients(), transaction.getTrxNo()));
                }

                if (EnumTagVerifier.isTagValid("T_TO", transaction.getTrxNo())) {
                    if (transaction.getTTos() != null && !transaction.getTTos().isEmpty())
                        xmlTransactionDto.setTTo(xmlToTagRenderService.getRenderedTTo(transaction.getTTos(), transaction.getTrxNo()));
                }


                xmlTransactionDto.setComments(transaction.getComments());
            }


            return xmlTransactionDto;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedTransaction: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    public static LocalDateTime convertToLocalDateTime(LocalDate date) {
        return date.atStartOfDay();
    }
}


