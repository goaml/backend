package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TEntityMyClientDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RCountryCodesDto;
import com.olak.goaml.dto.reference.REntityLegalFormTypeDto;
import com.olak.goaml.dto.reference.REntityPersonRoleTypeDto;
import com.olak.goaml.dto.reference.RGenderTypeDto;
import com.olak.goaml.dto.xml.transaction.TEntityMyClientDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TEntityMyClientMapper;
import com.olak.goaml.models.xml.transaction.TEntityMyClient;
import com.olak.goaml.service.reference.EntityLegalFormTypeService;
import com.olak.goaml.service.reference.EntityPersonRoleTypeService;
import com.olak.goaml.service.reference.RCountryCodesService;
import com.olak.goaml.service.transaction.TEntityMyClientModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TEntityMyClientModelServiceImpl implements TEntityMyClientModelService {
    private final TEntityMyClientDao tEntityMyClientDao;
    private final TEntityMyClientMapper tEntityMyClientMapper;
    private final FilterUtility<TEntityMyClient> filterUtility;
    private final EntityLegalFormTypeService entityLegalFormTypeService;
    private final EntityPersonRoleTypeService entityPersonRoleTypeService;
    private final RCountryCodesService countryCodesService;

    public TEntityMyClientModelServiceImpl(TEntityMyClientDao tEntityMyClientDao, TEntityMyClientMapper tEntityMyClientMapper, EntityLegalFormTypeService entityLegalFormTypeService, EntityPersonRoleTypeService entityPersonRoleTypeService, RCountryCodesService countryCodesService) {
        this.tEntityMyClientDao = tEntityMyClientDao;
        this.filterUtility = new FilterUtility<>(tEntityMyClientDao);
        this.tEntityMyClientMapper = tEntityMyClientMapper;
        this.entityLegalFormTypeService = entityLegalFormTypeService;
        this.entityPersonRoleTypeService = entityPersonRoleTypeService;
        this.countryCodesService = countryCodesService;
    }

    @Override
    public TEntityMyClient getEntityMyClientById(Integer id, Integer clientId) {
        log.info("Inside TEntityMyClientModelService: getEntityMyClientById method");
        try {
            Optional<TEntityMyClient> resultOp = tEntityMyClientDao.findByEntityMyClientIdAndRClientType_ClientTypeIdAndIsActive(id, clientId, true);
            TEntityMyClient response = resultOp.orElse(null);
            return response;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getEntityMyClientById: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TEntityMyClientDto>>> getAllEntityMyClientDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer entityMyClientId) {
        log.info("Inside TEntityMyClientModelServiceImpl: getAllEntityMyClientDetails method");
        try {

            List<FilterCriteria<TEntityMyClient>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("entityMyClientId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (entityMyClientId != null && !entityMyClientId.equals("")) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("entityMyClientId"), entityMyClientId)
                );
            }

            // pageable
            Page<TEntityMyClient> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TEntityMyClientDto> content = tEntityMyClientMapper.listToDto(pageableData.getContent());

            content.forEach(contentItem -> {
                if (contentItem.getEntityLegalFormTypeId() != null){
                    ResponseEntity<REntityLegalFormTypeDto> entityLegalFormType = entityLegalFormTypeService.getEntityLegalFormTypeById(contentItem.getEntityLegalFormTypeId());
                    contentItem.setREntityLegalFormType(entityLegalFormType.getBody());
                }
                if (contentItem.getCountryCodeId() != null) {
                    ResponseEntity<RCountryCodesDto> RCountryCodes = countryCodesService.getCountryCodeById(contentItem.getCountryCodeId());
                    contentItem.setRCountryCodes(RCountryCodes.getBody());
                }

                if (contentItem.getEntityPersonRoleTypeId() != null) {
                    ResponseEntity<REntityPersonRoleTypeDto> RCountryCodes = entityPersonRoleTypeService.getEntityPersonRoleTypeById(contentItem.getEntityPersonRoleTypeId());
                    contentItem.setREntityPersonRoleType(RCountryCodes.getBody());
                }
            });

            // response
            ApiResponseDto<List<TEntityMyClientDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
