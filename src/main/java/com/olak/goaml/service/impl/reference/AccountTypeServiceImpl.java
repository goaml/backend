package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dao.reference.RAccountTypeDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RAccountTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RAccountTypeMapper;
import com.olak.goaml.models.reference.RAccountType;
import com.olak.goaml.service.reference.AccountTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class AccountTypeServiceImpl implements AccountTypeService {

    private final RStatusDao rStatusDao;
    private final RAccountTypeDao rAccountTypeDao;
    private final RAccountTypeMapper rAccountTypeMapper;
    private final SearchNFilter searchNFilter;

    public AccountTypeServiceImpl(RStatusDao rStatusDao, RAccountTypeDao rAccountTypeDao, RAccountTypeMapper rAccountTypeMapper, SearchNFilter searchNFilter) {
        this.rStatusDao = rStatusDao;
        this.rAccountTypeDao = rAccountTypeDao;
        this.rAccountTypeMapper = rAccountTypeMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RAccountTypeDto>> getAllAccountTypes() {
        log.info("Inside AccountTypeService: getAllAccountTypes method");
        List<RAccountType> resultList = rAccountTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rAccountTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RAccountTypeDto>>> getAccountTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside AccountTypeService: getAccountTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RAccountType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RAccountTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rAccountTypeMapper.listToDto((List<RAccountType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RAccountTypeDto> addAccountType(RAccountTypeDto accountTypeDto) {
        log.info("Inside AccountTypeService: addAccountType method");

        try {
            if (accountTypeDto.getAccountTypeCode() == null || accountTypeDto.getAccountTypeCode().isEmpty()) {
                throw new BadRequestAlertException("AccountType code is required.", ENTITY_NAME, "error");
            }

            Optional<RAccountType> existingEntity = rAccountTypeDao.findByAccountTypeCodeAndIsActive(accountTypeDto.getAccountTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("AccountType already exists with code: " + accountTypeDto.getAccountTypeCode(), ENTITY_NAME, "idexists");
            }

            if (accountTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(accountTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "idinvalid");
            }

            accountTypeDto.setIsActive(true);
            accountTypeDto.setStatusId(1);

            RAccountType entity = rAccountTypeMapper.toEntity(accountTypeDto);
            entity.setRStatus(rStatusDao.findById(accountTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "idinvalid")));
            entity = rAccountTypeDao.save(entity);

            return new ResponseEntity<>(rAccountTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding AccountType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RAccountTypeDto> getAccountTypeById(Integer id) {
        log.info("Inside AccountTypeService: getAccountTypeById method");

        try {
            Optional<RAccountType> entity = rAccountTypeDao.findByAccountTypeIdAndIsActive(id, true);
            RAccountTypeDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("AccountType not found by ID: " + id, ENTITY_NAME, "idinvalid");
            } else {
                responseDto = rAccountTypeMapper.toDto(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting AccountType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RAccountTypeDto> updateAccountTypeById(Integer id, RAccountTypeDto accountTypeDto) {
        log.info("Inside AccountTypeService: updateAccountTypeById method");
        if (!id.equals(accountTypeDto.getAccountTypeId())) {
            throw new BadRequestAlertException("AccountType id mismatch.", ENTITY_NAME, "error");
        }

        if (accountTypeDto.getAccountTypeCode() == null || accountTypeDto.getAccountTypeCode().isEmpty()) {
            throw new BadRequestAlertException("AccountType Code is required.", ENTITY_NAME, "error");
        }

        if (rAccountTypeDao.existsByAccountTypeCodeAndIsActiveAndAccountTypeIdNot(accountTypeDto.getAccountTypeCode(), true, id)) {
            throw new BadRequestAlertException("AccountType Code is duplicate with another record.", ENTITY_NAME, "error");
        }

        if (accountTypeDto.getStatusId() == null) {
            throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
        }

        if (!rStatusDao.existsById(accountTypeDto.getStatusId())) {
            throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
        }

        Optional<RAccountType> existingEntity = rAccountTypeDao.findByAccountTypeIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("AccountType not found by ID: " + id, ENTITY_NAME, "error");
        } else {
            RAccountType entity = existingEntity.get();

            entity.setAccountTypeCode(accountTypeDto.getAccountTypeCode());
            entity.setDescription(accountTypeDto.getDescription());
            entity.setIsActive(accountTypeDto.getIsActive());
            entity.setRStatus(rStatusDao.findById(accountTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error")));

            try {
                entity = rAccountTypeDao.save(entity);
                return new ResponseEntity<>(rAccountTypeMapper.toDto(entity), HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while updating AccountType: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }


    @Override
    public ResponseEntity<RAccountTypeDto> deleteAccountTypeById(Integer id) {
        log.info("Inside AccountTypeService: deleteAccountTypeById method");
        try {
            Optional<RAccountType> existingEntity = rAccountTypeDao.findByAccountTypeIdAndIsActive(id, true);
            if (!existingEntity.isPresent()) {
                throw new BadRequestAlertException("Invalid account type ID", ENTITY_NAME, "idinvalid");
            }

            RAccountType entity = existingEntity.get();
            entity.setIsActive(false);
            entity = rAccountTypeDao.save(entity);

            return new ResponseEntity<>(rAccountTypeMapper.toDto(entity), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting Account Type: {}", e.getMessage());
            throw new BadRequestAlertException("Account Type could not be deleted", ENTITY_NAME, "deleteerror");
        }
    }

}
