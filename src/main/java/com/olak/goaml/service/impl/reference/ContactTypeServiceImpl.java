package com.olak.goaml.service.impl.reference;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.olak.goaml.dao.reference.RContactTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RContactTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RContactTypeMapper;
import com.olak.goaml.models.reference.RContactType;
import com.olak.goaml.service.reference.ContactTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ContactTypeServiceImpl implements ContactTypeService {

    private final RStatusDao rStatusDao;
    private final RContactTypeDao rContactTypeDao;
    private final RContactTypeMapper rContactTypeMapper;
    private final SearchNFilter searchNFilter;

    public ContactTypeServiceImpl(RStatusDao rStatusDao, RContactTypeDao rContactTypeDao, RContactTypeMapper rContactTypeMapper, SearchNFilter searchNFilter) {
        this.rStatusDao = rStatusDao;
        this.rContactTypeDao = rContactTypeDao;
        this.rContactTypeMapper = rContactTypeMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RContactTypeDto>> getAllContactTypes() {
        log.info("Inside ContactTypeService: getAllContactTypes method");
        List<RContactType> resultList = rContactTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rContactTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RContactTypeDto>>> getContactTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside ContactTypeService: getContactTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RContactType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RContactTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rContactTypeMapper.listToDto((List<RContactType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RContactTypeDto> addContactType(RContactTypeDto contactTypeDto) {
        log.info("Inside ContactTypeService: addContactType method");

        try {

            if (contactTypeDto.getContactTypeCode() == null || contactTypeDto.getContactTypeCode().isEmpty()) {
                throw new BadRequestAlertException("ContactType code is required.", "ContactType", "error");
            }


            Optional<RContactType> existingEntity = rContactTypeDao.findByContactTypeCodeAndIsActive(contactTypeDto.getContactTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("ContactType already exists with code: " + contactTypeDto.getContactTypeCode(), "ContactType", "error");
            }


            if (contactTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", "ContactType", "error");
            }

            if (!rStatusDao.existsById(contactTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", "ContactType", "error");
            }


            contactTypeDto.setIsActive(true);
            contactTypeDto.setStatusId(1);


            RContactType entity = rContactTypeMapper.toEntity(contactTypeDto);
            entity.setRStatus(rStatusDao.findById(contactTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", "ContactType", "invalidStatusId")));
            entity = rContactTypeDao.save(entity);


            return new ResponseEntity<>(rContactTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding ContactType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "ContactType", "error");
        }
    }


    @Override
    public ResponseEntity<RContactTypeDto> getContactTypeById(Integer id) {
        log.info("Inside ContactTypeService: getContactTypeById method");

        try {
            Optional<RContactType> entity = rContactTypeDao.findByContactTypeIdAndIsActive(id, true);
            RContactTypeDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("ContactType not found by ID: " + id, "ContactType", "error");
            } else {
                responseDto = rContactTypeMapper.toDto(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting ContactType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "ContactType", "error");
        }
    }


    @Override
    public ResponseEntity<RContactTypeDto> updateContactTypeById(Integer id, RContactTypeDto contactTypeDto) {
        log.info("Inside ContactTypeService: updateContactTypeById method");

        if (!id.equals(contactTypeDto.getContactTypeId())) {
            throw new BadRequestAlertException("ContactType id mismatch.", "ContactType", "error");
        }

        if (contactTypeDto.getContactTypeCode() == null || contactTypeDto.getContactTypeCode().isEmpty()) {
            throw new BadRequestAlertException("ContactType Code is required.", "ContactType", "error");
        }

        if (rContactTypeDao.existsByContactTypeCodeAndIsActiveAndContactTypeIdNot(contactTypeDto.getContactTypeCode(), true, id)) {
            throw new BadRequestAlertException("ContactType Code is duplicate with another record.", "ContactType", "error");
        }

        if (contactTypeDto.getStatusId() == null) {
            throw new BadRequestAlertException("Status ID is required.", "ContactType", "error");
        }

        if (!rStatusDao.existsById(contactTypeDto.getStatusId())) {
            throw new BadRequestAlertException("Invalid Status ID.", "ContactType", "error");
        }

        Optional<RContactType> existingEntity = rContactTypeDao.findByContactTypeIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("ContactType not found by ID: " + id, "ContactType", "error");
        }

        RContactType entity = existingEntity.get();
        entity.setContactTypeCode(contactTypeDto.getContactTypeCode());
        entity.setDescription(contactTypeDto.getDescription());
        entity.setIsActive(contactTypeDto.getIsActive());
        entity.setRStatus(rStatusDao.findById(contactTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid Status ID.", "ContactType", "error")));

        try {
            entity = rContactTypeDao.save(entity);
            return new ResponseEntity<>(rContactTypeMapper.toDto(entity), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while updating ContactType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "ContactType", "error");
        }
    }


    @Override
    public ResponseEntity<RContactTypeDto> deleteContactTypeById(Integer id) {
        log.info("Inside ContactTypeService: deleteContactTypeById method");

        try {
            Optional<RContactType> resultOp = rContactTypeDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("RContactType not found by ContactType ID: " + id, ENTITY_NAME, "idinvalid");
            }

            RContactType entity = resultOp.get();
            entity.setIsActive(false);
            entity = rContactTypeDao.save(entity);

            RContactTypeDto responseDto = rContactTypeMapper.toDto(entity);
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting RContactType: {}", e.getMessage());
            throw new BadRequestAlertException("RContactType could not be deleted", ENTITY_NAME, "deleteerror");
        }
    }
}
