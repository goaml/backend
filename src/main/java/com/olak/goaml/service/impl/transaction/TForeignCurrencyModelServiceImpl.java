package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TForeignCurrencyDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.xml.transaction.TForeignCurrencyDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TForeignCurrencyMapper;
import com.olak.goaml.mapper.xml.transaction.TTransactionMapper;
import com.olak.goaml.models.xml.transaction.TForeignCurrency;
import com.olak.goaml.models.xml.transaction.TReport;
import com.olak.goaml.models.xml.transaction.TTransaction;
import com.olak.goaml.service.transaction.TForeignCurrencyModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TForeignCurrencyModelServiceImpl implements TForeignCurrencyModelService {

    private final TForeignCurrencyDao tForeignCurrencyDao;
    private final TForeignCurrencyMapper tForeignCurrencyMapper;
    private final FilterUtility<TForeignCurrency> filterUtility;

    public TForeignCurrencyModelServiceImpl(TForeignCurrencyDao tForeignCurrencyDao, TForeignCurrencyMapper tForeignCurrencyMapper) {
        this.tForeignCurrencyDao = tForeignCurrencyDao;
        this.filterUtility = new FilterUtility<>(tForeignCurrencyDao);
        this.tForeignCurrencyMapper = tForeignCurrencyMapper;
    }

    @Override
    public TForeignCurrency getForeignCurrencyById(Integer id) {
        log.info("Inside TForeignCurrencyModelService: getForeignCurrencyById method");
        try {
            Optional<TForeignCurrency> resultOp = tForeignCurrencyDao.findByForeignCurrencyIdAndIsActive(id, true);
            TForeignCurrency response = resultOp.orElse(null);
            return response;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getForeignCurrencyById: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TForeignCurrencyDto>>> getAllForeignCurrencyDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer foreignCurrencyId) {
        log.info("Inside TReportModelService: getAllReportDetails method");
        try {

            List<FilterCriteria<TForeignCurrency>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("foreignCurrencyId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (foreignCurrencyId != null && foreignCurrencyId != 0) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("foreignCurrencyId"), foreignCurrencyId)
                );
            }

            // pageable
            Page<TForeignCurrency> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TForeignCurrencyDto> content = tForeignCurrencyMapper.listToDto(pageableData.getContent());

            // response
            ApiResponseDto<List<TForeignCurrencyDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
