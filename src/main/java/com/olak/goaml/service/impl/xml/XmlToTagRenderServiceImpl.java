package com.olak.goaml.service.impl.xml;

import com.olak.goaml.dto.xml.transaction.TToDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.xml.transaction.TTo;
import com.olak.goaml.service.xml.*;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.xmlDto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlToTagRenderServiceImpl implements XmlToTagRenderService {

    private final XmlFCurrencyTagRenderService xmlFCurrencyTagRenderService;
    private final XmlPersonTagRenderService xmlPersonTagRenderService;
    private final XmlAccountTagRenderService xmlAccountTagRenderService;
    private final XmlEntityTagRenderService xmlEntityTagRenderService;

    public XmlToTagRenderServiceImpl(XmlFCurrencyTagRenderService xmlFCurrencyTagRenderService, XmlPersonTagRenderService xmlPersonTagRenderService, XmlAccountTagRenderService xmlAccountTagRenderService, XmlEntityTagRenderService xmlEntityTagRenderService) {
        this.xmlFCurrencyTagRenderService = xmlFCurrencyTagRenderService;
        this.xmlPersonTagRenderService = xmlPersonTagRenderService;
        this.xmlAccountTagRenderService = xmlAccountTagRenderService;
        this.xmlEntityTagRenderService = xmlEntityTagRenderService;
    }

    @Override
    public xmlTToDto getRenderedTTo(List<TTo> toList, String scenarioName) {

        try{
            xmlTToDto xmlTToDto = new xmlTToDto();

            if (toList != null &&  !toList.isEmpty()) {
                for (TTo toDto : toList) {
//                TToDto toDto = new TToDto();

                    xmlTToDto.setToFundsCode(toDto.getRFundsType().getFundTypeCode());
                    xmlTToDto.setToFundsComment(toDto.getToFundsComment());

                    if (EnumTagVerifier.isTagValid("T_TO_TO_FOREIGN_CURRENCY", scenarioName)) {
                        if (toDto.getToForeignCurrencyId() != null)
                            xmlTToDto.setToForeignCurrency(xmlFCurrencyTagRenderService.getRenderedFCurrency(toDto.getToForeignCurrencyId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_TO_TO_ACCOUNT", scenarioName)) {
                        if (toDto.getToAccountId() != null)
                            xmlTToDto.setToAccount(xmlAccountTagRenderService.getRenderedAccount(toDto.getToAccountId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_TO_TO_PERSON", scenarioName)) {
                        if (toDto.getToPersonId() != null)
                            xmlTToDto.setToPerson(xmlPersonTagRenderService.getRenderedPerson(toDto.getToPersonId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_TO_TO_ENTITY", scenarioName)) {
                        if (toDto.getToEntityId() != null)
                            xmlTToDto.setToEntity(xmlEntityTagRenderService.getRenderedEntity(toDto.getToEntityId(), scenarioName));
                    }

                    xmlTToDto.setToCountry(toDto.getRCountryCodes().getCountryCodeCode());

                }
            }
            return xmlTToDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedTTo: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
