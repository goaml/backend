package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.reference.RCommunicationTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RCommunicationTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RCommunicationTypeMapper;
import com.olak.goaml.models.reference.RCommunicationType;
import com.olak.goaml.service.reference.CommunicationTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class CommunicationTypeServiceImpl implements CommunicationTypeService {

    private final RCommunicationTypeDao rCommunicationTypeDao;
    private final RCommunicationTypeMapper rCommunicationTypeMapper;
    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;

    public CommunicationTypeServiceImpl(RCommunicationTypeDao rCommunicationTypeDao, RCommunicationTypeMapper rCommunicationTypeMapper, RStatusDao rStatusDao, SearchNFilter searchNFilter) {
        this.rCommunicationTypeDao = rCommunicationTypeDao;
        this.rCommunicationTypeMapper = rCommunicationTypeMapper;
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RCommunicationTypeDto>> getAllCommunicationTypes() {
        log.info("Inside CommunicationTypeService: getAllCommunicationTypes method");
        List<RCommunicationType> resultList = rCommunicationTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rCommunicationTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RCommunicationTypeDto>>> getCommunicationTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside CommunicationTypeService: getCommunicationTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RCommunicationType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RCommunicationTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rCommunicationTypeMapper.listToDto((List<RCommunicationType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RCommunicationTypeDto> addCommunicationType(RCommunicationTypeDto communicationTypeDto) {
        log.info("Inside CommunicationTypeService: addCommunicationType method");

        try {

            if (communicationTypeDto.getCommunicationTypeCode() == null || communicationTypeDto.getCommunicationTypeCode().isEmpty()) {
                throw new BadRequestAlertException("CommunicationType code is required.", ENTITY_NAME, "error");
            }

            Optional<RCommunicationType> existingEntity = rCommunicationTypeDao.findByCommunicationTypeCodeAndIsActive(communicationTypeDto.getCommunicationTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("CommunicationType already exists with code: " + communicationTypeDto.getCommunicationTypeCode(), ENTITY_NAME, "error");
            }

            if (communicationTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(communicationTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
            }


            communicationTypeDto.setIsActive(true);
            communicationTypeDto.setStatusId(1);


            RCommunicationType entity = rCommunicationTypeMapper.toEntity(communicationTypeDto);
            entity.setRStatus(rStatusDao.findById(communicationTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", "CommunicationType", "invalidStatusId")));
            entity = rCommunicationTypeDao.save(entity);


            return new ResponseEntity<>(rCommunicationTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding CommunicationType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RCommunicationTypeDto> getCommunicationTypeById(Integer id) {
        log.info("Inside CommunicationTypeService: getCommunicationTypeById method");

        try {
            Optional<RCommunicationType> entity = rCommunicationTypeDao.findByCommunicationTypeIdAndIsActive(id, true);
            RCommunicationTypeDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("CommunicationType not found by ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = rCommunicationTypeMapper.toDto(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting CommunicationType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RCommunicationTypeDto> updateCommunicationTypeById(Integer id, RCommunicationTypeDto communicationTypeDto) {
        log.info("Inside CommunicationTypeService: updateCommunicationTypeById method");


        if (!id.equals(communicationTypeDto.getCommunicationTypeId())) {
            throw new BadRequestAlertException("CommunicationType id mismatch.", ENTITY_NAME, "error");
        }

        if (communicationTypeDto.getCommunicationTypeCode() == null || communicationTypeDto.getCommunicationTypeCode().isEmpty()) {
            throw new BadRequestAlertException("CommunicationType Code is required.", ENTITY_NAME, "error");
        }

        if (rCommunicationTypeDao.existsByCommunicationTypeCodeAndIsActiveAndCommunicationTypeIdNot(communicationTypeDto.getCommunicationTypeCode(), true, id)) {
            throw new BadRequestAlertException("CommunicationType Code is duplicate with another record.", ENTITY_NAME, "error");
        }

        if (communicationTypeDto.getStatusId() == null) {
            throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
        }

        if (!rStatusDao.existsById(communicationTypeDto.getStatusId())) {
            throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
        }

        Optional<RCommunicationType> existingEntity = rCommunicationTypeDao.findByCommunicationTypeIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("CommunicationType not found by ID: " + id, ENTITY_NAME, "error");
        } else {
            RCommunicationType entity = existingEntity.get();

            entity.setCommunicationTypeCode(communicationTypeDto.getCommunicationTypeCode());
            entity.setDescription(communicationTypeDto.getDescription());
            entity.setIsActive(communicationTypeDto.getIsActive());
            entity.setRStatus(rStatusDao.findById(communicationTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error")));

            try {
                entity = rCommunicationTypeDao.save(entity);
                return new ResponseEntity<>(rCommunicationTypeMapper.toDto(entity), HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while updating CommunicationType: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }


    @Override
    public ResponseEntity<RCommunicationTypeDto> deleteCommunicationTypeById(Integer id) {
        log.info("Inside CommunicationTypeService: deleteCommunicationTypeById method");

        try {
            Optional<RCommunicationType> existingEntity = rCommunicationTypeDao.findByCommunicationTypeIdAndIsActive(id, true);
            if (!existingEntity.isPresent()) {
                throw new BadRequestAlertException("Invalid communication type ID", ENTITY_NAME, "idinvalid");
            }

            RCommunicationType entity = existingEntity.get();
            entity.setIsActive(false);
            entity = rCommunicationTypeDao.save(entity);

            return new ResponseEntity<>(rCommunicationTypeMapper.toDto(entity), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting CommunicationType: {}", e.getMessage());
            throw new BadRequestAlertException("CommunicationType could not be deleted", ENTITY_NAME, "deleteerror");
        }
    }


}

