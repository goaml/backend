package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.master.*;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.master.GoAmlTrxCodesDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.parameter.GoAmlTrxCodesMapper;
import com.olak.goaml.models.master.*;
import com.olak.goaml.service.reference.GoAmlTrxCodeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class GoAmlTrxCodeServiceImpl implements GoAmlTrxCodeService {
    private final TransactionCodesDao transactionCodesDao;
    private final RptCodeDao rptCodeDao;
    private final ReportTypeDao reportTypeDao;
    private final MultiPartyRoleDao multiPartyRoleDao;
    private final MultiPartyInvolvePartyDao multiPartyInvolvePartyDao;
    private final MultiPartyFundCodeDao multiPartyFundCodeDao;
    private final FundTypeDescriptionDao fundTypeDescriptionDao;
    private final FundTypeDao fundTypeDao;
    private final CategoryDao categoryDao;

    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final GoAmlTrxCodeDao goAmlTrxCodeDao;
    private final GoAmlTrxCodesMapper goAmlTrxCodesMapper;

    public GoAmlTrxCodeServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, GoAmlTrxCodeDao goAmlTrxCodeDao, GoAmlTrxCodesMapper goAmlTrxCodesMapper,
                                   CategoryDao categoryDao,
                                   FundTypeDao fundTypeDao,
                                   FundTypeDescriptionDao fundTypeDescriptionDao,
                                   MultiPartyFundCodeDao multiPartyFundCodeDao,
                                   MultiPartyInvolvePartyDao multiPartyInvolvePartyDao,
                                   MultiPartyRoleDao multiPartyRoleDao,
                                   ReportTypeDao reportTypeDao,
                                   RptCodeDao rptCodeDao,
                                   TransactionCodesDao transactionCodesDao) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.goAmlTrxCodeDao = goAmlTrxCodeDao;
        this.goAmlTrxCodesMapper = goAmlTrxCodesMapper;
        this.categoryDao = categoryDao;
        this.fundTypeDao = fundTypeDao;
        this.fundTypeDescriptionDao = fundTypeDescriptionDao;
        this.multiPartyFundCodeDao = multiPartyFundCodeDao;
        this.multiPartyInvolvePartyDao = multiPartyInvolvePartyDao;
        this.multiPartyRoleDao = multiPartyRoleDao;
        this.reportTypeDao = reportTypeDao;
        this.rptCodeDao = rptCodeDao;
        this.transactionCodesDao = transactionCodesDao;
    }

    @Override
    public ResponseEntity<List<GoAmlTrxCodesDto>> getAllGoAmlTrxCodes() {
        log.info("Inside GoAmlTrxCodesService: getAllGoAmlTrxCodes method");
        List<GoAmlTrxCodes> resultList = goAmlTrxCodeDao.findByIsActive(true);
        return new ResponseEntity<>(goAmlTrxCodesMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<GoAmlTrxCodesDto>>> getGoAmlTrxCodesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside GoAmlTrxCodesService: getGoAmlTrxCodesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(GoAmlTrxCodes.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<GoAmlTrxCodesDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(goAmlTrxCodesMapper.listToDto((List<GoAmlTrxCodes>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<GoAmlTrxCodesDto> addGoAmlTrxCodes(GoAmlTrxCodesDto goAmlTrxCodesDto) {
        log.info("Inside GoAmlTrxCodesService: addGoAmlTrxCodes method");

        try {
//            if (goAmlTrxCodesDto.getCategoryId() == null || goAmlTrxCodesDto.getCategoryId().isEmpty())
//                throw new BadRequestAlertException("RptCode name is required: ", ENTITY_NAME, "error");

            if (goAmlTrxCodesDto.getCategoryId() == null)
                throw new BadRequestAlertException("CategoryId name is required: ", ENTITY_NAME, "error");

            Optional<Category> category = categoryDao.findById(goAmlTrxCodesDto.getCategoryId());
            if(category.isPresent())
                goAmlTrxCodesDto.setCategoryName(category.get().getCategoryName());

            if (goAmlTrxCodesDto.getFromFundTypeId() == null)
                throw new BadRequestAlertException("FromFundTypeId name is required: ", ENTITY_NAME, "error");

            Optional<FundType> fundType = fundTypeDao.findById(goAmlTrxCodesDto.getToFundTypeId());
            if (fundType.isPresent()){
                goAmlTrxCodesDto.setFromFundTypeId(goAmlTrxCodesDto.getFromFundTypeId());
                goAmlTrxCodesDto.setFromFundTypeName(fundType.get().getFundTypeName());
            }

            if(goAmlTrxCodesDto.getFromFundTypeDescriptionId() == null)
                throw new BadRequestAlertException("FromFundTypeDescriptionId name is required: ", ENTITY_NAME, "error");

            Optional<FundTypeDescription> fundTypeDescription = fundTypeDescriptionDao.findById(goAmlTrxCodesDto.getFromFundTypeDescriptionId());
            if (fundTypeDescription.isPresent()) {
                goAmlTrxCodesDto.setFromFundTypeDescriptionId(goAmlTrxCodesDto.getFromFundTypeDescriptionId());
                goAmlTrxCodesDto.setFromFundTypeDescription(fundTypeDescription.get().getFundTypeDescription());
            }

            if(goAmlTrxCodesDto.getToFundTypeId() == null)
                throw new BadRequestAlertException("ToFundTypeDescriptionId name is required: ", ENTITY_NAME, "error");

            Optional<FundType> fundType1 = fundTypeDao.findById(goAmlTrxCodesDto.getToFundTypeId());
            if (fundType1.isPresent()){
                goAmlTrxCodesDto.setToFundTypeId(goAmlTrxCodesDto.getToFundTypeId());
                goAmlTrxCodesDto.setToFundTypeName(fundType1.get().getFundTypeName());
            }

            if(goAmlTrxCodesDto.getToFundTypeDescriptionId() == null)
                throw new BadRequestAlertException("ToFundTypeDescriptionId name is required: ", ENTITY_NAME, "error");

            Optional<FundTypeDescription> fundTypeDescription1 = fundTypeDescriptionDao.findById(goAmlTrxCodesDto.getToFundTypeDescriptionId());
            if (fundTypeDescription1.isPresent()){
                goAmlTrxCodesDto.setToFundTypeDescriptionId(goAmlTrxCodesDto.getToFundTypeDescriptionId());
                goAmlTrxCodesDto.setToFundTypeDescription(fundTypeDescription1.get().getFundTypeDescription());
            }

           if(goAmlTrxCodesDto.getMultiPartyFundsCodeId() == null)
               throw new BadRequestAlertException("MultiPartyFundsCodeId name is required: ", ENTITY_NAME, "error");

           Optional<MultiPartyFundsCode> multiPartyFundsCode = multiPartyFundCodeDao.findById(goAmlTrxCodesDto.getMultiPartyFundsCodeId());
           if (multiPartyFundsCode.isPresent()){
               goAmlTrxCodesDto.setMultiPartyFundsCodeId(goAmlTrxCodesDto.getMultiPartyFundsCodeId());
               goAmlTrxCodesDto.setMultiPartyFundsCodeName(multiPartyFundsCode.get().getMultiPartyFundsCodeName());
           }

           if(goAmlTrxCodesDto.getMultiPartyInvolvePartyId() == null)
               throw new BadRequestAlertException("MultiPartyInvolvePartyId name is required: ", ENTITY_NAME, "error");

           Optional<MultiPartyInvolveParty> multiPartyInvolveParty = multiPartyInvolvePartyDao.findById(goAmlTrxCodesDto.getMultiPartyInvolvePartyId());
           if(multiPartyInvolveParty.isPresent()){
               goAmlTrxCodesDto.setMultiPartyInvolvePartyId(goAmlTrxCodesDto.getMultiPartyInvolvePartyId());
               goAmlTrxCodesDto.setMultiPartyInvolvePartyName(multiPartyInvolveParty.get().getMultiPartyInvolvePartyName());
           }

           if(goAmlTrxCodesDto.getMultiPartyRoleId() == null)
               throw new BadRequestAlertException("MultiPartyRoleId name is required: ", ENTITY_NAME, "error");

           Optional<MultiPartyRole> multiPartyRole = multiPartyRoleDao.findById(goAmlTrxCodesDto.getMultiPartyRoleId());
           if(multiPartyRole.isPresent()){
               goAmlTrxCodesDto.setMultiPartyRoleId(goAmlTrxCodesDto.getMultiPartyRoleId());
               goAmlTrxCodesDto.setMultiPartyRoleName(multiPartyRole.get().getMultiPartyRoleName());
           }

           if(goAmlTrxCodesDto.getReportTypeId() == null)
               throw new BadRequestAlertException("ReportTypeId name is required: ", ENTITY_NAME, "error");

           Optional<ReportType> reportType = reportTypeDao.findById(goAmlTrxCodesDto.getReportTypeId());
           if(reportType.isPresent()){
               goAmlTrxCodesDto.setReportTypeId(goAmlTrxCodesDto.getReportTypeId());
               goAmlTrxCodesDto.setReportTypeName(reportType.get().getReportTypeName());
           }

           if(goAmlTrxCodesDto.getRptCodeId() == null)
               throw new BadRequestAlertException("RptCodeId name is required: ", ENTITY_NAME, "error");

           Optional<RptCode> rptCode = rptCodeDao.findById(goAmlTrxCodesDto.getRptCodeId());
           if(rptCode.isPresent()){
               goAmlTrxCodesDto.setRptCodeId(goAmlTrxCodesDto.getRptCodeId());
               goAmlTrxCodesDto.setRptCodeName(rptCode.get().getRptCodeName());
           }

           if(goAmlTrxCodesDto.getTransactionCodeId() == null)
               throw new BadRequestAlertException("TransactionCodeId name is required: ", ENTITY_NAME, "error");

           Optional<TransactionCodes> transactionCodes = transactionCodesDao.findById(goAmlTrxCodesDto.getTransactionCodeId());
           if(transactionCodes.isPresent()){
               goAmlTrxCodesDto.setTransactionCodeId(goAmlTrxCodesDto.getTransactionCodeId());
               goAmlTrxCodesDto.setTransactionCodeName(transactionCodes.get().getTransactionCodeName());
           }

           if(goAmlTrxCodesDto.getGoamlTrxCodeScenario() == null || goAmlTrxCodesDto.getGoamlTrxCodeScenario().isEmpty())
               throw new BadRequestAlertException("GoamlTrxCode Scenario is required: ", ENTITY_NAME, "error");

           if(goAmlTrxCodesDto.getGoamlTrxCodeComments() == null || goAmlTrxCodesDto.getGoamlTrxCodeComments().isEmpty())
               throw new BadRequestAlertException("GoamlTrxCode Comments is required: ", ENTITY_NAME, "error");

           if(goAmlTrxCodesDto.getGoamlTrxCodeExample() == null || goAmlTrxCodesDto.getGoamlTrxCodeExample().isEmpty())
               throw new BadRequestAlertException("GoamlTrxCode Example is required: ", ENTITY_NAME, "error");

//            if (goAmlTrxCodesDto.getStatusId() == null)
            goAmlTrxCodesDto.setStatusId(1);

            goAmlTrxCodesDto.setIsActive(true);

            if (!rStatusDao.existsById(goAmlTrxCodesDto.getStatusId()))
                throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

            GoAmlTrxCodesDto saveDtoRes = goAmlTrxCodesMapper.toDto(goAmlTrxCodeDao.save(goAmlTrxCodesMapper.toEntity(goAmlTrxCodesDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add RptCode: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<GoAmlTrxCodesDto> getGoAmlTrxCodesById(Long id) {
        log.info("Inside GoAmlTrxCodesService: getGoAmlTrxCodesById method");

        try {
            Optional<GoAmlTrxCodes> resultOp = goAmlTrxCodeDao.findByGoamlTrxCodeIdAndIsActive(id, true);
            GoAmlTrxCodesDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("GoAmlTrxCodes not found by GoAmlTrxCodes ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = goAmlTrxCodesMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get GoAmlTrxCodes details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<GoAmlTrxCodesDto> updateGoAmlTrxCodesById(Long id, GoAmlTrxCodesDto goAmlTrxCodesDto) {
        log.info("Inside RptCodeService: updateRptCodeById method");

        // Validate
        if (id != goAmlTrxCodesDto.getGoamlTrxCodeId())
            throw new BadRequestAlertException("Transaction id mismatch.", ENTITY_NAME, "error");

        if (goAmlTrxCodesDto.getCategoryId() == null)
            throw new BadRequestAlertException("CategoryId name is required: ", ENTITY_NAME, "error");

        Optional<Category> category = categoryDao.findById(goAmlTrxCodesDto.getCategoryId());
        if(category.isPresent())
            goAmlTrxCodesDto.setCategoryName(category.get().getCategoryName());

        if (goAmlTrxCodesDto.getFromFundTypeId() == null)
            throw new BadRequestAlertException("FromFundTypeId name is required: ", ENTITY_NAME, "error");

        Optional<FundType> fundType = fundTypeDao.findById(goAmlTrxCodesDto.getToFundTypeId());
        if (fundType.isPresent()){
            goAmlTrxCodesDto.setFromFundTypeId(goAmlTrxCodesDto.getFromFundTypeId());
            goAmlTrxCodesDto.setFromFundTypeName(fundType.get().getFundTypeName());
        }

        if(goAmlTrxCodesDto.getFromFundTypeDescriptionId() == null)
            throw new BadRequestAlertException("FromFundTypeDescriptionId name is required: ", ENTITY_NAME, "error");

        Optional<FundTypeDescription> fundTypeDescription = fundTypeDescriptionDao.findById(goAmlTrxCodesDto.getFromFundTypeDescriptionId());
        if (fundTypeDescription.isPresent()) {
            goAmlTrxCodesDto.setFromFundTypeDescriptionId(goAmlTrxCodesDto.getFromFundTypeDescriptionId());
            goAmlTrxCodesDto.setFromFundTypeDescription(fundTypeDescription.get().getFundTypeDescription());
        }

        if(goAmlTrxCodesDto.getToFundTypeId() == null)
            throw new BadRequestAlertException("ToFundTypeDescriptionId name is required: ", ENTITY_NAME, "error");

        Optional<FundType> fundType1 = fundTypeDao.findById(goAmlTrxCodesDto.getToFundTypeId());
        if (fundType1.isPresent()){
            goAmlTrxCodesDto.setToFundTypeId(goAmlTrxCodesDto.getToFundTypeId());
            goAmlTrxCodesDto.setToFundTypeName(fundType1.get().getFundTypeName());
        }

        if(goAmlTrxCodesDto.getToFundTypeDescriptionId() == null)
            throw new BadRequestAlertException("ToFundTypeDescriptionId name is required: ", ENTITY_NAME, "error");

        Optional<FundTypeDescription> fundTypeDescription1 = fundTypeDescriptionDao.findById(goAmlTrxCodesDto.getToFundTypeDescriptionId());
        if (fundTypeDescription1.isPresent()){
            goAmlTrxCodesDto.setToFundTypeDescriptionId(goAmlTrxCodesDto.getToFundTypeDescriptionId());
            goAmlTrxCodesDto.setToFundTypeDescription(fundTypeDescription1.get().getFundTypeDescription());
        }

        if(goAmlTrxCodesDto.getMultiPartyFundsCodeId() == null)
            throw new BadRequestAlertException("MultiPartyFundsCodeId name is required: ", ENTITY_NAME, "error");

        Optional<MultiPartyFundsCode> multiPartyFundsCode = multiPartyFundCodeDao.findById(goAmlTrxCodesDto.getMultiPartyFundsCodeId());
        if (multiPartyFundsCode.isPresent()){
            goAmlTrxCodesDto.setMultiPartyFundsCodeId(goAmlTrxCodesDto.getMultiPartyFundsCodeId());
            goAmlTrxCodesDto.setMultiPartyFundsCodeName(multiPartyFundsCode.get().getMultiPartyFundsCodeName());
        }

        if(goAmlTrxCodesDto.getMultiPartyInvolvePartyId() == null)
            throw new BadRequestAlertException("MultiPartyInvolvePartyId name is required: ", ENTITY_NAME, "error");

        Optional<MultiPartyInvolveParty> multiPartyInvolveParty = multiPartyInvolvePartyDao.findById(goAmlTrxCodesDto.getMultiPartyInvolvePartyId());
        if(multiPartyInvolveParty.isPresent()){
            goAmlTrxCodesDto.setMultiPartyInvolvePartyId(goAmlTrxCodesDto.getMultiPartyInvolvePartyId());
            goAmlTrxCodesDto.setMultiPartyInvolvePartyName(multiPartyInvolveParty.get().getMultiPartyInvolvePartyName());
        }

        if(goAmlTrxCodesDto.getMultiPartyRoleId() == null)
            throw new BadRequestAlertException("MultiPartyRoleId name is required: ", ENTITY_NAME, "error");

        Optional<MultiPartyRole> multiPartyRole = multiPartyRoleDao.findById(goAmlTrxCodesDto.getMultiPartyRoleId());
        if(multiPartyRole.isPresent()){
            goAmlTrxCodesDto.setMultiPartyRoleId(goAmlTrxCodesDto.getMultiPartyRoleId());
            goAmlTrxCodesDto.setMultiPartyRoleName(multiPartyRole.get().getMultiPartyRoleName());
        }

        if(goAmlTrxCodesDto.getReportTypeId() == null)
            throw new BadRequestAlertException("ReportTypeId name is required: ", ENTITY_NAME, "error");

        Optional<ReportType> reportType = reportTypeDao.findById(goAmlTrxCodesDto.getReportTypeId());
        if(reportType.isPresent()){
            goAmlTrxCodesDto.setReportTypeId(goAmlTrxCodesDto.getReportTypeId());
            goAmlTrxCodesDto.setReportTypeName(reportType.get().getReportTypeName());
        }

        if(goAmlTrxCodesDto.getRptCodeId() == null)
            throw new BadRequestAlertException("RptCodeId name is required: ", ENTITY_NAME, "error");

        Optional<RptCode> rptCode = rptCodeDao.findById(goAmlTrxCodesDto.getRptCodeId());
        if(rptCode.isPresent()){
            goAmlTrxCodesDto.setRptCodeId(goAmlTrxCodesDto.getRptCodeId());
            goAmlTrxCodesDto.setRptCodeName(rptCode.get().getRptCodeName());
        }

        if(goAmlTrxCodesDto.getTransactionCodeId() == null)
            throw new BadRequestAlertException("TransactionCodeId name is required: ", ENTITY_NAME, "error");

        Optional<TransactionCodes> transactionCodes = transactionCodesDao.findById(goAmlTrxCodesDto.getTransactionCodeId());
        if(transactionCodes.isPresent()){
            goAmlTrxCodesDto.setTransactionCodeId(goAmlTrxCodesDto.getTransactionCodeId());
            goAmlTrxCodesDto.setTransactionCodeName(transactionCodes.get().getTransactionCodeName());
        }


        if (goAmlTrxCodesDto.getStatusId() == null)
            throw new BadRequestAlertException("Status Can Not Be Empty: ", ENTITY_NAME, "error");

        if (!rStatusDao.existsById(goAmlTrxCodesDto.getStatusId()))
            throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

        Optional<GoAmlTrxCodes> resultOp = goAmlTrxCodeDao.findByGoamlTrxCodeIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("RptCode not found by Go Aml Trx Codes ID: " + id, ENTITY_NAME, "error");
        } else {
            GoAmlTrxCodes existingRes = resultOp.get();
            goAmlTrxCodesDto.setIsActive(existingRes.getIsActive());
            goAmlTrxCodesDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                GoAmlTrxCodesDto responseDto = goAmlTrxCodesMapper.toDto(goAmlTrxCodeDao.save(goAmlTrxCodesMapper.toEntity(goAmlTrxCodesDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update Go Aml Trx Codes by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<GoAmlTrxCodesDto> deleteGoAmlTrxCodesById(Long id) {
        log.info("Inside GoAmlTrxCodesService: deleteGoAmlTrxCodesById method");

        try {
            Optional<GoAmlTrxCodes> resultOp = goAmlTrxCodeDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("GoAmlTrxCodes not found by GoAmlTrxCodes ID: " + id, ENTITY_NAME, "error");
            } else {
                GoAmlTrxCodes existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                GoAmlTrxCodesDto responseDto = goAmlTrxCodesMapper.toDto(existingRes);
                responseDto = goAmlTrxCodesMapper.toDto(goAmlTrxCodeDao.save(goAmlTrxCodesMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete GoAmlTrxCodes: {}", e.getMessage());
            throw new BadRequestAlertException("GoAmlTrxCodes con not delete by GoAmlTrxCodes ID: " + id, ENTITY_NAME, "error");
        }
    }
}
