package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.master.CategoryDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.master.CategoryDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.parameter.CategoryMapper;
import com.olak.goaml.models.master.Category;
import com.olak.goaml.service.reference.CategoryService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

    private final RStatusDao rStatusDao;
    private final CategoryDao categoryDao;
    private final CategoryMapper categoryMapper;
    private final SearchNFilter searchNFilter;

    public CategoryServiceImpl(RStatusDao rStatusDao, CategoryDao categoryDao, CategoryMapper categoryMapper, SearchNFilter searchNFilter) {
        this.rStatusDao = rStatusDao;
        this.categoryDao = categoryDao;
        this.categoryMapper = categoryMapper;
        this.searchNFilter = searchNFilter;
    }


    @Override
    public ResponseEntity<List<CategoryDto>> getAllCategory() {
        log.info("Inside CategoryService: getAllCategory method");
        List<Category> resultList = categoryDao.findByIsActive(true);
        return new ResponseEntity<>(categoryMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<CategoryDto>>> getCategoryWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside NatureTypeService: getNatureTypeWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(Category.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<CategoryDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(categoryMapper.listToDto((List<Category>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @Override
    public ResponseEntity<CategoryDto> addCategory(CategoryDto categoryDto) {
        log.info("Inside CategoryService: addCategory method");

        try {
            if (categoryDto.getCategoryName() == null || categoryDto.getCategoryName().isEmpty())
                throw new BadRequestAlertException("Category name is required: ", ENTITY_NAME, "error");

            Optional<Category> resultOp = categoryDao.findByCategoryNameAndIsActive(categoryDto.getCategoryName(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("Category already exists by Category name: " + categoryDto.getCategoryName(), ENTITY_NAME, "error");

            if (categoryDto.getCategoryDescription() == null || categoryDto.getCategoryDescription().isEmpty())
                throw new BadRequestAlertException("Category Description is required: ", ENTITY_NAME, "error");

            Optional<Category> resultOp1 = categoryDao.findByCategoryDescriptionAndIsActive(categoryDto.getCategoryDescription(), true);
            if (resultOp1.isPresent())
                throw new BadRequestAlertException("Category already exists by Category Description: " + categoryDto.getCategoryDescription(), ENTITY_NAME, "error");

//            if (categoryDto.getStatusId() == null)
                categoryDto.setStatusId(1);

            categoryDto.setIsActive(true);

//            if (!rStatusDao.existsById(categoryDto.getStatusId()))
//                throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

            CategoryDto saveDtoRes = categoryMapper.toDto(categoryDao.save(categoryMapper.toEntity(categoryDto)));

                return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add Category: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<CategoryDto> getCategoryById(Long id) {

        log.info("Inside CategoryService: getCategoryById method");

        try {
            Optional<Category> resultOp = categoryDao.findByCategoryIdAndIsActive(id, true);
            CategoryDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Category not found by Category ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = categoryMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get Category details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<CategoryDto> updateCategoryById(Long id, CategoryDto categoryDto) {

        log.info("Inside CategoryService: updateCategoryById method");

        // Validate
        if (id != categoryDto.getCategoryId())
            throw new BadRequestAlertException("Category id mismatch.", ENTITY_NAME, "error");

        if (categoryDto.getCategoryName() == null || categoryDto.getCategoryName().isEmpty())
            throw new BadRequestAlertException("Category Name is required: ", ENTITY_NAME, "error");

        if (categoryDao.existsByCategoryNameAndIsActiveAndCategoryIdNot(categoryDto.getCategoryName(), true, id))
            throw new BadRequestAlertException("Category Name is duplicate with another recode: ", ENTITY_NAME, "error");

//        if (categoryDto.getStatusId() == null)
//            throw new BadRequestAlertException("Status Can Not Be Empty: ", ENTITY_NAME, "error");
//
//        if (!rStatusDao.existsById(categoryDto.getStatusId()))
//            throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

        Optional<Category> resultOp = categoryDao.findByCategoryIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("Category not found by Category ID: " + id, ENTITY_NAME, "error");
        } else {
            Category existingRes = resultOp.get();
            categoryDto.setIsActive(existingRes.getIsActive());
            categoryDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                CategoryDto responseDto = categoryMapper.toDto(categoryDao.save(categoryMapper.toEntity(categoryDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update Category by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<CategoryDto> deleteCategoryById(Long id) {
        log.info("Inside CategoryService: deleteCategoryById method");

        try {
            Optional<Category> resultOp = categoryDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Category not found by Category ID: " + id, ENTITY_NAME, "error");
            } else {
                Category existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                CategoryDto responseDto = categoryMapper.toDto(existingRes);
                responseDto = categoryMapper.toDto(categoryDao.save(categoryMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete Category: {}", e.getMessage());
            throw new BadRequestAlertException("Category con not delete by Category ID: " + id, ENTITY_NAME, "error");
        }
    }
}
