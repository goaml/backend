package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TPersonIdentificationDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.xml.transaction.TPersonIdentificationDto;
import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TPersonIdentificationMapper;
import com.olak.goaml.mapper.xml.transaction.TTransactionMapper;
import com.olak.goaml.models.xml.transaction.TAddress;
import com.olak.goaml.models.xml.transaction.TPersonIdentification;
import com.olak.goaml.models.xml.transaction.TTransaction;
import com.olak.goaml.service.transaction.TPersonIdentificationModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TPersonIdentificationModelServiceImpl implements TPersonIdentificationModelService {
    private final TPersonIdentificationDao tPersonIdentificationDao;
    private final TPersonIdentificationMapper tPersonIdentificationMapper;
    private final FilterUtility<TPersonIdentification> filterUtility;

    public TPersonIdentificationModelServiceImpl(TPersonIdentificationDao tPersonIdentificationDao, TPersonIdentificationMapper tPersonIdentificationMapper) {
        this.tPersonIdentificationDao = tPersonIdentificationDao;
        this.filterUtility = new FilterUtility<>(tPersonIdentificationDao);
        this.tPersonIdentificationMapper = tPersonIdentificationMapper;
    }

    @Override
    public TPersonIdentification getIdentificationById(Integer id) {
        log.info("Inside TPersonIdentificationModelService: getIdentificationById method");
        try {
            Optional<TPersonIdentification> resultOp = tPersonIdentificationDao.findByPersonIdentificationIdAndIsActive(id, true);
            TPersonIdentification response = resultOp.orElse(null);
            return response;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getIdentificationById: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public List<TPersonIdentification> getIdentificationListById(Integer id) {
        log.info("Inside TPersonIdentificationModelService: getIdentificationListById method");
        try {
            List<TPersonIdentification> personIdentificationList = new ArrayList<>();
            Optional<TPersonIdentification> resultOp = tPersonIdentificationDao.findByPersonIdentificationIdAndIsActive(id, true);
            if (resultOp.isPresent()) {
                TPersonIdentification personIdentification = resultOp.get();
                personIdentificationList.add(personIdentification);
            }
            return personIdentificationList;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getIdentificationListById: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TPersonIdentificationDto>>> getAllPersonIdentificationDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer personIdentificationId) {
        log.info("Inside TPersonIdentificationModelService: getAllPersonIdentificationDetails method");
        try {

            List<FilterCriteria<TPersonIdentification>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("personIdentificationId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (personIdentificationId != null && personIdentificationId != 0) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("personIdentificationId"), personIdentificationId)
                );
            }

            // pageable
            Page<TPersonIdentification> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TPersonIdentificationDto> content = tPersonIdentificationMapper.listToDto(pageableData.getContent());

            // response
            ApiResponseDto<List<TPersonIdentificationDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
