package com.olak.goaml.service.impl.xml;

import com.olak.goaml.dao.reference.RCountryCodesDao;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.reference.RCountryCodes;
import com.olak.goaml.models.xml.transaction.TPersonIdentification;
import com.olak.goaml.service.transaction.TPersonIdentificationModelService;
import com.olak.goaml.service.xml.XmlPIdentificationTagRenderService;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonIdentificationDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlPIdentificationTagRenderServiceImpl implements XmlPIdentificationTagRenderService {

    private final TPersonIdentificationModelService tPersonIdentificationModelService;
    private final RCountryCodesDao rCountryCodesDao;

    public XmlPIdentificationTagRenderServiceImpl(TPersonIdentificationModelService tPersonIdentificationModelService, RCountryCodesDao rCountryCodesDao) {
        this.tPersonIdentificationModelService = tPersonIdentificationModelService;
        this.rCountryCodesDao = rCountryCodesDao;
    }

    @Override
    public xmlTPersonIdentificationDto getRenderedIdentification(Integer id, String scenarioName) {

        try{
            TPersonIdentification personIdentificationDto = tPersonIdentificationModelService.getIdentificationById(id);
            xmlTPersonIdentificationDto XmlPersonIdentificationDto = new xmlTPersonIdentificationDto();

            if (personIdentificationDto != null) {
                XmlPersonIdentificationDto.setType(personIdentificationDto.getRIdentifierType().getIdentifierTypeCode());
                XmlPersonIdentificationDto.setNumber(personIdentificationDto.getNumber());

                if (personIdentificationDto.getIssueDate() != null) {
                    Date issueDate = personIdentificationDto.getIssueDate();
                    XmlPersonIdentificationDto.setIssueDate(XmlUtils.convertDateToLocalDate(issueDate).atTime(0, 0, 0));
                }

                if (personIdentificationDto.getExpiryDate() != null) {
                    Date expiryDate = personIdentificationDto.getExpiryDate();
                    XmlPersonIdentificationDto.setExpiryDate(XmlUtils.convertDateToLocalDate(expiryDate).atTime(0, 0, 0));
                }

                XmlPersonIdentificationDto.setIssuedBy(personIdentificationDto.getIssuedBy());

                if (personIdentificationDto.getIssueCountryCodeId() != null) {
                    Optional<RCountryCodes> countryOp = rCountryCodesDao.findByCountryCodeIdAndIsActive(personIdentificationDto.getIssueCountryCodeId(), true);
                    countryOp.ifPresent(country -> XmlPersonIdentificationDto.setIssueCountry(country.getCountryCodeCode()));
                }

                XmlPersonIdentificationDto.setComments(personIdentificationDto.getComments());
            }

            return XmlPersonIdentificationDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedIdentification: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public List<xmlTPersonIdentificationDto> getRenderedIdentificationList(Integer id, String scenarioName) {

        try{
            List<xmlTPersonIdentificationDto> xmlTPersonIdentificationDtoList = new ArrayList<>();

            List<TPersonIdentification> personIdentificationDtoList = tPersonIdentificationModelService.getIdentificationListById(id);

            if (personIdentificationDtoList != null && !personIdentificationDtoList.isEmpty()) {
                for (TPersonIdentification personIdentificationDto : personIdentificationDtoList) {

                    xmlTPersonIdentificationDto XmlPersonIdentificationDto = new xmlTPersonIdentificationDto();

                    XmlPersonIdentificationDto.setType(personIdentificationDto.getRIdentifierType().getIdentifierTypeCode());
                    XmlPersonIdentificationDto.setNumber(personIdentificationDto.getNumber());

                    if (personIdentificationDto.getIssueDate() != null) {
                        Date issueDate = personIdentificationDto.getIssueDate();
                        XmlPersonIdentificationDto.setIssueDate(XmlUtils.convertDateToLocalDate(issueDate).atTime(0, 0, 0));
                    }

                    if (personIdentificationDto.getExpiryDate() != null) {
                        Date expiryDate = personIdentificationDto.getExpiryDate();
                        XmlPersonIdentificationDto.setExpiryDate(XmlUtils.convertDateToLocalDate(expiryDate).atTime(0, 0, 0));
                    }

                    XmlPersonIdentificationDto.setIssuedBy(personIdentificationDto.getIssuedBy());

                    if (personIdentificationDto.getIssueCountryCodeId() != null) {
                        Optional<RCountryCodes> countryOp = rCountryCodesDao.findByCountryCodeIdAndIsActive(personIdentificationDto.getIssueCountryCodeId(), true);
                        countryOp.ifPresent(country -> XmlPersonIdentificationDto.setIssueCountry(country.getCountryCodeCode()));
                    }

                    XmlPersonIdentificationDto.setComments(personIdentificationDto.getComments());

                    xmlTPersonIdentificationDtoList.add(XmlPersonIdentificationDto);
                }
            }

            if (xmlTPersonIdentificationDtoList.isEmpty()) {
                xmlTPersonIdentificationDtoList = null;
            }

            return xmlTPersonIdentificationDtoList;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedIdentificationList: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
