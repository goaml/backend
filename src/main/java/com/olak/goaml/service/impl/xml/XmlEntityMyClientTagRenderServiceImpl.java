package com.olak.goaml.service.impl.xml;

import com.olak.goaml.constants.ClientType;
import com.olak.goaml.dao.reference.RCountryCodesDao;
import com.olak.goaml.dao.reference.REntityLegalFormTypeDao;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.reference.RCountryCodes;
import com.olak.goaml.models.reference.REntityLegalFormType;
import com.olak.goaml.models.xml.transaction.TEntityMyClient;
import com.olak.goaml.service.transaction.TEntityMyClientModelService;
import com.olak.goaml.service.xml.XMLAddressTagRenderService;
import com.olak.goaml.service.xml.XMLPhoneTagRenderService;
import com.olak.goaml.service.xml.XmlDirectorMyClientTagRenderService;
import com.olak.goaml.service.xml.XmlEntityMyClientTagRenderService;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTEntityMyClientDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlEntityMyClientTagRenderServiceImpl implements XmlEntityMyClientTagRenderService {

    private final XMLAddressTagRenderService xmlAddressTagRenderService;
    private final XMLPhoneTagRenderService xmlPhoneTagRenderService;
    private final XmlDirectorMyClientTagRenderService xmlDirectorMyClientTagRenderService;
    private final TEntityMyClientModelService tEntityMyClientModelService;
    private final REntityLegalFormTypeDao rEntityLegalFormTypeDao;
    private final RCountryCodesDao rCountryCodesDao;

    public XmlEntityMyClientTagRenderServiceImpl(XMLAddressTagRenderService xmlAddressTagRenderService, XMLPhoneTagRenderService xmlPhoneTagRenderService, XmlDirectorMyClientTagRenderService xmlDirectorMyClientTagRenderService, TEntityMyClientModelService tEntityMyClientModelService, REntityLegalFormTypeDao rEntityLegalFormTypeDao, RCountryCodesDao rCountryCodesDao) {
        this.xmlAddressTagRenderService = xmlAddressTagRenderService;
        this.xmlPhoneTagRenderService = xmlPhoneTagRenderService;
        this.xmlDirectorMyClientTagRenderService = xmlDirectorMyClientTagRenderService;
        this.tEntityMyClientModelService = tEntityMyClientModelService;
        this.rEntityLegalFormTypeDao = rEntityLegalFormTypeDao;
        this.rCountryCodesDao = rCountryCodesDao;
    }

    @Override
    public xmlTEntityMyClientDto getRenderedEntity(Integer Id, String scenarioName) {

        try {
            TEntityMyClient entityMyClientDto = tEntityMyClientModelService.getEntityMyClientById(Id, ClientType.My_CLIENT);
            xmlTEntityMyClientDto xmlTEntityMyClientDto = new xmlTEntityMyClientDto();

            if (entityMyClientDto != null) {
                xmlTEntityMyClientDto.setName(entityMyClientDto.getName());
                xmlTEntityMyClientDto.setCommercialName(entityMyClientDto.getCommercialName());

                if (entityMyClientDto.getEntityLegalFormTypeId() != null) {
                    Optional<REntityLegalFormType> rEntityLegalFormTypeOp = rEntityLegalFormTypeDao.findByEntityLegalFormTypeIdAndIsActive(entityMyClientDto.getEntityLegalFormTypeId(), true);
                    rEntityLegalFormTypeOp.ifPresent(rEntityLegalFormType -> xmlTEntityMyClientDto.setIncorporationLegalForm(rEntityLegalFormType.getEntityLegalFormTypeCode()));
                }

                xmlTEntityMyClientDto.setIncorporationNumber(entityMyClientDto.getIncorporationNumber());
                xmlTEntityMyClientDto.setBusiness(entityMyClientDto.getBusiness());

                if (EnumTagVerifier.isTagValid("ENTITY_MY_CLIENT_PHONES_PHONE", scenarioName)) {
                    if (entityMyClientDto.getPhoneId() != null)
                        xmlTEntityMyClientDto.setPhones(xmlPhoneTagRenderService.getRenderedPhoneList(entityMyClientDto.getPhoneId(), scenarioName));
                }

                if (EnumTagVerifier.isTagValid("ENTITY_MY_CLIENT_ADDRESSES_ADDRESS", scenarioName)) {
                    if (entityMyClientDto.getAddressId() != null)
                        xmlTEntityMyClientDto.setAddresses(xmlAddressTagRenderService.getRenderedAddressList(entityMyClientDto.getAddressId(), scenarioName));
                }

                xmlTEntityMyClientDto.setEmail(entityMyClientDto.getEmail());
                xmlTEntityMyClientDto.setUrl(entityMyClientDto.getUrl());
                xmlTEntityMyClientDto.setIncorporationState(entityMyClientDto.getIncorporationState());

                if (entityMyClientDto.getCountryCodeId() != null) {
                    Optional<RCountryCodes> rCountryCodesOp = rCountryCodesDao.findByCountryCodeIdAndIsActive(entityMyClientDto.getCountryCodeId(), true);
                    rCountryCodesOp.ifPresent(rCountryCodes -> xmlTEntityMyClientDto.setIncorporationCountryCode(rCountryCodes.getCountryCodeCode()));
                }

                if (EnumTagVerifier.isTagValid("ENTITY_MY_CLIENT_DIRECTOR_ID", scenarioName)) {
                    if (entityMyClientDto.getDirectorPersonId() != null)
                        xmlTEntityMyClientDto.setDirectorId(xmlDirectorMyClientTagRenderService.getRenderedDirectorList(entityMyClientDto.getDirectorPersonId(), scenarioName));
                }


                if (entityMyClientDto.getIncorporationDate() != null) {
                    Date incorporationDate = entityMyClientDto.getIncorporationDate();
                    xmlTEntityMyClientDto.setIncorporationDate(XmlUtils.convertDateToLocalDate(incorporationDate).atTime(0, 0, 0));
                }

                if (entityMyClientDto.getBusinessClosed()) {
                    xmlTEntityMyClientDto.setBusinessClosed(entityMyClientDto.getBusinessClosed());

                    if (entityMyClientDto.getDateBusinessClosed() != null) {
                        Date dateBusinessClosed = entityMyClientDto.getDateBusinessClosed();
                        xmlTEntityMyClientDto.setDateBusinessClosed(XmlUtils.convertDateToLocalDate(dateBusinessClosed).atTime(0, 0, 0));
                    }

                }

                xmlTEntityMyClientDto.setTaxNumber(entityMyClientDto.getTaxNumber());
                xmlTEntityMyClientDto.setTaxRegNumber(entityMyClientDto.getTaxRegNumber());
                xmlTEntityMyClientDto.setComments(entityMyClientDto.getComments());
            }

            return xmlTEntityMyClientDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedEntity: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
