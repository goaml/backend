package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TAccountMyClientDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RAccountPersonRoleTypeDto;
import com.olak.goaml.dto.reference.RAccountStatusTypeDto;
import com.olak.goaml.dto.reference.RAccountTypeDto;
import com.olak.goaml.dto.reference.RCurrencyDto;
import com.olak.goaml.dto.xml.transaction.TAccountMyClientDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TAccountMyClientMapper;
import com.olak.goaml.models.xml.transaction.TAccountMyClient;
import com.olak.goaml.service.reference.AccountPersonRoleTypeService;
import com.olak.goaml.service.reference.AccountStatusTypeService;
import com.olak.goaml.service.reference.AccountTypeService;
import com.olak.goaml.service.reference.CurrencyService;
import com.olak.goaml.service.transaction.TAccountMyClientModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TAccountMyClientModelServiceImpl implements TAccountMyClientModelService {
    private final TAccountMyClientDao tAccountMyClientDao;
    private final TAccountMyClientMapper tAccountMyClientMapper;
    private final FilterUtility<TAccountMyClient> filterUtility;
    private final AccountPersonRoleTypeService accountPersonRoleTypeService;
    private final AccountTypeService accountTypeService;
    private final AccountStatusTypeService accountStatusTypeService;
    private final CurrencyService currencyService;

    public TAccountMyClientModelServiceImpl(TAccountMyClientDao tAccountMyClientDao, TAccountMyClientMapper tAccountMyClientMapper, AccountPersonRoleTypeService accountPersonRoleTypeService, AccountTypeService accountTypeService, AccountStatusTypeService accountStatusTypeService, CurrencyService currencyService) {
        this.tAccountMyClientDao = tAccountMyClientDao;
        this.filterUtility = new FilterUtility<>(tAccountMyClientDao);
        this.tAccountMyClientMapper = tAccountMyClientMapper;

        this.accountPersonRoleTypeService = accountPersonRoleTypeService;
        this.accountTypeService = accountTypeService;
        this.accountStatusTypeService = accountStatusTypeService;
        this.currencyService = currencyService;
    }

    @Override
    public TAccountMyClient getAccountMyClientById(Integer id, Integer clientType) {
        log.info("Inside TAccountMyClientModelService: getAccountMyClientById method");
        try {
            Optional<TAccountMyClient> resultOp = tAccountMyClientDao.findByAccountMyClientIdAndRClientType_ClientTypeIdAndIsActive(id, clientType, true);
            TAccountMyClient response = resultOp.orElse(null);
            return response;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getAccountMyClientById: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TAccountMyClientDto>>> getAllAccountMyClientDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer accountMyClientId) {
        log.info("Inside TAccountMyClientModelServiceImpl: getAllAccountMyClientDetails method");
        try {

            List<FilterCriteria<TAccountMyClient>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("accountMyClientId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (accountMyClientId != null && accountMyClientId != 0) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("accountMyClientId"), accountMyClientId)
                );
            }

            // pageable
            Page<TAccountMyClient> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TAccountMyClientDto> content = tAccountMyClientMapper.listToDto(pageableData.getContent());

            content.forEach(contentItem -> {
                if (contentItem.getAccountPersonRoleTypeId() != null){
                    ResponseEntity<RAccountPersonRoleTypeDto> RAccountPersonRoleType = accountPersonRoleTypeService.getAccountPersonRoleTypeById(contentItem.getAccountPersonRoleTypeId());
                    contentItem.setRAccountPersonRoleType(RAccountPersonRoleType.getBody());
                }

                if (contentItem.getAccountTypeId() != null){
                    ResponseEntity<RAccountTypeDto> RAccountType = accountTypeService.getAccountTypeById(contentItem.getAccountTypeId());
                    contentItem.setRAccountType(RAccountType.getBody());
                }

                if (contentItem.getAccountStatusTypeId() != null){
                    ResponseEntity<RAccountStatusTypeDto> RAccountStatusType = accountStatusTypeService.getAccountStatusTypeById(contentItem.getAccountStatusTypeId());
                    contentItem.setRAccountStatusType(RAccountStatusType.getBody());
                }

                if (contentItem.getCurrenciesId() != null){
                    ResponseEntity<RCurrencyDto> RCurrency = currencyService.getCurrencyById(contentItem.getCurrenciesId());
                    contentItem.setRCurrency(RCurrency.getBody());
                }

            });

            // response
            ApiResponseDto<List<TAccountMyClientDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
