package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.master.MultiPartyFundCodeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.master.MultiPartyFundCodeDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.parameter.MultiPartyFundsCodeMapper;
import com.olak.goaml.models.master.MultiPartyFundsCode;
import com.olak.goaml.service.reference.MultiPartyFundCodeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class MultiPartyFundCodeServiceImpl implements MultiPartyFundCodeService {

    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final MultiPartyFundCodeDao multiPartyFundCodeDao;
    private final MultiPartyFundsCodeMapper multiPartyFundsCodeMapper;

    public MultiPartyFundCodeServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, MultiPartyFundCodeDao multiPartyFundCodeDao, MultiPartyFundsCodeMapper multiPartyFundsCodeMapper) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.multiPartyFundCodeDao = multiPartyFundCodeDao;
        this.multiPartyFundsCodeMapper = multiPartyFundsCodeMapper;
    }

    @Override
    public ResponseEntity<List<MultiPartyFundCodeDto>> getAllMultiPartyFundCode() {
        log.info("Inside MultiPartyFundCodeService: getAllMultiPartyFundCode method");
        List<MultiPartyFundsCode> resultList = multiPartyFundCodeDao.findByIsActive(true);
        return new ResponseEntity<>(multiPartyFundsCodeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<MultiPartyFundCodeDto>>> getMultiPartyFundCodeWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside MultiPartyFundCodeService: getMultiPartyFundCodeWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(MultiPartyFundsCode.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<MultiPartyFundCodeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(multiPartyFundsCodeMapper.listToDto((List<MultiPartyFundsCode>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<MultiPartyFundCodeDto> addMultiPartyFundCode(MultiPartyFundCodeDto multiPartyFundCodeDto) {
        log.info("Inside MultiPartyFundCodeService: addMultiPartyFundCode method");

        try {
            if (multiPartyFundCodeDto.getMultiPartyFundsCodeName() == null || multiPartyFundCodeDto.getMultiPartyFundsCodeName().isEmpty())
                throw new BadRequestAlertException("Multi Party Fund Code name is required: ", ENTITY_NAME, "error");

            Optional<MultiPartyFundsCode> resultOp = multiPartyFundCodeDao.findByMultiPartyFundsCodeNameAndIsActive(multiPartyFundCodeDto.getMultiPartyFundsCodeName(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("Multi Party Fund Code already exists by Multi Party Fund Code name: " + multiPartyFundCodeDto.getMultiPartyFundsCodeName(), ENTITY_NAME, "error");

            if (multiPartyFundCodeDto.getMultiPartyFundsCodeDescription() == null || multiPartyFundCodeDto.getMultiPartyFundsCodeDescription().isEmpty())
                throw new BadRequestAlertException("Multi Party Fund Code Description is required: ", ENTITY_NAME, "error");

            Optional<MultiPartyFundsCode> resultOp1 = multiPartyFundCodeDao.findByMultiPartyFundsCodeDescriptionAndIsActive(multiPartyFundCodeDto.getMultiPartyFundsCodeDescription(), true);
            if (resultOp1.isPresent())
                throw new BadRequestAlertException("Multi Party Fund Code already exists by Multi Party Fund Code Description: " + multiPartyFundCodeDto.getMultiPartyFundsCodeDescription(), ENTITY_NAME, "error");

//            if (multiPartyFundCodeDto.getStatusId() == null)
                multiPartyFundCodeDto.setStatusId(1);

            multiPartyFundCodeDto.setIsActive(true);

//            if (!rStatusDao.existsById(multiPartyFundCodeDto.getStatusId()))
//                throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

            MultiPartyFundCodeDto saveDtoRes = multiPartyFundsCodeMapper.toDto(multiPartyFundCodeDao.save(multiPartyFundsCodeMapper.toEntity(multiPartyFundCodeDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add Category: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<MultiPartyFundCodeDto> getMultiPartyFundCodeById(Long id) {
        log.info("Inside MultiPartyFundCodeService: getMultiPartyFundCodeById method");

        try {
            Optional<MultiPartyFundsCode> resultOp = multiPartyFundCodeDao.findByMultiPartyFundsCodeIdAndIsActive(id, true);
            MultiPartyFundCodeDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Multi Party Fund Code not found by Multi Party Fund Cod ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = multiPartyFundsCodeMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get Category details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<MultiPartyFundCodeDto> updateMultiPartyFundCodeById(Long id, MultiPartyFundCodeDto multiPartyFundCodeDto) {
        log.info("Inside MultiPartyFundCodeService: updateMultiPartyFundCodeById method");

        // Validate
        if (id != multiPartyFundCodeDto.getMultiPartyFundsCodeId())
            throw new BadRequestAlertException("Multi Party Fund Code id mismatch.", ENTITY_NAME, "error");

        if (multiPartyFundCodeDto.getMultiPartyFundsCodeName() == null || multiPartyFundCodeDto.getMultiPartyFundsCodeName().isEmpty())
            throw new BadRequestAlertException("Multi Party Fund Code name is required: ", ENTITY_NAME, "error");

        Optional<MultiPartyFundsCode> resultOp = multiPartyFundCodeDao.findByMultiPartyFundsCodeNameAndIsActive(multiPartyFundCodeDto.getMultiPartyFundsCodeName(), true);
        if (resultOp.isPresent())
            throw new BadRequestAlertException("Multi Party Fund Code already exists by Multi Party Fund Code name: " + multiPartyFundCodeDto.getMultiPartyFundsCodeName(), ENTITY_NAME, "error");

        if (multiPartyFundCodeDto.getMultiPartyFundsCodeDescription() == null || multiPartyFundCodeDto.getMultiPartyFundsCodeDescription().isEmpty())
            throw new BadRequestAlertException("Multi Party Fund Code Description is required: ", ENTITY_NAME, "error");

        Optional<MultiPartyFundsCode> resultOp1 = multiPartyFundCodeDao.findByMultiPartyFundsCodeDescriptionAndIsActive(multiPartyFundCodeDto.getMultiPartyFundsCodeDescription(), true);
        if (resultOp1.isPresent())
            throw new BadRequestAlertException("Multi Party Fund Code already exists by Multi Party Fund Code Description: " + multiPartyFundCodeDto.getMultiPartyFundsCodeDescription(), ENTITY_NAME, "error");

//        if (multiPartyFundCodeDto.getStatusId() == null)
//            throw new BadRequestAlertException("Status Can Not Be Empty: ", ENTITY_NAME, "error");
//
//        if (!rStatusDao.existsById(multiPartyFundCodeDto.getStatusId()))
//            throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

        Optional<MultiPartyFundsCode> resultOp2 = multiPartyFundCodeDao.findByMultiPartyFundsCodeIdAndIsActive(id, true);
        if (!resultOp2.isPresent()) {
            throw new BadRequestAlertException("Multi Party Fund Code not found by Multi Party Fund Code ID: " + id, ENTITY_NAME, "error");
        } else {
            MultiPartyFundsCode existingRes = resultOp.get();
            multiPartyFundCodeDto.setIsActive(existingRes.getIsActive());
            multiPartyFundCodeDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                MultiPartyFundCodeDto responseDto = multiPartyFundsCodeMapper.toDto(multiPartyFundCodeDao.save(multiPartyFundsCodeMapper.toEntity(multiPartyFundCodeDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update Category by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<MultiPartyFundCodeDto> deleteMultiPartyFundCodeById(Long id) {
        log.info("Inside MultiPartyFundCodeService: deleteMultiPartyFundCodeById method");

        try {
            Optional<MultiPartyFundsCode> resultOp = multiPartyFundCodeDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Category not found by Category ID: " + id, ENTITY_NAME, "error");
            } else {
                MultiPartyFundsCode existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                MultiPartyFundCodeDto responseDto = multiPartyFundsCodeMapper.toDto(existingRes);
                responseDto = multiPartyFundsCodeMapper.toDto(multiPartyFundCodeDao.save(multiPartyFundsCodeMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete Category: {}", e.getMessage());
            throw new BadRequestAlertException("Category con not delete by Category ID: " + id, ENTITY_NAME, "error");
        }
    }
}
