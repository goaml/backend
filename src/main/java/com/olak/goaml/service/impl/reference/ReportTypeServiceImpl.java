package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.master.ReportTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.master.ReportTypeDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.parameter.ReportTypeMapper;
import com.olak.goaml.models.master.ReportType;
import com.olak.goaml.service.reference.ReportTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class ReportTypeServiceImpl implements ReportTypeService {

    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final ReportTypeDao reportTypeDao;
    private final ReportTypeMapper reportTypeMapper;

    public ReportTypeServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, ReportTypeDao reportTypeDao, ReportTypeMapper reportTypeMapper) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.reportTypeDao = reportTypeDao;
        this.reportTypeMapper = reportTypeMapper;
    }

    @Override
    public ResponseEntity<List<ReportTypeDto>> getAllReportType() {
        log.info("Inside ReportTypeService: getAllReportType method");
        List<ReportType> resultList = reportTypeDao.findByIsActive(true);
        return new ResponseEntity<>(reportTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<ReportTypeDto>>> getAllReportTypeWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside NatureTypeService: getNatureTypeWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(ReportType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<ReportTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(reportTypeMapper.listToDto((List<ReportType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ReportTypeDto> addReportType(ReportTypeDto reportTypeDto) {
        log.info("Inside ReportTypeService: addReportType method");

        try {
            if (reportTypeDto.getReportTypeName() == null || reportTypeDto.getReportTypeName().isEmpty())
                throw new BadRequestAlertException("ReportType name is required: ", ENTITY_NAME, "error");

            Optional<ReportType> resultOp = reportTypeDao.findByReportTypeNameAndIsActive(reportTypeDto.getReportTypeName(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("ReportType already exists by ReportType name: " + reportTypeDto.getReportTypeName(), ENTITY_NAME, "error");

            if (reportTypeDto.getReportTypeDescription() == null || reportTypeDto.getReportTypeDescription().isEmpty())
                throw new BadRequestAlertException("ReportType Description is required: ", ENTITY_NAME, "error");

            Optional<ReportType> resultOp1 = reportTypeDao.findByReportTypeDescriptionAndIsActive(reportTypeDto.getReportTypeDescription(), true);
            if (resultOp1.isPresent())
                throw new BadRequestAlertException("ReportType already exists by ReportType Description: " + reportTypeDto.getReportTypeDescription(), ENTITY_NAME, "error");

//            if (reportTypeDto.getStatusId() == null)
                reportTypeDto.setStatusId(1);

            reportTypeDto.setIsActive(true);

//            if (!rStatusDao.existsById(reportTypeDto.getStatusId()))
//                throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

            ReportTypeDto saveDtoRes = reportTypeMapper.toDto(reportTypeDao.save(reportTypeMapper.toEntity(reportTypeDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add ReportType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<ReportTypeDto> getReportTypeById(Long id) {
        log.info("Inside ReportTypeService: getReportTypeById method");

        try {
            Optional<ReportType> resultOp = reportTypeDao.findByReportTypeIdAndIsActive(id, true);
            ReportTypeDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("ReportType not found by ReportType ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = reportTypeMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get ReportType details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<ReportTypeDto> updateReportTypeById(Long id, ReportTypeDto reportTypeDto) {
        log.info("Inside ReportTypeService: updateReportTypeById method");

        // Validate
        if (id != reportTypeDto.getReportTypeId())
            throw new BadRequestAlertException("ReportType id mismatch.", ENTITY_NAME, "error");

        if (reportTypeDto.getReportTypeName() == null || reportTypeDto.getReportTypeName().isEmpty())
            throw new BadRequestAlertException("ReportType Name is required: ", ENTITY_NAME, "error");

        if (reportTypeDao.existsByReportTypeNameAndIsActiveAndReportTypeIdNot(reportTypeDto.getReportTypeName(), true, id))
            throw new BadRequestAlertException("ReportType Name is duplicate with another recode: ", ENTITY_NAME, "error");

//        if (reportTypeDto.getStatusId() == null)
//            throw new BadRequestAlertException("Status Can Not Be Empty: ", ENTITY_NAME, "error");
//
//        if (!rStatusDao.existsById(reportTypeDto.getStatusId()))
//            throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

        Optional<ReportType> resultOp = reportTypeDao.findByReportTypeIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("ReportType not found by ReportType ID: " + id, ENTITY_NAME, "error");
        } else {
            ReportType existingRes = resultOp.get();
            reportTypeDto.setIsActive(existingRes.getIsActive());
            reportTypeDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                ReportTypeDto responseDto = reportTypeMapper.toDto(reportTypeDao.save(reportTypeMapper.toEntity(reportTypeDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update ReportType by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<ReportTypeDto> deleteReportTypeById(Long id) {
        log.info("Inside ReportTypeService: deleteReportTypeById method");

        try {
            Optional<ReportType> resultOp = reportTypeDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("ReportType not found by ReportType ID: " + id, ENTITY_NAME, "error");
            } else {
                ReportType existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                ReportTypeDto responseDto = reportTypeMapper.toDto(existingRes);
                responseDto = reportTypeMapper.toDto(reportTypeDao.save(reportTypeMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete ReportType: {}", e.getMessage());
            throw new BadRequestAlertException("ReportType con not delete by ReportType ID: " + id, ENTITY_NAME, "error");
        }
    }
}
