package com.olak.goaml.service.impl.xml;

import com.olak.goaml.dto.xml.transaction.TFromDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.xml.transaction.TFrom;
import com.olak.goaml.models.xml.transaction.TFromMyClient;
import com.olak.goaml.service.xml.*;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.xmlDto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlFromTagRenderServiceImpl implements XmlFromTagRenderService {
    private final XmlFCurrencyTagRenderService xmlFCurrencyTagRenderService;
    private final XmlPersonTagRenderService xmlPersonTagRenderService;
    private final XmlAccountTagRenderService xmlAccountTagRenderService;
    private final XmlEntityTagRenderService xmlEntityTagRenderService;

    public XmlFromTagRenderServiceImpl(XmlFCurrencyTagRenderService xmlFCurrencyTagRenderService, XmlPersonTagRenderService xmlPersonTagRenderService, XmlAccountTagRenderService xmlAccountTagRenderService, XmlEntityTagRenderService xmlEntityTagRenderService) {
        this.xmlFCurrencyTagRenderService = xmlFCurrencyTagRenderService;
        this.xmlPersonTagRenderService = xmlPersonTagRenderService;
        this.xmlAccountTagRenderService = xmlAccountTagRenderService;
        this.xmlEntityTagRenderService = xmlEntityTagRenderService;
    }

    @Override
    public xmlTFromDto getRenderedTFrom(List<TFrom> fromList, String scenarioName) {

        try{
            xmlTFromDto xmlFromDto = new xmlTFromDto();
            if (fromList != null && !fromList.isEmpty()) {
                for (TFrom fromDto : fromList) {
//                TFromDto fromDto = new TFromDto();

                    xmlFromDto.setFromFundsCode(fromDto.getRFundsType().getFundTypeCode());
                    xmlFromDto.setFromFundsComment(fromDto.getFromFundsComment());

                    if (EnumTagVerifier.isTagValid("T_FROM_FROM_FOREIGN_CURRENCY", scenarioName)) {
                        if (fromDto.getFromForeignCurrencyId() != null)
                            xmlFromDto.setFromForeignCurrency(xmlFCurrencyTagRenderService.getRenderedFCurrency(fromDto.getFromForeignCurrencyId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_FROM_T_CONDUCTOR", scenarioName)) {
                        if (fromDto.getConductorPersonId() != null)
                            xmlFromDto.setTConductor(xmlPersonTagRenderService.getRenderedPerson(fromDto.getConductorPersonId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_FROM_FROM_ACCOUNT", scenarioName)) {
                        if (fromDto.getFromAccountId() != null)
                            xmlFromDto.setFromAccount(xmlAccountTagRenderService.getRenderedAccount(fromDto.getFromAccountId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_FROM_FROM_PERSON", scenarioName)) {
                        if (fromDto.getFromPersonId() != null)
                            xmlFromDto.setFromPerson(xmlPersonTagRenderService.getRenderedPerson(fromDto.getFromPersonId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_FROM_FROM_ENTITY", scenarioName)) {
                        if (fromDto.getFromEntityId() != null)
                            xmlFromDto.setFromEntity(xmlEntityTagRenderService.getRenderedEntity(fromDto.getFromEntityId(), scenarioName));
                    }

                    xmlFromDto.setFromCountry(fromDto.getRCountryCodes().getCountryCodeCode());
                }
            }

            return xmlFromDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedTFrom: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
