package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.reference.RCityListDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RCityListDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RCityListMapper;
import com.olak.goaml.models.reference.RCityList;
import com.olak.goaml.service.reference.CityListService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class CityListServiceImpl implements CityListService {

    private final RStatusDao rStatusDao;
    private final RCityListDao rCityListDao;
    private final RCityListMapper rCityListMapper;
    private final SearchNFilter searchNFilter;

    public CityListServiceImpl(RStatusDao rStatusDao, RCityListDao rCityListDao, RCityListMapper rCityListMapper, SearchNFilter searchNFilter) {
        this.rStatusDao = rStatusDao;
        this.rCityListDao = rCityListDao;
        this.rCityListMapper = rCityListMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RCityListDto>> getAllCityLists() {
        log.info("Inside CityListService: getAllCityLists method");
        List<RCityList> resultList = rCityListDao.findByIsActive(true);
        return new ResponseEntity<>(rCityListMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RCityListDto>>> getCityListsWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside CityListService: getCityListsWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RCityList.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RCityListDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rCityListMapper.listToDto((List<RCityList>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RCityListDto> addCityList(RCityListDto cityListDto) {
        log.info("Inside CityListService: addCityList method");

        try {
            if (cityListDto.getCityCode() == null || cityListDto.getCityCode().isEmpty()) {
                throw new BadRequestAlertException("City code is required.", ENTITY_NAME, "error");
            }

            Optional<RCityList> existingEntity = rCityListDao.findByCityCodeAndIsActive(cityListDto.getCityCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("CityList already exists with code: " + cityListDto.getCityCode(), ENTITY_NAME, "idexists");
            }

            if (cityListDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(cityListDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "idinvalid");
            }

            cityListDto.setIsActive(true);
            cityListDto.setStatusId(1);


            RCityList entity = rCityListMapper.toEntity(cityListDto);
            entity.setRStatus(rStatusDao.findById(cityListDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", "R_CITY_LIST", "idinvalid")));
            entity = rCityListDao.save(entity);

            return new ResponseEntity<>(rCityListMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding CityList: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RCityListDto> getCityListById(Integer id) {
        log.info("Inside CityListService: getCityListById method");

        try {
            Optional<RCityList> entity = rCityListDao.findByCityIdAndIsActive(id, true);
            RCityListDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("CityList not found by ID: " + id, "R_CITY_LIST", "idinvalid");
            } else {
                responseDto = rCityListMapper.toDto(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting CityList details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RCityListDto> updateCityListById(Integer id, RCityListDto cityListDto) {
        log.info("Inside CityListService: updateCityListById method");

        Optional<RCityList> existingEntity = rCityListDao.findByCityIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("Invalid city list ID", ENTITY_NAME, "idinvalid");
        }

        if (!id.equals(cityListDto.getCityId())) {
            throw new BadRequestAlertException("CityList id mismatch.", ENTITY_NAME, "error");
        }

        if (cityListDto.getCityCode() == null || cityListDto.getCityCode().isEmpty()) {
            throw new BadRequestAlertException("City Code is required.", ENTITY_NAME, "error");
        }

        if (rCityListDao.existsByCityCodeAndIsActiveAndCityIdNot(cityListDto.getCityCode(), true, id)) {
            throw new BadRequestAlertException("City Code is duplicate with another record.", ENTITY_NAME, "error");
        }

        if (cityListDto.getStatusId() == null) {
            throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
        }

        if (!rStatusDao.existsById(cityListDto.getStatusId())) {
            throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
        }

        RCityList entity = existingEntity.get();
        entity.setCityCode(cityListDto.getCityCode());
        entity.setDescription(cityListDto.getDescription());
        entity.setIsActive(cityListDto.getIsActive());
        entity.setRStatus(rStatusDao.findById(cityListDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", "R_CITY_LIST", "idinvalid")));

        entity = rCityListDao.save(entity);

        return new ResponseEntity<>(rCityListMapper.toDto(entity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RCityListDto> deleteCityListById(Integer id) {
        log.info("Inside CityListService: deleteCityListById method");

        try {
            Optional<RCityList> resultOp = rCityListDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("CityList not found by ID: " + id, ENTITY_NAME, "error");
            } else {
                RCityList existingEntity = resultOp.get();

                existingEntity.setIsActive(false);
                RCityListDto responseDto = rCityListMapper.toDto(rCityListDao.save(existingEntity));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting CityList: {}", e.getMessage());
            throw new BadRequestAlertException("CityList could not be deleted by ID: " + id, ENTITY_NAME, "error");
        }
    }

}

