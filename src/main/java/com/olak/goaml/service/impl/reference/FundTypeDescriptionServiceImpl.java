package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.master.FundTypeDescriptionDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.master.FundTypeDescriptionDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.parameter.FundTypeDescriptionMapper;
import com.olak.goaml.models.master.FundTypeDescription;
import com.olak.goaml.service.reference.FundTypeDescriptionService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class FundTypeDescriptionServiceImpl implements FundTypeDescriptionService {

    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final FundTypeDescriptionDao fundTypeDescriptionDao;
    private final FundTypeDescriptionMapper fundTypeDescriptionMapper;

    public FundTypeDescriptionServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, FundTypeDescriptionDao fundTypeDescriptionDao, FundTypeDescriptionMapper fundTypeDescriptionMapper) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.fundTypeDescriptionDao = fundTypeDescriptionDao;
        this.fundTypeDescriptionMapper = fundTypeDescriptionMapper;
    }

    @Override
    public ResponseEntity<List<FundTypeDescriptionDto>> getAllFundTypeDescription() {
        log.info("Inside FundTypeDescriptionService: getAllFundTypeDescription method");
        List<FundTypeDescription> resultList = fundTypeDescriptionDao.findByIsActive(true);
        return new ResponseEntity<>(fundTypeDescriptionMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<FundTypeDescriptionDto>>> getFundTypeDescriptionWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside FundTypeDescriptionService: getFundTypeDescriptionWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(FundTypeDescription.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<FundTypeDescriptionDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(fundTypeDescriptionMapper.listToDto((List<FundTypeDescription>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<FundTypeDescriptionDto> addFundTypeDescription(FundTypeDescriptionDto fundTypeDescriptionDto) {
        log.info("Inside FundTypeDescriptionService: addFundTypeDescription method");

        try {
            if (fundTypeDescriptionDto.getFundTypeDescription() == null || fundTypeDescriptionDto.getFundTypeDescription().isEmpty())
                throw new BadRequestAlertException("Fund Type Description is required: ", ENTITY_NAME, "error");

            Optional<FundTypeDescription> resultOp = fundTypeDescriptionDao.findByFundTypeDescriptionAndIsActive(fundTypeDescriptionDto.getFundTypeDescription(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("Fund Type Description already exists by Fund Type Description: " + fundTypeDescriptionDto.getFundTypeDescription(), ENTITY_NAME, "error");


//            if (fundTypeDescriptionDto.getStatusId() == null)
                fundTypeDescriptionDto.setStatusId(1);

            fundTypeDescriptionDto.setIsActive(true);

//            if (!rStatusDao.existsById(fundTypeDescriptionDto.getStatusId()))
//                throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

            FundTypeDescriptionDto saveDtoRes = fundTypeDescriptionMapper.toDto(fundTypeDescriptionDao.save(fundTypeDescriptionMapper.toEntity(fundTypeDescriptionDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add Fund Type Description: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<FundTypeDescriptionDto> getFundTypeDescriptionById(Long id) {
        log.info("Inside FundTypeDescriptionService: getFundTypeDescriptionById method");

        try {
            Optional<FundTypeDescription> resultOp = fundTypeDescriptionDao.findByFundTypeDescriptionIdAndIsActive(id, true);
            FundTypeDescriptionDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Fund Type Description not found by Fund Type Description ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = fundTypeDescriptionMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get Category details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<FundTypeDescriptionDto> updateFundTypeDescriptionById(Long id, FundTypeDescriptionDto fundTypeDescriptionDto) {
        log.info("Inside FundTypeDescriptionService: updateFundTypeDescriptionById method");

        // Validate
        if (id != fundTypeDescriptionDto.getFundTypeDescriptionId())
            throw new BadRequestAlertException("Fund Type Description id mismatch.", ENTITY_NAME, "error");

        if (fundTypeDescriptionDto.getFundTypeDescription() == null || fundTypeDescriptionDto.getFundTypeDescription().isEmpty())
            throw new BadRequestAlertException("Fund Type Description is required: ", ENTITY_NAME, "error");

        if (fundTypeDescriptionDao.existsByFundTypeDescriptionAndIsActiveAndFundTypeDescriptionIdNot(fundTypeDescriptionDto.getFundTypeDescription(), true, id))
            throw new BadRequestAlertException("Fund Type Description is duplicate with another recode: ", ENTITY_NAME, "error");

//        if (fundTypeDescriptionDto.getStatusId() == null)
//            throw new BadRequestAlertException("Status Can Not Be Empty: ", ENTITY_NAME, "error");
//
//        if (!rStatusDao.existsById(fundTypeDescriptionDto.getStatusId()))
//            throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

        Optional<FundTypeDescription> resultOp = fundTypeDescriptionDao.findByFundTypeDescriptionIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("Fund Type Description not found by Fund Type Description ID: " + id, ENTITY_NAME, "error");
        } else {
            FundTypeDescription existingRes = resultOp.get();
            fundTypeDescriptionDto.setIsActive(existingRes.getIsActive());
            fundTypeDescriptionDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                FundTypeDescriptionDto responseDto = fundTypeDescriptionMapper.toDto(fundTypeDescriptionDao.save(fundTypeDescriptionMapper.toEntity(fundTypeDescriptionDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update Fund Type Description by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<FundTypeDescriptionDto> deleteFundTypeDescriptionById(Long id) {
        log.info("Inside FundTypeDescriptionService: deleteFundTypeDescriptionById method");

        try {
            Optional<FundTypeDescription> resultOp = fundTypeDescriptionDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Fund Type Description not found by Fund Type Description ID: " + id, ENTITY_NAME, "error");
            } else {
                FundTypeDescription existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                FundTypeDescriptionDto responseDto = fundTypeDescriptionMapper.toDto(existingRes);
                responseDto = fundTypeDescriptionMapper.toDto(fundTypeDescriptionDao.save(fundTypeDescriptionMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete Fund Type Description: {}", e.getMessage());
            throw new BadRequestAlertException("Fund Type Description con not delete by Fund Type Description ID: " + id, ENTITY_NAME, "error");
        }
    }
}
