package com.olak.goaml.service.impl.reference;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dao.reference.RSubmissionTypeDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RSubmissionTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RSubmissionTypeMapper;
import com.olak.goaml.models.reference.RSubmissionType;
import com.olak.goaml.service.reference.SubmissionTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SubmissionTypeServiceImpl implements SubmissionTypeService {

    private final RStatusDao rStatusDao;
    private final RSubmissionTypeDao rSubmissionTypeDao;
    private final RSubmissionTypeMapper rSubmissionTypeMapper;
    private final SearchNFilter searchNFilter;

    public SubmissionTypeServiceImpl(RStatusDao rStatusDao, RSubmissionTypeDao rSubmissionTypeDao, RSubmissionTypeMapper rSubmissionTypeMapper, SearchNFilter searchNFilter){
        this.rStatusDao = rStatusDao;
        this.rSubmissionTypeDao = rSubmissionTypeDao;
        this.rSubmissionTypeMapper = rSubmissionTypeMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RSubmissionTypeDto>> getAllSubmissionTypes() {
        log.info("Inside RSubmissionTypeService: getAllSubmissionTypes method");
        List<RSubmissionType> resultList = rSubmissionTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rSubmissionTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RSubmissionTypeDto>>> getSubmissionTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside RSubmissionTypeService: getSubmissionTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RSubmissionType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RSubmissionTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rSubmissionTypeMapper.listToDto((List<RSubmissionType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RSubmissionTypeDto> addSubmissionType(RSubmissionTypeDto submissionTypeDto) {
        log.info("Inside RSubmissionTypeService: addSubmissionType method");

        try {

            if (submissionTypeDto.getSubmissionTypeCode() == null || submissionTypeDto.getSubmissionTypeCode().isEmpty()) {
                throw new BadRequestAlertException("Submission type code is required.", ENTITY_NAME, "error");
            }


            Optional<RSubmissionType> existingEntity = rSubmissionTypeDao.findBySubmissionTypeCodeAndIsActive(submissionTypeDto.getSubmissionTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("A new submission type cannot already have an existing code", ENTITY_NAME, "idexists");
            }


            if (submissionTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(submissionTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "idinvalid");
            }


            submissionTypeDto.setIsActive(true);
            submissionTypeDto.setStatusId(1);


            RSubmissionType entity = rSubmissionTypeMapper.toEntity(submissionTypeDto);
            entity.setRStatus(rStatusDao.findById(submissionTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "idinvalid")));


            entity = rSubmissionTypeDao.save(entity);


            return new ResponseEntity<>(rSubmissionTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding SubmissionType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RSubmissionTypeDto> getSubmissionTypeById(Integer id) {
        log.info("Inside RSubmissionTypeService: getSubmissionTypeById method");
        Optional<RSubmissionType> entity = rSubmissionTypeDao.findBySubmissionTypeIdAndIsActive(id, true);
        return new ResponseEntity<>(rSubmissionTypeMapper.toDto(entity.orElseThrow(() -> new BadRequestAlertException("Invalid submission type ID", ENTITY_NAME, "idinvalid"))), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RSubmissionTypeDto> updateSubmissionTypeById(Integer id, RSubmissionTypeDto submissionTypeDto) {
        log.info("Inside RSubmissionTypeService: updateSubmissionTypeById method");
        Optional<RSubmissionType> existingEntity = rSubmissionTypeDao.findBySubmissionTypeIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("Invalid submission type ID", ENTITY_NAME, "idinvalid");
        }
        if (rSubmissionTypeDao.existsBySubmissionTypeCodeAndIsActiveAndSubmissionTypeIdNot(submissionTypeDto.getSubmissionTypeCode(), true, id)) {
            throw new BadRequestAlertException("The submission type code already exists for a different entity", ENTITY_NAME, "idexists");
        }
        RSubmissionType entity = existingEntity.get();
        entity.setSubmissionTypeCode(submissionTypeDto.getSubmissionTypeCode());
        entity.setDescription(submissionTypeDto.getDescription());
        entity.setIsActive(submissionTypeDto.getIsActive());
        entity.setRStatus(rStatusDao.findById(submissionTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "idinvalid")));
        entity = rSubmissionTypeDao.save(entity);
        return new ResponseEntity<>(rSubmissionTypeMapper.toDto(entity), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RSubmissionTypeDto> deleteSubmissionTypeById(Integer id) {
        log.info("Inside RSubmissionTypeService: deleteSubmissionTypeById method");
        Optional<RSubmissionType> existingEntity = rSubmissionTypeDao.findBySubmissionTypeIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("Invalid submission type ID", ENTITY_NAME, "idinvalid");
        }
        RSubmissionType entity = existingEntity.get();
        entity.setIsActive(false);
        entity = rSubmissionTypeDao.save(entity);
        return new ResponseEntity<>(rSubmissionTypeMapper.toDto(entity), HttpStatus.OK);
    }
    
}