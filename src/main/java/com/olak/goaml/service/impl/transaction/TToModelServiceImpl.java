package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TFromDao;
import com.olak.goaml.dao.xml.transaction.TToDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.xml.transaction.TFromDto;
import com.olak.goaml.dto.xml.transaction.TToDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TFromMapper;
import com.olak.goaml.mapper.xml.transaction.TToMapper;
import com.olak.goaml.models.xml.transaction.TFrom;
import com.olak.goaml.models.xml.transaction.TTo;
import com.olak.goaml.service.transaction.TToModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TToModelServiceImpl implements TToModelService {

    private final TToDao tToDao;
    private final TToMapper tToMapper;
    private final FilterUtility<TTo> filterUtility;
    public TToModelServiceImpl(TToDao tToDao, TToMapper tToMapper) {
        this.tToDao = tToDao;
        this.filterUtility = new FilterUtility<>(tToDao);
        this.tToMapper = tToMapper;
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TToDto>>> getAllToDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer trxId) {
        log.info("Inside TToModelServiceImpl: getAllToDetails method");
        try {

            List<FilterCriteria<TTo>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("toId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (trxId != null && trxId != 0) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("TTransaction").get("transactionId"), trxId)
                );
            }

            // pageable
            Page<TTo> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TToDto> content = tToMapper.listToDto(pageableData.getContent());

            // response
            ApiResponseDto<List<TToDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
