package com.olak.goaml.service.impl.xml;

import com.olak.goaml.constants.ClientType;
import com.olak.goaml.dao.reference.RCountryCodesDao;
import com.olak.goaml.dao.reference.REntityLegalFormTypeDao;
import com.olak.goaml.dto.xml.transaction.TEntityMyClientDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.reference.RCountryCodes;
import com.olak.goaml.models.reference.REntityLegalFormType;
import com.olak.goaml.models.xml.transaction.TEntityMyClient;
import com.olak.goaml.service.transaction.TEntityMyClientModelService;
import com.olak.goaml.service.xml.*;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTEntityDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlEntityTagRenderServiceImpl implements XmlEntityTagRenderService {

    private final XMLAddressTagRenderService xmlAddressTagRenderService;
    private final XMLPhoneTagRenderService xmlPhoneTagRenderService;
    private final XmlDirectorTagRenderService xmlDirectorTagRenderService;
    private final TEntityMyClientModelService tEntityMyClientModelService;
    private final REntityLegalFormTypeDao rEntityLegalFormTypeDao;
    private final RCountryCodesDao rCountryCodesDao;

    public XmlEntityTagRenderServiceImpl(XMLAddressTagRenderService xmlAddressTagRenderService, XMLPhoneTagRenderService xmlPhoneTagRenderService, XmlDirectorTagRenderService xmlDirectorTagRenderService, TEntityMyClientModelService tEntityMyClientModelService, REntityLegalFormTypeDao rEntityLegalFormTypeDao, RCountryCodesDao rCountryCodesDao) {
        this.xmlAddressTagRenderService = xmlAddressTagRenderService;
        this.xmlPhoneTagRenderService = xmlPhoneTagRenderService;
        this.xmlDirectorTagRenderService = xmlDirectorTagRenderService;
        this.tEntityMyClientModelService = tEntityMyClientModelService;
        this.rEntityLegalFormTypeDao = rEntityLegalFormTypeDao;
        this.rCountryCodesDao = rCountryCodesDao;
    }

    @Override
    public xmlTEntityDto getRenderedEntity(Integer id, String scenarioName) {

        try {
            TEntityMyClient entityMyClientDto = tEntityMyClientModelService.getEntityMyClientById(id, ClientType.CLIENT);
            xmlTEntityDto xmlTEntityDto = new xmlTEntityDto();

            if (entityMyClientDto != null) {
                xmlTEntityDto.setName(entityMyClientDto.getName());
                xmlTEntityDto.setCommercialName(entityMyClientDto.getCommercialName());

                if (entityMyClientDto.getEntityLegalFormTypeId() != null) {
                    Optional<REntityLegalFormType> rEntityLegalFormTypeOp = rEntityLegalFormTypeDao.findByEntityLegalFormTypeIdAndIsActive(entityMyClientDto.getEntityLegalFormTypeId(), true);
                    rEntityLegalFormTypeOp.ifPresent(rEntityLegalFormType -> xmlTEntityDto.setIncorporationLegalForm(rEntityLegalFormType.getEntityLegalFormTypeCode()));
                }

                xmlTEntityDto.setIncorporationNumber(entityMyClientDto.getIncorporationNumber());
                xmlTEntityDto.setBusiness(entityMyClientDto.getBusiness());

                if (EnumTagVerifier.isTagValid("ENTITY_PHONES_PHONE", scenarioName)) {
                    if (entityMyClientDto.getPhoneId() != null)
                        xmlTEntityDto.setPhones(xmlPhoneTagRenderService.getRenderedPhoneList(entityMyClientDto.getPhoneId(), scenarioName));
                }

                if (EnumTagVerifier.isTagValid("ENTITY_ADDRESSES_ADDRESS", scenarioName)) {
                    if (entityMyClientDto.getAddressId() != null)
                        xmlTEntityDto.setAddresses(xmlAddressTagRenderService.getRenderedAddressList(entityMyClientDto.getAddressId(), scenarioName));
                }

                xmlTEntityDto.setEmail(entityMyClientDto.getEmail());
                xmlTEntityDto.setUrl(entityMyClientDto.getUrl());
                xmlTEntityDto.setIncorporationState(entityMyClientDto.getIncorporationState());

                if (entityMyClientDto.getCountryCodeId() != null) {
                    Optional<RCountryCodes> rCountryCodesOp = rCountryCodesDao.findByCountryCodeIdAndIsActive(entityMyClientDto.getCountryCodeId(), true);
                    rCountryCodesOp.ifPresent(rCountryCodes -> xmlTEntityDto.setIncorporationCountryCode(rCountryCodes.getCountryCodeCode()));
                }

                if (EnumTagVerifier.isTagValid("ENTITY_DIRECTOR_ID", scenarioName)) {
                    if (entityMyClientDto.getDirectorPersonId() != null)
                        xmlTEntityDto.setDirectorId(xmlDirectorTagRenderService.getRenderedDirectorList(entityMyClientDto.getDirectorPersonId(), scenarioName));
                }


                if (entityMyClientDto.getIncorporationDate() != null) {
                    Date incorporationDate = entityMyClientDto.getIncorporationDate();
                    xmlTEntityDto.setIncorporationDate(XmlUtils.convertDateToLocalDate(incorporationDate).atTime(0, 0, 0));
                }

                if (entityMyClientDto.getBusinessClosed()) {
                    xmlTEntityDto.setBusinessClosed(entityMyClientDto.getBusinessClosed());

                    if (entityMyClientDto.getDateBusinessClosed() != null) {
                        Date dateBusinessClosed = entityMyClientDto.getDateBusinessClosed();
                        xmlTEntityDto.setDateBusinessClosed(XmlUtils.convertDateToLocalDate(dateBusinessClosed).atTime(0, 0, 0));
                    }

                }

                xmlTEntityDto.setTaxNumber(entityMyClientDto.getTaxNumber());
                xmlTEntityDto.setTaxRegNumber(entityMyClientDto.getTaxRegNumber());
                xmlTEntityDto.setComments(entityMyClientDto.getComments());
            }

            return xmlTEntityDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedEntity: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
