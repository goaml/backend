package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.master.MultiPartyInvolvePartyDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.master.MultiPartyInvolvePartyDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.parameter.MultiPartyInvolvePartyMapper;
import com.olak.goaml.models.master.MultiPartyInvolveParty;
import com.olak.goaml.service.reference.MultiPartyInvolvePartyService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class MultiPartyInvolvePartyServiceImpl implements MultiPartyInvolvePartyService {

    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final MultiPartyInvolvePartyDao multiPartyInvolvePartyDao;
    private final MultiPartyInvolvePartyMapper multiPartyInvolvePartyMapper;

    public MultiPartyInvolvePartyServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, MultiPartyInvolvePartyDao multiPartyInvolvePartyDao, MultiPartyInvolvePartyMapper multiPartyInvolvePartyMapper) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.multiPartyInvolvePartyDao = multiPartyInvolvePartyDao;
        this.multiPartyInvolvePartyMapper = multiPartyInvolvePartyMapper;
    }

    @Override
    public ResponseEntity<List<MultiPartyInvolvePartyDto>> getAllMultiPartyInvolveParty() {
        log.info("Inside MultiPartyInvolvePartyService: getAllMultiPartyInvolveParty method");
        List<MultiPartyInvolveParty> resultList = multiPartyInvolvePartyDao.findByIsActive(true);
        return new ResponseEntity<>(multiPartyInvolvePartyMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<MultiPartyInvolvePartyDto>>> getMultiPartyInvolvePartyWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside MultiPartyInvolvePartyService: getMultiPartyInvolvePartyWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(MultiPartyInvolveParty.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<MultiPartyInvolvePartyDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(multiPartyInvolvePartyMapper.listToDto((List<MultiPartyInvolveParty>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<MultiPartyInvolvePartyDto> addMultiPartyInvolveParty(MultiPartyInvolvePartyDto multiPartyInvolvePartyDto) {
        log.info("Inside MultiPartyInvolvePartyService: addMultiPartyInvolveParty method");

        try {
            if (multiPartyInvolvePartyDto.getMultiPartyInvolvePartyName() == null || multiPartyInvolvePartyDto.getMultiPartyInvolvePartyName().isEmpty())
                throw new BadRequestAlertException("Multi Party Involve Party Name is required: ", ENTITY_NAME, "error");

            Optional<MultiPartyInvolveParty> resultOp = multiPartyInvolvePartyDao.findByMultiPartyInvolvePartyNameAndIsActive(multiPartyInvolvePartyDto.getMultiPartyInvolvePartyName(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("Multi Party Involve Party Name already exists by Multi Party Involve Party Name name: " + multiPartyInvolvePartyDto.getMultiPartyInvolvePartyName(), ENTITY_NAME, "error");

            if (multiPartyInvolvePartyDto.getMultiPartyInvolvePartyDescription() == null || multiPartyInvolvePartyDto.getMultiPartyInvolvePartyDescription().isEmpty())
                throw new BadRequestAlertException("Multi Party Involve Party Description is required: ", ENTITY_NAME, "error");

            Optional<MultiPartyInvolveParty> resultOp1 = multiPartyInvolvePartyDao.findByMultiPartyInvolvePartyDescriptionAndIsActive(multiPartyInvolvePartyDto.getMultiPartyInvolvePartyDescription(), true);
            if (resultOp1.isPresent())
                throw new BadRequestAlertException("Multi Party Involve Party already exists by Multi Party Involve Party Description: " + multiPartyInvolvePartyDto.getMultiPartyInvolvePartyDescription(), ENTITY_NAME, "error");

//            if (multiPartyInvolvePartyDto.getStatusId() == null)
                multiPartyInvolvePartyDto.setStatusId(1);

            multiPartyInvolvePartyDto.setIsActive(true);

//            if (!rStatusDao.existsById(multiPartyInvolvePartyDto.getStatusId()))
//                throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

            MultiPartyInvolvePartyDto saveDtoRes = multiPartyInvolvePartyMapper.toDto(multiPartyInvolvePartyDao.save(multiPartyInvolvePartyMapper.toEntity(multiPartyInvolvePartyDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add Multi Party Involve Party: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<MultiPartyInvolvePartyDto> getMultiPartyInvolvePartyById(Long id) {
        log.info("Inside MultiPartyInvolvePartyService: getMultiPartyInvolvePartyById method");

        try {
            Optional<MultiPartyInvolveParty> resultOp = multiPartyInvolvePartyDao.findByMultiPartyInvolvePartyIdAndIsActive(id, true);
            MultiPartyInvolvePartyDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Multi Party Involve Party not found by Multi Party Involve Party ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = multiPartyInvolvePartyMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get Multi Party Involve Party details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<MultiPartyInvolvePartyDto> updateMultiPartyInvolvePartyById(Long id, MultiPartyInvolvePartyDto multiPartyInvolvePartyDto) {
        log.info("Inside MultiPartyInvolvePartyService: updateMultiPartyInvolvePartyById method");

        // Validate
        if (id != multiPartyInvolvePartyDto.getMultiPartyInvolvePartyId())
            throw new BadRequestAlertException("Multi Party Involve Party id mismatch.", ENTITY_NAME, "error");

        if (multiPartyInvolvePartyDto.getMultiPartyInvolvePartyName() == null || multiPartyInvolvePartyDto.getMultiPartyInvolvePartyName().isEmpty())
            throw new BadRequestAlertException("Multi Party Involve Party Name is required: ", ENTITY_NAME, "error");

        if (multiPartyInvolvePartyDao.existsByMultiPartyInvolvePartyNameAndIsActiveAndMultiPartyInvolvePartyIdNot(multiPartyInvolvePartyDto.getMultiPartyInvolvePartyName(), true, id))
            throw new BadRequestAlertException("Multi Party Involve Party Name is duplicate with another recode: ", ENTITY_NAME, "error");

        if (multiPartyInvolvePartyDto.getMultiPartyInvolvePartyDescription() == null || multiPartyInvolvePartyDto.getMultiPartyInvolvePartyDescription().isEmpty())
            throw new BadRequestAlertException("Multi Party Involve Party Description is required: ", ENTITY_NAME, "error");

        if (multiPartyInvolvePartyDao.existsByMultiPartyInvolvePartyDescriptionAndIsActiveAndMultiPartyInvolvePartyIdNot(multiPartyInvolvePartyDto.getMultiPartyInvolvePartyDescription(), true, id))
            throw new BadRequestAlertException("Multi Party Involve Party Description is duplicate with another recode: ", ENTITY_NAME, "error");


//        if (multiPartyInvolvePartyDto.getStatusId() == null)
//            throw new BadRequestAlertException("Status Can Not Be Empty: ", ENTITY_NAME, "error");
//
//        if (!rStatusDao.existsById(multiPartyInvolvePartyDto.getStatusId()))
//            throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

        Optional<MultiPartyInvolveParty> resultOp = multiPartyInvolvePartyDao.findByMultiPartyInvolvePartyIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("Multi Party Involve Party not found by Multi Party Involve Party ID: " + id, ENTITY_NAME, "error");
        } else {
            MultiPartyInvolveParty existingRes = resultOp.get();
            multiPartyInvolvePartyDto.setIsActive(existingRes.getIsActive());
            multiPartyInvolvePartyDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                MultiPartyInvolvePartyDto responseDto = multiPartyInvolvePartyMapper.toDto(multiPartyInvolvePartyDao.save(multiPartyInvolvePartyMapper.toEntity(multiPartyInvolvePartyDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update Multi Party Involve Party by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<MultiPartyInvolvePartyDto> deleteMultiPartyInvolvePartyById(Long id) {
        log.info("Inside MultiPartyInvolvePartyService: deleteMultiPartyInvolvePartyById method");

        try {
            Optional<MultiPartyInvolveParty> resultOp = multiPartyInvolvePartyDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Multi Party Involve Party not found by Multi Party Involve Party ID: " + id, ENTITY_NAME, "error");
            } else {
                MultiPartyInvolveParty existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                MultiPartyInvolvePartyDto responseDto = multiPartyInvolvePartyMapper.toDto(existingRes);
                responseDto = multiPartyInvolvePartyMapper.toDto(multiPartyInvolvePartyDao.save(multiPartyInvolvePartyMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete Multi Party Involve Party: {}", e.getMessage());
            throw new BadRequestAlertException("Multi Party Involve Party con not delete by Multi Party Involve Party ID: " + id, ENTITY_NAME, "error");
        }
    }
}
