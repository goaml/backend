package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TFromDao;
import com.olak.goaml.dao.xml.transaction.TReportDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TFromMapper;
import com.olak.goaml.mapper.xml.transaction.TReportMapper;
import com.olak.goaml.mapper.xml.transaction.TTransactionMapper;
import com.olak.goaml.models.xml.transaction.TReport;
import com.olak.goaml.service.transaction.TReportModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TReportModelServiceImpl implements TReportModelService {
    private final TReportDao tReportDao;
    private final TReportMapper tReportMapper;
    private final TTransactionMapper tTransactionMapper;
    private final TFromMapper tFromMapper;
    private final TFromDao tFromDao;

    private final FilterUtility<TReport> filterUtility;

    public TReportModelServiceImpl(TReportDao tReportDao, TReportMapper tReportMapper, TTransactionMapper tTransactionMapper, TFromMapper tFromMapper, TFromDao tFromDao) {
        this.tReportDao = tReportDao;
        this.tReportMapper = tReportMapper;
        this.tTransactionMapper = tTransactionMapper;
        this.tFromMapper = tFromMapper;
        this.tFromDao = tFromDao;
        this.filterUtility = new FilterUtility<>(tReportDao);
    }

    @Override
    public TReport getReportDetailsByReportCode(String reportCode) {
        log.info("Inside TReportModelService: getReportDetailsByReportCode method");
        try {
//            Optional<TReport> resultOp = tReportDao.findByReportCodeAndIsActiveAndTTransactions_IsActive(reportCode, true, true);
            Optional<TReport> resultOp = tReportDao.findByReportCodeAndIsActive(reportCode, true);
            System.out.println("Transaction Count"+ resultOp.get().getTTransactions().size());
            TReport response = null;
            TReportDto responseDto = null;
            if (resultOp.isPresent()) {
                response = resultOp.get();
            }

            return response;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while ReportDetailsByReportCode: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TReportDto>>> getAllReportDetails(Integer page, Integer perPage, String search, String sort, String direction, String reportCode) {
        log.info("Inside TReportModelService: getAllReportDetails method");
        try {

            List<FilterCriteria<TReport>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("reportId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (reportCode != null && !reportCode.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("reportCode"), reportCode)
                );
            }

            // pageable
            Page<TReport> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TReportDto> content = tReportMapper.listToDto(pageableData.getContent());

            // response
            ApiResponseDto<List<TReportDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
