package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TAddressDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.xml.transaction.TAddressDto;
import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TAddressMapper;
import com.olak.goaml.mapper.xml.transaction.TTransactionMapper;
import com.olak.goaml.models.xml.transaction.TAddress;
import com.olak.goaml.models.xml.transaction.TForeignCurrency;
import com.olak.goaml.models.xml.transaction.TPersonMyClient;
import com.olak.goaml.models.xml.transaction.TTransaction;
import com.olak.goaml.service.transaction.TAddressModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TAddressModelServiceImpl implements TAddressModelService {

    private final TAddressDao tAddressDao;
    private final TAddressMapper tAddressMapper;
    private final FilterUtility<TAddress> filterUtility;

    public TAddressModelServiceImpl(TAddressDao tAddressDao,TAddressMapper tAddressMapper) {

        this.tAddressDao = tAddressDao;
        this.filterUtility = new FilterUtility<>(tAddressDao);
        this.tAddressMapper = tAddressMapper;
    }

    @Override
    public TAddress getAddressId(Integer id) {
        log.info("Inside TAddressModelService: getAddressId method");
        try {
            Optional<TAddress> resultOp = tAddressDao.findByAddressIdAndIsActive(id, true);
            TAddress response = resultOp.orElse(null);
            return response;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getAddressId: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public List<TAddress> getAddressListById(Integer id) {
        log.info("Inside TAddressModelService: getAddressListById method");
        try {
            List<TAddress> addressList = new ArrayList<>();
            Optional<TAddress> resultOp = tAddressDao.findByAddressIdAndIsActive(id, true);
            if (resultOp.isPresent()) {
                TAddress address = resultOp.get();
                addressList.add(address);
            }
            return addressList;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getAddressListById: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TAddressDto>>> getAllAddressDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer addressId) {
        log.info("Inside TAddressModelServiceImpl: getAllAddressDetails method");
        try {

            List<FilterCriteria<TAddress>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("addressId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (addressId != null && addressId != 0) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("addressId"), addressId)
                );
            }

            // pageable
            Page<TAddress> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TAddressDto> content = tAddressMapper.listToDto(pageableData.getContent());

            // response
            ApiResponseDto<List<TAddressDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
