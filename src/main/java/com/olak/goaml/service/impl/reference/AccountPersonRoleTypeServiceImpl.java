package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.reference.RAccountPersonRoleTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RAccountPersonRoleTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RAccountPersonRoleTypeMapper;
import com.olak.goaml.models.reference.RAccountPersonRoleType;
import com.olak.goaml.service.reference.AccountPersonRoleTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class AccountPersonRoleTypeServiceImpl implements AccountPersonRoleTypeService {

    private final RStatusDao rStatusDao;
    private final RAccountPersonRoleTypeDao rAccountPersonRoleTypeDao;
    private final RAccountPersonRoleTypeMapper rAccountPersonRoleTypeMapper;
    private final SearchNFilter searchNFilter;

    public AccountPersonRoleTypeServiceImpl(RStatusDao rStatusDao, RAccountPersonRoleTypeDao rAccountPersonRoleTypeDao, RAccountPersonRoleTypeMapper rAccountPersonRoleTypeMapper, SearchNFilter searchNFilter) {
        this.rStatusDao = rStatusDao;
        this.rAccountPersonRoleTypeDao = rAccountPersonRoleTypeDao;
        this.rAccountPersonRoleTypeMapper = rAccountPersonRoleTypeMapper;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<List<RAccountPersonRoleTypeDto>> getAllAccountPersonRoleTypes() {
        log.info("Inside AccountPersonRoleTypeService: getAllAccountPersonRoleTypes method");
        List<RAccountPersonRoleType> resultList = rAccountPersonRoleTypeDao.findByIsActive(true);
        return new ResponseEntity<>(rAccountPersonRoleTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RAccountPersonRoleTypeDto>>> getAccountPersonRoleTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside AccountPersonRoleTypeService: getAccountPersonRoleTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RAccountPersonRoleType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RAccountPersonRoleTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rAccountPersonRoleTypeMapper.listToDto((List<RAccountPersonRoleType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RAccountPersonRoleTypeDto> addAccountPersonRoleType(RAccountPersonRoleTypeDto accountPersonRoleTypeDto) {
        log.info("Inside AccountPersonRoleTypeService: addAccountPersonRoleType method");

        try {
            if (accountPersonRoleTypeDto.getAccountPersonRoleTypeCode() == null || accountPersonRoleTypeDto.getAccountPersonRoleTypeCode().isEmpty()) {
                throw new BadRequestAlertException("AccountPersonRoleType code is required.", ENTITY_NAME, "error");
            }

            Optional<RAccountPersonRoleType> existingEntity = rAccountPersonRoleTypeDao.findByAccountPersonRoleTypeCodeAndIsActive(accountPersonRoleTypeDto.getAccountPersonRoleTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("AccountPersonRoleType already exists with code: " + accountPersonRoleTypeDto.getAccountPersonRoleTypeCode(), ENTITY_NAME, "idexists");
            }

            if (accountPersonRoleTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(accountPersonRoleTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "idinvalid");
            }

            accountPersonRoleTypeDto.setIsActive(true);
            accountPersonRoleTypeDto.setStatusId(1);

            RAccountPersonRoleType entity = rAccountPersonRoleTypeMapper.toEntity(accountPersonRoleTypeDto);
            entity.setRStatus(rStatusDao.findById(accountPersonRoleTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "idinvalid")));
            entity = rAccountPersonRoleTypeDao.save(entity);

            return new ResponseEntity<>(rAccountPersonRoleTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding AccountPersonRoleType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RAccountPersonRoleTypeDto> getAccountPersonRoleTypeById(Integer id) {
        log.info("Inside AccountPersonRoleTypeService: getAccountPersonRoleTypeById method");

        try {
            Optional<RAccountPersonRoleType> entity = rAccountPersonRoleTypeDao.findByAccountPersonRoleTypeIdAndIsActive(id, true);
            RAccountPersonRoleTypeDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("AccountPersonRoleType not found by ID: " + id, ENTITY_NAME, "idinvalid");
            } else {
                responseDto = rAccountPersonRoleTypeMapper.toDto(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting AccountPersonRoleType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RAccountPersonRoleTypeDto> updateAccountPersonRoleTypeById(Integer id, RAccountPersonRoleTypeDto accountPersonRoleTypeDto) {
        log.info("Inside AccountPersonRoleTypeService: updateAccountPersonRoleTypeById method");

        Optional<RAccountPersonRoleType> existingEntity = rAccountPersonRoleTypeDao.findByAccountPersonRoleTypeIdAndIsActive(id, true);
        if (!existingEntity.isPresent()) {
            throw new BadRequestAlertException("Invalid account person role type ID", ENTITY_NAME, "idinvalid");
        }

        if (!id.equals(accountPersonRoleTypeDto.getAccountPersonRoleTypeId())) {
            throw new BadRequestAlertException("AccountPersonRoleType id mismatch.", ENTITY_NAME, "error");
        }

        if (accountPersonRoleTypeDto.getAccountPersonRoleTypeCode() == null || accountPersonRoleTypeDto.getAccountPersonRoleTypeCode().isEmpty()) {
            throw new BadRequestAlertException("AccountPersonRoleType code is required.", ENTITY_NAME, "error");
        }

        if (rAccountPersonRoleTypeDao.existsByAccountPersonRoleTypeCodeAndIsActiveAndAccountPersonRoleTypeIdNot(accountPersonRoleTypeDto.getAccountPersonRoleTypeCode(), true, id)) {
            throw new BadRequestAlertException("AccountPersonRoleType Code is duplicate with another record.", ENTITY_NAME, "error");
        }

        if (accountPersonRoleTypeDto.getStatusId() == null) {
            throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
        }

        if (!rStatusDao.existsById(accountPersonRoleTypeDto.getStatusId())) {
            throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
        }

        RAccountPersonRoleType entity = existingEntity.get();
        entity.setAccountPersonRoleTypeCode(accountPersonRoleTypeDto.getAccountPersonRoleTypeCode());
        entity.setDescription(accountPersonRoleTypeDto.getDescription());
        entity.setIsActive(accountPersonRoleTypeDto.getIsActive());
        entity.setRStatus(rStatusDao.findById(accountPersonRoleTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID.", ENTITY_NAME, "error")));

        try {
            entity = rAccountPersonRoleTypeDao.save(entity);
            return new ResponseEntity<>(rAccountPersonRoleTypeMapper.toDto(entity), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while updating AccountPersonRoleType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RAccountPersonRoleTypeDto> deleteAccountPersonRoleTypeById(Integer id) {
        log.info("Inside AccountPersonRoleTypeService: deleteAccountPersonRoleTypeById method");

        try {
            Optional<RAccountPersonRoleType> entity = rAccountPersonRoleTypeDao.findByAccountPersonRoleTypeIdAndIsActive(id, true);
            RAccountPersonRoleTypeDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("AccountPersonRoleType not found by ID: " + id, ENTITY_NAME, "idinvalid");
            } else {
                responseDto = rAccountPersonRoleTypeMapper.toDto(entity.get());
                entity.get().setIsActive(false);
                rAccountPersonRoleTypeDao.save(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting AccountPersonRoleType by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
