package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.master.FundTypeDao;
import com.olak.goaml.dao.reference.RFundsTypeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.master.FundTypeDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RFundsTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.parameter.FundTypeMapper;
import com.olak.goaml.mapper.reference.RFundsTypeMapper;
import com.olak.goaml.models.master.FundType;
import com.olak.goaml.models.reference.RFundsType;
import com.olak.goaml.service.reference.FundTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class FundTypeServiceImpl implements FundTypeService {

    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final RFundsTypeDao fundTypeDao;
    private final RFundsTypeMapper fundTypeMapper;

    public FundTypeServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, RFundsTypeDao fundTypeDao, RFundsTypeMapper fundTypeMapper) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.fundTypeDao = fundTypeDao;
        this.fundTypeMapper = fundTypeMapper;
    }

    @Override
    public ResponseEntity<List<RFundsTypeDto>> getAllFundType() {
        log.info("Inside FundTypeService: getAllFundType method");
        List<RFundsType> resultList = fundTypeDao.findByIsActive(true);
        return new ResponseEntity<>(fundTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RFundsTypeDto>>> getFundTypeWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside FundTypeService: getFundTypeWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RFundsType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RFundsTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(fundTypeMapper.listToDto((List<RFundsType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RFundsTypeDto> addFundType(RFundsTypeDto fundTypeDto) {
        log.info("Inside FundTypeService: addFundType method");

        try {
            if (fundTypeDto.getFundTypeCode() == null || fundTypeDto.getFundTypeCode().isEmpty())
                throw new BadRequestAlertException("Fund Type Code is required: ", ENTITY_NAME, "error");

            Optional<RFundsType> resultOp = fundTypeDao.findByFundTypeCodeAndIsActive(fundTypeDto.getFundTypeCode(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("Fund Type already exists by Fund Type code: " + fundTypeDto.getFundTypeCode(), ENTITY_NAME, "error");

            fundTypeDto.setStatusId(1);

            fundTypeDto.setIsActive(true);

//            if (!rStatusDao.existsById(fundTypeDto.getStatusId()))
//                throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

            System.out.println("fundTypeDto: " + fundTypeDto);
            RFundsTypeDto saveDtoRes = fundTypeMapper.toDto(fundTypeDao.save(fundTypeMapper.toEntity(fundTypeDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add Fund Type: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RFundsTypeDto> getFundTypeById(Long id) {
        log.info("Inside FundTypeService: getFundTypeById method");

        try {
            Optional<RFundsType> resultOp = fundTypeDao.findByFundTypeIdAndIsActive(id, true);
            RFundsTypeDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Fund type not found by Fund Type Id: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = fundTypeMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get Fund type by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RFundsTypeDto> updateFundTypeById(Long id, RFundsTypeDto fundTypeDto) {
        log.info("Inside FundTypeService: updateFundTypeById method");

        // Validate
        if (id.toString() != fundTypeDto.getFundTypeId().toString())
            throw new BadRequestAlertException("Fund Type id mismatch.", ENTITY_NAME, "error");

        if (fundTypeDto.getFundTypeCode() == null || fundTypeDto.getFundTypeCode().isEmpty())
            throw new BadRequestAlertException("Fund Type is required: ", ENTITY_NAME, "error");

        if (fundTypeDao.existsByFundTypeCodeAndIsActiveAndFundTypeIdNot(fundTypeDto.getFundTypeCode(), true, id))
            throw new BadRequestAlertException("Fund Type Name is duplicate with another recode: ", ENTITY_NAME, "error");

//        if (fundTypeDto.getStatusId() == null)
//            throw new BadRequestAlertException("Status Can Not Be Empty: ", ENTITY_NAME, "error");
//
//        if (!rStatusDao.existsById(fundTypeDto.getStatusId()))
//            throw new BadRequestAlertException("Status id is invalid ", ENTITY_NAME, "error");

        Optional<RFundsType> resultOp = fundTypeDao.findByFundTypeIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("Fund Type not found by Fund Type ID: " + id, ENTITY_NAME, "error");
        } else {
            RFundsType existingRes = resultOp.get();
            fundTypeDto.setIsActive(existingRes.getIsActive());
            fundTypeDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                RFundsTypeDto responseDto = fundTypeMapper.toDto(fundTypeDao.save(fundTypeMapper.toEntity(fundTypeDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update Fund Type by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<RFundsTypeDto> deleteFundTypeById(Integer id) {
        log.info("Inside FundTypeService: deleteFundTypeById method");

        try {
            Optional<RFundsType> resultOp = fundTypeDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Fund Type not found by Fund Type ID: " + id, ENTITY_NAME, "error");
            } else {
                RFundsType existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                RFundsTypeDto responseDto = fundTypeMapper.toDto(existingRes);
                responseDto = fundTypeMapper.toDto(fundTypeDao.save(fundTypeMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete Fund Type: {}", e.getMessage());
            throw new BadRequestAlertException("Fund Type con not delete by Fund Type ID: " + id, ENTITY_NAME, "error");
        }
    }
}
