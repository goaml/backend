package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TPhoneDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.xml.transaction.TPhoneDto;
import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TPhoneMapper;
import com.olak.goaml.mapper.xml.transaction.TTransactionMapper;
import com.olak.goaml.models.xml.transaction.TAddress;
import com.olak.goaml.models.xml.transaction.TPhone;
import com.olak.goaml.models.xml.transaction.TTransaction;
import com.olak.goaml.service.transaction.TPhoneModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TPhoneModelServiceImpl implements TPhoneModelService {
    private final TPhoneDao tPhoneDao;
    private final TPhoneMapper tPhoneMapper;
    private final FilterUtility<TPhone> filterUtility;

    public TPhoneModelServiceImpl(TPhoneDao tPhoneDao, TPhoneMapper tPhoneMapper) {
        this.tPhoneDao = tPhoneDao;
        this.filterUtility = new FilterUtility<>(tPhoneDao);
        this.tPhoneMapper = tPhoneMapper;
    }

    @Override
    public TPhone getPhoneById(Integer id) {
        log.info("Inside TPhoneModelService: getPhoneById method");
        try {
            Optional<TPhone> resultOp = tPhoneDao.findByPhoneIdAndIsActive(id, true);
            TPhone response = resultOp.orElse(null);
            return response;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getPhoneById: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public List<TPhone> getPhoneListByID(Integer id) {
        log.info("Inside TPhoneModelService: getPhoneListByID method");
        try {
            List<TPhone> phoneList = new ArrayList<>();
            Optional<TPhone> resultOp = tPhoneDao.findByPhoneIdAndIsActive(id, true);
            if (resultOp.isPresent()) {
                TPhone phone = resultOp.get();
                phoneList.add(phone);
            }
            return phoneList;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getPhoneListByID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TPhoneDto>>> getAllPhoneDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer phoneId) {
        log.info("Inside TPhoneModelServiceImpl: getAllPhoneDetails method");
        try {

            List<FilterCriteria<TPhone>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("phoneId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (phoneId != null && phoneId != 0) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("phoneId"), phoneId)
                );
            }


            // pageable
            Page<TPhone> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TPhoneDto> content = tPhoneMapper.listToDto(pageableData.getContent());

            // response
            ApiResponseDto<List<TPhoneDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
