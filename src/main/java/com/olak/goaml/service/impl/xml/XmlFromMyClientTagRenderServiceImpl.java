package com.olak.goaml.service.impl.xml;

import com.olak.goaml.dto.xml.transaction.TFromMyClientDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TFromMyClientMapper;
import com.olak.goaml.models.xml.transaction.TFromMyClient;
import com.olak.goaml.service.xml.*;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.xmlDto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlFromMyClientTagRenderServiceImpl implements XmlFromMyClientTagRenderService {

    private final XmlFCurrencyTagRenderService xmlFCurrencyTagRenderService;
    private final XmlPersonMyClientTagRenderService xmlPersonMyClientTagRenderService;
    private final XmlAccountMyClientTagRenderService xmlAccountMyClientTagRenderService;
    private final XmlEntityMyClientTagRenderService xmlEntityMyClientTagRenderService;
    private final TFromMyClientMapper tFromMyClientMapper;

    public XmlFromMyClientTagRenderServiceImpl(XmlFCurrencyTagRenderService xmlFCurrencyTagRenderService, XmlPersonMyClientTagRenderService xmlPersonMyClientTagRenderService, XmlAccountMyClientTagRenderService xmlAccountMyClientTagRenderService, XmlEntityMyClientTagRenderService xmlEntityMyClientTagRenderService, TFromMyClientMapper tFromMyClientMapper) {
        this.xmlFCurrencyTagRenderService = xmlFCurrencyTagRenderService;
        this.xmlPersonMyClientTagRenderService = xmlPersonMyClientTagRenderService;
        this.xmlAccountMyClientTagRenderService = xmlAccountMyClientTagRenderService;
        this.xmlEntityMyClientTagRenderService = xmlEntityMyClientTagRenderService;
        this.tFromMyClientMapper = tFromMyClientMapper;
    }

    @Override
    public xmlTFromMyClientDto getRenderedTFromMyClient(List<TFromMyClient> fromMyClientList, String scenarioName) {

        try{
            xmlTFromMyClientDto tFromMyClientDto = new xmlTFromMyClientDto();
            if (fromMyClientList != null &&  !fromMyClientList.isEmpty()) {
                for (TFromMyClient fromMyClientDto : fromMyClientList) {
//                TFromMyClientDto fromMyClientDto = new TFromMyClientDto();

                    tFromMyClientDto.setFromFundsCode(fromMyClientDto.getRFundsType().getFundTypeCode());
                    tFromMyClientDto.setFromFundsComment(fromMyClientDto.getFromFundsComment());

                    if (EnumTagVerifier.isTagValid("T_FROM_MY_CLIENT_FROM_FOREIGN_CURRENCY", scenarioName)) {
                        if (fromMyClientDto.getFromForeignCurrencyId() != null)
                            tFromMyClientDto.setFromForeignCurrency(xmlFCurrencyTagRenderService.getRenderedFCurrency(fromMyClientDto.getFromForeignCurrencyId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_FROM_MY_CLIENT_T_CONDUCTOR", scenarioName)) {
                        if (fromMyClientDto.getConductorPersonMyClientId() != null)
                            tFromMyClientDto.setTConductor(xmlPersonMyClientTagRenderService.getRenderedPerson(fromMyClientDto.getConductorPersonMyClientId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_FROM_MY_CLIENT_FROM_ACCOUNT", scenarioName)) {
                        if (fromMyClientDto.getFromAccountMyClientId() != null)
                            tFromMyClientDto.setFromAccount(xmlAccountMyClientTagRenderService.getRenderedAccount(fromMyClientDto.getFromAccountMyClientId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_FROM_MY_CLIENT_FROM_PERSON", scenarioName)) {
                        if (fromMyClientDto.getFromPersonMyClientId() != null)
                            tFromMyClientDto.setFromPerson(xmlPersonMyClientTagRenderService.getRenderedPerson(fromMyClientDto.getFromPersonMyClientId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_FROM_MY_CLIENT_FROM_ENTITY", scenarioName)) {
                        if (fromMyClientDto.getFromEntityMyClientId() != null)
                            tFromMyClientDto.setFromEntity(xmlEntityMyClientTagRenderService.getRenderedEntity(fromMyClientDto.getFromEntityMyClientId(), scenarioName));
                    }

                    tFromMyClientDto.setFromCountry(fromMyClientDto.getRCountryCodes().getCountryCodeCode());

                }
            }
            return tFromMyClientDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedTFromMyClient: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }

    }
}
