package com.olak.goaml.service.impl.xml;

import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.xml.transaction.TAddress;
import com.olak.goaml.service.transaction.TAddressModelService;
import com.olak.goaml.service.xml.XMLAddressTagRenderService;
import com.olak.goaml.xmlProcessor.xmlDto.xmlAddressesDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAddressDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XMLAddressTagRenderServiceImpl implements XMLAddressTagRenderService {
    private final TAddressModelService tAddressModelService;

    public XMLAddressTagRenderServiceImpl(TAddressModelService tAddressModelService) {
        this.tAddressModelService = tAddressModelService;
    }

    @Override
    public xmlTAddressDto getRenderedAddress(Integer id, String scenarioName) {

        try{
            TAddress addressDto = tAddressModelService.getAddressId(id);
            xmlTAddressDto xmlTAddressDto = new xmlTAddressDto();

            if (addressDto != null) {
                xmlTAddressDto.setAddressType(addressDto.getRContactType().getContactTypeCode());
                xmlTAddressDto.setAddress(addressDto.getAddress());
                xmlTAddressDto.setTown(addressDto.getTown());
                xmlTAddressDto.setCity(addressDto.getRCityList().getCityCode());
                xmlTAddressDto.setZip(addressDto.getZip());
                xmlTAddressDto.setCountryCode(addressDto.getRCountryCodes().getCountryCodeCode());
                xmlTAddressDto.setState(addressDto.getState());
                xmlTAddressDto.setComments(addressDto.getComments());
            }else{
                xmlTAddressDto = null;
            }


            return xmlTAddressDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedAddress: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public xmlAddressesDto getRenderedAddressList(Integer id, String scenarioName) {

        try {
            xmlAddressesDto xmlAddressesDto = new xmlAddressesDto();

            List<TAddress> addressDtoDtoList = tAddressModelService.getAddressListById(id);
            List<xmlTAddressDto> xmlTAddressDtoList = new ArrayList<>();

            if (addressDtoDtoList != null && !addressDtoDtoList.isEmpty()) {
                for (TAddress addressDto : addressDtoDtoList) {

                    xmlTAddressDto xmlTAddressDto = new xmlTAddressDto();
                    xmlTAddressDto.setAddressType(addressDto.getRContactType().getContactTypeCode());
                    xmlTAddressDto.setAddress(addressDto.getAddress());
                    xmlTAddressDto.setTown(addressDto.getTown());
                    xmlTAddressDto.setCity(addressDto.getRCityList().getCityCode());
                    xmlTAddressDto.setZip(addressDto.getZip());
                    xmlTAddressDto.setCountryCode(addressDto.getRCountryCodes().getCountryCodeCode());
                    xmlTAddressDto.setState(addressDto.getState());
                    xmlTAddressDto.setComments(addressDto.getComments());

                    xmlTAddressDtoList.add(xmlTAddressDto);
                }
            }

            if (!xmlTAddressDtoList.isEmpty()) {
                xmlAddressesDto.setAddress(xmlTAddressDtoList);
            }

            return xmlAddressesDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedAddressList: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
