package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RStatusMapper;
import com.olak.goaml.models.reference.RStatus;
import com.olak.goaml.service.reference.StatusService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class StatusServiceImpl implements StatusService {

    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final RStatusMapper rStatusMapper;

    public StatusServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, RStatusMapper rStatusMapper) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.rStatusMapper = rStatusMapper;
    }

    @Override
    public ResponseEntity<List<RStatusDto>> getAllStatuses() {
        log.info("Inside StatusService: getAllStatuses method");
        List<RStatus> resultList = rStatusDao.findByIsActive(true);
        return new ResponseEntity<>(rStatusMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RStatusDto>>> getStatusesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside StatusService: getStatusesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RStatus.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RStatusDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(rStatusMapper.listToDto((List<RStatus>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RStatusDto> addStatus(RStatusDto statusDto) {
        log.info("Inside StatusService: addStatus method");

        try {
            if (statusDto.getStatusCode() == null || statusDto.getStatusCode().isEmpty())
                throw new BadRequestAlertException("Status code is required: ", ENTITY_NAME, "error");

            Optional<RStatus> resultOp = rStatusDao.findByStatusCodeAndIsActive(statusDto.getStatusCode(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("Status already exists by Status code: " + statusDto.getStatusCode(), ENTITY_NAME, "error");

            if (statusDto.getDescription() == null || statusDto.getDescription().isEmpty())
                throw new BadRequestAlertException("Status description is required: ", ENTITY_NAME, "error");

            statusDto.setIsActive(true);
            statusDto.setStatusId(1);

            RStatusDto saveDtoRes = rStatusMapper.toDto(rStatusDao.save(rStatusMapper.toEntity(statusDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding Status: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RStatusDto> getStatusById(Integer id) {
        log.info("Inside StatusService: getStatusById method");

        try {
            Optional<RStatus> resultOp = rStatusDao.findByStatusIdAndIsActive(id, true);
            RStatusDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Status not found by Status ID: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = rStatusMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting Status details by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RStatusDto> updateStatusById(Integer id, RStatusDto statusDto) {
        log.info("Inside StatusService: updateStatusById method");

        if (!id.equals(statusDto.getStatusId()))
            throw new BadRequestAlertException("Status id mismatch.", ENTITY_NAME, "error");

        if (statusDto.getStatusCode() == null || statusDto.getStatusCode().isEmpty())
            throw new BadRequestAlertException("Status code is required: ", ENTITY_NAME, "error");

        if (rStatusDao.existsByStatusCodeAndIsActiveAndStatusIdNot(statusDto.getStatusCode(), true, id))
            throw new BadRequestAlertException("Status code is duplicate with another record: ", ENTITY_NAME, "error");

        Optional<RStatus> resultOp = rStatusDao.findByStatusIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("Status not found by Status ID: " + id, ENTITY_NAME, "error");
        } else {
            RStatus existingRes = resultOp.get();
            statusDto.setIsActive(existingRes.getIsActive());

            try {
                RStatusDto responseDto = rStatusMapper.toDto(rStatusDao.save(rStatusMapper.toEntity(statusDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while updating Status by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<RStatusDto> deleteStatusById(Integer id) {
        log.info("Inside StatusService: deleteStatusById method");

        try {
            Optional<RStatus> resultOp = rStatusDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Status not found by Status ID: " + id, ENTITY_NAME, "error");
            } else {
                RStatus existingRes = resultOp.get();
                existingRes.setIsActive(false);
                RStatusDto responseDto = rStatusMapper.toDto(existingRes);
                responseDto = rStatusMapper.toDto(rStatusDao.save(rStatusMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting Status: {}", e.getMessage());
            throw new BadRequestAlertException("Status cannot be deleted by Status ID: " + id, ENTITY_NAME, "error");
        }
    }
}
