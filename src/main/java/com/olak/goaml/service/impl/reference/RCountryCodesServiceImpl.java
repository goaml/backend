package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.reference.RCountryCodesDao;
import com.olak.goaml.dto.reference.RCountryCodesDto;
import com.olak.goaml.dto.reference.RCurrencyDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RCountryCodesMapper;
import com.olak.goaml.models.reference.RCountryCodes;
import com.olak.goaml.models.reference.RCurrency;
import com.olak.goaml.service.reference.RCountryCodesService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class RCountryCodesServiceImpl implements RCountryCodesService {
    private final SearchNFilter searchNFilter;
    private final RCountryCodesDao rCountryCodesDao;
    private final RCountryCodesMapper rCountryCodesMapper;

    public RCountryCodesServiceImpl(SearchNFilter searchNFilter, RCountryCodesDao rCountryCodesDao, RCountryCodesMapper rCountryCodesMapper) {
        this.searchNFilter = searchNFilter;
        this.rCountryCodesDao = rCountryCodesDao;
        this.rCountryCodesMapper = rCountryCodesMapper;
    }

    @Override
    public ResponseEntity<RCountryCodesDto> getCountryCodeById(Integer id) {
        log.info("Inside CountryCodesService: getCountryCodeById method");

        try {
            Optional<RCountryCodes> entity = rCountryCodesDao.findByCountryCodeIdAndIsActive(id, true);
            RCountryCodesDto responseDto;
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("Country Code not found by ID: " + id, ENTITY_NAME, "idinvalid");
            } else {
                responseDto = rCountryCodesMapper.toDto(entity.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting CountryCode details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
