package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TPartyDao;
import com.olak.goaml.dao.xml.transaction.TPhoneDao;
import com.olak.goaml.dao.xml.transaction.TTransactionDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.xml.transaction.TPartyDto;
import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TPartyMapper;
import com.olak.goaml.mapper.xml.transaction.TTransactionMapper;
import com.olak.goaml.models.xml.transaction.TParty;
import com.olak.goaml.models.xml.transaction.TPhone;
import com.olak.goaml.models.xml.transaction.TTransaction;
import com.olak.goaml.service.transaction.TPartyModelService;
import com.olak.goaml.service.transaction.TPhoneModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TPartyModelServiceImpl implements TPartyModelService {

    private final TPartyDao tPartyDao;
    private final TPartyMapper tPartyMapper;
    private final FilterUtility<TParty> filterUtility;

    public TPartyModelServiceImpl(TPartyDao tPartyDao,TPartyMapper tPartyMapper) {
        this.tPartyDao = tPartyDao;
        this.filterUtility = new FilterUtility<>(tPartyDao);
        this.tPartyMapper = tPartyMapper;
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<TPartyDto>>> getAllPartyDetails(Integer page, Integer perPage, String search, String sort, String direction, String rptCode, String trxNo) {
        log.info("Inside TPartyModelServiceImpl: getAllPartyDetails method");
        try {

            List<FilterCriteria<TParty>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("partyId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters

            if (trxNo != null && !trxNo.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("transactionId"), trxNo)
                );
            }

            // pageable
            Page<TParty> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TPartyDto> content = tPartyMapper.listToDto(pageableData.getContent());

            // response
            ApiResponseDto<List<TPartyDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
