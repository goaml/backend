package com.olak.goaml.service.impl.xml;

import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.xml.transaction.TParty;
import com.olak.goaml.service.xml.*;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.xmlDto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlPartyTagRenderServiceImpl implements XmlPartyTagRenderService {

    private final XmlFCurrencyTagRenderService xmlFCurrencyTagRenderService;
    private final XmlPersonMyClientTagRenderService xmlPersonMyClientTagRenderService;
    private final XmlAccountMyClientTagRenderService xmlAccountMyClientTagRenderService;
    private final XmlEntityMyClientTagRenderService xmlEntityMyClientTagRenderService;

    private final XmlPersonTagRenderService xmlPersonTagRenderService;
    private final XmlAccountTagRenderService xmlAccountTagRenderService;
    private final XmlEntityTagRenderService xmlEntityTagRenderService;


    public XmlPartyTagRenderServiceImpl(XmlFCurrencyTagRenderService xmlFCurrencyTagRenderService, XmlPersonMyClientTagRenderService xmlPersonMyClientTagRenderService, XmlAccountMyClientTagRenderService xmlAccountMyClientTagRenderService, XmlEntityMyClientTagRenderService xmlEntityMyClientTagRenderService, XmlPersonTagRenderService xmlPersonTagRenderService, XmlAccountTagRenderService xmlAccountTagRenderService, XmlEntityTagRenderService xmlEntityTagRenderService) {
        this.xmlFCurrencyTagRenderService = xmlFCurrencyTagRenderService;
        this.xmlPersonMyClientTagRenderService = xmlPersonMyClientTagRenderService;
        this.xmlAccountMyClientTagRenderService = xmlAccountMyClientTagRenderService;
        this.xmlEntityMyClientTagRenderService = xmlEntityMyClientTagRenderService;
        this.xmlPersonTagRenderService = xmlPersonTagRenderService;
        this.xmlAccountTagRenderService = xmlAccountTagRenderService;
        this.xmlEntityTagRenderService = xmlEntityTagRenderService;
    }

    @Override
    public xmlInvolvedPartiesDto getRenderedParty(List<TParty> tParties, String scenarioName) {
        try {
            xmlInvolvedPartiesDto involvedPartiesDto = new xmlInvolvedPartiesDto();
            List<xmlTPartyDto> partyDtoLIst = new ArrayList<>();
            if (tParties != null &&  !tParties.isEmpty()) {
                for (TParty party : tParties) {
                    xmlTPartyDto partyDto = new xmlTPartyDto();

                    partyDto.setRole(party.getRPartyType().getPartyTypeCode());

                    if (EnumTagVerifier.isTagValid("T_PARTY_PERSON", scenarioName)) {
                        if (party.getPersonId() != null)
                            partyDto.setPerson(xmlPersonTagRenderService.getRenderedPerson(party.getPersonId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_PARTY_PERSON_MY_CLIENT", scenarioName)) {
                        if (party.getPersonMyClientId() != null)
                            partyDto.setPersonMyClient(xmlPersonMyClientTagRenderService.getRenderedPerson(party.getPersonMyClientId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_PARTY_ACCOUNT", scenarioName)) {
                        if (party.getAccountId() != null)
                            partyDto.setAccount(xmlAccountTagRenderService.getRenderedAccount(party.getAccountId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_PARTY_ACCOUNT_MY_CLIENT", scenarioName)) {
                        if (party.getAccountMyClientId() != null)
                            partyDto.setAccountMyClient(xmlAccountMyClientTagRenderService.getRenderedAccount(party.getAccountMyClientId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_PARTY_ENTITY", scenarioName)) {
                        if (party.getEntityId() != null)
                            partyDto.setEntity(xmlEntityTagRenderService.getRenderedEntity(party.getEntityId(), scenarioName));
                    }

                    if (EnumTagVerifier.isTagValid("T_PARTY_ENTITY_MY_CLIENT", scenarioName)) {
                        if (party.getEntityMyClientId() != null)
                            partyDto.setEntityMyClient(xmlEntityMyClientTagRenderService.getRenderedEntity(party.getEntityMyClientId(), scenarioName));
                    }

                    partyDto.setFundsCode(party.getRFundsType().getFundTypeCode());
                    partyDto.setFundsComment(party.getFundsComment());

                    if (EnumTagVerifier.isTagValid("T_PARTY_FOREIGN_CURRENCY", scenarioName)) {
                        if (party.getForeignCurrencyId() != null)
                            partyDto.setForeignCurrency(xmlFCurrencyTagRenderService.getRenderedFCurrency(party.getForeignCurrencyId(), scenarioName));
                    }

                    partyDto.setCountry(party.getRCountryCodes().getCountryCodeCode());
                    partyDto.setSignificance(party.getSignificance());
                    partyDto.setComments(party.getComments());

                    partyDtoLIst.add(partyDto);
                }
            }

            if (partyDtoLIst != null && !partyDtoLIst.isEmpty())
                involvedPartiesDto.setParty(partyDtoLIst);

            return involvedPartiesDto;

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedPartyTag: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
