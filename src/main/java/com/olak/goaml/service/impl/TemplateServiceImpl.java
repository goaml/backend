package com.olak.goaml.service.impl;

import com.olak.goaml.dao.master.TemplateDao;
import com.olak.goaml.dto.master.TemplateModelDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.master.TemplateMapper;
import com.olak.goaml.models.master.TemplateModel;
import com.olak.goaml.service.TemplateService;
import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TemplateServiceImpl implements TemplateService {

    private final TemplateMapper templateMapper;
    private final TemplateDao templateDao;

    public TemplateServiceImpl(TemplateMapper templateMapper, TemplateDao templateDao) {
        this.templateMapper = templateMapper;
        this.templateDao = templateDao;
    }

    @Override
    public ResponseEntity<TemplateModelDto> getTemplate(Long id) {
        try {
            if (id == null) {
                throw new BadRequestAlertException("Id is null", "template", "error");
            } else {
                Optional<TemplateModel> templateModel = templateDao.findById(id);
                if (templateModel.isPresent()) {
                    return ResponseEntity.ok(templateMapper.toDto(templateModel.get()));
                } else {
                    throw new BadRequestAlertException("Template not found", "template", "error");
                }
            }
        } catch (Exception e) {
            throw new BadRequestAlertException(e.getMessage(), "template", "error");
        }
    }
}
