package com.olak.goaml.service.impl.xml;

import com.olak.goaml.constants.ClientType;
import com.olak.goaml.dao.reference.RAccountStatusTypeDao;
import com.olak.goaml.dao.reference.RAccountTypeDao;
import com.olak.goaml.dao.reference.RCurrencyDao;
import com.olak.goaml.dto.xml.transaction.TAccountMyClientDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.reference.RAccountStatusType;
import com.olak.goaml.models.reference.RAccountType;
import com.olak.goaml.models.reference.RCurrency;
import com.olak.goaml.models.xml.transaction.TAccountMyClient;
import com.olak.goaml.service.transaction.TAccountMyClientModelService;
import com.olak.goaml.service.xml.XmlAccountTagRenderService;
import com.olak.goaml.service.xml.XmlEntityTagRenderService;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlCurrencyTypeDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAccountDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlAccountTagRenderServiceImpl implements XmlAccountTagRenderService {

    private final XmlEntityTagRenderService xmlEntityTagRenderService;
    private final TAccountMyClientModelService tAccountMyClientModelService;
    private final RCurrencyDao rCurrencyDao;
    private final RAccountTypeDao rAccountTypeDao;
    private final RAccountStatusTypeDao rAccountStatusTypeDao;

    public XmlAccountTagRenderServiceImpl(XmlEntityTagRenderService xmlEntityTagRenderService, TAccountMyClientModelService tAccountMyClientModelService, RCurrencyDao rCurrencyDao, RAccountTypeDao rAccountTypeDao, RAccountStatusTypeDao rAccountStatusTypeDao) {
        this.xmlEntityTagRenderService = xmlEntityTagRenderService;
        this.tAccountMyClientModelService = tAccountMyClientModelService;
        this.rCurrencyDao = rCurrencyDao;
        this.rAccountTypeDao = rAccountTypeDao;
        this.rAccountStatusTypeDao = rAccountStatusTypeDao;
    }

    @Override
    public xmlTAccountDto getRenderedAccount(Integer id, String scenarioName) {

        try{
            TAccountMyClient accountMyClientDto = tAccountMyClientModelService.getAccountMyClientById(id, ClientType.CLIENT);
            xmlTAccountDto xmlTAccountDto = new xmlTAccountDto();

            if (accountMyClientDto != null) {
                xmlTAccountDto.setInstitutionName(accountMyClientDto.getInstitutionName());
                xmlTAccountDto.setSwift(accountMyClientDto.getSwift());
                xmlTAccountDto.setNonBankInstitution(accountMyClientDto.getNonBankInstitution());
                xmlTAccountDto.setBranch(accountMyClientDto.getBranch());
                xmlTAccountDto.setAccount(accountMyClientDto.getAccount());

                if (accountMyClientDto.getCurrenciesId() != null) {
                    xmlCurrencyTypeDto currencyTypeDto = new xmlCurrencyTypeDto();
                    Optional<RCurrency> rCurrencyOp = rCurrencyDao.findByCurrenciesIdAndIsActive(accountMyClientDto.getCurrenciesId(), true);
                    rCurrencyOp.ifPresent(rCurrency -> currencyTypeDto.setCurrencyCode(rCurrency.getCurrenciesCode()));
                    xmlTAccountDto.setCurrencyCode(currencyTypeDto);
                }

                xmlTAccountDto.setAccountName(accountMyClientDto.getAccountName());
                xmlTAccountDto.setIban(accountMyClientDto.getIban());
                xmlTAccountDto.setClientNumber(accountMyClientDto.getClientNumber());

                if (accountMyClientDto.getAccountTypeId() != null) {
                    Optional<RAccountType> rAccountTypeOp = rAccountTypeDao.findByAccountTypeIdAndIsActive(accountMyClientDto.getAccountTypeId(), true);
                    rAccountTypeOp.ifPresent(rAccountType -> xmlTAccountDto.setPersonalAccountType(rAccountType.getAccountTypeCode()));
                }


                if (EnumTagVerifier.isTagValid("ACCOUNT_T_ENTITY", scenarioName)) {
                    if (accountMyClientDto.getEntityMyClientId() != null)
                        xmlTAccountDto.setTEntity(xmlEntityTagRenderService.getRenderedEntity(accountMyClientDto.getEntityMyClientId(), scenarioName));
                }

                if (EnumTagVerifier.isTagValid("ACCOUNT_SIGNATORY", scenarioName)) {
                    //        xmlTAccountDto.setSignatory(new List<new xmlAccountMyClientSignatoryDto()>());
                }

                if (accountMyClientDto.getOpened() != null) {
                    Date openDate = accountMyClientDto.getOpened();
                    xmlTAccountDto.setOpened(XmlUtils.convertDateToLocalDate(openDate).atTime(0, 0, 0));
                }

                if (accountMyClientDto.getClosed() != null) {
                    Date closeDate = accountMyClientDto.getClosed();
                    xmlTAccountDto.setClosed(XmlUtils.convertDateToLocalDate(closeDate).atTime(0, 0, 0));
                }

                xmlTAccountDto.setBalance(accountMyClientDto.getBalance());

                if (accountMyClientDto.getDateBalance() != null) {
                    Date balanceDate = accountMyClientDto.getDateBalance();
                    xmlTAccountDto.setDateBalance(XmlUtils.convertDateToLocalDate(balanceDate).atTime(0, 0, 0));
                }

                if (accountMyClientDto.getAccountStatusTypeId() != null) {
                    Optional<RAccountStatusType> rAccountStatusTypeOp = rAccountStatusTypeDao.findByAccountStatusTypeIdAndIsActive(accountMyClientDto.getAccountStatusTypeId(), true);
                    rAccountStatusTypeOp.ifPresent(rAccountStatusType -> xmlTAccountDto.setStatusCode(rAccountStatusType.getAccountStatusTypeCode()));
                }

                xmlTAccountDto.setBeneficiary(accountMyClientDto.getBeneficiary());
                xmlTAccountDto.setBeneficiaryComment(accountMyClientDto.getBeneficiaryComment());
                xmlTAccountDto.setComments(accountMyClientDto.getComments());
            }

            return xmlTAccountDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedAccount: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
