package com.olak.goaml.service.impl.transaction;

import com.olak.goaml.dao.xml.transaction.TFromMyClientDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.xml.transaction.TFromMyClientDto;
import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.xml.transaction.TFromMyClientMapper;
import com.olak.goaml.models.xml.transaction.TFromMyClient;
import com.olak.goaml.models.xml.transaction.TTransaction;
import com.olak.goaml.service.transaction.TFromMyClientModelService;
import com.olak.goaml.userMGT.dao.template.updated.FilterCriteria;
import com.olak.goaml.userMGT.dao.template.updated.FilterUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class TFromMyClientModelServiceImpl implements TFromMyClientModelService {

    private final FilterUtility<TFromMyClient> filterUtility;

    private final TFromMyClientDao tFromMyClientDao;

    private  final TFromMyClientMapper tFromMyClientMapper;
    public TFromMyClientModelServiceImpl(TFromMyClientDao tFromMyClientDao,
                                         TFromMyClientMapper tFromMyClientMapper){
        this.tFromMyClientDao = tFromMyClientDao;
        this.tFromMyClientMapper = tFromMyClientMapper;
        this.filterUtility = new FilterUtility<>(tFromMyClientDao);
    }
    @Override
    public ResponseEntity<ApiResponseDto<List<TFromMyClientDto>>> getAllFromMyClientDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer trxId) {
        log.info("Inside TFromMyClientModelServiceImpl: getAllFromMyClientDetails method");
        try {

            List<FilterCriteria<TFromMyClient>> filterCriteriaList = new ArrayList<>();

            // set search
            if (search != null && !search.isEmpty()) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.like(
                                cb.lower(root.get("fromMyClientId").as(String.class)),
                                "%" + search.toLowerCase() + "%"
                        )
                );
            }

            // set filters
            if (trxId != null && trxId != 0) {
                filterCriteriaList.add((root1, criteriaBuilder) ->
                        (root, query, cb) -> cb.equal(root.get("TTransaction").get("transactionId"), trxId)
                );
            }

            // pageable
            Page<TFromMyClient> pageableData = filterUtility.filterRecords(page, perPage, sort, direction, filterCriteriaList);

            List<TFromMyClientDto> content = tFromMyClientMapper.listToDto(pageableData.getContent());

            // response
            ApiResponseDto<List<TFromMyClientDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) pageableData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(content);

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
