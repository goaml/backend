package com.olak.goaml.service.impl.xml;

import com.olak.goaml.constants.ClientType;
import com.olak.goaml.dao.reference.RAccountStatusTypeDao;
import com.olak.goaml.dao.reference.RAccountTypeDao;
import com.olak.goaml.dao.reference.RCurrencyDao;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.reference.RAccountStatusType;
import com.olak.goaml.models.reference.RAccountType;
import com.olak.goaml.models.reference.RCurrency;
import com.olak.goaml.models.xml.transaction.TAccountMyClient;
import com.olak.goaml.service.transaction.TAccountMyClientModelService;
import com.olak.goaml.service.xml.XmlAccountMyClientTagRenderService;
import com.olak.goaml.service.xml.XmlEntityMyClientTagRenderService;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlCurrencyTypeDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAccountMyClientDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlAccountMyClientTagRenderServiceImpl implements XmlAccountMyClientTagRenderService {

    private final XmlEntityMyClientTagRenderService xmlEntityMyClientTagRenderService;
    private final TAccountMyClientModelService tAccountMyClientModelService;
    private final RCurrencyDao rCurrencyDao;
    private final RAccountTypeDao rAccountTypeDao;
    private final RAccountStatusTypeDao rAccountStatusTypeDao;

    public XmlAccountMyClientTagRenderServiceImpl(XmlEntityMyClientTagRenderService xmlEntityMyClientTagRenderService, TAccountMyClientModelService tAccountMyClientModelService, RCurrencyDao rCurrencyDao, RAccountTypeDao rAccountTypeDao, RAccountStatusTypeDao rAccountStatusTypeDao) {
        this.xmlEntityMyClientTagRenderService = xmlEntityMyClientTagRenderService;
        this.tAccountMyClientModelService = tAccountMyClientModelService;
        this.rCurrencyDao = rCurrencyDao;
        this.rAccountTypeDao = rAccountTypeDao;
        this.rAccountStatusTypeDao = rAccountStatusTypeDao;
    }

    @Override
    public xmlTAccountMyClientDto getRenderedAccount(Integer Id, String scenarioName) {
        try {

            TAccountMyClient accountMyClientDto = tAccountMyClientModelService.getAccountMyClientById(Id, ClientType.My_CLIENT);
            xmlTAccountMyClientDto xmlTAccountMyClientDto = new xmlTAccountMyClientDto();

            if (accountMyClientDto != null) {

                xmlTAccountMyClientDto.setInstitutionName(accountMyClientDto.getInstitutionName());
                xmlTAccountMyClientDto.setSwift(accountMyClientDto.getSwift());
                xmlTAccountMyClientDto.setNonBankInstitution(accountMyClientDto.getNonBankInstitution());
                xmlTAccountMyClientDto.setBranch(accountMyClientDto.getBranch());
                xmlTAccountMyClientDto.setAccount(accountMyClientDto.getAccount());

                if (accountMyClientDto.getCurrenciesId() != null) {
                    xmlCurrencyTypeDto currencyTypeDto = new xmlCurrencyTypeDto();
                    Optional<RCurrency> rCurrencyOp = rCurrencyDao.findByCurrenciesIdAndIsActive(accountMyClientDto.getCurrenciesId(), true);
                    rCurrencyOp.ifPresent(rCurrency -> currencyTypeDto.setCurrencyCode(rCurrency.getCurrenciesCode()));
                    xmlTAccountMyClientDto.setCurrencyCode(currencyTypeDto);
                }


                xmlTAccountMyClientDto.setAccountName(accountMyClientDto.getAccountName());
                xmlTAccountMyClientDto.setIban(accountMyClientDto.getIban());
                xmlTAccountMyClientDto.setClientNumber(accountMyClientDto.getClientNumber());

                if (accountMyClientDto.getAccountTypeId() != null) {
                    Optional<RAccountType> rAccountTypeOp = rAccountTypeDao.findByAccountTypeIdAndIsActive(accountMyClientDto.getAccountTypeId(), true);
                    rAccountTypeOp.ifPresent(rAccountType -> xmlTAccountMyClientDto.setPersonalAccountType(rAccountType.getAccountTypeCode()));
                }

                if (EnumTagVerifier.isTagValid("ACCOUNT_MY_CLIENT_T_ENTITY", scenarioName)) {
                    if (accountMyClientDto.getEntityMyClientId() != null)
                        xmlTAccountMyClientDto.setTEntity(xmlEntityMyClientTagRenderService.getRenderedEntity(accountMyClientDto.getEntityMyClientId(), scenarioName));
                }

                if (EnumTagVerifier.isTagValid("ACCOUNT_MY_CLIENT_SIGNATORY", scenarioName)) {
                    //        xmlTAccountMyClientDto.setSignatory(new List<new xmlAccountMyClientSignatoryDto()>());
                }

                if (accountMyClientDto.getOpened() != null) {
                    Date openDate = accountMyClientDto.getOpened();
                    xmlTAccountMyClientDto.setOpened(XmlUtils.convertDateToLocalDate(openDate).atTime(0, 0, 0));
                }

                if (accountMyClientDto.getClosed() != null) {
                    Date closeDate = accountMyClientDto.getClosed();
                    xmlTAccountMyClientDto.setClosed(XmlUtils.convertDateToLocalDate(closeDate).atTime(0, 0, 0));
                }

                xmlTAccountMyClientDto.setBalance(accountMyClientDto.getBalance());

                if (accountMyClientDto.getDateBalance() != null) {
                    Date balanceDate = accountMyClientDto.getDateBalance();
                    xmlTAccountMyClientDto.setDateBalance(XmlUtils.convertDateToLocalDate(balanceDate).atTime(0, 0, 0));
                }

                if (accountMyClientDto.getAccountStatusTypeId() != null) {
                    Optional<RAccountStatusType> rAccountStatusTypeOp = rAccountStatusTypeDao.findByAccountStatusTypeIdAndIsActive(accountMyClientDto.getAccountStatusTypeId(), true);
                    rAccountStatusTypeOp.ifPresent(rAccountStatusType -> xmlTAccountMyClientDto.setStatusCode(rAccountStatusType.getAccountStatusTypeCode()));
                }

                xmlTAccountMyClientDto.setBeneficiary(accountMyClientDto.getBeneficiary());
                xmlTAccountMyClientDto.setBeneficiaryComment(accountMyClientDto.getBeneficiaryComment());
                xmlTAccountMyClientDto.setComments(accountMyClientDto.getComments());
            }

            return xmlTAccountMyClientDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedAccount: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
