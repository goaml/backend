package com.olak.goaml.service.impl.reference;

import com.olak.goaml.dao.reference.RReportCodeDao;
import com.olak.goaml.dao.reference.RStatusDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RFundsTypeDto;
import com.olak.goaml.dto.reference.RReportCodeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RReportCodeMapper;
import com.olak.goaml.models.reference.RFundsType;
import com.olak.goaml.models.reference.RReportCode;
import com.olak.goaml.service.reference.RReportCodeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;


@Slf4j
@Service
public class RReportCodeServiceImpl implements RReportCodeService {
    private final RStatusDao rStatusDao;
    private final SearchNFilter searchNFilter;
    private final RReportCodeDao reportCodeDao;
    private final RReportCodeMapper reportCodeMapper;

    public RReportCodeServiceImpl(RStatusDao rStatusDao, SearchNFilter searchNFilter, RReportCodeDao reportCodeDao, RReportCodeMapper reportCodeMapper) {
        this.rStatusDao = rStatusDao;
        this.searchNFilter = searchNFilter;
        this.reportCodeDao = reportCodeDao;
        this.reportCodeMapper = reportCodeMapper;
    }

    @Override
    public ResponseEntity<List<RReportCodeDto>> getAllReportCode() {
        log.info("Inside ReportCodeService: getAllReportCode method");
        List<RReportCode> resultList = reportCodeDao.findByIsActive(true);
        return new ResponseEntity<>(reportCodeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RReportCodeDto>>> getReportCodeWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside ReportCodeService: getReportCodeWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RReportCode.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RReportCodeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(reportCodeMapper.listToDto((List<RReportCode>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RReportCodeDto> addReportCode(RReportCodeDto reportCodeDto) {
        log.info("Inside ReportCodeService: addReportCode method");

        try {
            if (reportCodeDto.getReportCode() == null || reportCodeDto.getReportCode().isEmpty())
                throw new BadRequestAlertException("Report Code is required: ", ENTITY_NAME, "error");

            Optional<RReportCode> resultOp = reportCodeDao.findByReportCodeAndIsActive(reportCodeDto.getReportCode(), true);
            if (resultOp.isPresent())
                throw new BadRequestAlertException("Report Code already exists by Report Code: " + reportCodeDto.getReportCode(), ENTITY_NAME, "error");

            reportCodeDto.setStatusId(1);

            reportCodeDto.setIsActive(true);

            RReportCodeDto saveDtoRes = reportCodeMapper.toDto(reportCodeDao.save(reportCodeMapper.toEntity(reportCodeDto)));

            return new ResponseEntity<>(saveDtoRes, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while add Report Code: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RReportCodeDto> getReportCodeById(Integer id) {
        log.info("Inside ReportCodeService: getReportCodeById method");

        try {
            Optional<RReportCode> resultOp = reportCodeDao.findByReportCodeIdAndIsActive(id, true);
            RReportCodeDto responseDto;
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Report Cod not found by Report Cod Id: " + id, ENTITY_NAME, "error");
            } else {
                responseDto = reportCodeMapper.toDto(resultOp.get());
            }
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while get FReport Code by id: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

    @Override
    public ResponseEntity<RReportCodeDto> updateReportCodeById(Integer id, RReportCodeDto reportCodeDto) {
        log.info("Inside ReportCodeService: updateReportCodeById method");

        // Validate
        if (id != reportCodeDto.getReportCodeId())
            throw new BadRequestAlertException("Report Code id mismatch.", ENTITY_NAME, "error");

        if (reportCodeDto.getReportCode() == null || reportCodeDto.getReportCode().isEmpty())
            throw new BadRequestAlertException("Report Code is required: ", ENTITY_NAME, "error");

        if (reportCodeDao.existsByReportCodeAndIsActiveAndReportCodeIdNot(reportCodeDto.getReportCode(), true, id))
            throw new BadRequestAlertException("Report Code Name is duplicate with another recode: ", ENTITY_NAME, "error");


        Optional<RReportCode> resultOp = reportCodeDao.findByReportCodeIdAndIsActive(id, true);
        if (!resultOp.isPresent()) {
            throw new BadRequestAlertException("Report Code not found by Report Code ID: " + id, ENTITY_NAME, "error");
        } else {
            RReportCode existingRes = resultOp.get();
            reportCodeDto.setIsActive(existingRes.getIsActive());
            reportCodeDto.setStatusId(existingRes.getRStatus().getStatusId());

            try {
                RReportCodeDto responseDto = reportCodeMapper.toDto(reportCodeDao.save(reportCodeMapper.toEntity(reportCodeDto)));
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error while update Report Code by id: {}", e.getMessage());
                throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
            }
        }
    }

    @Override
    public ResponseEntity<RReportCodeDto> deleteReportCodeById(Integer id) {
        log.info("Inside ReportCodeService: deleteReportCodeById method");

        try {
            Optional<RReportCode> resultOp = reportCodeDao.findById(id);
            if (!resultOp.isPresent()) {
                throw new BadRequestAlertException("Report Code not found by Report Code ID: " + id, ENTITY_NAME, "error");
            } else {
                RReportCode existingRes = resultOp.get();

                existingRes.setIsActive(existingRes.getIsActive());
                RReportCodeDto responseDto = reportCodeMapper.toDto(existingRes);
                responseDto = reportCodeMapper.toDto(reportCodeDao.save(reportCodeMapper.toEntity(responseDto)));

                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while delete Report Code: {}", e.getMessage());
            throw new BadRequestAlertException("Report Code con not delete by Report Code ID: " + id, ENTITY_NAME, "error");
        }
    }
}
