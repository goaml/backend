package com.olak.goaml.service.impl.reference;
import com.olak.goaml.dao.reference.RStatusDao;

import com.olak.goaml.dao.reference.RAccountStatusTypeDao;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.PaginationDto;
import com.olak.goaml.dto.reference.RAccountStatusTypeDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.mapper.reference.RAccountStatusTypeMapper;
import com.olak.goaml.models.reference.RAccountStatusType;
import com.olak.goaml.service.reference.AccountStatusTypeService;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class AccountStatusTypeServiceImpl implements AccountStatusTypeService {


    private final RStatusDao rStatusDao;

    private final RAccountStatusTypeDao accountStatusTypeDao;
    private final RAccountStatusTypeMapper accountStatusTypeMapper;
    private final SearchNFilter searchNFilter;


    public AccountStatusTypeServiceImpl(RStatusDao rStatusDao, RAccountStatusTypeDao accountStatusTypeDao, RAccountStatusTypeMapper accountStatusTypeMapper, SearchNFilter searchNFilter) {

        this.rStatusDao = rStatusDao;
        this.accountStatusTypeDao = accountStatusTypeDao;
        this.accountStatusTypeMapper = accountStatusTypeMapper;
        this.searchNFilter = searchNFilter;

    }

    @Override
    public ResponseEntity<List<RAccountStatusTypeDto>> getAllAccountStatusTypes() {
        log.info("Inside AccountStatusTypeService: getAllAccountStatusTypes method");
        List<RAccountStatusType> resultList = accountStatusTypeDao.findByIsActive(true);
        return new ResponseEntity<>(accountStatusTypeMapper.listToDto(resultList), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<RAccountStatusTypeDto>>> getAccountStatusTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction) {
        log.info("Inside AccountStatusTypeService: getAccountStatusTypesWithPagination method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setEntityClass(RAccountStatusType.class);
        searchNFilter.setVariables(search, true);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        ApiResponseDto<List<RAccountStatusTypeDto>> response = new ApiResponseDto<>();
        response.setPagination(paginationDto);
        response.setResult(accountStatusTypeMapper.listToDto((List<RAccountStatusType>) dbData.getContent()));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RAccountStatusTypeDto> addAccountStatusType(RAccountStatusTypeDto accountStatusTypeDto) {
        log.info("Inside AccountStatusTypeService: addAccountStatusType method");

        try {

            if (accountStatusTypeDto.getAccountStatusTypeCode() == null || accountStatusTypeDto.getAccountStatusTypeCode().isEmpty()) {
                throw new BadRequestAlertException("AccountStatusType code is required.", ENTITY_NAME, "error");
            }

            Optional<RAccountStatusType> existingEntity = accountStatusTypeDao.findByAccountStatusTypeCodeAndIsActive(accountStatusTypeDto.getAccountStatusTypeCode(), true);
            if (existingEntity.isPresent()) {
                throw new BadRequestAlertException("AccountStatusType already exists with code: " + accountStatusTypeDto.getAccountStatusTypeCode(), ENTITY_NAME, "error");
            }

            if (accountStatusTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(accountStatusTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
            }


            accountStatusTypeDto.setIsActive(true);
            accountStatusTypeDto.setStatusId(1);


            RAccountStatusType entity = accountStatusTypeMapper.toEntity(accountStatusTypeDto);
            entity.setRStatus(rStatusDao.findById(accountStatusTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid status ID", ENTITY_NAME, "error")));
            entity = accountStatusTypeDao.save(entity);


            return new ResponseEntity<>(accountStatusTypeMapper.toDto(entity), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while adding AccountStatusType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RAccountStatusTypeDto> getAccountStatusTypeById(Integer id) {
        log.info("Inside AccountStatusTypeService: getAccountStatusTypeById method");

        try {
            Optional<RAccountStatusType> entity = accountStatusTypeDao.findByAccountStatusTypeIdAndIsActive(id, true);
            if (!entity.isPresent()) {
                throw new BadRequestAlertException("AccountStatusType not found by ID: " + id, ENTITY_NAME, "idinvalid");
            }

            RAccountStatusTypeDto responseDto = accountStatusTypeMapper.toDto(entity.get());
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting AccountStatusType details by ID: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RAccountStatusTypeDto> updateAccountStatusTypeById(Integer id, RAccountStatusTypeDto accountStatusTypeDto) {
        log.info("Inside AccountStatusTypeService: updateAccountStatusTypeById method");

        try {

            if (!id.equals(accountStatusTypeDto.getAccountStatusTypeId())) {
                throw new BadRequestAlertException("AccountStatusType id mismatch.", ENTITY_NAME, "error");
            }

            if (accountStatusTypeDto.getAccountStatusTypeCode() == null || accountStatusTypeDto.getAccountStatusTypeCode().isEmpty()) {
                throw new BadRequestAlertException("AccountStatusType Code is required.", ENTITY_NAME, "error");
            }

            if (accountStatusTypeDao.existsByAccountStatusTypeCodeAndIsActiveAndAccountStatusTypeIdNot(accountStatusTypeDto.getAccountStatusTypeCode(), true, id)) {
                throw new BadRequestAlertException("AccountStatusType Code is duplicate with another record.", ENTITY_NAME, "error");
            }

            if (accountStatusTypeDto.getStatusId() == null) {
                throw new BadRequestAlertException("Status ID is required.", ENTITY_NAME, "error");
            }

            if (!rStatusDao.existsById(accountStatusTypeDto.getStatusId())) {
                throw new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error");
            }

            Optional<RAccountStatusType> existingEntity = accountStatusTypeDao.findByAccountStatusTypeIdAndIsActive(id, true);
            if (!existingEntity.isPresent()) {
                throw new BadRequestAlertException("AccountStatusType not found by ID: " + id, ENTITY_NAME, "error");
            } else {
                RAccountStatusType entity = existingEntity.get();


                entity.setAccountStatusTypeCode(accountStatusTypeDto.getAccountStatusTypeCode());
                entity.setDescription(accountStatusTypeDto.getDescription());
                entity.setIsActive(accountStatusTypeDto.getIsActive());
                entity.setRStatus(rStatusDao.findById(accountStatusTypeDto.getStatusId()).orElseThrow(() -> new BadRequestAlertException("Invalid Status ID.", ENTITY_NAME, "error")));


                entity = accountStatusTypeDao.save(entity);
                return new ResponseEntity<>(accountStatusTypeMapper.toDto(entity), HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while updating AccountStatusType: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    @Override
    public ResponseEntity<RAccountStatusTypeDto> deleteAccountStatusTypeById(Integer id) {
        log.info("Inside AccountStatusTypeService: deleteAccountStatusTypeById method");

        try {
            Optional<RAccountStatusType> existingEntity = accountStatusTypeDao.findByAccountStatusTypeIdAndIsActive(id, true);
            if (!existingEntity.isPresent()) {
                throw new BadRequestAlertException("Invalid account status type ID", ENTITY_NAME, "idinvalid");
            }

            RAccountStatusType entity = existingEntity.get();
            entity.setIsActive(false);
            entity = accountStatusTypeDao.save(entity);

            return new ResponseEntity<>(accountStatusTypeMapper.toDto(entity), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while deleting AccountStatusType: {}", e.getMessage());
            throw new BadRequestAlertException("AccountStatusType could not be deleted", ENTITY_NAME, "deleteerror");
        }
    }

}
