package com.olak.goaml.service.impl.xml;

import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.xml.transaction.TForeignCurrency;
import com.olak.goaml.service.transaction.TForeignCurrencyModelService;
import com.olak.goaml.service.xml.XmlFCurrencyTagRenderService;
import com.olak.goaml.xmlProcessor.xmlDto.xmlCurrencyTypeDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTForeignCurrencyDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlFCurrencyTagRenderServiceImpl implements XmlFCurrencyTagRenderService {

    private final TForeignCurrencyModelService tForeignCurrencyModelService;

    public XmlFCurrencyTagRenderServiceImpl(TForeignCurrencyModelService tForeignCurrencyModelService) {
        this.tForeignCurrencyModelService = tForeignCurrencyModelService;
    }

    @Override
    public xmlTForeignCurrencyDto getRenderedFCurrency(Integer id, String scenarioName) {

        try {
            TForeignCurrency foreignCurrencyDto = tForeignCurrencyModelService.getForeignCurrencyById(id);
            xmlTForeignCurrencyDto xmlTForeignCurrencyDto = new xmlTForeignCurrencyDto();

            if (foreignCurrencyDto != null) {
                xmlCurrencyTypeDto xmlCurrencyTypeDto = new xmlCurrencyTypeDto();
                xmlCurrencyTypeDto.setCurrencyCode(foreignCurrencyDto.getRCurrency().getCurrenciesCode());
                xmlTForeignCurrencyDto.setForeignCurrencyCode(xmlCurrencyTypeDto);

                xmlTForeignCurrencyDto.setForeignAmount(foreignCurrencyDto.getForeignAmount());
                xmlTForeignCurrencyDto.setForeignExchangeRate(foreignCurrencyDto.getForeignExchangeRate());
            }else{
                xmlTForeignCurrencyDto = null;
            }

            return xmlTForeignCurrencyDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedFCurrency: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
