package com.olak.goaml.service.impl.xml;

import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.xml.transaction.TPhone;
import com.olak.goaml.service.transaction.TPhoneModelService;
import com.olak.goaml.service.xml.XMLPhoneTagRenderService;
import com.olak.goaml.xmlProcessor.xmlDto.xmlPhonesDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPhoneDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XMLPhoneTagRenderServiceImpl implements XMLPhoneTagRenderService {
    private final TPhoneModelService tPhoneModelService;

    public XMLPhoneTagRenderServiceImpl(TPhoneModelService tPhoneModelService) {
        this.tPhoneModelService = tPhoneModelService;
    }

    @Override
    public xmlTPhoneDto getRenderedPhone(Integer id, String scenarioName) {

        TPhone phoneDto = tPhoneModelService.getPhoneById(id);
        xmlTPhoneDto xmlTPhoneDto = new xmlTPhoneDto();

        if (phoneDto != null) {
            xmlTPhoneDto.setTphContactType(phoneDto.getRContactType().getContactTypeCode());
            xmlTPhoneDto.setTphCommunicationType(phoneDto.getRCommunicationType().getCommunicationTypeCode());
            xmlTPhoneDto.setTphCountryPrefix(phoneDto.getTphCountryPrefix());
            xmlTPhoneDto.setTphNumber(phoneDto.getTphNumber());
            xmlTPhoneDto.setTphExtension(phoneDto.getTphExtension());
            xmlTPhoneDto.setComments(phoneDto.getComments());
        }else{
            xmlTPhoneDto = null;
        }

        return xmlTPhoneDto;
    }

    @Override
    public xmlPhonesDto getRenderedPhoneList(Integer id, String scenarioName) {

        try{
            xmlPhonesDto xmlPhonesDto = new xmlPhonesDto();

            List<TPhone> phoneDtoDtoList = tPhoneModelService.getPhoneListByID(id);
            List<xmlTPhoneDto> xmlTPhoneDtoList = new ArrayList<>();

            if (phoneDtoDtoList != null && !phoneDtoDtoList.isEmpty()) {
                for (TPhone phoneDto : phoneDtoDtoList) {

                    xmlTPhoneDto xmlTPhoneDto = new xmlTPhoneDto();
                    xmlTPhoneDto.setTphContactType(phoneDto.getRContactType().getContactTypeCode());
                    xmlTPhoneDto.setTphCommunicationType(phoneDto.getRCommunicationType().getCommunicationTypeCode());
                    xmlTPhoneDto.setTphCountryPrefix(phoneDto.getTphCountryPrefix());
                    xmlTPhoneDto.setTphNumber(phoneDto.getTphNumber());
                    xmlTPhoneDto.setTphExtension(phoneDto.getTphExtension());
                    xmlTPhoneDto.setComments(phoneDto.getComments());

                    xmlTPhoneDtoList.add(xmlTPhoneDto);
                }
            }

            if (!xmlTPhoneDtoList.isEmpty()) {
                xmlPhonesDto.setPhone(xmlTPhoneDtoList);
            }

            return xmlPhonesDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedPhoneList: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
