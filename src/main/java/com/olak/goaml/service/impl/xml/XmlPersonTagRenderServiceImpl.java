package com.olak.goaml.service.impl.xml;

import com.olak.goaml.constants.ClientType;
import com.olak.goaml.dao.reference.RCountryCodesDao;
import com.olak.goaml.dao.reference.RGenderTypeDao;
import com.olak.goaml.dto.xml.transaction.TPersonMyClientDto;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.models.reference.RCountryCodes;
import com.olak.goaml.models.reference.RGenderType;
import com.olak.goaml.models.xml.transaction.TPersonMyClient;
import com.olak.goaml.service.transaction.TPersonMyClientModelService;
import com.olak.goaml.service.xml.XMLAddressTagRenderService;
import com.olak.goaml.service.xml.XMLPhoneTagRenderService;
import com.olak.goaml.service.xml.XmlPIdentificationTagRenderService;
import com.olak.goaml.service.xml.XmlPersonTagRenderService;
import com.olak.goaml.xmlProcessor.utils.EnumTagVerifier;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class XmlPersonTagRenderServiceImpl implements XmlPersonTagRenderService {

    private final XMLAddressTagRenderService xmlAddressTagRenderService;
    private final XMLPhoneTagRenderService xmlPhoneTagRenderService;
    private final XmlPIdentificationTagRenderService xmlPIdentificationTagRenderService;
    private final TPersonMyClientModelService tPersonMyClientModelService;
    private final RGenderTypeDao rGenderTypeDao;
    private final RCountryCodesDao rCountryCodesDao;

    public XmlPersonTagRenderServiceImpl(XMLAddressTagRenderService xmlAddressTagRenderService, XMLPhoneTagRenderService xmlPhoneTagRenderService, XmlPIdentificationTagRenderService xmlPIdentificationTagRenderService, TPersonMyClientModelService tPersonMyClientModelService, RGenderTypeDao rGenderTypeDao, RCountryCodesDao rCountryCodesDao) {
        this.xmlAddressTagRenderService = xmlAddressTagRenderService;
        this.xmlPhoneTagRenderService = xmlPhoneTagRenderService;
        this.xmlPIdentificationTagRenderService = xmlPIdentificationTagRenderService;
        this.tPersonMyClientModelService = tPersonMyClientModelService;
        this.rGenderTypeDao = rGenderTypeDao;
        this.rCountryCodesDao = rCountryCodesDao;
    }

    @Override
    public xmlTPersonDto getRenderedPerson(Integer id, String scenarioName) {

        try{
            TPersonMyClient personMyClientDto = tPersonMyClientModelService.getPersonMyClientById(id, ClientType.CLIENT);
            xmlTPersonDto xmlPersonDto = new xmlTPersonDto();

            if (personMyClientDto != null) {
                if (personMyClientDto.getGenderTypeId() != null) {
                    Optional<RGenderType> rGenderTypeOp = rGenderTypeDao.findByGenderTypeIdAndIsActive(personMyClientDto.getGenderTypeId(), true);
                    rGenderTypeOp.ifPresent(rGenderType -> xmlPersonDto.setGender(rGenderType.getGenderTypeCode()));
                }

                xmlPersonDto.setTitle(personMyClientDto.getTitle());
                xmlPersonDto.setFirstName(personMyClientDto.getFirstName());
//                xmlPersonDto.setMiddleName(personMyClientDto.getMiddleName());
//                xmlPersonDto.setPrefix(personMyClientDto.getPrefix());
                xmlPersonDto.setLastName(personMyClientDto.getLastName());

                if (personMyClientDto.getBirthdate() != null) {
                    Date birthDate = personMyClientDto.getBirthdate();
                    xmlPersonDto.setBirthdate(XmlUtils.convertDateToLocalDate(birthDate).atTime(0, 0, 0));
                }

//                xmlPersonDto.setBirthPlace(personMyClientDto.getBirthPlace());
//                xmlPersonDto.setMothersName(personMyClientDto.getMothersName());
//                xmlPersonDto.setAlias(personMyClientDto.getAlias());
                xmlPersonDto.setSsn(personMyClientDto.getSsn());
                xmlPersonDto.setPassportNumber(personMyClientDto.getPassportNumber());

                setCountryCode(personMyClientDto.getPassportCountryId(), xmlPersonDto::setPassportCountry);
//            xmlPersonDto.setPassportCountry(personMyClientDto.getRCountryCodes1().getCountryCodeCode());

                xmlPersonDto.setIdNumber(personMyClientDto.getIdNumber());

                setCountryCode(personMyClientDto.getNationalityCountryId(), xmlPersonDto::setNationality1);
                setCountryCode(personMyClientDto.getNationalityCountry2Id(), xmlPersonDto::setNationality2);
                setCountryCode(personMyClientDto.getNationalityCountry3Id(), xmlPersonDto::setNationality3);
                setCountryCode(personMyClientDto.getResidenceCountryId(), xmlPersonDto::setResidence);

//            xmlPersonDto.setNationality1(personMyClientDto.getRCountryCodes2().getCountryCodeCode());
//            xmlPersonDto.setNationality2(personMyClientDto.getRCountryCodes3().getCountryCodeCode());
//            xmlPersonDto.setNationality3(personMyClientDto.getRCountryCodes4().getCountryCodeCode());
//            xmlPersonDto.setResidence(personMyClientDto.getRCountryCodes5().getCountryCodeCode());

                if (EnumTagVerifier.isTagValid("PERSON_PHONES_PHONE", scenarioName)) {
                    if (personMyClientDto.getPhoneId() != null)
                        xmlPersonDto.setPhones(xmlPhoneTagRenderService.getRenderedPhoneList(personMyClientDto.getPhoneId(), scenarioName));
                }

                if (EnumTagVerifier.isTagValid("PERSON_ADDRESSES_ADDRESS", scenarioName)) {
                    if (personMyClientDto.getAddressId() != null)
                        xmlPersonDto.setAddresses(xmlAddressTagRenderService.getRenderedAddressList(personMyClientDto.getAddressId(), scenarioName));
                }

                List<String> emailList = new ArrayList<>();
                emailList.add(personMyClientDto.getEmail());
                xmlPersonDto.setEmail(emailList);

                xmlPersonDto.setOccupation(personMyClientDto.getOccupation());
//                xmlPersonDto.setEmployerName(personMyClientDto.getEmployerName());
//
//                if (EnumTagVerifier.isTagValid("PERSON_EMPLOYER_ADDRESS_ID", scenarioName)) {
//                    if (personMyClientDto.getEmployerAddressId() != null)
//                        xmlPersonDto.setEmployerAddressId(xmlAddressTagRenderService.getRenderedAddress(personMyClientDto.getEmployerAddressId(), scenarioName));
//                }
//
//                if (EnumTagVerifier.isTagValid("PERSON_EMPLOYER_PHONE_ID", scenarioName)) {
//                    if (personMyClientDto.getEmployerPhoneId() != null)
//                        xmlPersonDto.setEmployerPhoneId(xmlPhoneTagRenderService.getRenderedPhone(personMyClientDto.getEmployerPhoneId(), scenarioName));
//                }
//
//                if (EnumTagVerifier.isTagValid("PERSON_IDENTIFICATION", scenarioName)) {
//                    if (personMyClientDto.getPersonIdentificationId() != null)
//                        xmlPersonDto.setIdentification(xmlPIdentificationTagRenderService.getRenderedIdentificationList(personMyClientDto.getPersonIdentificationId(), scenarioName));
//                }
//
//
//                xmlPersonDto.setDeceased(false);
//
//                if (xmlPersonDto.getDeceased()) {
//                    if (personMyClientDto.getDeceasedDate() != null) {
//                        Date deceasedDate = personMyClientDto.getDeceasedDate();
//                        xmlPersonDto.setDateDeceased(XmlUtils.convertDateToLocalDate(deceasedDate).atTime(0, 0, 0));
//                    }
//                }
//
//                xmlPersonDto.setTaxNumber(personMyClientDto.getTaxNumber());
//                xmlPersonDto.setTaxRegNumber(personMyClientDto.getTaxRegNumber());
//                xmlPersonDto.setSourceOfWealth(personMyClientDto.getSourceOfWealth());
                xmlPersonDto.setComments(personMyClientDto.getComments());
            }


            return xmlPersonDto;
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error while getRenderedPerson: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }


    private void setCountryCode(Integer countryId, Consumer<String> setter) {
        if (countryId != null) {
            Optional<RCountryCodes> countryOp = rCountryCodesDao.findByCountryCodeIdAndIsActive(countryId, true);
            countryOp.ifPresent(RCountryCode -> setter.accept(RCountryCode.getCountryCodeCode()));
        }
    }
}
