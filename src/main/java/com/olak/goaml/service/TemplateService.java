package com.olak.goaml.service;

import com.olak.goaml.dto.master.TemplateModelDto;
import org.springframework.http.ResponseEntity;

public interface TemplateService {
    ResponseEntity<TemplateModelDto> getTemplate(Long id);
}
