//package com.olak.goaml.service.master;
//
//import com.olak.goaml.dao.transaction.DailyTranDetailTableDao;
//import com.olak.goaml.models.transaction.DailyTranDetailTableTemp;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import java.util.List;
//
//@Service
//@Slf4j
//@EnableTransactionManagement
//public class MainMapper {
//
//    private volatile boolean hasLogged = false;
//    private final DailyTranDetailTableDao dailyTranDetailTableDao;
//
//    public MainMapper(DailyTranDetailTableDao dailyTranDetailTableDao) {
//        this.dailyTranDetailTableDao = dailyTranDetailTableDao;
//    }
//
////    @Scheduled(fixedDelay = 5000)
//    public void doMapping() {
//        if (!hasLogged) {
//            log.info("Mapping started");
//            hasLogged = true;
//        }
//
//        List<DailyTranDetailTableTemp> dailyTranDetailTableTemps = dailyTranDetailTableDao.findAll();
//        if (!dailyTranDetailTableTemps.isEmpty()) {
//            dailyTranDetailTableTemps.forEach(dailyTranDetailTableTemp -> {
//                log.info("Data: {}", dailyTranDetailTableTemp.getId().getTranId());
//            });
//        } else {
//            log.info("No data found");
//        }
//    }
//
//}
