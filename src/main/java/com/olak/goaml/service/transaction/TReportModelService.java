package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TReport;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TReportModelService {
    TReport getReportDetailsByReportCode(String reportCode);

    ResponseEntity<ApiResponseDto<List<TReportDto>>> getAllReportDetails(Integer page, Integer perPage, String search, String sort, String direction, String reportCode);
}
