package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TFromDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TFromModelService {
    ResponseEntity<ApiResponseDto<List<TFromDto>>> getAllFromDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer trxId);
}
