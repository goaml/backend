package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TPersonIdentificationDto;
import com.olak.goaml.models.xml.transaction.TPersonIdentification;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TPersonIdentificationModelService {
    TPersonIdentification getIdentificationById(Integer id);

    List<TPersonIdentification> getIdentificationListById(Integer id);

    ResponseEntity<ApiResponseDto<List<TPersonIdentificationDto>>> getAllPersonIdentificationDetails(Integer page, Integer perPage, String search, String sort, String direction,Integer personIdentificationId);
}
