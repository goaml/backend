package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TEntityMyClientDto;
import com.olak.goaml.models.xml.transaction.TEntityMyClient;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TEntityMyClientModelService {
    TEntityMyClient getEntityMyClientById(Integer id, Integer clientId);

    ResponseEntity<ApiResponseDto<List<TEntityMyClientDto>>> getAllEntityMyClientDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer entityMyClientId);
}
