package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TTransactionModelService {
    ResponseEntity<ApiResponseDto<List<TTransactionDto>>> getAllTransactionDetails(Integer page, Integer perPage, String search, String sort, String direction, String rptCode, String trxNo);
}
