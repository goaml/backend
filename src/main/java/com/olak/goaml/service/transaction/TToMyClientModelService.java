package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TToMyClientDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TToMyClientModelService {
    ResponseEntity<ApiResponseDto<List<TToMyClientDto>>> getAllToMyClientDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer trxId);
}
