package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TForeignCurrencyDto;
import com.olak.goaml.models.xml.transaction.TForeignCurrency;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TForeignCurrencyModelService {
    TForeignCurrency getForeignCurrencyById(Integer id);

    ResponseEntity<ApiResponseDto<List<TForeignCurrencyDto>>> getAllForeignCurrencyDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer foreignCurrencyId);
}
