package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TAddressDto;
import com.olak.goaml.models.xml.transaction.TAddress;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TAddressModelService {
    TAddress getAddressId(Integer id);

    List<TAddress> getAddressListById(Integer id);

    ResponseEntity<ApiResponseDto<List<TAddressDto>>> getAllAddressDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer addressId);
}
