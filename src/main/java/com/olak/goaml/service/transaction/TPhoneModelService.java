package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TPhoneDto;
import com.olak.goaml.models.xml.transaction.TPhone;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TPhoneModelService {
    TPhone getPhoneById(Integer id);

    List<TPhone> getPhoneListByID(Integer id);

    ResponseEntity<ApiResponseDto<List<TPhoneDto>>> getAllPhoneDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer phoneId);
}
