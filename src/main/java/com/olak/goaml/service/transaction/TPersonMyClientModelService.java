package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TPersonMyClientDto;
import com.olak.goaml.models.xml.transaction.TPersonMyClient;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TPersonMyClientModelService {
    TPersonMyClient getPersonMyClientById(Integer id, Integer clientType);

    List<TPersonMyClient> getDirectorListByPersonId(Integer id);

    ResponseEntity<ApiResponseDto<List<TPersonMyClientDto>>> getAllPersonMyClientDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer personMyClientId);
}
