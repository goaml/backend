package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TAccountMyClientDto;
import com.olak.goaml.dto.xml.transaction.TToDto;
import com.olak.goaml.models.xml.transaction.TAccountMyClient;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TAccountMyClientModelService {
    TAccountMyClient getAccountMyClientById(Integer id, Integer clientType);

    ResponseEntity<ApiResponseDto<List<TAccountMyClientDto>>> getAllAccountMyClientDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer accountMyClientId);
}
