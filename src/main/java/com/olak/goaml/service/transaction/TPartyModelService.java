package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TPartyDto;
import com.olak.goaml.models.xml.transaction.TPhone;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TPartyModelService {

    ResponseEntity<ApiResponseDto<List<TPartyDto>>> getAllPartyDetails(Integer page, Integer perPage, String search, String sort, String direction, String rptCode, String trxNo);
}
