package com.olak.goaml.service.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.xml.transaction.TToDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TToModelService {
    ResponseEntity<ApiResponseDto<List<TToDto>>> getAllToDetails(Integer page, Integer perPage, String search, String sort, String direction, Integer trxId);
}
