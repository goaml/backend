package com.olak.goaml.service;

import com.olak.goaml.dto.miscellaneous.TranRequestDto;
import com.olak.goaml.userMGT.dto.other.ResponseDto;

public interface SaveDailyTranToTableService {
    ResponseDto saveDailyTranToTable(TranRequestDto tranRequestDto);
}
