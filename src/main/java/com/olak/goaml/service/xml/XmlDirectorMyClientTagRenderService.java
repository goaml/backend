package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlEntityMyClientDirectorIdDto;

import java.util.List;

public interface XmlDirectorMyClientTagRenderService {
    List<xmlEntityMyClientDirectorIdDto> getRenderedDirectorList(Integer id, String scenarioName);
}
