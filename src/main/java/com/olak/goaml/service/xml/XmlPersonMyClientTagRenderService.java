package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonMyClientDto;

public interface XmlPersonMyClientTagRenderService {
    xmlTPersonMyClientDto getRenderedPerson(Integer Id, String scenarioName);
}
