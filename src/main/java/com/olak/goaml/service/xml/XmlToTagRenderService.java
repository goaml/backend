package com.olak.goaml.service.xml;

import com.olak.goaml.models.xml.transaction.TTo;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTToDto;

import java.util.List;

public interface XmlToTagRenderService {
    xmlTToDto getRenderedTTo(List<TTo> toList, String scenarioName);
}
