package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonDto;

public interface XmlPersonTagRenderService {
    xmlTPersonDto getRenderedPerson(Integer id, String scenarioName);
}
