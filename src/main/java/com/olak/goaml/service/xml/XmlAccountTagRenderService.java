package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlTAccountDto;

public interface XmlAccountTagRenderService {
    xmlTAccountDto getRenderedAccount(Integer id, String scenarioName);
}
