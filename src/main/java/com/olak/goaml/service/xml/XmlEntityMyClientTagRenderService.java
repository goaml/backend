package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlTEntityMyClientDto;

public interface XmlEntityMyClientTagRenderService {
    xmlTEntityMyClientDto getRenderedEntity(Integer Id, String scenarioName);
}
