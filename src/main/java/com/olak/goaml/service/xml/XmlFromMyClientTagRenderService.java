package com.olak.goaml.service.xml;

import com.olak.goaml.dto.xml.transaction.TFromMyClientDto;
import com.olak.goaml.models.xml.transaction.TFromMyClient;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTFromMyClientDto;

import java.util.List;

public interface XmlFromMyClientTagRenderService {
    xmlTFromMyClientDto getRenderedTFromMyClient(List<TFromMyClient> fromMyClientList, String scenarioName);
}
