package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlTAccountMyClientDto;

public interface XmlAccountMyClientTagRenderService {
    xmlTAccountMyClientDto getRenderedAccount(Integer Id, String scenarioName);
}
