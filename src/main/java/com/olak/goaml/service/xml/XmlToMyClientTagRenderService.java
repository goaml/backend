package com.olak.goaml.service.xml;

import com.olak.goaml.models.xml.transaction.TToMyClient;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTToMyClientDto;

import java.util.List;

public interface XmlToMyClientTagRenderService {
    xmlTToMyClientDto getRenderedTToMyClient(List<TToMyClient> toMyClientList, String scenarioName);
}
