package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlEntityDirectorIdDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlEntityMyClientDirectorIdDto;

import java.util.List;

public interface XmlDirectorTagRenderService {

    List<xmlEntityDirectorIdDto> getRenderedDirectorList(Integer id,String scenarioName);
}
