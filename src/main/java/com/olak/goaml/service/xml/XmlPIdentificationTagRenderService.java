package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonIdentificationDto;

import java.util.List;

public interface XmlPIdentificationTagRenderService {
    xmlTPersonIdentificationDto getRenderedIdentification(Integer id, String scenarioName);

    List<xmlTPersonIdentificationDto> getRenderedIdentificationList(Integer id, String scenarioName);
}
