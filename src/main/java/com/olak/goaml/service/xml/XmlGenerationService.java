package com.olak.goaml.service.xml;

public interface XmlGenerationService {
    String callApiAndGenerateXml(String reportCode);
}
