package com.olak.goaml.service.xml;

import com.olak.goaml.models.xml.transaction.TFrom;
import com.olak.goaml.models.xml.transaction.TFromMyClient;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTFromDto;

import java.util.List;

public interface XmlFromTagRenderService {
    xmlTFromDto getRenderedTFrom(List<TFrom> fromList, String scenarioName);
}
