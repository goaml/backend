package com.olak.goaml.service.xml;

import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.models.xml.transaction.TTransaction;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTransactionDto;

public interface XmlTransactionTagRenderService {
    xmlTransactionDto getRenderedTransaction(TTransaction transaction);
}
