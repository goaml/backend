package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlTForeignCurrencyDto;

public interface XmlFCurrencyTagRenderService {
    xmlTForeignCurrencyDto getRenderedFCurrency(Integer id, String scenarioName);
}
