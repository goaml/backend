package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlAddressesDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAddressDto;

public interface XMLAddressTagRenderService {
    xmlTAddressDto getRenderedAddress(Integer id, String scenarioName);

    xmlAddressesDto getRenderedAddressList(Integer id, String scenarioName);
}
