package com.olak.goaml.service.xml;

import com.olak.goaml.models.xml.transaction.TParty;
import com.olak.goaml.xmlProcessor.xmlDto.xmlInvolvedPartiesDto;

import java.util.List;

public interface XmlPartyTagRenderService {
    xmlInvolvedPartiesDto getRenderedParty(List<TParty> tParties, String trxNo);
}
