package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlReportDto;

public interface XmlReportTagRenderService {
    xmlReportDto getRenderedReport(String reportCode);
}
