package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlTEntityDto;

public interface XmlEntityTagRenderService {
    xmlTEntityDto getRenderedEntity(Integer id, String scenarioName);
}
