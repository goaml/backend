package com.olak.goaml.service.xml;

import com.olak.goaml.xmlProcessor.xmlDto.xmlPhonesDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPhoneDto;

public interface XMLPhoneTagRenderService {
    xmlTPhoneDto getRenderedPhone(Integer id, String scenarioName);

    xmlPhonesDto getRenderedPhoneList(Integer id, String scenarioName);
}
