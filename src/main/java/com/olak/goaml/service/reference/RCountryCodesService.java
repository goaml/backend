package com.olak.goaml.service.reference;

import com.olak.goaml.dto.reference.RCountryCodesDto;
import com.olak.goaml.dto.reference.RCurrencyDto;
import org.springframework.http.ResponseEntity;

public interface RCountryCodesService {
    ResponseEntity<RCountryCodesDto> getCountryCodeById(Integer id);
}
