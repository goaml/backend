package com.olak.goaml.service.reference;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RReportIndicatorTypeDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ReportIndicatorTypeService {
    ResponseEntity<List<RReportIndicatorTypeDto>> getAllReportIndicatorTypes();

    ResponseEntity<ApiResponseDto<List<RReportIndicatorTypeDto>>> getReportIndicatorTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RReportIndicatorTypeDto> addReportIndicatorType(RReportIndicatorTypeDto reportIndicatorTypeDto);

    ResponseEntity<RReportIndicatorTypeDto> getReportIndicatorTypeById(Integer id);

    ResponseEntity<RReportIndicatorTypeDto> updateReportIndicatorTypeById(Integer id, RReportIndicatorTypeDto reportIndicatorTypeDto);

    ResponseEntity<RReportIndicatorTypeDto> deleteReportIndicatorTypeById(Integer id);
}
