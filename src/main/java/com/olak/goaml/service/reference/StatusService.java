package com.olak.goaml.service.reference;

import com.olak.goaml.dto.reference.RStatusDto;
import org.springframework.http.ResponseEntity;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;

import java.util.List;

public interface StatusService {
    ResponseEntity<List<RStatusDto>> getAllStatuses();

    ResponseEntity<ApiResponseDto<List<RStatusDto>>> getStatusesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RStatusDto> getStatusById(Integer id);

    ResponseEntity<RStatusDto> addStatus(RStatusDto statusDto);

    ResponseEntity<RStatusDto> updateStatusById(Integer id, RStatusDto statusDto);

    ResponseEntity<RStatusDto> deleteStatusById(Integer id);
}
