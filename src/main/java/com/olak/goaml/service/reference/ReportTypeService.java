package com.olak.goaml.service.reference;

import com.olak.goaml.dto.master.ReportTypeDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ReportTypeService {

    ResponseEntity<List<ReportTypeDto>> getAllReportType();

    ResponseEntity<ApiResponseDto<List<ReportTypeDto>>> getAllReportTypeWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<ReportTypeDto> addReportType(ReportTypeDto reportTypeDto);

    ResponseEntity<ReportTypeDto> getReportTypeById(Long id);

    ResponseEntity<ReportTypeDto> updateReportTypeById(Long id, ReportTypeDto reportTypeDto);

    ResponseEntity<ReportTypeDto> deleteReportTypeById(Long id);
}
