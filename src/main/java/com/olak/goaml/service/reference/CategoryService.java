package com.olak.goaml.service.reference;

import com.olak.goaml.dto.master.CategoryDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CategoryService {

    ResponseEntity<List<CategoryDto>> getAllCategory();

    ResponseEntity<ApiResponseDto<List<CategoryDto>>> getCategoryWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<CategoryDto> addCategory(CategoryDto categoryDto);

    ResponseEntity<CategoryDto> getCategoryById(Long id);

    ResponseEntity<CategoryDto> updateCategoryById(Long id, CategoryDto categoryDto);

    ResponseEntity<CategoryDto> deleteCategoryById(Long id);
}
