package com.olak.goaml.service.reference;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RReportCodeDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface RReportCodeService {
    ResponseEntity<List<RReportCodeDto>> getAllReportCode();

    ResponseEntity<ApiResponseDto<List<RReportCodeDto>>> getReportCodeWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RReportCodeDto> addReportCode(RReportCodeDto reportCodeDto);

    ResponseEntity<RReportCodeDto> getReportCodeById(Integer id);

    ResponseEntity<RReportCodeDto> updateReportCodeById(Integer id, RReportCodeDto reportCodeDto);

    ResponseEntity<RReportCodeDto> deleteReportCodeById(Integer id);
}
