package com.olak.goaml.service.reference;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RAccountPersonRoleTypeDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AccountPersonRoleTypeService {
    ResponseEntity<List<RAccountPersonRoleTypeDto>> getAllAccountPersonRoleTypes();
    ResponseEntity<ApiResponseDto<List<RAccountPersonRoleTypeDto>>> getAccountPersonRoleTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);
    ResponseEntity<RAccountPersonRoleTypeDto> addAccountPersonRoleType(RAccountPersonRoleTypeDto accountPersonRoleTypeDto);
    ResponseEntity<RAccountPersonRoleTypeDto> getAccountPersonRoleTypeById(Integer id);
    ResponseEntity<RAccountPersonRoleTypeDto> updateAccountPersonRoleTypeById(Integer id, RAccountPersonRoleTypeDto accountPersonRoleTypeDto);
    ResponseEntity<RAccountPersonRoleTypeDto> deleteAccountPersonRoleTypeById(Integer id);
}
