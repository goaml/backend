package com.olak.goaml.service.reference;

import org.springframework.http.ResponseEntity;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RGenderTypeDto;

import java.util.List;

public interface GenderTypeService {
    ResponseEntity<List<RGenderTypeDto>> getAllGenderTypes();

    ResponseEntity<ApiResponseDto<List<RGenderTypeDto>>> getGenderTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RGenderTypeDto> addGenderType(RGenderTypeDto genderTypeDto);

    ResponseEntity<RGenderTypeDto> getGenderTypeById(Integer id);

    ResponseEntity<RGenderTypeDto> updateGenderTypeById(Integer id, RGenderTypeDto genderTypeDto);

    ResponseEntity<RGenderTypeDto> deleteGenderTypeById(Integer id);
}
