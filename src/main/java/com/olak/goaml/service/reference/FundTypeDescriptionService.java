package com.olak.goaml.service.reference;

import com.olak.goaml.dto.master.FundTypeDescriptionDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface FundTypeDescriptionService {

    ResponseEntity<List<FundTypeDescriptionDto>> getAllFundTypeDescription();

    ResponseEntity<ApiResponseDto<List<FundTypeDescriptionDto>>> getFundTypeDescriptionWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<FundTypeDescriptionDto> addFundTypeDescription(FundTypeDescriptionDto fundTypeDescriptionDto);

    ResponseEntity<FundTypeDescriptionDto> getFundTypeDescriptionById(Long id);

    ResponseEntity<FundTypeDescriptionDto> updateFundTypeDescriptionById(Long id, FundTypeDescriptionDto fundTypeDescriptionDto);

    ResponseEntity<FundTypeDescriptionDto> deleteFundTypeDescriptionById(Long id);
}
