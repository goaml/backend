package com.olak.goaml.service.reference;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.REntityLegalFormTypeDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface EntityLegalFormTypeService {
    ResponseEntity<List<REntityLegalFormTypeDto>> getAllEntityLegalFormTypes();

    ResponseEntity<ApiResponseDto<List<REntityLegalFormTypeDto>>> getEntityLegalFormTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<REntityLegalFormTypeDto> addEntityLegalFormType(REntityLegalFormTypeDto entityLegalFormTypeDto);

    ResponseEntity<REntityLegalFormTypeDto> getEntityLegalFormTypeById(Integer id);

    ResponseEntity<REntityLegalFormTypeDto> updateEntityLegalFormTypeById(Integer id, REntityLegalFormTypeDto entityLegalFormTypeDto);

    ResponseEntity<REntityLegalFormTypeDto> deleteEntityLegalFormTypeById(Integer id);
}
