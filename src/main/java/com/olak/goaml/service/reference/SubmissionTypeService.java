package com.olak.goaml.service.reference;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RSubmissionTypeDto;

public interface SubmissionTypeService {
    ResponseEntity<List<RSubmissionTypeDto>> getAllSubmissionTypes();

    ResponseEntity<ApiResponseDto<List<RSubmissionTypeDto>>> getSubmissionTypesWithPagination(Integer page,Integer perPage, String search, String sort, String direction);

    ResponseEntity<RSubmissionTypeDto> addSubmissionType(RSubmissionTypeDto submissionTypeDto);

    ResponseEntity<RSubmissionTypeDto> getSubmissionTypeById(Integer id);

    ResponseEntity<RSubmissionTypeDto> updateSubmissionTypeById(Integer id, RSubmissionTypeDto submissionTypeDto);

    ResponseEntity<RSubmissionTypeDto> deleteSubmissionTypeById(Integer id);

}
