package com.olak.goaml.service.reference;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RCityListDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CityListService {
    ResponseEntity<List<RCityListDto>> getAllCityLists();

    ResponseEntity<ApiResponseDto<List<RCityListDto>>> getCityListsWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RCityListDto> addCityList(RCityListDto cityListDto);

    ResponseEntity<RCityListDto> getCityListById(Integer id);

    ResponseEntity<RCityListDto> updateCityListById(Integer id, RCityListDto cityListDto);

    ResponseEntity<RCityListDto> deleteCityListById(Integer id);
}
