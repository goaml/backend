package com.olak.goaml.service.reference;

import com.olak.goaml.dto.master.MultiPartyFundCodeDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MultiPartyFundCodeService {

    ResponseEntity<List<MultiPartyFundCodeDto>> getAllMultiPartyFundCode();

    ResponseEntity<ApiResponseDto<List<MultiPartyFundCodeDto>>> getMultiPartyFundCodeWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<MultiPartyFundCodeDto> addMultiPartyFundCode(MultiPartyFundCodeDto multiPartyFundCodeDto);

    ResponseEntity<MultiPartyFundCodeDto> getMultiPartyFundCodeById(Long id);

    ResponseEntity<MultiPartyFundCodeDto> updateMultiPartyFundCodeById(Long id, MultiPartyFundCodeDto multiPartyFundCodeDto);

    ResponseEntity<MultiPartyFundCodeDto> deleteMultiPartyFundCodeById(Long id);
}
