package com.olak.goaml.service.reference;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RPartyTypeDto;

public interface PartyTypeService {
    ResponseEntity<List<RPartyTypeDto>> getAllPartyTypes();

    ResponseEntity<ApiResponseDto<List<RPartyTypeDto>>> getPartyTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RPartyTypeDto> addPartyType(RPartyTypeDto partyTypeDto);

    ResponseEntity<RPartyTypeDto> getPartyTypeById(Integer id);

    ResponseEntity<RPartyTypeDto> updatePartyTypeById(Integer id, RPartyTypeDto partyTypeDto);

    ResponseEntity<RPartyTypeDto> deletePartyTypeById(Integer id);
}
