package com.olak.goaml.service.reference;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RContactTypeDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ContactTypeService {
    ResponseEntity<List<RContactTypeDto>> getAllContactTypes();

    ResponseEntity<ApiResponseDto<List<RContactTypeDto>>> getContactTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RContactTypeDto> addContactType(RContactTypeDto contactTypeDto);

    ResponseEntity<RContactTypeDto> getContactTypeById(Integer id);

    ResponseEntity<RContactTypeDto> updateContactTypeById(Integer id, RContactTypeDto contactTypeDto);

    ResponseEntity<RContactTypeDto> deleteContactTypeById(Integer id);
}
