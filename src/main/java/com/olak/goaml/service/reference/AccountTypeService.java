package com.olak.goaml.service.reference;

import org.springframework.http.ResponseEntity;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RAccountTypeDto;
import java.util.List;

public interface AccountTypeService {
    ResponseEntity<List<RAccountTypeDto>> getAllAccountTypes();

    ResponseEntity<ApiResponseDto<List<RAccountTypeDto>>> getAccountTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RAccountTypeDto> addAccountType(RAccountTypeDto accountTypeDto);

    ResponseEntity<RAccountTypeDto> getAccountTypeById(Integer id);

    ResponseEntity<RAccountTypeDto> updateAccountTypeById(Integer id, RAccountTypeDto accountTypeDto);

    ResponseEntity<RAccountTypeDto> deleteAccountTypeById(Integer id);
}
