package com.olak.goaml.service.reference;

import com.olak.goaml.dto.master.FundTypeDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RFundsTypeDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface FundTypeService {

    ResponseEntity<List<RFundsTypeDto>> getAllFundType();

    ResponseEntity<ApiResponseDto<List<RFundsTypeDto>>> getFundTypeWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RFundsTypeDto> addFundType(RFundsTypeDto fundTypeDto);

    ResponseEntity<RFundsTypeDto> getFundTypeById(Long id);

    ResponseEntity<RFundsTypeDto> updateFundTypeById(Long id, RFundsTypeDto fundTypeDto);

    ResponseEntity<RFundsTypeDto> deleteFundTypeById(Integer id);
}
