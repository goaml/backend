package com.olak.goaml.service.reference;

import com.olak.goaml.dto.master.MultiPartyInvolvePartyDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MultiPartyInvolvePartyService {

    ResponseEntity<List<MultiPartyInvolvePartyDto>> getAllMultiPartyInvolveParty();

    ResponseEntity<ApiResponseDto<List<MultiPartyInvolvePartyDto>>> getMultiPartyInvolvePartyWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<MultiPartyInvolvePartyDto> addMultiPartyInvolveParty(MultiPartyInvolvePartyDto multiPartyInvolvePartyDto);

    ResponseEntity<MultiPartyInvolvePartyDto> getMultiPartyInvolvePartyById(Long id);

    ResponseEntity<MultiPartyInvolvePartyDto> updateMultiPartyInvolvePartyById(Long id, MultiPartyInvolvePartyDto multiPartyInvolvePartyDto);

    ResponseEntity<MultiPartyInvolvePartyDto> deleteMultiPartyInvolvePartyById(Long id);
}
