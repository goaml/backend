package com.olak.goaml.service.reference;

import com.olak.goaml.dto.master.MultiPartyRoleDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MultiPartyRoleService {

    ResponseEntity<List<MultiPartyRoleDto>> getAllMultiPartyRole();

    ResponseEntity<ApiResponseDto<List<MultiPartyRoleDto>>> getMultiPartyRoleWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<MultiPartyRoleDto> addMultiPartyRole(MultiPartyRoleDto multiPartyRoleDto);

    ResponseEntity<MultiPartyRoleDto> getMultiPartyRole(Long id);

    ResponseEntity<MultiPartyRoleDto> updateMultiPartyRoleById(Long id, MultiPartyRoleDto multiPartyRoleDto);

    ResponseEntity<MultiPartyRoleDto> deleteMultiPartyRoleById(Long id);
}
