package com.olak.goaml.service.reference;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RCurrencyDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CurrencyService {
    ResponseEntity<List<RCurrencyDto>> getAllCurrencies();

    ResponseEntity<ApiResponseDto<List<RCurrencyDto>>> getCurrenciesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RCurrencyDto> addCurrency(RCurrencyDto currencyDto);

    ResponseEntity<RCurrencyDto> getCurrencyById(Integer id);

    ResponseEntity<RCurrencyDto> updateCurrencyById(Integer id, RCurrencyDto currencyDto);

    ResponseEntity<RCurrencyDto> deleteCurrencyById(Integer id);
}
