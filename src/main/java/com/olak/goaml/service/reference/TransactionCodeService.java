package com.olak.goaml.service.reference;

import com.olak.goaml.dto.master.TransactionCodesDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TransactionCodeService {

    ResponseEntity<List<TransactionCodesDto>> getAllTransactionCodes();

    ResponseEntity<ApiResponseDto<List<TransactionCodesDto>>> getTransactionCodesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<TransactionCodesDto> addTransactionCodes(TransactionCodesDto transactionCodesDto);

    ResponseEntity<TransactionCodesDto> getTransactionCodesById(Long id);

    ResponseEntity<TransactionCodesDto> updateTransactionCodesById(Long id, TransactionCodesDto transactionCodesDto);

    ResponseEntity<TransactionCodesDto> deleteTransactionCodesById(Long id);
}
