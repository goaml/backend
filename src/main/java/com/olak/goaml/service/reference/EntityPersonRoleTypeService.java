package com.olak.goaml.service.reference;

import com.olak.goaml.dto.reference.REntityPersonRoleTypeDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface EntityPersonRoleTypeService {

    ResponseEntity<List<REntityPersonRoleTypeDto>> getAllEntityPersonRoleTypes();

    ResponseEntity<ApiResponseDto<List<REntityPersonRoleTypeDto>>> getEntityPersonRoleTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<REntityPersonRoleTypeDto> getEntityPersonRoleTypeById(Integer id);

    ResponseEntity<REntityPersonRoleTypeDto> addEntityPersonRoleType(REntityPersonRoleTypeDto entityPersonRoleTypeDto);

    ResponseEntity<REntityPersonRoleTypeDto> updateEntityPersonRoleTypeById(Integer id, REntityPersonRoleTypeDto entityPersonRoleTypeDto);

    ResponseEntity<REntityPersonRoleTypeDto> deleteEntityPersonRoleTypeById(Integer id);
}
