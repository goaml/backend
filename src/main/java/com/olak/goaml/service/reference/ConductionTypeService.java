package com.olak.goaml.service.reference;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RConductionTypeDto;

public interface ConductionTypeService {
    ResponseEntity<List<RConductionTypeDto>> getAllConductionTypes();

    ResponseEntity<ApiResponseDto<List<RConductionTypeDto>>> getConductionTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RConductionTypeDto> addConductionType(RConductionTypeDto conductionTypeDto);

    ResponseEntity<RConductionTypeDto> getConductionTypeById(Integer id);

    ResponseEntity<RConductionTypeDto> updateConductionTypeById(Integer id, RConductionTypeDto conductionTypeDto);

    ResponseEntity<RConductionTypeDto> deleteConductionTypeById(Integer id);
}
