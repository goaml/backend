package com.olak.goaml.service.reference;

import com.olak.goaml.dto.master.GoAmlTrxCodesDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface GoAmlTrxCodeService {

    ResponseEntity<List<GoAmlTrxCodesDto>> getAllGoAmlTrxCodes();

    ResponseEntity<ApiResponseDto<List<GoAmlTrxCodesDto>>> getGoAmlTrxCodesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<GoAmlTrxCodesDto> addGoAmlTrxCodes(GoAmlTrxCodesDto goAmlTrxCodesDto);

    ResponseEntity<GoAmlTrxCodesDto> getGoAmlTrxCodesById(Long id);

    ResponseEntity<GoAmlTrxCodesDto> updateGoAmlTrxCodesById(Long id, GoAmlTrxCodesDto goAmlTrxCodesDto);

    ResponseEntity<GoAmlTrxCodesDto> deleteGoAmlTrxCodesById(Long id);
}
