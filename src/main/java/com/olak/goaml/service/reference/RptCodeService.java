package com.olak.goaml.service.reference;

import com.olak.goaml.dto.master.RptCodeDto;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface RptCodeService {

    ResponseEntity<List<RptCodeDto>> getAllRptCode();

    ResponseEntity<ApiResponseDto<List<RptCodeDto>>> getRptCodeWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RptCodeDto> addRptCode(RptCodeDto rptCodeDto);

    ResponseEntity<RptCodeDto> getRptCodeById(Long id);

    ResponseEntity<RptCodeDto> updateRptCodeById(Long id, RptCodeDto rptCodeDto);

    ResponseEntity<RptCodeDto> deleteRptCodeById(Long id);
}
