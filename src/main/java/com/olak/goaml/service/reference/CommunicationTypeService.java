package com.olak.goaml.service.reference;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RCommunicationTypeDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CommunicationTypeService {
    ResponseEntity<List<RCommunicationTypeDto>> getAllCommunicationTypes();

    ResponseEntity<ApiResponseDto<List<RCommunicationTypeDto>>> getCommunicationTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RCommunicationTypeDto> addCommunicationType(RCommunicationTypeDto communicationTypeDto);

    ResponseEntity<RCommunicationTypeDto> getCommunicationTypeById(Integer id);

    ResponseEntity<RCommunicationTypeDto> updateCommunicationTypeById(Integer id, RCommunicationTypeDto communicationTypeDto);

    ResponseEntity<RCommunicationTypeDto> deleteCommunicationTypeById(Integer id);
}
