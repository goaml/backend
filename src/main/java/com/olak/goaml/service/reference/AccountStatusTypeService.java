package com.olak.goaml.service.reference;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RAccountStatusTypeDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AccountStatusTypeService {

    ResponseEntity<List<RAccountStatusTypeDto>> getAllAccountStatusTypes();

    ResponseEntity<ApiResponseDto<List<RAccountStatusTypeDto>>> getAccountStatusTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RAccountStatusTypeDto> addAccountStatusType(RAccountStatusTypeDto accountStatusTypeDto);

    ResponseEntity<RAccountStatusTypeDto> getAccountStatusTypeById(Integer id);

    ResponseEntity<RAccountStatusTypeDto> updateAccountStatusTypeById(Integer id, RAccountStatusTypeDto accountStatusTypeDto);

    ResponseEntity<RAccountStatusTypeDto> deleteAccountStatusTypeById(Integer id);
}
