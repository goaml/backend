package com.olak.goaml.service.reference;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.RIdentifierTypeDto;

public interface IdentifierTypeService {
    ResponseEntity<List<RIdentifierTypeDto>> getAllIdentifierTypes();

    ResponseEntity<ApiResponseDto<List<RIdentifierTypeDto>>> getIdentifierTypesWithPagination(Integer page, Integer perPage, String search, String sort, String direction);

    ResponseEntity<RIdentifierTypeDto> addIdentifierType(RIdentifierTypeDto identifierTypeDto);

    ResponseEntity<RIdentifierTypeDto> getIdentifierTypeById(Integer id);

    ResponseEntity<RIdentifierTypeDto> updateIdentifierTypeById(Integer id, RIdentifierTypeDto identifierTypeDto);

    ResponseEntity<RIdentifierTypeDto> deleteIdentifierTypeById(Integer id);
}
