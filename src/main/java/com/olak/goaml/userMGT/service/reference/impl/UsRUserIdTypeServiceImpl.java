package com.olak.goaml.userMGT.service.reference.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.constants.HardCodeConstants;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.reference.UsRLoginTypeDao;
import com.olak.goaml.userMGT.dao.reference.UsRUserIdTypeDao;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.reference.UsRUserIdTypeDto;
import com.olak.goaml.userMGT.dto.reference.UserIdTypeReqDto;
import com.olak.goaml.userMGT.mappers.reference.UsRUserIdTypeMapper;
import com.olak.goaml.userMGT.models.reference.UsRLoginType;
import com.olak.goaml.userMGT.models.reference.UsRUserIdType;
import com.olak.goaml.userMGT.service.reference.UsRUserIdTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class UsRUserIdTypeServiceImpl implements UsRUserIdTypeService {

    private final UsRUserIdTypeMapper userIdTypeMapper;
    private final UsRUserIdTypeDao userIdTypeDao;
    private final UsRLoginTypeDao loginTypeDao;

    public UsRUserIdTypeServiceImpl(UsRUserIdTypeMapper userIdTypeMapper, UsRUserIdTypeDao userIdTypeDao, UsRLoginTypeDao loginTypeDao) {
        this.userIdTypeMapper = userIdTypeMapper;
        this.userIdTypeDao = userIdTypeDao;
        this.loginTypeDao = loginTypeDao;
    }

    @Override
    public ResponseEntity<List<UsRUserIdTypeDto>> getUserTypesForDropdown() {
        try {
            List<UsRUserIdType> userIdTypeList = userIdTypeDao.findAllByIsActive(ActiveStatus.ACTIVE.getValue());
            if (userIdTypeList.isEmpty()) {
                throw new BadRequestAlertException("No User Type Found", "User Type", "user_type_not_found");
            } else {
                return ResponseEntity.ok(userIdTypeMapper.toDtoList(userIdTypeList));
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "User Type", "user_type_error");
        }
    }

    @Override
    public ResponseEntity<List<UsRUserIdTypeDto>> getUserTypesForDropdownWithoutStaff() {
        try {
            List<UsRUserIdType> userIdTypeList = userIdTypeDao.findAllByIsActive(ActiveStatus.ACTIVE.getValue());
            if (userIdTypeList.isEmpty()) {
                throw new BadRequestAlertException("No User Type Found", "User Type", "user_type_not_found");
            } else {
                List<UsRUserIdTypeDto> newUserIdTypeList = new ArrayList<>();
                for (UsRUserIdType userIdType : userIdTypeList) {
                    if (!Objects.equals(userIdType.getUserTypeId(), HardCodeConstants.STAFF_USER_TYPE_ID)) {
                        newUserIdTypeList.add(userIdTypeMapper.toDto(userIdType));
                    }
                }
                return ResponseEntity.ok(newUserIdTypeList);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "User Type", "user_type_error");
        }
    }

    @Override
    public ResponseEntity<UsRUserIdTypeDto> createUserIdType(UserIdTypeReqDto userIdTypeReqDto) {
        try {

            Optional<UsRUserIdType> optUserType = userIdTypeDao.findByUserCategory(userIdTypeReqDto.getUserCategory());
            if (!optUserType.isPresent()) {
                throw new BadRequestAlertException("User Type Already Exists", "User Type", "user_type_exists");
            } else {

                Optional<UsRLoginType> optLoginType = loginTypeDao.findById(userIdTypeReqDto.getLoginTypeId());
                if (!optLoginType.isPresent()) {
                    throw new BadRequestAlertException("Login Type Not Found", "Login Type", "login_type_not_found");
                }

                UsRUserIdType userIdType = new UsRUserIdType();
                userIdType.setUserCategory(userIdTypeReqDto.getUserCategory());
                userIdType.setUsRLoginType(optLoginType.get());
                userIdType.setIsActive(ActiveStatus.ACTIVE.getValue());

                userIdType = userIdTypeDao.save(userIdType);
                if (userIdType.getUserTypeId() == null) {
                    throw new BadRequestAlertException("User Type Creation Failed", "User Type", "user_type_creation_failed");
                } else {
                    return ResponseEntity.ok(userIdTypeMapper.toDto(userIdType));
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "User Type", "user_type_error");
        }
    }

    @Override
    public ResponseEntity<UsRUserIdTypeDto> getUserIdType(Integer userTypeId) {
        try {
            if (userTypeId == null) {
                throw new BadRequestAlertException("User Type Id Required", "User Type", "user_type_id_required");
            } else {
                Optional<UsRUserIdType> optUserType = userIdTypeDao.findByUserTypeIdAndIsActive(userTypeId, ActiveStatus.ACTIVE.getValue());
                if (!optUserType.isPresent()) {
                    throw new BadRequestAlertException("User Type Not Found", "User Type", "user_type_not_found");
                } else {
                    return ResponseEntity.ok(userIdTypeMapper.toDto(optUserType.get()));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "User Type", "user_type_error");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<UsRUserIdTypeDto>>> getAllUserIdType(Integer page, Integer per_page, String search, String direction, String sort, Integer loginTypeId) {
        Page<UsRUserIdType> dbData;
        if (loginTypeId != null) {
            dbData = userIdTypeDao.findUserIdTypesForTableByLoginType(loginTypeId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
        } else if (direction.equalsIgnoreCase("asc")) {
            dbData = userIdTypeDao.findUserIdTypeForTable(search, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else {
            dbData = userIdTypeDao.findUserIdTypeForTable(search, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
        }

        ApiResponseDto<List<UsRUserIdTypeDto>> response = new ApiResponseDto<>();
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);
        response.setResult(userIdTypeMapper.toDtoList(dbData.getContent()));
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<UsRUserIdTypeDto> updateUserIdType(Integer userTypeId, UsRUserIdTypeDto usRUserIdTypeDto) {
        try {
            if (userTypeId == null) {
                throw new BadRequestAlertException("userTypeId null", "ERROR", "updateUserIdType");
            } else if (!userTypeId.equals(usRUserIdTypeDto.getUserTypeId())) {
                throw new BadRequestAlertException("userTypeId mismatched", "ERROR", "updateUserIdType");
            } else {

                Optional<UsRLoginType> optLoginType = loginTypeDao.findByLoginTypeIdAndIsActive(usRUserIdTypeDto.getLoginTypeId(), ActiveStatus.ACTIVE.getValue());
                if (!optLoginType.isPresent()) {
                    throw new BadRequestAlertException("Login type not found", "ERROR", "updateUserIdType");
                }

                Optional<UsRUserIdType> optUserIdType = userIdTypeDao.findByUserTypeIdAndIsActive(userTypeId, ActiveStatus.ACTIVE.getValue());
                if (!optUserIdType.isPresent()) {
                    throw new BadRequestAlertException("User id type not found", "ERROR", "updateUserIdType");
                } else {
                    UsRUserIdType userIdType = optUserIdType.get();
                    userIdType.setUserCategory(usRUserIdTypeDto.getUserCategory());
                    userIdType.setUsRLoginType(optLoginType.get());

                    userIdType = userIdTypeDao.save(userIdType);
                    return ResponseEntity.ok(userIdTypeMapper.toDto(userIdType));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "ERROR", "updateUserIdType");
        }
    }

    @Override
    public ResponseEntity<Integer> deleteUserIdType(Integer userTypeId) {
        try {
            if (userTypeId == null) {
                throw new BadRequestAlertException("userTypeId null", "ERROR", "deleteUserIdType");
            } else {
                Optional<UsRUserIdType> optUserIdType = userIdTypeDao.findByUserTypeIdAndIsActive(userTypeId, ActiveStatus.ACTIVE.getValue());
                if (!optUserIdType.isPresent()) {
                    throw new BadRequestAlertException("User id type not found", "ERROR", "deleteUserIdType");
                } else {
                    UsRUserIdType userIdType = optUserIdType.get();
                    userIdType.setIsActive(ActiveStatus.INACTIVE.getValue());
                    userIdTypeDao.save(userIdType);
                    if (userIdTypeDao.findByUserTypeIdAndIsActive(userTypeId, ActiveStatus.ACTIVE.getValue()).isPresent()) {
                        throw new BadRequestAlertException("User id type deletion failed", "ERROR", "deleteUserIdType");
                    } else {
                        return ResponseEntity.ok(userTypeId);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "ERROR", "deleteUserIdType");
        }
    }

}
