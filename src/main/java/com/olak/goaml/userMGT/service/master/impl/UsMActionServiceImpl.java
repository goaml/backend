package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.userMGT.dao.master.UsMActionDao;
import com.olak.goaml.userMGT.dto.master.UsMActionDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.mappers.master.UsMActionMapper;
import com.olak.goaml.userMGT.models.master.UsMAction;
import com.olak.goaml.userMGT.service.master.UsMActionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsMActionServiceImpl implements UsMActionService {

    private final UsMActionMapper actionMapper;
    private final UsMActionDao actionDao;

    public UsMActionServiceImpl(UsMActionMapper actionMapper, UsMActionDao actionDao) {
        this.actionMapper = actionMapper;
        this.actionDao = actionDao;
    }

    @Override
    public ApiResponseDto<List<UsMActionDto>> getAllAction(Integer page, Integer per_page, String search, String sort, String direction) {

        Page<UsMAction> dbData;

        if (direction.equalsIgnoreCase("asc")) {
            dbData = actionDao.getAllActionsForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else {
            dbData = actionDao.getAllActionsForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.DESC, sort));
        }

        ApiResponseDto<List<UsMActionDto>> response = new ApiResponseDto<>();
        response.setResult(actionMapper.listToDto(dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;

    }
}
