package com.olak.goaml.userMGT.service.log;

import com.olak.goaml.userMGT.dto.log.UsLActivityLogDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsLActivityLogService {
    ResponseEntity<UsLActivityLogDto> createActivityLog(UsLActivityLogDto activityLogDto);

    ResponseEntity<ApiResponseDto<List<UsLActivityLogDto>>> getAllActivityLogs(
            Integer page,
            Integer perPage,
            String sort,
            String search,
            String direction,
            Integer actionId,
            String username,
            Integer branchId
    );

    ResponseEntity<UsLActivityLogDto> updateActivityLog(Integer activityId, UsLActivityLogDto activityLogDto);

    ResponseEntity<UsLActivityLogDto> activityLogGetById(Integer activityId);
}
