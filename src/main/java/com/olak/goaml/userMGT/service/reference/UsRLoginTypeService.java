package com.olak.goaml.userMGT.service.reference;

import com.olak.goaml.userMGT.dto.reference.UsRLoginTypeDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsRLoginTypeService {
    ResponseEntity<List<UsRLoginTypeDto>> getLoginTypesForDropdown();
}
