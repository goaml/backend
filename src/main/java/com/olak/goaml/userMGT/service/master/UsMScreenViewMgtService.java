package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMScreenViewMgtDto;
import com.olak.goaml.userMGT.dto.other.AttributeListDto;

import java.util.List;

public interface UsMScreenViewMgtService {

    List<UsMScreenViewMgtDto> createAttributeAccess(AttributeListDto attributeListDto);

}
