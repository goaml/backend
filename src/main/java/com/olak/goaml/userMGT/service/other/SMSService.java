package com.olak.goaml.userMGT.service.other;

import com.olak.goaml.constants.HardCodeConstants;
import com.olak.goaml.userMGT.dto.miscellaneous.SmsDto;
import com.olak.goaml.userMGT.proxy.SmsSend;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;

@Slf4j
@Service
public class SMSService {
    public static void sendSMS(String to, String body) {

        SmsSend smsSend = new SmsSend();
        // sms sent
        SmsDto smsDto = new SmsDto();
        smsDto.setMask(HardCodeConstants.SMS_MASK);

        smsDto.setMsg(body);
        smsDto.setNumbers(to);
        HttpURLConnection con = smsSend.smsIntegration(smsDto);
        try {
            System.out.println(con.getResponseCode());
            System.out.println(con.getResponseMessage());
            System.out.println(con.getContent());
            log.info("SMS sent to " + to);
        } catch (IOException e) {
            e.fillInStackTrace();
        }

    }
}