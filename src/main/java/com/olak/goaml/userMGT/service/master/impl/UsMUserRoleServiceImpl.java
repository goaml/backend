package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.constants.HardCodeConstants;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.master.AdMUserDao;
import com.olak.goaml.userMGT.dao.master.UsMMenuAllowedMapDao;
import com.olak.goaml.userMGT.dao.master.UsMUserRoleDao;
import com.olak.goaml.userMGT.dao.reference.UsRBrachDepartmentDao;
import com.olak.goaml.userMGT.dao.reference.UsRStatusDetailDao;
import com.olak.goaml.userMGT.dao.reference.UsRUserBranchDao;
import com.olak.goaml.userMGT.dao.transaction.UsTMenuActionDao;
import com.olak.goaml.userMGT.dto.master.UsMUserRoleDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.other.BranchListDto;
import com.olak.goaml.userMGT.dto.other.MenuActionListDto;
import com.olak.goaml.userMGT.dto.other.RoleCreationDto;
import com.olak.goaml.userMGT.dto.other.RoleOptionsDto;
import com.olak.goaml.userMGT.dto.reference.UsRBrachDepartmentDto;
import com.olak.goaml.userMGT.mappers.master.UsMUserRoleMapper;
import com.olak.goaml.userMGT.mappers.reference.UsRBrachDepartmentMapper;
import com.olak.goaml.userMGT.models.master.AdMUser;
import com.olak.goaml.userMGT.models.master.UsMMenuAllowedMap;
import com.olak.goaml.userMGT.models.master.UsMUserRole;
import com.olak.goaml.userMGT.models.reference.UsRBrachDepartment;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.reference.UsRUserBranch;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import com.olak.goaml.userMGT.service.master.UsMUserRoleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Service
public class UsMUserRoleServiceImpl implements UsMUserRoleService {

    private static final String CODE014 = "STATUS_ID_NULL";
    private static final String CODE015 = "USER_ROLE_NAME_NULL";
    private static final String CODE016 = "ROLE_ALREADY_EXISTS";
    private static final String CODE017 = "ROLE_CREATION_FAILED";
    private static final String CODE018 = "MENU_ACTION_ID_NOT_FOUND";
    private final UsMUserRoleDao userRoleDao;
    private final UsMUserRoleMapper userRoleMapper;

    private final UsMMenuAllowedMapDao menuAllowedMapDao;
    private final UsTMenuActionDao menuActionDao;
    private final UsRStatusDetailDao statusDetailDao;
    private final UsRUserBranchDao usRUserBranchDao;
    private final UsRBrachDepartmentDao usRBrachDepartmentDao;
    private final AdMUserDao userDao;
    private final UsRBrachDepartmentMapper usRBrachDepartmentMapper;

    public UsMUserRoleServiceImpl(UsMUserRoleDao userRoleDao, UsMUserRoleMapper userRoleMapper, UsMMenuAllowedMapDao menuAllowedMapDao, UsTMenuActionDao menuActionDao, UsRStatusDetailDao statusDetailDao, UsRUserBranchDao usRUserBranchDao, UsRBrachDepartmentDao usRBrachDepartmentDao, AdMUserDao userDao, UsRBrachDepartmentMapper usRBrachDepartmentMapper) {
        this.userRoleDao = userRoleDao;
        this.userRoleMapper = userRoleMapper;
        this.menuAllowedMapDao = menuAllowedMapDao;
        this.menuActionDao = menuActionDao;
        this.statusDetailDao = statusDetailDao;
        this.usRUserBranchDao = usRUserBranchDao;
        this.usRBrachDepartmentDao = usRBrachDepartmentDao;
        this.userDao = userDao;
        this.usRBrachDepartmentMapper = usRBrachDepartmentMapper;
    }


    @Override
    public ApiResponseDto<List<UsMUserRoleDto>> getAllUserRoles(int page, Integer per_page, String sort, String direction) {

        Page<UsMUserRole> dbData;

        if (direction.equalsIgnoreCase("asc")) {
            dbData = userRoleDao.findByUsRStatusDetailStatusId(HardCodeConstants.ACTIVE_AUTHORIZED_STATUS_ID, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else {
            dbData = userRoleDao.findByUsRStatusDetailStatusId(HardCodeConstants.ACTIVE_AUTHORIZED_STATUS_ID, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
        }

        ApiResponseDto<List<UsMUserRoleDto>> response = new ApiResponseDto<>();
        response.setResult(userRoleMapper.listToDto(dbData.getContent()));

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;

    }

    @Override
    public UsMUserRoleDto roleCreation(RoleCreationDto userRole) {
        if (userRole.getStatusId() == null) {
            throw new BadRequestAlertException("CODE014", ENTITY_NAME, CODE014);
        }
        if (userRole.getUserRoleName() == null || userRole.getUserRoleName().isEmpty()) {
            throw new BadRequestAlertException("CODE015", ENTITY_NAME, CODE015);
        }

        UsMUserRoleDto userRoleAdd = new UsMUserRoleDto();

        String userRoleName = userRole.getUserRoleName();
        UsMUserRole usMUserRole = userRoleDao.findByUserRoleName(userRoleName);

        if (usMUserRole == null) {
            Integer status = userRole.getStatusId();
            System.out.println("Status = " + status);

            userRoleAdd.setStatusId(status);
            userRoleAdd.setUserRoleName(userRoleName);
            userRoleAdd.setUserRoleDesc(userRole.getUserRoleDescription());
            userRoleAdd.setStatusFlag("N");
            userRoleAdd.setRoleInBussiness(userRole.getRoleInBussiness());
            userRoleAdd.setCCode(userRole.getCCode());
            userRoleAdd.setRoleApprvReason(userRole.getRoleApproveReason());

            usMUserRole = userRoleDao.save(userRoleMapper.toEntity(userRoleAdd));

            List<RoleOptionsDto> roleOptionsList = userRole.getRoleOptions();
            ArrayList<Integer> roleOptionIdArray = new ArrayList<>();

            //Set role option id to arrayList
            for (RoleOptionsDto roleOptions : roleOptionsList) {
                if (roleOptions.getMenuActionId() != null) {
                    Optional<UsTMenuAction> menuAction = menuActionDao.findById(roleOptions.getMenuActionId());
                    if (menuAction.isPresent()) {
                        roleOptionIdArray.add(menuAction.get().getMenuActionId());
                    } else {
                        throw new BadRequestAlertException("CODE018", ENTITY_NAME, CODE018);
                    }
                }
            }
            System.out.println("Role Options : " + roleOptionIdArray);

            List<UsMMenuAllowedMap> menuAllowedMapList = new ArrayList<>();
            for (Integer i : roleOptionIdArray) {

                Optional<UsMUserRole> optUserRole = userRoleDao.findById(usMUserRole.getUserRoleId());
                Optional<UsTMenuAction> optMenuAction = menuActionDao.findById(i);
                Optional<UsRStatusDetail> optStatusDetail = statusDetailDao.findById(3);
                if (!optUserRole.isPresent() || !optMenuAction.isPresent() || !optStatusDetail.isPresent()) {
                    throw new BadRequestAlertException("CODE018", ENTITY_NAME, CODE018);
                }

                UsMMenuAllowedMap menuAllowedMap = new UsMMenuAllowedMap();
                menuAllowedMap.setUsMUserRole(optUserRole.get());
                menuAllowedMap.setUsTMenuAction(optMenuAction.get());
                menuAllowedMap.setUsRStatusDetail(optStatusDetail.get());
                menuAllowedMapList.add(menuAllowedMap);
            }

            System.out.println("Menu Allowed Map List Size : " + menuAllowedMapList.size());

            menuAllowedMapDao.saveAll(menuAllowedMapList);
            System.out.println("Menu Allowed Map List Saved Successfully");

        } else if (usMUserRole.getUserRoleName().equals(userRoleName)) {
            throw new BadRequestAlertException("CODE016", ENTITY_NAME, CODE016);
        } else {
            throw new BadRequestAlertException("CODE017", ENTITY_NAME, CODE017);
        }
        return userRoleMapper.toDto(usMUserRole);
    }

    @Override
    public MenuActionListDto getAllowedMenuActionsForUserRole(Integer userRoleId, Boolean status) {

        MenuActionListDto resData = new MenuActionListDto();
        List<RoleOptionsDto> data = new ArrayList<>();

        List<UsTMenuAction> menuActionItems;

        Optional<UsMUserRole> optUserRole = userRoleDao.findById(userRoleId);
        if (!optUserRole.isPresent()) {
            throw new BadRequestAlertException("USER_ROLE_NOT_FOUND", ENTITY_NAME, "USER_ROLE_NOT_FOUND");
        }

        if (status) {
            menuActionItems = userRoleDao.findAllowedMenuActionsForRole(userRoleId);
        } else {
            menuActionItems = userRoleDao.findRestrictedMenuActionsForRole(userRoleId);
        }

        if (menuActionItems != null) {
            for (UsTMenuAction menuAction : menuActionItems) {
                RoleOptionsDto res = new RoleOptionsDto();
                if (menuAction.getUsRStatusDetail().getStatusId().equals(3)) {
                    res.setMenuActionId(menuAction.getMenuActionId());
                    res.setStatus(status);
                    res.setMenuActionName(menuAction.getUsMAction().getActionName());
                    res.setMenuName(menuAction.getUsMMenu().getMenuName());
                    res.setUrl(menuAction.getUsMMenu().getMenuUrl());

                    data.add(res);
                }
            }
        }

        UsMUserRole userRole = optUserRole.get();

        resData.setUserRoleId(userRole.getUserRoleId());
        resData.setUserRoleName(userRole.getUserRoleName());
        resData.setUserRoleDesc(userRole.getUserRoleDesc());
        resData.setRoleOptions(data);
        resData.setHasAccess(status);
        resData.setStatusId(userRole.getUsRStatusDetail().getStatusId());
        return resData;

    }

    @Override
    public MenuActionListDto getMenuActionListForRole(Integer userRoleId) {

        MenuActionListDto response = new MenuActionListDto();
        List<UsTMenuAction> menuActionItems;

        Optional<UsMUserRole> optionalUserRole = userRoleDao.findById(userRoleId);
        if (optionalUserRole.isPresent()) {

            menuActionItems = menuActionDao.findUserAccessMenuListForUserRole(userRoleId);
            Optional<UsRStatusDetail> optionalStatus = statusDetailDao.findById(optionalUserRole.get().getUsRStatusDetail().getStatusId());
            optionalStatus.ifPresent(usRStatusDetail -> response.setStatusId(usRStatusDetail.getStatusId()));
            response.setUserRoleId(optionalUserRole.get().getUserRoleId());
            response.setUserRoleName(optionalUserRole.get().getUserRoleName());
            response.setUserRoleDesc(optionalUserRole.get().getUserRoleDesc());

        } else {
            throw new BadRequestAlertException("USER_ROLE_NOT_FOUND", ENTITY_NAME, "USER_ROLE_NOT_FOUND");
        }

        List<UsTMenuAction> activeMenuActionList = menuActionDao.getAllActivityMenuActionItems();

        if (activeMenuActionList != null) {
            List<RoleOptionsDto> roleOptions = new ArrayList<>();

            for (UsTMenuAction menuAction : activeMenuActionList) {

                RoleOptionsDto option = new RoleOptionsDto();

                option.setStatus(menuActionItems.contains(menuAction));
                option.setMenuActionId(menuAction.getMenuActionId());
                option.setMenuActionName(menuAction.getUsMAction().getActionName());
                option.setUrl(menuAction.getUsMMenu().getMenuUrl());
                option.setMenuName(menuAction.getUsMMenu().getMenuName());

                roleOptions.add(option);

            }
            response.setRoleOptions(roleOptions);
        } else {
            throw new BadRequestAlertException("NO_DATA_FOUND!", ENTITY_NAME, "ERROR");
        }
        response.setHasAccess(true);
        return response;

    }

    @Override
    public UsMUserRoleDto createUserRole(UsMUserRoleDto userRoleDto) {

        if (userRoleDto.getUserRoleName() == null) {
            throw new BadRequestAlertException("USER_ROLE_NAME_NULL", ENTITY_NAME, "USER_ROLE_NAME");
        }

        UsMUserRole optUserRole = userRoleDao.findByUserRoleName(userRoleDto.getUserRoleName());
        if (optUserRole != null) {
            throw new BadRequestAlertException("USER_ROLE_ALREADY_EXIST", ENTITY_NAME, "USER_ROLE");
        }

        UsMUserRoleDto userRole = new UsMUserRoleDto();
        userRole.setUserRoleDesc(userRoleDto.getUserRoleDesc());
        userRole.setUserRoleName(userRoleDto.getUserRoleName());
        userRole.setStatusFlag("N");
        userRole.setStatusId(3);
        userRole.setRoleInBussiness(userRoleDto.getUserRoleName());
        userRole.setRoleApprvReason(userRoleDto.getUserRoleName());
        userRole.setCCode(0);

        UsMUserRoleDto roleDto = userRoleMapper.toDto(userRoleDao.save(userRoleMapper.toEntity(userRole)));
        if (roleDto.getUserRoleId() != null) {
            return roleDto;
        } else {
            throw new BadRequestAlertException("USER_ROLE_CREATION_FAILED", ENTITY_NAME, "USER_ROLE_CREATION_FAILED");
        }

    }

    @Override
    public ResponseEntity<BranchListDto> getBranchForUser(Integer userId) {
        BranchListDto branchListDto = new BranchListDto();
        List<UsRUserBranch> uRUserBranchlist;
        List<UsRBrachDepartmentDto> usRBrachDepartmentlist = new ArrayList<>();
        uRUserBranchlist = usRUserBranchDao.findUsRUserBranchByAdMUserUserId(userId);

        if (!uRUserBranchlist.isEmpty()) {
            for (UsRUserBranch usRUserBranch : uRUserBranchlist) {
                UsRBrachDepartmentDto usRBrachDepartmentDto = new UsRBrachDepartmentDto();
                usRBrachDepartmentDto.setBranchId(usRUserBranch.getUsRBrachDepartment().getUsMBranch().getBranchId());
                System.out.println("---------" + usRUserBranch.getEmployeeBranchId());
                Optional<UsRBrachDepartment> usRBrachDepartmentOptional = usRBrachDepartmentDao.findById(usRUserBranch.getUsRBrachDepartment().getBrachDeptId());
                UsRBrachDepartmentDto branchDept = new UsRBrachDepartmentDto();
                if (usRBrachDepartmentOptional.isPresent()) {
                    branchDept = usRBrachDepartmentMapper.toDto(usRBrachDepartmentOptional.get());
                }
                branchListDto.setMessage("Success");
                usRBrachDepartmentlist.add(branchDept);
            }

        } else {
            System.out.println("inside else");
            branchListDto.setMessage("No Branch");
        }
        branchListDto.setUsRBrachDepartmentDto(usRBrachDepartmentlist);
        return ResponseEntity.ok(branchListDto);
    }


    @Override
    public ResponseEntity<String> deleteUserRole(Integer userRoleId) {

        if (userRoleId == null) {
            throw new BadRequestAlertException("USER_ROLE_ID_NULL", "ERROR", "ERROR");
        }
        Optional<UsMUserRole> optUserRole = userRoleDao.findById(userRoleId);

        List<AdMUser> userList = userDao.findAll().stream().filter(user -> Objects.equals(user.getUsMUserRole().getUserRoleId(), userRoleId)).collect(Collectors.toList());
        if (!userList.isEmpty()) {
            throw new BadRequestAlertException("THIS ROLE HAS ALREADY BEEN ASSIGNED TO USERS", "ERROR", "ERROR");
        }

        List<UsMMenuAllowedMap> allowList = menuAllowedMapDao.findAll().stream().filter(usMMenuAllowedMap -> Objects.equals(usMMenuAllowedMap.getUsMUserRole().getUserRoleId(), userRoleId)).collect(Collectors.toList());
        if (!allowList.isEmpty()) {
            throw new BadRequestAlertException("THIS ROLE HAS ALREADY BEEN ASSIGN TO ACTIONS", "ERROR", "ERROR");
        }

        if (optUserRole.isPresent()) {
            userRoleDao.deleteById(userRoleId);
            return ResponseEntity.ok("USER ROLE DELETED");
        } else {
            throw new BadRequestAlertException("USER ROLE NOT FOUND", "ERROR", "ERROR");
        }

    }

    @Override
    public ResponseEntity<UsMUserRoleDto> updateUserRole(UsMUserRoleDto userRoleDto) {

        if (userRoleDto.getUserRoleId() == null) {
            throw new BadRequestAlertException("USER ROLE ID NULL", "ERROR", "ERROR");
        }

        if (userRoleDto.getStatusId() == null) {
            throw new BadRequestAlertException("STATUS ID NULL", "ERROR", "ERROR");
        }

        Optional<UsRStatusDetail> statusDetail = statusDetailDao.findById(userRoleDto.getStatusId());
        if (!statusDetail.isPresent()) {
            throw new BadRequestAlertException("STATUS NOT FOUND", "ERROR", "ERROR");
        }

        Optional<UsMUserRole> optUserRole = userRoleDao.findById(userRoleDto.getUserRoleId());
        if (optUserRole.isPresent()) {
            UsMUserRole userRole = optUserRole.get();

            userRole.setCCode(userRoleDto.getCCode());
            userRole.setRoleApprvReason(userRoleDto.getRoleApprvReason());
            userRole.setRoleInBussiness(userRoleDto.getRoleInBussiness());
            userRole.setUserRoleDesc(userRoleDto.getUserRoleDesc());
            userRole.setStatusFlag(userRoleDto.getStatusFlag());
            userRole.setUserRoleName(userRoleDto.getUserRoleName());
            userRole.setUsRStatusDetail(statusDetail.get());

            UsMUserRole savedUserRole = userRoleDao.save(userRole);
            return ResponseEntity.ok(userRoleMapper.toDto(savedUserRole));

        } else {
            throw new BadRequestAlertException("USER ROLE NOT FOUND", "ERROR", "ERROR");
        }

    }


    @Override
    public ResponseEntity<UsMUserRoleDto> getUserRole(Integer userRoleId) {
        if (userRoleId == null) {
            throw new BadRequestAlertException("USER ROLE ID NULL", "ERROR", "ERROR");
        }
        Optional<UsMUserRole> optUserRole = userRoleDao.findById(userRoleId);
        if (optUserRole.isPresent()) {
            UsMUserRole userRole = optUserRole.get();
            return ResponseEntity.ok().body(userRoleMapper.toDto(userRole));
        } else {
            throw new BadRequestAlertException("USER ROLE NOT FOUND", "ERROR", "ERROR");
        }
    }

    @Override
    public ResponseEntity<List<UsMUserRoleDto>> getUserRolesForDropdown() {
        try {
            List<UsMUserRole> userRoleList = userRoleDao.findAll();
            if (userRoleList.isEmpty()) {
                throw new BadRequestAlertException("No data found", "ERROR", "ERROR");
            } else {
                return ResponseEntity.ok(userRoleMapper.listToDto(userRoleList));
            }
        } catch (Exception e) {
            throw new BadRequestAlertException(e.getMessage(), "ERROR", "ERROR");
        }
    }
}