package com.olak.goaml.userMGT.service.reference;

import com.olak.goaml.userMGT.dto.master.UsMDepartmentDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.other.BranchDepartmentCreateReqDto;
import com.olak.goaml.userMGT.dto.other.UserBranchCreateReqDto;
import com.olak.goaml.userMGT.dto.reference.UsRBrachDepartmentDto;
import com.olak.goaml.userMGT.dto.reference.UsRUserBranchDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsRBrachDepartmentService {
    ApiResponseDto<List<UsMDepartmentDto>> getDepartmentForBranchId(Integer branchId, int page, Integer per_page, String sort, String direction);

    ResponseEntity<Boolean> getValidationStatus(Integer branchId, Integer departmentId);

    ResponseEntity<List<UsRBrachDepartmentDto>> getBranchDepartmentForDropdown(Integer userId);

    ResponseEntity<UsRBrachDepartmentDto> createBranchDepartment(BranchDepartmentCreateReqDto branchDepartmentCreateReqDto);

    ResponseEntity<ApiResponseDto<List<UsRBrachDepartmentDto>>> getAllBranchDepartment(Integer page, Integer per_page, String search, String direction, String sort, Integer branchId, Integer departmentId);

    ResponseEntity<UsRBrachDepartmentDto> getBranchDepartmentById(Integer brachDeptId);

    ResponseEntity<UsRBrachDepartmentDto> updateBranchDepartment(Integer brachDeptId, UsRBrachDepartmentDto usRBrachDepartmentDto);

    ResponseEntity<Integer> deleteBranchDepartment(Integer brachDeptId);

    ResponseEntity<UsRUserBranchDto> createUserBranch(UserBranchCreateReqDto userBranchCreateReqDto);

    ResponseEntity<ApiResponseDto<List<UsRUserBranchDto>>> getAllUserBranch(Integer page, Integer per_page, String search, String direction, String sort, Integer userId, Integer brachDeptId, Integer statusId);

    ResponseEntity<UsRUserBranchDto> getUserBranchById(Integer employeeBranchId);

    ResponseEntity<UsRUserBranchDto> updateUserBranch(Integer employeeBranchId, UsRUserBranchDto usRUserBranchDto);

    ResponseEntity<Integer> deleteUserBranch(Integer employeeBranchId);
}
