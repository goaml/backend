package com.olak.goaml.userMGT.service.reference.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.userMGT.dao.reference.UsRStatusDetailDao;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import com.olak.goaml.userMGT.mappers.reference.UsRUserStatusDetailMapper;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.service.reference.UsRStatusDetailService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsRStatusDetailServiceImpl implements UsRStatusDetailService {

    private final UsRStatusDetailDao statusDetailDao;
    private final UsRUserStatusDetailMapper statusDetailMapper;

    public UsRStatusDetailServiceImpl(UsRStatusDetailDao statusDetailDao, UsRUserStatusDetailMapper statusDetailMapper) {
        this.statusDetailDao = statusDetailDao;
        this.statusDetailMapper = statusDetailMapper;
    }

    @Override
    public ApiResponseDto<List<UsRStatusDetailDto>> getAllStatus(Integer page, Integer per_page, String search, String sort, String direction) {
        Page<UsRStatusDetail> dbData;

        if (direction.equalsIgnoreCase("asc")) {
            dbData = statusDetailDao.getAllStatusForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else {
            dbData = statusDetailDao.getAllStatusForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.DESC, sort));
        }

        ApiResponseDto<List<UsRStatusDetailDto>> response = new ApiResponseDto<>();
        response.setResult(statusDetailMapper.entityListToDtoList(dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;
    }
}
