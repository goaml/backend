package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMUserRestrictionDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.other.*;
import com.olak.goaml.userMGT.dto.restriction.UserMenuDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsMUserRestrictionService {

    ResponseEntity<List<UserMenuDto>> getMenuForUser(Integer userId, Integer branchId);

    List<UsMUserRestrictionDto> saveUserRestrictions(UserRestrictionReqDto restrictionReq);

    ApiResponseDto<List<UserRestrictionDto>> getUserRestrictionForUserId(Integer userId, Integer branchId, int page, Integer per_page, String sort, String direction);

    ApiResponseDto<List<UserRestrictionDto>> getNewUserRestrictionForUserId(Integer userId, Integer branchId, int page, Integer per_page, String sort, String direction);

    ResponseDto updateUserRestriction(Integer userId, Integer branchId, Integer menuActionId);

    ResponseEntity<SideBarMenuDto> getSidebarMenu(Integer userId, Integer moduleId);

    ResponseEntity<List<SideMenuDto>> getSideMenuList(Integer userId, Integer moduleId);
}