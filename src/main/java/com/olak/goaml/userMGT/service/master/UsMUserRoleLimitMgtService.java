package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMUserRoleLimitMgtDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsMUserRoleLimitMgtService {
    ApiResponseDto<List<UsMUserRoleLimitMgtDto>> getUserRole(int page, Integer per_page, String search, String sort, String direction);

    ResponseEntity<UsMUserRoleLimitMgtDto> saveUserRole(UsMUserRoleLimitMgtDto usMUserRoleLimitMgtDto);

    UsMUserRoleLimitMgtDto updateUserRole(UsMUserRoleLimitMgtDto usMUserRoleLimitMgtDto);

    UsMUserRoleLimitMgtDto getUserRoleById(Integer roleLimitId);
}
