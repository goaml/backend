package com.olak.goaml.userMGT.service.reference;

import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.reference.UsRUserIdTypeDto;
import com.olak.goaml.userMGT.dto.reference.UserIdTypeReqDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsRUserIdTypeService {
    ResponseEntity<List<UsRUserIdTypeDto>> getUserTypesForDropdown();

    ResponseEntity<List<UsRUserIdTypeDto>> getUserTypesForDropdownWithoutStaff();

    ResponseEntity<UsRUserIdTypeDto> createUserIdType(UserIdTypeReqDto userIdTypeReqDto);

    ResponseEntity<UsRUserIdTypeDto> getUserIdType(Integer userTypeId);

    ResponseEntity<ApiResponseDto<List<UsRUserIdTypeDto>>> getAllUserIdType(Integer page, Integer perPage, String search, String direction, String sort, Integer loginTypeId);

    ResponseEntity<UsRUserIdTypeDto> updateUserIdType(Integer userTypeId, UsRUserIdTypeDto usRUserIdTypeDto);

    ResponseEntity<Integer> deleteUserIdType(Integer userTypeId);

}