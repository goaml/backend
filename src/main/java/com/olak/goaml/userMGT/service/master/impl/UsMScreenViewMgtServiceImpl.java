package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.userMGT.dao.master.UsMScreenViewMgtDao;
import com.olak.goaml.userMGT.dto.master.UsMScreenAttributesDto;
import com.olak.goaml.userMGT.dto.master.UsMScreenViewMgtDto;
import com.olak.goaml.userMGT.dto.other.AttributeListDto;
import com.olak.goaml.userMGT.mappers.master.UsMScreenViewMgtMapper;
import com.olak.goaml.userMGT.models.master.UsMScreenViewMgt;
import com.olak.goaml.userMGT.service.master.UsMScreenViewMgtService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsMScreenViewMgtServiceImpl implements UsMScreenViewMgtService {

    private final UsMScreenViewMgtMapper screenViewMgtMapper;
    private final UsMScreenViewMgtDao screenViewMgtDao;

    public UsMScreenViewMgtServiceImpl(UsMScreenViewMgtMapper screenViewMgtMapper, UsMScreenViewMgtDao screenViewMgtDao) {
        this.screenViewMgtMapper = screenViewMgtMapper;
        this.screenViewMgtDao = screenViewMgtDao;
    }

    @Override
    public List<UsMScreenViewMgtDto> createAttributeAccess(AttributeListDto attributeListDto) {

        List<UsMScreenViewMgtDto> response = new ArrayList<>();

        List<UsMScreenAttributesDto> attributesList = attributeListDto.getAttributeList();

        for (UsMScreenAttributesDto u : attributesList) {

            UsMScreenViewMgtDto saveDto = new UsMScreenViewMgtDto();
            saveDto.setScreenAttributeId(u.getScreenAttributeId());
            saveDto.setIsVisible(u.getIsVisible());
            saveDto.setMenuActionId(u.getMenuActionId());

            UsMScreenViewMgt usMScreenViewMgt = screenViewMgtDao.save(screenViewMgtMapper.toEntity(saveDto));
            response.add(screenViewMgtMapper.toDto(usMScreenViewMgt));
            System.out.println("Save List Item.. + " + usMScreenViewMgt.getScreenViewId());
        }
        return response;
    }
}
