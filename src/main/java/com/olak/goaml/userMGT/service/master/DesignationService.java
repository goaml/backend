package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.DesignationDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;

import java.util.List;

public interface DesignationService {
    ApiResponseDto<List<DesignationDto>> getAllDesignation(Integer page, Integer per_page, String search, String sort, String direction);
}
