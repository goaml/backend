package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.userMGT.dao.master.DesignationDao;
import com.olak.goaml.userMGT.dto.master.DesignationDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.mappers.master.DesignationMapper;
import com.olak.goaml.userMGT.models.master.Designation;
import com.olak.goaml.userMGT.service.master.DesignationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DesignationServiceImpl implements DesignationService {

    private final DesignationDao designationDao;
    private final DesignationMapper designationMapper;

    public DesignationServiceImpl(DesignationDao designationDao, DesignationMapper designationMapper) {
        this.designationDao = designationDao;
        this.designationMapper = designationMapper;
    }

    @Override
    public ApiResponseDto<List<DesignationDto>> getAllDesignation(Integer page, Integer per_page, String search, String sort, String direction) {

        Page<Designation> dbData;

        if (direction.equalsIgnoreCase("asc")) {
            dbData = designationDao.getAllDesignationsForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else {
            dbData = designationDao.getAllDesignationsForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.DESC, sort));
        }

        ApiResponseDto<List<DesignationDto>> response = new ApiResponseDto<>();
        response.setResult(designationMapper.listToDto(dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;
    }
}
