package com.olak.goaml.userMGT.service.reference;

import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;

import java.util.List;

public interface UsRStatusDetailService {
    ApiResponseDto<List<UsRStatusDetailDto>> getAllStatus(Integer page, Integer per_page, String search, String sort, String direction);
}
