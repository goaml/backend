package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMMenuAllowedMapDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.other.ResponseDto;
import com.olak.goaml.userMGT.dto.other.RoleRestrictionCreateDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsMMenuAllowedMapService {
    UsMMenuAllowedMapDto saveRoleRestriction(UsMMenuAllowedMapDto menuAllowedMapDto);

    ApiResponseDto<List<UsMMenuAllowedMapDto>> getUserRoleRestriction(Integer userRoleId, Integer statusId, int page, Integer per_page, String sort, String direction);

    ResponseEntity<List<UsMMenuAllowedMapDto>> saveRoleRestrictionList(List<RoleRestrictionCreateDto> roleRestrictionCreateDtoList);

    ResponseEntity<ResponseDto> updateRoleRestriction(Integer userRoleId, Integer menuActionId);
}
