package com.olak.goaml.userMGT.service.other;

import com.olak.goaml.error.BadRequestAlertException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SMSMailConfig {

    public static void sendNotification(String toMobileNo, String toMail, String mailSubject, String mailBody, String smsBody, String notificationMode) {
        try {
            if (notificationMode.equalsIgnoreCase("E")) {
                if (toMail != null && !toMail.isEmpty()) {
                    MailService.sendEMail(toMail, mailSubject, mailBody);
                }
            } else if (notificationMode.equalsIgnoreCase("S")) {
                if (toMobileNo != null && !toMobileNo.isEmpty()) {
                    SMSService.sendSMS(toMobileNo, smsBody);
                }
            } else if (notificationMode.equalsIgnoreCase("B")) {
                if (toMail != null && !toMail.isEmpty()) {
                    MailService.sendEMail(toMail, mailSubject, mailBody);
                }
                if (toMobileNo != null && !toMobileNo.isEmpty()) {
                    SMSService.sendSMS(toMobileNo, smsBody);
                }
            } else {
                log.error("Invalid Notification Mode");
                throw new BadRequestAlertException("Invalid Notification Mode", "SMSMailConfig", "Invalid Notification Mode");
            }
        } catch (Exception e) {
            log.error("{}", e);
            e.fillInStackTrace();
        }
    }

}