package com.olak.goaml.userMGT.service.other;

import com.olak.goaml.constants.HardCodeConstants;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.master.AdMUserDao;
import com.olak.goaml.userMGT.models.master.AdMUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.Random;

@Slf4j
@Service
public class OTPService {

    private final AdMUserDao userDao;

    public OTPService(AdMUserDao userDao) {
        this.userDao = userDao;
    }

    public static String generateOTP() {
        Random random = new Random();
        return String.valueOf(100000 + random.nextInt(900000));
    }

    public void sendOTP(String userId) {
        try {

            String otp = generateOTP();
            log.info("OTP : {}", otp);

            //save otp in db
            if (userId.isEmpty()) {
                throw new BadRequestAlertException("Invalid User Id", "OTPService", "Invalid User Id");
            } else {
                if (userDao.findByUserName(userId).isPresent()) {
                    //save otp in db
                    AdMUser user = userDao.findByUserName(userId).get();
                    user.setOtp(otp);
                    user.setOtpGeneratedTime(LocalTime.now());
                    userDao.save(user);
                    log.info("OTP saved in db");

                    String msg = "Your One Time Password is " + otp + ". Please use this code for verification.";
                    String mailSubject = "One Time Password";

                    //send otp to user
                    SMSMailConfig.sendNotification(user.getMobileNo(), user.getEmail(), mailSubject, msg, msg, "B");
                    log.info("OTP sent to user");
                } else {
                    throw new BadRequestAlertException("Invalid User Id", "OTPService", "Invalid User Id");
                }
            }

        } catch (Exception e) {
            e.fillInStackTrace();
            log.error("Error : {}", e.getMessage());
            throw new BadRequestAlertException("OTP Sending Failed", "OTPService", "OTP Sending Failed");
        }
    }

    public boolean validateOTP(String userId, String otp) {
        try {
            if (userId.isEmpty()) {
                throw new BadRequestAlertException("Invalid User Id", "OTPService", "Invalid User Id");
            } else {
                if (userDao.findByUserName(userId).isPresent()) {
                    //validate otp
                    AdMUser user = userDao.findByUserName(userId).get();

                    //Check time
                    int length = LocalTime.now().getMinute() - user.getOtpGeneratedTime().getMinute();
                    log.info("OTP validation time : {}", length + " minutes");
                    if (length >= HardCodeConstants.OTP_EXPIRE_TIME) {
                        throw new BadRequestAlertException("OTP Expired", "OTPService", "OTP Expired");
                    }

                    log.info("OTP validation : {}", user.getOtp().equals(otp));
                    return user.getOtp().equals(otp);
                } else {
                    throw new BadRequestAlertException("Invalid User Id", "OTPService", "Invalid User Id");
                }
            }
        } catch (Exception e) {
            log.error("Error : {}", e);
            throw new BadRequestAlertException("OTP Validation Failed", "OTPService", "OTP Validation Failed");
        }
    }

}