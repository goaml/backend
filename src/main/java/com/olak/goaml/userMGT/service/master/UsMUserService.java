package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMUserDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.other.GetUserResponseDto;
import com.olak.goaml.userMGT.dto.other.ResponseDto;
import com.olak.goaml.userMGT.dto.user.AdMUserDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsMUserService {
    ResponseDto createUser(UsMUserDto usMUserDto);

    ResponseDto updateUser(UsMUserDto usMUserDto);

    ApiResponseDto<List<GetUserResponseDto>> getUsers(int page, Integer per_page, String sort, String direction, String search, Integer statusId);

    GetUserResponseDto getUserById(Integer userId);

    ResponseEntity<List<AdMUserDto>> getUsersForDropdown();

}
