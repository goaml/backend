package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.constants.HardCodeConstants;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.master.UsMMenuAllowedMapDao;
import com.olak.goaml.userMGT.dao.master.UsMUserRoleDao;
import com.olak.goaml.userMGT.dao.reference.UsRStatusDetailDao;
import com.olak.goaml.userMGT.dao.transaction.UsTMenuActionDao;
import com.olak.goaml.userMGT.dto.master.UsMMenuAllowedMapDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.other.ResponseDto;
import com.olak.goaml.userMGT.dto.other.RoleRestrictionCreateDto;
import com.olak.goaml.userMGT.mappers.master.UsMMenuAllowMapMapper;
import com.olak.goaml.userMGT.models.master.UsMMenuAllowedMap;
import com.olak.goaml.userMGT.models.master.UsMUserRole;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import com.olak.goaml.userMGT.service.master.UsMMenuAllowedMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class UsMMenuAllowedMapServiceImpl implements UsMMenuAllowedMapService {

    private final UsTMenuActionDao menuActionDao;
    private final UsRStatusDetailDao statusDetailDao;
    private final UsMUserRoleDao userRoleDao;
    private final UsMMenuAllowedMapDao menuAllowedMapDao;
    private final UsMMenuAllowMapMapper menuAllowMapMapper;

    public UsMMenuAllowedMapServiceImpl(UsTMenuActionDao menuActionDao, UsRStatusDetailDao statusDetailDao, UsMUserRoleDao userRoleDao, UsMMenuAllowedMapDao menuAllowedMapDao, UsMMenuAllowMapMapper menuAllowMapMapper) {
        this.menuActionDao = menuActionDao;
        this.statusDetailDao = statusDetailDao;
        this.userRoleDao = userRoleDao;
        this.menuAllowedMapDao = menuAllowedMapDao;
        this.menuAllowMapMapper = menuAllowMapMapper;
    }

    /**
     * @param menuAllowedMapDto
     * @return
     */
    @Override
    public UsMMenuAllowedMapDto saveRoleRestriction(UsMMenuAllowedMapDto menuAllowedMapDto) {

        if (menuAllowedMapDto.getMenuActionId() == null) {
            throw new BadRequestAlertException("MENU_ACTION_ID NULL", ENTITY_NAME, "MENU_ACTION_ID");
        }
        if (menuAllowedMapDto.getStatusId() == null) {
            throw new BadRequestAlertException("STATUS_ID NULL", ENTITY_NAME, "STATUS_ID");
        }
        if (menuAllowedMapDto.getUserRoleId() == null) {
            throw new BadRequestAlertException("USER_ROLE_ID NULL", ENTITY_NAME, "USER_ROLE_ID");
        }

        UsMMenuAllowedMap saveMenu = new UsMMenuAllowedMap();

        Optional<UsTMenuAction> menuAction = menuActionDao.findById(menuAllowedMapDto.getMenuActionId());
        Optional<UsRStatusDetail> statusDetail = statusDetailDao.findById(menuAllowedMapDto.getStatusId());
        Optional<UsMUserRole> userRole = userRoleDao.findById(menuAllowedMapDto.getUserRoleId());

        if (menuAction.isPresent()) {
            if (statusDetail.isPresent()) {
                if (userRole.isPresent()) {
                    saveMenu.setUsTMenuAction(menuAction.get());
                    saveMenu.setUsRStatusDetail(statusDetail.get());
                    saveMenu.setUsMUserRole(userRole.get());
                } else {
                    throw new BadRequestAlertException("USER_ROLE_ID NOT FOUND", ENTITY_NAME, "USER_ROLE_ID");
                }
            } else {
                throw new BadRequestAlertException("STATUS_ID NOT FOUND", ENTITY_NAME, "STATUS_ID");
            }
        } else {
            throw new BadRequestAlertException("MENU_ACTION_ID NOT FOUND", ENTITY_NAME, "MENU_ACTION_ID");
        }

        UsMMenuAllowedMap allowedMap = menuAllowedMapDao.save(saveMenu);
        UsMMenuAllowedMapDto response = new UsMMenuAllowedMapDto();
        response.setMenuAllowMapId(allowedMap.getMenuAllowMapId());
        response.setUserRoleId(allowedMap.getUsMUserRole().getUserRoleId());
        response.setMenuActionId(allowedMap.getUsTMenuAction().getMenuActionId());
        response.setStatusId(allowedMap.getUsRStatusDetail().getStatusId());
        return response;
    }


    @Override
    public ApiResponseDto<List<UsMMenuAllowedMapDto>> getUserRoleRestriction(Integer userRoleId, Integer statusId, int page, Integer per_page, String sort, String direction) {

        Page<UsMMenuAllowedMap> dbData;
        if (userRoleId != null && statusId != null) {
            dbData = menuAllowedMapDao.findMenuAllowMapByUserRoleIdAndStatusId(userRoleId, statusId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else if (direction.equalsIgnoreCase("asc")) {
            dbData = menuAllowedMapDao.findMenuAllowMapForTable(userRoleId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else {
            dbData = menuAllowedMapDao.findMenuAllowMapForTable(userRoleId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
        }

        ApiResponseDto<List<UsMMenuAllowedMapDto>> response = new ApiResponseDto<>();
        response.setResult(menuAllowMapMapper.listToDto(dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);
        return response;
    }

    @Override
    public ResponseEntity<List<UsMMenuAllowedMapDto>> saveRoleRestrictionList(List<RoleRestrictionCreateDto> roleRestrictionCreateDtoList) {
        try {

            roleRestrictionCreateDtoList.forEach(res -> {
                if (res.getMenuActionId() == null) {
                    throw new BadRequestAlertException("MENU_ACTION_ID NULL", ENTITY_NAME, "MENU_ACTION_ID");
                } else if (res.getUserRoleId() == null) {
                    throw new BadRequestAlertException("USER_ROLE_ID NULL", ENTITY_NAME, "USER_ROLE_ID");
                } else {
                    log.info("No Null Value Found. Process Continue");
                }
            });

            List<UsMMenuAllowedMap> allowedMapList = new ArrayList<>();
            roleRestrictionCreateDtoList.forEach(res -> {
                Optional<UsTMenuAction> menuAction = menuActionDao.findById(res.getMenuActionId());
                Optional<UsRStatusDetail> statusDetail = statusDetailDao.findById(HardCodeConstants.ACTIVE_AUTHORIZED_STATUS_ID);
                Optional<UsMUserRole> userRole = userRoleDao.findById(res.getUserRoleId());

                if (!menuAction.isPresent()) {
                    throw new BadRequestAlertException("MENU_ACTION NOT FOUND", ENTITY_NAME, "MENU_ACTION");
                } else if (!statusDetail.isPresent()) {
                    throw new BadRequestAlertException("STATUS NOT FOUND", ENTITY_NAME, "STATUS");
                } else if (!userRole.isPresent()) {
                    throw new BadRequestAlertException("USER_ROLE NOT FOUND", ENTITY_NAME, "USER_ROLE");
                } else {
                    log.info("All Data Found. Process Continue");

                    Optional<UsMMenuAllowedMap> optAllowMap = menuAllowedMapDao.findByUsMUserRoleUserRoleIdAndUsTMenuActionMenuActionId(userRole.get().getUserRoleId(), menuAction.get().getMenuActionId());
                    if (optAllowMap.isPresent()) {
                        throw new BadRequestAlertException("Some Role Restriction already exist", ENTITY_NAME, "Error");
                    }

                    UsMMenuAllowedMap allowedMap = new UsMMenuAllowedMap();
                    allowedMap.setUsMUserRole(userRole.get());
                    allowedMap.setUsRStatusDetail(statusDetail.get());
                    allowedMap.setUsTMenuAction(menuAction.get());

                    allowedMapList.add(allowedMap);
                }
            });

            List<UsMMenuAllowedMap> savedList = menuAllowedMapDao.saveAll(allowedMapList);
            savedList.forEach(res -> {
                if (res.getMenuAllowMapId() == null) {
                    throw new BadRequestAlertException("Role restriction save request failed", ENTITY_NAME, "Error");
                }
            });

            return ResponseEntity.ok(menuAllowMapMapper.listToDto(savedList));

        } catch (Exception e) {
            log.error("Error", e);
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "Error");
        }
    }

    @Override
    public ResponseEntity<ResponseDto> updateRoleRestriction(Integer userRoleId, Integer menuActionId) {
        try {
            if (userRoleId == null) {
                throw new BadRequestAlertException("USER_ROLE_ID NULL", ENTITY_NAME, "USER_ROLE_ID");
            } else if (menuActionId == null) {
                throw new BadRequestAlertException("MENU_ACTION_ID NULL", ENTITY_NAME, "MENU_ACTION_ID");
            } else {
                log.info("No Null Value Found. Process Continue");
            }

            Optional<UsMUserRole> optUserRole = userRoleDao.findById(userRoleId);
            Optional<UsTMenuAction> optMenuAction = menuActionDao.findById(menuActionId);
            if (!optUserRole.isPresent()) {
                throw new BadRequestAlertException("USER_ROLE NOT FOUND", ENTITY_NAME, "USER_ROLE");
            } else if (!optMenuAction.isPresent()) {
                throw new BadRequestAlertException("MENU_ACTION NOT FOUND", ENTITY_NAME, "MENU_ACTION");
            } else {
                log.info("All Data Found. Process Continue");
            }

            Optional<UsMMenuAllowedMap> optAllowMap = menuAllowedMapDao.findByUsMUserRoleUserRoleIdAndUsTMenuActionMenuActionId(userRoleId, menuActionId);
            if (!optAllowMap.isPresent()) {
                throw new BadRequestAlertException("Role Restriction not found", ENTITY_NAME, "Error");
            } else {
                log.info("Role Restriction Found. Process Continue");
                menuAllowedMapDao.deleteById(optAllowMap.get().getMenuAllowMapId());
                if (menuAllowedMapDao.findById(optAllowMap.get().getMenuAllowMapId()).isPresent()) {
                    throw new BadRequestAlertException("Role Restriction remove request failed", ENTITY_NAME, "Error");
                } else {
                    log.info("Role Restriction Removed. Process Continue");
                    ResponseDto responseDto = new ResponseDto();
                    responseDto.setData("");
                    responseDto.setMessage("Role Restriction removed successfully");
                    return ResponseEntity.ok(responseDto);
                }
            }

        } catch (Exception e) {
            log.error("Error", e);
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "Error");
        }
    }
}
