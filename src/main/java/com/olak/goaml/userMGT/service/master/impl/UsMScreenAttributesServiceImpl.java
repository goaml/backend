package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.master.UsMScreenAttributesDao;
import com.olak.goaml.userMGT.dto.master.UsMScreenAttributesDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.mappers.master.UsMScreenAttributesMapper;
import com.olak.goaml.userMGT.models.master.UsMScreenAttributes;
import com.olak.goaml.userMGT.service.master.UsMScreenAttributesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class UsMScreenAttributesServiceImpl implements UsMScreenAttributesService {

    private final UsMScreenAttributesDao screenAttributesDao;
    private final UsMScreenAttributesMapper screenAttributesMapper;

    public UsMScreenAttributesServiceImpl(UsMScreenAttributesDao screenAttributesDao, UsMScreenAttributesMapper screenAttributesMapper) {
        this.screenAttributesDao = screenAttributesDao;
        this.screenAttributesMapper = screenAttributesMapper;
    }

    @Override
    public UsMScreenAttributesDto createAttribute(UsMScreenAttributesDto usMScreenAttributesDto) {
        try {
            if (usMScreenAttributesDto.getAttributeName().isEmpty() || usMScreenAttributesDto.getAttributeName() == null) {
                throw new BadRequestAlertException("Attribute Name is required", ENTITY_NAME, "ERROR");
            }
            if (usMScreenAttributesDto.getMenuActionId() == null) {
                throw new BadRequestAlertException("Menu Action Id is required", ENTITY_NAME, "ERROR");
            }
            if (usMScreenAttributesDto.getIsVisible() == null) {
                throw new BadRequestAlertException("Is Visible is required", ENTITY_NAME, "ERROR");
            }

            UsMScreenAttributes usMScreenAttributes = screenAttributesMapper.toEntity(usMScreenAttributesDto);
            usMScreenAttributes = screenAttributesDao.save(usMScreenAttributes);
            if (usMScreenAttributes == null) {
                throw new BadRequestAlertException("Failed to create attribute", ENTITY_NAME, "ERROR");
            } else {
                return screenAttributesMapper.toDto(usMScreenAttributes);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ApiResponseDto<List<UsMScreenAttributesDto>> getAllAttributes(Integer page, Integer per_page, String search, String sort, String direction) {

        Page<UsMScreenAttributes> dbData;

        if (direction.equalsIgnoreCase("asc")) {
            dbData = screenAttributesDao.getAllAttributesForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else {
            dbData = screenAttributesDao.getAllAttributesForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.DESC, sort));
        }

        ApiResponseDto<List<UsMScreenAttributesDto>> response = new ApiResponseDto<>();
        response.setResult(screenAttributesMapper.listToDto(dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;

    }

}
