package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.constants.HardCodeConstants;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.master.*;
import com.olak.goaml.userMGT.dao.reference.UsRStatusDetailDao;
import com.olak.goaml.userMGT.dao.reference.UsRUserBranchDao;
import com.olak.goaml.userMGT.dao.transaction.UsTMenuActionDao;
import com.olak.goaml.userMGT.dto.master.UsMUserRestrictionDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.other.*;
import com.olak.goaml.userMGT.dto.restriction.UserMenuDto;
import com.olak.goaml.userMGT.mappers.master.UsMUserRestrictionMapper;
import com.olak.goaml.userMGT.mappers.transaction.UsTMenuActionMapper;
import com.olak.goaml.userMGT.models.master.*;
import com.olak.goaml.userMGT.models.reference.UsRBrachDepartment;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.reference.UsRUserBranch;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import com.olak.goaml.userMGT.service.master.UsMUserRestrictionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Service
@Slf4j
@Transactional
public class UsMUserRestrictionServiceImpl implements UsMUserRestrictionService {
    private final UsMMenuSubGroupDao usMMenuSubGroupDao;
    private final UsMMenuGroupDao usMMenuGroupDao;
    private final UsMUserRestrictionMapper usMUserRestrictionMapper;
    private final UsMUserRestrictionDao usMUserRestrictionDao;
    private final AdMUserDao adMUserDao;
    private final UsTMenuActionMapper usTMenuActionMapper;
    private final UsRStatusDetailDao statusDetailDao;
    private final UsRUserBranchDao userBranchDao;
    private final UsTMenuActionDao menuActionDao;
    private final UsMMenuAllowedMapDao menuAllowedMapDao;

    public UsMUserRestrictionServiceImpl(UsMMenuSubGroupDao usMMenuSubGroupDao, UsMMenuGroupDao usMMenuGroupDao, UsMUserRestrictionMapper usMUserRestrictionMapper, UsMUserRestrictionDao usMUserRestrictionDao, AdMUserDao adMUserDao, UsTMenuActionMapper usTMenuActionMapper, UsRStatusDetailDao statusDetailDao, UsRUserBranchDao userBranchDao, UsTMenuActionDao menuActionDao, UsMMenuAllowedMapDao menuAllowedMapDao) {
        this.usMMenuSubGroupDao = usMMenuSubGroupDao;
        this.usMMenuGroupDao = usMMenuGroupDao;
        this.usMUserRestrictionMapper = usMUserRestrictionMapper;
        this.usMUserRestrictionDao = usMUserRestrictionDao;
        this.adMUserDao = adMUserDao;
        this.usTMenuActionMapper = usTMenuActionMapper;
        this.statusDetailDao = statusDetailDao;
        this.userBranchDao = userBranchDao;
        this.menuActionDao = menuActionDao;
        this.menuAllowedMapDao = menuAllowedMapDao;
    }

    private static SideMenuDto getSideMenuDto(UsMMenuGroup group, UsMMenuSubGroup subGroup, UsTMenuAction s) {
        SideMenuDto sideMenuDto = new SideMenuDto();
        sideMenuDto.setGroup(group.getMenuGroupName());
        sideMenuDto.setGroupSeqNo(group.getSeqNo());
        sideMenuDto.setSubGroup(subGroup.getSubGroupName());
        sideMenuDto.setSubGroupSeqNo(subGroup.getSeqNo());
        sideMenuDto.setId(s.getUsMMenu().getMenuId());
        sideMenuDto.setTitle(s.getUsMMenu().getMenuName());
        sideMenuDto.setMenuSeqNo(s.getUsMMenu().getSeqNo());
        sideMenuDto.setUrl(s.getUrl());
        sideMenuDto.setIcon(s.getUsMMenu().getMenuIcon());
        return sideMenuDto;
    }


//    @Override
//    public ResponseEntity<UsMUserRestrictionDto> saveUserRestriction(UsMUserRestrictionDto usMUserRestrictionDto) {
//        return new ResponseEntity<>(usMUserRestrictionMapper.toDto(usMUserRestrictionDao.save(usMUserRestrictionMapper.toEntity(usMUserRestrictionDto))), HttpStatus.OK);
//    }
//
//    @Override
//    public ApiResponseDto<List<UsMUserRestrictionDto>> getUserRestriction(int page, Integer per_page, String search, String sort, String direction) {
//        List<SearchPassDto> searchList = new ArrayList<>();
//
//        List<FilterPassDto> filterList = new ArrayList<>();
//
//
//        searchNFilter.setPaginationDetails(page, per_page, sort, direction);
//        searchNFilter.setVariables(search, ActiveStatus.ACTIVE.getValue());
//        searchNFilter.setEntityClass(UsMUserRestriction.class);
//        searchNFilter.setSearchList(searchList);
//        searchNFilter.setFilterList(filterList);
//
//        Page<?> dbData = searchNFilter.getFiltered();
//
//
//        ApiResponseDto<List<UsMUserRestrictionDto>> response = new ApiResponseDto<>();
//        response.setResult(usMUserRestrictionMapper.listToDto((List<UsMUserRestriction>) dbData.getContent()));
//        PaginationDto paginationDto = new PaginationDto();
//        paginationDto.setTotal((int) dbData.getTotalElements());
//        response.setPagination(paginationDto);
//
//        return response;
///*
//        Page<UsMUserRestriction> dbData;
//
//        if (direction.equalsIgnoreCase("asc")) {
//            dbData = usMUserRestrictionDao.findByUserRestrictionForTable(search, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
//        } else {
//            dbData = usMUserRestrictionDao.findByUserRestrictionForTable(search, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
//        }
//
//        ApiResponseDto<List<UsMUserRestrictionDto>> response = new ApiResponseDto<>();
//        response.setResult(usMUserRestrictionMapper.listToDto(dbData.getContent()));
//
//        PaginationDto paginationDto = new PaginationDto();
//        paginationDto.setTotal((int) dbData.getTotalElements());
//        response.setPagination(paginationDto);
//
//        System.out.println(response);
//        return response;*/
//    }
//
//    @Override
//    public UsMUserRestrictionDto updateUserRestriction(UsMUserRestrictionDto usMUserRestrictionDto) {
//        return usMUserRestrictionMapper.toDto(usMUserRestrictionDao.save(usMUserRestrictionMapper.toEntity(usMUserRestrictionDto)));
//    }

    @Override
    public ResponseEntity<List<UserMenuDto>> getMenuForUser(Integer userId, Integer branchId) {
        try {
            Optional<AdMUser> user = adMUserDao.findByUserIdAndIsActive(userId, ActiveStatus.ACTIVE.getValue());

            if (user.isPresent()) {
                AdMUser us = user.get();

                List<UserMenuDto> menu = new ArrayList<>();

                List<UsMMenuAllowedMap> roleAllowed = us.getUsMUserRole().getUsMMenuAllowedMaps();

                Optional<UsRUserBranch> UserBranch = us.getUsRUserBranches()
                        .stream()
                        .peek(obj -> System.out.println("branch-id  " + obj.getUsRBrachDepartment().getUsMBranch().getBranchId()))
                        .filter(obj -> obj.getUsRBrachDepartment().getUsMBranch().getBranchId() == branchId)
                        .findAny();

                if (UserBranch.isPresent()) {
                    List<UsMUserRestriction> userRestrictions = UserBranch.get().getUsMUserRestrictions();

                    userRestrictions.forEach(userRestriction -> {
                        if (userRestriction.getUsRStatusDetail().getStatusId() == 3) {
                            UserMenuDto menuItem = new UserMenuDto();

                            menuItem.setMenuAction(usTMenuActionMapper.toDto(userRestriction.getUsTMenuAction()));
                            menuItem.setTransactionLimit(userRestriction.getTransactionLimit());
                            menu.add(menuItem);
                        }
                    });

                    roleAllowed.forEach(roleRestriction -> {
                        if (!userRestrictions.stream().anyMatch(userRestriction -> userRestriction.getUsTMenuAction() == roleRestriction.getUsTMenuAction())) {
                            UserMenuDto menuItem = new UserMenuDto();

                            menuItem.setMenuAction(usTMenuActionMapper.toDto(roleRestriction.getUsTMenuAction()));
                            menu.add(menuItem);
                        }
                    });

                    return new ResponseEntity<>(menu, HttpStatus.OK);
                } else {
                    System.out.println("Branch not found");
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            } else {
                System.out.println("user not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            e.fillInStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<UsMUserRestrictionDto> saveUserRestrictions(UserRestrictionReqDto restrictionReq) {

        List<UsMUserRestrictionDto> response = new ArrayList<>();

        Integer listSize = restrictionReq.getUserRestrictionList().size();
        System.out.println(listSize);

        Optional<UsRUserBranch> userBranch = null;

        for (UsMUserRestrictionDto listItem : restrictionReq.getUserRestrictionList()) {

            Optional<UsRStatusDetail> statusDetail = statusDetailDao.findById(listItem.getStatusId());
            userBranch = userBranchDao.findUsRUserBranchByAdMUserUserIdAndUsRBrachDepartmentUsMBranchBranchId(listItem.getUserId(), listItem.getBranchId());
            Optional<UsTMenuAction> menuAction = menuActionDao.findById(listItem.getMenuActionId());

            if (statusDetail.isPresent()) {
                System.out.println("STATUS_ID FOUND");
                if (userBranch.isPresent()) {
                    System.out.println("EMPLOYEE_BRANCH_ID");
                    if (menuAction.isPresent()) {
                        System.out.println("MENU_ACTION_ID");
                    } else {
                        throw new BadRequestAlertException("MENU_ACTION_ID NOT FOUND", ENTITY_NAME, "MENU_ACTION_ID");
                    }
                } else {
                    throw new BadRequestAlertException("EMPLOYEE_BRANCH_ID NOT FOUND", ENTITY_NAME, "EMPLOYEE_BRANCH_ID");
                }
            } else {
                throw new BadRequestAlertException("STATUS_ID NOT FOUND", ENTITY_NAME, "STATUS_ID");
            }
        }

        for (UsMUserRestrictionDto restrictionDto : restrictionReq.getUserRestrictionList()) {

            UsMUserRestriction saveRestriction = new UsMUserRestriction();
            saveRestriction.setFrom(restrictionDto.getFrom());
            saveRestriction.setReason(restrictionDto.getReason());
            saveRestriction.setTo(restrictionDto.getTo());
            saveRestriction.setTransactionLimit(restrictionDto.getTransactionLimit());
            saveRestriction.setUsRStatusDetail(statusDetailDao.findById(restrictionDto.getStatusId()).get());
            saveRestriction.setUsRUserBranch(userBranch.get());
            saveRestriction.setUsTMenuAction(menuActionDao.findById(restrictionDto.getMenuActionId()).get());

            UsMUserRestriction userRestriction = usMUserRestrictionDao.save(saveRestriction);

            if (userRestriction.getUserRestrictionId() != null) {
                UsMUserRestrictionDto userRestrictionDto = usMUserRestrictionMapper.toDto(userRestriction);
                userRestrictionDto.setStatusId(userRestriction.getUsRStatusDetail().getStatusId());
                userRestrictionDto.setEmployeeBranchId(userRestriction.getUsRUserBranch().getEmployeeBranchId());
                userRestrictionDto.setMenuActionId(userRestriction.getUsTMenuAction().getMenuActionId());
                response.add(userRestrictionDto);
            } else {
                System.out.println("Failed");
            }
        }
        return response;
    }

    //Developed By Gayashan 2022-12-03
    @Override
    public ApiResponseDto<List<UserRestrictionDto>> getUserRestrictionForUserId(Integer userId, Integer branchId, int page, Integer per_page, String sort, String direction) {

        List<UserRestrictionDto> responseList = new ArrayList<>();

        List<Object[]> restrictionList = usMUserRestrictionDao.findRestrictionListForTable(userId, branchId);
        List<Object[]> allowMapList = menuAllowedMapDao.findAllowMapListForTable(userId);

        List<UserRestrictionDto> userRestrictionList = new ArrayList<>();
        List<UserRestrictionDto> userAllowMapList = new ArrayList<>();

        for (Object[] objt : allowMapList) {
            if (objt[0] instanceof AdMUser && objt[1] instanceof UsMUserRole && objt[2] instanceof UsMMenuAllowedMap && objt[3] instanceof UsMMenu && objt[4] instanceof UsMModule && objt[5] instanceof UsMAction && objt[6] instanceof UsTMenuAction) {
                AdMUser user = (AdMUser) objt[0];
                UsMUserRole userRole = (UsMUserRole) objt[1];
                UsMMenuAllowedMap allowedMap = (UsMMenuAllowedMap) objt[2];
                UsMMenu menu = (UsMMenu) objt[3];
                UsMModule module = (UsMModule) objt[4];
                UsMAction action = (UsMAction) objt[5];
                UsTMenuAction menuAction = (UsTMenuAction) objt[6];

                UserRestrictionDto allowList = new UserRestrictionDto();
                allowList.setMenuActionId(menuAction.getMenuActionId());
                allowList.setMenuActionName(menuAction.getName());
                allowList.setModuleId(module.getModuleId());
                allowList.setModuleName(module.getModuleName());
                allowList.setMenuId(menu.getMenuId());
                allowList.setMenuName(menu.getMenuName());
                allowList.setActionId(action.getActionId());
                allowList.setActionName(action.getActionName());
                allowList.setStatusId(menuAction.getUsRStatusDetail().getStatusId());

                userAllowMapList.add(allowList);
            }
        }

        for (Object[] obj : restrictionList) {
            if (obj[0] instanceof AdMUser && obj[1] instanceof UsRUserBranch && obj[2] instanceof UsRBrachDepartment && obj[3] instanceof UsTMenuAction && obj[4] instanceof UsMUserRestriction && obj[5] instanceof UsMModule && obj[6] instanceof UsMAction && obj[7] instanceof UsMMenu) {
                AdMUser user = (AdMUser) obj[0];
                UsRUserBranch userBranch = (UsRUserBranch) obj[1];
                UsRBrachDepartment brachDepartment = (UsRBrachDepartment) obj[2];
                UsTMenuAction menuAction = (UsTMenuAction) obj[3];
                UsMUserRestriction userRestriction = (UsMUserRestriction) obj[4];
                UsMModule module = (UsMModule) obj[5];
                UsMAction action = (UsMAction) obj[6];
                UsMMenu menu = (UsMMenu) obj[7];

                UserRestrictionDto restriction = new UserRestrictionDto();
                restriction.setMenuActionId(menuAction.getMenuActionId());
                restriction.setMenuActionName(menuAction.getName());
                restriction.setModuleId(module.getModuleId());
                restriction.setModuleName(module.getModuleName());
                restriction.setMenuId(menu.getMenuId());
                restriction.setMenuName(menu.getMenuName());
                restriction.setActionId(action.getActionId());
                restriction.setActionName(action.getActionName());
                restriction.setStatusId(userRestriction.getUsRStatusDetail().getStatusId());

                userRestrictionList.add(restriction);
            }
        }

        List<UserRestrictionDto> filterList = new ArrayList<>();
//        filterList.addAll(userAllowMapList);
        filterList.addAll(userRestrictionList);

//        for (int s = 0; s < userAllowMapList.size(); s++) {
//            for (int f = s; f < userRestrictionList.size(); f++) {
//                if (!userAllowMapList.contains(userRestrictionList.get(f))) {
//                    filterList.add(userRestrictionList.get(f));
//                }
//                break;
//            }
//        }

        for (UserRestrictionDto m : userAllowMapList) {
            Optional<UserRestrictionDto> restriction = userRestrictionList.stream().filter(item -> item.getMenuActionId() == m.getMenuActionId()).findAny();
            if (!restriction.isPresent()) {
                System.out.println(m.getMenuActionId());
                filterList.add(m);
            } else {
                System.out.println();
            }
        }
//
//        System.out.println("FILTER LIST SIZE : " + filterList.size());
//        System.out.println("RESTRICTION LIST : " + restrictionList.size());
//        System.out.println("ALLOW MAP LIST : " + allowMapList.size());

        for (UserRestrictionDto obj : filterList) {
            responseList.add(obj);
        }

        Pageable pageable = PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort));
        Page<UserRestrictionDto> data = convertToPage(responseList, pageable);

        ApiResponseDto<List<UserRestrictionDto>> response = new ApiResponseDto<>();
        response.setResult(data.getContent());
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) data.getTotalElements());
        response.setPagination(paginationDto);
        return response;

    }

    //Page Creator
    public Page<UserRestrictionDto> convertToPage(List<UserRestrictionDto> objectList, Pageable pageable) {
        int start = (int) pageable.getOffset();
        int end = Math.min(start + pageable.getPageSize(), objectList.size());
        List<UserRestrictionDto> subList = start >= end ? new ArrayList<>() : objectList.subList(start, end);
        return new PageImpl<>(subList, pageable, objectList.size());
    }

    @Override
    public ApiResponseDto<List<UserRestrictionDto>> getNewUserRestrictionForUserId(Integer userId, Integer branchId, int page, Integer per_page, String sort, String direction) {

        List<UserRestrictionDto> responseList = new ArrayList<>();
        List<Integer> finalIdList = new ArrayList<>();
        ApiResponseDto<List<UserRestrictionDto>> restrictionList = getUserRestrictionForUserId(userId, branchId, 0, 1000, "userId", "asc");

        List<Integer> restrictMenuActionIdList = new ArrayList<>();
        for (UserRestrictionDto dto : restrictionList.getResult()) {
            restrictMenuActionIdList.add(dto.getMenuActionId());
        }

        List<UsTMenuAction> menuActionList = menuActionDao.findAll();
        List<Integer> allMenuActionIdList = new ArrayList<>();
        for (UsTMenuAction m : menuActionList) {
            allMenuActionIdList.add(m.getMenuActionId());
        }

        for (int value : allMenuActionIdList) {
            if (!restrictMenuActionIdList.contains(value)) {
                System.out.println(value + " is Unique value");
                finalIdList.add(value);
            }
        }

        if (finalIdList.size() != 0) {
            for (Integer i : finalIdList) {
                System.out.println("ID : " + i);
            }
        } else {
            System.out.println("FINAL ID LIST = 0");
        }
        System.out.println("FINAL ID LIST = " + finalIdList);

        for (Integer i : finalIdList) {
            UsTMenuAction menuAction = menuActionDao.findById(i).get();
            UserRestrictionDto restrictionDto = new UserRestrictionDto();
            restrictionDto.setMenuActionId(menuAction.getMenuActionId());
            restrictionDto.setMenuActionName(menuAction.getName());
            restrictionDto.setActionId(menuAction.getUsMAction().getActionId());
            restrictionDto.setActionName(menuAction.getUsMAction().getActionName());
            restrictionDto.setModuleId(menuAction.getUsMModule().getModuleId());
            restrictionDto.setModuleName(menuAction.getUsMModule().getModuleName());
            restrictionDto.setMenuId(menuAction.getUsMMenu().getMenuId());
            restrictionDto.setMenuName(menuAction.getUsMMenu().getMenuName());

            responseList.add(restrictionDto);
        }

        Pageable pageable = PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort));
        Page<UserRestrictionDto> data = convertToPage(responseList, pageable);

        ApiResponseDto<List<UserRestrictionDto>> response = new ApiResponseDto<>();
        response.setResult(data.getContent());
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) data.getTotalElements());
        response.setPagination(paginationDto);
        return response;
    }

    /**
     * @param menuActionId
     * @param userId
     * @return
     */
    @Override
    public ResponseDto updateUserRestriction(Integer userId, Integer branchId, Integer menuActionId) {
        try {

            ResponseDto response = new ResponseDto();
            List<Object[]> restrictionList = usMUserRestrictionDao.findRestrictionListForTable(userId, branchId);
            List<Object[]> allowMapList = menuAllowedMapDao.findAllowMapListForTable(userId);

            List<UsMUserRestriction> userRestrictionList = new ArrayList<>();
            List<UsMMenuAllowedMap> menuAllowedMapList = new ArrayList<>();

            for (Object[] obj : restrictionList) {
                if (obj[0] instanceof AdMUser && obj[1] instanceof UsRUserBranch && obj[2] instanceof UsRBrachDepartment && obj[3] instanceof UsTMenuAction && obj[4] instanceof UsMUserRestriction && obj[5] instanceof UsMModule && obj[6] instanceof UsMAction && obj[7] instanceof UsMMenu) {
                    UsMUserRestriction userRestriction = (UsMUserRestriction) obj[4];
                    userRestrictionList.add(userRestriction);
                }
            }

            for (Object[] obj : allowMapList) {
                if (obj[0] instanceof AdMUser && obj[1] instanceof UsMUserRole && obj[2] instanceof UsMMenuAllowedMap && obj[3] instanceof UsMMenu && obj[4] instanceof UsMModule && obj[5] instanceof UsMAction && obj[6] instanceof UsTMenuAction) {
                    UsMMenuAllowedMap menuAllowedMap = (UsMMenuAllowedMap) obj[2];
                    menuAllowedMapList.add(menuAllowedMap);
                }
            }

            Optional<UsMUserRestriction> restriction = userRestrictionList.stream().filter(item -> item.getUsTMenuAction().getMenuActionId() == menuActionId).findAny();
            String msg;
            if (restriction.isPresent()) {
                UsMUserRestriction re = restriction.get();

                usMUserRestrictionDao.deleteById(re.getUserRestrictionId());
                Optional<UsMUserRestriction> optUserRestriction = usMUserRestrictionDao.findById(re.getUserRestrictionId());
                if (optUserRestriction.isPresent()) {
                    throw new BadRequestAlertException("RESTRICTION REMOVE REQUEST FAILED", ENTITY_NAME, "RESTRICTION REMOVE REQUEST FAILED");
                } else {
                    msg = "RESTRICTION DELETED";
                }
                response.setMessage(msg);
                return response;
            } else {
                Optional<UsMMenuAllowedMap> allowMap = menuAllowedMapList.stream().filter(item -> item.getUsTMenuAction().getMenuActionId() == menuActionId).findAny();
                if (allowMap.isPresent()) {
                    UsMUserRestriction userRestriction = new UsMUserRestriction();

                    userRestriction.setUsTMenuAction(allowMap.get().getUsTMenuAction());
                    userRestriction.setUsRStatusDetail(statusDetailDao.findById(HardCodeConstants.RESTRICTED_STATUS_ID).get());
                    List<UsRUserBranch> usRUserBranch = userBranchDao.findBranchByUserId(userId, branchId);
                    userRestriction.setUsRUserBranch(usRUserBranch.get(0));
                    usMUserRestrictionDao.save(userRestriction);

                    msg = "From Allow Map";

                    response.setMessage(msg);
                    return response;
                } else {
                    System.out.println("Record Not Found");
                }

            }

            return response;
        } catch (Exception e) {
            e.fillInStackTrace();

            ResponseDto response = new ResponseDto();
            response.setMessage(e.getMessage());
            return response;
        }

    }

    @Override
    public ResponseEntity<SideBarMenuDto> getSidebarMenu(Integer userId, Integer moduleId) {
        try {
            Optional<AdMUser> user = adMUserDao.findByUserIdAndIsActive(userId, ActiveStatus.ACTIVE.getValue());

            if (user.isPresent()) {
                AdMUser us = user.get();

                List<UsTMenuAction> menuActions = new ArrayList<>();

                List<UsMMenuAllowedMap> roleAllowed = us.getUsMUserRole().getUsMMenuAllowedMaps();

                Optional<UsRUserBranch> UserBranch = us.getUsRUserBranches()
                        .stream()
                        .peek(obj -> System.out.println("branch-id  " + obj.getUsRBrachDepartment().getUsMBranch().getBranchId()))
                        .filter(obj -> obj.getUsRBrachDepartment().getUsMBranch().getBranchId() == us.getUsMBranch().getBranchId())
                        .findAny();

                if (UserBranch.isPresent()) {
                    List<UsMUserRestriction> userRestrictions = UserBranch.get().getUsMUserRestrictions();

                    userRestrictions.forEach(userRestriction -> {
                        if (userRestriction.getUsRStatusDetail().getStatusId() == 3) {
                            menuActions.add(userRestriction.getUsTMenuAction());
                        }
                    });

                    roleAllowed.forEach(roleRestriction -> {
                        if (!userRestrictions.stream().anyMatch(userRestriction -> userRestriction.getUsTMenuAction() == roleRestriction.getUsTMenuAction())) {
                            menuActions.add(roleRestriction.getUsTMenuAction());
                        }
                    });

                    SideBarMenuDto sideBarMenuDto = new SideBarMenuDto();
                    List<UsMMenuGroup> groupList = usMMenuGroupDao.findAllByUsMModuleModuleIdOrderBySeqNoAsc(moduleId);
                    log.info("groupList : {}", groupList.size());
                    List<MenuGroupDto> resGroupList = new ArrayList<>();
                    for (UsMMenuGroup group : groupList) {
                        MenuGroupDto menuGroupDto = new MenuGroupDto();
                        menuGroupDto.setMenuGroupName(group.getMenuGroupName());
                        menuGroupDto.setSeqNo(group.getSeqNo());

                        List<UsMMenuSubGroup> subGroupList = usMMenuSubGroupDao.findAllByUsMMenuGroupMenuGroupIdOrderBySeqNo(group.getMenuGroupId());
                        log.info("subGroupList : {}", subGroupList.size());
                        List<SubMenuGroupDto> resSubGroupList = new ArrayList<>();
                        for (UsMMenuSubGroup subGroup : subGroupList) {
                            SubMenuGroupDto subMenuGroupDto = new SubMenuGroupDto();
                            subMenuGroupDto.setSubGroupName(subGroup.getSubGroupName());
                            subMenuGroupDto.setSeqNo(subGroup.getSeqNo());

                            List<UsTMenuAction> filteredList = menuActions.stream().filter(
                                            menuAction ->
                                                    menuAction.getUsMMenu()
                                                            .getUsMMenuSubGroup()
                                                            .getUsMMenuGroup()
                                                            .getMenuGroupId() == group.getMenuGroupId() && (menuAction
                                                            .getUsMMenu()
                                                            .getUsMMenuSubGroup()
                                                            .getSubGroupId() == subGroup.getSubGroupId()) || menuAction.getUsMMenu()
                                                            .getUsMMenuSubGroup()
                                                            .getSubGroupId() == null).distinct()
                                    .collect(Collectors.toList());

                            log.info("filteredList : {}", filteredList.size());

                            List<MenuDto> menus = filteredList.stream().map(menuAction -> {
                                MenuDto menuDto = new MenuDto();
                                menuDto.setTitle(menuAction.getUsMMenu().getMenuName());
                                menuDto.setUrl(menuAction.getUrl());
                                menuDto.setIcon(menuAction.getUsMMenu().getMenuIcon());
                                menuDto.setId(menuAction.getUsMMenu().getSeqNo());
                                return menuDto;
                            }).collect(Collectors.toList());

                            log.info("menus : {}", menus.size());

                            subMenuGroupDto.setMenu(menus);
                            resSubGroupList.add(subMenuGroupDto);
                        }
                        menuGroupDto.setSubGroup(resSubGroupList);
                        resGroupList.add(menuGroupDto);
                    }
                    sideBarMenuDto.setGroup(resGroupList);
                    return ResponseEntity.ok(sideBarMenuDto);
                } else {
                    System.out.println("Branch not found");
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            } else {
                System.out.println("user not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.fillInStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<SideMenuDto>> getSideMenuList(Integer userId, Integer moduleId) {
        try {
            Optional<AdMUser> user = adMUserDao.findByUserIdAndIsActive(userId, ActiveStatus.ACTIVE.getValue());

            if (user.isPresent()) {
                AdMUser us = user.get();

                List<UsTMenuAction> menuActions = new ArrayList<>();

                List<UsMMenuAllowedMap> roleAllowed = us.getUsMUserRole().getUsMMenuAllowedMaps();

                Optional<UsRUserBranch> UserBranch = us.getUsRUserBranches()
                        .stream()
                        .peek(obj -> System.out.println("branch-id  " + obj.getUsRBrachDepartment().getUsMBranch().getBranchId()))
                        .filter(obj -> obj.getUsRBrachDepartment().getUsMBranch().getBranchId() == us.getUsMBranch().getBranchId())
                        .findAny();

                if (UserBranch.isPresent()) {
                    List<UsMUserRestriction> userRestrictions = UserBranch.get().getUsMUserRestrictions();

                    userRestrictions.forEach(userRestriction -> {
                        if (userRestriction.getUsRStatusDetail().getStatusId() == 3) {
                            menuActions.add(userRestriction.getUsTMenuAction());
                        }
                    });

                    roleAllowed.forEach(roleRestriction -> {
                        if (!userRestrictions.stream().anyMatch(userRestriction -> userRestriction.getUsTMenuAction() == roleRestriction.getUsTMenuAction())) {
                            menuActions.add(roleRestriction.getUsTMenuAction());
                        }
                    });

                    List<SideMenuDto> sideBarMenuList = new ArrayList<>();
                    List<UsMMenuGroup> groupList = usMMenuGroupDao.findAllByUsMModuleModuleIdOrderBySeqNoAsc(moduleId);
                    log.info("groupList : {}", groupList.size());
                    for (UsMMenuGroup group : groupList) {
                        List<UsMMenuSubGroup> subGroupList = usMMenuSubGroupDao.findAllByUsMMenuGroupMenuGroupIdOrderBySeqNo(group.getMenuGroupId());
                        log.info("subGroupList : {}", subGroupList.size());

                        for (UsMMenuSubGroup subGroup : subGroupList) {

                            List<UsTMenuAction> filteredList = menuActions.stream().filter(
                                            menuAction ->
                                                    menuAction.getUsMMenu()
                                                            .getUsMMenuSubGroup()
                                                            .getUsMMenuGroup()
                                                            .getMenuGroupId() == group.getMenuGroupId() && (menuAction
                                                            .getUsMMenu()
                                                            .getUsMMenuSubGroup()
                                                            .getSubGroupId() == subGroup.getSubGroupId()) || menuAction.getUsMMenu()
                                                            .getUsMMenuSubGroup()
                                                            .getSubGroupId() == null).distinct()
                                    .collect(Collectors.toList());

                            log.info("filteredList : {}", filteredList.size());

                            //Sort by menu seq no
                            Collections.sort(filteredList, Comparator.comparing(o -> o.getUsMMenu().getSeqNo()));

                            for (UsTMenuAction s : new ArrayList<>(filteredList)) {
                                SideMenuDto sideMenuDto = getSideMenuDto(group, subGroup, s);
                                sideBarMenuList.add(sideMenuDto);
                            }
                        }
                    }
                    return ResponseEntity.ok(sideBarMenuList);
                } else {
                    System.out.println("Branch not found");
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            } else {
                System.out.println("user not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.fillInStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}