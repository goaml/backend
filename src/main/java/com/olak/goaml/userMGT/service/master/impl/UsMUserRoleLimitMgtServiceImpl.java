package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.builder.CustomBuilder;
import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.userMGT.dao.master.UsMUserRoleLimitMgtDao;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import com.olak.goaml.userMGT.dto.master.UsMUserRoleLimitMgtDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.mappers.master.UsMUserRoleLimitMgtMapper;
import com.olak.goaml.userMGT.models.master.UsMUserRoleLimitMgt;
import com.olak.goaml.userMGT.service.master.UsMUserRoleLimitMgtService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@Transactional
public class UsMUserRoleLimitMgtServiceImpl implements UsMUserRoleLimitMgtService {

    private static final String I18NKEY = "USER_ROLE_NOT_FOUND";
    private static final String MESSAGE = "There is no User Role related data for the given userRole id -";

    private final UsMUserRoleLimitMgtMapper usMUserRoleLimitMgtMapper;
    private final UsMUserRoleLimitMgtDao usMUserRoleLimitMgtDao;
    private final SearchNFilter searchNFilter;

    public UsMUserRoleLimitMgtServiceImpl(UsMUserRoleLimitMgtMapper usMUserRoleLimitMgtMapper, UsMUserRoleLimitMgtDao usMUserRoleLimitMgtDao, SearchNFilter searchNFilter) {
        this.usMUserRoleLimitMgtMapper = usMUserRoleLimitMgtMapper;
        this.usMUserRoleLimitMgtDao = usMUserRoleLimitMgtDao;
        this.searchNFilter = searchNFilter;
    }

    @Override
    public ResponseEntity<UsMUserRoleLimitMgtDto> saveUserRole(UsMUserRoleLimitMgtDto usMUserRoleLimitMgtDto) {
        return new ResponseEntity<>(usMUserRoleLimitMgtMapper.toDto(usMUserRoleLimitMgtDao.save(usMUserRoleLimitMgtMapper.toEntity(usMUserRoleLimitMgtDto))), HttpStatus.OK);

    }

    @Override
    public ApiResponseDto<List<UsMUserRoleLimitMgtDto>> getUserRole(int page, Integer per_page, String search, String sort, String direction) {
        List<SearchPassDto> searchList = new ArrayList<>();

        List<FilterPassDto> filterList = new ArrayList<>();


        searchNFilter.setPaginationDetails(page, per_page, sort, direction);
        searchNFilter.setVariables(search, ActiveStatus.ACTIVE.getValue());
        searchNFilter.setEntityClass(UsMUserRoleLimitMgt.class);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();


        ApiResponseDto<List<UsMUserRoleLimitMgtDto>> response = new ApiResponseDto<>();
        response.setResult(usMUserRoleLimitMgtMapper.listToDto((List<UsMUserRoleLimitMgt>) dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;

       /* Page<UsMUserRoleLimitMgt> dbData;

        if (direction.equalsIgnoreCase("asc")) {
            dbData = usMUserRoleLimitMgtDao.findByUserRoleForTables(search, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
        } else {
            dbData = usMUserRoleLimitMgtDao.findByUserRoleForTables(search, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        }

        ApiResponseDto<List<UsMUserRoleLimitMgtDto>> response = new ApiResponseDto<>();
        response.setResult(usMUserRoleLimitMgtMapper.listToDto(dbData.getContent()));

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        System.out.println(response);
        return response;*/

    }

    @Override
    public UsMUserRoleLimitMgtDto updateUserRole(UsMUserRoleLimitMgtDto usMUserRoleLimitMgtDto) {
        return usMUserRoleLimitMgtMapper.toDto(usMUserRoleLimitMgtDao.save(usMUserRoleLimitMgtMapper.toEntity(usMUserRoleLimitMgtDto)));

    }

    @Override
    public UsMUserRoleLimitMgtDto getUserRoleById(Integer roleLimitId) {
        Optional<UsMUserRoleLimitMgt> optionalInTRequistHeader = usMUserRoleLimitMgtDao.findById(roleLimitId);
        if (optionalInTRequistHeader.isPresent()) {
            log.info("Getting User role  with ID {}.", usMUserRoleLimitMgtMapper.toDto(optionalInTRequistHeader.get()));
            return usMUserRoleLimitMgtMapper.toDto(optionalInTRequistHeader.get());
        }
        throw CustomBuilder.buildBusinessRuleException(I18NKEY, MESSAGE + roleLimitId);

    }
}

