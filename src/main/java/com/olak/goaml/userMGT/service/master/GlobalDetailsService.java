package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.GlobalDetailsDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;

import java.util.List;

public interface GlobalDetailsService {
    GlobalDetailsDto saveGlobalDetails(GlobalDetailsDto globalDetailsDto);

    GlobalDetailsDto getById(Integer id);

    GlobalDetailsDto updateGlobalDetails(Integer id, GlobalDetailsDto globalDetailsDto);

    ApiResponseDto<List<GlobalDetailsDto>> getAllGlobalDetals(Integer page, Integer per_page, String sort, String search, String direction);
}
