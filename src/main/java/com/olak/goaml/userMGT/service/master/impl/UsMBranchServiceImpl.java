package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.constants.HardCodeConstants;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.master.UsMBranchDao;
import com.olak.goaml.userMGT.dao.reference.UsRStatusDetailDao;
import com.olak.goaml.userMGT.dto.master.UsMBranchDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.other.BranchCreateReqDto;
import com.olak.goaml.userMGT.mappers.master.UsMBranchMapper;
import com.olak.goaml.userMGT.models.master.UsMBranch;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.service.master.UsMBranchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class UsMBranchServiceImpl implements UsMBranchService {

    private final UsMBranchDao branchDao;
    private final UsMBranchMapper branchMapper;
    private final UsRStatusDetailDao statusDetailDao;

    public UsMBranchServiceImpl(UsMBranchDao branchDao, UsMBranchMapper branchMapper, UsRStatusDetailDao statusDetailDao) {
        this.branchDao = branchDao;
        this.branchMapper = branchMapper;
        this.statusDetailDao = statusDetailDao;
    }

    private static UsMBranch getUsMBranch(BranchCreateReqDto branchCreateReqDto) {
        UsMBranch branch = new UsMBranch();
        branch.setBranchAddress(branchCreateReqDto.getBranchAddress());
        branch.setBranchLocation(branchCreateReqDto.getBranchLocation());
        branch.setBranchName(branchCreateReqDto.getBranchName());
        branch.setContactNumber(branchCreateReqDto.getContactNumber());
        branch.setDesc(branchCreateReqDto.getDesc());
        branch.setEmail(branchCreateReqDto.getEmail());
        branch.setLimitAmt(branchCreateReqDto.getLimitAmt());
        return branch;
    }

    private static UsMBranch getUsMBranch(UsMBranchDto usMBranchDto, UsMBranch usMBranch) {
        usMBranch.setBranchAddress(usMBranchDto.getBranchAddress());
        usMBranch.setBranchLocation(usMBranchDto.getBranchLocation());
        usMBranch.setBranchName(usMBranchDto.getBranchName());
        usMBranch.setContactNumber(usMBranchDto.getContactNumber());
        usMBranch.setDesc(usMBranchDto.getDesc());
        usMBranch.setEmail(usMBranchDto.getEmail());
        usMBranch.setLimitAmt(usMBranchDto.getLimitAmt());
        return usMBranch;
    }

    @Override
    public ApiResponseDto<List<UsMBranchDto>> getAllBranches(Integer page, Integer per_page, String search, String sort, String direction, String branchAddress, String branchLocation, String branchName, String contactNumber, String email, Integer userId, Integer branchId) {
        Page<UsMBranch> dbData;

        String address = branchAddress.toLowerCase();
        String location = branchLocation.toLowerCase();
        String name = branchName.toLowerCase();
        String contact = contactNumber.toLowerCase();
        String e_mail = email.toLowerCase();

        if (branchId != null) {
            dbData = branchDao.findUsMBranchByBranchId(branchId, PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else if (userId != null) {
            dbData = branchDao.findUsMBranchByUserId(userId, PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else if (!branchAddress.equals("") || !branchLocation.equals("") || !branchName.equals("") || !contactNumber.equals("") || !email.equals("")) {
            dbData = branchDao.findAllBranchesWithAttributes(address, location, name, contact, e_mail, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else if (direction.equalsIgnoreCase("asc")) {
            dbData = branchDao.getAllBranchesForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else {
            dbData = branchDao.getAllBranchesForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.DESC, sort));
        }

        ApiResponseDto<List<UsMBranchDto>> response = new ApiResponseDto<>();
        response.setResult(branchMapper.listToDto(dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;
    }

    @Override
    public ResponseEntity<UsMBranchDto> getUsMBranchById(Integer branchId) {
        try {
            if (branchId == null) {
                throw new BadRequestAlertException("branchId null", ENTITY_NAME, "ERROR");
            } else {
                Optional<UsMBranch> optBranch = branchDao.findById(branchId);
                if (!optBranch.isPresent()) {
                    throw new BadRequestAlertException("Branch not found", ENTITY_NAME, "ERROR");
                } else {
                    return ResponseEntity.ok(branchMapper.toDto(optBranch.get()));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<List<UsMBranchDto>> getBranchesForDropdown() {
        try {

            List<UsMBranch> branchList = branchDao.findAll();
            if (branchList.isEmpty()) {
                throw new BadRequestAlertException("No data found", ENTITY_NAME, "ERROR");
            } else {
                return ResponseEntity.ok(branchMapper.listToDto(branchList));
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<UsMBranchDto> createBranch(BranchCreateReqDto branchCreateReqDto) {
        try {
            Optional<UsMBranch> optBranch = branchDao.findByBranchNameOrDesc(branchCreateReqDto.getBranchName(), branchCreateReqDto.getDesc());
            if (optBranch.isPresent()) {
                throw new BadRequestAlertException("Branch already exist with given branch name or branch description", ENTITY_NAME, "ERROR");
            }

            UsRStatusDetail statusDetail = null;
            Optional<UsRStatusDetail> optStatus = statusDetailDao.findById(HardCodeConstants.NEW_STATUS_ID);
            if (!optStatus.isPresent()) {
                throw new BadRequestAlertException("Status not found for given id", ENTITY_NAME, "ERROR");
            } else {
                statusDetail = optStatus.get();
            }

            UsMBranch branch = getUsMBranch(branchCreateReqDto);
            branch.setUsRStatusDetail(statusDetail);
            branch = branchDao.save(branch);
            if (branch.getBranchId() == null) {
                throw new BadRequestAlertException("Branch create request failed.", ENTITY_NAME, "ERROR");
            } else {
                return ResponseEntity.ok(branchMapper.toDto(branch));
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<UsMBranchDto> updateBranch(Integer branchId, UsMBranchDto usMBranchDto) {
        try {
            if (branchId == null) {
                throw new BadRequestAlertException("BranchId null", ENTITY_NAME, "ERROR");
            } else if (!branchId.equals(usMBranchDto.getBranchId())) {
                throw new BadRequestAlertException("BranchId mismatch", ENTITY_NAME, "ERROR");
            } else {
                Optional<UsMBranch> optBranch = branchDao.findById(usMBranchDto.getBranchId());
                if (!optBranch.isPresent()) {
                    throw new BadRequestAlertException("Branch not found", ENTITY_NAME, "ERROR");
                } else {

                    UsRStatusDetail statusDetail = null;
                    Optional<UsRStatusDetail> optStatus = statusDetailDao.findById(HardCodeConstants.NEW_STATUS_ID);
                    if (!optStatus.isPresent()) {
                        throw new BadRequestAlertException("Status not found for given id", ENTITY_NAME, "ERROR");
                    } else {
                        statusDetail = optStatus.get();
                    }

                    UsMBranch branch = getUsMBranch(usMBranchDto, optBranch.get());
                    branch.setUsRStatusDetail(statusDetail);
                    branch = branchDao.save(branch);
                    return ResponseEntity.ok(branchMapper.toDto(branch));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }
}
