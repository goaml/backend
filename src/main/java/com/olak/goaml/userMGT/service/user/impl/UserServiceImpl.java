package com.olak.goaml.userMGT.service.user.impl;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.olak.goaml.audit.AuditUser;
import com.olak.goaml.audit.AuditUtils;
import com.olak.goaml.config.JwtUtils;
import com.olak.goaml.config.PasswordUtils;
import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.constants.HardCodeConstants;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.master.*;
import com.olak.goaml.userMGT.dao.reference.*;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import com.olak.goaml.userMGT.dto.authenticate.LoginDto;
import com.olak.goaml.userMGT.dto.master.GlobalDetailsDto;
import com.olak.goaml.userMGT.dto.master.UsMBranchDto;
import com.olak.goaml.userMGT.dto.master.UsMDepartmentDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.other.*;
import com.olak.goaml.userMGT.dto.user.*;
import com.olak.goaml.userMGT.mappers.master.GlobalDetailsMapper;
import com.olak.goaml.userMGT.mappers.master.UsMBranchMapper;
import com.olak.goaml.userMGT.mappers.master.UsMDepartmentMapper;
import com.olak.goaml.userMGT.mappers.user.AdMUserMapper;
import com.olak.goaml.userMGT.models.master.*;
import com.olak.goaml.userMGT.models.reference.*;
import com.olak.goaml.userMGT.service.other.SMSMailConfig;
import com.olak.goaml.userMGT.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DateTimeException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.olak.goaml.config.JwtUtils.EXPIRATION_TIME;
import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private static final String success_status = "success";
    private static final String error_status = "error";
    private static final String success_code = "002";
    private static final String error_code = "003";
    private static final String AGE_RESTRICT = "Age Must Be Bellow 55";
    private static final String WRONG_AGE = "Given NIC and DOB doesn't match";
    private static final String INVALID_NIC = "Invalid NIC";
    private static final String DOB_NOT_MATCHED = "Date of birth not matched with NIC";
    private final AdMUserDao adMUserDao;
    private final GlobalDetailsMapper globalDetailsMapper;
    private final GlobalDetailsDao globalDetailsDao;
    private final UsMUserRoleDao userRoleDao;
    private final UsRStatusDetailDao statusDetailDao;
    private final UsMBranchDao branchDao;
    private final UsMDepartmentDao departmentDao;
    private final AdMUserMapper adMUserMapper;
    private final UsMBranchMapper branchMapper;
    private final UsMDepartmentMapper departmentMapper;
    private final SearchNFilter searchNFilter;
    private final UsRUserBranchDao userBranchDao;
    private final UsRBrachDepartmentDao brachDepartmentDao;
    private final AuditUtils auditUtils;
    private final UsRUserIdTypeDao userIdTypeDao;
    private final UsRLoginTypeDao loginTypeDao;
    private final UsMCompanyDetailDao companyDetailDao;
    private int AGE;

    public UserServiceImpl(AdMUserDao adMUserDao, GlobalDetailsMapper globalDetailsMapper, GlobalDetailsDao globalDetailsDao, UsMUserRoleDao userRoleDao, UsRStatusDetailDao statusDetailDao, UsMBranchDao branchDao, UsMDepartmentDao departmentDao, AdMUserMapper adMUserMapper, UsMBranchMapper branchMapper, UsMDepartmentMapper departmentMapper, SearchNFilter searchNFilter, UsRUserBranchDao userBranchDao, UsRBrachDepartmentDao brachDepartmentDao, AuditUtils auditUtils, UsRUserIdTypeDao userIdTypeDao, UsRLoginTypeDao loginTypeDao, UsMCompanyDetailDao companyDetailDao) {
        this.adMUserDao = adMUserDao;
        this.globalDetailsMapper = globalDetailsMapper;
        this.globalDetailsDao = globalDetailsDao;
        this.userRoleDao = userRoleDao;
        this.statusDetailDao = statusDetailDao;
        this.branchDao = branchDao;
        this.departmentDao = departmentDao;
        this.adMUserMapper = adMUserMapper;
        this.branchMapper = branchMapper;
        this.departmentMapper = departmentMapper;
        this.searchNFilter = searchNFilter;
        this.userBranchDao = userBranchDao;
        this.brachDepartmentDao = brachDepartmentDao;
        this.auditUtils = auditUtils;
        this.userIdTypeDao = userIdTypeDao;
        this.loginTypeDao = loginTypeDao;
        this.companyDetailDao = companyDetailDao;
    }

    public static GetUserResponseDto getGetUserResponseDto(AdMUser d) {
        GetUserResponseDto userResponseDto = new GetUserResponseDto();
        userResponseDto.setDescription(d.getDescription());
        userResponseDto.setEmpEmail(d.getEmail());
        userResponseDto.setEmpMobileNumber(d.getMobileNo());
        userResponseDto.setFirstName(d.getFirstName());
        userResponseDto.setLastName(d.getLastName());
        userResponseDto.setUserRoleId(d.getUsMUserRole().getUserRoleId());
        userResponseDto.setStatusId(d.getUsRStatusDetail().getStatusId());
        userResponseDto.setUserId(d.getUserId());
        userResponseDto.setUserName(d.getUserName());
        userResponseDto.setIsActive(d.getIsActive());
        userResponseDto.setNic(d.getNic());
        userResponseDto.setReferenceNo(d.getReferenceNo());
        userResponseDto.setDepartmentId(d.getUsMDepartment().getDepartmentId());
        userResponseDto.setBranchId(d.getUsMBranch().getBranchId());
        userResponseDto.setStatusName(d.getUsRStatusDetail().getStatusName());
        userResponseDto.setUserRoleName(d.getUsMUserRole().getUserRoleName());
        userResponseDto.setBranchName(d.getUsMBranch().getBranchName());
        userResponseDto.setDepartmentName(d.getUsMDepartment().getDeptName());
        userResponseDto.setUserTypeId(d.getUsRUserIdType().getUserTypeId());
        userResponseDto.setUserTypeName(d.getUsRUserIdType().getUserCategory());
        userResponseDto.setCompanyId(d.getComId());
        return userResponseDto;
    }

    private static UserDetailsDto getUserDetailsDto(AdMUser user, List<UsMBranchDto> branchList, List<UsMDepartmentDto> departmentList) {
        UserDetailsDto userDetailsDto = new UserDetailsDto();
        userDetailsDto.setUserId(user.getUserId());
        userDetailsDto.setUserName(user.getUserName());
        userDetailsDto.setUserRoleName(user.getUsMUserRole().getUserRoleName());
        userDetailsDto.setUserRoleId(user.getUsMUserRole().getUserRoleId());
        userDetailsDto.setBranchList(branchList);
        userDetailsDto.setDepartmentList(departmentList);
        userDetailsDto.setFirstName(user.getFirstName());
        userDetailsDto.setLastName(user.getLastName());
        userDetailsDto.setEmail(user.getEmail());
        userDetailsDto.setMobile(user.getMobileNo());
        userDetailsDto.setIsFirstLogin(user.getIsFirstLogin());
        userDetailsDto.setUserTypeId(user.getUsRUserIdType().getUserTypeId());
        userDetailsDto.setUserType(user.getUsRUserIdType().getUserCategory());
        userDetailsDto.setCompanyId(user.getComId());
        userDetailsDto.setIsExistingCompany(user.getIsExistingCompany());
        userDetailsDto.setReferenceNo(user.getReferenceNo());
        return userDetailsDto;
    }

    private static UserRequestDto getUserRequestDto(Integer userTypeId, CreateApproveUserDto createApproveUserDto) {
        UserRequestDto userRequestDto = new UserRequestDto();
        userRequestDto.setDescription(createApproveUserDto.getDescription());
        userRequestDto.setEmail(createApproveUserDto.getEmail());
        userRequestDto.setMobileNo(createApproveUserDto.getMobileNo());
        userRequestDto.setFirstName(createApproveUserDto.getFirstName());
        userRequestDto.setLastName(createApproveUserDto.getLastName());
        userRequestDto.setUserRoleId(createApproveUserDto.getUserRoleId());
        userRequestDto.setBranchId(createApproveUserDto.getBranchId());
        userRequestDto.setDepartmentId(createApproveUserDto.getDepartmentId());
        userRequestDto.setNic(createApproveUserDto.getNic());
        userRequestDto.setReferenceNo(createApproveUserDto.getReferenceNo());
        userRequestDto.setUserTypeId(userTypeId);

        if (createApproveUserDto.getComId() != null)
            userRequestDto.setComId(createApproveUserDto.getComId());
        else
            userRequestDto.setComId(HardCodeConstants.DEFAULT_COMPANY_ID);

        userRequestDto.setIsExistingCompany(createApproveUserDto.getIsExistingCompany());

        return userRequestDto;
    }

    @Override
    public ResponseEntity<ResponseDto> createUser(String userType, UserRequestDto userRequestDto) {
        ResponseDto responseDto = new ResponseDto();
        try {
            LocalDate userResetDate;
            LocalDate passwordResetDate;

            if (userRequestDto.getUserRoleId() == null) {
                throw new BadRequestAlertException("User Role Id is required.", ENTITY_NAME, "error");
            }
            if (userRequestDto.getUserTypeId() == null) {
                throw new BadRequestAlertException("User Type Id is required.", ENTITY_NAME, "error");
            }

            userRequestDto.setDepartmentId(HardCodeConstants.DEFAULT_DEPARTMENT_ID);

            Optional<UsMCompanyDetail> optComDetails = companyDetailDao.findById(userRequestDto.getComId());
            if (!optComDetails.isPresent()) {
                throw new BadRequestAlertException("Company not found", ENTITY_NAME, "ERROR");
            }

            Optional<UsMUserRole> optUserRole = userRoleDao.findById(userRequestDto.getUserRoleId());
            Optional<UsRStatusDetail> optStatusDetails = statusDetailDao.findById(HardCodeConstants.NEW_STATUS_ID);
            Optional<UsMDepartment> optDepartment = departmentDao.findById(userRequestDto.getDepartmentId());
            Optional<UsMBranch> optBranch = branchDao.findById(userRequestDto.getBranchId());
            Optional<UsRUserIdType> optUserIdType = userIdTypeDao.findById(userRequestDto.getUserTypeId());

            if (!optUserRole.isPresent()) {
                throw new BadRequestAlertException("USER_ROLE_NOT_FOUND", ENTITY_NAME, "USER_ROLE_NOT_FOUND");
            } else if (!optStatusDetails.isPresent()) {
                throw new BadRequestAlertException("STATUS_DETAILS_NOT_FOUND", ENTITY_NAME, "STATUS_DETAILS_NOT_FOUND");
            } else if (!optDepartment.isPresent()) {
                throw new BadRequestAlertException("DEPARTMENT_NOT_FOUND", ENTITY_NAME, "DEPARTMENT_NOT_FOUND");
            } else if (!optBranch.isPresent()) {
                throw new BadRequestAlertException("BRANCH_NOT_FOUND", ENTITY_NAME, "BRANCH_NOT_FOUND");
            } else if (!optUserIdType.isPresent()) {
                throw new BadRequestAlertException("USER_TYPE_NOT_FOUND", ENTITY_NAME, "USER_TYPE_NOT_FOUND");
            } else {
                log.info("Foreign key details found");
            }

            String userName = selectUserName(optUserIdType.get(), userRequestDto.getNic(), userRequestDto.getMobileNo(), userRequestDto.getEmail());
            log.info("Username : {}", userName);

            AdMUser user = new AdMUser();
            user.setUserName(userName);
            user.setDescription(userRequestDto.getDescription());
            user.setIsFirstLogin(true);
            user.setIsActive(ActiveStatus.INACTIVE.getValue());
            user.setEmail(userRequestDto.getEmail());
            user.setMobileNo(userRequestDto.getMobileNo());
            user.setFirstName(userRequestDto.getFirstName());
            user.setLastName(userRequestDto.getLastName());
            user.setUsMUserRole(optUserRole.get());
            user.setUsRStatusDetail(optStatusDetails.get());
            user.setUsMBranch(optBranch.get());
            user.setUsMDepartment(optDepartment.get());
            user.setUsRUserIdType(optUserIdType.get());
            user.setNic(userRequestDto.getNic());
            user.setReferenceNo(String.valueOf(userRequestDto.getReferenceNo()));
            user.setIsExistingCompany(userRequestDto.getIsExistingCompany());
            user.setComId(userRequestDto.getComId());

            //Get Global Details Data
            Optional<GlobalDetails> gd = globalDetailsDao.findById(HardCodeConstants.GLOBAL_DETAILS_ID);
            if (!gd.isPresent()) {
                throw new BadRequestAlertException("GLOBAL_DETAILS NOT FOUND", ENTITY_NAME, "GLOBAL_DETAILS NOT FOUND");
            }
            GlobalDetailsDto globalDetails = globalDetailsMapper.toDto(gd.get());

            userResetDate = LocalDate.now().plusDays(globalDetails.getUserExpDate());
            passwordResetDate = LocalDate.now().plusDays(globalDetails.getPasswordExpDate());

            log.info("User reset date       : {}", userResetDate);
            log.info("Password reset date   : {}", passwordResetDate);

            user.setUserExpDate(userResetDate);
            user.setPasswordExpDate(passwordResetDate);

            // Generate Password
            String autoPassword = PasswordUtils.generateRandomPassword();

            // Password Encryption
            String encryptedPassword = PasswordUtils.encodePassword(autoPassword);
            user.setPassword(encryptedPassword);
            user.setSecureKey(autoPassword);
            log.info("Password encrypted and set to user object");

            log.info("User object : {}", user);
            // Save User
            log.info("User saving started");
            user = adMUserDao.save(user);
            log.info("User saving completed : {}", user.getUserName());
            if (user.getUserId() == null) {
                log.error("User not saved");
                throw new BadRequestAlertException("USER_NOT_SAVED. ERROR_FOUND", ENTITY_NAME, "USER_NOT_SAVED");
            } else {
                saveUserBranch(user);
                log.info("Branch Department Mapping successes");
                responseDto.setMessage("User Created Successfully");
                responseDto.setData(adMUserMapper.toDto(user));
                responseDto.setReference(String.valueOf(user.getUserId()));
                return ResponseEntity.ok(responseDto);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            responseDto.setMessage(e.getMessage());
            responseDto.setData(null);
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    public String selectUserName(UsRUserIdType userIdType, String nic, String mobileNo, String email) {
        String username;
        List<UsRLoginType> loginTypeList = loginTypeDao.findAll();
        if (loginTypeList.isEmpty()) {
            throw new BadRequestAlertException("Login types not found", ENTITY_NAME, "USER_TYPE_NOT_FOUND");
        } else {
            Integer loginTypeId = userIdType.getUsRLoginType().getLoginTypeId();
            if (Objects.equals(loginTypeId, HardCodeConstants.LOGIN_TYPE_NIC_ID)) {
                if (nic == null || nic.isEmpty()) {
                    throw new BadRequestAlertException("NIC is required.", ENTITY_NAME, "error");
                }

//                if (email == null || email.isEmpty()) {
//                    throw new BadRequestAlertException("Email address is required.", ENTITY_NAME, "error");
//                }

                if (mobileNo == null || mobileNo.isEmpty()) {
                    throw new BadRequestAlertException("Mobile number is required.", ENTITY_NAME, "error");
                }

                username = nic;
                log.info("Select nic as username");
            } else if (Objects.equals(loginTypeId, HardCodeConstants.LOGIN_TYPE_MOBILE_ID)) {
                if (mobileNo == null || mobileNo.isEmpty()) {
                    throw new BadRequestAlertException("Mobile number is required.", ENTITY_NAME, "error");
                }
                username = mobileNo;
                log.info("Select mobile number as username");
            } else if (Objects.equals(loginTypeId, HardCodeConstants.LOGIN_TYPE_EMAIL_ID)) {
                if (email == null || email.isEmpty()) {
                    throw new BadRequestAlertException("Email address is required.", ENTITY_NAME, "error");
                }
                username = email;
                log.info("Select email as username");
            } else {
                log.error("Login type not found");
                throw new BadRequestAlertException("Login type not found", ENTITY_NAME, "USER_TYPE_NOT_FOUND");
            }

            //Check user already exist
            Optional<AdMUser> optUser = adMUserDao.findByUserName(username);
            if (optUser.isPresent()) {
                throw new BadRequestAlertException("User already registered with the given username.", ENTITY_NAME, "error");
            }

            //Check email, nic, mobile number and referenceNo already registered
            if (email != null && !email.isEmpty()) {
                List<AdMUser> optUserByEmail = adMUserDao.findByEmail(email);
                if (!optUserByEmail.isEmpty()) {
                    throw new BadRequestAlertException("User already registered with the given email.", ENTITY_NAME, "error");
                }
            }
            if (nic != null && !nic.isEmpty()) {
                List<AdMUser> optUserByNic = adMUserDao.findByNic(nic);
                if (!optUserByNic.isEmpty()) {
                    throw new BadRequestAlertException("User already registered with the given NIC.", ENTITY_NAME, "error");
                }
            }
            if (mobileNo != null && !mobileNo.isEmpty()) {
                List<AdMUser> optUserByMobileNo = adMUserDao.findByMobileNo(mobileNo);
                if (!optUserByMobileNo.isEmpty()) {
                    throw new BadRequestAlertException("User already registered with the given mobile number.", ENTITY_NAME, "error");
                }
            }
            return username;
        }
    }

    public void saveUserBranch(AdMUser savedUser) {
        Optional<UsRBrachDepartment> usRBranchDepartment = brachDepartmentDao.findByUsMBranchBranchIdAndUsMDepartmentDepartmentId(savedUser.getUsMBranch().getBranchId(), savedUser.getUsMDepartment().getDepartmentId());
        if (usRBranchDepartment.isPresent()) {
            UsRUserBranch usRUserBranch = new UsRUserBranch();
            usRUserBranch.setAdMUser(savedUser);
            usRUserBranch.setFrom(LocalDate.now());
            usRUserBranch.setTo(LocalDate.now());
            usRUserBranch.setIsDefualtDept(1);
            usRUserBranch.setUsRBrachDepartment(usRBranchDepartment.get());
            usRUserBranch.setUsRStatusDetail(savedUser.getUsRStatusDetail());
            userBranchDao.save(usRUserBranch);
        } else {
            log.error("Branch Department mapping not found for branchId : {} and departmentId : {}", savedUser.getUsMBranch().getBranchId(), savedUser.getUsMDepartment().getDepartmentId());
            throw new BadRequestAlertException("Branch Department mapping not found for branchId : " + savedUser.getUsMBranch().getBranchId() + " and departmentId : " + savedUser.getUsMDepartment().getDepartmentId(), "ERROR", "ERROR");
        }
    }

    @Override
    public ResponseEntity<ResponseDto> updateUser(AdMUserDto adMUserDto, String userType) {
        ResponseDto responseDto = new ResponseDto();
        try {
            if (adMUserDto.getUserId() == null) {
                throw new BadRequestAlertException("USER_ID_NOT_FOUND", ENTITY_NAME, "USER_ID_NOT_FOUND");
            } else {
                Optional<AdMUser> optUser = adMUserDao.findById(adMUserDto.getUserId());
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("USER_NOT_FOUND", ENTITY_NAME, "USER_NOT_FOUND");
                } else {
                    Optional<UsMUserRole> optUserRole = userRoleDao.findById(adMUserDto.getUserRoleId());
                    Optional<UsRStatusDetail> optStatusDetails = statusDetailDao.findById(adMUserDto.getStatusId());
                    Optional<UsMDepartment> optDepartment = departmentDao.findById(adMUserDto.getDepartmentId());
                    Optional<UsMBranch> optBranch = branchDao.findById(adMUserDto.getBranchId());
                    if (!optUserRole.isPresent()) {
                        throw new BadRequestAlertException("USER_ROLE_NOT_FOUND", ENTITY_NAME, "USER_ROLE_NOT_FOUND");
                    } else if (!optStatusDetails.isPresent()) {
                        throw new BadRequestAlertException("STATUS_DETAILS_NOT_FOUND", ENTITY_NAME, "STATUS_DETAILS_NOT_FOUND");
                    } else if (!optDepartment.isPresent()) {
                        throw new BadRequestAlertException("DEPARTMENT_NOT_FOUND", ENTITY_NAME, "DEPARTMENT_NOT_FOUND");
                    } else if (!optBranch.isPresent()) {
                        throw new BadRequestAlertException("BRANCH_NOT_FOUND", ENTITY_NAME, "BRANCH_NOT_FOUND");
                    } else {
                        log.info("Foreign key details found");
                    }
                    AdMUser user = optUser.get();
                    user.setDescription(adMUserDto.getDescription());
                    user.setIsFirstLogin(adMUserDto.getIsFirstLogin());
                    user.setIsActive(adMUserDto.getIsActive());
                    user.setEmail(adMUserDto.getEmail());
                    user.setMobileNo(adMUserDto.getMobileNo());
                    user.setFirstName(adMUserDto.getFirstName());
                    user.setLastName(adMUserDto.getLastName());
                    user.setUsMUserRole(optUserRole.get());
                    user.setUsRStatusDetail(optStatusDetails.get());
                    user.setUsMBranch(optBranch.get());
                    user.setUsMDepartment(optDepartment.get());

                    user = adMUserDao.save(user);
                    responseDto.setMessage("User Updated Successfully");
                    responseDto.setData(adMUserMapper.toDto(user));
                    return ResponseEntity.ok(responseDto);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            responseDto.setMessage(e.getMessage());
            responseDto.setData(null);
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<AdMUserDto> getOneUser(Integer userId) {
        try {
            if (userId == null) {
                throw new BadRequestAlertException("USER_ID_NOT_FOUND", ENTITY_NAME, "USER_ID_NOT_FOUND");
            } else {
                Optional<AdMUser> optUser = adMUserDao.findByUserIdAndIsActive(userId, ActiveStatus.ACTIVE.getValue());
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("USER_NOT_FOUND", ENTITY_NAME, "USER_NOT_FOUND");
                } else {
                    log.info("User found!!");
                    return ResponseEntity.ok(adMUserMapper.toDto(optUser.get()));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<AdMUserDto> findUserByUserName(String userName) {
        try {
            if (userName.isEmpty() || userName == null) {
                throw new BadRequestAlertException("USER_NAME_NOT_FOUND", ENTITY_NAME, "USER_NAME_NOT_FOUND");
            } else {
                Optional<AdMUser> optUser = adMUserDao.findByUserNameAndIsActive(userName, ActiveStatus.ACTIVE.getValue());
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("USER_NOT_FOUND", ENTITY_NAME, "USER_NOT_FOUND");
                } else {
                    log.info("User found!!");
                    return ResponseEntity.ok(adMUserMapper.toDto(optUser.get()));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<String> inactivateUser(Integer userId) {
        try {
            if (userId == null) {
                throw new BadRequestAlertException("USER_ID_NOT_FOUND", ENTITY_NAME, "USER_ID_NOT_FOUND");
            } else {
                Optional<AdMUser> optUser = adMUserDao.findById(userId);
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("USER_NOT_FOUND", ENTITY_NAME, "USER_NOT_FOUND");
                } else {
                    AdMUser user = optUser.get();
                    user.setIsActive(ActiveStatus.INACTIVE.getValue());
                    adMUserDao.save(user);
                    return ResponseEntity.ok("User Inactivated");
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<AdMUserDto>>> getAllUsers(Integer page, Integer per_page, String
            sort, String direction, String search) {
        try {
            Page<AdMUser> dbData = null;
            if (direction.equalsIgnoreCase("asc")) {
                dbData = adMUserDao.findAllUsersForTable(search.toLowerCase(), PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
            } else {
                dbData = adMUserDao.findAllUsersForTable(search.toLowerCase(), PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
            }
            ApiResponseDto<List<AdMUserDto>> responseDto = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) dbData.getTotalElements());
            responseDto.setPagination(paginationDto);
            responseDto.setResult(adMUserMapper.listToDto(dbData.getContent()));
            return ResponseEntity.ok(responseDto);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<List<AdMUserDto>> getAllUsersForDropdown() {
        try {
            List<AdMUser> dbData = adMUserDao.findAllByIsActive(ActiveStatus.ACTIVE.getValue());
            if (dbData.isEmpty()) {
                throw new BadRequestAlertException("USER_LIST_NOT_FOUND", ENTITY_NAME, "USER_LIST_NOT_FOUND");
            } else {
                return ResponseEntity.ok(adMUserMapper.listToDto(dbData));
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
        }
    }

    @Override
    @Transactional
    public ResponseEntity<String> passwordReset(PasswordResetDto passwordResetDto, String userName) {
        try {
            AdMUser adMUser;
            Optional<AdMUser> optAdMUser = adMUserDao.findByUserNameAndIsActive(userName, ActiveStatus.ACTIVE.getValue());
            if (!optAdMUser.isPresent()) {
                throw new BadRequestAlertException("USER_NOT_FOUND", ENTITY_NAME, "USER_NOT_FOUND");
            } else {
                adMUser = optAdMUser.get();
            }

            //check current password
            Boolean isCorrect = PasswordUtils.isPasswordValid(passwordResetDto.getCurrentPassword(), optAdMUser.get().getPassword());
            if (isCorrect) {
                //Check password is valid or not
                String result = checkValidPassword(passwordResetDto.getNewPassword());
                if (!result.equals("PASSWORD IS VALID")) {
                    throw new BadRequestAlertException(result, ENTITY_NAME, "PASSWORD ERROR");
                }

                //check new password and re password are same
                if (!passwordResetDto.getNewPassword().equals(passwordResetDto.getReNewPassword())) {
                    throw new BadRequestAlertException("PASSWORD_MISMATCH", ENTITY_NAME, "PASSWORD_MISMATCH");
                }

                String encryptedNewPassword = PasswordUtils.encodePassword(passwordResetDto.getNewPassword());
                adMUser.setPassword(encryptedNewPassword);
                if (adMUser.getIsFirstLogin()) {
                    log.info("First Login");
                    adMUser.setIsFirstLogin(false);
                }
                adMUserDao.save(adMUser);
                return ResponseEntity.ok("Password Reset Successfully");

            } else {
                throw new BadRequestAlertException("CURRENT_PASSWORD_INCORRECT", ENTITY_NAME, "ERROR");
            }
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<TokenValidateDto> validateToken(ValidateTokenRequestDto validateTokenRequestDto) {
        try {
            TokenValidateDto tokenValidateDto = new TokenValidateDto();
            Boolean isValid = JwtUtils.validateToken(validateTokenRequestDto.getToken());
            if (isValid) {
                String userName = JwtUtils.getUsernameFromToken(validateTokenRequestDto.getToken());
                tokenValidateDto.setIsValid(true);
                tokenValidateDto.setUserName(userName);
                return ResponseEntity.ok(tokenValidateDto);
            } else {
                throw new BadRequestAlertException("Token is not valid", ENTITY_NAME, "ERROR");
            }
        } catch (JWTVerificationException e) {
            log.error("JWT verification failed: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new TokenValidateDto(false, null));
        } catch (Exception e) {
            log.error("An unexpected error occurred: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new TokenValidateDto(false, null));
        }
    }

    @Override
    public ResponseEntity<AuthenticationResponse> login(LoginDto loginDto) {
        try {
            AdMUser user;
            if (loginDto.getUserName().isEmpty() || loginDto.getUserName() == null) {
                throw new BadRequestAlertException("User name is required", ENTITY_NAME, "User name is required");
            } else if (loginDto.getPassword().isEmpty() || loginDto.getPassword() == null) {
                throw new BadRequestAlertException("Password is required", ENTITY_NAME, "Password is required");
            } else {
                Optional<AdMUser> optUser = adMUserDao.findByUserNameAndIsActive(loginDto.getUserName(), ActiveStatus.ACTIVE.getValue());
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("Invalid credentials", ENTITY_NAME, "User not found");
                } else {
                    user = optUser.get();
                }
            }
            if (!Objects.equals(user.getUsRStatusDetail().getStatusId(), HardCodeConstants.ACTIVE_AUTHORIZED_STATUS_ID)) {
                throw new BadRequestAlertException("User is not authorized", ENTITY_NAME, "User is not authorized");
            }

            Boolean isCorrect = PasswordUtils.isPasswordValid(loginDto.getPassword(), user.getPassword());
            if (!isCorrect) {
                throw new BadRequestAlertException("Invalid credentials", ENTITY_NAME, "Invalid credentials");
            } else {
                log.info("User logged in successfully");
                AuthenticationResponse authenticationResponse = new AuthenticationResponse();

                //Generate Token
                String authToken = JwtUtils.generateToken(user.getUserName());

                //Set Token Details
                List<UsMBranchDto> branchList = new ArrayList<>();
                List<UsMDepartmentDto> departmentList = new ArrayList<>();
                branchList.add(branchMapper.toDto(user.getUsMBranch()));
                departmentList.add(departmentMapper.toDto(user.getUsMDepartment()));

                authenticationResponse.setAccessToken(authToken);
                authenticationResponse.setExpiresIn((int) EXPIRATION_TIME);
                authenticationResponse.setUserId(user.getUserId());
                authenticationResponse.setUserName(user.getUserName());
                authenticationResponse.setUserRoleName(user.getUsMUserRole().getUserRoleName());
                authenticationResponse.setUserRoleId(user.getUsMUserRole().getUserRoleId());
                authenticationResponse.setBranchList(branchList);
                authenticationResponse.setDepartmentList(departmentList);
                authenticationResponse.setFirstName(user.getFirstName());
                authenticationResponse.setLastName(user.getLastName());
                authenticationResponse.setEmail(user.getEmail());
                authenticationResponse.setMobile(user.getMobileNo());
                authenticationResponse.setIsFirstLogin(user.getIsFirstLogin());
                authenticationResponse.setUserTypeId(user.getUsRUserIdType().getUserTypeId());
                authenticationResponse.setUserType(user.getUsRUserIdType().getUserCategory());
                authenticationResponse.setCompanyId(user.getComId());
                authenticationResponse.setIsExistingCompany(user.getIsExistingCompany());
                authenticationResponse.setReferenceNo(user.getReferenceNo());

                authenticationResponse = calculateUsrPasswordExpDatesForLogin(user, authenticationResponse);
                return ResponseEntity.ok(authenticationResponse);
            }
        } catch (Exception e) {
            e.fillInStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    private AuthenticationResponse calculateUsrPasswordExpDatesForLogin(AdMUser user, AuthenticationResponse
            authenticationResponse) {
        long passwordExpDateCount;
        long userExpDateCount;
        Optional<GlobalDetails> gd = globalDetailsDao.findById(HardCodeConstants.GLOBAL_DETAILS_ID);
        if (!gd.isPresent()) {
            throw new BadRequestAlertException("GLOBAL_DETAILS NOT FOUND", ENTITY_NAME, "GLOBAL_DETAILS NOT FOUND");
        }

        LocalDate today = LocalDate.now();
        LocalDate uExpDate = user.getUserExpDate();
        LocalDate pExpDate = user.getPasswordExpDate();

        long passDayCount = Duration.between(today.atStartOfDay(), pExpDate.atStartOfDay()).toDays();
        long userDayCount = Duration.between(today.atStartOfDay(), uExpDate.atStartOfDay()).toDays();

        passwordExpDateCount = Math.abs(passDayCount);
        userExpDateCount = Math.abs(userDayCount);
        System.out.println("DATE : " + userDayCount);
        if (userExpDateCount <= 0) {
            throw new BadRequestAlertException("USER_EXPIRED", ENTITY_NAME, "USER_ACC");
        }
        authenticationResponse.setUserResetDateCount((int) userDayCount);

        if (passwordExpDateCount <= 0) {
            Optional<AdMUser> updateUser = adMUserDao.findByUserName(user.getUserName());
            updateUser.ifPresent(adMUser -> adMUser.setIsFirstLogin(true));
        }
        authenticationResponse.setPasswordResetDateCount((int) passwordExpDateCount);
        return authenticationResponse;
    }

    @Override
    @Transactional
    public ResponseEntity<ResponseDto> authorizedUser(Integer userId, String status) {
        ResponseDto response = new ResponseDto();
        System.out.println("Inside method");
        Optional<AdMUser> optionalAdMUser = adMUserDao.findById(userId);
        System.out.println("userId---------------- " + userId);
        if (optionalAdMUser.isPresent()) {
            System.out.println("Inside present");


            Optional<GlobalDetails> optGlobalDetails = globalDetailsDao.findById(HardCodeConstants.GLOBAL_DETAILS_ID);
            if (!optGlobalDetails.isPresent()) {
                throw new BadRequestAlertException("GLOBAL_DETAILS_NOT_FOUND", ENTITY_NAME, "GLOBAL_DETAILS_NOT_FOUND");
            }

            if (status.equals("true")) {
                AdMUserDto userDto = adMUserMapper.toDto(optionalAdMUser.get());
                if (userDto.getStatusId() == HardCodeConstants.NEW_STATUS_ID) {
                    System.out.println("inside status true and status new");
                    //Set status ID 3
                    userDto.setStatusId(HardCodeConstants.ACTIVE_AUTHORIZED_STATUS_ID);
                    userDto.setIsActive(true);
                    AdMUser ad = adMUserDao.saveAndFlush(adMUserMapper.toEntity(userDto));
                    AdMUserDto resUser = adMUserMapper.toDto(ad);

                    /** Send auto password to mail address */
                    new Thread(() -> {
                        GlobalDetails globalDetails;
                        globalDetails = optGlobalDetails.get();

                        String username = resUser.getUserName();

                        try {
                            String emailSubject = "GoAML SYSTEM USER REGISTRATION";

                            String emailBody = "<h4>Dear " + resUser.getFirstName() + " " + resUser.getLastName() + ",</h4>" + "<h3 style='color:green;'>User Account Creation</h3>" + "<p>Welcome to Go AML System.<br><br>" + "Your account has been created successfully. Please use the following credentials to login to the system.<br><br>" + "Username: " + username + "<br>" + "Password: " + resUser.getSecureKey() + "<br><br>" + "<p>Thank You!,</p> " + "<p>System Administrator<br>" + "Email :support@olak.org<br>" + "<p style=\"color:red\">Please do not reply to this email</p>";

                            String sms = "Your Username for the 'Go Aml' has been created as\nUsername: " + username + "\nPassword: " + resUser.getSecureKey() + "\nPlease make sure to change the password at the first login. Thank you. For inquiries visit www.goaml.lk\nGo Aml";

                            SMSMailConfig.sendNotification(resUser.getMobileNo(), resUser.getEmail(), emailSubject, emailBody, sms, globalDetails.getUserNotificationMode());

                        } catch (Exception e) {
                            log.error(e.getMessage());
                            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
                        }
                    }).start();

                    response.setData(resUser);
                    response.setMessage("Authorized Success");
                    return ResponseEntity.ok(response);
                } else if (userDto.getStatusId() == HardCodeConstants.EDITED_STATUS_ID) {
                    System.out.println("inside status true and status edited");
                    userDto.setStatusId(HardCodeConstants.ACTIVE_AUTHORIZED_STATUS_ID);
                    userDto.setIsActive(true);
                    AdMUser ad = adMUserDao.saveAndFlush(adMUserMapper.toEntity(userDto));
                    AdMUserDto resUser = adMUserMapper.toDto(ad);

                    response.setData(resUser);
                    response.setMessage("Authorized Success");
                    return ResponseEntity.ok(response);
                } else if (userDto.getStatusId() == HardCodeConstants.RESTRICTED_STATUS_ID) {
                    //Need clarifications
                    System.out.println("inside status true and status restricted");
                    userDto.setStatusId(HardCodeConstants.RESTRICTED_STATUS_ID);
                    userDto.setIsActive(false);
                    AdMUser ad = adMUserDao.saveAndFlush(adMUserMapper.toEntity(userDto));
                    AdMUserDto resUser = adMUserMapper.toDto(ad);

                    response.setData(resUser);
                    response.setMessage("User Restricted");
                    return ResponseEntity.ok(response);
                } else {
                    response.setMessage("Authorization Failed");
                    return ResponseEntity.ok(response);
                }
            } else if (status.equals("false")) {
                AdMUserDto userDto = adMUserMapper.toDto(optionalAdMUser.get());
                if (userDto.getStatusId() == HardCodeConstants.NEW_STATUS_ID) {
                    System.out.println("inside statue false");
                    //Set status ID 3
                    userDto.setStatusId(HardCodeConstants.RESTRICTED_STATUS_ID);
                    AdMUser ad = adMUserDao.saveAndFlush(adMUserMapper.toEntity(userDto));
                    AdMUserDto resUser = adMUserMapper.toDto(ad);

                    response.setData(resUser);
                    response.setMessage("User Restricted");
                    return ResponseEntity.ok(response);
                } else {
                    response.setMessage("Authorization Failed");
                    return ResponseEntity.ok(response);
                }
            } else {
                response.setMessage("Authorization Failed");
                return ResponseEntity.ok(response);
            }
        } else {
            response.setMessage("User not found");
            return ResponseEntity.ok(response);
        }
    }

    @Override
    public CheckUserAvailableResponseDto checkUserAvailable(CheckUserAvailableReqDto user) {
        CheckUserAvailableResponseDto response = new CheckUserAvailableResponseDto();
        Optional<AdMUser> dbUser = adMUserDao.findAdMUserByUserNameAndEmailAndMobileNo(user.getUsername(), user.getEmail(), user.getMobileNo());
        if (dbUser.isPresent()) {
            response.setStatus(true);
            response.setMessage("USER FOUND");

//            sendOTP(dbUser.get().getMobileNo());

        } else {
            response.setStatus(false);
            response.setMessage("USER NOT FOUND");
        }
        return response;
    }

//    @Override
//    public ApiResponseDto<List<GetUserResponseDto>> getUsersList(int page, Integer per_page, String sort, String
//            direction, String search, Integer userRoleId, Integer statusId, Integer userId, Integer branchId, Integer
//                                                                         monthFilterFlag, LocalDate fromDate, LocalDate toDate, Boolean isActive, String isAuthorizationList) {
//
//        log.info("Getting Users for table {} page:" + page + " per_page: " + per_page + " sort: " + sort);
//
//        if (!isAuthorizationList.equalsIgnoreCase("true")) {
//            Page<AdMUser> dbData = null;
//            if (direction.equalsIgnoreCase("asc")) {
//                dbData = adMUserDao.findAllUsersForTableWithFilter(search.toLowerCase(), userRoleId, statusId, branchId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
//            } else {
//                dbData = adMUserDao.findAllUsersForTableWithFilter(search.toLowerCase(), userRoleId, statusId, branchId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
//            }
//
//            //Map user list to response dto
////            List<GetUserResponseDto> userResponseDtoList = dbData.stream().map(UserServiceImpl::getGetUserResponseDto).collect(Collectors.toList());
//
//            List<GetUserResponseDto> responseList;
//            responseList = dbData.stream().map(UserServiceImpl::getGetUserResponseDto).collect(Collectors.toList());
//
//            ApiResponseDto<List<GetUserResponseDto>> response = new ApiResponseDto<>();
//            PaginationDto paginationDto = new PaginationDto();
//            paginationDto.setTotal((int) dbData.getTotalElements());
//            response.setPagination(paginationDto);
//            response.setResult(responseList);
//            return response;
//        } else {
//            List<Integer> statusIdList = new ArrayList<>();
//            statusIdList.add(HardCodeConstants.NEW_STATUS_ID);
//            statusIdList.add(HardCodeConstants.EDITED_STATUS_ID);
//            statusIdList.add(HardCodeConstants.RESTRICTED_STATUS_ID);
//            Integer companyId = null;
//            Page<AdMUser> dbData = null;
//            if (userId != null) {
//                Optional<AdMUser> optUser = adMUserDao.findById(userId);
//                if (!optUser.isPresent()) {
//                    throw new BadRequestAlertException("USER_NOT_FOUND", ENTITY_NAME, "USER_NOT_FOUND");
//                } else {
//                    log.info("Login user present");
//                    log.info("Login user type : {}", optUser.get().getUsRUserIdType().getUserTypeId());
//                    AdMUser user = optUser.get();
//                    if (user.getUsRUserIdType().getUserTypeId() == HardCodeConstants.COMPANY_USER_TYPE_ID) {
//                        companyId = user.getLdMCompanyDetail().getComId();
//                        log.info("Login user company : {}", companyId);
//                        log.info("Login user company name : {}", user.getLdMCompanyDetail().getApplicantName());
//                    }
//                }
//            }
//            if (companyId != null) {
//                dbData = adMUserDao.findAuthorizedUsersForCompany(statusIdList, companyId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
//            } else if (direction.equalsIgnoreCase("asc")) {
//                dbData = adMUserDao.findAllAuthorizedUsersForTable(statusIdList, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
//            } else {
//                dbData = adMUserDao.findAllAuthorizedUsersForTable(statusIdList, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
//            }
//
//            List<GetUserResponseDto> responseList;
//            responseList = dbData.stream().map(UserServiceImpl::getGetUserResponseDto).collect(Collectors.toList());
//
//            ApiResponseDto<List<GetUserResponseDto>> response = new ApiResponseDto<>();
//            PaginationDto paginationDto = new PaginationDto();
//            paginationDto.setTotal(responseList.size());
//            response.setPagination(paginationDto);
//            response.setResult(responseList);
//            return response;
//        }
//
//    }

    @Override
    public UserProfileDto getUserProfiile(Integer userId) {

        UserProfileDto response = new UserProfileDto();
        Optional<AdMUser> dbUser = adMUserDao.findById(userId);
        if (dbUser.isPresent()) {
            AdMUser user = dbUser.get();

            LocalDate today = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth());
            LocalDate pExpDate = LocalDate.of(user.getPasswordExpDate().getYear(), user.getPasswordExpDate().getMonth(), user.getPasswordExpDate().getDayOfMonth());

            long daysCount = Duration.between(today.atStartOfDay(), pExpDate.atStartOfDay()).toDays();

            response.setPasswordExpDateCount(Integer.valueOf(Long.toString(daysCount)));
            response.setUserId(user.getUserId());
            response.setDescription(user.getDescription());
            response.setUserExpDate(user.getUserExpDate());
            response.setPasswordExpDate(user.getPasswordExpDate());
            response.setFirstName(user.getFirstName());
            response.setLastName(user.getLastName());
            response.setEmpEmail(user.getEmail());
            response.setEmpMobileNumber(user.getMobileNo());
            response.setUserName(user.getUserName());

        } else {
            throw new BadRequestAlertException("USER_NOT_FOUND", ENTITY_NAME, "USER");
        }
        return response;

    }

    @Override
    public UserProfileDto getByUserName(String username) {
        UserProfileDto response = new UserProfileDto();
        Optional<AdMUser> dbUser = adMUserDao.findByUserName(username);
        if (dbUser.isPresent()) {
            AdMUser user = dbUser.get();

            LocalDate today = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth());
            LocalDate pExpDate = LocalDate.of(user.getPasswordExpDate().getYear(), user.getPasswordExpDate().getMonth(), user.getPasswordExpDate().getDayOfMonth());

            long daysCount = Duration.between(today.atStartOfDay(), pExpDate.atStartOfDay()).toDays();

            response.setPasswordExpDateCount(Integer.valueOf(Long.toString(daysCount)));
            response.setUserId(user.getUserId());
            response.setDescription(user.getDescription());
            response.setUserExpDate(user.getUserExpDate());
            response.setPasswordExpDate(user.getPasswordExpDate());
            response.setFirstName(user.getFirstName());
            response.setLastName(user.getLastName());
            response.setEmpEmail(user.getEmail());
            response.setEmpMobileNumber(user.getMobileNo());
            response.setUserName(user.getUserName());

        } else {
            throw new BadRequestAlertException("USER_NOT_FOUND", ENTITY_NAME, "USER");
        }
        return response;
    }

    @Override
    public ResponseEntity<AdMUserDto> extendingPasswordExpDate(ExtendPasswordDto extendPasswordDto) {

        if (extendPasswordDto.getUserId() == null) {
            throw new BadRequestAlertException("USER_ID NULL", ENTITY_NAME, "ERROR");
        }
        Optional<AdMUser> optUser = adMUserDao.findById(extendPasswordDto.getUserId());
        if (!optUser.isPresent()) {
            throw new BadRequestAlertException("USER NOT FOUND", ENTITY_NAME, "ERROR");
        } else {

            AdMUser user = optUser.get();
            GlobalDetails globalDetails = globalDetailsDao.findById(1).get();

            LocalDate newExpDate = user.getPasswordExpDate().plusDays(globalDetails.getPasswordExpDate());
            user.setPasswordExpDate(newExpDate);
            AdMUserDto updatedUser = adMUserMapper.toDto(adMUserDao.save(user));

            if (optUser.get().getPasswordExpDate() != updatedUser.getPasswordExpDate()) {
                System.out.println("PASSWORD EXP DATE UPDATED");
            }

            return ResponseEntity.ok().body(updatedUser);
        }

    }

    @Override
    public ApiResponseDto<List<GetUserResponseDto>> getUsersList(int page, Integer per_page, String sort, String
            direction, String search, Integer userRoleId, Integer statusId, Integer userId, Integer branchId, Integer
                                                                         monthFilterFlag, LocalDate fromDate, LocalDate toDate, Boolean isActive, String isAuthorizationList) {
        log.info("Getting Users for table {} page:" + page + " per_page: " + per_page + " sort: " + sort);

        if (!isAuthorizationList.equalsIgnoreCase("true")) {
            List<SearchPassDto> searchPassDto = new ArrayList<>();
            List<FilterPassDto> filterPassDto = new ArrayList<>();

            searchPassDto.add(new SearchPassDto("firstName"));
            searchPassDto.add(new SearchPassDto("lastName"));
            searchPassDto.add(new SearchPassDto("email"));
            searchPassDto.add(new SearchPassDto("mobileNo"));
            searchPassDto.add(new SearchPassDto("userName"));

            filterPassDto.add(new FilterPassDto("usMUserRole", "userRoleId", userRoleId));
            filterPassDto.add(new FilterPassDto("usRStatusDetail", "statusId", statusId));
            filterPassDto.add(new FilterPassDto("usMBranch", "branchId", branchId));

            searchNFilter.setPaginationDetails(page, per_page, sort, direction);
            searchNFilter.setVariables(search, ActiveStatus.ACTIVE.getValue());
            searchNFilter.setEntityClass(AdMUser.class);
            searchNFilter.setSearchList(searchPassDto);
            searchNFilter.setFilterList(filterPassDto);

            Page<?> dbData = searchNFilter.getFiltered();

            List<GetUserResponseDto> responseList = new ArrayList<>();

            for (AdMUser d : (List<AdMUser>) dbData.getContent()) {
                GetUserResponseDto userResponseDto = getGetUserResponseDto(d);
                responseList.add(userResponseDto);
            }

            ApiResponseDto<List<GetUserResponseDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) dbData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(responseList);
            return response;
        } else {
            List<Integer> statusIdList = new ArrayList<>();
            statusIdList.add(HardCodeConstants.NEW_STATUS_ID);
            statusIdList.add(HardCodeConstants.EDITED_STATUS_ID);
            statusIdList.add(HardCodeConstants.RESTRICTED_STATUS_ID);
            Integer companyId = null;
            Page<AdMUser> dbData = null;
            if (userId != null) {
                Optional<AdMUser> optUser = adMUserDao.findById(userId);
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("USER_NOT_FOUND", ENTITY_NAME, "USER_NOT_FOUND");
                } else {
                    log.info("Login user present");
                    log.info("Login user type : {}", optUser.get().getUsRUserIdType().getUserTypeId());
                    AdMUser user = optUser.get();
                    if (user.getUsRUserIdType().getUserTypeId() == HardCodeConstants.COMPANY_USER_TYPE_ID) {
                        companyId = user.getComId();
                        log.info("Login user company : {}", companyId);
                    }
                }
            }
            if (companyId != null) {
                dbData = adMUserDao.findAuthorizedUsersForCompany(statusIdList, companyId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
            } else if (direction.equalsIgnoreCase("asc")) {
                dbData = adMUserDao.findAllAuthorizedUsersForTable(statusIdList, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
            } else {
                dbData = adMUserDao.findAllAuthorizedUsersForTable(statusIdList, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
            }

            List<GetUserResponseDto> responseList;
            responseList = dbData.stream().map(UserServiceImpl::getGetUserResponseDto).collect(Collectors.toList());

            ApiResponseDto<List<GetUserResponseDto>> response = new ApiResponseDto<>();
            PaginationDto paginationDto = new PaginationDto();
            paginationDto.setTotal((int) dbData.getTotalElements());
            response.setPagination(paginationDto);
            response.setResult(responseList);
            return response;
        }

    }

    public String checkValidPassword(String password) {

        // Check password length
        if (password.length() < 8 || password.length() > 12) {
            return "PASSWORD LENGTH IS NOT IN 8-12";
        }

        // Check for lowercase character
        boolean hasLowercase = !password.equals(password.toUpperCase());
        if (!hasLowercase) {
            return "NO LOWERCASE LETTER FOUND";
        }

        // Check for uppercase character
        boolean hasUppercase = !password.equals(password.toLowerCase());
        if (!hasUppercase) {
            return "NO UPPERCASE LETTER FOUND";
        }

        // Check for digit
        boolean hasDigit = false;
        for (char c : password.toCharArray()) {
            if (Character.isDigit(c)) {
                hasDigit = true;
                break;
            }
        }
        if (!hasDigit) {
            return "NO DIGITS FOUND";
        }

        // Check for special character
        boolean hasSpecialChar = !password.matches("[A-Za-z0-9 ]*");
        if (!hasSpecialChar) {
            return "NO SPECIAL CHARACTER FOUND";
        }

        return "PASSWORD IS VALID";
    }

    @Override
    public ResponseEntity<UserDataDto> getUserBranchDept(Integer userId, Integer branchId) {

        if (userId == null) {
            throw new BadRequestAlertException("userId null", ENTITY_NAME, "ERROR");
        } else if (branchId == null) {
            throw new BadRequestAlertException("branchId null", ENTITY_NAME, "ERROR");
        }

        UserDataDto userBranchDeptDto = new UserDataDto();

        List<UsRUserBranch> branchList = userBranchDao.findUsRUserBranchByAdMUserUserId(userId);
        if (branchList.isEmpty()) {
            throw new BadRequestAlertException("branch list empty", ENTITY_NAME, "ERROR");
        } else {
            for (UsRUserBranch b : branchList) {
                Optional<UsRBrachDepartment> optBranchDept = brachDepartmentDao.findById(b.getUsRBrachDepartment().getBrachDeptId());
                if (optBranchDept.isPresent() && Objects.equals(optBranchDept.get().getUsMBranch().getBranchId(), branchId)) {

                    AdMUser user = new AdMUser();
                    Optional<AdMUser> optUser = adMUserDao.findById(userId);
                    if (optUser.isPresent()) {
                        user = optUser.get();
                    }

                    userBranchDeptDto.setBranchId(optBranchDept.get().getUsMBranch().getBranchId());
                    userBranchDeptDto.setDepartmentId(optBranchDept.get().getUsMDepartment().getDepartmentId());
                    userBranchDeptDto.setBranchName(optBranchDept.get().getUsMBranch().getBranchName());
                    userBranchDeptDto.setDepartmentName(optBranchDept.get().getUsMDepartment().getDeptName());
                    userBranchDeptDto.setUsername(user.getUserName());
                    userBranchDeptDto.setDisplayName(user.getDescription());
                    userBranchDeptDto.setRoleId(String.valueOf(user.getUsMUserRole().getUserRoleId()));
                    userBranchDeptDto.setRoleName(user.getUsMUserRole().getUserRoleName());
                    return ResponseEntity.ok(userBranchDeptDto);
                }
            }
            throw new BadRequestAlertException("No matching record found", ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ApiResponseDto<List<AdMUserDetailsDto>> getUserListWithoutLoginUser(Integer page, Integer
            per_page, String search, String direction, String sort) {

        AuditUser loginUser = auditUtils.getAuditUser();
        Page<AdMUser> dbData;

        if (direction.equalsIgnoreCase("asc")) {
            dbData = adMUserDao.getAllUsersForTable(search, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else {
            dbData = adMUserDao.getAllUsersForTable(search, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
        }

        List<AdMUserDetailsDto> userList = new ArrayList<>();
        for (AdMUser user : dbData.getContent()) {
            if (!user.getUserName().equals(loginUser.getUserName())) {

                UsMUserRole userRole = userRoleDao.findById(user.getUsMUserRole().getUserRoleId()).get();
                UsRStatusDetail status = statusDetailDao.findById(user.getUsRStatusDetail().getStatusId()).get();

                AdMUserDetailsDto userResponseDto = new AdMUserDetailsDto();

                userResponseDto.setUserId(user.getUserId());
                userResponseDto.setUserName(user.getUserName());
                userResponseDto.setIsActive(user.getIsActive());
                userResponseDto.setDepartmentId(user.getUsMDepartment().getDepartmentId());
                userResponseDto.setBranchId(user.getUsMBranch().getBranchId());
                userResponseDto.setUserRoleId(userRole.getUserRoleId());
                userResponseDto.setStatusId(status.getStatusId());
                userResponseDto.setDescription(user.getDescription());
                userResponseDto.setIsFirstLogin(user.getIsFirstLogin());
                userResponseDto.setIsActive(user.getIsActive());
                userResponseDto.setEmail(user.getEmail());
                userResponseDto.setMobileNo(user.getMobileNo());
                userResponseDto.setSecurityKey(user.getPassword());
                userResponseDto.setUserExpDate(user.getUserExpDate());
                userResponseDto.setPasswordExpDate(user.getPasswordExpDate());

                userList.add(userResponseDto);
            }
        }

        ApiResponseDto<List<AdMUserDetailsDto>> response = new ApiResponseDto<>();
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements() - 1);
        response.setPagination(paginationDto);
        response.setResult(userList);
        return response;
    }

    @Override
    public ResponseEntity<String> forgotPassword(ForgotPasswordDto forgotPasswordDto) {
        try {

            AdMUser user;

            if (forgotPasswordDto.getUserName().isEmpty() || forgotPasswordDto.getUserName() == null) {
                throw new BadRequestAlertException("User name is required", ENTITY_NAME, "User name is required");
            } else {
                Optional<AdMUser> optUser = adMUserDao.findByUserNameAndIsActive(forgotPasswordDto.getUserName(), ActiveStatus.ACTIVE.getValue());
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("User not found", ENTITY_NAME, "User not found");
                } else {
                    user = optUser.get();
                }
            }

            //Get Global Details Data
            Optional<GlobalDetails> gd = globalDetailsDao.findById(HardCodeConstants.GLOBAL_DETAILS_ID);
            if (!gd.isPresent()) {
                throw new BadRequestAlertException("GLOBAL_DETAILS NOT FOUND", ENTITY_NAME, "GLOBAL_DETAILS NOT FOUND");
            }
            GlobalDetailsDto globalDetails = globalDetailsMapper.toDto(gd.get());

            LocalDate userResetDate = LocalDate.now().plusDays(globalDetails.getUserExpDate());
            LocalDate passwordResetDate = LocalDate.now().plusDays(globalDetails.getPasswordExpDate());

            log.info("User reset date       : {}", userResetDate);
            log.info("Password reset date   : {}", passwordResetDate);
            user.setUserExpDate(userResetDate);
            user.setPasswordExpDate(passwordResetDate);

            String newGenPassword = PasswordUtils.generateRandomPassword();
            user.setSecureKey(newGenPassword);

            String encodeNewGenPassword = PasswordUtils.encodePassword(newGenPassword);
            user.setPassword(encodeNewGenPassword);

            AdMUser updatedUser = adMUserDao.save(user);
            log.info("New generated password updated successfully");

            String username = updatedUser.getUserName();

            new Thread(() -> {

                try {

                    String emailSubject = "Go Aml SYSTEM FORGOT PASSWORD REQUEST";

                    String emailBody = "<h4>Dear " + updatedUser.getFirstName() + " " + updatedUser.getLastName() + ",</h4>" + "<h3 style='color:green;'>Your New Password</h3>" + "<p>Welcome to Go Aml System.<br><br>" + "Your account password has been reset successfully. Please use the following credentials to login to the system.<br><br>" + "Username: " + username + "<br>" + "Password: " + updatedUser.getSecureKey() + "<br>" + "<p>Thank You!,</p> " + "<p>System Administrator<br>" + "Email :support@olak.org<br>" + "<p style=\"color:red\">Please do not reply to this email</p>";

                    String sms = "Go Aml System forgot password request!\n\n" + "Your account password has been reset successfully. Please use the following credentials to login to the system.\n\n" + "Username: " + username + "\n" + "Password: " + updatedUser.getSecureKey() + "\n\n" + "Please contact 011 XXX XX XX for assistance";

                    SMSMailConfig.sendNotification(updatedUser.getMobileNo(), updatedUser.getEmail(), emailSubject, emailBody, sms, globalDetails.getUserNotificationMode());

                } catch (Exception e) {
                    log.error(e.getMessage());
                    throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
                }
            }).start();
            return ResponseEntity.ok("Password reset successfully. New password sent to your email address.");


        } catch (Exception e) {
            log.error("Error in forgot password : " + e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "ERROR", "ERROR");
        }
    }

    @Override
    public ResponseEntity<AdMUserDto> restrictAdMUser(Integer userId) {
        try {
            if (userId == null) {
                throw new BadRequestAlertException("User id null", ENTITY_NAME, "ERROR");
            } else {
                /** Check active user exist or not */
                Optional<AdMUser> optUser = adMUserDao.findByUserIdAndIsActive(userId, ActiveStatus.ACTIVE.getValue());
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("User not found", ENTITY_NAME, "ERROR");
                } else {

                    Optional<UsRStatusDetail> optStatus = statusDetailDao.findById(HardCodeConstants.RESTRICTED_STATUS_ID);
                    if (!optStatus.isPresent()) {
                        throw new BadRequestAlertException("Status not found", ENTITY_NAME, "ERROR");
                    }

                    AdMUser user = optUser.get();
                    /** setIsActive to false */
                    user.setIsActive(true);
                    user.setUsRStatusDetail(optStatus.get());
                    user = adMUserDao.save(user);
                    return ResponseEntity.ok(adMUserMapper.toDto(user));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<AdMUserDetailsDto>>> getAdMUserDetails(Integer page, Integer
            per_page, String search, String direction, String sort, String employeeId, String userRoleId, String
                                                                                             statusId, String branchId, String departmentId) {
        //search me
        String loggedUser = auditUtils.getAuditUser().getUserName();
        List<SearchPassDto> searchPassDto = new ArrayList<>();
        List<FilterPassDto> filterPassDto = new ArrayList<>();

//        filterPassDto.add(new FilterPassDto("usMEmployee", "employeeId", employeeId));
        filterPassDto.add(new FilterPassDto("usMUserRole", "userRoleId", userRoleId));
        filterPassDto.add(new FilterPassDto("usRStatusDetail", "statusId", statusId));
        filterPassDto.add(new FilterPassDto("usMBranch", "branchId", branchId));
        filterPassDto.add(new FilterPassDto("usMDepartment", "departmentId", departmentId));

        searchNFilter.setPaginationDetails(page, per_page, sort, direction);
        searchNFilter.setVariables(search, ActiveStatus.ACTIVE.getValue());
        searchNFilter.setEntityClass(AdMUser.class);
        searchNFilter.setSearchList(searchPassDto);
        searchNFilter.setFilterList(filterPassDto);

        Page<?> dbData = searchNFilter.getFiltered();

        List<AdMUserDetailsDto> responseList = new ArrayList<>();
        for (AdMUser d : (List<AdMUser>) dbData.getContent()) {

            if (!d.getUserName().equals(loggedUser)) {
                AdMUserDetailsDto adMUserDetailsDto = new AdMUserDetailsDto();

                adMUserDetailsDto.setUserId(d.getUserId());
                adMUserDetailsDto.setUserName(d.getUserName());
                adMUserDetailsDto.setDescription(d.getDescription());
                adMUserDetailsDto.setIsFirstLogin(d.getIsFirstLogin());
                adMUserDetailsDto.setIsActive(d.getIsActive());
                adMUserDetailsDto.setEmail(d.getEmail());
                adMUserDetailsDto.setMobileNo(d.getMobileNo());
                adMUserDetailsDto.setSecurityKey(d.getPassword());
                adMUserDetailsDto.setUserExpDate(d.getUserExpDate());
                adMUserDetailsDto.setPasswordExpDate(d.getPasswordExpDate());
//                adMUserDetailsDto.setEmployeeId(d.getUsMEmployee().getEmployeeId());
                adMUserDetailsDto.setUserRoleId(d.getUsMUserRole().getUserRoleId());
                adMUserDetailsDto.setStatusId(d.getUsRStatusDetail().getStatusId());
                adMUserDetailsDto.setBranchId(d.getUsMBranch().getBranchId());
                adMUserDetailsDto.setDepartmentId(d.getUsMDepartment().getDepartmentId());

                responseList.add(adMUserDetailsDto);
            }
        }

        ApiResponseDto<List<AdMUserDetailsDto>> response = new ApiResponseDto<>();
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);
        response.setResult(responseList);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDetailsDto> userDetails(String token) {
        try {

            String username = "";
            ResponseEntity<TokenValidateDto> response = validateToken(new ValidateTokenRequestDto(token));
            if (response.getStatusCodeValue() == 200) {
                username = Objects.requireNonNull(response.getBody()).getUserName();
            } else {
                throw new BadRequestAlertException("Invalid token", ENTITY_NAME, "ERROR");
            }

            AdMUser user = adMUserDao.findByUserName(username).get();

            List<UsMBranchDto> branchList = new ArrayList<>();
            List<UsMDepartmentDto> departmentList = new ArrayList<>();
            branchList.add(branchMapper.toDto(user.getUsMBranch()));
            departmentList.add(departmentMapper.toDto(user.getUsMDepartment()));

            UserDetailsDto userDetailsDto = getUserDetailsDto(user, branchList, departmentList);

            userDetailsDto = calculateUsrPasswordExpDates(user, userDetailsDto);

            return ResponseEntity.ok(userDetailsDto);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    public UserDetailsDto calculateUsrPasswordExpDates(AdMUser user, UserDetailsDto userDetailsDto) {
        long passwordExpDateCount;
        long userExpDateCount;
        Optional<GlobalDetails> gd = globalDetailsDao.findById(HardCodeConstants.GLOBAL_DETAILS_ID);
        if (!gd.isPresent()) {
            throw new BadRequestAlertException("GLOBAL_DETAILS NOT FOUND", ENTITY_NAME, "GLOBAL_DETAILS NOT FOUND");
        }

        LocalDate today = LocalDate.now();
        LocalDate uExpDate = user.getUserExpDate();
        LocalDate pExpDate = user.getPasswordExpDate();

        long passDayCount = Duration.between(today.atStartOfDay(), pExpDate.atStartOfDay()).toDays();
        long userDayCount = Duration.between(today.atStartOfDay(), uExpDate.atStartOfDay()).toDays();

        passwordExpDateCount = Math.abs(passDayCount);
        userExpDateCount = Math.abs(userDayCount);
        System.out.println("DATE : " + userDayCount);
        if (userExpDateCount <= 0) {
            throw new BadRequestAlertException("USER_EXPIRED", ENTITY_NAME, "USER_ACC");
        }
        userDetailsDto.setUserResetDateCount((int) userDayCount);

        if (passwordExpDateCount <= 0) {
            Optional<AdMUser> updateUser = adMUserDao.findByUserName(user.getUserName());
            log.info("Password Expired for user : {}", updateUser.get().getUserName());
            log.info("Set isFirstLogin to true for user : {}", updateUser.get().getUserName());
            updateUser.ifPresent(adMUser -> adMUser.setIsFirstLogin(true));
        }
        userDetailsDto.setPasswordResetDateCount((int) passwordExpDateCount);

        userDetailsDto.setUserResetDateCount((int) userExpDateCount);
        return userDetailsDto;
    }

    @Override
    public ResponseEntity<AdMUserDto> getAdMUserByUserId(Integer userId) {
        try {
            if (userId == null) {
                throw new BadRequestAlertException("User id null", ENTITY_NAME, "ERROR");
            } else {
                Optional<AdMUser> optUser = adMUserDao.findByUserIdAndIsActive(userId, ActiveStatus.ACTIVE.getValue());
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("User not found", ENTITY_NAME, "ERROR");
                } else {
                    return ResponseEntity.ok(adMUserMapper.toDto(optUser.get()));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<AdMUserDto> updateAdMUser(Integer userId, AdMUserDto adMUserDto) {
        try {
            if (userId == null) {
                throw new BadRequestAlertException("User id null", ENTITY_NAME, "ERROR");
            } else {
                if (!userId.equals(adMUserDto.getUserId())) {
                    throw new BadRequestAlertException("User id mismatch", ENTITY_NAME, "ERROR");
                } else {
                    Optional<AdMUser> optUser = adMUserDao.findById(userId);
                    if (!optUser.isPresent()) {
                        throw new BadRequestAlertException("User not found", ENTITY_NAME, "ERROR");
                    } else {

                        /** Check user role exist or not*/
                        Optional<UsMUserRole> optUserRole = userRoleDao.findById(adMUserDto.getUserRoleId());
                        if (!optUserRole.isPresent()) {
                            throw new BadRequestAlertException("User role not found", ENTITY_NAME, "ERROR");
                        }

                        /** Check user status exist or not */
                        Optional<UsRStatusDetail> optStatus = statusDetailDao.findById(HardCodeConstants.EDITED_STATUS_ID);
                        if (!optStatus.isPresent()) {
                            throw new BadRequestAlertException("Status not found", ENTITY_NAME, "ERROR");
                        }

                        /** Check branch exist or not */
                        Optional<UsMBranch> optBranch = branchDao.findById(adMUserDto.getBranchId());
                        if (!optBranch.isPresent()) {
                            throw new BadRequestAlertException("Branch not found", ENTITY_NAME, "ERROR");
                        }

                        /** Check department exist or not */
                        Optional<UsMDepartment> optDepartment = departmentDao.findById(adMUserDto.getDepartmentId());
                        if (!optDepartment.isPresent()) {
                            throw new BadRequestAlertException("Department not found", ENTITY_NAME, "ERROR");
                        }

                        /** Check userType exist or not */
                        Optional<UsRUserIdType> optUserType = userIdTypeDao.findById(adMUserDto.getUserTypeId());
                        if (!optUserType.isPresent()) {
                            throw new BadRequestAlertException("User type not found", ENTITY_NAME, "ERROR");
                        }

                        AdMUser user = optUser.get();
                        user.setFirstName(adMUserDto.getFirstName());
                        user.setLastName(adMUserDto.getLastName());
                        user.setMobileNo(adMUserDto.getMobileNo());
                        user.setEmail(adMUserDto.getEmail());
                        user.setDescription(adMUserDto.getDescription());
                        user.setIsActive(false);
                        user.setIsFirstLogin(adMUserDto.getIsFirstLogin());
                        user.setUserName(adMUserDto.getUserName());
                        user.setUserExpDate(adMUserDto.getUserExpDate());
                        user.setPasswordExpDate(adMUserDto.getPasswordExpDate());
                        user.setUsMUserRole(optUserRole.get());
                        user.setUsRStatusDetail(optStatus.get());
                        user.setUsMBranch(optBranch.get());
                        user.setUsMDepartment(optDepartment.get());
                        user.setUsRUserIdType(optUserType.get());
                        user.setNic(adMUserDto.getNic());
                        user.setReferenceNo(adMUserDto.getReferenceNo());
                        user = adMUserDao.save(user);
                        return ResponseEntity.ok(adMUserMapper.toDto(user));
                    }
                }
            }
        } catch (Exception e) {
            e.fillInStackTrace();
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<AdMUserDto> deleteAdMUser(Integer userId) {
        try {

            if (userId == null) {
                throw new BadRequestAlertException("User id null", ENTITY_NAME, "ERROR");
            } else {
                /** Check active user exist or not */
                Optional<AdMUser> optUser = adMUserDao.findByUserIdAndIsActive(userId, ActiveStatus.ACTIVE.getValue());
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("User not found", ENTITY_NAME, "ERROR");
                } else {
                    AdMUser user = optUser.get();
                    /** setIsActive to false */
                    user.setIsActive(false);
                    user = adMUserDao.save(user);
                    return ResponseEntity.ok(adMUserMapper.toDto(user));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<ResponseDto> createAndApproveUser(Integer userTypeId, CreateApproveUserDto createApproveUserDto) {
        try {

            UserRequestDto userRequestDto = getUserRequestDto(userTypeId, createApproveUserDto);

            ResponseEntity<ResponseDto> userCreateRes = createUser("Admin", userRequestDto);
            if (userCreateRes.getStatusCode() == HttpStatus.OK) {
                log.info("User created successfully");
                ResponseDto userCreateResDto = userCreateRes.getBody();
                assert userCreateResDto != null;
                ResponseEntity<ResponseDto> authorizationRes = authorizedUser(Integer.parseInt(userCreateResDto.getReference()), "true");
                if (authorizationRes.getStatusCode() == HttpStatus.OK) {
                    log.info("User authorized successfully");
                    return ResponseEntity.ok(authorizationRes.getBody());
                } else {
                    throw new BadRequestAlertException("User authorization failed", ENTITY_NAME, "ERROR");
                }
            } else {
                throw new BadRequestAlertException("User creation failed", ENTITY_NAME, "ERROR");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<NicResponseDto> validateNicWithDob(NicValidationDto nicValidationDto) {
        // validate given nic
        JSONObject validatedNicObject = nicValidatorAndConvertor(nicValidationDto.getNic(), nicValidationDto.getDob());
        ResponseObject responseObject = (ResponseObject) validatedNicObject.get("resObject");

        NicResponseDto nicResponseDto = new NicResponseDto();

        // error response
        if (responseObject.getCode().equals(error_code)) {
            throw new BadRequestAlertException(responseObject.getDescription(), ENTITY_NAME, "ERROR");
        }

        if (nicValidationDto.getNic() == null) {
            throw new BadRequestAlertException("NIC COULDN'T BE NULL", ENTITY_NAME, "NULL_NIC");
        }

        // error response
        if (responseObject.getCode().equals(success_code)) {
            String validatedNic = validatedNicObject.getString("validatedNic");

            nicResponseDto.setNic(validatedNic);
            nicResponseDto.setIsValid(true);
        } else {
            nicResponseDto.setNic(nicValidationDto.getNic());
            nicResponseDto.setIsValid(false);
        }
        return ResponseEntity.ok(nicResponseDto);
    }

    @Override
    public ResponseEntity<List<AdMUserDto>> getUsersForDropdown() {
        try {
            List<AdMUserDto> userList = adMUserMapper.listToDto(adMUserDao.findAll());

            if (userList.isEmpty()) {
                log.info("User list empty");
                throw new BadRequestAlertException("User list empty", ENTITY_NAME, "ERROR");
            } else {
                return ResponseEntity.ok(userList);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }


    public void ageCheckerWithInsertedDob(String nic, LocalDate birthday) {
        Integer year = Integer.valueOf(nic.substring(0, 4));
        int dateCount = Integer.parseInt(nic.substring(4, 7));
        int month = 0;
        int[] daysArray = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        if (dateCount > 500) {
            dateCount = dateCount - 500;
        }
        //Month
        for (int i = 0; i < daysArray.length; i++) {
            if (dateCount > daysArray[i]) {
                dateCount = dateCount - daysArray[i];
            } else {
                month = i + 1;
                break;
            }
        }
        LocalDate dob = null;
        try {
            dob = LocalDate.of(year, month, dateCount);
            if (dob.equals(birthday)) {
                System.out.println("DOB MATCHED");
            } else {
                throw new BadRequestAlertException(DOB_NOT_MATCHED, ENTITY_NAME, "DOB");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "INVALID_NIC");
        }
        int age = Period.between(dob, LocalDate.now()).getYears();
        System.out.println("Age: " + age);
        System.out.println("Dob: " + dob);
        if (age >= 55) {
            throw new BadRequestAlertException(AGE_RESTRICT, ENTITY_NAME, "ELIGIBILITY");
        }
        AGE = age;
    }


    private JSONObject nicValidatorAndConvertor(String nic, LocalDate birthday) {
        JSONObject result = new JSONObject();
        ResponseObject responseObject = new ResponseObject();
        JSONObject instructions = new JSONObject();
        // nic length check
        // OLD
        if (nic.length() == 10) {
            // check V letter
            String lastChar = String.valueOf(nic.charAt(nic.length() - 1));
            if (lastChar.equalsIgnoreCase("v")) {
                System.out.println("ok");
            } else if (lastChar.equalsIgnoreCase("x")) {
                System.out.println("ok");
            } else {
                // set error
                responseObject.setCode(error_code);
                responseObject.setStatus(error_status);
                responseObject.setDescription("'V' or 'X' REQUIRED FOR NIC!");
                // set instructions
                instructions.put("new_nic", "HAS 12 DIGITS");
                instructions.put("old_nic", "HAS 9 DIGITS AND 'V'");
                responseObject.setInstructions(instructions.toMap());
                // set details
                result.put("resObject", responseObject);
                result.put("entered_nic", nic);
                return result;
            }
            // check given nic has letters
            for (int i = 0; i < nic.length() - 1; i++) {
                String Char = String.valueOf(nic.charAt(i));
                try {
                    Integer.parseInt(Char);
                } catch (NumberFormatException e) {
                    System.out.println(e.getMessage());
                    // set error
                    responseObject.setCode(error_code);
                    responseObject.setStatus(error_status);
                    responseObject.setDescription("ACCEPT ONLY NUMERIC VALUES EXCEPT 'V' or 'X'!");
                    instructions.put("new_nic", "HAS 12 DIGITS");
                    instructions.put("old_nic", "HAS 9 DIGITS AND 'V' or 'X'");
                    responseObject.setInstructions(instructions.toMap());
                    result.put("resObject", responseObject);
                    result.put("entered_nic", nic);
                    return result;
                }
            }
            // OLD NIC TO NEW NIC
            String conNic;
            if ((nic).startsWith("00")) {
                conNic = "20" + nic;
            } else {
                conNic = "19" + nic;
            }
            StringBuilder newNic = new StringBuilder();
            StringBuilder lastDigits = new StringBuilder();
            for (int i = 0; i < conNic.length() - 1; i++) {
                if (i >= 7) {
                    // 8th digit
                    if (i == 7) {
                        newNic.append(0);
                    }
                    // lastDigits
                    String Char = String.valueOf(conNic.charAt(i));
                    lastDigits.append(Char);
                } else {
                    // frontDigits
                    String Char = String.valueOf(conNic.charAt(i));
                    newNic.append(Char);
                }
            }
            newNic.append(lastDigits);

            boolean flag = ageChecker(newNic.toString(), birthday);
            if (flag) {
                // set success
                responseObject.setCode(success_code);
                responseObject.setStatus(success_status);
                responseObject.setDescription("entered_nic: " + nic + ", validated nic: " + newNic);
                result.put("resObject", responseObject);
                result.put("entered_nic", nic);
                result.put("validatedNic", newNic.toString());
                return result;
            } else {
                throw new BadRequestAlertException(DOB_NOT_MATCHED, ENTITY_NAME, "DOB");

            }
        }
        // NEW
        else if (nic.length() == 12) {
            // check given nic has letters
            for (int i = 0; i < nic.length(); i++) {
                String Char = String.valueOf(nic.charAt(i));
                try {
                    Integer.parseInt(Char);
                } catch (NumberFormatException e) {
                    System.out.println(e.getMessage());
                    // set error
                    responseObject.setCode(error_code);
                    responseObject.setStatus(error_status);
                    responseObject.setDescription("ACCEPT ONLY NUMERIC VALUES!");
                    instructions.put("new_nic", "HAS 12 DIGITS");
                    instructions.put("old_nic", "HAS 9 DIGITS AND 'V'");
                    responseObject.setInstructions(instructions.toMap());
                    result.put("resObject", responseObject);
                    result.put("entered_nic", nic);
                    return result;
                }
            }
            // NEW NIC TO OLD NIC
            String conNic = nic.substring(2);
            StringBuilder oldNic = new StringBuilder();
            for (int i = 0; i < conNic.length(); i++) {
                if (i != 5) {
                    String Char = String.valueOf(conNic.charAt(i));
                    oldNic.append(Char);
                }
            }
            oldNic.append('V');

            boolean flag = ageChecker(oldNic.toString(), birthday);
            if (flag) {
                responseObject.setCode(success_code);
                responseObject.setStatus(success_status);
                responseObject.setDescription("entered_nic: " + nic + ", validated nic: " + oldNic);
                result.put("resObject", responseObject);
                result.put("entered_nic", nic);
                result.put("validatedNic", oldNic.toString());
                return result;
            } else {
                throw new BadRequestAlertException(DOB_NOT_MATCHED, ENTITY_NAME, "DOB");
            }
        } else {
            responseObject.setCode(error_code);
            responseObject.setStatus(error_status);
            responseObject.setDescription("LENGTH DOESN'T MATCH FOR NIC TYPE!");
            instructions.put("new_nic", "HAS 12 DIGITS");
            instructions.put("old_nic", "HAS 9 DIGITS AND 'V'");
            responseObject.setInstructions(instructions.toMap());
            result.put("resObject", responseObject);
            result.put("entered_nic", nic);
            return result;
        }
    }

    // check user age using old and new model nic
    public boolean ageChecker(String nic, LocalDate birthday) {
        Integer year = Integer.valueOf(nic.substring(0, 4));
        int dateCount = Integer.parseInt(nic.substring(4, 7));
        int month = 0;
        int[] daysArray = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        if (dateCount > 500) {
            dateCount = dateCount - 500;
        }
        //Month
        for (int i = 0; i < daysArray.length; i++) {
            if (dateCount > daysArray[i]) {
                dateCount = dateCount - daysArray[i];
            } else {
                month = i + 1;
                break;
            }
        }
        LocalDate dob = null;
        try {
            dob = LocalDate.of(year, month, dateCount);
            if (dob.equals(birthday)) {
                System.out.println("DOB MATCHED");
                return true;
            } else {
                return false;
            }
        } catch (DateTimeException e) {
            e.fillInStackTrace();
            throw new BadRequestAlertException(INVALID_NIC, ENTITY_NAME, "INVALID_NIC");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<GetUserResponseDto>>> getAllUserList(int page, Integer perPage, String sort, String direction, String search) {
        Page<AdMUser> dbData;

        if (direction.equalsIgnoreCase("asc")) {
            dbData = adMUserDao.getAllUsersForTable(search, PageRequest.of(page, perPage, Sort.by(Sort.Direction.ASC, sort)));
        } else {
            dbData = adMUserDao.getAllUsersForTable(search, PageRequest.of(page, perPage, Sort.by(Sort.Direction.DESC, sort)));
        }

        List<GetUserResponseDto> responseList;
        responseList = dbData.stream().map(UserServiceImpl::getGetUserResponseDto).collect(Collectors.toList());

        ApiResponseDto<List<GetUserResponseDto>> response = new ApiResponseDto<>();
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);
        response.setResult(responseList);
        return ResponseEntity.ok(response);
    }

    @Override
    public ApiResponseDto<List<GetUserResponseDto>> getUserList(int page, Integer per_page, String sort, String
            direction, String search, Integer statusId) {

        Page<Object[]> dataList = null;

        if (statusId != null) {
            dataList = adMUserDao.findUsersWithStatusId(statusId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else if (direction.equalsIgnoreCase("asc")) {
            dataList = adMUserDao.findUsersWithStatusForTable(search, (PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort))));
        } else {
            dataList = adMUserDao.findUsersWithStatusForTable(search, (PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort))));
        }

        List<GetUserResponseDto> userResponseDtoList = new ArrayList<>();

        for (Object[] userDetails : dataList) {
            if (userDetails[0] instanceof AdMUser && userDetails[1] instanceof UsRStatusDetail && userDetails[2] instanceof UsMUserRole) {

                AdMUser user = (AdMUser) userDetails[0];
                UsRStatusDetail status = (UsRStatusDetail) userDetails[1];
                UsMUserRole userRole = (UsMUserRole) userDetails[2];

                GetUserResponseDto userResponseDto = new GetUserResponseDto();

                //User Data
                userResponseDto.setUserId(user.getUserId());
                userResponseDto.setUserName(user.getUserName());
                userResponseDto.setIsActive(user.getIsActive());
                userResponseDto.setDepartmentId(user.getUsMDepartment().getDepartmentId());
                userResponseDto.setBranchId(user.getUsMBranch().getBranchId());
                userResponseDto.setUserRoleId(userRole.getUserRoleId());
                userResponseDto.setDescription(user.getDescription());
                userResponseDto.setEmpEmail(user.getEmail());
                userResponseDto.setEmpMobileNumber(user.getMobileNo());
                userResponseDto.setFirstName(user.getFirstName());
                userResponseDto.setLastName(user.getLastName());
                userResponseDto.setStatusId(status.getStatusId());

                userResponseDtoList.add(userResponseDto);
            }
        }

        ApiResponseDto<List<GetUserResponseDto>> response = new ApiResponseDto<>();
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dataList.getTotalElements());
        response.setPagination(paginationDto);
        response.setResult(userResponseDtoList);
        return response;

    }

    @Override
    public ResponseEntity<AdMUserDetailsDto> getUserByUserID(Integer userId) {
        ResponseDto response = new ResponseDto();
        Optional<AdMUser> adMUser = adMUserDao.findById(userId);
        AdMUserDetailsDto adMUserDetailsDto = null;
        if (adMUser.isPresent()) {
            response.setMessage("USER_FOUND");

            AdMUser user = adMUser.get();

            adMUserDetailsDto = new AdMUserDetailsDto();
            adMUserDetailsDto.setUserId(user.getUserId());
            adMUserDetailsDto.setUserName(user.getUserName());
            adMUserDetailsDto.setDescription(user.getDescription());
            adMUserDetailsDto.setIsFirstLogin(user.getIsFirstLogin());
            adMUserDetailsDto.setIsActive(user.getIsActive());
            adMUserDetailsDto.setEmail(user.getEmail());
            adMUserDetailsDto.setMobileNo(user.getMobileNo());
            adMUserDetailsDto.setSecurityKey(user.getPassword());
            adMUserDetailsDto.setUserExpDate(user.getUserExpDate());
            adMUserDetailsDto.setPasswordExpDate(user.getPasswordExpDate());
            adMUserDetailsDto.setUserRoleId(user.getUsMUserRole().getUserRoleId());
            adMUserDetailsDto.setStatusId(user.getUsRStatusDetail().getStatusId());
            adMUserDetailsDto.setBranchId(user.getUsMBranch().getBranchId());
            adMUserDetailsDto.setDepartmentId(user.getUsMDepartment().getDepartmentId());
            adMUserDetailsDto.setDefaultBranch(user.getUsMBranch().getBranchName());

            response.setData(adMUserDetailsDto);
        } else {
            response.setMessage("USER_NOT_FOUND");
            response.setData(null);
        }

        return ResponseEntity.ok(adMUserDetailsDto);
    }


}