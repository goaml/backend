package com.olak.goaml.userMGT.service.transaction;

import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.other.MenuActionCreateTestApiDto;
import com.olak.goaml.userMGT.dto.transaction.UsTMenuActionDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsTMenuActionService {
    ApiResponseDto<List<UsTMenuActionDto>> getAllMenuActions(int page, Integer per_page, String sort, String direction, String search, Integer statusId, Integer moduleId, Integer menuId, Integer actionId);

    ResponseEntity<UsTMenuActionDto> createMenuAction(UsTMenuActionDto usTMenuActionDto);

    ResponseEntity<UsTMenuActionDto> updateMenuAction(UsTMenuActionDto usTMenuAction);

    ResponseEntity<UsTMenuActionDto> getMenuAction(Integer menuActionId);

    ApiResponseDto<List<UsTMenuActionDto>> getAllMenuActionsList(int page, Integer per_page, String sort, String direction, String search, String[] statusId, String[] moduleId, String[] menuId, String[] actionId);

    ResponseEntity<List<UsTMenuActionDto>> createMenuActionTranTestApi(MenuActionCreateTestApiDto menuActionCreateTestApiDto);
}
