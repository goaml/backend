package com.olak.goaml.userMGT.service.transaction.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.criteria.transaction.UsTMenuActionRepo;
import com.olak.goaml.userMGT.dao.master.UsMActionDao;
import com.olak.goaml.userMGT.dao.master.UsMMenuDao;
import com.olak.goaml.userMGT.dao.master.UsMModuleDao;
import com.olak.goaml.userMGT.dao.reference.UsRStatusDetailDao;
import com.olak.goaml.userMGT.dao.transaction.UsTMenuActionDao;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.other.MenuActionCreateTestApiDto;
import com.olak.goaml.userMGT.dto.other.SystemMenuDto;
import com.olak.goaml.userMGT.dto.transaction.UsTMenuActionDto;
import com.olak.goaml.userMGT.mappers.transaction.UsTMenuActionMapper;
import com.olak.goaml.userMGT.models.master.UsMAction;
import com.olak.goaml.userMGT.models.master.UsMMenu;
import com.olak.goaml.userMGT.models.master.UsMModule;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import com.olak.goaml.userMGT.service.transaction.UsTMenuActionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Service
public class UsTMenuActionServiceImpl implements UsTMenuActionService {

    private final UsTMenuActionDao menuActionDao;
    private final UsTMenuActionMapper menuActionMapper;
    private final UsMMenuDao menuDao;
    private final UsMModuleDao moduleDao;
    private final UsMActionDao actionDao;
    private final UsRStatusDetailDao statusDetailDao;
    private final UsTMenuActionRepo menuActionRepo;

    public UsTMenuActionServiceImpl(UsTMenuActionDao menuActionDao, UsTMenuActionMapper menuActionMapper, UsMMenuDao menuDao, UsMModuleDao moduleDao, UsMActionDao actionDao, UsRStatusDetailDao statusDetailDao, UsTMenuActionRepo menuActionRepo) {
        this.menuActionDao = menuActionDao;
        this.menuActionMapper = menuActionMapper;
        this.menuDao = menuDao;
        this.moduleDao = moduleDao;
        this.actionDao = actionDao;
        this.statusDetailDao = statusDetailDao;
        this.menuActionRepo = menuActionRepo;
    }

    @Override
    public ApiResponseDto<List<UsTMenuActionDto>> getAllMenuActions(int page, Integer per_page, String sort, String direction, String search, Integer statusId, Integer moduleId, Integer menuId, Integer actionId) {

        Page<UsTMenuAction> dbData = null;

        if (statusId != null && moduleId == null && menuId == null && actionId == null) {
            dbData = menuActionDao.findByUsRStatusDetailStatusId(statusId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else if (statusId == null && moduleId != null && menuId == null && actionId == null) {
            dbData = menuActionDao.findByUsMModuleModuleId(moduleId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else if (statusId == null && moduleId == null && menuId != null && actionId == null) {
            dbData = menuActionDao.findByUsMMenuMenuId(menuId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else if (statusId == null && moduleId == null && menuId == null && actionId != null) {
            dbData = menuActionDao.findByUsMActionActionId(actionId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else if (direction.equalsIgnoreCase("asc") && statusId == null && moduleId == null && menuId == null && actionId == null) {
            dbData = menuActionDao.getAllMenuActions(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else {
            dbData = menuActionDao.getAllMenuActions(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
        }

        ApiResponseDto<List<UsTMenuActionDto>> response = new ApiResponseDto<>();
        response.setResult(menuActionMapper.listToDto(dbData.getContent()));

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;

    }


    @Override
    public ResponseEntity<UsTMenuActionDto> createMenuAction(UsTMenuActionDto usTMenuActionDto) {
        if (usTMenuActionDto.getMenuId() == null) {
            throw new BadRequestAlertException("MENU_ID NULL", "ERROR", "ERROR");
        } else if (usTMenuActionDto.getActionId() == null) {
            throw new BadRequestAlertException("ACTION_ID NULL", "ERROR", "ERROR");
        } else if (usTMenuActionDto.getModuleId() == null) {
            throw new BadRequestAlertException("MODULE_ID NULL", "ERROR", "ERROR");
        } else if (usTMenuActionDto.getStatusId() == null) {
            throw new BadRequestAlertException("STATUS_ID NULL", "ERROR", "ERROR");
        }
        Optional<UsMMenu> optMenu = menuDao.findById(usTMenuActionDto.getMenuId());
        if (!optMenu.isPresent()) {
            throw new BadRequestAlertException("MENU NOT FOUND", "ERROR", "ERROR");
        }
        Optional<UsMModule> optModule = moduleDao.findById(usTMenuActionDto.getModuleId());
        if (!optModule.isPresent()) {
            throw new BadRequestAlertException("MODULE NOT FOUND", "ERROR", "ERROR");
        }
        Optional<UsMAction> optAction = actionDao.findById(usTMenuActionDto.getActionId());
        if (!optAction.isPresent()) {
            throw new BadRequestAlertException("ACTION NOT FOUND", "ERROR", "ERROR");
        }
        Optional<UsRStatusDetail> optStatus = statusDetailDao.findById(usTMenuActionDto.getStatusId());
        if (!optStatus.isPresent()) {
            throw new BadRequestAlertException("STATUS NOT FOUND", "ERROR", "ERROR");
        }

        UsMMenu menu = optMenu.get();
        UsMModule module = optModule.get();
        UsMAction action = optAction.get();
        UsRStatusDetail status = optStatus.get();

        Optional<UsTMenuAction> optMenuAction = menuActionDao.findUsTMenuActionByUsMActionAndUsMMenuAndUsMModule(action, menu, module);
        if (optMenuAction.isPresent()) {
            throw new BadRequestAlertException("Menu Action Already Exists", ENTITY_NAME, "ERROR");
        }

        UsTMenuAction menuAction = new UsTMenuAction();
        menuAction.setUsMMenu(menu);
        menuAction.setUsMModule(module);
        menuAction.setUsMAction(action);
        menuAction.setUsRStatusDetail(status);
        menuAction.setName(usTMenuActionDto.getName());

        if (usTMenuActionDto.getUrl() == null || usTMenuActionDto.getUrl().isEmpty()) {
            menuAction.setUrl("/" + module.getModuleName().toLowerCase() + "/" + menu.getMenuName().toLowerCase() + "/" + action.getActionName().toLowerCase());
        } else {
            menuAction.setUrl(usTMenuActionDto.getUrl());
        }

        if (usTMenuActionDto.getIsMenu() == null) {
            menuAction.setIsMenu(true);
        } else {
            menuAction.setIsMenu(usTMenuActionDto.getIsMenu());
        }

        UsTMenuAction savedMenuAction = menuActionDao.save(menuAction);
        if (savedMenuAction.getMenuActionId() != null) {
            return ResponseEntity.ok().body(menuActionMapper.toDto(savedMenuAction));
        } else {
            throw new BadRequestAlertException("MENU ACTION SAVE FAILED", "ERROR", "ERROR");
        }
    }


    @Override
    public ResponseEntity<UsTMenuActionDto> updateMenuAction(UsTMenuActionDto usTMenuAction) {

        if (usTMenuAction.getMenuActionId() == null) {
            throw new BadRequestAlertException("MENU ACTION ID NULL", "ERROR", "ERROR");
        }
        Optional<UsTMenuAction> optMenuAction = menuActionDao.findById(usTMenuAction.getMenuActionId());
        if (!optMenuAction.isPresent()) {
            throw new BadRequestAlertException("MENU ACTION NOT FOUND", "ERROR", "ERROR");
        }

        if (usTMenuAction.getIsMenu() == null) {
            throw new BadRequestAlertException("MENU_ID NULL", "ERROR", "ERROR");
        } else if (usTMenuAction.getActionId() == null) {
            throw new BadRequestAlertException("MENU_ACTION_ID NULL", "ERROR", "ERROR");
        } else if (usTMenuAction.getModuleId() == null) {
            throw new BadRequestAlertException("MODULE_ID NULL", "ERROR", "ERROR");
        } else if (usTMenuAction.getStatusId() == null) {
            throw new BadRequestAlertException("STATUS_ID NULL", "ERROR", "ERROR");
        }
        Optional<UsMMenu> optMenu = menuDao.findById(usTMenuAction.getMenuId());
        if (!optMenu.isPresent()) {
            throw new BadRequestAlertException("MENU NOT FOUND", "ERROR", "ERROR");
        }
        Optional<UsMModule> optModule = moduleDao.findById(usTMenuAction.getModuleId());
        if (!optModule.isPresent()) {
            throw new BadRequestAlertException("MODULE NOT FOUND", "ERROR", "ERROR");
        }
        Optional<UsMAction> optAction = actionDao.findById(usTMenuAction.getActionId());
        if (!optAction.isPresent()) {
            throw new BadRequestAlertException("ACTION NOT FOUND", "ERROR", "ERROR");
        }
        Optional<UsRStatusDetail> optStatus = statusDetailDao.findById(usTMenuAction.getStatusId());
        if (!optStatus.isPresent()) {
            throw new BadRequestAlertException("STATUS NOT FOUND", "ERROR", "ERROR");
        }

        UsMMenu menu = optMenu.get();
        UsMModule module = optModule.get();
        UsMAction action = optAction.get();
        UsRStatusDetail status = optStatus.get();

        UsTMenuAction menuAction = optMenuAction.get();
        menuAction.setUsMMenu(menu);
        menuAction.setUsMModule(module);
        menuAction.setUsMAction(action);
        menuAction.setUsRStatusDetail(status);
        menuAction.setName(usTMenuAction.getName());

        if (usTMenuAction.getUrl() == null || usTMenuAction.getUrl().isEmpty()) {
            menuAction.setUrl("/" + module.getModuleName().toLowerCase() + "/" + menu.getMenuName().toLowerCase() + "/" + action.getActionName().toLowerCase());
        } else {
            menuAction.setUrl(usTMenuAction.getUrl());
        }

        if (usTMenuAction.getIsMenu() == null) {
            menuAction.setIsMenu(true);
        } else {
            menuAction.setIsMenu(usTMenuAction.getIsMenu());
        }

        UsTMenuAction updatedMenuAct = menuActionDao.save(menuAction);
        return ResponseEntity.ok().body(menuActionMapper.toDto(updatedMenuAct));
    }


    @Override
    public ResponseEntity<UsTMenuActionDto> getMenuAction(Integer menuActionId) {
        if (menuActionId == null) {
            throw new BadRequestAlertException("MENU ACTION ID NULL", "ERROR", "ERROR");
        }
        Optional<UsTMenuAction> optMenuAction = menuActionDao.findById(menuActionId);
        if (!optMenuAction.isPresent()) {
            throw new BadRequestAlertException("MENU ACTION NOT FOUND", "ERROR", "ERROR");
        } else {
            UsTMenuAction menuAction = optMenuAction.get();
            return ResponseEntity.ok().body(menuActionMapper.toDto(menuAction));
        }
    }


    @Override
    public ApiResponseDto<List<UsTMenuActionDto>> getAllMenuActionsList(int page, Integer per_page, String sort, String direction, String search, String[] statusId, String[] moduleId, String[] menuId, String[] actionId) {
        Page<UsTMenuAction> dbData = menuActionRepo.findAllWithFilters(page, per_page, sort, direction, search, statusId, moduleId, menuId, actionId);
        ApiResponseDto<List<UsTMenuActionDto>> response = new ApiResponseDto<>();
        response.setResult(menuActionMapper.listToDto(dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);
        return response;
    }

    @Override
    public ResponseEntity<List<UsTMenuActionDto>> createMenuActionTranTestApi(MenuActionCreateTestApiDto menuActionCreateTestApiDto) {
        try {

            List<UsTMenuActionDto> menuActionList = new ArrayList<>();
            List<UsTMenuActionDto> responseList = new ArrayList<>();

            if (menuActionCreateTestApiDto.getSystemMenus().isEmpty()) {
                throw new BadRequestAlertException("System Menu is empty", "Error", "Error");
            } else {
                if (menuActionCreateTestApiDto.getModuleId() == null) {
                    throw new BadRequestAlertException("Module Id is empty", "Error", "Error");
                }
                if (menuActionCreateTestApiDto.getStatusId() == null) {
                    throw new BadRequestAlertException("Status Id is empty", "Error", "Error");
                }

                for (SystemMenuDto menuDto : menuActionCreateTestApiDto.getSystemMenus()) {
                    if (menuDto.getMenuId() == null) {
                        throw new BadRequestAlertException("Menu Id is empty", "Error", "Error");
                    } else {
                        Optional<UsMMenu> optMenu = menuDao.findById(menuDto.getMenuId());
                        if (!optMenu.isPresent()) {
                            throw new BadRequestAlertException("Menu not found", "Error", "Error");
                        }
                    }

                    if (menuDto.getActionId() == null) {
                        throw new BadRequestAlertException("Action Id is empty", "Error", "Error");
                    } else {
                        Optional<UsMAction> optAction = actionDao.findById(menuDto.getActionId());
                        if (!optAction.isPresent()) {
                            throw new BadRequestAlertException("Action not found", "Error", "Error");
                        }
                    }
                }


                for (SystemMenuDto menuDto : menuActionCreateTestApiDto.getSystemMenus()) {

                    Optional<UsMMenu> optMenu = menuDao.findById(menuDto.getMenuId());
                    UsMMenu menu = optMenu.get();
                    Optional<UsMAction> optAction = actionDao.findById(menuDto.getActionId());
                    UsMAction action = optAction.get();

                    UsTMenuActionDto menuAction = new UsTMenuActionDto();
                    menuAction.setName(menu.getMenuName());
                    menuAction.setUrl(menu.getMenuUrl());
                    menuAction.setIsMenu(true);
                    menuAction.setMenuId(menuDto.getMenuId());
                    menuAction.setModuleId(menuActionCreateTestApiDto.getModuleId());
                    menuAction.setStatusId(menuActionCreateTestApiDto.getStatusId());
                    menuAction.setActionId(action.getActionId());

                    menuActionList.add(menuAction);
                }

                responseList = menuActionMapper.listToDto(menuActionDao.saveAll(menuActionMapper.listToEntity(menuActionList)));

                for (UsTMenuActionDto menuA : responseList) {
                    if (menuA.getMenuActionId() == null) {
                        throw new BadRequestAlertException("Menu Action save failed", "Error", "Error");
                    }
                }
                return ResponseEntity.ok(responseList);
            }

        } catch (Exception e) {
            throw new BadRequestAlertException(e.getMessage(), "Error", "Error");
        }
    }

}
