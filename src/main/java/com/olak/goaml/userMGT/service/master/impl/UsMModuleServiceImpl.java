package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.userMGT.dao.master.UsMModuleDao;
import com.olak.goaml.userMGT.dto.master.UsMModuleDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.mappers.master.UsMModuleMapper;
import com.olak.goaml.userMGT.models.master.UsMModule;
import com.olak.goaml.userMGT.service.master.UsMModuleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsMModuleServiceImpl implements UsMModuleService {

    private final UsMModuleDao moduleDao;
    private final UsMModuleMapper moduleMapper;

    public UsMModuleServiceImpl(UsMModuleDao moduleDao, UsMModuleMapper moduleMapper) {
        this.moduleDao = moduleDao;
        this.moduleMapper = moduleMapper;
    }

    @Override
    public ApiResponseDto<List<UsMModuleDto>> getAllModules(Integer page, Integer per_page, String search, String sort, String direction) {
        Page<UsMModule> dbData;

        if (direction.equalsIgnoreCase("asc")) {
            dbData = moduleDao.getAllModulesForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else {
            dbData = moduleDao.getAllModulesForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.DESC, sort));
        }

        ApiResponseDto<List<UsMModuleDto>> response = new ApiResponseDto<>();
        response.setResult(moduleMapper.listToDto(dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;

    }
}
