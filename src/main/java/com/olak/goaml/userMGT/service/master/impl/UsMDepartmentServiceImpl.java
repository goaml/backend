package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.constants.HardCodeConstants;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.master.UsMDepartmentDao;
import com.olak.goaml.userMGT.dao.reference.UsRBrachDepartmentDao;
import com.olak.goaml.userMGT.dao.reference.UsRStatusDetailDao;
import com.olak.goaml.userMGT.dto.master.UsMDepartmentDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.other.DepartmentCreateReqDto;
import com.olak.goaml.userMGT.mappers.master.UsMDepartmentMapper;
import com.olak.goaml.userMGT.models.master.UsMDepartment;
import com.olak.goaml.userMGT.models.reference.UsRBrachDepartment;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.service.master.UsMDepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class UsMDepartmentServiceImpl implements UsMDepartmentService {

    private final UsMDepartmentDao departmentDao;
    private final UsMDepartmentMapper departmentMapper;
    private final UsRBrachDepartmentDao branchDepartmentDao;
    private final UsRStatusDetailDao statusDetailDao;

    public UsMDepartmentServiceImpl(UsMDepartmentDao departmentDao, UsMDepartmentMapper departmentMapper, UsRBrachDepartmentDao branchDepartmentDao, UsRStatusDetailDao statusDetailDao) {
        this.departmentDao = departmentDao;
        this.departmentMapper = departmentMapper;
        this.branchDepartmentDao = branchDepartmentDao;
        this.statusDetailDao = statusDetailDao;
    }

    private static UsMDepartment getUsMDepartment(DepartmentCreateReqDto departmentCreateReqDto) {
        UsMDepartment department = new UsMDepartment();
        department.setContactNumber(departmentCreateReqDto.getContactNumber());
        department.setDeptAddress(departmentCreateReqDto.getDeptAddress());
        department.setDeptLocation(departmentCreateReqDto.getDeptLocation());
        department.setDeptName(departmentCreateReqDto.getDeptName());
        department.setDesc(departmentCreateReqDto.getDesc());
        department.setEmail(departmentCreateReqDto.getEmail());
        return department;
    }

    private static UsMDepartment getUsMDepartment(UsMDepartmentDto usMDepartmentDto, UsMDepartment usMDepartment) {
        UsMDepartment department = usMDepartment;
        department.setContactNumber(usMDepartmentDto.getContactNumber());
        department.setDeptAddress(usMDepartmentDto.getDeptAddress());
        department.setDeptLocation(usMDepartmentDto.getDeptLocation());
        department.setDeptName(usMDepartmentDto.getDeptName());
        department.setDesc(usMDepartmentDto.getDesc());
        department.setEmail(usMDepartmentDto.getEmail());
        return department;
    }

    @Override
    public ApiResponseDto<List<UsMDepartmentDto>> getDepartmentList(int page, Integer per_page, String sort, String direction, String search) {
        Page<UsMDepartment> dbData;
        if (direction.equalsIgnoreCase("asc")) {
            dbData = departmentDao.getAllDepartmentsForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else {
            dbData = departmentDao.getAllDepartmentsForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.DESC, sort));
        }

        ApiResponseDto<List<UsMDepartmentDto>> response = new ApiResponseDto<>();
        response.setResult(departmentMapper.listToDto(dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;
    }

    @Override
    public ResponseEntity<List<UsMDepartmentDto>> getDepartmentForDropdown() {
        try {
            List<UsMDepartment> departments = departmentDao.findAll();
            if (departments.isEmpty()) {
                throw new BadRequestAlertException("No department found", "ERROR", "ERROR");
            } else {
                return ResponseEntity.ok().body(departmentMapper.listToDto(departments));
            }
        } catch (Exception e) {
            throw new BadRequestAlertException(e.getMessage(), "ERROR", "ERROR");
        }
    }

    @Override
    public ResponseEntity<List<UsMDepartmentDto>> getDepartmentNotAssignedToBranch(Integer branchId) {
        try {
            List<UsMDepartment> responseList = new ArrayList<>();
            List<UsMDepartment> departmentList = departmentDao.findAll();
            List<UsRBrachDepartment> branchDepartmentList = branchDepartmentDao.findUsRBrachDepartmentByUsMBranchBranchId(branchId);
            if (!branchDepartmentList.isEmpty()) {
                for (UsMDepartment department : departmentList) {
                    boolean isAssigned = false;
                    for (UsRBrachDepartment branchDepartment : branchDepartmentList) {
                        if (department.getDepartmentId().equals(branchDepartment.getUsMDepartment().getDepartmentId())) {
                            isAssigned = true;
                            break;
                        }
                    }
                    if (!isAssigned) {
                        responseList.add(department);
                    }
                }
            } else {
                responseList.addAll(departmentList);
            }
            return ResponseEntity.ok(departmentMapper.listToDto(responseList));
        } catch (Exception e) {
            throw new BadRequestAlertException(e.getMessage(), "ERROR", "ERROR");
        }
    }

    @Override
    public ResponseEntity<UsMDepartmentDto> createDepartment(DepartmentCreateReqDto departmentCreateReqDto) {
        try {
            Optional<UsMDepartment> optDepartment = departmentDao.findByDeptNameOrDesc(departmentCreateReqDto.getDeptName(), departmentCreateReqDto.getDesc());
            if (optDepartment.isPresent()) {
                throw new BadRequestAlertException("Department already exists with given department name or department description", ENTITY_NAME, "ERROR");
            } else {
                UsRStatusDetail statusDetail;
                Optional<UsRStatusDetail> optStatus = statusDetailDao.findById(HardCodeConstants.NEW_STATUS_ID);
                if (!optStatus.isPresent()) {
                    throw new BadRequestAlertException("Status not found for given id", ENTITY_NAME, "ERROR");
                } else {
                    statusDetail = optStatus.get();
                }
                UsMDepartment department = getUsMDepartment(departmentCreateReqDto);
                department.setUsRStatusDetail(statusDetail);
                department = departmentDao.save(department);
                if (department.getDepartmentId() == null) {
                    throw new BadRequestAlertException("Department create request failed.", ENTITY_NAME, "ERROR");
                } else {
                    return ResponseEntity.ok(departmentMapper.toDto(department));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<UsMDepartmentDto> updateDepartment(Integer departmentId, UsMDepartmentDto usMDepartmentDto) {
        try {
            if (departmentId == null) {
                throw new BadRequestAlertException("Department Id null", ENTITY_NAME, "ERROR");
            } else if (!departmentId.equals(usMDepartmentDto.getDepartmentId())) {
                throw new BadRequestAlertException("Department Id mismatch", ENTITY_NAME, "ERROR");
            } else {
                Optional<UsMDepartment> optDepartment = departmentDao.findById(usMDepartmentDto.getDepartmentId());
                if (!optDepartment.isPresent()) {
                    throw new BadRequestAlertException("Department not found", ENTITY_NAME, "ERROR");
                } else {
                    UsRStatusDetail statusDetail;
                    Optional<UsRStatusDetail> optStatus = statusDetailDao.findById(HardCodeConstants.NEW_STATUS_ID);
                    if (!optStatus.isPresent()) {
                        throw new BadRequestAlertException("Status not found for given id", ENTITY_NAME, "ERROR");
                    } else {
                        statusDetail = optStatus.get();
                    }
                    UsMDepartment department = getUsMDepartment(usMDepartmentDto, optDepartment.get());
                    department.setUsRStatusDetail(statusDetail);
                    department = departmentDao.save(department);
                    return ResponseEntity.ok(departmentMapper.toDto(department));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<UsMDepartmentDto> getDepartmentById(Integer departmentId) {
        try {
            if (departmentId == null) {
                throw new BadRequestAlertException("DepartmentId null", ENTITY_NAME, "ERROR");
            } else {
                Optional<UsMDepartment> optDepartment = departmentDao.findById(departmentId);
                if (!optDepartment.isPresent()) {
                    throw new BadRequestAlertException("Department not found for given id", ENTITY_NAME, "ERROR");
                } else {
                    return ResponseEntity.ok(departmentMapper.toDto(optDepartment.get()));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }


}
