package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMMenuDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;

import java.util.List;

public interface UsMMenuService {
    ApiResponseDto<List<UsMMenuDto>> getAllMenu(int page, Integer per_page, String sort, String direction, String search, Integer status);
}
