package com.olak.goaml.userMGT.service.reference.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.constants.HardCodeConstants;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.master.AdMUserDao;
import com.olak.goaml.userMGT.dao.master.UsMBranchDao;
import com.olak.goaml.userMGT.dao.master.UsMDepartmentDao;
import com.olak.goaml.userMGT.dao.reference.UsRBrachDepartmentDao;
import com.olak.goaml.userMGT.dao.reference.UsRStatusDetailDao;
import com.olak.goaml.userMGT.dao.reference.UsRUserBranchDao;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import com.olak.goaml.userMGT.dto.master.UsMDepartmentDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.other.BranchDepartmentCreateReqDto;
import com.olak.goaml.userMGT.dto.other.UserBranchCreateReqDto;
import com.olak.goaml.userMGT.dto.reference.UsRBrachDepartmentDto;
import com.olak.goaml.userMGT.dto.reference.UsRUserBranchDto;
import com.olak.goaml.userMGT.mappers.master.UsMDepartmentMapper;
import com.olak.goaml.userMGT.mappers.reference.UsRBrachDepartmentMapper;
import com.olak.goaml.userMGT.mappers.reference.UsRUserBranchMapper;
import com.olak.goaml.userMGT.models.master.AdMUser;
import com.olak.goaml.userMGT.models.master.UsMBranch;
import com.olak.goaml.userMGT.models.master.UsMDepartment;
import com.olak.goaml.userMGT.models.reference.UsRBrachDepartment;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.reference.UsRUserBranch;
import com.olak.goaml.userMGT.service.reference.UsRBrachDepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class UsRBrachDepartmentServiceImpl implements UsRBrachDepartmentService {

    private final UsRBrachDepartmentDao brachDepartmentDao;
    private final UsMDepartmentDao departmentDao;
    private final UsMDepartmentMapper departmentMapper;
    private final UsRBrachDepartmentMapper branchDepartmentMapper;
    private final UsRUserBranchDao userBranchDao;
    private final UsMBranchDao branchDao;
    private final SearchNFilter searchNFilter;
    private final AdMUserDao userDao;
    private final UsRStatusDetailDao statusDetailDao;
    private final UsRUserBranchMapper userBranchMapper;

    public UsRBrachDepartmentServiceImpl(UsRBrachDepartmentDao brachDepartmentDao, UsMDepartmentDao departmentDao, UsMDepartmentMapper departmentMapper, UsRBrachDepartmentMapper branchDepartmentMapper, UsRUserBranchDao userBranchDao, UsMBranchDao branchDao, SearchNFilter searchNFilter, AdMUserDao userDao, UsRStatusDetailDao statusDetailDao, UsRUserBranchMapper userBranchMapper) {
        this.brachDepartmentDao = brachDepartmentDao;
        this.departmentDao = departmentDao;
        this.departmentMapper = departmentMapper;
        this.branchDepartmentMapper = branchDepartmentMapper;
        this.userBranchDao = userBranchDao;
        this.branchDao = branchDao;
        this.searchNFilter = searchNFilter;
        this.userDao = userDao;
        this.statusDetailDao = statusDetailDao;
        this.userBranchMapper = userBranchMapper;
    }


    /**
     * @param branchId
     * @param page
     * @param per_page
     * @param sort
     * @param direction
     * @return
     */
    @Override
    public ApiResponseDto<List<UsMDepartmentDto>> getDepartmentForBranchId(Integer branchId, int page, Integer per_page, String sort, String direction) {
        Page<UsRBrachDepartment> dbData = null;

        if (branchId != null && direction.equalsIgnoreCase("asc")) {
            dbData = brachDepartmentDao.getDepartmentForBranchId(branchId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else {
            dbData = brachDepartmentDao.getDepartmentForBranchId(branchId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
        }

        List<UsMDepartmentDto> departmentList = new ArrayList<>();

        if (dbData.getTotalElements() == 0) {
            throw new BadRequestAlertException("No Data found", ENTITY_NAME, "Not Found");
        }

        for (UsRBrachDepartment b : dbData) {
            Optional<UsMDepartment> optDept = departmentDao.findById(b.getUsMDepartment().getDepartmentId());

            departmentList.add(departmentMapper.toDto(optDept.get()));
        }
        ApiResponseDto<List<UsMDepartmentDto>> response = new ApiResponseDto<>();
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);
        response.setResult(departmentList);
        return response;
    }


    @Override
    public ResponseEntity<Boolean> getValidationStatus(Integer branchId, Integer departmentId) {

        try {
            if (branchId == null) {
                throw new BadRequestAlertException("branchId null", ENTITY_NAME, "ERROR");
            } else if (departmentId == null) {
                throw new BadRequestAlertException("departmentId null", ENTITY_NAME, "ERROR");
            }

            Optional<UsRBrachDepartment> optBranchDept = brachDepartmentDao.findByUsMBranchBranchIdAndUsMDepartmentDepartmentId(branchId, departmentId);
            if (optBranchDept.isPresent()) {
                return ResponseEntity.ok(true);
            } else {
                return ResponseEntity.ok(false);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<List<UsRBrachDepartmentDto>> getBranchDepartmentForDropdown(Integer userId) {
        try {

            if (userId == null) {
                throw new BadRequestAlertException("userId null", ENTITY_NAME, "Not Found");
            } else {
                List<UsRUserBranch> userBranchList = userBranchDao.findUsRUserBranchByAdMUserUserId(userId);
                log.info("User branch list size : " + userBranchList.size());
                List<UsRBrachDepartment> branchDepartmentList = brachDepartmentDao.findAll();
                log.info("Branch department list size : " + branchDepartmentList.size());
                List<UsRBrachDepartment> responseList = new ArrayList<>();
                if (!userBranchList.isEmpty()) {
                    for (UsRBrachDepartment branchDepartment : branchDepartmentList) {
                        boolean flag = false;
                        for (UsRUserBranch userBranch : userBranchList) {
                            if (userBranch.getUsRBrachDepartment().getBrachDeptId().equals(branchDepartment.getBrachDeptId())) {
                                flag = true;
                                break;
                            }
                        }
                        if (!flag) {
                            responseList.add(branchDepartment);
                        }
                    }
                } else {
                    responseList.addAll(branchDepartmentList);
                }
                log.info("Response list size : " + responseList.size());
                return ResponseEntity.ok(branchDepartmentMapper.entityListToDtoList(responseList));
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "Not Found");
        }
    }

    @Override
    public ResponseEntity<UsRBrachDepartmentDto> createBranchDepartment(BranchDepartmentCreateReqDto branchDepartmentCreateReqDto) {
        try {
            if (branchDepartmentCreateReqDto.getBranchId() == null) {
                throw new BadRequestAlertException("branchId null", ENTITY_NAME, "Not Found");
            } else if (branchDepartmentCreateReqDto.getDepartmentId() == null) {
                throw new BadRequestAlertException("departmentId null", ENTITY_NAME, "Not Found");
            } else {
                Optional<UsMBranch> optBranch = branchDao.findById(branchDepartmentCreateReqDto.getBranchId());
                Optional<UsMDepartment> optDept = departmentDao.findById(branchDepartmentCreateReqDto.getDepartmentId());
                if (!optBranch.isPresent()) {
                    throw new BadRequestAlertException("Branch not found", ENTITY_NAME, "Not Found");
                } else if (!optDept.isPresent()) {
                    throw new BadRequestAlertException("Department not found", ENTITY_NAME, "Not Found");
                } else {
                    List<UsRBrachDepartment> branchDeptList = brachDepartmentDao.findUsRBrachDepartmentByUsMBranchBranchIdAndUsMDepartmentDepartmentId(branchDepartmentCreateReqDto.getBranchId(), branchDepartmentCreateReqDto.getDepartmentId());
                    if (!branchDeptList.isEmpty()) {
                        throw new BadRequestAlertException("Branch department already exists", ENTITY_NAME, "Not Found");
                    } else {
                        UsRBrachDepartment branchDepartment = new UsRBrachDepartment();
                        branchDepartment.setUsMBranch(optBranch.get());
                        branchDepartment.setUsMDepartment(optDept.get());
                        branchDepartment.setIsActive(true);
                        branchDepartment = brachDepartmentDao.save(branchDepartment);
                        if (branchDepartment.getBrachDeptId() == null) {
                            throw new BadRequestAlertException("Branch department save request failed.", ENTITY_NAME, "ERROR");
                        } else {
                            return ResponseEntity.ok(branchDepartmentMapper.toDto(branchDepartment));
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "Not Found");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<UsRBrachDepartmentDto>>> getAllBranchDepartment(Integer page, Integer per_page, String search, String direction, String sort, Integer branchId, Integer departmentId) {
        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        filterList.add(new FilterPassDto("usMBranch", "branchId", branchId));
        filterList.add(new FilterPassDto("usMDepartment", "departmentId", departmentId));

        searchList.add(new SearchPassDto("usMBranch", "branchName"));
        searchList.add(new SearchPassDto("usMDepartment", "deptName"));

        searchNFilter.setPaginationDetails(page, per_page, sort, direction);
        searchNFilter.setVariables(search, ActiveStatus.ACTIVE.getValue());
        searchNFilter.setEntityClass(UsRBrachDepartment.class);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        ApiResponseDto<List<UsRBrachDepartmentDto>> response = new ApiResponseDto<>();
        response.setResult(branchDepartmentMapper.entityListToDtoList((List<UsRBrachDepartment>) dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<UsRBrachDepartmentDto> getBranchDepartmentById(Integer brachDeptId) {
        try {
            if (brachDeptId == null) {
                throw new BadRequestAlertException("brachDeptId null", ENTITY_NAME, "Not Found");
            } else {
                Optional<UsRBrachDepartment> optBranchDept = brachDepartmentDao.findById(brachDeptId);
                if (!optBranchDept.isPresent()) {
                    throw new BadRequestAlertException("Branch department not found", ENTITY_NAME, "Not Found");
                } else {
                    return ResponseEntity.ok(branchDepartmentMapper.toDto(optBranchDept.get()));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "Not Found");
        }
    }

    @Override
    public ResponseEntity<UsRBrachDepartmentDto> updateBranchDepartment(Integer brachDeptId, UsRBrachDepartmentDto usRBrachDepartmentDto) {
        try {
            if (brachDeptId == null) {
                throw new BadRequestAlertException("brachDeptId null", ENTITY_NAME, "Null");
            } else if (!brachDeptId.equals(usRBrachDepartmentDto.getBrachDeptId())) {
                throw new BadRequestAlertException("brachDeptId mismatch", ENTITY_NAME, "Mismatch");
            } else {
                Optional<UsRBrachDepartment> optBranchDept = brachDepartmentDao.findById(brachDeptId);
                if (!optBranchDept.isPresent()) {
                    throw new BadRequestAlertException("Branch department not found", ENTITY_NAME, "Not Found");
                } else {

                    Optional<UsMBranch> optBranch = branchDao.findById(usRBrachDepartmentDto.getBranchId());
                    Optional<UsMDepartment> optDept = departmentDao.findById(usRBrachDepartmentDto.getDepartmentId());

                    if (!optBranch.isPresent()) {
                        throw new BadRequestAlertException("Branch not found", ENTITY_NAME, "Not Found");
                    } else if (!optDept.isPresent()) {
                        throw new BadRequestAlertException("Department not found", ENTITY_NAME, "Not Found");
                    } else {
                        log.info("FK found");
                    }

                    UsRBrachDepartment branchDepartment = optBranchDept.get();
                    branchDepartment.setUsMBranch(optBranch.get());
                    branchDepartment.setUsMDepartment(optDept.get());
                    branchDepartment = brachDepartmentDao.save(branchDepartment);
                    return ResponseEntity.ok(branchDepartmentMapper.toDto(branchDepartment));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "Not Found");
        }
    }

    @Override
    public ResponseEntity<Integer> deleteBranchDepartment(Integer brachDeptId) {
        try {
            if (brachDeptId == null) {
                throw new BadRequestAlertException("brachDeptId null", ENTITY_NAME, "Not Found");
            } else {
                Optional<UsRBrachDepartment> optBranchDept = brachDepartmentDao.findById(brachDeptId);
                if (!optBranchDept.isPresent()) {
                    throw new BadRequestAlertException("Branch department not found", ENTITY_NAME, "Not Found");
                } else {
                    brachDepartmentDao.delete(optBranchDept.get());
                    if (brachDepartmentDao.findById(brachDeptId).isPresent()) {
                        throw new BadRequestAlertException("Branch department delete request failed.", ENTITY_NAME, "ERROR");
                    } else {
                        return ResponseEntity.ok(brachDeptId);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "Not Found");
        }
    }

    @Override
    public ResponseEntity<UsRUserBranchDto> createUserBranch(UserBranchCreateReqDto userBranchCreateReqDto) {
        try {
            if (userBranchCreateReqDto.getUserId() == null) {
                throw new BadRequestAlertException("userId null", ENTITY_NAME, "Not Found");
            } else if (userBranchCreateReqDto.getBrachDeptId() == null) {
                throw new BadRequestAlertException("brachDeptId null", ENTITY_NAME, "Not Found");
            } else {
                Optional<AdMUser> optUser = userDao.findById(userBranchCreateReqDto.getUserId());
                Optional<UsRBrachDepartment> optBranchDept = brachDepartmentDao.findById(userBranchCreateReqDto.getBrachDeptId());
                if (!optUser.isPresent()) {
                    throw new BadRequestAlertException("User not found", ENTITY_NAME, "Not Found");
                } else if (!optBranchDept.isPresent()) {
                    throw new BadRequestAlertException("Branch department not found", ENTITY_NAME, "Not Found");
                } else {

                    Optional<UsRStatusDetail> optStatus = statusDetailDao.findById(HardCodeConstants.NEW_STATUS_ID);
                    if (!optStatus.isPresent()) {
                        throw new BadRequestAlertException("Status not found", ENTITY_NAME, "Not Found");
                    }

                    List<UsRUserBranch> userBranchList = userBranchDao.findUsRUserBranchByAdMUserUserIdAndUsRBrachDepartmentBrachDeptId(userBranchCreateReqDto.getUserId(), userBranchCreateReqDto.getBrachDeptId());
                    if (!userBranchList.isEmpty()) {
                        throw new BadRequestAlertException("User branch already exists", ENTITY_NAME, "Not Found");
                    } else {
                        UsRUserBranch userBranch = new UsRUserBranch();
                        userBranch.setAdMUser(optUser.get());
                        userBranch.setUsRBrachDepartment(optBranchDept.get());
                        userBranch.setUsRStatusDetail(optStatus.get());
                        userBranch.setFrom(userBranchCreateReqDto.getFrom());
                        userBranch.setTo(userBranchCreateReqDto.getTo());
                        userBranch.setIsDefualtDept(0);
                        userBranch.setIsActive(true);
                        userBranch = userBranchDao.save(userBranch);
                        if (userBranch.getEmployeeBranchId() == null) {
                            throw new BadRequestAlertException("User branch save request failed.", ENTITY_NAME, "ERROR");
                        } else {
                            return ResponseEntity.ok(userBranchMapper.toDto(userBranch));
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<UsRUserBranchDto>>> getAllUserBranch(Integer page, Integer per_page, String search, String direction, String sort, Integer userId, Integer brachDeptId, Integer statusId) {
        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        filterList.add(new FilterPassDto("adMUser", "userId", userId));
        filterList.add(new FilterPassDto("usRBrachDepartment", "brachDeptId", brachDeptId));
        filterList.add(new FilterPassDto("usRStatusDetail", "statusId", statusId));

        searchList.add(new SearchPassDto("adMUser", "userName"));
        searchList.add(new SearchPassDto("usRBrachDepartment", "usMBranch", "branchName"));
        searchList.add(new SearchPassDto("usRBrachDepartment", "usMDepartment", "deptName"));

        searchNFilter.setPaginationDetails(page, per_page, sort, direction);
        searchNFilter.setVariables(search, ActiveStatus.ACTIVE.getValue());
        searchNFilter.setEntityClass(UsRUserBranch.class);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        ApiResponseDto<List<UsRUserBranchDto>> response = new ApiResponseDto<>();
        response.setResult(userBranchMapper.entityListToDtoList((List<UsRUserBranch>) dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<UsRUserBranchDto> getUserBranchById(Integer employeeBranchId) {
        try {
            if (employeeBranchId == null) {
                throw new BadRequestAlertException("employeeBranchId null", ENTITY_NAME, "Null");
            } else {
                Optional<UsRUserBranch> optUserBranch = userBranchDao.findById(employeeBranchId);
                if (!optUserBranch.isPresent()) {
                    throw new BadRequestAlertException("User branch not found", ENTITY_NAME, "Not Found");
                } else {
                    return ResponseEntity.ok(userBranchMapper.toDto(optUserBranch.get()));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<UsRUserBranchDto> updateUserBranch(Integer employeeBranchId, UsRUserBranchDto usRUserBranchDto) {
        try {
            if (employeeBranchId == null) {
                throw new BadRequestAlertException("employeeBranchId null", ENTITY_NAME, "Null");
            } else if (!employeeBranchId.equals(usRUserBranchDto.getEmployeeBranchId())) {
                throw new BadRequestAlertException("employeeBranchId mismatch", ENTITY_NAME, "Mismatch");
            } else {
                Optional<UsRUserBranch> optUserBranch = userBranchDao.findById(employeeBranchId);
                if (!optUserBranch.isPresent()) {
                    throw new BadRequestAlertException("User branch not found", ENTITY_NAME, "Not Found");
                } else {

                    Optional<AdMUser> optUser = userDao.findById(usRUserBranchDto.getUserId());
                    Optional<UsRBrachDepartment> optBranchDept = brachDepartmentDao.findById(usRUserBranchDto.getBrachDeptId());
                    Optional<UsRStatusDetail> optStatus = statusDetailDao.findById(usRUserBranchDto.getStatusId());

                    if (!optUser.isPresent()) {
                        throw new BadRequestAlertException("User not found", ENTITY_NAME, "Not Found");
                    } else if (!optBranchDept.isPresent()) {
                        throw new BadRequestAlertException("Branch department not found", ENTITY_NAME, "Not Found");
                    } else if (!optStatus.isPresent()) {
                        throw new BadRequestAlertException("Status not found", ENTITY_NAME, "Not Found");
                    } else {
                        log.info("FK found");
                    }

                    UsRUserBranch userBranch = optUserBranch.get();
                    userBranch.setAdMUser(optUser.get());
                    userBranch.setUsRBrachDepartment(optBranchDept.get());
                    userBranch.setUsRStatusDetail(optStatus.get());
                    userBranch.setFrom(usRUserBranchDto.getFrom());
                    userBranch.setTo(usRUserBranchDto.getTo());
                    userBranch.setIsDefualtDept(usRUserBranchDto.getIsDefualtDept());
                    userBranch = userBranchDao.save(userBranch);
                    return ResponseEntity.ok(userBranchMapper.toDto(userBranch));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity<Integer> deleteUserBranch(Integer employeeBranchId) {
        try {
            if (employeeBranchId == null) {
                throw new BadRequestAlertException("employeeBranchId null", ENTITY_NAME, "Null");
            } else {
                Optional<UsRUserBranch> optUserBranch = userBranchDao.findById(employeeBranchId);
                if (!optUserBranch.isPresent()) {
                    throw new BadRequestAlertException("User branch not found", ENTITY_NAME, "Not Found");
                } else {
                    userBranchDao.delete(optUserBranch.get());
                    if (userBranchDao.findById(employeeBranchId).isPresent()) {
                        throw new BadRequestAlertException("User branch delete request failed.", ENTITY_NAME, "ERROR");
                    } else {
                        return ResponseEntity.ok(employeeBranchId);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }
}
