package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMBranchDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.other.BranchCreateReqDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsMBranchService {
    ApiResponseDto<List<UsMBranchDto>> getAllBranches(Integer page, Integer per_page, String search, String sort, String direction, String branchAddress, String branchLocation, String branchName, String contactNumber, String email, Integer userId, Integer branchId);

    ResponseEntity<UsMBranchDto> getUsMBranchById(Integer branchId);

    ResponseEntity<List<UsMBranchDto>> getBranchesForDropdown();

    ResponseEntity<UsMBranchDto> createBranch(BranchCreateReqDto branchCreateReqDto);

    ResponseEntity<UsMBranchDto> updateBranch(Integer branchId, UsMBranchDto usMBranchDto);
}
