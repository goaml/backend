package com.olak.goaml.userMGT.service.log;

import com.olak.goaml.userMGT.dto.other.NoteLogCreateReqDto;
import org.springframework.http.ResponseEntity;

public interface UsLNoteLogService {

    ResponseEntity getAllNoteLogs(int page, Integer per_page, String sort, String search, String direction, Integer userId, Integer branchId, Integer departmentId, Integer menuId, Integer actionId);

    ResponseEntity createNoteLog(NoteLogCreateReqDto noteLogCreateReqDto);

    ResponseEntity getNoteLogById(Integer noteId);
}
