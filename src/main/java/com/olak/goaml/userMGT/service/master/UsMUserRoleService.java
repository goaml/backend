package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMUserRoleDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.other.BranchListDto;
import com.olak.goaml.userMGT.dto.other.MenuActionListDto;
import com.olak.goaml.userMGT.dto.other.RoleCreationDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsMUserRoleService {
    ApiResponseDto<List<UsMUserRoleDto>> getAllUserRoles(int page, Integer per_page, String sort, String direction);

    UsMUserRoleDto roleCreation(RoleCreationDto userRole);

    MenuActionListDto getAllowedMenuActionsForUserRole(Integer userRoleId, Boolean status);

    MenuActionListDto getMenuActionListForRole(Integer userRoleId);

    UsMUserRoleDto createUserRole(UsMUserRoleDto userRoleDto);

    ResponseEntity<BranchListDto> getBranchForUser(Integer userRoleId);

    ResponseEntity<String> deleteUserRole(Integer userRoleId);

    ResponseEntity<UsMUserRoleDto> updateUserRole(UsMUserRoleDto userRoleDto);

    ResponseEntity<UsMUserRoleDto> getUserRole(Integer userRoleId);

    ResponseEntity<List<UsMUserRoleDto>> getUserRolesForDropdown();
}
