package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.master.AdMUserDao;
import com.olak.goaml.userMGT.dao.master.UsMEmployeeDao;
import com.olak.goaml.userMGT.dao.master.UsMUserDao;
import com.olak.goaml.userMGT.dao.master.UsMUserRoleDao;
import com.olak.goaml.userMGT.dao.reference.UsRStatusDetailDao;
import com.olak.goaml.userMGT.dto.master.UsMEmployeeDto;
import com.olak.goaml.userMGT.dto.master.UsMUserDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.other.GetUserResponseDto;
import com.olak.goaml.userMGT.dto.other.ResponseDto;
import com.olak.goaml.userMGT.dto.user.AdMUserDto;
import com.olak.goaml.userMGT.mappers.master.UsMEmployeeMapper;
import com.olak.goaml.userMGT.mappers.master.UsMUserMapper;
import com.olak.goaml.userMGT.mappers.user.AdMUserMapper;
import com.olak.goaml.userMGT.models.master.UsMEmployee;
import com.olak.goaml.userMGT.models.master.UsMUser;
import com.olak.goaml.userMGT.models.master.UsMUserRole;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.service.master.UsMUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class UsMUserServiceImpl implements UsMUserService {
    private static final String ERROR01 = "EMPLOYEE_NOT_FOUND";
    private static final String ERROR02 = "USER_ROLE_NOT_FOUND";
    private static final String ERROR03 = "EMPLOYEE_ID_NULL";
    private static final String ERROR04 = "USER_ROLE_ID_NULL";
    private static final String ERROR05 = "USER_ALREADY_EXIST";
    private static final String ERROR06 = "USER_NOT_FOUND";
    private static final String ERROR07 = "USER_ID_NULL";
    private static final String ERROR08 = "USERNAME_IS_TAKEN";
    private static final String ERROR09 = "USERNAME_NULL_OR_EMPTY";
    private final AdMUserDao adMUserDao;
    private final UsMUserMapper userMapper;
    private final UsMUserDao userDao;
    private final UsMEmployeeDao employeeDao;
    private final UsMUserRoleDao userRoleDao;
    private final UsMEmployeeMapper employeeMapper;

    private final UsRStatusDetailDao usRStatusDetailDao;
    private final AdMUserMapper adMUserMapper;

    public UsMUserServiceImpl(UsMUserMapper userMapper, UsMUserDao userDao, UsMEmployeeDao employeeDao, UsMUserRoleDao userRoleDao, UsMEmployeeMapper employeeMapper, UsRStatusDetailDao usRStatusDetailDao,
                              AdMUserDao adMUserDao, AdMUserMapper adMUserMapper) {
        this.userMapper = userMapper;
        this.userDao = userDao;
        this.employeeDao = employeeDao;
        this.userRoleDao = userRoleDao;
        this.employeeMapper = employeeMapper;
        this.usRStatusDetailDao = usRStatusDetailDao;
        this.adMUserDao = adMUserDao;
        this.adMUserMapper = adMUserMapper;
    }

    public void validateUser(UsMUserDto usMUserDto) {
        if (usMUserDto.getEmployeeId() == null) {
            throw new BadRequestAlertException(ERROR03, ENTITY_NAME, "ERROR03");
        }
        if (usMUserDto.getUserRoleId() == null) {
            throw new BadRequestAlertException(ERROR04, ENTITY_NAME, "ERROR04");
        }

        Optional<UsMEmployee> optionalMEmployee = employeeDao.findById(usMUserDto.getEmployeeId());
        Optional<UsMUserRole> optionalRole = userRoleDao.findById(usMUserDto.getUserRoleId());

        if (!optionalMEmployee.isPresent()) {
            throw new BadRequestAlertException(ERROR01, ENTITY_NAME, "ERROR01");
        }
        if (!optionalRole.isPresent()) {
            throw new BadRequestAlertException(ERROR02, ENTITY_NAME, "ERROR02");
        }
    }

    @Override
    public ResponseDto createUser(UsMUserDto usMUserDto) {

        validateUser(usMUserDto);

        if (usMUserDto.getUsername() == null || usMUserDto.getUsername() == "") {
            throw new BadRequestAlertException(ERROR09, ENTITY_NAME, "ERROR09");
        }

        Optional<UsMUser> optionalUser = userDao.findUsMUserByUsMEmployeeEmployeeId(usMUserDto.getEmployeeId());
        if (optionalUser.isPresent()) {
            throw new BadRequestAlertException(ERROR05, ENTITY_NAME, "ERROR05");
        }

        Optional<UsMUser> optionalUserWithUsername = userDao.findUsMUserByUsername(usMUserDto.getUsername());
        if (optionalUserWithUsername.isPresent()) {
            throw new BadRequestAlertException(ERROR08, ENTITY_NAME, "ERROR08");
        }

        ResponseDto response = new ResponseDto();

        usMUserDto.setStatusId(1);

        String lowUsername = usMUserDto.getUsername().trim().toLowerCase();
        usMUserDto.setUsername(lowUsername);

        usMUserDto.setPassword("dummypassword");

        UsMUser usMUser = userDao.save(userMapper.toEntity(usMUserDto));
        if (usMUser.getUserId() != null) {
            System.out.println("USER_CREATED_SUCCESSFULLY");
            response.setMessage("USER_CREATED_SUCCESSFULLY");
            response.setData(userMapper.toDto(usMUser));
        } else {
            response.setMessage("USER_CREATION_FAILED");
            response.setData(null);
        }

        return response;
    }

    @Override
    public ResponseDto updateUser(UsMUserDto usMUserDto) {

        if (usMUserDto.getUserId() == null) {
            throw new BadRequestAlertException(ERROR07, ENTITY_NAME, "ERROR06");
        }

        validateUser(usMUserDto);
        ResponseDto response = new ResponseDto();

        Optional<UsMUser> optionalUser = userDao.findById(usMUserDto.getUserId());
        if (optionalUser.isPresent()) {
            UsMUserDto userDto = userMapper.toDto(userDao.save(userMapper.updateUsMUserFromUsMUserDto(usMUserDto, optionalUser.get())));
            System.out.println("USER_UPDATE_SUCCESSFULLY");
            response.setMessage("USER_UPDATE_SUCCESSFULLY");
            response.setData(userDto);
        } else {
            throw new BadRequestAlertException(ERROR06, ENTITY_NAME, "ERROR06");
        }
        return response;
    }

    @Override
    public ApiResponseDto<List<GetUserResponseDto>> getUsers(int page, Integer per_page, String sort, String direction, String search, Integer statusId) {

        Page<Object[]> dataList = null;

        if (statusId != null) {
            dataList = userDao.findUsersWithStatusId(statusId, PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else if (direction.equalsIgnoreCase("asc")) {
            dataList = userDao.findUsersWithStatusForTable(search, (PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort))));
        } else {
            dataList = userDao.findUsersWithStatusForTable(search, (PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort))));
        }

        List<GetUserResponseDto> userResponseDtoList = new ArrayList<>();

        for (Object[] userDetails : dataList) {
            if (userDetails[0] instanceof UsMUser && userDetails[1] instanceof UsRStatusDetail && userDetails[2] instanceof UsMUserRole && userDetails[3] instanceof UsMEmployee) {

                UsMUser user = (UsMUser) userDetails[0];
                UsRStatusDetail status = (UsRStatusDetail) userDetails[1];
                UsMUserRole userRole = (UsMUserRole) userDetails[2];
                UsMEmployee employee = (UsMEmployee) userDetails[3];

                GetUserResponseDto userResponseDto = new GetUserResponseDto();

                userResponseDto.setDescription(employee.getDesc());
                userResponseDto.setEmpEmail(employee.getEmpEmail());
                userResponseDto.setEmpMobileNumber(employee.getEmpMobileNumber());
                userResponseDto.setFirstName(employee.getFirstName());
                userResponseDto.setLastName(employee.getLastName());
                userResponseDto.setStatusId(status.getStatusId());

                userResponseDtoList.add(userResponseDto);
            }
        }

        ApiResponseDto<List<GetUserResponseDto>> response = new ApiResponseDto<>();
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal(userResponseDtoList.size());
        response.setPagination(paginationDto);
        response.setResult(userResponseDtoList);
        return response;

    }

    @Override
    public GetUserResponseDto getUserById(Integer userId) {

        GetUserResponseDto userResponseDto = new GetUserResponseDto();

        Optional<UsMUser> userOptional = userDao.findById(userId);
        if (userOptional.isPresent()) {
            Optional<UsMEmployee> employeeOptional = employeeDao.findById(userOptional.get().getUsMEmployee().getEmployeeId());
            if (employeeOptional.isPresent()) {

                UsMUserDto user = userMapper.toDto(userOptional.get());
                UsMEmployeeDto employee = employeeMapper.toDto(employeeOptional.get());

                userResponseDto.setUserId(user.getUserId());
                userResponseDto.setUserRoleId(user.getUserRoleId());
                userResponseDto.setDescription(employee.getDesc());
                userResponseDto.setEmpEmail(employee.getEmpEmail());
                userResponseDto.setEmpMobileNumber(employee.getEmpMobileNumber());
                userResponseDto.setFirstName(employee.getFirstName());
                userResponseDto.setLastName(employee.getLastName());
                userResponseDto.setStatusId(user.getStatusId());
                userResponseDto.setUserName(user.getUsername());

                return userResponseDto;
            } else {
                throw new BadRequestAlertException(ERROR01, ENTITY_NAME, "ERROR01");
            }
        } else {
            throw new BadRequestAlertException(ERROR06, ENTITY_NAME, "ERROR06");
        }
    }

    @Override
    public ResponseEntity<List<AdMUserDto>> getUsersForDropdown() {
        try {
            List<AdMUserDto> userList = adMUserMapper.listToDto(adMUserDao.findAll());

            if (userList.isEmpty()) {
                log.info("User list empty");
                throw new BadRequestAlertException("User list empty", ENTITY_NAME, "ERROR");
            } else {
                return ResponseEntity.ok(userList);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

}