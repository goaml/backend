package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMScreenAttributesDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;

import java.util.List;

public interface UsMScreenAttributesService {

    UsMScreenAttributesDto createAttribute(UsMScreenAttributesDto usMScreenAttributesDto);

    ApiResponseDto<List<UsMScreenAttributesDto>> getAllAttributes(Integer page, Integer per_page, String search, String sort, String direction);

}
