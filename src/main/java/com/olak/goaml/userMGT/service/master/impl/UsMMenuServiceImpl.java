package com.olak.goaml.userMGT.service.master.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.userMGT.dao.master.UsMMenuDao;
import com.olak.goaml.userMGT.dto.master.UsMMenuDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.mappers.master.UsMMenuMapper;
import com.olak.goaml.userMGT.models.master.UsMMenu;
import com.olak.goaml.userMGT.service.master.UsMMenuService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsMMenuServiceImpl implements UsMMenuService {

    private final UsMMenuDao menuDao;
    private final UsMMenuMapper menuMapper;

    public UsMMenuServiceImpl(UsMMenuDao menuDao, UsMMenuMapper menuMapper) {
        this.menuDao = menuDao;
        this.menuMapper = menuMapper;
    }

    @Override
    public ApiResponseDto<List<UsMMenuDto>> getAllMenu(int page, Integer per_page, String sort, String direction, String search, Integer status) {

        Page<UsMMenu> dbData;

        if (status != null) {
            dbData = menuDao.findByStatusId(status, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else if (direction.equalsIgnoreCase("asc")) {
            dbData = menuDao.getAllMenu(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.by(Sort.Direction.ASC, sort)));
        } else {
            dbData = menuDao.getAllMenu(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.by(Sort.Direction.DESC, sort)));
        }

        ApiResponseDto<List<UsMMenuDto>> response = new ApiResponseDto<>();
        response.setResult(menuMapper.listToDto(dbData.getContent()));

        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);

        return response;

    }
}
