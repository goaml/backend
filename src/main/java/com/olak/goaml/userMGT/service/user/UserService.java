package com.olak.goaml.userMGT.service.user;

import com.olak.goaml.userMGT.dto.authenticate.LoginDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.other.*;
import com.olak.goaml.userMGT.dto.user.*;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.List;

/**
 * The interface User service.
 */
public interface UserService {
    ResponseEntity<ResponseDto> createUser(String userType, UserRequestDto userRequestDto);

    ResponseEntity<ResponseDto> updateUser(AdMUserDto adMUserDto, String userType);

    ResponseEntity<AdMUserDto> getOneUser(Integer userId);

    ResponseEntity<AdMUserDto> findUserByUserName(String userName);

    ResponseEntity<String> inactivateUser(Integer userId);

    ResponseEntity<ApiResponseDto<List<AdMUserDto>>> getAllUsers(Integer page, Integer per_page, String sort, String direction, String search);

    ResponseEntity<List<AdMUserDto>> getAllUsersForDropdown();

    ResponseEntity<AuthenticationResponse> login(LoginDto loginDto);

    ResponseEntity<TokenValidateDto> validateToken(ValidateTokenRequestDto validateTokenRequestDto);

    ResponseEntity<ResponseDto> authorizedUser(Integer userId, String status);

    CheckUserAvailableResponseDto checkUserAvailable(CheckUserAvailableReqDto user);

    UserProfileDto getUserProfiile(Integer userId);

    UserProfileDto getByUserName(String username);

    ResponseEntity<AdMUserDto> extendingPasswordExpDate(ExtendPasswordDto extendPasswordDto);

    ApiResponseDto<List<GetUserResponseDto>> getUsersList(int page, Integer per_page, String sort, String direction, String search, Integer userRoleId, Integer statusId, Integer userId, Integer branchId, Integer monthFilterFlag, LocalDate fromDate, LocalDate toDate, Boolean isActive, String isAuthorizationList);

    ResponseEntity<ApiResponseDto<List<AdMUserDetailsDto>>> getAdMUserDetails(Integer page, Integer per_page, String search, String direction, String sort, String employeeId, String userRoleId, String statusId, String branchId, String departmentId);

    ResponseEntity<UserDataDto> getUserBranchDept(Integer userId, Integer branchId);

    ResponseEntity<UserDetailsDto> userDetails(String token);

    ResponseEntity<AdMUserDto> getAdMUserByUserId(Integer userId);

    ResponseEntity<AdMUserDto> updateAdMUser(Integer userId, AdMUserDto adMUserDto);

    ResponseEntity<AdMUserDto> deleteAdMUser(Integer userId);

    ApiResponseDto<List<AdMUserDetailsDto>> getUserListWithoutLoginUser(Integer page, Integer perPage, String search, String direction, String sort);

    ResponseEntity<String> passwordReset(PasswordResetDto passwordResetDto, String userName);

    ResponseEntity<String> forgotPassword(ForgotPasswordDto forgotPasswordDto);

    ResponseEntity<AdMUserDto> restrictAdMUser(Integer userId);

    ResponseEntity<ResponseDto> createAndApproveUser(Integer userTypeId, CreateApproveUserDto createApproveUserDto);

    ResponseEntity<NicResponseDto> validateNicWithDob(NicValidationDto nicValidationDto);

    ResponseEntity<List<AdMUserDto>> getUsersForDropdown();

    ResponseEntity<ApiResponseDto<List<GetUserResponseDto>>> getAllUserList(int page, Integer perPage, String sort, String direction, String search);

    ApiResponseDto<List<GetUserResponseDto>> getUserList(int page, Integer perPage, String sort, String direction, String search, Integer statusId);

    ResponseEntity<AdMUserDetailsDto> getUserByUserID(Integer userId);
}
