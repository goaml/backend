package com.olak.goaml.userMGT.service.other;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Slf4j
@Service
public class MailService {

    public static void sendEMail(String to, String subject, String body) {

        try {
            Properties properties = System.getProperties();

//            String email = "villankoon@lk.onsystechnologies.com";
//            String Password = "&9}s0)@;].y]";
            String email = "support@olak.org";
            String Password = "support@789";

//            properties.put("mail.smtp.auth", "true");
//            properties.put("mail.smtp.starttls.enable", "true");
//            properties.put("mail.smtp.host", "lk.onsystechnologies.com");
//            properties.put("mail.smtp.port", "587");
//            properties.put("mail.smtp.ssl.trust", "lk.onsystechnologies.com");

            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", "mail.olak.org");
            properties.put("mail.smtp.port", "587");
            properties.put("mail.smtp.ssl.trust", "mail.olak.org");

            Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(email, Password);
                }
            });

            Message message = new MimeMessage(session);
            message.addFrom(new InternetAddress[]{new InternetAddress("support@olak.org")});
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setContent(body, "text/html");
            Transport.send(message);
            log.info("Email sent successfully");
        } catch (Exception e) {
            e.fillInStackTrace();
            log.error("Email sending failed");
            log.error("{}", e);
        }
    }

}