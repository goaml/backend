package com.olak.goaml.userMGT.service.log.impl;

import com.olak.goaml.audit.AuditUser;
import com.olak.goaml.audit.AuditUtils;
import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.log.UsLNoteLogDao;
import com.olak.goaml.userMGT.dao.master.*;
import com.olak.goaml.userMGT.dao.reference.UsRUserBranchDao;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import com.olak.goaml.userMGT.dto.log.UsLNoteLogDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.dto.other.NoteLogCreateReqDto;
import com.olak.goaml.userMGT.mappers.log.UsLNoteLogMapper;
import com.olak.goaml.userMGT.models.log.UsLNoteLog;
import com.olak.goaml.userMGT.models.master.*;
import com.olak.goaml.userMGT.models.reference.UsRUserBranch;
import com.olak.goaml.userMGT.service.log.UsLNoteLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Slf4j
@Service
public class UsLNoteLogServiceImpl implements UsLNoteLogService {
    private final UsMActionDao usMActionDao;
    private final UsMMenuDao usMMenuDao;
    private final AdMUserDao adMUserDao;

    private final UsLNoteLogDao usLNoteLogDao;
    private final UsLNoteLogMapper usLNoteLogMapper;
    private final SearchNFilter searchNFilter;
    private final AuditUtils auditUtils;
    private final UsRUserBranchDao userBranchDao;
    private final UsMBranchDao branchDao;
    private final UsMDepartmentDao departmentDao;
    private final GlobalDetailsDao globalDetailsDao;

    public UsLNoteLogServiceImpl(UsLNoteLogDao usLNoteLogDao, UsLNoteLogMapper usLNoteLogMapper, SearchNFilter searchNFilter, AuditUtils auditUtils,
                                 AdMUserDao adMUserDao, UsRUserBranchDao userBranchDao, UsMBranchDao branchDao, UsMDepartmentDao departmentDao, GlobalDetailsDao globalDetailsDao,
                                 UsMMenuDao usMMenuDao,
                                 UsMActionDao usMActionDao) {
        this.usLNoteLogDao = usLNoteLogDao;
        this.usLNoteLogMapper = usLNoteLogMapper;
        this.searchNFilter = searchNFilter;
        this.auditUtils = auditUtils;
        this.adMUserDao = adMUserDao;
        this.userBranchDao = userBranchDao;
        this.branchDao = branchDao;
        this.departmentDao = departmentDao;
        this.globalDetailsDao = globalDetailsDao;
        this.usMMenuDao = usMMenuDao;
        this.usMActionDao = usMActionDao;
    }

    @Override
    public ResponseEntity getAllNoteLogs(int page, Integer per_page, String sort, String search, String direction, Integer userId, Integer branchId, Integer departmentId, Integer menuId, Integer actionId) {
        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        filterList.add(new FilterPassDto("adMUser", "userId", userId));
        filterList.add(new FilterPassDto("usMBranch", "branchId", branchId));
        filterList.add(new FilterPassDto("usMDepartment", "departmentId", departmentId));

        searchNFilter.setPaginationDetails(page, per_page, sort, direction);
        searchNFilter.setVariables(search, ActiveStatus.ACTIVE.getValue());
        searchNFilter.setEntityClass(UsLNoteLog.class);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();

        ApiResponseDto<List<UsLNoteLogDto>> response = new ApiResponseDto<>();
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);
        response.setResult(usLNoteLogMapper.listToDto((List<UsLNoteLog>) dbData.getContent()));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity createNoteLog(NoteLogCreateReqDto noteLogCreateReqDto) {
        try {
            AuditUser auditUser = auditUtils.getAuditUser();

            Integer branchDeptId;
            Integer branchId;
            Integer departmentId;
            AdMUser user;
            UsRUserBranch userBranch;
            UsMBranch branch;
            UsMDepartment department;
            GlobalDetails globalDetails;
            UsMMenu menu;
            UsMAction action;
            String companyId = "";
            Optional<AdMUser> adMUser = adMUserDao.findByUserName(auditUser.getUserName());
            if (!adMUser.isPresent()) {
                throw new BadRequestAlertException("User not found", ENTITY_NAME, "ERROR");
            } else {
                user = adMUser.get();
                List<UsRUserBranch> userBranchList = userBranchDao.findUsRUserBranchByAdMUserUserId(user.getUserId());
                if (userBranchList.size() > 0) {
                    branchDeptId = userBranchList.get(0).getUsRBrachDepartment().getBrachDeptId();
                    Optional<UsRUserBranch> optUserBranch = userBranchDao.findById(branchDeptId);
                    if (!optUserBranch.isPresent()) {
                        throw new BadRequestAlertException("User branch not found", ENTITY_NAME, "ERROR");
                    } else {
                        userBranch = optUserBranch.get();
                        branchId = userBranch.getUsRBrachDepartment().getUsMBranch().getBranchId();
                        departmentId = userBranch.getUsRBrachDepartment().getUsMDepartment().getDepartmentId();

                        Optional<UsMBranch> optBranch = branchDao.findById(branchId);
                        Optional<UsMDepartment> optDepartment = departmentDao.findById(departmentId);
                        if (!optBranch.isPresent() || !optDepartment.isPresent()) {
                            throw new BadRequestAlertException("User or department not found", ENTITY_NAME, "ERROR");
                        } else {
                            branch = optBranch.get();
                            department = optDepartment.get();
                        }
                    }
                } else {
                    throw new BadRequestAlertException("User branch not found", ENTITY_NAME, "ERROR");
                }
            }

            Optional<GlobalDetails> optGlobal = globalDetailsDao.findById(1);
            if (!optGlobal.isPresent()) {
                throw new BadRequestAlertException("Global details not found", ENTITY_NAME, "ERROR");
            } else {
                globalDetails = optGlobal.get();
                companyId = globalDetails.getCompanyName();
            }

            Optional<UsMMenu> optMenu = usMMenuDao.findById(noteLogCreateReqDto.getMenuId());
            Optional<UsMAction> optAction = usMActionDao.findById(noteLogCreateReqDto.getActionId());
            if (!optMenu.isPresent()) {
                throw new BadRequestAlertException("Menu not found", ENTITY_NAME, "ERROR");
            } else {
                menu = optMenu.get();
            }
            if (!optAction.isPresent()) {
                throw new BadRequestAlertException("Action not found", ENTITY_NAME, "ERROR");
            } else {
                action = optAction.get();
            }
            log.info("Company ID : {}", globalDetails.getCompanyName());
            UsLNoteLog noteLog = new UsLNoteLog();
            noteLog.setMenuName(menu.getMenuName());
            noteLog.setActionName(action.getActionName());
            noteLog.setUsername(user.getUserName());
            noteLog.setSystemDate(LocalDate.now());
            noteLog.setCurrentDate(LocalDate.now());
            noteLog.setTime(LocalTime.now());
            noteLog.setCompanyId(companyId);
            noteLog.setBranchName(branch.getBranchName());
            noteLog.setDepartmentName(department.getDeptName());
            noteLog.setKeyValueId(noteLogCreateReqDto.getKeyValueId());
            noteLog.setKeyValue(noteLogCreateReqDto.getKeyValue());
            noteLog.setComment(noteLogCreateReqDto.getComment());
            noteLog.setAdMUser(user);
            noteLog.setUsMMenu(menu);
            noteLog.setUsMAction(action);
            noteLog.setUsMBranch(branch);
            noteLog.setUsMDepartment(department);
            noteLog.setIsActive(true);

            noteLog = usLNoteLogDao.save(noteLog);
            if (noteLog == null) {
                throw new BadRequestAlertException("Note log not saved", ENTITY_NAME, "ERROR");
            } else {
                return ResponseEntity.ok(usLNoteLogMapper.toDto(noteLog));
            }

        } catch (Exception e) {
            log.info(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

    @Override
    public ResponseEntity getNoteLogById(Integer noteId) {
        try {
            if (noteId == null) {
                throw new BadRequestAlertException("Note log id is required", ENTITY_NAME, "ERROR");
            } else {
                Optional<UsLNoteLog> optNoteLog = usLNoteLogDao.findById(noteId);
                if (!optNoteLog.isPresent()) {
                    throw new BadRequestAlertException("Note log not found", ENTITY_NAME, "ERROR");
                } else {
                    return ResponseEntity.ok(usLNoteLogMapper.toDto(optNoteLog.get()));
                }
            }
        } catch (Exception e) {
            log.info(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ERROR");
        }
    }

}
