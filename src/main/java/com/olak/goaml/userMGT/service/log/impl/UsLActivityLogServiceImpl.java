package com.olak.goaml.userMGT.service.log.impl;

import com.olak.goaml.audit.AuditUtils;
import com.olak.goaml.builder.CustomBuilder;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.log.UsLActivityLogDao;
import com.olak.goaml.userMGT.dao.master.*;
import com.olak.goaml.userMGT.dao.template.SearchNFilter;
import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import com.olak.goaml.userMGT.dto.log.UsLActivityLogDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.mappers.log.UsLActivityLogMapper;
import com.olak.goaml.userMGT.models.log.UsLActivityLog;
import com.olak.goaml.userMGT.models.master.GlobalDetails;
import com.olak.goaml.userMGT.models.master.UsMBranch;
import com.olak.goaml.userMGT.service.log.UsLActivityLogService;
import com.olak.goaml.userMGT.service.other.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Service
@Slf4j
@Validated
public class UsLActivityLogServiceImpl implements UsLActivityLogService {
    private final UsMDepartmentDao usMDepartmentDao;
    private final UsMActionDao usMActionDao;
    private final UsMMenuDao usMMenuDao;
    private final GlobalDetailsDao globalDetailsDao;

    private final UsLActivityLogMapper activityLogMapper;
    private final AuditUtils auditUtils;
    private final UsMBranchDao branchDao;
    private final UsLActivityLogDao activityLogDao;
    private final SearchNFilter searchNFilter;

    public UsLActivityLogServiceImpl(UsLActivityLogMapper activityLogMapper, AuditUtils auditUtils, UsMBranchDao branchDao, UsLActivityLogDao activityLogDao, SearchNFilter searchNFilter,
                                     UsMMenuDao usMMenuDao,
                                     UsMActionDao usMActionDao,
                                     UsMDepartmentDao usMDepartmentDao, GlobalDetailsDao globalDetailsDao) {
        this.activityLogMapper = activityLogMapper;
        this.auditUtils = auditUtils;
        this.branchDao = branchDao;
        this.activityLogDao = activityLogDao;
        this.searchNFilter = searchNFilter;
        this.usMMenuDao = usMMenuDao;
        this.usMActionDao = usMActionDao;
        this.usMDepartmentDao = usMDepartmentDao;
        this.globalDetailsDao = globalDetailsDao;
    }

    @Override
    public ResponseEntity<UsLActivityLogDto> createActivityLog(UsLActivityLogDto activityLogDto) {
        log.info("Inside UsLActivityLogService: createActivityLog method");

        if (activityLogDto.getMenuId() == null) {
            throw CustomBuilder.buildBusinessRuleException("VALIDATION_ERROR", "Menu id is required.");
        }
//        if (activityLogDto.getMenuName() == null) {
//            throw new BadRequestAlertException("Menu name is required.", ENTITY_NAME, "error");
//        }
        if (activityLogDto.getActionId() == null) {
            throw CustomBuilder.buildBusinessRuleException("VALIDATION_ERROR", "Action id is required.");
        }
        if (activityLogDto.getBranchId() == null) {
            throw CustomBuilder.buildBusinessRuleException("VALIDATION_ERROR", "Logged User Branch id is required.");
        }

        // find global detail
        List<GlobalDetails> globalDetails = globalDetailsDao.findAll();

        if (globalDetails.isEmpty()) {
            throw CustomBuilder.buildBusinessRuleException("NOT_FOUND_ERROR", "Global details not found.");
        }

        // username
        activityLogDto.setUserName(auditUtils.getAuditUser().getUserName());
        activityLogDto.setIsActive(true);
        // date timestamps
        activityLogDto.setSystemDate(LocalDate.now());
        activityLogDto.setCurrentDate(globalDetails.get(0).getCurrentWorkingDate());
        activityLogDto.setServerTime(CommonUtils.covertToColomboTimeZone(LocalTime.now()));
        // department details
        if (activityLogDto.getDeptId() == null) {
            activityLogDto.setDeptId(0);
            activityLogDto.setDeptName("Default");
        }
        // branch name
        Optional<UsMBranch> brnchOp = branchDao.findById(activityLogDto.getBranchId());
        if (brnchOp.isPresent()) {
            activityLogDto.setBranchName(brnchOp.get().getBranchName());
        } else {
            throw new BadRequestAlertException("Invalid branch id: " + activityLogDto.getBranchId(), ENTITY_NAME, "error");
        }

        UsLActivityLog logEntity = activityLogMapper.toEntity(activityLogDto);
        activityLogDto = activityLogMapper.toDto(activityLogDao.save(logEntity));

        return new ResponseEntity<>(activityLogDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<ApiResponseDto<List<UsLActivityLogDto>>> getAllActivityLogs(
            Integer page,
            Integer perPage,
            String sort,
            String search,
            String direction,
            Integer actionId,
            String username,
            Integer branchId
    ) {
        log.info("Inside UsLActivityLogService: getAllActivityLogs method");

        List<SearchPassDto> searchList = new ArrayList<>();
        List<FilterPassDto> filterList = new ArrayList<>();

        searchList.add(new SearchPassDto("keyValue"));
        searchList.add(new SearchPassDto("userName"));
        searchList.add(new SearchPassDto("description"));

        filterList.add(new FilterPassDto("actionId", actionId));
        filterList.add(new FilterPassDto("userName", username));
        filterList.add(new FilterPassDto("branchId", branchId));

        searchNFilter.setVariables(search, true);
        searchNFilter.setEntityClass(UsLActivityLog.class);
        searchNFilter.setPaginationDetails(page, perPage, sort, direction);
        searchNFilter.setSearchList(searchList);
        searchNFilter.setFilterList(filterList);

        Page<?> dbData = searchNFilter.getFiltered();
        List<UsLActivityLog> content = (List<UsLActivityLog>) dbData.getContent();

        content.forEach(contentItem -> {
            System.out.println("menuId: " + contentItem.getMenuId());
            // menu name
            usMMenuDao.findById(contentItem.getMenuId()).ifPresent(d -> contentItem.setMenuName(d.getMenuName()));
            // action name
            usMActionDao.findById(contentItem.getActionId()).ifPresent(d -> contentItem.setActionName(d.getActionName()));
            // department name
            usMDepartmentDao.findById(contentItem.getDeptId()).ifPresent(d -> contentItem.setDeptName(d.getDeptName()));

        });

        PaginationDto pgdto = new PaginationDto();
        pgdto.setTotal((int) dbData.getTotalElements());

        ApiResponseDto<List<UsLActivityLogDto>> resp = new ApiResponseDto<>();
        resp.setPagination(pgdto);
        resp.setResult(activityLogMapper.listToDto(content));

        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UsLActivityLogDto> updateActivityLog(Integer activityId, UsLActivityLogDto activityLogDto) {
        log.info("Inside UsLActivityLogService: updateActivityLog method");

        Optional<UsLActivityLog> activityLogOp = activityLogDao.findById(activityId);
        if (!activityLogOp.isPresent()) {
            throw new BadRequestAlertException("ActivityId: " + activityLogOp + " related data not found!", ENTITY_NAME, "error");
        }
        if (activityLogDto.getActivityId() == null) {
            throw new BadRequestAlertException("Null activityId!", ENTITY_NAME, "error");
        }
        if (!activityId.equals(activityLogDto.getActivityId())) {
            throw new BadRequestAlertException("ActivityId mismatch", ENTITY_NAME, "error");
        }

        // username
        activityLogDto.setUserName(auditUtils.getAuditUser().getUserName());
        activityLogDto.setIsActive(true);
//        activityLogDto.setCurrentDate(LocalDate.now());

        // department details
        if (activityLogDto.getDeptId() == null) {
            activityLogDto.setDeptId(0);
            activityLogDto.setDeptName("Default");
        }
        // branch name
        Optional<UsMBranch> brnchOp = branchDao.findById(activityLogDto.getBranchId());
        if (brnchOp.isPresent()) {
            activityLogDto.setBranchName(brnchOp.get().getBranchName());
        } else {
            throw new BadRequestAlertException("Invalid branch id: " + activityLogDto.getBranchId(), ENTITY_NAME, "error");
        }

        UsLActivityLog logEntity = activityLogMapper.toEntity(activityLogDto);
        activityLogDto = activityLogMapper.toDto(activityLogDao.save(logEntity));

        return new ResponseEntity<>(activityLogDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UsLActivityLogDto> activityLogGetById(Integer activityId) {
        log.info("Inside UsLActivityLogService: activityLogGetById method");

        Optional<UsLActivityLog> activityLogOp = activityLogDao.findById(activityId);
        if (!activityLogOp.isPresent()) {
            throw new BadRequestAlertException("ActivityId: " + activityLogOp + " related data not found!", ENTITY_NAME, "error");
        }

        UsLActivityLogDto activityLogDto = activityLogMapper.toDto(activityLogOp.get());

        // menu name
        usMMenuDao.findById(activityLogDto.getMenuId()).ifPresent(d -> activityLogDto.setMenuName(d.getMenuName()));
        // action name
        usMActionDao.findById(activityLogDto.getActionId()).ifPresent(d -> activityLogDto.setActionName(d.getActionName()));
        // department name
        usMDepartmentDao.findById(activityLogDto.getDeptId()).ifPresent(d -> activityLogDto.setDeptName(d.getDeptName()));


        return new ResponseEntity<>(activityLogDto, HttpStatus.OK);
    }
}
