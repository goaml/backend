package com.olak.goaml.userMGT.service.master.impl;


import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.userMGT.dao.master.GlobalDetailsDao;
import com.olak.goaml.userMGT.dto.master.GlobalDetailsDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.miscellaneous.PaginationDto;
import com.olak.goaml.userMGT.mappers.master.GlobalDetailsMapper;
import com.olak.goaml.userMGT.models.master.GlobalDetails;
import com.olak.goaml.userMGT.service.master.GlobalDetailsService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GlobalDetailsServiceImpl implements GlobalDetailsService {

    private final GlobalDetailsMapper globalDetailsMapper;
    private final GlobalDetailsDao globalDetailsDao;

    public GlobalDetailsServiceImpl(GlobalDetailsMapper globalDetailsMapper, GlobalDetailsDao globalDetailsDao) {
        this.globalDetailsMapper = globalDetailsMapper;
        this.globalDetailsDao = globalDetailsDao;
    }

    @Override
    public GlobalDetailsDto saveGlobalDetails(GlobalDetailsDto globalDetailsDto) {
        return globalDetailsMapper.toDto(
                globalDetailsDao.save(globalDetailsMapper.toEntity(globalDetailsDto))
        );
    }

    @Override
    public GlobalDetailsDto getById(Integer id) {

        Optional<GlobalDetails> globalDetails = globalDetailsDao.findById(id);
        if (globalDetails.isPresent()) {
            GlobalDetailsDto globalDetailsDto = globalDetailsMapper.toDto(globalDetails.get());
            if (globalDetailsDto.getUserNotificationMode().equalsIgnoreCase("E")) {
                globalDetailsDto.setUserNotificationMode("Email");
            } else if (globalDetailsDto.getUserNotificationMode().equalsIgnoreCase("S")) {
                globalDetailsDto.setUserNotificationMode("SMS");
            } else if (globalDetailsDto.getUserNotificationMode().equalsIgnoreCase("B")) {
                globalDetailsDto.setUserNotificationMode("Both");
            } else {
                globalDetailsDto.setUserNotificationMode("None");
            }
            return globalDetailsDto;
        }
        return null;
    }

    @Override
    public GlobalDetailsDto updateGlobalDetails(Integer id, GlobalDetailsDto globalDetailsDto) {
        Optional<GlobalDetails> globalDetails = globalDetailsDao.findById(id);
        if (globalDetails.isPresent()) {
            return globalDetailsMapper.toDto(globalDetailsDao.save(
                    globalDetailsMapper.updateGlobalDetailsFromGlobalDetailsDto(globalDetailsDto, globalDetails.get())
            ));
        }
        return null;
    }

    @Override
    public ApiResponseDto<List<GlobalDetailsDto>> getAllGlobalDetals(Integer page, Integer per_page, String sort, String search, String direction) {
        Page<GlobalDetails> dbData;

        if (direction.equalsIgnoreCase("asc")) {
            dbData = globalDetailsDao.getAllDetailsForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.ASC, sort));
        } else {
            dbData = globalDetailsDao.getAllDetailsForTable(search, ActiveStatus.ACTIVE.getValue(), PageRequest.of(page, per_page, Sort.Direction.DESC, sort));
        }

        ApiResponseDto<List<GlobalDetailsDto>> response = new ApiResponseDto<>();
        response.setResult(globalDetailsMapper.toDtoList(dbData.getContent()));
        PaginationDto paginationDto = new PaginationDto();
        paginationDto.setTotal((int) dbData.getTotalElements());
        response.setPagination(paginationDto);
        return response;
    }


}
