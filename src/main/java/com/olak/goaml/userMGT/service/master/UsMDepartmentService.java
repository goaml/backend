package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMDepartmentDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.other.DepartmentCreateReqDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UsMDepartmentService {
    ApiResponseDto<List<UsMDepartmentDto>> getDepartmentList(int page, Integer per_page, String sort, String direction, String search);

    ResponseEntity<List<UsMDepartmentDto>> getDepartmentForDropdown();

    ResponseEntity<List<UsMDepartmentDto>> getDepartmentNotAssignedToBranch(Integer branchId);

    ResponseEntity<UsMDepartmentDto> createDepartment(DepartmentCreateReqDto departmentCreateReqDto);

    ResponseEntity<UsMDepartmentDto> updateDepartment(Integer departmentId, UsMDepartmentDto usMDepartmentDto);

    ResponseEntity<UsMDepartmentDto> getDepartmentById(Integer departmentId);
}
