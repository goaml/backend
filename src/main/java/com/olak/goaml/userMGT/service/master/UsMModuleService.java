package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMModuleDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;

import java.util.List;

public interface UsMModuleService {
    ApiResponseDto<List<UsMModuleDto>> getAllModules(Integer page, Integer per_page, String search, String sort, String direction);
}
