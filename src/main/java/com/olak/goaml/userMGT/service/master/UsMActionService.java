package com.olak.goaml.userMGT.service.master;

import com.olak.goaml.userMGT.dto.master.UsMActionDto;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;

import java.util.List;

public interface UsMActionService {
    ApiResponseDto<List<UsMActionDto>> getAllAction(Integer page, Integer per_page, String search, String sort, String direction);
}
