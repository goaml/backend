package com.olak.goaml.userMGT.service.reference.impl;

import com.olak.goaml.constants.ActiveStatus;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dao.reference.UsRLoginTypeDao;
import com.olak.goaml.userMGT.dto.reference.UsRLoginTypeDto;
import com.olak.goaml.userMGT.mappers.reference.UsRLoginTypeMapper;
import com.olak.goaml.userMGT.models.reference.UsRLoginType;
import com.olak.goaml.userMGT.service.reference.UsRLoginTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UsRLoginTypeServiceImpl implements UsRLoginTypeService {
    private final UsRLoginTypeMapper loginTypeMapper;
    private final UsRLoginTypeDao loginTypeDao;

    public UsRLoginTypeServiceImpl(UsRLoginTypeMapper loginTypeMapper, UsRLoginTypeDao loginTypeDao) {
        this.loginTypeMapper = loginTypeMapper;
        this.loginTypeDao = loginTypeDao;
    }

    @Override
    public ResponseEntity<List<UsRLoginTypeDto>> getLoginTypesForDropdown() {
        try {
            List<UsRLoginType> loginTypeList = loginTypeDao.findAllByIsActive(ActiveStatus.ACTIVE.getValue());
            if (loginTypeList.isEmpty()) {
                throw new BadRequestAlertException("No Login Type Found", "Login Type", "login_type_not_found");
            } else {
                return ResponseEntity.ok(loginTypeMapper.toDtoList(loginTypeList));
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), "Login Type", "login_type_error");
        }
    }
}
