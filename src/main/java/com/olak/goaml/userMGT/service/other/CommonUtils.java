package com.olak.goaml.userMGT.service.other;

import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Component
public class CommonUtils {

    public static LocalTime covertToColomboTimeZone(LocalTime time) {
        // Define the Colombo time zone
        ZoneId colomboZone = ZoneId.of("Asia/Colombo");

        // Convert local time to Colombo time
        ZonedDateTime colomboTime = ZonedDateTime.of(
                        ZonedDateTime.now().toLocalDate(), time, ZoneId.systemDefault())
                .withZoneSameInstant(colomboZone);

        return colomboTime.toLocalTime();
    }

}