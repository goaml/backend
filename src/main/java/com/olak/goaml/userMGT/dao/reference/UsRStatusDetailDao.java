package com.olak.goaml.userMGT.dao.reference;

import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsRStatusDetailDao extends JpaRepository<UsRStatusDetail, Integer> {

    @Query("SELECT s FROM UsRStatusDetail s WHERE cast(s.statusId string) LIKE %?1%")
    Page<UsRStatusDetail> getAllStatusForTable(String search, Boolean value, PageRequest of);
}