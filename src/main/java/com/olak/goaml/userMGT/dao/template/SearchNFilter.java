package com.olak.goaml.userMGT.dao.template;

import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SearchNFilter {

    /**
     * @param page
     * @param per_page
     * @param sort
     * @param direction
     */
    void setPaginationDetails(int page, Integer per_page, String sort, String direction);

    /**
     * @param searchVariableList
     * @Doc Set which columns | fields to be search
     */
    void setSearchList(List<SearchPassDto> searchVariableList);

    /**
     * @param filterVariableList
     * @Doc Set which columns | fields to be filtered
     */
    void setFilterList(List<FilterPassDto> filterVariableList);

    /**
     * @param search
     * @param activeStatus
     * @Doc Set parsed search value from frontend
     */
    void setVariables(String search, Boolean activeStatus);

    /**
     * @param entityClass
     * @Doc Set entity class to be filtered or search
     */
    void setEntityClass(Class<?> entityClass);

    /**
     * @return
     * @Doc get all filtered and searched rows
     */
    Page<?> getFiltered();
}
