package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.GlobalDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GlobalDetailsDao extends JpaRepository<GlobalDetails, Integer> {

    @Query("SELECT GD FROM GlobalDetails GD WHERE " +
            "GD.companyName LIKE %?1% OR " +
            "cast(GD.globalID string) LIKE %?1%")
    Page<GlobalDetails> getAllDetailsForTable(String search, Boolean value, PageRequest of);

}
