package com.olak.goaml.userMGT.dao.reference;

import com.olak.goaml.userMGT.models.reference.UsRLoginType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UsRLoginTypeDao extends JpaRepository<UsRLoginType, Integer> {

    List<UsRLoginType> findAllByIsActive(Boolean isActive);

    Optional<UsRLoginType> findByLoginTypeIdAndIsActive(Integer loginTypeId, Boolean isActive);

}
