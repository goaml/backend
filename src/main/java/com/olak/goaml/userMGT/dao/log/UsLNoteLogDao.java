package com.olak.goaml.userMGT.dao.log;

import com.olak.goaml.userMGT.models.log.UsLNoteLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsLNoteLogDao extends JpaRepository<UsLNoteLog, Integer> {
}
