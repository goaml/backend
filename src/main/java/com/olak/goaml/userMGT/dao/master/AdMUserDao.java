package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.AdMUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AdMUserDao extends JpaRepository<AdMUser, Integer>, PagingAndSortingRepository<AdMUser, Integer> {

    Optional<AdMUser> findByUserName(String username);

    Optional<AdMUser> findByUserIdAndIsActive(Integer userId, Boolean isActive);

    Optional<AdMUser> findByUserNameAndIsActive(String userName, Boolean isActive);

    //    @Query("SELECT u, s, r, e FROM AdMUser u INNER JOIN UsRStatusDetail s ON u.usRStatusDetail.statusId = s.statusId INNER JOIN UsMUserRole r ON u.usMUserRole.userRoleId = r.userRoleId INNER JOIN UsMEmployee e ON u.usMEmployee.employeeId = e.employeeId")
    @Query("SELECT u, s, r FROM AdMUser u INNER JOIN UsRStatusDetail s ON u.usRStatusDetail.statusId = s.statusId INNER JOIN UsMUserRole r ON u.usMUserRole.userRoleId = r.userRoleId")
    Page<Object[]> findUsersWithStatusForTable(String search, PageRequest of);

    @Query("SELECT u, s, r FROM AdMUser u INNER JOIN UsRStatusDetail s ON u.usRStatusDetail.statusId = s.statusId INNER JOIN UsMUserRole r ON u.usMUserRole.userRoleId = r.userRoleId WHERE s.statusId=?1")
    Page<Object[]> findUsersWithStatusId(Integer statusId, PageRequest of);

    Optional<AdMUser> findAdMUserByUserNameAndEmailAndMobileNo(String username, String email, String mobileNo);

    @Query("SELECT U FROM AdMUser U WHERE cast(U.userId string) LIKE %?1% OR U.userName LIKE %?1%")
    Page<AdMUser> getAllUsersForTable(String search, PageRequest of);

    @Query("SELECT U FROM AdMUser U WHERE lower(U.userName) LIKE %?1% OR lower(U.firstName) LIKE %?1% OR lower(U.lastName) LIKE %?1% OR lower(U.email) LIKE %?1% OR lower(U.mobileNo) LIKE %?1% AND U.isActive=true")
    Page<AdMUser> findAllUsersForTable(String search, PageRequest of);

    List<AdMUser> findAllByIsActive(Boolean isActive);

    List<AdMUser> findByEmail(String email);

    List<AdMUser> findByNic(String nic);

    @Query("SELECT U FROM AdMUser U WHERE U.usRStatusDetail.statusId IN ?1")
    Page<AdMUser> findAllAuthorizedUsersForTable(List<Integer> statusIds, Pageable pageable);

    @Query("SELECT U FROM AdMUser U WHERE U.comId=?2 AND U.usRStatusDetail.statusId IN ?1")
    Page<AdMUser> findAuthorizedUsersForCompany(List<Integer> statusIdList, Integer companyId, PageRequest of);

    List<AdMUser> findByMobileNo(String mobileNo);

}
