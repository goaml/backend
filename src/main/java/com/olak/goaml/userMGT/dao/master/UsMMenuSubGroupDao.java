package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMMenuSubGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsMMenuSubGroupDao extends JpaRepository<UsMMenuSubGroup, Integer> {

    List<UsMMenuSubGroup> findAllByUsMMenuGroupMenuGroupIdOrderBySeqNo(Integer menuGroupId);
}
