package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMEmployee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UsMEmployeeDao extends JpaRepository<UsMEmployee, Integer> {

    /**
     * SELECT e FROM sici.us_m_employee e WHERE e.employee_id not in
     * (select u.employee_id from sici.us_m_user u)
     */

    @Query("SELECT e FROM UsMEmployee e WHERE e.employeeId NOT IN (SELECT u.usMEmployee.employeeId FROM UsMUser u)")
    Page<UsMEmployee> findEmployeeWithoutUserId(Boolean value, PageRequest of);

    @Query("SELECT e FROM UsMEmployee e WHERE cast(e.employeeId string) LIKE %?1%")
    Page<UsMEmployee> findAllEmployeesForTable(String search, Boolean value, PageRequest of);

    Optional<UsMEmployee> findByEmpNumber(String empNumber);

    @Query("SELECT e FROM UsMEmployee e WHERE e.firstName LIKE %?1% OR e.empNic LIKE %?1% OR e.empNumber LIKE %?1%")
    Page<UsMEmployee> findBySearchParam(String search, PageRequest of);
}