package com.olak.goaml.userMGT.dao.transaction;

import com.olak.goaml.userMGT.models.transaction.UsTActivityLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsTActivityLogDao extends JpaRepository<UsTActivityLog, Integer> {
}