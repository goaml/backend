package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMUserRestriction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UsMUserRestrictionDao extends JpaRepository<UsMUserRestriction, Integer> {

    @Query("SELECT user,userbranch,userbranchdept,menuaction,restriction,module,action,menu " +
            "FROM AdMUser user,UsRUserBranch userbranch, UsRBrachDepartment userbranchdept, UsTMenuAction menuaction,UsMUserRestriction restriction, UsMModule module, UsMAction action, UsMMenu menu " +
            "WHERE " +
            "user.userId=?1 AND " +
            "userbranchdept.usMBranch.branchId=?2 AND " +
            "user.userId=userbranch.adMUser.userId AND " +
            "userbranch.usRBrachDepartment.brachDeptId=userbranchdept.brachDeptId AND " +
            "userbranch.employeeBranchId=restriction.usRUserBranch.employeeBranchId AND " +
            "restriction.usTMenuAction.menuActionId=menuaction.menuActionId AND " +
            "menuaction.usMAction.actionId = action.actionId AND " +
            "menuaction.usMModule.moduleId = module.moduleId AND " +
            "menuaction.usMMenu.menuId = menu.menuId")
    List<Object[]> findRestrictionListForTable(Integer userId, Integer branchId);

}