package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMScreenViewMgt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsMScreenViewMgtDao extends JpaRepository<UsMScreenViewMgt, Integer> {
}
