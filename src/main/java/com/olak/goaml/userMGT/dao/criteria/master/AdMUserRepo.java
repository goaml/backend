package com.olak.goaml.userMGT.dao.criteria.master;

import com.olak.goaml.userMGT.models.master.AdMUser;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class AdMUserRepo {

    private final EntityManager entityManager;
    private final CriteriaBuilder criteriaBuilder;

    public AdMUserRepo(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }


    private Predicate getPredicate(
            String search,
            String[] designationId,
            String employeeId,
            String[] userRoleId,
            String[] statusId,
            String userId,
            String[] branchId,
            LocalDate fromDate,
            LocalDate toDate,
            Root<AdMUser> userRoot,
            Boolean isActive
    ) {

        List<Predicate> searchList = new ArrayList<>();

        searchList.add(
                criteriaBuilder.like(criteriaBuilder.lower(userRoot.get("userName")), "%" + search.toLowerCase() + "%")
        );

        // filterList
        List<Predicate> filterList = new ArrayList<>();

        if (Objects.nonNull(fromDate) && Objects.nonNull(toDate)) {
            filterList.add(
                    criteriaBuilder.between(userRoot.get("createdOn"), fromDate, toDate)
            );
        }


        List<Predicate> userRoleSet = new ArrayList<>();
        for (String userRId : userRoleId) {
            userRoleSet.add(
                    criteriaBuilder.equal(userRoot.get("usMUserRole").get("userRoleId").as(String.class), userRId)
            );
        }

        List<Predicate> statusSet = new ArrayList<>();
        for (String statId : statusId) {
            statusSet.add(
                    criteriaBuilder.equal(userRoot.get("usRStatusDetail").get("statusId").as(String.class), statId)
            );
        }

        List<Predicate> branchSet = new ArrayList<>();
        for (String brId : branchId) {
            branchSet.add(
                    criteriaBuilder.equal(userRoot.get("usMBranch").get("branchId").as(String.class), brId)
            );
        }

        if (!userId.isEmpty()) {
            filterList.add(
                    criteriaBuilder.equal(userRoot.get("userId").as(String.class), userId)
            );
        }

        Predicate searchPredicates = criteriaBuilder.or(searchList.toArray(new Predicate[0]));
        Predicate filterPredicates = criteriaBuilder.and(filterList.toArray(new Predicate[0]));


        //designation set
//        Predicate designationPredicates = criteriaBuilder.or(designationSet.toArray(new Predicate[0]));
        //userRole set
        Predicate userRolePredicates = criteriaBuilder.or(userRoleSet.toArray(new Predicate[0]));
        // status set
        Predicate statusPredicates = criteriaBuilder.or(statusSet.toArray(new Predicate[0]));
        Predicate branchPredicates = criteriaBuilder.or(branchSet.toArray(new Predicate[0]));

        // last predicate with all
        Predicate all = criteriaBuilder.and(searchPredicates, filterPredicates, userRolePredicates, statusPredicates, branchPredicates);

        if (designationId.length == 0 && userRoleId.length == 0 && statusId.length == 0 && branchId.length == 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates);
        }
        if (userRoleId.length == 0 && statusId.length == 0 && branchId.length == 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates);
        }
        if (designationId.length == 0 && userRoleId.length != 0 && statusId.length == 0 && branchId.length == 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, userRolePredicates);
        }
        if (designationId.length == 0 && userRoleId.length == 0 && statusId.length != 0 && branchId.length == 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, statusPredicates);
        }
        if (userRoleId.length != 0 && statusId.length == 0 && branchId.length == 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, userRolePredicates);
        }
        if (userRoleId.length == 0 && statusId.length != 0 && branchId.length == 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, statusPredicates);
        }
        if (designationId.length == 0 && userRoleId.length != 0 && statusId.length != 0 && branchId.length == 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, userRolePredicates, statusPredicates);
        }
        if (designationId.length != 0 && userRoleId.length != 0 && statusId.length != 0 && branchId.length == 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, userRolePredicates, statusPredicates);
        }
        if (designationId.length == 0 && userRoleId.length == 0 && statusId.length == 0 && branchId.length != 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, branchPredicates);
        }
        if (designationId.length != 0 && userRoleId.length == 0 && statusId.length == 0 && branchId.length != 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, branchPredicates);
        }
        if (designationId.length == 0 && userRoleId.length != 0 && statusId.length == 0 && branchId.length != 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, branchPredicates, userRolePredicates);
        }
        if (designationId.length == 0 && userRoleId.length == 0 && statusId.length != 0 && branchId.length != 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, branchPredicates, statusPredicates);
        }
        if (designationId.length != 0 && userRoleId.length != 0 && statusId.length == 0 && branchId.length != 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, branchPredicates, statusPredicates);
        }
        if (designationId.length == 0 && userRoleId.length != 0 && statusId.length != 0 && branchId.length != 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, branchPredicates, statusPredicates);
        }
        if (designationId.length != 0 && userRoleId.length == 0 && statusId.length != 0 && branchId.length != 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, branchPredicates, statusPredicates);
        }
        if (designationId.length != 0 && userRoleId.length != 0 && statusId.length != 0 && branchId.length != 0) {
            all = criteriaBuilder.and(searchPredicates, filterPredicates, branchPredicates, statusPredicates);
        }

        return all;

    }

    private void setOrder(CriteriaQuery<AdMUser> criteriaQuery, String sort, String direction, Root<AdMUser> userRoot) {
        if (direction.equalsIgnoreCase("asc")) {
            criteriaQuery.orderBy(criteriaBuilder.asc(userRoot.get(sort)));
        } else {
            criteriaQuery.orderBy(criteriaBuilder.desc(userRoot.get(sort)));
        }
    }

    private Pageable getPageable(int page, Integer per_page, String sort, String direction) {
        Sort sort1 = Sort.by(direction, sort);
        return PageRequest.of(page, per_page, sort1);
    }

    private long getCount(Predicate predicate) {
        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        Root<AdMUser> countRoot = countQuery.from(AdMUser.class);
        countQuery.select(criteriaBuilder.count(countRoot)).where(predicate);
        return entityManager.createQuery(countQuery).getSingleResult();
    }

    public Page<AdMUser> findAllWithFilters(
            int page,
            Integer per_page,
            String sort,
            String direction,
            String search,
            String[] designationId,
            String employeeId,
            String[] userRoleId,
            String[] statusId,
            String userId,
            String[] branchId,
            LocalDate fromDate,
            LocalDate toDate,
            Boolean isActive
    ) {
        CriteriaQuery<AdMUser> criteriaQuery = criteriaBuilder.createQuery(AdMUser.class);
        Root<AdMUser> userRoot = criteriaQuery.from(AdMUser.class);
        Predicate predicate = getPredicate(search, designationId, employeeId, userRoleId, statusId, userId, branchId, fromDate, toDate, userRoot, isActive);
        criteriaQuery.where(predicate);
        setOrder(criteriaQuery, sort, direction, userRoot);

        TypedQuery<AdMUser> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setFirstResult(page * per_page);
        typedQuery.setMaxResults(per_page);

        Pageable pageable = getPageable(page, per_page, sort, direction);

        long count = getCount(predicate);

        return new PageImpl<>(typedQuery.getResultList(), pageable, count);
    }

}
