package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsMAddressDao extends JpaRepository<UsMAddress, Integer> {
}