package com.olak.goaml.userMGT.dao.transaction;

import com.olak.goaml.userMGT.models.transaction.UsTUserNavigation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsTUserNavigationDao extends JpaRepository<UsTUserNavigation, Integer> {
}