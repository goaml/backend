package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMScreenAttributes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsMScreenAttributesDao extends JpaRepository<UsMScreenAttributes, Integer> {

    @Query("SELECT SR FROM UsMScreenAttributes SR WHERE cast(SR.screenAttributeId string) LIKE %?1%")
    Page<UsMScreenAttributes> getAllAttributesForTable(String search, Boolean value, PageRequest of);


}
