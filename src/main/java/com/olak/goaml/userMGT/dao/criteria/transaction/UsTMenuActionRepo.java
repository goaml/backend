package com.olak.goaml.userMGT.dao.criteria.transaction;

import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UsTMenuActionRepo {

    private final EntityManager entityManager;
    private final CriteriaBuilder criteriaBuilder;

    public UsTMenuActionRepo(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    private Predicate getPredicate(
            String search,
            String[] moduleId,
            String[] menuId,
            String[] actionId,
            String[] statusId,
            Root<UsTMenuAction> menuRoot
    ) {
        List<Predicate> searchList = new ArrayList<>();

        searchList.add(
                criteriaBuilder.like(criteriaBuilder.lower(menuRoot.get("menuActionId").as(String.class)), "%" + search.toLowerCase() + "%")
        );
        searchList.add(
                criteriaBuilder.like(criteriaBuilder.lower(menuRoot.get("name")), "%" + search.toLowerCase() + "%")
        );

        List<Predicate> moduleSet = new ArrayList<>();
        for (String modId : moduleId) {
            moduleSet.add(
                    criteriaBuilder.equal(menuRoot.get("usMModule").get("moduleId").as(String.class), modId)
            );
        }

        List<Predicate> menuSet = new ArrayList<>();
        for (String menId : menuId) {
            menuSet.add(
                    criteriaBuilder.equal(menuRoot.get("usMMenu").get("menuId").as(String.class), menId)
            );
        }

        List<Predicate> actSet = new ArrayList<>();
        for (String actId : actionId) {
            actSet.add(
                    criteriaBuilder.equal(menuRoot.get("usMAction").get("actionId").as(String.class), actId)
            );
        }

        List<Predicate> stSet = new ArrayList<>();
        for (String stId : statusId) {
            stSet.add(
                    criteriaBuilder.equal(menuRoot.get("usRStatusDetail").get("statusId").as(String.class), stId)
            );
        }
        Predicate searchPredicates = criteriaBuilder.or(searchList.toArray(new Predicate[0]));
        Predicate modulePredicates = criteriaBuilder.or(moduleSet.toArray(new Predicate[0]));
        Predicate menuPredicates = criteriaBuilder.or(menuSet.toArray(new Predicate[0]));
        Predicate actionPredicates = criteriaBuilder.or(actSet.toArray(new Predicate[0]));
        Predicate statusPredicates = criteriaBuilder.or(stSet.toArray(new Predicate[0]));

        Predicate all = criteriaBuilder.and(searchPredicates, modulePredicates, menuPredicates, actionPredicates, statusPredicates);

        if (moduleId.length == 0 && menuId.length == 0 && actionId.length == 0 && statusId.length == 0) {
            all = criteriaBuilder.and(searchPredicates);
        }
        if (moduleId.length != 0 && menuId.length == 0 && actionId.length == 0 && statusId.length == 0) {
            all = criteriaBuilder.and(modulePredicates);
        }
        if (moduleId.length == 0 && menuId.length != 0 && actionId.length == 0 && statusId.length == 0) {
            all = criteriaBuilder.and(menuPredicates);
        }
        if (moduleId.length == 0 && menuId.length == 0 && actionId.length != 0 && statusId.length == 0) {
            all = criteriaBuilder.and(actionPredicates);
        }
        if (moduleId.length == 0 && menuId.length == 0 && actionId.length == 0 && statusId.length != 0) {
            all = criteriaBuilder.and(statusPredicates);
        }
        if (moduleId.length != 0 && menuId.length != 0 && actionId.length == 0 && statusId.length == 0) {
            all = criteriaBuilder.and(modulePredicates, menuPredicates);
        }
        if (moduleId.length != 0 && menuId.length == 0 && actionId.length != 0 && statusId.length == 0) {
            all = criteriaBuilder.and(modulePredicates, actionPredicates);
        }
        if (moduleId.length != 0 && menuId.length == 0 && actionId.length == 0 && statusId.length != 0) {
            all = criteriaBuilder.and(modulePredicates, statusPredicates);
        }
        if (moduleId.length == 0 && menuId.length != 0 && actionId.length != 0 && statusId.length == 0) {
            all = criteriaBuilder.and(menuPredicates, actionPredicates);
        }
        if (moduleId.length == 0 && menuId.length != 0 && actionId.length == 0 && statusId.length != 0) {
            all = criteriaBuilder.and(menuPredicates, statusPredicates);
        }
        if (moduleId.length == 0 && menuId.length == 0 && actionId.length != 0 && statusId.length != 0) {
            all = criteriaBuilder.and(actionPredicates, statusPredicates);
        }
        if (moduleId.length != 0 && menuId.length != 0 && actionId.length != 0 && statusId.length == 0) {
            all = criteriaBuilder.and(modulePredicates, menuPredicates, actionPredicates);
        }
        if (moduleId.length != 0 && menuId.length != 0 && actionId.length == 0 && statusId.length != 0) {
            all = criteriaBuilder.and(modulePredicates, menuPredicates, statusPredicates);
        }
        if (moduleId.length != 0 && menuId.length == 0 && actionId.length != 0 && statusId.length != 0) {
            all = criteriaBuilder.and(modulePredicates, actionPredicates, statusPredicates);
        }
        if (moduleId.length == 0 && menuId.length != 0 && actionId.length != 0 && statusId.length != 0) {
            all = criteriaBuilder.and(menuPredicates, actionPredicates, statusPredicates);
        }
        if (moduleId.length != 0 && menuId.length != 0 && actionId.length != 0 && statusId.length != 0) {
            all = criteriaBuilder.and(modulePredicates, menuPredicates, actionPredicates, statusPredicates);
        }

        return all;

    }


    private void setOrder(CriteriaQuery<UsTMenuAction> criteriaQuery, String sort, String direction, Root<UsTMenuAction> menuRoot) {
        if (direction.equalsIgnoreCase("asc")) {
            criteriaQuery.orderBy(criteriaBuilder.asc(menuRoot.get(sort)));
        } else {
            criteriaQuery.orderBy(criteriaBuilder.desc(menuRoot.get(sort)));
        }
    }

    private Pageable getPageable(int page, Integer per_page, String sort, String direction) {
        Sort sort1 = Sort.by(direction, sort);
        return PageRequest.of(page, per_page, sort1);
    }

    private long getCount(Predicate predicate) {
        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        Root<UsTMenuAction> countRoot = countQuery.from(UsTMenuAction.class);
        countQuery.select(criteriaBuilder.count(countRoot)).where(predicate);
        return entityManager.createQuery(countQuery).getSingleResult();
    }

    public Page<UsTMenuAction> findAllWithFilters(int page, Integer per_page, String sort, String direction, String search, String[] statusId, String[] moduleId, String[] menuId, String[] actionId) {
        CriteriaQuery<UsTMenuAction> criteriaQuery = criteriaBuilder.createQuery(UsTMenuAction.class);
        Root<UsTMenuAction> menuRoot = criteriaQuery.from(UsTMenuAction.class);
        Predicate predicate = getPredicate(search, moduleId, menuId, actionId, statusId, menuRoot);
        criteriaQuery.where(predicate);
        setOrder(criteriaQuery, sort, direction, menuRoot);

        TypedQuery<UsTMenuAction> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setFirstResult(page * per_page);
        typedQuery.setMaxResults(per_page);

        Pageable pageable = getPageable(page, per_page, sort, direction);

        long count = getCount(predicate);

        return new PageImpl<>(typedQuery.getResultList(), pageable, count);
    }
}
