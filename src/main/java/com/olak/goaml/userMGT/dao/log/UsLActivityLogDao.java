package com.olak.goaml.userMGT.dao.log;

import com.olak.goaml.userMGT.models.log.UsLActivityLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsLActivityLogDao extends JpaRepository<UsLActivityLog, Integer> {
}