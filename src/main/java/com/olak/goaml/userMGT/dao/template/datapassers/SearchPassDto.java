package com.olak.goaml.userMGT.dao.template.datapassers;

import lombok.Data;

/**
 * @Name: Search filed passer for Search & filter util
 * @Author: Chamod Ishankha
 * @Version: v3.0
 * @Date: 07-Sep-2022
 * @Doc: Allow five entity deep search!
 */
@Data
public class SearchPassDto {

    private String fieldName;
    private String subEntityName;
    private String subEntityName2;
    private String subEntityName3;
    private String subEntityName4;
    private String subEntityName5;

    /**
     * @param fieldName
     * @Doc: Normally pass the filed name
     */
    public SearchPassDto(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @param subEntityName
     * @param fieldName
     */
    public SearchPassDto(String subEntityName, String fieldName) {
        this.subEntityName = subEntityName;
        this.fieldName = fieldName;
    }

    /**
     * @param subEntityName
     * @param subEntityName2
     * @param fieldName
     */
    public SearchPassDto(String subEntityName, String subEntityName2, String fieldName) {
        this.subEntityName = subEntityName;
        this.subEntityName2 = subEntityName2;
        this.fieldName = fieldName;
    }

    /**
     * @param subEntityName
     * @param subEntityName2
     * @param subEntityName3
     * @param fieldName
     */
    public SearchPassDto(String subEntityName, String subEntityName2, String subEntityName3, String fieldName) {
        this.subEntityName = subEntityName;
        this.subEntityName2 = subEntityName2;
        this.subEntityName3 = subEntityName3;
        this.fieldName = fieldName;
    }

    /**
     * @param subEntityName
     * @param subEntityName2
     * @param subEntityName3
     * @param subEntityName4
     * @param fieldName
     */
    public SearchPassDto(String subEntityName, String subEntityName2, String subEntityName3, String subEntityName4, String fieldName) {
        this.subEntityName = subEntityName;
        this.subEntityName2 = subEntityName2;
        this.subEntityName3 = subEntityName3;
        this.subEntityName4 = subEntityName4;
        this.fieldName = fieldName;
    }

    /**
     * @param subEntityName
     * @param subEntityName2
     * @param subEntityName3
     * @param subEntityName4
     * @param subEntityName5
     * @param fieldName
     */
    public SearchPassDto(String subEntityName, String subEntityName2, String subEntityName3, String subEntityName4, String subEntityName5, String fieldName) {
        this.subEntityName = subEntityName;
        this.subEntityName2 = subEntityName2;
        this.subEntityName3 = subEntityName3;
        this.subEntityName4 = subEntityName4;
        this.subEntityName5 = subEntityName5;
        this.fieldName = fieldName;
    }
}
