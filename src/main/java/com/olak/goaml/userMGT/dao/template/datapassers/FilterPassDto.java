package com.olak.goaml.userMGT.dao.template.datapassers;

import lombok.Data;

/**
 * @Name: Filter filed & content passer for Search & filter util
 * @Author: Chamod Ishankha
 * @Version: v3.0
 * @Date: 05-Sep-2022
 * @Doc: Allow five entity deep filter!
 */
@Data
public class FilterPassDto {
    private String fieldName;
    private Object content;
    private String subEntityName;
    private String subEntityName2;
    private String subEntityName3;
    private String subEntityName4;
    private String subEntityName5;

    /**
     * @param fieldName
     * @param content
     * @Doc: Normally pass the filed name and value
     */
    public FilterPassDto(String fieldName, Object content) {
        this.fieldName = fieldName;
        this.content = content;
    }

    /**
     * @param subEntityName
     * @param fieldName
     * @param content
     */
    public FilterPassDto(String subEntityName, String fieldName, Object content) {
        this.subEntityName = subEntityName;
        this.fieldName = fieldName;
        this.content = content;
    }

    /**
     * @param subEntityName
     * @param subEntityName2
     * @param fieldName
     * @param content
     */
    public FilterPassDto(String subEntityName, String subEntityName2, String fieldName, Object content) {
        this.subEntityName = subEntityName;
        this.subEntityName2 = subEntityName2;
        this.fieldName = fieldName;
        this.content = content;
    }

    /**
     * @param subEntityName
     * @param subEntityName2
     * @param subEntityName3
     * @param fieldName
     * @param content
     */
    public FilterPassDto(String subEntityName, String subEntityName2, String subEntityName3, String fieldName, Object content) {
        this.subEntityName = subEntityName;
        this.subEntityName2 = subEntityName2;
        this.subEntityName3 = subEntityName3;
        this.fieldName = fieldName;
        this.content = content;
    }

    /**
     * @param subEntityName
     * @param subEntityName2
     * @param subEntityName3
     * @param subEntityName4
     * @param fieldName
     * @param content
     */
    public FilterPassDto(String subEntityName, String subEntityName2, String subEntityName3, String subEntityName4, String fieldName, Object content) {
        this.subEntityName = subEntityName;
        this.subEntityName2 = subEntityName2;
        this.subEntityName3 = subEntityName3;
        this.subEntityName4 = subEntityName4;
        this.fieldName = fieldName;
        this.content = content;
    }

    /**
     * @param subEntityName
     * @param subEntityName2
     * @param subEntityName3
     * @param subEntityName4
     * @param subEntityName5
     * @param fieldName
     * @param content
     */
    public FilterPassDto(String subEntityName, String subEntityName2, String subEntityName3, String subEntityName4, String subEntityName5, String fieldName, Object content) {
        this.subEntityName = subEntityName;
        this.subEntityName2 = subEntityName2;
        this.subEntityName3 = subEntityName3;
        this.subEntityName4 = subEntityName4;
        this.subEntityName5 = subEntityName5;
        this.fieldName = fieldName;
        this.content = content;
    }

}
