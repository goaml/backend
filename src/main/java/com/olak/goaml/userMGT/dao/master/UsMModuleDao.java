package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMModule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsMModuleDao extends JpaRepository<UsMModule, Integer> {

    @Query("SELECT m FROM UsMModule m WHERE cast(m.moduleId string) LIKE %?1%")
    Page<UsMModule> getAllModulesForTable(String search, Boolean value, PageRequest of);
}