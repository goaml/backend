package com.olak.goaml.userMGT.dao.reference;

import com.olak.goaml.userMGT.models.reference.UsRUserBranch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsRUserBranchDao extends JpaRepository<UsRUserBranch, Integer> {

    // Optional<UsRUserBranch> findUsRUserBranchByAdMUserUserId(Integer userId);

    List<UsRUserBranch> findUsRUserBranchByAdMUserUserId(Integer userId);

    Optional<UsRUserBranch> findUsRUserBranchByAdMUserUserIdAndUsRBrachDepartmentUsMBranchBranchId(Integer userId, Integer branchId);

    @Query("SELECT u FROM UsRUserBranch u WHERE u.adMUser.userId=?1 AND u.usRBrachDepartment.usMBranch.branchId=?2")
    List<UsRUserBranch> findBranchByUserId(Integer userId, Integer branchId);

    List<UsRUserBranch> findUsRUserBranchByAdMUserUserIdAndUsRBrachDepartmentBrachDeptId(Integer userId, Integer brachDeptId);
}
