package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMLocationDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsMLocationDetailDao extends JpaRepository<UsMLocationDetail, Integer> {
}