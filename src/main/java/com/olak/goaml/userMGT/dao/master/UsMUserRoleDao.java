package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMUserRole;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsMUserRoleDao extends JpaRepository<UsMUserRole, Integer> {

    @Query("SELECT usr FROM UsMUserRole usr WHERE usr.statusFlag='N'")
    Page<UsMUserRole> findByStatusFlag(Boolean value, PageRequest of);

    UsMUserRole findByUserRoleName(String userRoleName);

    @Query("SELECT men FROM UsTMenuAction men WHERE "
            + "men.menuActionId IN "
            + "(SELECT map.usTMenuAction.menuActionId FROM UsMMenuAllowedMap map WHERE map.usMUserRole.userRoleId = ?1)")
    List<UsTMenuAction> findAllowedMenuActionsForRole(Integer userRoleId);

    @Query("SELECT men FROM UsTMenuAction men WHERE "
            + "men.menuActionId NOT IN "
            + "(SELECT map.usTMenuAction.menuActionId FROM UsMMenuAllowedMap map WHERE map.usMUserRole.userRoleId = ?1)")
    List<UsTMenuAction> findRestrictedMenuActionsForRole(Integer userRoleId);

    @Query("SELECT usr FROM UsMUserRole usr WHERE usr.usRStatusDetail.statusId = ?1")
    Page<UsMUserRole> findByUsRStatusDetailStatusId(Integer statusId, PageRequest of);

}