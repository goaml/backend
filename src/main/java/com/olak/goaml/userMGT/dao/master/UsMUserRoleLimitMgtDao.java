package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMUserRoleLimitMgt;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsMUserRoleLimitMgtDao extends JpaRepository<UsMUserRoleLimitMgt, Integer> {

    @Query("SELECT rol FROM UsMUserRoleLimitMgt rol WHERE cast( rol.roleLimitId as string) LIKE %?1%  ")
    Page<UsMUserRoleLimitMgt> findByUserRoleForTables(String search, PageRequest of);
}