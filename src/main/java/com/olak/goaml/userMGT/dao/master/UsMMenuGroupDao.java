package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMMenuGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsMMenuGroupDao extends JpaRepository<UsMMenuGroup, Integer> {
    List<UsMMenuGroup> findAllByUsMModuleModuleIdOrderBySeqNoAsc(Integer moduleId);
}
