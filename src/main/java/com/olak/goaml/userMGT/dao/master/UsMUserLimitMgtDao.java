package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMUserLimitMgt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsMUserLimitMgtDao extends JpaRepository<UsMUserLimitMgt, Integer> {
}