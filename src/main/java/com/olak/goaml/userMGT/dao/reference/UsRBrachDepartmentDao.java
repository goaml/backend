package com.olak.goaml.userMGT.dao.reference;

import com.olak.goaml.userMGT.models.reference.UsRBrachDepartment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsRBrachDepartmentDao extends JpaRepository<UsRBrachDepartment, Integer> {

    Optional<UsRBrachDepartment> findByUsMBranchBranchIdAndUsMDepartmentDepartmentId(Integer branchId, Integer DepartId);

//    UsRBrachDepartment findUsRBrachDepartmentByUsMBranchBranchIdAndUsMDepartmentDepartmentId(Integer branchId,Integer departmentId);

    @Query("SELECT D FROM UsRBrachDepartment D WHERE D.usMBranch.branchId=?1")
    Page<UsRBrachDepartment> getDepartmentForBranchId(Integer branchId, PageRequest of);

    List<UsRBrachDepartment> findUsRBrachDepartmentByUsMBranchBranchIdAndUsMDepartmentDepartmentId(Integer branchId, Integer departmentId);

    List<UsRBrachDepartment> findUsRBrachDepartmentByUsMBranchBranchId(Integer branchId);
}
