package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMBranch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsMBranchDao extends JpaRepository<UsMBranch, Integer> {

    @Query("SELECT BR FROM UsMBranch BR WHERE cast(BR.branchId string) LIKE %?1%")
    Page<UsMBranch> getAllBranchesForTable(String search, Boolean value, PageRequest of);

    @Query("SELECT BR FROM UsMBranch BR WHERE " +
            "LOWER(CONCAT(BR.branchAddress,BR.branchLocation,BR.branchName,BR.contactNumber,BR.email)) LIKE %?1% AND " +
            "LOWER(CONCAT(BR.branchAddress,BR.branchLocation,BR.branchName,BR.contactNumber,BR.email)) LIKE %?2% AND " +
            "LOWER(CONCAT(BR.branchAddress,BR.branchLocation,BR.branchName,BR.contactNumber,BR.email)) LIKE %?3% AND " +
            "LOWER(CONCAT(BR.branchAddress,BR.branchLocation,BR.branchName,BR.contactNumber,BR.email)) LIKE %?4% AND " +
            "LOWER(CONCAT(BR.branchAddress,BR.branchLocation,BR.branchName,BR.contactNumber,BR.email)) LIKE %?5%")
    Page<UsMBranch> findAllBranchesWithAttributes(String branchAddress, String branchLocation, String branchName, String contactNumber, String email, Boolean value, PageRequest of);

    @Query("SELECT b FROM UsMBranch b,AdMUser u WHERE u.userId=?1 AND b.branchId=u.usMBranch.branchId")
    Page<UsMBranch> findUsMBranchByUserId(Integer userId, PageRequest of);

    @Query("SELECT b FROM UsMBranch b WHERE b.branchId=?1")
    Page<UsMBranch> findUsMBranchByBranchId(Integer branchId, PageRequest of);

    Optional<UsMBranch> findByBranchNameOrDesc(String branchName, String desc);

}