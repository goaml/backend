package com.olak.goaml.userMGT.dao.reference;

import com.olak.goaml.userMGT.models.reference.UsRUserIdType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UsRUserIdTypeDao extends JpaRepository<UsRUserIdType, Integer> {
    List<UsRUserIdType> findAllByIsActive(Boolean isActive);

    Optional<UsRUserIdType> findByUserCategory(String userCategory);

    @Query("SELECT USR FROM UsRUserIdType USR WHERE (USR.usRLoginType.loginTypeId = ?1) AND USR.isActive = true")
    Page<UsRUserIdType> findUserIdTypesForTableByLoginType(Integer loginTypeId, PageRequest of);

    @Query("SELECT USR FROM UsRUserIdType USR WHERE (USR.userCategory LIKE %?1%) AND USR.isActive = true")
    Page<UsRUserIdType> findUserIdTypeForTable(String search, PageRequest of);

    Optional<UsRUserIdType> findByUserTypeIdAndIsActive(Integer userTypeId, Boolean isActive);
}
