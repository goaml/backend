package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMDepartment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UsMDepartmentDao extends JpaRepository<UsMDepartment, Integer> {

    @Query("SELECT d FROM UsMDepartment d WHERE cast(d.departmentId string) LIKE %?1")
    Page<UsMDepartment> getAllDepartmentsForTable(String search, Boolean value, PageRequest of);

    Optional<UsMDepartment> findByDeptNameOrDesc(String deptName, String desc);
}