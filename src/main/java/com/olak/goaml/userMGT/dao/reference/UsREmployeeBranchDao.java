package com.olak.goaml.userMGT.dao.reference;

import com.olak.goaml.userMGT.models.reference.UsREmployeeBranch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsREmployeeBranchDao extends JpaRepository<UsREmployeeBranch, Integer> {
}
