package com.olak.goaml.userMGT.dao.template.updated;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;


public interface FilterCriteria<T> {
    Specification<T> buildPredicate(Root<T> root, CriteriaBuilder criteriaBuilder);
}

