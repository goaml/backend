package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UsMUserDao extends JpaRepository<UsMUser, Integer> {
    Optional<UsMUser> findUsMUserByUsMEmployeeEmployeeId(Integer employeeId);

    Optional<UsMUser> findUsMUserByUsername(String username);

    @Query("SELECT u, s, r, e FROM UsMUser u INNER JOIN UsRStatusDetail s ON u.usRStatusDetail.statusCode = s.statusCode INNER JOIN UsMUserRole r ON u.usMUserRole.userRoleId = r.userRoleId INNER JOIN UsMEmployee e ON u.usMEmployee.employeeId = e.employeeId")
    Page<Object[]> findUsersWithStatusForTable(String search, PageRequest of);

    @Query("SELECT u, s, r, e FROM UsMUser u INNER JOIN UsRStatusDetail s ON u.usRStatusDetail.statusCode = s.statusCode INNER JOIN UsMUserRole r ON u.usMUserRole.userRoleId = r.userRoleId INNER JOIN UsMEmployee e ON u.usMEmployee.employeeId = e.employeeId WHERE s.statusId=?1")
    Page<Object[]> findUsersWithStatusId(Integer statusId, PageRequest of);
}