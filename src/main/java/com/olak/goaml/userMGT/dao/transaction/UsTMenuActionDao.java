package com.olak.goaml.userMGT.dao.transaction;

import com.olak.goaml.userMGT.models.master.UsMAction;
import com.olak.goaml.userMGT.models.master.UsMMenu;
import com.olak.goaml.userMGT.models.master.UsMModule;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UsTMenuActionDao extends JpaRepository<UsTMenuAction, Integer> {

    @Query("SELECT ma FROM UsTMenuAction ma")
    List<UsTMenuAction> getAllActivityMenuActionItems();

    @Query("SELECT menuActions " +
            "FROM UsTMenuAction menuActions " +
            "WHERE menuActions.menuActionId IN " +
            "(SELECT map.usTMenuAction.menuActionId FROM UsMMenuAllowedMap map WHERE map.usMUserRole.userRoleId=?1) AND menuActions.usRStatusDetail.statusId = 3")
    List<UsTMenuAction> findUserAccessMenuListForUserRole(Integer userRoleId);

    @Query("SELECT ma FROM UsTMenuAction ma WHERE ma.usRStatusDetail.statusId = ?1")
    Page<UsTMenuAction> findByUsRStatusDetailStatusId(Integer statusId, PageRequest of);

    @Query("SELECT ma FROM UsTMenuAction ma WHERE ma.usMMenu.menuId = ?1")
    Page<UsTMenuAction> findByUsMMenuMenuId(Integer menuId, PageRequest of);

    @Query("SELECT ma FROM UsTMenuAction ma WHERE ma.usMModule.moduleId = ?1")
    Page<UsTMenuAction> findByUsMModuleModuleId(Integer moduleId, PageRequest of);

    @Query("SELECT ma FROM UsTMenuAction ma WHERE ma.usMAction.actionId = ?1")
    Page<UsTMenuAction> findByUsMActionActionId(Integer actionId, PageRequest of);

    @Query("SELECT MA FROM UsTMenuAction MA WHERE cast(MA.menuActionId string) LIKE %?1%")
    Page<UsTMenuAction> getAllMenuActions(String search, Boolean value, PageRequest of);

    Optional<UsTMenuAction> findUsTMenuActionByUsMActionAndUsMMenuAndUsMModule(UsMAction usMAction, UsMMenu usMMenu, UsMModule usMModule);

}