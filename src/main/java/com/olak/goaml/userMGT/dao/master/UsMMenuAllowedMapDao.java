package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMMenuAllowedMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UsMMenuAllowedMapDao extends JpaRepository<UsMMenuAllowedMap, Integer> {
    @Query("SELECT DISTINCT user,role,allowMap,menu,module,action,menuAction FROM AdMUser user, UsMUserRole role, UsMMenuAllowedMap allowMap, UsMMenu menu, UsMModule module, UsMAction action, UsTMenuAction menuAction " +
            "WHERE " +
            "user.usMUserRole.userRoleId=role.userRoleId AND " +
            "role.userRoleId=allowMap.usMUserRole.userRoleId AND " +
            "allowMap.usTMenuAction.menuActionId=menuAction.menuActionId AND " +
            "menuAction.usMMenu.menuId=menu.menuId AND " +
            "menuAction.usMModule.moduleId=module.moduleId AND " +
            "menuAction.usMAction.actionId=action.actionId AND " +
            "user.userId=?1")
    List<Object[]> findAllowMapListForTable(Integer userId);

    @Query("SELECT alm FROM UsMMenuAllowedMap alm WHERE alm.usMUserRole.userRoleId=?1 AND alm.usRStatusDetail.statusId=?2")
    Page<UsMMenuAllowedMap> findMenuAllowMapByUserRoleIdAndStatusId(Integer userRoleId, Integer statusId, PageRequest of);

    @Query("SELECT alm FROM UsMMenuAllowedMap alm WHERE alm.usMUserRole.userRoleId=?1")
    Page<UsMMenuAllowedMap> findMenuAllowMapForTable(Integer userRoleId, PageRequest of);

    Optional<UsMMenuAllowedMap> findByUsMUserRoleUserRoleIdAndUsTMenuActionMenuActionId(Integer userRoleId, Integer menuActionId);
}
