package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMAction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsMActionDao extends JpaRepository<UsMAction, Integer> {

    @Query("SELECT a FROM UsMAction a WHERE cast(a.actionId string) LIKE %?1%")
    Page<UsMAction> getAllActionsForTable(String search, Boolean value, PageRequest of);
}