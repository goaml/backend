package com.olak.goaml.userMGT.dao.template;

import com.olak.goaml.userMGT.dao.template.datapassers.FilterPassDto;
import com.olak.goaml.userMGT.dao.template.datapassers.SearchPassDto;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * @Name: Search & filter util (Automation Mode)
 * @Author: Chamod Ishankha
 * @Version: v3.0
 * @Date: 05-Sep-2022
 * @Doc: Allow five entity dig search | filter!
 */
@Repository
@Slf4j
@Data
public class SearchNFilterImpl implements SearchNFilter {

    private static final String temporaryClassesPath = "/temporary/classes"; // not use with this version
    /**
     * managers (initialize when service starting.)
     */
    private final EntityManager entityManager;
    /**********************************************************************************************************/
    private final CriteriaBuilder criteriaBuilder;
    /**
     * check this entity has an isActive columns
     */
    boolean isActiveColumn = false;
    /**********************************************************************************************************/
    /**
     * configurations...
     * you can change default search variable index by using setter
     */
    private int DEFAULT_SEARCH_VARIABLE_INDEX = 0;
    /**
     * pagination variables
     */
    private int page;
    private Integer per_page;
    private String sort;
    /**********************************************************************************************************/
    private String direction;
    /**
     * global variables
     */
    private String search;
    private Boolean activeStatus;
    private Class<?> entityClass;
    private List<SearchPassDto> searchFieldList;
    private List<FilterPassDto> filterFieldList;
    /**
     * deep level search fields
     */
    private List<SearchPassDto> deepSearchlvl1;
    private List<SearchPassDto> deepSearchlvl2;
    private List<SearchPassDto> deepSearchlvl3;
    private List<SearchPassDto> deepSearchlvl4;
    private List<SearchPassDto> deepSearchlvl5;
    /**
     * deep level filter fields
     */
    private List<FilterPassDto> deepFilterlvl1;
    private List<FilterPassDto> deepFilterlvl2;
    private List<FilterPassDto> deepFilterlvl3;
    private List<FilterPassDto> deepFilterlvl4;
    private List<FilterPassDto> deepFilterlvl5;
    /**********************************************************************************************************/
    /**********************************************************************************************************/
    private List<Field> parentEntityFieldsArrayWithoutRedundant;

    /**
     * constructor
     *
     * @param entityManager
     */
    public SearchNFilterImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    // create temporary class with given fields
    private static Class<?> createTemporaryClassWithGivenFields(String entityClassName, List<Field> subEntityFieldListWithoutRedundant) {
        Class<?> entityClass;
        try {
            // create directory for save temporary classes
            // create a directory if not exists
            String dir = System.getProperty("user.dir") + temporaryClassesPath;
            Path path = Paths.get(dir);
            Files.createDirectories(path);
            String suffix = ".class";
            // create classpath
            String classPathWithName = dir + "/" + entityClassName;
            // write the class
            ClassWriter classWriter = new ClassWriter(0);
            classWriter.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC | Opcodes.ACC_SUPER, entityClassName, null, "java/lang/Object", new String[0]);
            classWriter.visitSource(".dyn", null);
            // add @Entity to class
            classWriter.visitAnnotation(Type.getDescriptor(javax.persistence.Entity.class), true).visitEnd();
            for (Field field : subEntityFieldListWithoutRedundant) {
                classWriter.visitField(Opcodes.ACC_PRIVATE, field.getName(), Type.getDescriptor(field.getType()), null, null).visitEnd();
            }
            classWriter.visitEnd();
            // finish write the class
            // save
            byte[] bytes = classWriter.toByteArray();
            OutputStream save = new BufferedOutputStream(Files.newOutputStream(Paths.get(classPathWithName + suffix)));
            save.write(bytes);
            save.close();
            // saved
            // get class from it
            // create Link
            File file = new File(dir);
            URL url = file.toURI().toURL();
            URL[] urls = new URL[]{url};
            // create the new ClassLoader with the directory
            ClassLoader classLoader = new URLClassLoader(urls);
            // Load class
            entityClass = classLoader.loadClass(entityClassName);
            // delete created temporary files & directories
            FileUtils.deleteDirectory(new File(System.getProperty("user.dir") + "/temporary"));
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        // return class
        return entityClass;
    }

    /**
     * @param page
     * @param per_page
     * @param sort
     * @param direction
     * @Doc Pagination Details setter
     */
    @Override
    public void setPaginationDetails(int page, Integer per_page, String sort, String direction) {
        this.page = page;
        this.per_page = per_page;
        this.sort = sort;
        this.direction = direction;
    }

    /**
     * @param searchFieldList
     * @Doc Search columns setter
     */
    @Override
    public void setSearchList(List<SearchPassDto> searchFieldList) {
        // remove null search variables from searchFieldList --------
        searchFieldList.removeIf(searchPassDto ->
                searchPassDto.getFieldName() == null ||
                        searchPassDto.getFieldName().length() == 0 ||
                        searchPassDto.getFieldName().equals("null")
        );
        List<SearchPassDto> localDeepSearchlvl1 = new ArrayList<>();
        List<SearchPassDto> localDeepSearchlvl2 = new ArrayList<>();
        List<SearchPassDto> localDeepSearchlvl3 = new ArrayList<>();
        List<SearchPassDto> localDeepSearchlvl4 = new ArrayList<>();
        List<SearchPassDto> localDeepSearchlvl5 = new ArrayList<>();
        for (Iterator<SearchPassDto> searchPassDtoIterator = searchFieldList.iterator(); searchPassDtoIterator.hasNext(); ) {
            SearchPassDto searchPassDto = searchPassDtoIterator.next();
            // Level 01 deep
            if (
                    searchPassDto.getSubEntityName() != null
                            && searchPassDto.getSubEntityName2() == null
                            && searchPassDto.getFieldName() != null
            ) {
                localDeepSearchlvl1.add(searchPassDto);
                searchPassDtoIterator.remove();
            }
            // Level 02 deep
            if (
                    searchPassDto.getSubEntityName() != null
                            && searchPassDto.getSubEntityName2() != null
                            && searchPassDto.getSubEntityName3() == null
                            && searchPassDto.getFieldName() != null
            ) {
                localDeepSearchlvl2.add(searchPassDto);
                searchPassDtoIterator.remove();
            }
            // Level 03 deep
            if (
                    searchPassDto.getSubEntityName() != null
                            && searchPassDto.getSubEntityName2() != null
                            && searchPassDto.getSubEntityName3() != null
                            && searchPassDto.getSubEntityName4() == null
                            && searchPassDto.getFieldName() != null
            ) {
                localDeepSearchlvl3.add(searchPassDto);
                searchPassDtoIterator.remove();
            }
            // Level 04 deep
            if (
                    searchPassDto.getSubEntityName() != null
                            && searchPassDto.getSubEntityName2() != null
                            && searchPassDto.getSubEntityName3() != null
                            && searchPassDto.getSubEntityName4() != null
                            && searchPassDto.getSubEntityName5() == null
                            && searchPassDto.getFieldName() != null
            ) {
                localDeepSearchlvl4.add(searchPassDto);
                searchPassDtoIterator.remove();
            }
            // Level 05 deep
            if (
                    searchPassDto.getSubEntityName() != null
                            && searchPassDto.getSubEntityName2() != null
                            && searchPassDto.getSubEntityName3() != null
                            && searchPassDto.getSubEntityName4() != null
                            && searchPassDto.getSubEntityName5() != null
                            && searchPassDto.getFieldName() != null
            ) {
                localDeepSearchlvl5.add(searchPassDto);
                searchPassDtoIterator.remove();
            }
        }
        // set founded deep search values to global variables
        this.deepSearchlvl1 = localDeepSearchlvl1;
        this.deepSearchlvl2 = localDeepSearchlvl2;
        this.deepSearchlvl3 = localDeepSearchlvl3;
        this.deepSearchlvl4 = localDeepSearchlvl4;
        this.deepSearchlvl5 = localDeepSearchlvl5;
        this.searchFieldList = searchFieldList;
    }

    /**
     * @param filterFieldList
     * @Doc Filter columns setter
     */
    @Override
    public void setFilterList(List<FilterPassDto> filterFieldList) {
        // remove null filter variables from filterFieldList --------
        filterFieldList.removeIf(filterPassDto ->
                filterPassDto.getContent() == null ||
                        filterPassDto.getContent().toString().length() == 0 ||
                        filterPassDto.getContent().equals("null") ||
                        filterPassDto.getFieldName() == null ||
                        filterPassDto.getFieldName().length() == 0 ||
                        filterPassDto.getFieldName().equals("null")
        );
        // separate by deep filter level

        List<FilterPassDto> localDeepFilterlvl1 = new ArrayList<>();
        List<FilterPassDto> localDeepFilterlvl2 = new ArrayList<>();
        List<FilterPassDto> localDeepFilterlvl3 = new ArrayList<>();
        List<FilterPassDto> localDeepFilterlvl4 = new ArrayList<>();
        List<FilterPassDto> localDeepFilterlvl5 = new ArrayList<>();
        for (Iterator<FilterPassDto> filterPassDtoIterator = filterFieldList.iterator(); filterPassDtoIterator.hasNext(); ) {
            FilterPassDto filterPassDto = filterPassDtoIterator.next();
            // Level 01 deep
            if (
                    filterPassDto.getSubEntityName() != null
                            && filterPassDto.getSubEntityName2() == null
                            && filterPassDto.getFieldName() != null
            ) {
                localDeepFilterlvl1.add(filterPassDto);
                filterPassDtoIterator.remove();
            }
            // Level 02 deep
            if (
                    filterPassDto.getSubEntityName() != null
                            && filterPassDto.getSubEntityName2() != null
                            && filterPassDto.getSubEntityName3() == null
                            && filterPassDto.getFieldName() != null
            ) {
                localDeepFilterlvl2.add(filterPassDto);
                filterPassDtoIterator.remove();
            }
            // Level 03 deep
            if (
                    filterPassDto.getSubEntityName() != null
                            && filterPassDto.getSubEntityName2() != null
                            && filterPassDto.getSubEntityName3() != null
                            && filterPassDto.getSubEntityName4() == null
                            && filterPassDto.getFieldName() != null
            ) {
                localDeepFilterlvl3.add(filterPassDto);
                filterPassDtoIterator.remove();
            }
            // Level 04 deep
            if (
                    filterPassDto.getSubEntityName() != null
                            && filterPassDto.getSubEntityName2() != null
                            && filterPassDto.getSubEntityName3() != null
                            && filterPassDto.getSubEntityName4() != null
                            && filterPassDto.getSubEntityName5() == null
                            && filterPassDto.getFieldName() != null
            ) {
                localDeepFilterlvl4.add(filterPassDto);
                filterPassDtoIterator.remove();
            }
            // Level 05 deep
            if (
                    filterPassDto.getSubEntityName() != null
                            && filterPassDto.getSubEntityName2() != null
                            && filterPassDto.getSubEntityName3() != null
                            && filterPassDto.getSubEntityName4() != null
                            && filterPassDto.getSubEntityName5() != null
                            && filterPassDto.getFieldName() != null
            ) {
                localDeepFilterlvl5.add(filterPassDto);
                filterPassDtoIterator.remove();
            }
        }
        // set founded deep filter values to global variables
        this.deepFilterlvl1 = localDeepFilterlvl1;
        this.deepFilterlvl2 = localDeepFilterlvl2;
        this.deepFilterlvl3 = localDeepFilterlvl3;
        this.deepFilterlvl4 = localDeepFilterlvl4;
        this.deepFilterlvl5 = localDeepFilterlvl5;
        this.filterFieldList = filterFieldList;
    }
    /**********************************************************************************************************/

    /**
     * @param search
     * @param activeStatus
     * @Doc Parsed search value from frontend setter
     */
    @Override
    public void setVariables(String search, Boolean activeStatus) {
        this.search = search;
        this.activeStatus = activeStatus;
    }

    /**
     * @param entityClass
     * @Doc Entity class to be filtered | search setter
     */
    @Override
    public void setEntityClass(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * @return
     * @Doc Main process to return queried detail list
     * filtered or | and searched, paginated list getter
     */
    @Override
    public Page<?> getFiltered() {
        CriteriaQuery<?> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        Root<?> root = criteriaQuery.from(entityClass);
        Predicate predicate = getPredicate(root);
        criteriaQuery.where(predicate);
        setOrder(criteriaQuery, sort, direction, root);

        TypedQuery<?> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setFirstResult(page * per_page);
        typedQuery.setMaxResults(per_page);

        Pageable pageable = getPageable(page, per_page, sort, direction);

        long count = getCount(predicate, entityClass);
        return new PageImpl<>(typedQuery.getResultList(), pageable, count);
//        try {
//
//        } catch (Exception e) {
//            // Handle the exception here
//            e.fillInStackTrace(); // Or log the exception for debugging purposes
//
//            // You can also throw a custom exception or return a specific error response
//            // throw new CustomException("Error occurred while fetching data.", e);
//            // return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal Server Error");
//
//            // Return an empty or default Page in case of an error, or handle it as appropriate for your application
//            return new PageImpl<>(Collections.emptyList(), pageable, 0);
//        }
    }

    /**
     * build where clause for select query
     *
     * @param root
     * @return
     */
    private Predicate getPredicate(Root<?> root) {
        log.info("----------------------- start search & filter method -----------------------");
        //-----------------------------------------------
        // collect data start --
        // check parent entity has sub entities
        List<Field> entityList = checkEntityHasEntities(entityClass);
        // drop redundant fields of parent entity and save to fields array
        parentEntityFieldsArrayWithoutRedundant = dropRedundantVariablesInsideEntity(entityClass);
        // store first field as default search variable for search predicate; (usage: if searchFieldsLis (array) == 0)
        Field defaultSearchField = parentEntityFieldsArrayWithoutRedundant.get(DEFAULT_SEARCH_VARIABLE_INDEX);
        // collect data end --
        //-----------------------------------------------
        // build search predicate (build where clause with 'LIKE' and 'OR' gate)
        List<Predicate> searchPredicateList = buildSearchPredicates(defaultSearchField, entityList, root);
        //-----------------------------------------------
        // build filter predicate (build where clause with 'EQUAL' and 'AND' gate)
        List<Predicate> filterPredicateList = new ArrayList<>();
        if (filterFieldList.size() != 0 || deepFilterlvl1.size() != 0 || deepFilterlvl2.size() != 0 || deepFilterlvl3.size() != 0 || deepFilterlvl4.size() != 0 || deepFilterlvl5.size() != 0) {
            filterPredicateList = buildFilterPredicateList(entityList, root);
        }
        //-----------------------------------------------
        // activate isActive checker
        if (isActiveColumn) {
            log.info("------------------------------------ This entity has an isActive field ------------------------------------");
            filterPredicateList.add(
                    criteriaBuilder.equal(root.get("isActive"), activeStatus)
            );
            log.info("________________________________");
        } else {
            log.info("--------------------------------- This entity doesn't have an active field ---------------------------------");
            log.info("________________________________");
        }
        // ------------------------------------------------------------
        // all search queries join with 'OR' gate
        Predicate searchPredicates = criteriaBuilder.or(searchPredicateList.toArray(new Predicate[0]));
        // all filter queries join with 'AND' gate
        Predicate filterPredicates = criteriaBuilder.and(filterPredicateList.toArray(new Predicate[0]));
        log.info("----------------------- end search & filter method -----------------------");
        // search & filter query join with 'AND' gate
        return criteriaBuilder.and(searchPredicates, filterPredicates);
    }

    /*
    build filter predicates
     */
    private List<Predicate> buildFilterPredicateList(List<Field> entityList, Root<?> root) {
        log.info("________________________________");
        List<Predicate> filterPredicateList = new ArrayList<>();
        // Field filter
        if (filterFieldList.size() != 0) {
            filterPredicateList.addAll(buildFiledFilterPredicateList(root));
        }
        // Level 01 deep filter call
        if (deepFilterlvl1.size() != 0 && entityList.size() != 0) {
            filterPredicateList.addAll(buildLvl1DeepFilterPredicateList(root));
        }
        // Level 02 deep filter call
        if (deepFilterlvl2.size() != 0 && entityList.size() != 0) {
            filterPredicateList.addAll(buildLvl2DeepFilterPredicateList(root));
        }
        // Level 03 deep filter call
        if (deepFilterlvl3.size() != 0 && entityList.size() != 0) {
            filterPredicateList.addAll(buildLvl3DeepFilterPredicateList(root));
        }
        // Level 04 deep filter call
        if (deepFilterlvl4.size() != 0 && entityList.size() != 0) {
            filterPredicateList.addAll(buildLvl4DeepFilterPredicateList(root));
        }
        // Level 05 deep filter call
        if (deepFilterlvl5.size() != 0 && entityList.size() != 0) {
            filterPredicateList.addAll(buildLvl5DeepFilterPredicateList(root));
        }
        return filterPredicateList;
    }

    // deep filter lvl5 predicate list creation
    private Collection<? extends Predicate> buildLvl5DeepFilterPredicateList(Root<?> root) {
        List<Predicate> filterListDeepLv5 = new ArrayList<>();
        for (FilterPassDto filterPassDto : deepFilterlvl5) {
            if (filterPassDto.getContent() instanceof List) {
                List<?> idList = (List<?>) filterPassDto.getContent();
                filterListDeepLv5.add(
                        root.
                                get(filterPassDto.getSubEntityName()).
                                get(filterPassDto.getSubEntityName2()).
                                get(filterPassDto.getSubEntityName3()).
                                get(filterPassDto.getSubEntityName4()).
                                get(filterPassDto.getSubEntityName5()).
                                get(filterPassDto.getFieldName()).in(idList));
            } else {
                filterListDeepLv5.add(
                        criteriaBuilder.equal(
                                root.
                                        get(filterPassDto.getSubEntityName()).
                                        get(filterPassDto.getSubEntityName2()).
                                        get(filterPassDto.getSubEntityName3()).
                                        get(filterPassDto.getSubEntityName4()).
                                        get(filterPassDto.getSubEntityName5()).
                                        get(filterPassDto.getFieldName()
                                        ), filterPassDto.getContent()
                        )
                );
            }
        }
        return filterListDeepLv5;
    }

    // deep filter lvl4 predicate list creation
    private Collection<? extends Predicate> buildLvl4DeepFilterPredicateList(Root<?> root) {
        List<Predicate> filterListDeepLv4 = new ArrayList<>();
        for (FilterPassDto filterPassDto : deepFilterlvl4) {
            if (filterPassDto.getContent() instanceof List) {
                List<?> idList = (List<?>) filterPassDto.getContent();
                filterListDeepLv4.add(
                        root.
                                get(filterPassDto.getSubEntityName()).
                                get(filterPassDto.getSubEntityName2()).
                                get(filterPassDto.getSubEntityName3()).
                                get(filterPassDto.getSubEntityName4()).
                                get(filterPassDto.getFieldName()).in(idList));
            } else {
                filterListDeepLv4.add(
                        criteriaBuilder.equal(
                                root.
                                        get(filterPassDto.getSubEntityName()).
                                        get(filterPassDto.getSubEntityName2()).
                                        get(filterPassDto.getSubEntityName3()).
                                        get(filterPassDto.getSubEntityName4()).
                                        get(filterPassDto.getFieldName()
                                        ), filterPassDto.getContent()
                        )
                );
            }
        }
        return filterListDeepLv4;
    }

    // deep filter lvl3 predicate list creation
    private Collection<? extends Predicate> buildLvl3DeepFilterPredicateList(Root<?> root) {
        List<Predicate> filterListDeepLv3 = new ArrayList<>();
        for (FilterPassDto filterPassDto : deepFilterlvl3) {
            if (filterPassDto.getContent() instanceof List) {
                List<?> idList = (List<?>) filterPassDto.getContent();
                filterListDeepLv3.add(
                        root.
                                get(filterPassDto.getSubEntityName()).
                                get(filterPassDto.getSubEntityName2()).
                                get(filterPassDto.getSubEntityName3()).
                                get(filterPassDto.getFieldName()).in(idList));

            } else {
                filterListDeepLv3.add(
                        criteriaBuilder.equal(
                                root.
                                        get(filterPassDto.getSubEntityName()).
                                        get(filterPassDto.getSubEntityName2()).
                                        get(filterPassDto.getSubEntityName3()).
                                        get(filterPassDto.getFieldName()
                                        ), filterPassDto.getContent()
                        )
                );
            }
        }
        return filterListDeepLv3;
    }

    // deep filter lvl2 predicate list creation
    private Collection<? extends Predicate> buildLvl2DeepFilterPredicateList(Root<?> root) {
        List<Predicate> filterListDeepLv2 = new ArrayList<>();
        for (FilterPassDto filterPassDto : deepFilterlvl2) {
            if (filterPassDto.getContent() instanceof List) {
                List<?> idList = (List<?>) filterPassDto.getContent();
                filterListDeepLv2.add(
                        root.
                                get(filterPassDto.getSubEntityName()).
                                get(filterPassDto.getSubEntityName2()).
                                get(filterPassDto.getFieldName()).in(idList));

            } else {
                filterListDeepLv2.add(
                        criteriaBuilder.equal(
                                root.
                                        get(filterPassDto.getSubEntityName()).
                                        get(filterPassDto.getSubEntityName2()).
                                        get(filterPassDto.getFieldName()
                                        ), filterPassDto.getContent()
                        )
                );
            }
        }
        return filterListDeepLv2;
    }

    // deep filter lvl1 predicate list creation
    private Collection<? extends Predicate> buildLvl1DeepFilterPredicateList(Root<?> root) {
        List<Predicate> filterListDeepLv1 = new ArrayList<>();
        for (FilterPassDto filterPassDto : deepFilterlvl1) {
            if (filterPassDto.getContent() instanceof List) {
                List<?> idList = (List<?>) filterPassDto.getContent();
                filterListDeepLv1.add(
                        root.
                                get(filterPassDto.getSubEntityName()).
                                get(filterPassDto.getFieldName()).in(idList));
            } else {
                filterListDeepLv1.add(
                        criteriaBuilder.equal(
                                root.
                                        get(filterPassDto.getSubEntityName()).
                                        get(filterPassDto.getFieldName()
                                        ), filterPassDto.getContent()
                        )
                );

            }

        }
        return filterListDeepLv1;
    }

    // field filter predicate list creation
    private Collection<? extends Predicate> buildFiledFilterPredicateList(Root<?> root) {
        List<Predicate> filterList = new ArrayList<>();
        for (FilterPassDto filterPassDto : filterFieldList) {

            if (filterPassDto.getContent() instanceof List) {
                List<?> idList = (List<?>) filterPassDto.getContent();
                filterList.add(root.get(filterPassDto.getFieldName()).in(filterPassDto.getContent()));

//                Predicate[] idPredicates = new Predicate[idList.size()];
//                int index = 0;
//                for(Integer id : idList) {
//                    idPredicates[index++] = criteriaBuilder.equal(root.get(filterPassDto.getFieldName()), id);
//                }
//
//                filterListDeepLv1.add(criteriaBuilder.or(idPredicates));
            } else {
                filterList.add(
                        criteriaBuilder.equal(
                                root.
                                        get(filterPassDto.getFieldName()
                                        ), filterPassDto.getContent()
                        )
                );
            }

        }
        return filterList;
    }

    /*
    build search predicates
     */
    private List<Predicate> buildSearchPredicates(Field defaultSearchField, List<Field> entityList, Root<?> root) {
        log.info("________________________________");
        log.info("Query search with! >>");
        List<Predicate> searchPredicateList = new ArrayList<>();
        // if every search passers size = 0 enable default search field
        if (searchFieldList.size() == 0 && deepSearchlvl1.size() == 0 && deepSearchlvl2.size() == 0 && deepSearchlvl3.size() == 0 && deepSearchlvl4.size() == 0 && deepSearchlvl5.size() == 0) {
            searchPredicateList.add(defaultSearchPredicate(defaultSearchField, root));
        } else {
            // Normal search field lvl 0
            if (searchFieldList.size() != 0) {
                searchPredicateList.addAll(buildFiledSearchPredicateList(root));
            }
            // Level 01 deep search call
            if (deepSearchlvl1.size() != 0 && entityList.size() != 0) {
                searchPredicateList.addAll(buildLvl1DeepSearchPredicateList(root));
            }
            // Level 02 deep search call
            if (deepSearchlvl2.size() != 0 && entityList.size() != 0) {
                searchPredicateList.addAll(buildLvl2DeepSearchPredicateList(root));
            }
            // Level 03 deep search call
            if (deepSearchlvl3.size() != 0 && entityList.size() != 0) {
                searchPredicateList.addAll(buildLvl3DeepSearchPredicateList(root));
            }
            // Level 04 deep search call
            if (deepSearchlvl4.size() != 0 && entityList.size() != 0) {
                searchPredicateList.addAll(buildLvl4DeepSearchPredicateList(root));
            }
            // Level 05 deep search call
            if (deepSearchlvl5.size() != 0 && entityList.size() != 0) {
                searchPredicateList.addAll(buildLvl5DeepSearchPredicateList(root));
            }
        }
        // available variables for search
        if (parentEntityFieldsArrayWithoutRedundant.size() != 0) {
            log.info("________________________________");
            log.info("Available variable names for search >>");
            for (Field variable : parentEntityFieldsArrayWithoutRedundant) {
                log.info(variable.getName() + " : " + variable.getType().getSimpleName());
            }
        }
        log.info("________________________________");
        return searchPredicateList;
    }

    // default search predicate creation
    private Predicate defaultSearchPredicate(Field defaultSearchField, Root<?> root) {
        Predicate defaultPredicate = criteriaBuilder.like(criteriaBuilder.lower(root.get(defaultSearchField.getName()).as(String.class)), "%" + search.toLowerCase() + "%");
        log.info(defaultSearchField.getName() + " : " + defaultSearchField.getType().getSimpleName() + " [by default]");
        // remove added field from parentEntityFieldsArray
        parentEntityFieldsArrayWithoutRedundant.remove(defaultSearchField);
        return defaultPredicate;
    }

    // deep search lvl5 predicate list creation
    private Collection<? extends Predicate> buildLvl5DeepSearchPredicateList(Root<?> root) {
        List<Predicate> searchListDeepLv5 = new ArrayList<>();
        for (SearchPassDto searchPassDto : deepSearchlvl5) {
            searchListDeepLv5.add(
                    criteriaBuilder.like(
                            criteriaBuilder.lower(
                                    root.
                                            get(searchPassDto.getSubEntityName()).
                                            get(searchPassDto.getSubEntityName2()).
                                            get(searchPassDto.getSubEntityName3()).
                                            get(searchPassDto.getSubEntityName4()).
                                            get(searchPassDto.getSubEntityName5()).
                                            get(searchPassDto.getFieldName())
                                            .as(String.class)), "%" + search.toLowerCase() + "%")
            );
        }
        return searchListDeepLv5;
    }

    // deep search lvl4 predicate list creation
    private Collection<? extends Predicate> buildLvl4DeepSearchPredicateList(Root<?> root) {
        List<Predicate> searchListDeepLv4 = new ArrayList<>();
        for (SearchPassDto searchPassDto : deepSearchlvl4) {
            searchListDeepLv4.add(
                    criteriaBuilder.like(
                            criteriaBuilder.lower(
                                    root.
                                            get(searchPassDto.getSubEntityName()).
                                            get(searchPassDto.getSubEntityName2()).
                                            get(searchPassDto.getSubEntityName3()).
                                            get(searchPassDto.getSubEntityName4()).
                                            get(searchPassDto.getFieldName())
                                            .as(String.class)), "%" + search.toLowerCase() + "%")
            );
        }
        return searchListDeepLv4;
    }

    // deep search lvl3 predicate list creation
    private Collection<? extends Predicate> buildLvl3DeepSearchPredicateList(Root<?> root) {
        List<Predicate> searchListDeepLv3 = new ArrayList<>();
        for (SearchPassDto searchPassDto : deepSearchlvl3) {
            searchListDeepLv3.add(
                    criteriaBuilder.like(
                            criteriaBuilder.lower(
                                    root.
                                            get(searchPassDto.getSubEntityName()).
                                            get(searchPassDto.getSubEntityName2()).
                                            get(searchPassDto.getSubEntityName3()).
                                            get(searchPassDto.getFieldName())
                                            .as(String.class)), "%" + search.toLowerCase() + "%")
            );
        }
        return searchListDeepLv3;
    }

    // deep search lvl2 predicate list creation
    private Collection<? extends Predicate> buildLvl2DeepSearchPredicateList(Root<?> root) {
        List<Predicate> searchListDeepLv2 = new ArrayList<>();
        for (SearchPassDto searchPassDto : deepSearchlvl2) {
            searchListDeepLv2.add(
                    criteriaBuilder.like(
                            criteriaBuilder.lower(
                                    root.
                                            get(searchPassDto.getSubEntityName()).
                                            get(searchPassDto.getSubEntityName2()).
                                            get(searchPassDto.getFieldName())
                                            .as(String.class)), "%" + search.toLowerCase() + "%")
            );
        }
        return searchListDeepLv2;
    }

    // deep search lvl1 predicate list creation
    private Collection<? extends Predicate> buildLvl1DeepSearchPredicateList(Root<?> root) {
        List<Predicate> searchListDeepLv1 = new ArrayList<>();
        for (SearchPassDto searchPassDto : deepSearchlvl1) {
            searchListDeepLv1.add(
                    criteriaBuilder.like(
                            criteriaBuilder.lower(
                                    root.
                                            get(searchPassDto.getSubEntityName()).
                                            get(searchPassDto.getFieldName())
                                            .as(String.class)), "%" + search.toLowerCase() + "%")
            );
        }
        return searchListDeepLv1;
    }

    /**********************************************************************************************************/

    // normal search predicate list creation (lvl 0)
    private Collection<? extends Predicate> buildFiledSearchPredicateList(Root<?> root) {
        List<Predicate> searchList = new ArrayList<>();
        for (Iterator<Field> fieldIterator = parentEntityFieldsArrayWithoutRedundant.iterator(); fieldIterator.hasNext(); ) {
            Field variable = fieldIterator.next();
            // matching fields with searchFieldList and add to where clause
            for (Iterator<SearchPassDto> searchPassDtoIterator = searchFieldList.iterator(); searchPassDtoIterator.hasNext(); ) {
                SearchPassDto searchPassDto = searchPassDtoIterator.next();
                if (variable.getName().equals(searchPassDto.getFieldName())) {
                    log.info(variable.getName() + " : " + variable.getType().getSimpleName());
                    searchList.add(
                            criteriaBuilder.like(criteriaBuilder.lower(root.get(variable.getName()).as(String.class)), "%" + search.toLowerCase() + "%")
                    );
                    fieldIterator.remove();
                    searchPassDtoIterator.remove();
                }
            }
        }
        return searchList;
    }

    /**
     * commonly used functions
     *
     * @param entityClass
     * @return
     */

    // drop redundant fields
    private List<Field> dropRedundantVariablesInsideEntity(Class<?> entityClass) {
        log.info("________________________________");
        // store redundant
        List<String> redundant = new ArrayList<>();
        List<Field> fieldsArray = new ArrayList<>(Arrays.asList(entityClass.getDeclaredFields()));
        for (Iterator<Field> fieldIterator = fieldsArray.iterator(); fieldIterator.hasNext(); ) {
            Field variable = fieldIterator.next();
            // first drop redundant variables
            if (variable.getType().getSimpleName().equals("List")) {
                redundant.add(variable.getName());
                fieldIterator.remove();
            } else if (variable.getName().equals("serialVersionUID")) {
                redundant.add(variable.getName());
                fieldIterator.remove();
            } else if (variable.getName().equals("isActive")) {
                // this entity has is active column
                isActiveColumn = true;
                redundant.add(variable.getName());
                fieldIterator.remove();
            }
        }
        // redundant information (only for show)
        if (redundant.size() != 0) {
            log.info("________________________________");
            log.info("Redundant [removed] >>");
            for (String s : redundant) {
                log.info(s);
            }
        }
        return fieldsArray;
    }
    /**********************************************************************************************************/

    /**********************************************************************************************************/

    // entity checker
    private List<Field> checkEntityHasEntities(Class<?> entityClass) {
        log.info("________________________________");
        // entity checker
        //  if we got an Entity inside from current entityClass, if found, add it to entityList and remove it from variableArray
        List<Field> fieldsArray = new ArrayList<>(Arrays.asList(entityClass.getDeclaredFields()));
        ArrayList<Field> entityList = new ArrayList<>();
        for (Iterator<Field> filedsIterator = fieldsArray.iterator(); filedsIterator.hasNext(); ) {
            Field variable = filedsIterator.next();
            // find has it @Entity and confirm it's an Entity
            Annotation[] anoList = variable.getType().getAnnotations();
            for (Annotation ano : anoList) {
                String[] anoEn = ano.toString().split("[ .,()]+");
                for (String s : anoEn) {
                    if (s.equals("Entity")) {
                        log.info("Entity Found: " + variable.getName());
                        entityList.add(variable);
                        filedsIterator.remove();
                    }
                }
            }
        }
        if (entityList.size() != 0) {
            log.info("________________________________");
        }
        return entityList;
    }
    /**********************************************************************************************************/

    /**
     * @param criteriaQuery
     * @param sort
     * @param direction
     * @param root
     * @Doc Order by
     */
    private void setOrder(CriteriaQuery<?> criteriaQuery, String sort, String direction, Root<?> root) {
        if (direction.equalsIgnoreCase("asc")) {
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sort)));
        } else {
            criteriaQuery.orderBy(criteriaBuilder.desc(root.get(sort)));
        }
    }
    /**********************************************************************************************************/

    /**
     * @param page
     * @param per_page
     * @param sort
     * @param direction
     * @return
     * @Doc Sort & Pageable
     */
    private Pageable getPageable(int page, Integer per_page, String sort, String direction) {
        Sort sort1 = Sort.by(direction, sort);
        return PageRequest.of(page, per_page, sort1);
    }
    /**********************************************************************************************************/


    /**********************************************************************************************************/
    /**********************************************************************************************************/
    /**********************************************************************************************************/

    /**
     * @param predicate
     * @param entityClass
     * @return
     * @Doc Get count
     */
    private long getCount(Predicate predicate, Class<?> entityClass) {
        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        Root<?> countRoot = countQuery.from(entityClass);
        countQuery.select(criteriaBuilder.count(countRoot)).where(predicate);
        return entityManager.createQuery(countQuery).getSingleResult();
    }

    /**
     * @Refer: Outdated methods (Not use)
     */

    // match and return entityList.Type = reCreatedClazz.Name
    private String matchTypeAndFieldType(Field entityFromEntityList, Class<?> reCreatedClazz) {
        if (entityFromEntityList.getType().getSimpleName().equals(reCreatedClazz.getName()))
            return entityFromEntityList.getName();
        else
            return null;
    }

    /**********************************************************************************************************/
    /**********************************************************************************************************/
    /**********************************************************************************************************/

}
