package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMCompanyDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsMCompanyDetailDao extends JpaRepository<UsMCompanyDetail, Integer> {
}
