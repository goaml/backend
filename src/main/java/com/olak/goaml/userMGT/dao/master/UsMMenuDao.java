package com.olak.goaml.userMGT.dao.master;

import com.olak.goaml.userMGT.models.master.UsMMenu;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsMMenuDao extends JpaRepository<UsMMenu, Integer> {

    @Query("SELECT m FROM UsMMenu m WHERE m.usRStatusDetail.statusId = ?1")
    Page<UsMMenu> findByStatusId(Integer status, Boolean value, PageRequest of);

    @Query("SELECT m FROM UsMMenu m WHERE m.menuName LIKE %?1% OR cast(m.menuId string) LIKE %?1%")
    Page<UsMMenu> getAllMenu(String search, Boolean value, PageRequest of);

}