package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.reference.UsRUserBranch;
import com.olak.goaml.userMGT.models.reference.UsRUserIdType;
import com.olak.goaml.userMGT.models.transaction.UsTActivityLog;
import com.olak.goaml.userMGT.models.transaction.UsTUserNavigation;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Builder
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@EqualsAndHashCode
@Table(name = "AD_M_USER", indexes = {
        @Index(name = "USER_NAME_UNIQUE", columnList = "USER_NAME", unique = true),
        @Index(name = "fk_AD_M_USER_US_M_USER_ROLE1_idx", columnList = "USER_ROLE_ID"),
        @Index(name = "fk_AD_M_USER_US_R_STATUS_DETAILS1_idx", columnList = "STATUS_ID"),
        @Index(name = "fk_AD_M_USER_US_M_BRANCH1_idx", columnList = "BRANCH_ID"),
        @Index(name = "fk_AD_M_USER_US_M_DEPARTMENT1_idx", columnList = "DEPARTMENT_ID"),
        @Index(name = "fk_AD_M_USER_US_R_USER_ID_TYPE1_idx", columnList = "USER_TYPE_ID")
})
public class AdMUser extends AuditModel implements Serializable {

    private static final long serialVersionUID = -676439320146269469L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AD_M_USER")
    @SequenceGenerator(sequenceName = "AD_M_USER_SEQ", allocationSize = 1, name = "AD_M_USER")
    @Column(name = "USER_ID", nullable = false)
    private Integer userId;
    @Column(name = "USER_NAME", nullable = false)
    private String userName;
    @Column(name = "DESCRIPTION", length = 50)
    private String description;
    @Column(name = "IS_FIRST_LOGIN")
    private Boolean isFirstLogin;
    @Column(name = "IS_ACTIVE", nullable = false)
    private Boolean isActive;
    @Column(name = "EMP_EMAIL", length = 255)
    private String email;
    @Column(name = "EMP_MOBILE_NUMBER", length = 11)
    private String mobileNo;
    @Column(name = "USER_EXPIRY_DATE")
    private LocalDate userExpDate;
    @Column(name = "PASSWORD_EXPIRY_DATE")
    private LocalDate passwordExpDate;
    @Column(name = "FIRST_NAME", length = 50)
    private String firstName;
    @Column(name = "LAST_NAME", length = 50)
    private String lastName;
    @Column(name = "PASSWORD", length = 255)
    private String password;
    @Column(name = "OTP", length = 6)
    private String otp;
    @Column(name = "OTP_GENERATED_TIME")
    private LocalTime otpGeneratedTime;

    //bi-directional many-to-one association to UsMUserRole
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ROLE_ID", referencedColumnName = "USER_ROLE_ID", nullable = false)
    private UsMUserRole usMUserRole;

    //bi-directional many-to-one association to UsRStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

    //bi-directional many-to-one association to UsMBranch
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRANCH_ID", referencedColumnName = "BRANCH_ID", nullable = false)
    private UsMBranch usMBranch;

    //bi-directional many-to-one association to UsMBranch
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTMENT_ID", referencedColumnName = "DEPARTMENT_ID", nullable = false)
    private UsMDepartment usMDepartment;

    @Column(name = "SECURE_KEY", length = 100)
    private String secureKey;

    @Column(name = "NIC", length = 12)
    private String nic;

    @Column(name = "REFERENCE_NO", length = 50)
    private String referenceNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_TYPE_ID", referencedColumnName = "USER_TYPE_ID", nullable = false)
    private UsRUserIdType usRUserIdType;

    @Column(name = "COM_ID")
    private Integer comId;

    @Column(name = "IS_EXISTING_COMPANY")
    private Boolean isExistingCompany;

    @OneToMany(mappedBy = "adMUser", fetch = FetchType.LAZY)
    private List<UsTUserNavigation> usTUserNavigations;

    @OneToMany(mappedBy = "adMUser", fetch = FetchType.LAZY)
    private List<UsTActivityLog> usTActivityLogs;

    @OneToMany(mappedBy = "adMUser", fetch = FetchType.LAZY)
    private List<UsRUserBranch> usRUserBranches;

}