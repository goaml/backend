package com.olak.goaml.userMGT.models.reference;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.master.*;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "US_R_STATUS_DETAILS", indexes = {
        @Index(name = "STATUS_CODE_UNIQUE", columnList = "STATUS_CODE")
})
public class UsRStatusDetail extends AuditModel implements Serializable {


    private static final long serialVersionUID = -6263612501160945081L;
    @Id
    @SequenceGenerator(name = "US_R_STATUS_DETAILS", allocationSize = 1, sequenceName = "US_R_STATUS_DETAILS_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_R_STATUS_DETAILS")
    @Column(name = "STATUS_ID", nullable = false, columnDefinition = "number(8)")
    private Integer statusId;

    @Column(name = "STATUS_CODE", unique = true, length = 5)
    private String statusCode;

    @Column(name = "STATUS_DESC", length = 255)
    private String statusDesc;

    @Column(name = "STATUS_NAME", length = 255)
    private String statusName;

    //bi-directional many-to-one association to UsMAction
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsMAction> usMActions;

    //bi-directional many-to-one association to UsMBranch
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsMBranch> usMBranches;

    //bi-directional many-to-one association to UsMDepartment
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsMDepartment> usMDepartments;

    //bi-directional many-to-one association to UsMMenu
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsMMenu> usMMenus;

    //bi-directional many-to-one association to UsMMenuAllowedMap
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsMMenuAllowedMap> usMMenuAllowedMaps;

    //bi-directional many-to-one association to UsMModule
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsMModule> usMModules;

    //bi-directional many-to-one association to UsMUserRestriction
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsMUserRestriction> usMUserRestrictions;

    //bi-directional many-to-one association to UsMUserRole
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsMUserRole> usMUserRoles;

    //bi-directional many-to-one association to UsMUserRoleLimitMgt
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsMUserRoleLimitMgt> usMUserRoleLimitMgts;

    //bi-directional many-to-one association to UsRUserBranch
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsRUserBranch> usRUserBranches;

    //bi-directional many-to-one association to UsTMenuAction
    @OneToMany(mappedBy = "usRStatusDetail")
    private List<UsTMenuAction> usTMenuActions;

    @OneToMany(mappedBy = "usRStatusDetail")
    private List<AdMUser> adMUsers;
}