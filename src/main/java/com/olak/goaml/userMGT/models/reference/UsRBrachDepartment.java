package com.olak.goaml.userMGT.models.reference;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.master.UsMBranch;
import com.olak.goaml.userMGT.models.master.UsMDepartment;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_R_BRACH_DEPARTMENT", indexes = {
        @Index(name = "fk_US_R_BRACH_DEPARTMENT_US_M_BRANCH1_idx", columnList = "BRANCH_ID"),
        @Index(name = "fk_US_R_BRACH_DEPARTMENT_US_M_DEPARTMENT1_idx", columnList = "DEPARTMENT_ID")
})
public class UsRBrachDepartment extends AuditModel implements Serializable {

    private static final long serialVersionUID = 5082305223032200432L;

    @Id
    @SequenceGenerator(name = "US_R_BRACH_DEPARTMENT", sequenceName = "US_R_BRACH_DEPARTMENT_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_R_BRACH_DEPARTMENT")
    @Column(name = "BRACH_DEPT_ID")
    private Integer brachDeptId;

    //bi-directional many-to-one association to UsMBranch
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRANCH_ID", referencedColumnName = "BRANCH_ID", nullable = false)
    private UsMBranch usMBranch;

    //bi-directional many-to-one association to UsMDepartment
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTMENT_ID", referencedColumnName = "DEPARTMENT_ID", nullable = false)
    private UsMDepartment usMDepartment;

    //bi-directional many-to-one association to UsRUserBranch
    @OneToMany(mappedBy = "usRBrachDepartment")
    private List<UsRUserBranch> usRUserBranches;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

}
