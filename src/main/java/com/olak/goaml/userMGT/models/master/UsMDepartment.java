package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRBrachDepartment;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_M_DEPARTMENT", indexes = {
        @Index(name = "fk_US_M_DEPARTMENT_US_R_STATUS_DETAIL1_idx", columnList = "STATUS_ID")
})
public class UsMDepartment extends AuditModel implements Serializable {

    private static final long serialVersionUID = 4786741241288252324L;
    @Id
    @SequenceGenerator(name = "US_M_DEPARTMENT", sequenceName = "US_M_DEPARTMENT_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_DEPARTMENT")
    @Column(name = "DEPARTMENT_ID", nullable = false)
    private Integer departmentId;

    @Column(name = "CONTACT_NUMBER", length = 11)
    private String contactNumber;

    @Column(name = "DEPT_ADDRESS", length = 100)
    private String deptAddress;

    @Column(name = "DEPT_LOCATION", length = 100)
    private String deptLocation;

    @Column(name = "DEPT_NAME", length = 100)
    private String deptName;

    @Column(name = "DESCRIPTION", length = 255)
    private String desc;

    @Column(name = "EMAIL", length = 60)
    private String email;

    @Column(name = "BRANCH_ID", length = 5)
    private Integer branchId;

    //bi-directional many-to-one association to UsRUserStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

    //bi-directional many-to-one association to UsRBrachDepartment
    @OneToMany(mappedBy = "usMDepartment")
    private List<UsRBrachDepartment> usRBrachDepartments;

    @OneToMany(mappedBy = "usMDepartment")
    private List<AdMUser> adMUsers;

}