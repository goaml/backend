package com.olak.goaml.userMGT.models.reference;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "US_R_LOGIN_TYPE")
public class UsRLoginType extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_R_LOGIN_TYPE")
    @SequenceGenerator(sequenceName = "US_R_LOGIN_TYPE_SEQ", allocationSize = 1, name = "US_R_LOGIN_TYPE")
    @Column(name = "LOGIN_TYPE_ID", nullable = false)
    private Integer loginTypeId;

    @Column(name = "TYPE_CODE", nullable = false, length = 15)
    private String typeCode;

    @Column(name = "TYPE_DESCRIPTION", length = 100)
    private String typeDescription;

    @Column(name = "IS_ACTIVE", nullable = false)
    private Boolean isActive;

}
