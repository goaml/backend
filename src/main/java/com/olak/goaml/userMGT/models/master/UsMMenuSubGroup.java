package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "US_M_MENU_SUB_GROUP", indexes = {
        @Index(name = "fk_US_M_MENU_SUB_GROUP_US_M_MENU_GROUP1_idx", columnList = "MENU_GROUP_ID"),
})
public class UsMMenuSubGroup extends AuditModel {

    @Id
    @SequenceGenerator(name = "US_M_MENU_SUB_GROUP", allocationSize = 1, sequenceName = "US_M_MENU_SUB_GROUP_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_MENU_SUB_GROUP")
    @Column(name = "MENU_SUB_GROUP_ID", nullable = false)
    private Integer subGroupId;

    @Column(name = "MENU_SUB_GROUP_NAME", length = 255)
    private String subGroupName;

    @Column(name = "SEQ_NO")
    private Integer seqNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_GROUP_ID", referencedColumnName = "MENU_GROUP_ID", nullable = false)
    private UsMMenuGroup usMMenuGroup;

    @OneToMany(mappedBy = "usMMenuSubGroup")
    private List<UsMMenu> usMMenus;

}