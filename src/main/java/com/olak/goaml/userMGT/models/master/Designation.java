package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "DESIGNATION")
public class Designation extends AuditModel implements Serializable {

    private static final long serialVersionUID = 878317489358074717L;

    @Id
    @SequenceGenerator(name = "DESIGNATION", allocationSize = 1, sequenceName = "DESIGNATION_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DESIGNATION")
    @Column(name = "DESIGNATION_ID", nullable = false)
    private Integer designationId;

    @Column(name = "DESIGNATION_NAME", length = 45)
    private String designationName;

}