package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "US_M_SCREEN_VIEW_MGT", indexes = {
        @Index(name = "fk_US_M_SCREEN_VIEW_MGT_US_M_SCREEN_ATTRIBUTES1_idx", columnList = "US_M_SCREEN_ATTRIBUTES_ID"),
        @Index(name = "fk_US_M_SCREEN_VIEW_MGT_US_T_MENU_ACTION1_idx", columnList = "MENU_ACTION_ID")
})
public class UsMScreenViewMgt extends AuditModel implements Serializable {

    private static final long serialVersionUID = -2876559333574843861L;
    @Id
    @SequenceGenerator(name = "US_M_SCREEN_VIEW_MGT", allocationSize = 1, sequenceName = "US_M_SCREEN_VIEW_MGT_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_SCREEN_VIEW_MGT")
    @Column(name = "US_M_SCREEN_VIEW_ID", nullable = false, columnDefinition = "number(20)")
    private Integer screenViewId;

    @Column(name = "IS_VISIBLE", length = 255)
    private Boolean isVisible;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_ACTION_ID", referencedColumnName = "MENU_ACTION_ID", nullable = false)
    private UsTMenuAction usTMenuAction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "US_M_SCREEN_ATTRIBUTES_ID", referencedColumnName = "US_M_SCREEN_ATTRIBUTES_ID", nullable = false)
    private UsMScreenAttributes usMScreenAttributes;

}
