package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;


/**
 * The persistent class for the us_m_user database table.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "US_M_USER", indexes = {
        @Index(name = "fk_US_M_USER_US_M_USER_ROLE1_idx", columnList = "USER_ROLE_ID"),
        @Index(name = "fk_US_M_USER_US_R_USER_STATUS_DETAILS1_idx", columnList = "STATUS_ID"),
        @Index(name = "fk_US_M_USER_US_M_EMPLOYEE1_idx", columnList = "EMPLOYEE_ID"),
})
public class UsMUser extends AuditModel implements Serializable {

    private static final long serialVersionUID = 4533195585540091735L;
    @Id
    @SequenceGenerator(name = "US_M_USER", allocationSize = 1, sequenceName = "US_M_USER_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_USER")
    @Column(name = "USERID", nullable = false, columnDefinition = "number(8)")
    private Integer userId;

    @Column(name = "COMPANY_ID", length = 255)
    private String companyId;

    @Column(name = "EXPIRE_DATE")
    private LocalDate expireDate;

    @Column(name = "OTP", length = 255)
    private String otp;

    @Column(name = "PASSWORD", length = 100)
    private String password;

    @Column(name = "ROLE_OWNER_ID", columnDefinition = "number(8)")
    private Integer roleOwnerId;

    @Column(name = "USER_APPRV_REASON", length = 255)
    private String userApprvReason;

    @Column(name = "USERNAME", length = 50)
    private String username;

    //bi-directional many-to-one association to UsMEmployee
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "EMPLOYEE_ID", nullable = false)
    private UsMEmployee usMEmployee;

    //bi-directional many-to-one association to UsMUserRole
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ROLE_ID", referencedColumnName = "USER_ROLE_ID", nullable = false)
    private UsMUserRole usMUserRole;

    //bi-directional many-to-one association to UsRStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

//	//bi-directional many-to-one association to UsRUserBranch
//	@OneToMany(mappedBy="usMUser")
//	private List<UsRUserBranch> usRUserBranch;
//
//	//bi-directional many-to-one association to UsTActivityLog
//	@OneToMany(mappedBy="usMUser")
//	private List<UsTActivityLog> usTActivityLogs;
//
//	//bi-directional many-to-one association to UsTUserNavigation
//	@OneToMany(mappedBy="usMUser")
//	private List<UsTUserNavigation> usTUserNavigations;

}