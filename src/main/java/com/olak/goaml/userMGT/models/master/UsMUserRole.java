package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "US_M_USER_ROLE", indexes = {
        @Index(name = "fk_US_M_USER_ROLE_US_R_STATUS_DETAILS1_idx", columnList = "STATUS_ID")
})
public class UsMUserRole extends AuditModel implements Serializable {

    private static final long serialVersionUID = 6927480598375065810L;
    @Id
    @SequenceGenerator(name = "US_M_USER_ROLE", allocationSize = 1, sequenceName = "US_M_USER_ROLE_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_USER_ROLE")
    @Column(name = "USER_ROLE_ID", nullable = false, columnDefinition = "number(8)")
    private Integer userRoleId;

    @Column(name = "C_CODE", columnDefinition = "number(8)")
    private Integer cCode;

    @Column(name = "ROLE_APPRV_REASON", length = 255)
    private String roleApprvReason;

    @Column(name = "ROLE_IN_BUSSINESS", length = 255)
    private String roleInBussiness;

    @Column(name = "STATUS_FLAG", length = 255)
    private String statusFlag;

    @Column(name = "USER_ROLE_DESC", length = 255)
    private String userRoleDesc;

    @Column(name = "USER_ROLE_NAME", length = 255)
    private String userRoleName;

    //bi-directional many-to-one association to UsMMenuAllowedMap
    @OneToMany(mappedBy = "usMUserRole")
    private List<UsMMenuAllowedMap> usMMenuAllowedMaps;

    //bi-directional many-to-one association to UsRStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

    @OneToMany(mappedBy = "usMUserRole")
    private List<AdMUser> adMUsers;

}