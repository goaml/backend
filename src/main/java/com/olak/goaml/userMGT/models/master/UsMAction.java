package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_M_ACTION", indexes = {
        @Index(name = "ACTION_CODE_UNQ", columnList = "ACTION_CODE", unique = true),
        @Index(name = "fk_US_M_ACTION_US_R_STATUS_DETAIL1_idx", columnList = "STATUS_ID")
})
public class UsMAction extends AuditModel implements Serializable {

    private static final long serialVersionUID = 7539459348014363937L;

    @Id
    @SequenceGenerator(name = "US_M_ACTION", sequenceName = "US_M_ACTION_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_ACTION")
    @Column(name = "ACTION_ID", nullable = false)
    private Integer actionId;

    @Column(name = "ACTION_CODE", length = 45, unique = true)
    private String actionCode;

    @Column(name = "ACTION_NAME", length = 45)
    private String actionName;

    //bi-directional many-to-one association to UsRStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

    //bi-directional many-to-one association to UsTMenuAction
    @OneToMany(mappedBy = "usMAction")
    private List<UsTMenuAction> usTMenuActions;

}