package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "GLOBAL_DETAILS")
public class GlobalDetails extends AuditModel implements Serializable {

    private static final long serialVersionUID = 8319039145763708819L;

    @Id
    @SequenceGenerator(name = "GLOBAL_DETAILS", sequenceName = "GLOBAL_DETAILS_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GLOBAL_DETAILS")
    @Column(name = "P_GLOBAL_ID", nullable = false)
    private Integer globalID;

    @Column(name = "P_FRONT_URL")
    private String frontURL;

    @Column(name = "P_DOMAIN_DESCRIPTION")
    private String domainDestription;

    @Column(name = "P_CORE_URL")
    private String coreURL;

    @Column(name = "P_DATE_TIME")
    private LocalDateTime datetime;

    @Column(name = "P_USER_EXP_DAYS")
    private Integer userExpirationDays;

    @Column(name = "P_USER_LOGIN_ATTEMPTS")
    private Integer userLoginAttempts;

    @Column(name = "P_ENCRYPT_KEY")
    private String encryptKey;

    @Column(name = "P_COMPANY_NAME")
    private String companyName;

    @Column(name = "P_FOOTER_TITLE")
    private String footerTitle;

    @Column(name = "P_VERSION_NUM")
    private String versionNumber;

    @Column(name = "P_LOGO_PATH")
    private String logoPath;

    @Column(name = "P_USER_MALE")
    private String userMalePath;

    @Column(name = "P_USER_FEMALE")
    private String userFemalePath;

    @Column(name = "P_STATUS")
    private Integer status;

    @Column(name = "P_LOCAL_TIME")
    private LocalTime plocaltime;

    @Column(name = "P_LOCAL_DATE")
    private LocalDate plocaldate;

    @Column(name = "CREATE_ON")
    private LocalDateTime createOn;

    @Column(name = "LAST_CHANGED")
    private LocalDateTime lastChanged;

    @Column(name = "USER_EXPIRY_DATE")
    private Integer userExpDate;

    @Column(name = "PASSWORD_EXPIRY_DATE")
    private Integer passwordExpDate;

    @Column(name = "RESET_DATE_COUNT")
    private Integer resetDateCount;

    @Column(name = "CURRENT_WORKING_DATE")
    private LocalDate currentWorkingDate;

    @Column(name = "NEXT_WORKING_DATE")
    private LocalDate nextWorkingDate;

    @Column(name = "USER_NOTIFICATION_MODE", length = 1) // S || E || B
    private String userNotificationMode;

    @Column(name = "PWD_MIN_LENGTH")
    private Integer passwordMinLength;

    @Column(name = "PWD_MAX_LENGTH")
    private Integer passwordMaxLength;

    @Column(name = "PWD_NUM_OF_SPECIAL_CHARS")
    private Integer passwordNumOfSpecialCharacter;

    @Column(name = "PWD_NUM_OF_CAPITAL_LETTERS")
    private Integer passwordNumOfCapitalLetters;

    @Column(name = "PWD_NUM_OF_SIMPLE_LETTERS")
    private Integer passwordNumOfSimpleLetters;

    @Column(name = "PWD_NUM_OF_DIGITS")
    private Integer passwordNumOfDigits;

    @Column(name = "IS_MAINTENANCE_ON")
    private Boolean isMaintenanceOn;

    @Column(name = "BANK_NAME")
    private String bankName;
    @Column(name = "SWIFT_CODE")
    private String swiftCode;
    @Column(name = "NON_BANK_INSTITUTION")
    private Boolean nonBankInstitution;
    @Column(name = "REPORT_GEN_DATE")
    private LocalDate reportGenDate;

}
