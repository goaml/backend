package com.olak.goaml.userMGT.models.log;


import com.olak.goaml.audit.AuditModel;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@EqualsAndHashCode
@Table(name = "US_L_ACTIVITY_LOG", indexes = {})
public class UsLActivityLog extends AuditModel implements Serializable {

    private static final long serialVersionUID = -676439320156269132L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_L_ACTIVITY_LOG")
    @SequenceGenerator(sequenceName = "US_L_ACTIVITY_LOG_SEQ", allocationSize = 1, name = "US_L_ACTIVITY_LOG")
    @Column(name = "ACTIVITY_ID", nullable = false)
    private Integer activityId;

    @Column(name = "MENU_ID")
    private Integer menuId;

    @Column(name = "MENU_NAME")
    private String menuName;

    @Column(name = "ACTION_ID")
    private Integer actionId;

    @Column(name = "ACTION_NAME")
    private String actionName;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "SYSTEM_DATE")
    private LocalDate systemDate;

    @Column(name = "CURRUNT_DATE")
    private LocalDate currentDate;

    @Column(name = "SERVER_TIME")
    private LocalTime serverTime;

    @Column(name = "COMPANY_ID")
    private Integer companyId;

    @Column(name = "BRANCH_ID")
    private Integer branchId;

    @Column(name = "BRANCH_NAME")
    private String branchName;

    @Column(name = "DEPT_ID")
    private Integer deptId;

    @Column(name = "DEPT_NAME")
    private String deptName;

    @Column(name = "KEY_VALUE_ID")
    private Integer keyValueId;

    @Column(name = "KEY_VALUE")
    private String keyValue;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "IS_ACTIVE", nullable = false)
    private Boolean isActive;

}
