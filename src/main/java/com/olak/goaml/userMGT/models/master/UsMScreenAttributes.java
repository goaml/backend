package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "US_M_SCREEN_ATTRIBUTES", indexes = {
        @Index(name = "fk_US_M_SCREEN_ATTRIBUTES_US_T_MENU_ACTION1_idx", columnList = "MENU_ACTION_ID")
})
public class UsMScreenAttributes extends AuditModel implements Serializable {

    private static final long serialVersionUID = -131816991047984202L;

    @Id
    @SequenceGenerator(name = "US_M_SCREEN_ATTRIBUTES", allocationSize = 1, sequenceName = "US_M_SCREEN_ATTRIBUTES_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_SCREEN_ATTRIBUTES")
    @Column(name = "US_M_SCREEN_ATTRIBUTES_ID", nullable = false)
    private Integer screenAttributeId;

    @Column(name = "ATTRIBUTE_NAME", length = 255)
    private String attributeName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_ACTION_ID", referencedColumnName = "MENU_ACTION_ID", nullable = false)
    private UsTMenuAction usTMenuAction;

    @OneToMany(mappedBy = "usMScreenAttributes")
    private List<UsMScreenViewMgt> usMScreenViewMgt;

}
