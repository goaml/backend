package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the us_m_address database table.
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_M_ADDRESS", indexes = {
        @Index(name = "fk_US_M_ADDRESS_US_M_EMPLOYEE1_idx", columnList = "EMPLOYEE_ID")
})
public class UsMAddress extends AuditModel implements Serializable {

    private static final long serialVersionUID = 5557855236405826035L;

    @Id
    @SequenceGenerator(name = "US_M_ADDRESS", sequenceName = "US_M_ADDRESS_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_ADDRESS")
    @Column(name = "ADDRESS_ID", nullable = false)
    private Integer addressId;

    @Column(name = "AD_NUM", length = 45)
    private String adNum;

    @Column(name = "ADDRESS_LINE_1", length = 45)
    private String addressLine1;

    @Column(name = "ADDRESS_LINE_2", length = 45)
    private String addressLine2;

    @Column(name = "ADDRESS_LINE_3", length = 45)
    private String addressLine3;

    @Column(name = "ADDRESS_TYPE", length = 45)
    private String addressType;

    @Column(name = "CITY", length = 45)
    private String city;

    //bi-directional many-to-one association to UsMUser
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMPLOYEE_ID", nullable = false, referencedColumnName = "EMPLOYEE_ID")
    private UsMEmployee usMEmployee;

}