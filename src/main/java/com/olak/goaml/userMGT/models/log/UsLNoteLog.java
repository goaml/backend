package com.olak.goaml.userMGT.models.log;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.master.*;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@EqualsAndHashCode
@Table(name = "US_L_NOTE_LOG", indexes = {
        @Index(name = "fk_US_L_NOTE_LOG_AD_M_USER1_idx", columnList = "USER_ID"),
        @Index(name = "fk_US_L_NOTE_LOG_US_M_MENU1_idx", columnList = "MENU_ID"),
        @Index(name = "fk_US_L_NOTE_LOG_US_M_ACTION1_idx", columnList = "ACTION_ID"),
        @Index(name = "fk_US_L_NOTE_LOG_US_M_BRANCH1_idx", columnList = "BRANCH_ID"),
        @Index(name = "fk_US_L_NOTE_LOG_US_M_DEPARTMENT1_idx", columnList = "DEPARTMENT_ID")
})
public class UsLNoteLog extends AuditModel implements Serializable {

    private static final long serialVersionUID = -4436449400895830167L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_L_NOTE_LOG")
    @SequenceGenerator(sequenceName = "US_L_NOTE_LOG_SEQ", allocationSize = 1, name = "US_L_NOTE_LOG")
    @Column(name = "NOTE_ID", nullable = false)
    private Integer noteId;

    @Column(name = "MENU_NAME")
    private String menuName;

    @Column(name = "ACTION_NAME")
    private String actionName;

    @Column(name = "USR_NAME")
    private String username;

    @Column(name = "SYSTEM_DATE")
    private LocalDate systemDate;

    @Column(name = "CURRENT_LOCAL_DATE")
    private LocalDate currentDate;

    @Column(name = "CURRENT_LOCAL_TIME")
    private LocalTime time;

    @Column(name = "COMPANY_ID")
    private String companyId;

    @Column(name = "BRANCH_NAME")
    private String branchName;

    @Column(name = "DEPARTMENT_NAME")
    private String departmentName;

    @Column(name = "KEY_VALUE_ID")
    private Integer keyValueId;

    @Column(name = "KEY_VALUE")
    private String keyValue;

    @Column(name = "COMMENT_MSG")
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    private AdMUser adMUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_ID", referencedColumnName = "MENU_ID", nullable = false)
    private UsMMenu usMMenu;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTION_ID", referencedColumnName = "ACTION_ID", nullable = false)
    private UsMAction usMAction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRANCH_ID", referencedColumnName = "BRANCH_ID", nullable = false)
    private UsMBranch usMBranch;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEPARTMENT_ID", referencedColumnName = "DEPARTMENT_ID", nullable = false)
    private UsMDepartment usMDepartment;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

}