package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the us_m_user_limit_mgt database table.
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_M_USER_LIMIT_MGT", indexes = {
        @Index(name = "fk_US_M_USER_LIMIT_MGT_US_R_USER_STATUS_DETAILS1_idx", columnList = "STATUS_ID"),
        @Index(name = "fk_US_M_USER_LIMIT_MGT_US_M_USER1_idx", columnList = "USERID"),
        @Index(name = "fk_US_M_USER_LIMIT_MGT_US_M_MENU1_idx", columnList = "MENU_ID")
})
public class UsMUserLimitMgt extends AuditModel implements Serializable {

    private static final long serialVersionUID = -5300370673683893489L;
    @Id
    @SequenceGenerator(name = "US_M_USER_LIMIT_MGT", sequenceName = "US_M_USER_LIMIT_MGT_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_USER_LIMIT_MGT")
    @Column(name = "USER_LIMIT_ID", nullable = false)
    private Integer userLimitId;

    @Column(name = "USER_LIMIT")
    private Double limit;

    //bi-directional many-to-one association to UsMMenu
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_ID", referencedColumnName = "MENU_ID", nullable = false)
    private UsMMenu usMMenu;

    //bi-directional many-to-one association to UsMUser
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERID", referencedColumnName = "USERID", nullable = false)
    private UsMUser usMUser;

    //bi-directional many-to-one association to UsRUserStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

}