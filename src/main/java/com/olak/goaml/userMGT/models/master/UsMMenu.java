package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import com.olak.goaml.userMGT.models.transaction.UsTUserNavigation;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "US_M_MENU", indexes = {
        @Index(name = "fk_US_M_MENU_US_R_STATUS_DETAILS1_idx", columnList = "STATUS_ID"),
        @Index(name = "fk_US_M_MENU_US_M_MENU_SUB_GROUP1_idx", columnList = "MENU_SUB_GROUP_ID"),
})
public class UsMMenu extends AuditModel implements Serializable {

    private static final long serialVersionUID = -4440112473575660353L;
    @Id
    @SequenceGenerator(name = "US_M_MENU", allocationSize = 1, sequenceName = "US_M_MENU_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_MENU")
    @Column(name = "MENU_ID", nullable = false, columnDefinition = "number(8)")
    private Integer menuId;

    @Column(name = "MENU_DESC", length = 255)
    private String menuDesc;

    @Column(name = "MENU_NAME", length = 255)
    private String menuName;

    @Column(name = "MENU_URL", length = 255)
    private String menuUrl;

    @Column(name = "MENU_ICON", length = 255)
    private String menuIcon;

    @Column(name = "SEQ_NO")
    private Integer seqNo;

    //bi-directional many-to-one association to UsRUserStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

    //bi-directional many-to-one association to UsMMenuSubGroup
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_SUB_GROUP_ID", referencedColumnName = "MENU_SUB_GROUP_ID", nullable = false)
    private UsMMenuSubGroup usMMenuSubGroup;

    //bi-directional many-to-one association to UsTMenuAction
    @OneToMany(mappedBy = "usMMenu")
    private List<UsTMenuAction> usTMenuActions;

    //bi-directional many-to-one association to UsTUserNavigation
    @OneToMany(mappedBy = "usMMenu")
    private List<UsTUserNavigation> usTUserNavigations;

}