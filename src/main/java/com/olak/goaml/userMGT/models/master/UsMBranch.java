package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRBrachDepartment;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_M_BRANCH", indexes = {
        @Index(name = "fk_US_M_BRANCH1_US_R_STATUS_DETAIL1_idx", columnList = "STATUS_ID")
})
public class UsMBranch extends AuditModel implements Serializable {
    private static final long serialVersionUID = -8070077908214886793L;
    @Id
    @SequenceGenerator(name = "US_M_BRANCH", allocationSize = 1, sequenceName = "US_M_BRANCH_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_BRANCH")
    @Column(name = "BRANCH_ID", nullable = false)
    private Integer branchId;

    @Column(name = "BRANCH_ADDRESS", length = 255)
    private String branchAddress;

    @Column(name = "BRANCH_LOCATION")
    private String branchLocation;

    @Column(name = "BRANCH_NAME", length = 255)
    private String branchName;

    @Column(name = "CONTACT_NUMBER", length = 11)
    private String contactNumber;

    @Column(name = "DESCRIPTION", length = 255)
    private String desc;

    @Column(name = "EMAIL", length = 60)
    private String email;

    @Column(name = "LIMIT_AMT")
    private BigDecimal limitAmt;

    //bi-directional many-to-one association to UsRStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID")
    private UsRStatusDetail usRStatusDetail;

    //bi-directional many-to-one association to UsMUserRoleLimitMgt
    @OneToMany(mappedBy = "usMBranch")
    private List<UsMUserRoleLimitMgt> usMUserRoleLimitMgts;

    //bi-directional many-to-one association to UsRBrachDepartment
    @OneToMany(mappedBy = "usMBranch")
    private List<UsRBrachDepartment> usRBrachDepartments;

    //bi-directional many-to-one association to AdMUser
    @OneToMany(mappedBy = "usMBranch")
    private List<AdMUser> adMUsers;

}