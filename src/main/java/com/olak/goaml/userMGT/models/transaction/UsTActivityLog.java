package com.olak.goaml.userMGT.models.transaction;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.master.AdMUser;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "US_T_ACTIVITY_LOG", indexes = {
        @Index(name = "fk_US_T_ACTIVITY_LOG_AD_M_USER1_idx", columnList = "USER_ID"),
        @Index(name = "fk_US_T_ACTIVITY_LOG_US_T_MENU_ACTION1_idx", columnList = "MENU_ACTION_ID")
})
public class UsTActivityLog extends AuditModel implements Serializable {


    private static final long serialVersionUID = -4655142774395971723L;
    @Id
    @SequenceGenerator(name = "US_T_ACTIVITY_LOG", allocationSize = 1, sequenceName = "US_T_ACTIVITY_LOG_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_T_ACTIVITY_LOG")
    @Column(name = "ACTIVITY_LOG_ID", nullable = false, columnDefinition = "number(8)")
    private Integer activityLogId;

    //bi-directional many-to-one association to UsMUser
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    private AdMUser adMUser;

    //bi-directional many-to-one association to UsTMenuAction
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_ACTION_ID", referencedColumnName = "MENU_ACTION_ID", nullable = false)
    private UsTMenuAction usTMenuAction;


}