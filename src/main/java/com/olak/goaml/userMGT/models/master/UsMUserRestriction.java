package com.olak.goaml.userMGT.models.master;


import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.reference.UsRUserBranch;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "US_M_USER_RESTRICTIONS", indexes = {
        @Index(name = "fk_US_M_USER_RESTRICTIONS_US_R_STATUS_DETAILS1_idx", columnList = "STATUS_ID"),
        @Index(name = "fk_US_M_USER_RESTRICTIONS_US_R_USER_BRANCH1_idx", columnList = "EMPLOYEE_BRANCH_ID"),
        @Index(name = "fk_US_M_USER_RESTRICTIONS_US_T_MENU_ACTION1_idx", columnList = "MENU_ACTION_ID")
})
public class UsMUserRestriction extends AuditModel implements Serializable {


    private static final long serialVersionUID = -6169569628412654806L;
    @Id
    @SequenceGenerator(name = "US_M_USER_RESTRICTIONS", allocationSize = 1, sequenceName = "US_M_USER_RESTRICTIONS_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_USER_RESTRICTIONS")
    @Column(name = "USER_RESTRICTION_ID", nullable = false, columnDefinition = "number(8)")
    private Integer userRestrictionId;

    @Column(name = "FROM_DATE")
    private LocalDate from;

    @Column(name = "REASON", length = 255)
    private String reason;

    @Column(name = "TO_DATE", length = 255)
    private LocalDate to;

    @Column(name = "TRANSACTION_LIMIT")
    private Double transactionLimit;

    //bi-directional many-to-one association to UsRStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

    //bi-directional many-to-one association to UsRUserBranch
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMPLOYEE_BRANCH_ID", referencedColumnName = "EMPLOYEE_BRANCH_ID", nullable = false)
    private UsRUserBranch usRUserBranch;

    //bi-directional many-to-one association to UsTMenuAction
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_ACTION_ID", referencedColumnName = "MENU_ACTION_ID", nullable = false)
    private UsTMenuAction usTMenuAction;

}