package com.olak.goaml.userMGT.models.reference;


import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.master.UsMBranch;
import com.olak.goaml.userMGT.models.master.UsMUser;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_R_EMPLOYEE_BRANCH", indexes = {
        @Index(name = "fk_US_R_EMPLOYEE_BRANCH_US_M_BRANCH1_idx", columnList = "BRANCH_ID"),
        @Index(name = "fk_US_R_EMPLOYEE_BRANCH_US_M_USER1_idx", columnList = "USERID"),
        @Index(name = "fk_US_R_EMPLOYEE_BRANCH_US_R_STATUS_DETAILS1_idx", columnList = "STATUS_ID")
})
public class UsREmployeeBranch extends AuditModel implements Serializable {

    private static final long serialVersionUID = -9170379563171891214L;

    @Id
    @SequenceGenerator(name = "US_R_EMPLOYEE_BRANCH", allocationSize = 1, sequenceName = "US_R_EMPLOYEE_BRANCH_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_R_EMPLOYEE_BRANCH")
    @Column(name = "EMPLOYEE_BRANCH_ID", nullable = false)
    private Integer employeeBranchId;

    @Column(name = "FROM_DATE")
    private LocalDate from;

    @Column(name = "TO_DATE")
    private LocalDate to;

    //bi-directional many-to-one association to UsMBranch
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRANCH_ID", referencedColumnName = "BRANCH_ID", nullable = false)
    private UsMBranch usMBranch;

    //bi-directional many-to-one association to UsMUser
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERID", referencedColumnName = "USERID", nullable = false)
    private UsMUser usMUser;

    //bi-directional many-to-one association to UsRUserStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

}
