package com.olak.goaml.userMGT.models.transaction;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.master.AdMUser;
import com.olak.goaml.userMGT.models.master.UsMMenu;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "US_T_USER_NAVIGATIONS", indexes = {
        @Index(name = "fk_US_T_USER_NAVIGATIONS_AD_M_USER1_idx", columnList = "USER_ID"),
        @Index(name = "fk_US_T_USER_NAVIGATIONS_US_M_MENU1_idx", columnList = "MENU_ID")
})
public class UsTUserNavigation extends AuditModel implements Serializable {

    private static final long serialVersionUID = 5044373803429809659L;
    @Id
    @SequenceGenerator(name = "US_T_USER_NAVIGATIONS", allocationSize = 1, sequenceName = "US_T_USER_NAVIGATIONS_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_T_USER_NAVIGATIONS")
    @Column(name = "USER_NAVIGATION_ID", nullable = false)
    private Integer userNavigationId;

    @Column(name = "REMOTE_IP")
    private String remoteIp;

    //bi-directional many-to-one association to UsMMenu
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_ID", referencedColumnName = "MENU_ID", nullable = false)
    private UsMMenu usMMenu;

    //bi-directional many-to-one association to UsMUser
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    private AdMUser adMUser;

}