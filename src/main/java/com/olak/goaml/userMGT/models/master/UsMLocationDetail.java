package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_M_LOCATION_DETAILS", indexes = {
        @Index(name = "fk_US_M_LOCATION_DETAILS_US_M_BRANCH1_idx", columnList = "BRANCH_ID"),
        @Index(name = "fk_US_M_LOCATION_DETAILS_US_R_STATUS_DETAILS1_idx", columnList = "STATUS_ID")
})
public class UsMLocationDetail extends AuditModel implements Serializable {

    private static final long serialVersionUID = -780952444225803906L;
    @Id
    @SequenceGenerator(name = "US_M_LOCATION_DETAILS", sequenceName = "US_M_LOCATION_DETAILS_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_LOCATION_DETAILS")
    @Column(name = "LOCATION_DETAILS_ID", nullable = false)
    private Integer locationDetailsId;

    @Column(name = "END_DATE")
    private LocalDate endDate;

    @Column(name = "LOCATION_TYPE", length = 45)
    private String locationType;

    @Column(name = "START_DATE")
    private LocalDate startDate;

    //bi-directional many-to-one association to UsMBranch
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRANCH_ID", referencedColumnName = "BRANCH_ID", nullable = false)
    private UsMBranch usMBranch;

    //bi-directional many-to-one association to UsRUserStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

}