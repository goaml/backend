package com.olak.goaml.userMGT.models.transaction;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.master.*;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_T_MENU_ACTION", indexes = {
        @Index(name = "fk_US_T_MENU_ACTION_US_M_ACTION1_idx", columnList = "ACTION_ID"),
        @Index(name = "fk_US_T_MENU_ACTION_US_M_MENU1_idx", columnList = "MENU_ID"),
        @Index(name = "fk_US_T_MENU_ACTION_US_M_MODULE1_idx", columnList = "MODULE_ID"),
        @Index(name = "fk_US_T_MENU_ACTION_US_R_STATUS_DETAIL1_idx", columnList = "STATUS_ID"),
})
public class UsTMenuAction extends AuditModel implements Serializable {

    private static final long serialVersionUID = -7019637805257965123L;
    @Id
    @SequenceGenerator(name = "US_T_MENU_ACTION", sequenceName = "US_T_MENU_ACTION_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_T_MENU_ACTION")
    @Column(name = "MENU_ACTION_ID", nullable = false)
    private Integer menuActionId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "URL")
    private String url;

    @Column(name = "IS_MENU")
    private Boolean isMenu;

    //bi-directional many-to-one association to UsMAction
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTION_ID", referencedColumnName = "ACTION_ID", nullable = false)
    private UsMAction usMAction;

    //bi-directional many-to-one association to UsMMenu
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_ID", referencedColumnName = "MENU_ID", nullable = false)
    private UsMMenu usMMenu;

    //bi-directional many-to-one association to UsMModule
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODULE_ID", referencedColumnName = "MODULE_ID", nullable = false)
    private UsMModule usMModule;

    //bi-directional many-to-one association to UsRUserStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

    //bi-directional many-to-one association to UsMMenuAllowedMap
    @OneToMany(mappedBy = "usTMenuAction")
    private List<UsMMenuAllowedMap> usMMenuAllowedMaps;

    //bi-directional many-to-one association to UsMUserRestriction
    @OneToMany(mappedBy = "usTMenuAction")
    private List<UsMUserRestriction> usMUserRestrictions;

    //bi-directional many-to-one association to UsTActivityLog
    @OneToMany(mappedBy = "usTMenuAction")
    private List<UsTActivityLog> usTActivityLogs;

}