package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "US_M_COMPANY_DETAILS")
public class UsMCompanyDetail extends AuditModel {

    @Id
    @SequenceGenerator(name = "LD_M_COMPANY_DETAILS", allocationSize = 1, sequenceName = "LD_M_COMPANY_DETAILS_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LD_M_COMPANY_DETAILS")
    @Column(name = "COM_ID", nullable = false, length = 8)
    private Integer comId;

    @Column(name = "APPLICANT_NAME", length = 200)
    private String applicantName;

    @Column(name = "BUSINESS_REG_NO", length = 20)
    private String businessRegNo;

    @Column(name = "BOI_REG_NO", length = 20)
    private String boiRegNo;

    @Column(name = "COMPANY_CODE", nullable = false, length = 10)
    private String companyCode;

    @Column(name = "CONTACT_NO", length = 15)
    private String contactNo;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

}
