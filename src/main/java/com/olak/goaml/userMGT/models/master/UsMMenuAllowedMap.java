package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "US_M_MENU_ALLOWED_MAP", indexes = {
        @Index(name = "fk_US_M_MENU_ALLOWED_MAP_US_M_USER_ROLE1_idx", columnList = "USER_ROLE_ID"),
        @Index(name = "fk_US_M_MENU_ALLOWED_MAP_US_R_STATUS_DETAIL1_idx", columnList = "STATUS_ID"),
        @Index(name = "fk_US_M_MENU_ALLOWED_MAP_US_T_MENU_ACTION1_idx", columnList = "MENU_ACTION_ID"),
})
public class UsMMenuAllowedMap extends AuditModel implements Serializable {

    @Id
    @SequenceGenerator(name = "US_M_MENU_ALLOWED_MAP", sequenceName = "US_M_MENU_ALLOWED_MAP_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_MENU_ALLOWED_MAP_GENERATOR")
    @Column(name = "MENU_ALLOW_MAP_ID", nullable = false)
    private Integer menuAllowMapId;

    //bi-directional many-to-one association to UsMUserRole
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ROLE_ID", referencedColumnName = "USER_ROLE_ID", nullable = false)
    private UsMUserRole usMUserRole;

    //bi-directional many-to-one association to UsRStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

    //bi-directional many-to-one association to UsTMenuAction
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_ACTION_ID", referencedColumnName = "MENU_ACTION_ID", nullable = false)
    private UsTMenuAction usTMenuAction;

    //bi-directional many-to-one association to UsMUserRoleLimitMgt
    @OneToMany(mappedBy = "usMMenuAllowedMap")
    private List<UsMUserRoleLimitMgt> usMUserRoleLimitMgts;

}
