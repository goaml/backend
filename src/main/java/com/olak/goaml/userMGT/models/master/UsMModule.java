package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_M_MODULE", indexes = {
        @Index(name = "fk_US_M_MODULE_US_R_STATUS_DETAIL1_idx", columnList = "STATUS_ID")
})
public class UsMModule extends AuditModel implements Serializable {

    private static final long serialVersionUID = 7816532849439864105L;

    @Id
    @SequenceGenerator(name = "US_M_MODULE", sequenceName = "US_M_MODULE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_MODULE")
    @Column(name = "MODULE_ID", nullable = false)
    private Integer moduleId;

    @Column(name = "MODULE_NAME", length = 45)
    private String moduleName;

    //bi-directional many-to-one association to UsRStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

    //bi-directional many-to-one association to UsTMenuAction
    @OneToMany(mappedBy = "usMModule")
    private List<UsTMenuAction> usTMenuActions;

    //bi-directional many-to-one association to UsMMenuGroup
    @OneToMany(mappedBy = "usMModule")
    private List<UsMMenuGroup> usMMenuGroups;

}