package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the us_m_employee database table.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "US_M_EMPLOYEE", indexes = {

        @Index(name = "fk_US_M_EMPLOYEE_DESIGNATION1_idx", columnList = "DESIGNATION_ID"),
        @Index(name = "fk_US_M_EMPLOYEE_US_R_STATUS_DETAIL1_idx", columnList = "STATUS_ID")
})
public class UsMEmployee extends AuditModel implements Serializable {

    private static final long serialVersionUID = 7124461129180904819L;

    @Id
    @SequenceGenerator(name = "US_M_EMPLOYEE", sequenceName = "US_M_EMPLOYEE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_EMPLOYEE")
    @Column(name = "EMPLOYEE_ID", nullable = false, columnDefinition = "number(8)")
    private Integer employeeId;

    @Column(name = "ADDRESS", length = 255)
    private String address;

    @Column(name = "DESCRIPTION", length = 255)
    private String desc;

    @Column(name = "EMP_EMAIL", length = 255)
    private String empEmail;

    @Column(name = "EMP_MOBILE_NUMBER", length = 11)
    private String empMobileNumber;

    @Column(name = "EMP_NIC", length = 12)
    private String empNic;

    @Column(name = "EMP_NUMBER", length = 255)
    private String empNumber;

    @Column(name = "FIRST_NAME", length = 255)
    private String firstName;

    @Column(name = "GENDER", length = 1)
    private String gender;

    @Column(name = "LAST_NAME", length = 255)
    private String lastName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DESIGNATION_ID", nullable = false, referencedColumnName = "DESIGNATION_ID")
    private Designation designation;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", nullable = false, referencedColumnName = "STATUS_ID")
    private UsRStatusDetail usRStatusDetail;

    //bi-directional many-to-one association to UsMAddress
    @OneToMany(mappedBy = "usMEmployee")
    private List<UsMAddress> usMAddresses;

    //bi-directional many-to-one association to UsMUser
    @OneToMany(mappedBy = "usMEmployee")
    private List<UsMUser> usMUsers;

//    @OneToMany(mappedBy = "usMEmployee")
//    private List<AdMUser> adMUsers;

}