package com.olak.goaml.userMGT.models.reference;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.master.AdMUser;
import com.olak.goaml.userMGT.models.master.UsMUserRestriction;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "US_R_USER_BRANCH", indexes = {
        @Index(name = "fk_US_R_USER_BRANCH_Ad_M_USER1_idx", columnList = "USER_ID"),
        @Index(name = "fk_US_R_USER_BRANCH_US_R_BRACH_DEPARTMENT", columnList = "BRACH_DEPT_ID"),
        @Index(name = "fk_US_R_USER_BRANCH_US_R_STATUS_DETAIL", columnList = "STATUS_ID")
})
public class UsRUserBranch extends AuditModel implements Serializable {

    private static final long serialVersionUID = 2740552514905990663L;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_R_USER_BRANCH")
    @SequenceGenerator(sequenceName = "US_R_USER_BRANCH_SEQ", allocationSize = 1, name = "US_R_USER_BRANCH")
    @Column(name = "EMPLOYEE_BRANCH_ID", nullable = false)
    private Integer employeeBranchId;

    @Column(name = "FROM_DATE")
    private LocalDate from;

    @Column(name = "IS_DEFAULT_DEPT")
    private Integer isDefualtDept;

    @Column(name = "TO_DATE")
    private LocalDate to;

    //bi-directional many-to-one association to UsMUserRestriction
    @OneToMany(mappedBy = "usRUserBranch")
    private List<UsMUserRestriction> usMUserRestrictions;

    //bi-directional many-to-one association to AdMUser
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    private AdMUser adMUser;

    //bi-directional many-to-one association to UsRBrachDepartment
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRACH_DEPT_ID", referencedColumnName = "BRACH_DEPT_ID", nullable = false)
    private UsRBrachDepartment usRBrachDepartment;

    //bi-directional many-to-one association to UsRStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

}
