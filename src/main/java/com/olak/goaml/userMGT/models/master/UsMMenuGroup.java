package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "US_M_MENU_GROUP", indexes = {
        @Index(name = "fk_US_M_MENU_GROUP_US_M_MODULE1_idx", columnList = "MODULE_ID"),
})
public class UsMMenuGroup extends AuditModel {

    @Id
    @SequenceGenerator(name = "US_M_MENU_GROUP", allocationSize = 1, sequenceName = "US_M_MENU_GROUP_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_MENU_GROUP")
    @Column(name = "MENU_GROUP_ID", nullable = false)
    private Integer menuGroupId;

    @Column(name = "MENU_GROUP_NAME", length = 255)
    private String menuGroupName;

    @Column(name = "SEQ_NO")
    private Integer seqNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODULE_ID", referencedColumnName = "MODULE_ID", nullable = false)
    private UsMModule usMModule;

    @OneToMany(mappedBy = "usMMenuGroup")
    private List<UsMMenuSubGroup> usMMenuSubGroups;

}