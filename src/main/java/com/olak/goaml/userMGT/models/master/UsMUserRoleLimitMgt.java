package com.olak.goaml.userMGT.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Table(name = "US_M_USER_ROLE_LIMIT_MGT", indexes = {
        @Index(name = "fk_US_M_USER_ROLE_LIMIT_MGT_US_R_STATUS_DETAILS1_idx", columnList = "STATUS_ID"),
        @Index(name = "fk_US_M_USER_ROLE_LIMIT_MGT_US_M_MENU_ALLOWED_MAP1_idx", columnList = "MENU_ALLOW_MAP_ID"),
        @Index(name = "fk_US_M_USER_ROLE_LIMIT_MGT_US_M_BRANCH1_idx", columnList = "BRANCH_ID"),
})
public class UsMUserRoleLimitMgt extends AuditModel implements Serializable {

    private static final long serialVersionUID = -3925218872348937427L;

    @Id
    @SequenceGenerator(name = "US_M_USER_ROLE_LIMIT_MGT", sequenceName = "US_M_USER_ROLE_LIMIT_MGT_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_M_USER_ROLE_LIMIT_MGT")
    @Column(name = "ROLE_LIMIT_ID", nullable = false)
    private Integer roleLimitId;

    @Column(name = "ROLE_LIMIT")
    private Double limit;

    //bi-directional many-to-one association to UsMBranch
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRANCH_ID", referencedColumnName = "BRANCH_ID", nullable = false)
    private UsMBranch usMBranch;

    //bi-directional many-to-one association to UsMMenuAllowedMap
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MENU_ALLOW_MAP_ID", referencedColumnName = "MENU_ALLOW_MAP_ID", nullable = false)
    private UsMMenuAllowedMap usMMenuAllowedMap;

    //bi-directional many-to-one association to UsRStatusDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private UsRStatusDetail usRStatusDetail;

}