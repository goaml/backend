package com.olak.goaml.userMGT.models.reference;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.userMGT.models.master.AdMUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "US_R_USER_ID_TYPE", indexes = {
        @Index(name = "fk_US_R_USER_ID_TYPE_US_R_LOGIN_TYPE1_idx", columnList = "LOGIN_TYPE_ID")
})
public class UsRUserIdType extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "US_R_USER_TYPE")
    @SequenceGenerator(sequenceName = "US_R_USER_TYPE_SEQ", allocationSize = 1, name = "US_R_USER_TYPE")
    @Column(name = "USER_TYPE_ID", nullable = false)
    private Integer userTypeId;

    @Column(name = "USER_CATEGORY", length = 50)
    private String userCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOGIN_TYPE_ID", nullable = false, referencedColumnName = "LOGIN_TYPE_ID")
    private UsRLoginType usRLoginType;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @OneToMany(mappedBy = "usRUserIdType")
    private List<AdMUser> adMUsers;

}
