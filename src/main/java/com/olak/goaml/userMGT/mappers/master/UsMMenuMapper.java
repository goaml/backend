package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMMenuDto;
import com.olak.goaml.userMGT.models.master.UsMMenu;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMMenuMapper {

    @Mappings(
            @Mapping(source = "usRStatusDetail.statusId", target = "statusId")
    )
    List<UsMMenuDto> listToDto(List<UsMMenu> content);

    @Mappings({
            @Mapping(target = "usRStatusDetail.statusId", source = "statusId")
    })
    UsMMenu toEntity(UsMMenuDto menuActionDto);

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId")
    })
    UsMMenuDto toDto(UsMMenu menuAction);
}