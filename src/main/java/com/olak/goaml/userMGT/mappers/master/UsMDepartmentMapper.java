package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMDepartmentDto;
import com.olak.goaml.userMGT.models.master.UsMDepartment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMDepartmentMapper {

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId")
    })
    List<UsMDepartmentDto> listToDto(List<UsMDepartment> usMDepartments);

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId")
    })
    UsMDepartmentDto toDto(UsMDepartment usMDepartment);
}
