package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMUserDto;
import com.olak.goaml.userMGT.dto.user.AdMUserDto;
import com.olak.goaml.userMGT.models.master.AdMUser;
import com.olak.goaml.userMGT.models.master.UsMUser;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMUserMapper {

    @Mappings({
            @Mapping(target = "usRStatusDetail.statusId", source = "statusId"),
            @Mapping(target = "usMUserRole.userRoleId", source = "userRoleId"),
            @Mapping(target = "usMEmployee.employeeId", source = "employeeId"),
    })
    UsMUser toEntity(UsMUserDto usMUserDto);

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId"),
            @Mapping(target = "userRoleId", source = "usMUserRole.userRoleId"),
            @Mapping(target = "employeeId", source = "usMEmployee.employeeId"),
    })
    UsMUserDto toDto(UsMUser usMUser);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    UsMUser updateUsMUserFromUsMUserDto(UsMUserDto usMUserDto, @MappingTarget UsMUser usMUser);

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId"),
            @Mapping(target = "userRoleId", source = "usMUserRole.userRoleId"),
            @Mapping(target = "employeeId", source = "usMEmployee.employeeId"),
    })
    List<AdMUserDto> toDtoList(List<AdMUser> userList);
}
