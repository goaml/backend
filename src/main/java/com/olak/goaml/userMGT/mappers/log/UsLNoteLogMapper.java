package com.olak.goaml.userMGT.mappers.log;

import com.olak.goaml.userMGT.dto.log.UsLNoteLogDto;
import com.olak.goaml.userMGT.models.log.UsLNoteLog;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsLNoteLogMapper {

    @Mappings({
            @Mapping(target = "adMUser.userId", source = "userId"),
            @Mapping(target = "usMMenu.menuId", source = "menuId"),
            @Mapping(target = "usMAction.actionId", source = "actionId"),
            @Mapping(target = "usMBranch.branchId", source = "branchId"),
            @Mapping(target = "usMDepartment.departmentId", source = "departmentId"),
            @Mapping(target = "adMUser.userName", source = "username"),
            @Mapping(target = "usMMenu.menuName", source = "menuName"),
            @Mapping(target = "usMAction.actionName", source = "actionName"),
            @Mapping(target = "usMBranch.branchName", source = "branchName"),
            @Mapping(target = "usMDepartment.deptName", source = "departmentName")
    })
    UsLNoteLog toEntity(UsLNoteLogDto dto);

    @Mappings({
            @Mapping(target = "userId", source = "adMUser.userId"),
            @Mapping(target = "menuId", source = "usMMenu.menuId"),
            @Mapping(target = "actionId", source = "usMAction.actionId"),
            @Mapping(target = "branchId", source = "usMBranch.branchId"),
            @Mapping(target = "departmentId", source = "usMDepartment.departmentId")
    })
    UsLNoteLogDto toDto(UsLNoteLog entity);

    @Mappings({
            @Mapping(target = "userId", source = "adMUser.userId"),
            @Mapping(target = "menuId", source = "usMMenu.menuId"),
            @Mapping(target = "actionId", source = "usMAction.actionId"),
            @Mapping(target = "branchId", source = "usMBranch.branchId"),
            @Mapping(target = "departmentId", source = "usMDepartment.departmentId")
    })
    List<UsLNoteLogDto> listToDto(List<UsLNoteLog> entities);

}