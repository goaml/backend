package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMActionDto;
import com.olak.goaml.userMGT.models.master.UsMAction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMActionMapper {

    @Mappings({
            @Mapping(target = "usRStatusDetail.statusId", source = "statusId")
    })
    UsMAction toEntity(UsMActionDto actionDto);

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId")
    })
    UsMActionDto toDto(UsMAction action);

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId")
    })
    List<UsMActionDto> listToDto(List<UsMAction> content);
}
