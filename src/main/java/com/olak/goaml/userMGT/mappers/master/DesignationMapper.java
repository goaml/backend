package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.DesignationDto;
import com.olak.goaml.userMGT.models.master.Designation;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DesignationMapper {
    Designation toEntity(DesignationDto designationDto);

    DesignationDto toDto(Designation designation);

    List<DesignationDto> listToDto(List<Designation> designationList);
}
