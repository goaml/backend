package com.olak.goaml.userMGT.mappers.master;


import com.olak.goaml.userMGT.dto.master.GlobalDetailsDto;
import com.olak.goaml.userMGT.models.master.GlobalDetails;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;


@Mapper(componentModel = "spring")
public interface GlobalDetailsMapper {

    GlobalDetails toEntity(GlobalDetailsDto globalDetailsDto);

    GlobalDetailsDto toDto(GlobalDetails globalDetails);

    List<GlobalDetailsDto> toDtoList(List<GlobalDetails> globalDetails);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    GlobalDetails updateGlobalDetailsFromGlobalDetailsDto(GlobalDetailsDto globalDetailsDto, @MappingTarget GlobalDetails globalDetails);

}

