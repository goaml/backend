package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMMenuGroupDto;
import com.olak.goaml.userMGT.models.master.UsMMenuGroup;
import org.mapstruct.Mapper;

@Mapper
public interface UsMMenuGroupMapper {

    UsMMenuGroup toEntity(UsMMenuGroupDto usMMenuGroupDto);

    UsMMenuGroupDto toDto(UsMMenuGroup usMMenuGroup);

}