package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMBranchDto;
import com.olak.goaml.userMGT.models.master.UsMBranch;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMBranchMapper {
    @Mappings({
            @Mapping(target = "usRStatusDetail.statusId", source = "statusId")
    })
    UsMBranch toEntity(UsMBranchDto usMBranchDto);

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId")
    })
    UsMBranchDto toDto(UsMBranch usMBranch);

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId")
    })
    List<UsMBranchDto> listToDto(List<UsMBranch> usMBranch);
}
