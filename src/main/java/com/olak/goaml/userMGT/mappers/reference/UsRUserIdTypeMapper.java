package com.olak.goaml.userMGT.mappers.reference;

import com.olak.goaml.userMGT.dto.reference.UsRUserIdTypeDto;
import com.olak.goaml.userMGT.models.reference.UsRUserIdType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsRUserIdTypeMapper {

    @Mappings({
            @Mapping(source = "loginTypeId", target = "usRLoginType.loginTypeId")
    })
    UsRUserIdType toEntity(UsRUserIdTypeDto usRUserIdTypeDto);

    @Mappings({
            @Mapping(source = "usRLoginType.loginTypeId", target = "loginTypeId")
    })
    UsRUserIdTypeDto toDto(UsRUserIdType usRUserIdType);

    @Mappings({
            @Mapping(source = "loginTypeId", target = "usRLoginType.loginTypeId")
    })
    List<UsRUserIdType> toEntityList(List<UsRUserIdTypeDto> usRUserIdTypeDtoList);

    @Mappings({
            @Mapping(source = "usRLoginType.loginTypeId", target = "loginTypeId")
    })
    List<UsRUserIdTypeDto> toDtoList(List<UsRUserIdType> usRUserIdTypeList);

}