package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMUserRoleLimitMgtDto;
import com.olak.goaml.userMGT.models.master.UsMUserRoleLimitMgt;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMUserRoleLimitMgtMapper {

    UsMUserRoleLimitMgtMapper INSTANCE = Mappers.getMapper(UsMUserRoleLimitMgtMapper.class);

    @Mappings({
            @Mapping(source = "usMBranch.branchId", target = "branchId"),
            @Mapping(source = "usMMenuAllowedMap.menuAllowMapId", target = "menuAllowMapId"),
            @Mapping(source = "usRStatusDetail.statusId", target = "statusId")

    })
    UsMUserRoleLimitMgtDto toDto(UsMUserRoleLimitMgt usMUserRoleLimitMgt);

    @Mappings({
            @Mapping(source = "branchId", target = "usMBranch.branchId"),
            @Mapping(source = "menuAllowMapId", target = "usMMenuAllowedMap.menuAllowMapId"),
            @Mapping(source = "statusId", target = "usRStatusDetail.statusId")
    })
    UsMUserRoleLimitMgt toEntity(UsMUserRoleLimitMgtDto usMUserRoleLimitMgtDto);

    @Mappings({
            @Mapping(source = "usMBranch.branchId", target = "branchId"),
            @Mapping(source = "usMMenuAllowedMap.menuAllowMapId", target = "menuAllowMapId"),
            @Mapping(source = "usRStatusDetail.statusId", target = "statusId")
    })
    List<UsMUserRoleLimitMgtDto> listToDto(List<UsMUserRoleLimitMgt> usMUserRoleLimitMgt);

    @Mappings({
            @Mapping(source = "branchId", target = "usMBranch.branchId"),
            @Mapping(source = "menuAllowMapId", target = "usMMenuAllowedMap.menuAllowMapId"),
            @Mapping(source = "statusId", target = "usRStatusDetail.statusId")
    })
    List<UsMUserRoleLimitMgt> listToEntity(List<UsMUserRoleLimitMgtDto> usMUserRoleLimitMgtDto);

}
