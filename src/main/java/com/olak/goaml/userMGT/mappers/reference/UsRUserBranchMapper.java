package com.olak.goaml.userMGT.mappers.reference;

import com.olak.goaml.userMGT.dto.reference.UsRUserBranchDto;
import com.olak.goaml.userMGT.models.reference.UsRUserBranch;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsRUserBranchMapper {

    @Mappings({
            @Mapping(target = "userId", source = "adMUser.userId"),
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId"),
            @Mapping(target = "brachDeptId", source = "usRBrachDepartment.brachDeptId"),
            @Mapping(target = "branchName", source = "usRBrachDepartment.usMBranch.branchName"),
            @Mapping(target = "deptName", source = "usRBrachDepartment.usMDepartment.deptName")
    })
    UsRUserBranchDto toDto(UsRUserBranch save);

    @Mappings({
            @Mapping(target = "adMUser.userId", source = "userId"),
            @Mapping(target = "usRStatusDetail.statusId", source = "statusId"),
            @Mapping(target = "usRBrachDepartment.brachDeptId", source = "brachDeptId"),
            @Mapping(target = "usRBrachDepartment.usMBranch.branchName", source = "branchName"),
            @Mapping(target = "usRBrachDepartment.usMDepartment.deptName", source = "deptName")
    })
    UsRUserBranch toEntity(UsRUserBranchDto usRUserBranchDto);

    @Mappings({
            @Mapping(target = "userId", source = "adMUser.userId"),
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId"),
            @Mapping(target = "brachDeptId", source = "usRBrachDepartment.brachDeptId"),
            @Mapping(target = "branchName", source = "usRBrachDepartment.usMBranch.branchName"),
            @Mapping(target = "deptName", source = "usRBrachDepartment.usMDepartment.deptName")
    })
    List<UsRUserBranchDto> entityListToDtoList(List<UsRUserBranch> content);

}