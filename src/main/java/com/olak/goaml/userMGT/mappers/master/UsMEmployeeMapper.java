package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMEmployeeDto;
import com.olak.goaml.userMGT.models.master.UsMEmployee;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMEmployeeMapper {
    UsMEmployeeDto toDto(UsMEmployee usMEmployee);

    List<UsMEmployeeDto> listToDto(List<UsMEmployee> content);
}
