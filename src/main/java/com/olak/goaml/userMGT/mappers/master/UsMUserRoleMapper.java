package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMUserRoleDto;
import com.olak.goaml.userMGT.models.master.UsMUserRole;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMUserRoleMapper {

    List<UsMUserRoleDto> listToDto(List<UsMUserRole> content);

    @Mappings(
            @Mapping(target = "usRStatusDetail.statusId", source = "statusId")
    )
    UsMUserRole toEntity(UsMUserRoleDto userRoleDto);

    @Mappings(
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId")
    )
    UsMUserRoleDto toDto(UsMUserRole userRole);

}