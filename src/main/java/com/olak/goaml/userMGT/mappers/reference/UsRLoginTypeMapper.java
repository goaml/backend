package com.olak.goaml.userMGT.mappers.reference;

import com.olak.goaml.userMGT.dto.reference.UsRLoginTypeDto;
import com.olak.goaml.userMGT.models.reference.UsRLoginType;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsRLoginTypeMapper {

    UsRLoginType toEntity(UsRLoginTypeDto usRLoginTypeDto);

    UsRLoginTypeDto toDto(UsRLoginType usRLoginType);

    List<UsRLoginType> toEntityList(List<UsRLoginTypeDto> usRLoginTypeDtoList);

    List<UsRLoginTypeDto> toDtoList(List<UsRLoginType> usRLoginTypeList);

}
