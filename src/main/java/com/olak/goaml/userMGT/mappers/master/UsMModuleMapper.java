package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMModuleDto;
import com.olak.goaml.userMGT.models.master.UsMModule;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMModuleMapper {
    List<UsMModuleDto> listToDto(List<UsMModule> moduleList);

    UsMModuleDto toDto(UsMModule usMModule);

    UsMModule toEntity(UsMModuleDto usMModuleDto);
}
