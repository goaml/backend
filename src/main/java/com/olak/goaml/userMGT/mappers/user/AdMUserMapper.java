package com.olak.goaml.userMGT.mappers.user;

import com.olak.goaml.userMGT.dto.user.AdMUserDto;
import com.olak.goaml.userMGT.dto.user.CreateUserResponseDto;
import com.olak.goaml.userMGT.dto.user.UserGetOneDto;
import com.olak.goaml.userMGT.dto.user.UserRequestDto;
import com.olak.goaml.userMGT.models.master.AdMUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * The interface Ad m user mapper.
 */
@Mapper(componentModel = "spring")
public interface AdMUserMapper {

    /**
     * The constant INSTANCE.
     */
    AdMUserMapper INSTANCE = Mappers.getMapper(AdMUserMapper.class);

    /**
     * To dto ad m user dto.
     *
     * @param adMUser the ad m user
     * @return the ad m user dto
     */

    @Mappings({
            @Mapping(source = "usRStatusDetail.statusId", target = "statusId"),
            @Mapping(source = "usMUserRole.userRoleId", target = "userRoleId"),
            @Mapping(source = "usMBranch.branchId", target = "branchId"),
            @Mapping(source = "usMDepartment.departmentId", target = "departmentId"),
            @Mapping(source = "usRUserIdType.userTypeId", target = "userTypeId")
    })
    AdMUserDto toDto(AdMUser adMUser);

    /**
     * To entity ad r security question.
     *
     * @param adMUserDto the ad m user dto
     * @return the ad m user
     */
    @Mappings({
            @Mapping(source = "statusId", target = "usRStatusDetail.statusId"),
            @Mapping(source = "userRoleId", target = "usMUserRole.userRoleId"),
            @Mapping(source = "branchId", target = "usMBranch.branchId"),
            @Mapping(source = "departmentId", target = "usMDepartment.departmentId"),
            @Mapping(source = "userTypeId", target = "usRUserIdType.userTypeId")
    })
    AdMUser toEntity(AdMUserDto adMUserDto);


    /**
     * To entity ad r security question.
     *
     * @param userRequestDto the CreateUserRequestDto
     * @return the ad m user
     */
    @Mapping(source = "userRoleId", target = "usMUserRole.userRoleId")
    @Mapping(source = "branchId", target = "usMBranch.branchId")
    @Mapping(source = "departmentId", target = "usMDepartment.departmentId")
    AdMUser createUserRequestDtoToEntity(UserRequestDto userRequestDto);

    /**
     * To dto ad m user dto.
     *
     * @param adMUser the ad m user
     * @return the CreateUserResponseDto
     */

    @Mapping(source = "usRStatusDetail.statusId", target = "statusId")
    @Mapping(source = "usMUserRole.userRoleId", target = "userRoleId")
    @Mapping(source = "usMBranch.branchId", target = "branchId")
    @Mapping(source = "usMDepartment.departmentId", target = "departmentId")
    CreateUserResponseDto entityToCreateUserResponseDto(AdMUser adMUser);


    /**
     * Entity to user request dto user request dto.
     *
     * @param adMUser the ad m user
     * @return the user request dto
     */
    @Mapping(source = "usMUserRole.userRoleId", target = "userRoleId")
    @Mapping(source = "usMBranch.branchId", target = "branchId")
    @Mapping(source = "usMDepartment.departmentId", target = "departmentId")
    UserRequestDto entityToUserRequestDto(AdMUser adMUser);

    /**
     * Entity to user request dto user request dto.
     *
     * @param adMUser the ad m user
     * @return the user request dto
     */

    UserGetOneDto entityToUserGetOneDto(AdMUser adMUser);


    @Mappings({
            @Mapping(source = "departmentId", target = "usMDepartment.departmentId"),
            @Mapping(source = "branchId", target = "usMBranch.branchId"),
            @Mapping(source = "userRoleId", target = "usMUserRole.userRoleId"),
            @Mapping(source = "statusId", target = "usRStatusDetail.statusId")

    })
    AdMUser dtoToEntity(AdMUserDto adMUserDto);

    @Mappings({
            @Mapping(source = "usRStatusDetail.statusId", target = "statusId"),
            @Mapping(source = "usMUserRole.userRoleId", target = "userRoleId"),
            @Mapping(source = "usMBranch.branchId", target = "branchId"),
            @Mapping(source = "usMDepartment.departmentId", target = "departmentId"),
            @Mapping(source = "usRUserIdType.userTypeId", target = "userTypeId")
    })
    List<AdMUserDto> listToDto(List<AdMUser> adMUserList);

}