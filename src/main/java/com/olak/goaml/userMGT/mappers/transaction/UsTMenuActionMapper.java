package com.olak.goaml.userMGT.mappers.transaction;

import com.olak.goaml.userMGT.dto.transaction.UsTMenuActionDto;
import com.olak.goaml.userMGT.models.transaction.UsTMenuAction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsTMenuActionMapper {

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId"),
            @Mapping(target = "menuId", source = "usMMenu.menuId"),
            @Mapping(target = "actionId", source = "usMAction.actionId"),
            @Mapping(target = "moduleId", source = "usMModule.moduleId"),
            @Mapping(target = "actionName", source = "usMAction.actionName"),
            @Mapping(target = "statusName", source = "usRStatusDetail.statusName"),
            @Mapping(target = "moduleName", source = "usMModule.moduleName")
    })
    List<UsTMenuActionDto> listToDto(List<UsTMenuAction> content);

    @Mappings({
            @Mapping(target = "statusId", source = "usRStatusDetail.statusId"),
            @Mapping(target = "menuId", source = "usMMenu.menuId"),
            @Mapping(target = "actionId", source = "usMAction.actionId"),
            @Mapping(target = "moduleId", source = "usMModule.moduleId"),
            @Mapping(target = "actionName", source = "usMAction.actionName"),
            @Mapping(target = "statusName", source = "usRStatusDetail.statusName"),
            @Mapping(target = "moduleName", source = "usMModule.moduleName")
    })
    UsTMenuActionDto toDto(UsTMenuAction menuAction);

    @Mappings({
            @Mapping(target = "usRStatusDetail.statusId", source = "statusId"),
            @Mapping(target = "usMMenu.menuId", source = "menuId"),
            @Mapping(target = "usMAction.actionId", source = "actionId"),
            @Mapping(target = "usMModule.moduleId", source = "moduleId"),
    })
    UsTMenuAction toEntity(UsTMenuActionDto menuActionDto);

    @Mappings({
            @Mapping(target = "usRStatusDetail.statusId", source = "statusId"),
            @Mapping(target = "usMMenu.menuId", source = "menuId"),
            @Mapping(target = "usMAction.actionId", source = "actionId"),
            @Mapping(target = "usMModule.moduleId", source = "moduleId"),
    })
    List<UsTMenuAction> listToEntity(List<UsTMenuActionDto> menuActionList);
}
