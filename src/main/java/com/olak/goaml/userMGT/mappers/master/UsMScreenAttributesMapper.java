package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMScreenAttributesDto;
import com.olak.goaml.userMGT.models.master.UsMScreenAttributes;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMScreenAttributesMapper {
    @Mappings({
            @Mapping(target = "usTMenuAction.menuActionId", source = "menuActionId")
    })
    UsMScreenAttributes toEntity(UsMScreenAttributesDto usMScreenAttributesDto);

    @Mappings({
            @Mapping(target = "menuActionId", source = "usTMenuAction.menuActionId")
    })
    UsMScreenAttributesDto toDto(UsMScreenAttributes usMScreenAttributes);

    @Mappings({
            @Mapping(target = "menuActionId", source = "usTMenuAction.menuActionId")
    })
    List<UsMScreenAttributesDto> listToDto(List<UsMScreenAttributes> content);
}
