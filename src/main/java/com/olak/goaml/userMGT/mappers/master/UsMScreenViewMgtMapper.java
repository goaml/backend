package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMScreenViewMgtDto;
import com.olak.goaml.userMGT.models.master.UsMScreenViewMgt;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface UsMScreenViewMgtMapper {

    @Mappings({
            @Mapping(target = "usMScreenAttributes.screenAttributeId", source = "screenAttributeId"),
            @Mapping(target = "usTMenuAction.menuActionId", source = "menuActionId"),
    })
    UsMScreenViewMgt toEntity(UsMScreenViewMgtDto usMScreenViewMgtDto);

    @Mappings({
            @Mapping(target = "screenAttributeId", source = "usMScreenAttributes.screenAttributeId"),
            @Mapping(target = "menuActionId", source = "usTMenuAction.menuActionId"),
    })
    UsMScreenViewMgtDto toDto(UsMScreenViewMgt usMScreenViewMgt);

}
