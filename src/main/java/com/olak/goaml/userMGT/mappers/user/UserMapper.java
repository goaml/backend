package com.olak.goaml.userMGT.mappers.user;

import lombok.Getter;
import org.springframework.stereotype.Service;

/**
 * The type User mapper.
 */
@Service
@Getter
public class UserMapper {
//    private final AdMUserMapper userDetailsMapper;
//    private final AdMUserRoleMapper userRoleMapper;
//    private final AdMUserUserGroupMapper userGroupMapper;
//    private final AdRUserTypeMapper adRUserTypeMapper;

    /**
     * Instantiates a new User mapper.
     *
     * @param userDetailsMapper the user details mapper
     * @param userRoleMapper    the user role mapper
     * @param userGroupMapper   the user group mapper
     * @param adRUserTypeMapper the ad r user type mapper
     */
//    public UserMapper(AdMUserMapper userDetailsMapper, AdMUserRoleMapper userRoleMapper, AdMUserUserGroupMapper userGroupMapper, AdRUserTypeMapper adRUserTypeMapper) {
//        this.userDetailsMapper = userDetailsMapper;
//        this.userRoleMapper = userRoleMapper;
//        this.userGroupMapper = userGroupMapper;
//        this.adRUserTypeMapper = adRUserTypeMapper;
//    }


}
