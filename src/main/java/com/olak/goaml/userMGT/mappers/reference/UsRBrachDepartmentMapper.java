package com.olak.goaml.userMGT.mappers.reference;

import com.olak.goaml.userMGT.dto.reference.UsRBrachDepartmentDto;
import com.olak.goaml.userMGT.models.reference.UsRBrachDepartment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsRBrachDepartmentMapper {
    @Mappings({
            @Mapping(source = "usMBranch.branchId", target = "branchId"),
            @Mapping(source = "usMDepartment.departmentId", target = "departmentId"),
            @Mapping(source = "usMBranch.branchName", target = "branchName"),
    })
    UsRBrachDepartmentDto toDto(UsRBrachDepartment usRBrachDepartment);

    @Mappings({
            @Mapping(source = "branchId", target = "usMBranch.branchId"),
            @Mapping(source = "departmentId", target = "usMDepartment.departmentId"),
            @Mapping(source = "branchName", target = "usMBranch.branchName")
    })
    UsRBrachDepartment toEntity(UsRBrachDepartmentDto usRBrachDepartmentDto);

    @Mappings({
            @Mapping(target = "branchId", source = "usMBranch.branchId"),
            @Mapping(target = "departmentId", source = "usMDepartment.departmentId"),
            @Mapping(target = "branchName", source = "usMBranch.branchName"),
            @Mapping(target = "deptName", source = "usMDepartment.deptName")
    })
    List<UsRBrachDepartmentDto> entityListToDtoList(List<UsRBrachDepartment> usRBrachDepartmentList);
}
