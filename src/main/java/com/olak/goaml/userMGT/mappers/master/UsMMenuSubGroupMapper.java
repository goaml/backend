package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMMenuSubGroupDto;
import com.olak.goaml.userMGT.models.master.UsMMenuSubGroup;
import org.mapstruct.Mapper;

@Mapper
public interface UsMMenuSubGroupMapper {

    UsMMenuSubGroup toEntity(UsMMenuSubGroupDto usMMenuSubGroupDto);

    UsMMenuSubGroupDto toDto(UsMMenuSubGroup usMMenuSubGroup);

}