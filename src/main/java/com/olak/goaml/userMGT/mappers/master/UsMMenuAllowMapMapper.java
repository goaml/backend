package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMMenuAllowedMapDto;
import com.olak.goaml.userMGT.models.master.UsMMenuAllowedMap;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsMMenuAllowMapMapper {

    List<UsMMenuAllowedMapDto> listToDto(List<UsMMenuAllowedMap> content);
}
