package com.olak.goaml.userMGT.mappers.master;

import com.olak.goaml.userMGT.dto.master.UsMUserRestrictionDto;
import com.olak.goaml.userMGT.models.master.UsMUserRestriction;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper(componentModel = "spring")
public interface UsMUserRestrictionMapper {

//     UsMUserRestriction toEntity(UsMUserRestrictionDto usMUserRestrictionDto);
//     UsMUserRestrictionDto toDto(UsMUserRestriction usMUserRestriction);

    UsMUserRestrictionMapper INSTANCE = Mappers.getMapper(UsMUserRestrictionMapper.class);

    @Mappings({
            @Mapping(source = "usRStatusDetail.statusId", target = "statusId"),
            @Mapping(source = "usRUserBranch.employeeBranchId", target = "employeeBranchId"),
            @Mapping(source = "usTMenuAction.menuActionId", target = "menuActionId")
    })
    UsMUserRestrictionDto toDto(UsMUserRestriction usMUserRestriction);

    @Mappings({
            @Mapping(source = "statusId", target = "usRStatusDetail.statusId"),
            @Mapping(source = "employeeBranchId", target = "usRUserBranch.employeeBranchId"),
            @Mapping(source = "menuActionId", target = "usTMenuAction.menuActionId")
    })
    UsMUserRestriction toEntity(UsMUserRestrictionDto usMUserRestrictionDto);

    @Mappings({
            @Mapping(source = "usRStatusDetail.statusId", target = "statusId"),
            @Mapping(source = "usRUserBranch.employeeBranchId", target = "employeeBranchId"),
            @Mapping(source = "usTMenuAction.menuActionId", target = "menuActionId")
    })
    List<UsMUserRestrictionDto> listToDto(List<UsMUserRestriction> usMUserRestriction);

    @Mappings({
            @Mapping(source = "statusId", target = "usRStatusDetail.statusId"),
            @Mapping(source = "employeeBranchId", target = "usRUserBranch.employeeBranchId"),
            @Mapping(source = "menuActionId", target = "usTMenuAction.menuActionId")
    })
    List<UsMUserRestriction> listToEntity(List<UsMUserRestrictionDto> usMUserRestrictionDto);

//    UsMUserRestrictionDto toDto(UsMUserRestriction userRestriction);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    UsMUserRestriction updateUserRestriction(UsMUserRestriction userRestriction, @MappingTarget UsMUserRestriction userRest);

}
