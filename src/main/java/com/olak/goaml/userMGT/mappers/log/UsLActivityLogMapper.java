package com.olak.goaml.userMGT.mappers.log;

import com.olak.goaml.userMGT.dto.log.UsLActivityLogDto;
import com.olak.goaml.userMGT.models.log.UsLActivityLog;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsLActivityLogMapper {
    UsLActivityLog toEntity(UsLActivityLogDto dto);

    UsLActivityLogDto toDto(UsLActivityLog entity);

    List<UsLActivityLogDto> listToDto(List<UsLActivityLog> entities);
}
