package com.olak.goaml.userMGT.mappers.reference;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import com.olak.goaml.userMGT.models.reference.UsRStatusDetail;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsRUserStatusDetailMapper {

    List<UsRStatusDetailDto> entityListToDtoList(List<UsRStatusDetail> content);

    UsRStatusDetailDto toDto(UsRStatusDetail usRStatusDetail);

    UsRStatusDetail toEntity(UsRStatusDetailDto usRStatusDetailDto);
}
