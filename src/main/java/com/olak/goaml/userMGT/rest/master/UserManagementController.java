package com.olak.goaml.userMGT.rest.master;

import com.olak.goaml.config.PasswordUtils;
import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dto.log.UsLActivityLogDto;
import com.olak.goaml.userMGT.dto.master.*;
import com.olak.goaml.userMGT.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.userMGT.dto.other.*;
import com.olak.goaml.userMGT.dto.reference.*;
import com.olak.goaml.userMGT.dto.restriction.UserMenuDto;
import com.olak.goaml.userMGT.dto.transaction.UsTMenuActionDto;
import com.olak.goaml.userMGT.dto.user.AdMUserDto;
import com.olak.goaml.userMGT.dto.user.PasswordResetDto;
import com.olak.goaml.userMGT.service.log.UsLActivityLogService;
import com.olak.goaml.userMGT.service.log.UsLNoteLogService;
import com.olak.goaml.userMGT.service.master.*;
import com.olak.goaml.userMGT.service.reference.UsRBrachDepartmentService;
import com.olak.goaml.userMGT.service.reference.UsRLoginTypeService;
import com.olak.goaml.userMGT.service.reference.UsRStatusDetailService;
import com.olak.goaml.userMGT.service.reference.UsRUserIdTypeService;
import com.olak.goaml.userMGT.service.transaction.UsTMenuActionService;
import com.olak.goaml.userMGT.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Validated
@Slf4j
@RestController
@RequestMapping("/user-management")
public class UserManagementController {

    private final UsMMenuService usMMenuService;
    private final UsMUserRoleService userRoleService;
    private final UsMBranchService branchService;
    private final UsMModuleService moduleService;
    private final DesignationService designationService;
    private final UsMActionService actionService;
    private final UserService admUserService;
    private final UsMDepartmentService departmentService;
    private final UsMUserRestrictionService userRestrictionService;
    private final UsMMenuAllowedMapService menuAllowedMapService;
    private final UsRBrachDepartmentService brachDepartmentService;
    private final UsRStatusDetailService statusDetailService;
    private final UsRUserIdTypeService userIdTypeService;
    private final UsRLoginTypeService loginTypeService;
    private final UserService userService;
    private final GlobalDetailsService globalDetailsService;
    private final UsMScreenAttributesService screenAttributesService;
    private final UsMScreenViewMgtService screenViewMgtService;
    private final UsLActivityLogService activityLogService;
    private final UsLNoteLogService noteLogService;
    private final UsTMenuActionService menuActionService;

    public UserManagementController(UsMMenuService usMMenuService, UsMUserRoleService userRoleService, UsMBranchService branchService, UsMModuleService moduleService, DesignationService designationService, UsMActionService actionService, UserService admUserService, UsMDepartmentService departmentService, UsMUserRestrictionService userRestrictionService, UsMMenuAllowedMapService menuAllowedMapService, UsRBrachDepartmentService brachDepartmentService, UsRStatusDetailService statusDetailService, UsRUserIdTypeService userIdTypeService, UsRLoginTypeService loginTypeService, UserService userService, GlobalDetailsService globalDetailsService, UsMScreenAttributesService screenAttributesService, UsMScreenViewMgtService screenViewMgtService, UsLActivityLogService activityLogService, UsLNoteLogService noteLogService, UsTMenuActionService menuActionService) {
        this.usMMenuService = usMMenuService;
        this.userRoleService = userRoleService;
        this.branchService = branchService;
        this.moduleService = moduleService;
        this.designationService = designationService;
        this.actionService = actionService;
        this.admUserService = admUserService;
        this.departmentService = departmentService;
        this.userRestrictionService = userRestrictionService;
        this.menuAllowedMapService = menuAllowedMapService;
        this.brachDepartmentService = brachDepartmentService;
        this.statusDetailService = statusDetailService;
        this.userIdTypeService = userIdTypeService;
        this.loginTypeService = loginTypeService;
        this.userService = userService;
        this.globalDetailsService = globalDetailsService;
        this.screenAttributesService = screenAttributesService;
        this.screenViewMgtService = screenViewMgtService;
        this.activityLogService = activityLogService;
        this.noteLogService = noteLogService;
        this.menuActionService = menuActionService;
    }


    @GetMapping("/getMenuList")
    public ResponseEntity<ApiResponseDto<List<UsMMenuDto>>> getAllMenu(
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "menuId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(required = false) Integer status
    ) {
        ApiResponseDto<List<UsMMenuDto>> response = usMMenuService.getAllMenu(page, per_page, sort, direction, search, status);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/roleView")
    public ResponseEntity<ApiResponseDto<List<UsMUserRoleDto>>> getAllUserRoles(
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "userRoleId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction) {
        ApiResponseDto<List<UsMUserRoleDto>> response = userRoleService.getAllUserRoles(page, per_page, sort, direction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @PostMapping("/createRole")
    public ResponseEntity<UsMUserRoleDto> roleCreation(@RequestBody RoleCreationDto userRole) {
        UsMUserRoleDto response = userRoleService.roleCreation(userRole);
        if (response.getUserRoleId() != null) {
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/roleAccess/allowed/{status}/userRoleId/{userRoleId}")
    public ResponseEntity<MenuActionListDto> getAllowedMenuActionsForUserRole(@PathVariable Integer userRoleId, @PathVariable Boolean status) {
        MenuActionListDto response;

        try {
            response = userRoleService.getAllowedMenuActionsForUserRole(userRoleId, status);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/roleAccess/userRoleId/{userRoleId}")
    public ResponseEntity<MenuActionListDto> getMenuActionListForRole(@PathVariable Integer userRoleId) {
        MenuActionListDto data;
        try {
            data = userRoleService.getMenuActionListForRole(userRoleId);
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            e.fillInStackTrace();
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "ROLE_ACCESS");
        }
    }


    @PostMapping("/createUserRole")
    public ResponseEntity<UsMUserRoleDto> createUserRole(@RequestBody UsMUserRoleDto userRoleDto) {

        UsMUserRoleDto userRole = userRoleService.createUserRole(userRoleDto);
        if (userRole.getUserRoleId() != null) {
            return new ResponseEntity<>(userRole, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getBranchList")
    public ResponseEntity<ApiResponseDto<List<UsMBranchDto>>> getAllBranches(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "branchId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "", required = false) String branchAddress,
            @RequestParam(defaultValue = "", required = false) String branchLocation,
            @RequestParam(defaultValue = "", required = false) String branchName,
            @RequestParam(defaultValue = "", required = false) String contactNumber,
            @RequestParam(defaultValue = "", required = false) String email,
            @RequestParam(required = false) Integer userId,
            @RequestParam(required = false) Integer branchId
    ) {
        ApiResponseDto<List<UsMBranchDto>> response = branchService.getAllBranches(page, per_page, search, sort, direction, branchAddress, branchLocation, branchName, contactNumber, email, userId, branchId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping("/getModuleList")
    public ResponseEntity<ApiResponseDto<List<UsMModuleDto>>> getAllModules(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "moduleId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        ApiResponseDto<List<UsMModuleDto>> response = moduleService.getAllModules(page, per_page, search, sort, direction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/getDesignationList")
    public ResponseEntity<ApiResponseDto<List<DesignationDto>>> getAllDesignation(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "designationId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        ApiResponseDto<List<DesignationDto>> response = designationService.getAllDesignation(page, per_page, search, sort, direction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/getActionList")
    public ResponseEntity<ApiResponseDto<List<UsMActionDto>>> getAllAction(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "actionId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        ApiResponseDto<List<UsMActionDto>> response = actionService.getAllAction(page, per_page, search, sort, direction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/getUsers")
    public ResponseEntity<ApiResponseDto<List<GetUserResponseDto>>> getUserList(
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "userId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(required = false) String search,
            @RequestParam(required = false) Integer statusId
    ) {
        ApiResponseDto<List<GetUserResponseDto>> response = admUserService.getUserList(page, per_page, sort, direction, search, statusId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping("/getDepartments")
    public ResponseEntity<ApiResponseDto<List<UsMDepartmentDto>>> getDepartmentList(
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "departmentId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(required = false, defaultValue = "") String search
    ) {
        ApiResponseDto<List<UsMDepartmentDto>> response = departmentService.getDepartmentList(page, per_page, sort, direction, search);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/user/{userId}/authorize/{status}")
    public ResponseEntity<ResponseDto> authorizedUser(@PathVariable Integer userId, @PathVariable String status) {

        ResponseEntity<ResponseDto> savedDto = admUserService.authorizedUser(userId, status);
        ResponseDto response = savedDto.getBody();
        assert response != null;
        if (response.getMessage().equals("Authorized Success")) {
            return ResponseEntity.ok(response);
        } else {
            throw new BadRequestAlertException(response.getMessage(), ENTITY_NAME, "AUTHORIZED_USER");
        }
    }

    @GetMapping("/branchForUser/user/{userId}")
    public ResponseEntity<BranchListDto> getBranchForUser(@PathVariable Integer userId) {
        try {
            return userRoleService.getBranchForUser(userId);
        } catch (Exception e) {
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "BRANCH_FOR_USER");
        }
    }

    @GetMapping("/user/{userId}/branch/{branchId}/menu")
    public ResponseEntity<List<UserMenuDto>> getUserMenu(@PathVariable Integer userId,
                                                         @PathVariable Integer branchId) {
        try {
            return userRestrictionService.getMenuForUser(userId, branchId);
        } catch (Exception e) {
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "USER_MENU");
        }
    }

    @PostMapping("/user/userRestrictions")
    public ResponseEntity<List<UsMUserRestrictionDto>> saveUserRestrictions(@RequestBody UserRestrictionReqDto restrictionReq) {

        List<UsMUserRestrictionDto> response = userRestrictionService.saveUserRestrictions(restrictionReq);
        return new ResponseEntity<>(response, HttpStatus.CREATED);

    }

    //Developed By:Gayashan 2022-12-03
    @GetMapping("/userRestriction/user/{userId}/brach/{branchId}")
    public ResponseEntity<ApiResponseDto<List<UserRestrictionDto>>> getAllUserRestriction(
            @PathVariable Integer userId,
            @PathVariable Integer branchId,
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "userId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        ApiResponseDto<List<UserRestrictionDto>> response = userRestrictionService.getUserRestrictionForUserId(userId, branchId, page, per_page, sort, direction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/newUserRestriction/user/{userId}/branch/{branchId}")
    public ResponseEntity<ApiResponseDto<List<UserRestrictionDto>>> getNewRestriction(
            @PathVariable Integer userId,
            @PathVariable Integer branchId,
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "100", required = false) Integer per_page,
            @RequestParam(defaultValue = "userId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        ApiResponseDto<List<UserRestrictionDto>> response = userRestrictionService.getNewUserRestrictionForUserId(userId, branchId, page, per_page, sort, direction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    //Gayashan - 2022-12-10
    @PostMapping("/roleRestriction")
    public ResponseEntity<UsMMenuAllowedMapDto> saveRoleRestriction(@RequestBody UsMMenuAllowedMapDto menuAllowedMapDto) {

        UsMMenuAllowedMapDto response = menuAllowedMapService.saveRoleRestriction(menuAllowedMapDto);
        if (response.getMenuAllowMapId() != null) {
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/role-restriction")
    public ResponseEntity<List<UsMMenuAllowedMapDto>> saveRoleRestrictionList(@RequestBody List<RoleRestrictionCreateDto> roleRestrictionCreateDtoList) {
        return menuAllowedMapService.saveRoleRestrictionList(roleRestrictionCreateDtoList);
    }

    @GetMapping("/roleRestriction/role/{userRoleId}")
    public ResponseEntity<ApiResponseDto<List<UsMMenuAllowedMapDto>>> getUserRoleRestriction(
            @PathVariable Integer userRoleId,
            @RequestParam(required = false) Integer statusId,
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "menuAllowMapId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        ApiResponseDto<List<UsMMenuAllowedMapDto>> response = menuAllowedMapService.getUserRoleRestriction(userRoleId, statusId, page, per_page, sort, direction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/userRestriction/user/{userId}/branch/{branchId}/menuAction/{menuActionId}")
    public ResponseEntity<ResponseDto> updateUserRestriction(@PathVariable Integer userId, @PathVariable Integer branchId, @PathVariable Integer menuActionId) {

        ResponseDto response = userRestrictionService.updateUserRestriction(userId, branchId, menuActionId);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("/checkUserAvailable")
    public ResponseEntity<CheckUserAvailableResponseDto> checkUserAvailable(@RequestBody CheckUserAvailableReqDto user) {

        CheckUserAvailableResponseDto response = admUserService.checkUserAvailable(user);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("/userProfile/user/{userId}")
    public ResponseEntity<UserProfileDto> getUserProfiile(@PathVariable Integer userId) {

        UserProfileDto response = admUserService.getUserProfiile(userId);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("/userGetByUserName/{username}")
    public ResponseEntity<UserProfileDto> getByUserName(@PathVariable String username) {

        UserProfileDto response = admUserService.getByUserName(username);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @Transactional
    @PutMapping("/extendingPasswordExpDate")
    public ResponseEntity<AdMUserDto> extendingPasswordExpDate(@RequestBody ExtendPasswordDto extendPasswordDto) {
        return admUserService.extendingPasswordExpDate(extendPasswordDto);
    }

    @DeleteMapping("/deleteUserRole/{userRoleId}")
    public ResponseEntity<String> deleteUserRole(@PathVariable Integer userRoleId) {
        return userRoleService.deleteUserRole(userRoleId);
    }

    @PutMapping("/updateUserRole")
    public ResponseEntity<UsMUserRoleDto> updateUserRole(@RequestBody UsMUserRoleDto userRoleDto) {
        return userRoleService.updateUserRole(userRoleDto);
    }

    @GetMapping("/userRole/{userRoleId}")
    public ResponseEntity<UsMUserRoleDto> getUserRole(@PathVariable Integer userRoleId) {
        return userRoleService.getUserRole(userRoleId);
    }


    //Get All Users with Filters
    @GetMapping("/getUsersList")
    public ResponseEntity<ApiResponseDto<List<GetUserResponseDto>>> getUsersList(
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "userId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "", required = false) Integer userRoleId,
            @RequestParam(defaultValue = "", required = false) Integer statusId,
            @RequestParam(defaultValue = "", required = false) Integer userId,
            @RequestParam(defaultValue = "", required = false) Integer branchId,
            @RequestParam(defaultValue = "", required = false) Integer monthFilterFlag,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate,
            @RequestParam(required = false, defaultValue = "true") String isActive,
            @RequestParam(required = false, defaultValue = "false") String isAuthorizationList
    ) {
        if (!Objects.isNull(fromDate) && Objects.isNull(toDate)) {
            throw new BadRequestAlertException("PLEASE SELECT TO DATE!", ENTITY_NAME, "FILTER_DATES");
        } else if (!Objects.isNull(toDate) && Objects.isNull(fromDate)) {
            throw new BadRequestAlertException("PLEASE SELECT FROM DATE", ENTITY_NAME, "FILTER_DATES");
        }

        boolean statusFlag;
        statusFlag = isActive.equalsIgnoreCase("true");

        ApiResponseDto<List<GetUserResponseDto>> response = admUserService.getUsersList(
                page,
                per_page,
                sort,
                direction,
                search,
                userRoleId,
                statusId,
                userId,
                branchId,
                monthFilterFlag,
                fromDate,
                toDate,
                statusFlag,
                isAuthorizationList
        );
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping("/branch/{branchId}/departments")
    public ResponseEntity<ApiResponseDto<List<UsMDepartmentDto>>> getDepartmentForBranchId(
            @PathVariable Integer branchId,
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "brachDeptId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        ApiResponseDto<List<UsMDepartmentDto>> response = brachDepartmentService.getDepartmentForBranchId(branchId, page, per_page, sort, direction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping("/getUsersWithoutLoginUser")
    public ResponseEntity<ApiResponseDto<List<AdMUserDetailsDto>>> getUserListWithoutLoginUser(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "userId", required = false) String sort
    ) {
        ApiResponseDto<List<AdMUserDetailsDto>> response = admUserService.getUserListWithoutLoginUser(page, per_page, search, direction, sort);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/user-details")
    public ResponseEntity<ApiResponseDto<List<AdMUserDetailsDto>>> getAdMUserDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "userId", required = false) String sort,
            @RequestParam(defaultValue = "", required = false) String employeeId,
            @RequestParam(defaultValue = "", required = false) String userRoleId,
            @RequestParam(defaultValue = "", required = false) String statusId,
            @RequestParam(defaultValue = "", required = false) String branchId,
            @RequestParam(defaultValue = "", required = false) String departmentId
    ) {
        return admUserService.getAdMUserDetails(page, per_page, search, direction, sort, employeeId, userRoleId, statusId, branchId, departmentId);
    }

    @GetMapping("/branch/{branchId}")
    public ResponseEntity<UsMBranchDto> getUsMBranchById(@PathVariable Integer branchId) {
        return branchService.getUsMBranchById(branchId);
    }

    @GetMapping("/user-list/fdd")
    public ResponseEntity<List<AdMUserDto>> getUsersForDropdown() {
        return admUserService.getUsersForDropdown();
    }

    @GetMapping("/getStatusList")
    public ResponseEntity<ApiResponseDto<List<UsRStatusDetailDto>>> getAllStatus(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "statusId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        ApiResponseDto<List<UsRStatusDetailDto>> response = statusDetailService.getAllStatus(page, per_page, search, sort, direction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/branch-dept-validate/branch/{branchId}/department/{departmentId}")
    public ResponseEntity<Boolean> getValidationStatus(@PathVariable Integer branchId, @PathVariable Integer departmentId) {
        return brachDepartmentService.getValidationStatus(branchId, departmentId);
    }

    @GetMapping("/adMUser/{userId}")
    public ResponseEntity<AdMUserDto> getAdMUserByUserId(@PathVariable Integer userId) {
        return admUserService.getAdMUserByUserId(userId);
    }

    @PutMapping("/adMUser/{userId}")
    public ResponseEntity<AdMUserDto> updateAdMUser(@PathVariable Integer userId, @Valid @RequestBody AdMUserDto adMUserDto) {
        return admUserService.updateAdMUser(userId, adMUserDto);
    }

    @DeleteMapping("/adMUser/{userId}")
    public ResponseEntity<AdMUserDto> deleteAdMUser(@PathVariable Integer userId) {
        return admUserService.deleteAdMUser(userId);
    }

    @DeleteMapping("/adMUser/restrict/{userId}")
    public ResponseEntity<AdMUserDto> restrictAdMUser(@PathVariable Integer userId) {
        return admUserService.restrictAdMUser(userId);
    }

    @PostMapping("/change-password")
    public ResponseEntity<String> passwordReset(@RequestBody PasswordResetDto passwordResetDto, @RequestParam String userName) {
        log.debug("REST request to reset password : {}", passwordResetDto);
        return admUserService.passwordReset(passwordResetDto, userName);
    }

    @GetMapping("/module/{moduleId}/user/{userId}/sidebar-menu")
    public ResponseEntity<SideBarMenuDto> getSidebarMenu(@PathVariable Integer userId, @PathVariable Integer moduleId) {
        return userRestrictionService.getSidebarMenu(userId, moduleId);
    }

    @GetMapping("/module/{moduleId}/user/{userId}/side-menu")
    public ResponseEntity<List<SideMenuDto>> getSideMenuList(@PathVariable Integer userId, @PathVariable Integer moduleId) {
        return userRestrictionService.getSideMenuList(userId, moduleId);
    }

    @GetMapping("/branch/fdd")
    public ResponseEntity<List<UsMBranchDto>> getBranchesForDropdown() {
        return branchService.getBranchesForDropdown();
    }

    @GetMapping("/userRole/fdd")
    public ResponseEntity<List<UsMUserRoleDto>> getUserRolesForDropdown() {
        return userRoleService.getUserRolesForDropdown();
    }

    @GetMapping("/user-type/fdd")
    public ResponseEntity<List<UsRUserIdTypeDto>> getUserTypesForDropdown() {
        return userIdTypeService.getUserTypesForDropdown();
    }

    @PostMapping("/user-type")
    public ResponseEntity<UsRUserIdTypeDto> createUserIdType(@Valid @RequestBody UserIdTypeReqDto userIdTypeReqDto) {
        return userIdTypeService.createUserIdType(userIdTypeReqDto);
    }

    @GetMapping("/user-type/{userTypeId}")
    public ResponseEntity<UsRUserIdTypeDto> getUserIdType(@PathVariable Integer userTypeId) {
        return userIdTypeService.getUserIdType(userTypeId);
    }

    @GetMapping("/user-type")
    public ResponseEntity<ApiResponseDto<List<UsRUserIdTypeDto>>> getAllUserIdType(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "userTypeId", required = false) String sort,
            @RequestParam(defaultValue = "", required = false) Integer loginTypeId
    ) {
        return userIdTypeService.getAllUserIdType(page, per_page, search, direction, sort, loginTypeId);
    }

    @PutMapping("/user-type/{userTypeId}")
    public ResponseEntity<UsRUserIdTypeDto> updateUserIdType(@PathVariable Integer userTypeId, @RequestBody UsRUserIdTypeDto usRUserIdTypeDto) {
        return userIdTypeService.updateUserIdType(userTypeId, usRUserIdTypeDto);
    }

    @DeleteMapping("/user-type/{userTypeId}")
    public ResponseEntity<Integer> deleteUserIdType(@PathVariable Integer userTypeId) {
        return userIdTypeService.deleteUserIdType(userTypeId);
    }

    @GetMapping("/login-type/fdd")
    public ResponseEntity<List<UsRLoginTypeDto>> getLoginTypesForDropdown() {
        return loginTypeService.getLoginTypesForDropdown();
    }

    @PutMapping("/roleRestriction/role/{userRoleId}/menuAction/{menuActionId}")
    public ResponseEntity<ResponseDto> updateRoleRestriction(@PathVariable Integer userRoleId, @PathVariable Integer menuActionId) {
        return menuAllowedMapService.updateRoleRestriction(userRoleId, menuActionId);
    }

    @PostMapping("/nicValidation-with-dob")
    public ResponseEntity<NicResponseDto> validateNicWithDob(@RequestBody NicValidationDto nicValidationDto) {
        log.info("INSIDE APPLICATION_CONTROLLER: validateNicWithDob METHOD: {}", nicValidationDto);

        System.out.println(nicValidationDto.getNic());
        return admUserService.validateNicWithDob(nicValidationDto);
    }

    // 2024-01-19

    @GetMapping("/department-list/fdd")
    public ResponseEntity<List<UsMDepartmentDto>> getDepartmentForDropdown() {
        return departmentService.getDepartmentForDropdown();
    }

    @GetMapping("/branch-department-not-assigned-to-user/fdd/userId/{userId}")
    public ResponseEntity<List<UsRBrachDepartmentDto>> getBranchDepartmentForDropdown(@PathVariable Integer userId) {
        return brachDepartmentService.getBranchDepartmentForDropdown(userId);
    }

    @PostMapping("/branch-department")
    public ResponseEntity<UsRBrachDepartmentDto> createBranchDepartment(@Valid @RequestBody BranchDepartmentCreateReqDto branchDepartmentCreateReqDto) {
        return brachDepartmentService.createBranchDepartment(branchDepartmentCreateReqDto);
    }

    @GetMapping("/branch-department")
    public ResponseEntity<ApiResponseDto<List<UsRBrachDepartmentDto>>> getAllBranchDepartment(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "brachDeptId", required = false) String sort,
            @RequestParam(defaultValue = "", required = false) Integer branchId,
            @RequestParam(defaultValue = "", required = false) Integer departmentId
    ) {
        return brachDepartmentService.getAllBranchDepartment(page, per_page, search, direction, sort, branchId, departmentId);
    }

    @GetMapping("/branch-department/{brachDeptId}")
    public ResponseEntity<UsRBrachDepartmentDto> getBranchDepartmentById(@PathVariable Integer brachDeptId) {
        return brachDepartmentService.getBranchDepartmentById(brachDeptId);
    }

    @PutMapping("/branch-department/{brachDeptId}")
    public ResponseEntity<UsRBrachDepartmentDto> updateBranchDepartment(@PathVariable Integer brachDeptId, @RequestBody UsRBrachDepartmentDto usRBrachDepartmentDto) {
        return brachDepartmentService.updateBranchDepartment(brachDeptId, usRBrachDepartmentDto);
    }

    @DeleteMapping("/branch-department/{brachDeptId}")
    public ResponseEntity<Integer> deleteBranchDepartment(@PathVariable Integer brachDeptId) {
        return brachDepartmentService.deleteBranchDepartment(brachDeptId);
    }

    @GetMapping("/department-not-assigned-to-branch/fdd/branchId/{branchId}")
    public ResponseEntity<List<UsMDepartmentDto>> getDepartmentNotAssignedToBranch(@PathVariable Integer branchId) {
        return departmentService.getDepartmentNotAssignedToBranch(branchId);
    }

    @PostMapping("/user-branch")
    public ResponseEntity<UsRUserBranchDto> createUserBranch(@Valid @RequestBody UserBranchCreateReqDto userBranchCreateReqDto) {
        return brachDepartmentService.createUserBranch(userBranchCreateReqDto);
    }

    @GetMapping("/user-branch")
    public ResponseEntity<ApiResponseDto<List<UsRUserBranchDto>>> getAllUserBranch(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "employeeBranchId", required = false) String sort,
            @RequestParam(defaultValue = "", required = false) Integer userId,
            @RequestParam(defaultValue = "", required = false) Integer brachDeptId,
            @RequestParam(defaultValue = "", required = false) Integer statusId
    ) {
        return brachDepartmentService.getAllUserBranch(page, per_page, search, direction, sort, userId, brachDeptId, statusId);
    }

    @GetMapping("/user-branch/{employeeBranchId}")
    public ResponseEntity<UsRUserBranchDto> getUserBranchById(@PathVariable Integer employeeBranchId) {
        return brachDepartmentService.getUserBranchById(employeeBranchId);
    }

    @PutMapping("/user-branch/{employeeBranchId}")
    public ResponseEntity<UsRUserBranchDto> updateUserBranch(@PathVariable Integer employeeBranchId, @RequestBody UsRUserBranchDto usRUserBranchDto) {
        return brachDepartmentService.updateUserBranch(employeeBranchId, usRUserBranchDto);
    }

    @DeleteMapping("/user-branch/{employeeBranchId}")
    public ResponseEntity<Integer> deleteUserBranch(@PathVariable Integer employeeBranchId) {
        return brachDepartmentService.deleteUserBranch(employeeBranchId);
    }

    @PostMapping("/branch")
    public ResponseEntity<UsMBranchDto> createBranch(@Valid @RequestBody BranchCreateReqDto branchCreateReqDto) {
        return branchService.createBranch(branchCreateReqDto);
    }

    @PutMapping("/branch/{branchId}")
    public ResponseEntity<UsMBranchDto> updateBranch(@PathVariable Integer branchId, @RequestBody UsMBranchDto usMBranchDto) {
        return branchService.updateBranch(branchId, usMBranchDto);
    }

    @PostMapping("/department")
    public ResponseEntity<UsMDepartmentDto> createDepartment(@Valid @RequestBody DepartmentCreateReqDto departmentCreateReqDto) {
        return departmentService.createDepartment(departmentCreateReqDto);
    }

    @PutMapping("/department/{departmentId}")
    public ResponseEntity<UsMDepartmentDto> updateDepartment(@PathVariable Integer departmentId, @RequestBody UsMDepartmentDto usMDepartmentDto) {
        return departmentService.updateDepartment(departmentId, usMDepartmentDto);
    }

    @GetMapping("/get-all-users")
    public ResponseEntity<ApiResponseDto<List<GetUserResponseDto>>> getAllUserList(
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "userId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(required = false, defaultValue = "") String search
    ) {
        return admUserService.getAllUserList(page, per_page, sort, direction, search);
    }

    //User Controller

    @PutMapping("/user/{userType}/updateUser")
    public ResponseEntity<ResponseDto> updateUser(@RequestBody AdMUserDto adMUserDto, @PathVariable String userType) {
        log.info("UserController => updateUser() => {}, userType : {}", adMUserDto, userType);
        return userService.updateUser(adMUserDto, userType);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<AdMUserDto> getOneUser(@PathVariable Integer userId) {
        log.info("UserController => getOneUser() => userId : {}", userId);
        return userService.getOneUser(userId);
    }

    @GetMapping("/user/getByUserName/{userName}")
    public ResponseEntity<AdMUserDto> findUserByUserName(@PathVariable String userName) {
        log.info("UserController => findUserByUserName() => userName : {}", userName);
        return userService.findUserByUserName(userName);
    }

    @DeleteMapping("/user/inactivate/{userId}")
    public ResponseEntity<String> inactivateUser(@PathVariable Integer userId) {
        log.info("UserController => inactivateUser() => userId : {}", userId);
        return userService.inactivateUser(userId);
    }

    @GetMapping("/user/all")
    public ResponseEntity<ApiResponseDto<List<AdMUserDto>>> getAllUsers(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "userId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "", required = false) String search
    ) {
        log.info("UserController => getAllUsers() => page : {}, per_page : {}, sort : {}, direction : {}, search : {}", page, per_page, sort, direction, search);
        return userService.getAllUsers(page, per_page, sort, direction, search);
    }

    @GetMapping("/user/fdd")
    public ResponseEntity<List<AdMUserDto>> getAllUsersForDropdown() {
        log.info("UserController => getAllUsersForFdd()");
        return userService.getAllUsersForDropdown();
    }

    @GetMapping("/user/getUser/{userId}")
    public ResponseEntity<AdMUserDetailsDto> getUserByUserID(@PathVariable Integer userId) {
        return userService.getUserByUserID(userId);
    }

    //Global Details Controller

    @PostMapping("/global-details")
    public ResponseEntity<GlobalDetailsDto> saveGlobaldetails(@RequestBody GlobalDetailsDto globalDetailsDto) {

        GlobalDetailsDto savedDto = globalDetailsService.saveGlobalDetails(globalDetailsDto);
        return new ResponseEntity<>(savedDto, HttpStatus.CREATED);

    }

    @GetMapping("/global-details/{id}")
    public ResponseEntity<ApiResponseDto<GlobalDetailsDto>> getGlobaldetails(@PathVariable Integer id) {
        ApiResponseDto<GlobalDetailsDto> response = new ApiResponseDto<>();
        response.setResult(globalDetailsService.getById(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/global-details/{id}")
    public ResponseEntity<GlobalDetailsDto> updateGlobaldetails(@PathVariable Integer id, @RequestBody GlobalDetailsDto globalDetailsDto) {
        GlobalDetailsDto updateDto = globalDetailsService.updateGlobalDetails(id, globalDetailsDto);
        return new ResponseEntity<>(updateDto, HttpStatus.CREATED);
    }

    @GetMapping("/global-details")
    public ResponseEntity<ApiResponseDto<List<GlobalDetailsDto>>> getAllDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "globalID", required = false) String sort,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "acs", required = false) String direction
    ) {
        ApiResponseDto<List<GlobalDetailsDto>> response = globalDetailsService.getAllGlobalDetals(page, per_page, sort, search, direction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    //Screen Controller

//    @PostMapping("/screen/create-attribute")
//    public ResponseEntity<UsMScreenAttributesDto> createAttribute(@RequestBody UsMScreenAttributesDto usMScreenAttributesDto) {
//        UsMScreenAttributesDto response = screenAttributesService.createAttribute(usMScreenAttributesDto);
//        return new ResponseEntity<>(response, HttpStatus.CREATED);
//    }
//
//    @GetMapping("/screen/getAllAttributes")
//    public ResponseEntity<ApiResponseDto<List<UsMScreenAttributesDto>>> getAllAttributes(
//            @RequestParam(defaultValue = "0", required = false) Integer page,
//            @RequestParam(defaultValue = "10", required = false) Integer per_page,
//            @RequestParam(defaultValue = "", required = false) String search,
//            @RequestParam(defaultValue = "screenAttributeId", required = false) String sort,
//            @RequestParam(defaultValue = "asc", required = false) String direction
//    ) {
//        ApiResponseDto<List<UsMScreenAttributesDto>> response = screenAttributesService.getAllAttributes(page, per_page, search, sort, direction);
//        return new ResponseEntity<>(response, HttpStatus.OK);
//    }
//
//    @PostMapping("/screen/create-Attribute-Access")
//    public ResponseEntity<List<UsMScreenViewMgtDto>> createAttributeAccess(@RequestBody AttributeListDto attributeListDto) {
//
//        List<UsMScreenViewMgtDto> response = screenViewMgtService.createAttributeAccess(attributeListDto);
//        return new ResponseEntity<>(response, HttpStatus.CREATED);
//    }

    //User test controller

    @PostMapping("/user-test/generate-password")
    public ResponseEntity<String> generatePassword(
            @RequestParam(defaultValue = "") String password
    ) {
        String pass = PasswordUtils.encodePassword(password);
        return ResponseEntity.ok(pass);
    }

    //Activity Controller

    @PostMapping("/api-log/create")
    public ResponseEntity<UsLActivityLogDto> createActivityLog(@RequestBody UsLActivityLogDto activityLogDto) {
        log.info("Inside ActivityLogController: createActivityLog method");
        return activityLogService.createActivityLog(activityLogDto);
    }

    @GetMapping("/api-log/view")
    public ResponseEntity<ApiResponseDto<List<UsLActivityLogDto>>> getAllActivityLogs(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "activityId", required = false) String sort,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "", required = false) Integer actionId,
            @RequestParam(defaultValue = "", required = false) String username,
            @RequestParam(defaultValue = "", required = false) Integer branchId
    ) {
        log.info("Inside ActivityLogController: getAllActivityLogs method");
        return activityLogService.getAllActivityLogs(page, per_page, sort, search, direction, actionId, username, branchId);
    }

    @PutMapping("/api-log/update/activityId/{activityId}")
    public ResponseEntity<UsLActivityLogDto> updateActivityLog(
            @PathVariable Integer activityId,
            @RequestBody UsLActivityLogDto activityLogDto
    ) {
        log.info("Inside ActivityLogController: updateActivityLog method");
        return activityLogService.updateActivityLog(activityId, activityLogDto);
    }

    @GetMapping("/api-log/view/activityId/{activityId}")
    public ResponseEntity<UsLActivityLogDto> activityLogGetById(@PathVariable Integer activityId) {
        log.info("Inside ActivityLogController: activityLogGetById method");
        return activityLogService.activityLogGetById(activityId);
    }

    //Note Log Controller

//    @GetMapping("/note-log/list")
//    public ResponseEntity<ApiResponseDto<List<UsLNoteLogDto>>> getAllNoteLogs(
//            @RequestParam(defaultValue = "0", required = false) int page,
//            @RequestParam(defaultValue = "10", required = false) Integer per_page,
//            @RequestParam(defaultValue = "noteId", required = false) String sort,
//            @RequestParam(defaultValue = "", required = false) String search,
//            @RequestParam(defaultValue = "asc", required = false) String direction,
//            @RequestParam(required = false) Integer userId,
//            @RequestParam(required = false) Integer branchId,
//            @RequestParam(required = false) Integer departmentId,
//            @RequestParam(required = false) Integer menuId,
//            @RequestParam(required = false) Integer actionId
//    ) {
//        ResponseEntity response = noteLogService.getAllNoteLogs(page, per_page, sort, search, direction, userId, branchId, departmentId, menuId, actionId);
//        return response;
//    }
//
//    @PostMapping("/note-log")
//    public ResponseEntity<UsLNoteLogDto> createNoteLog(@RequestBody NoteLogCreateReqDto noteLogCreateReqDto) {
//        ResponseEntity response = noteLogService.createNoteLog(noteLogCreateReqDto);
//        return response;
//    }
//
//    @GetMapping("/note-log/get/{noteId}")
//    public ResponseEntity<UsLNoteLogDto> getNoteLogById(@PathVariable Integer noteId) {
//        ResponseEntity response = noteLogService.getNoteLogById(noteId);
//        return response;
//    }

    // Transaction Controller

    @PostMapping("/transaction/createMenuAction")
    public ResponseEntity<UsTMenuActionDto> createMenuAction(@RequestBody UsTMenuActionDto usTMenuActionDto) {
        ResponseEntity<UsTMenuActionDto> response = menuActionService.createMenuAction(usTMenuActionDto);
        return response;
    }

    @GetMapping("/transaction/getMenuActionList")
    public ResponseEntity<ApiResponseDto<List<UsTMenuActionDto>>> getAllMenuActions(
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "menuActionId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(required = false) Integer statusId,
            @RequestParam(required = false) Integer moduleId,
            @RequestParam(required = false) Integer menuId,
            @RequestParam(required = false) Integer actionId
    ) {
        ApiResponseDto<List<UsTMenuActionDto>> response = menuActionService.getAllMenuActions(page, per_page, sort, direction, search, statusId, moduleId, menuId, actionId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/transaction/updateMenuAction")
    public ResponseEntity<UsTMenuActionDto> updateMenuAction(@RequestBody UsTMenuActionDto usTMenuAction) {
        ResponseEntity<UsTMenuActionDto> response = menuActionService.updateMenuAction(usTMenuAction);
        return response;
    }

    @GetMapping("/transaction/getMenuAction/{menuActionId}")
    public ResponseEntity<UsTMenuActionDto> getMenuAction(@PathVariable Integer menuActionId) {
        ResponseEntity<UsTMenuActionDto> response = menuActionService.getMenuAction(menuActionId);
        return response;
    }

//    @GetMapping("/transaction/getMenuActionsList")
//    public ResponseEntity<ApiResponseDto<List<UsTMenuActionDto>>> getAllMenuActionsList(
//            @RequestParam(defaultValue = "0", required = false) int page,
//            @RequestParam(defaultValue = "10", required = false) Integer per_page,
//            @RequestParam(defaultValue = "menuActionId", required = false) String sort,
//            @RequestParam(defaultValue = "asc", required = false) String direction,
//            @RequestParam(defaultValue = "", required = false) String search,
//            @RequestParam(required = false, defaultValue = "") String[] statusId,
//            @RequestParam(required = false, defaultValue = "") String[] moduleId,
//            @RequestParam(required = false, defaultValue = "") String[] menuId,
//            @RequestParam(required = false, defaultValue = "") String[] actionId,
//            @RequestParam(required = false,defaultValue = "") String menuActionId,
//            @RequestParam(required = false,defaultValue = "") String name
//    ) {
//        ApiResponseDto<List<UsTMenuActionDto>> response = menuActionService.getAllMenuActionsList(page,per_page,sort,direction,search,statusId,moduleId,menuId,actionId,menuActionId,name);
//        return new ResponseEntity<>(response, HttpStatus.OK);
//    }

    @GetMapping("/transaction/getMenuActionsList")
    public ResponseEntity<ApiResponseDto<List<UsTMenuActionDto>>> getAllMenuActionsList(
            @RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "menuActionId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(required = false, defaultValue = "") String[] statusId,
            @RequestParam(required = false, defaultValue = "") String[] moduleId,
            @RequestParam(required = false, defaultValue = "") String[] menuId,
            @RequestParam(required = false, defaultValue = "") String[] actionId
    ) {
        ApiResponseDto<List<UsTMenuActionDto>> response = menuActionService.getAllMenuActionsList(page, per_page, sort, direction, search, statusId, moduleId, menuId, actionId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/transaction/menuAction-tran-create-test-api")
    public ResponseEntity<List<UsTMenuActionDto>> createMenuActionTranTestApi(@RequestBody MenuActionCreateTestApiDto menuActionCreateTestApiDto) {
        ResponseEntity response = menuActionService.createMenuActionTranTestApi(menuActionCreateTestApiDto);
        return response;
    }

    @GetMapping("/department/{departmentId}")
    public ResponseEntity<UsMDepartmentDto> getDepartmentById(@PathVariable Integer departmentId) {
        return departmentService.getDepartmentById(departmentId);
    }


}