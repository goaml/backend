package com.olak.goaml.userMGT.rest;

import com.olak.goaml.error.BadRequestAlertException;
import com.olak.goaml.userMGT.dto.authenticate.LoginDto;
import com.olak.goaml.userMGT.dto.other.*;
import com.olak.goaml.userMGT.dto.user.ForgotPasswordDto;
import com.olak.goaml.userMGT.dto.user.UserDetailsDto;
import com.olak.goaml.userMGT.dto.user.UserRequestDto;
import com.olak.goaml.userMGT.service.other.OTPService;
import com.olak.goaml.userMGT.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;


@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthController {

    private final UserService userService;
    private final OTPService otpService;

    public AuthController(UserService userService, OTPService otpService) {
        this.userService = userService;
        this.otpService = otpService;
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> login(@RequestBody LoginDto loginDto) {
        return userService.login(loginDto);
    }

    @Transactional
    @PostMapping("/{userType}/register")
    public ResponseEntity<ResponseDto> createUser(@RequestBody UserRequestDto userRequestDto, @PathVariable String userType) {

        log.info("UserController => createUser() => {}, userType : {}", userRequestDto, userType);
        return userService.createUser(userType, userRequestDto);
    }

    @PostMapping("/validate-token")
    public ResponseEntity<TokenValidateDto> validateToken(@RequestBody ValidateTokenRequestDto validateTokenRequestDto) {
        return userService.validateToken(validateTokenRequestDto);
    }


    @GetMapping("/user-data/user/{userId}/branch/{branchId}")
    public ResponseEntity<UserDataDto> getUserBranchDept(@PathVariable Integer userId, @PathVariable Integer branchId) {
        return userService.getUserBranchDept(userId, branchId);
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<String> forgotPassword(@RequestBody ForgotPasswordDto forgotPasswordDto) {
        return userService.forgotPassword(forgotPasswordDto);
    }

    @GetMapping("/current-user/details")
    public ResponseEntity<UserDetailsDto> details(@RequestHeader("Authorization") String token) {
        return userService.userDetails(token);
    }

    @PostMapping("/create-approve-user/{userTypeId}")
    public ResponseEntity<ResponseDto> createAndApproveUser(@PathVariable Integer userTypeId, @RequestBody CreateApproveUserDto createApproveUserDto) {
        return userService.createAndApproveUser(userTypeId, createApproveUserDto);
    }

    @PostMapping("/send-otp/{userId}")
    public ResponseEntity<ResponseDto> sendOTP(@PathVariable String userId) {
        ResponseDto responseDto = new ResponseDto();
        otpService.sendOTP(userId);

        responseDto.setMessage("OTP sent successfully");
        return ResponseEntity.ok(responseDto);
    }

    @PostMapping("/validate-otp/{userId}/{otp}")
    public ResponseEntity<ResponseDto> validateOTP(@PathVariable String userId, @PathVariable String otp) {
        ResponseDto responseDto = new ResponseDto();
        boolean isValid = otpService.validateOTP(userId, otp);
        if (isValid) {
            responseDto.setMessage("OTP validated successfully");
            return ResponseEntity.ok(responseDto);
        } else {
            throw new BadRequestAlertException("OTP Validation Failed", ENTITY_NAME, "OTP_VALIDATION_FAILED");
        }
    }

}