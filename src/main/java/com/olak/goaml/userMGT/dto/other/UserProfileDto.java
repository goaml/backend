package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserProfileDto {

    private Integer userId;
    private String userName;
    private String description;
    private String empEmail;
    private String empMobileNumber;
    private LocalDate userExpDate;
    private LocalDate passwordExpDate;
    private String firstName;
    private String lastName;
    private Integer passwordExpDateCount;

}