package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DepartmentCreateReqDto {
    private String contactNumber;
    private String deptAddress;
    private String deptLocation;
    @NotNull
    @NotEmpty
    @NotBlank
    private String deptName;
    @NotNull
    @NotEmpty
    @NotBlank
    private String desc;
    private String email;
    @NotNull
    @NotEmpty
    @NotBlank
    private Integer branchId;
}
