package com.olak.goaml.userMGT.dto.reference;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsRStatusDetailDto {

    private Integer statusId;
    private String statusCode;
    private String statusDesc;
    private String statusName;

}
