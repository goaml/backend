package com.olak.goaml.userMGT.dto.organization;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Ad m branch currency dto.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdMBranchCurrencyDto {

    private Integer branchCurrencyId;
    private Integer id;
    private String currencyCode;
    private Boolean isBase;
    private Boolean isActive;
    private Integer branchId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdMBranchCurrencyDto that = (AdMBranchCurrencyDto) o;

        if (getBranchCurrencyId() != null ? !getBranchCurrencyId().equals(that.getBranchCurrencyId()) : that.getBranchCurrencyId() != null)
            return false;
        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getCurrencyCode() != null ? !getCurrencyCode().equals(that.getCurrencyCode()) : that.getCurrencyCode() != null)
            return false;
        if (getIsBase() != null ? !getIsBase().equals(that.getIsBase()) : that.getIsBase() != null) return false;
        if (getIsActive() != null ? !getIsActive().equals(that.getIsActive()) : that.getIsActive() != null)
            return false;
        return getBranchId() != null ? getBranchId().equals(that.getBranchId()) : that.getBranchId() == null;
    }

    @Override
    public int hashCode() {
        int result = getBranchCurrencyId() != null ? getBranchCurrencyId().hashCode() : 0;
        result = 31 * result + (getId() != null ? getId().hashCode() : 0);
        result = 31 * result + (getCurrencyCode() != null ? getCurrencyCode().hashCode() : 0);
        result = 31 * result + (getIsBase() != null ? getIsBase().hashCode() : 0);
        result = 31 * result + (getIsActive() != null ? getIsActive().hashCode() : 0);
        result = 31 * result + (getBranchId() != null ? getBranchId().hashCode() : 0);
        return result;
    }
}
