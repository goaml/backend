package com.olak.goaml.userMGT.dto.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DesignationDto {

    private String created_by;
    private Timestamp created_on;
    private String updated_by;
    private Timestamp updated_on;
    private Integer designationId;
    private String designationName;

}
