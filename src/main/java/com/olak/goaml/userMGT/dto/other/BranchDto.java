package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BranchDto {

    private Integer branchId;
    private Integer departmentId;
    private LocalDate fromDate;
    private LocalDate toDate;

}
