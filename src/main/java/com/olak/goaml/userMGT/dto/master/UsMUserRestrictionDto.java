package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import com.olak.goaml.userMGT.dto.transaction.UsTMenuActionDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsMUserRestrictionDto {

    //    private String created_by;
//    private Timestamp created_on;
//    private String updated_by;
//    private Timestamp updated_on;
    private Integer userRestrictionId;
    private LocalDate from;
    private String reason;
    private LocalDate to;
    private Double transactionLimit;

    private Integer statusId;
    private Integer branchId;
    private Integer menuActionId;
    private Integer userId;
    private Integer employeeBranchId;

    private UsRStatusDetailDto usRStatusDetail;
    private UsMBranchDto usMBranch;
    private UsTMenuActionDto usTMenuAction;
}
