package com.olak.goaml.userMGT.dto.other;

import com.olak.goaml.userMGT.dto.master.UsMUserRestrictionDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserRestrictionReqDto {

    private List<UsMUserRestrictionDto> userRestrictionList;

}
