package com.olak.goaml.userMGT.dto.user;

import lombok.*;

import java.util.List;

/**
 * The type User function dto.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserFunctionDto {
    private Integer userId;
    private List<Integer> functionIds;
}
