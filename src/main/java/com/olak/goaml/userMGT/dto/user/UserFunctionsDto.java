package com.olak.goaml.userMGT.dto.user;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserFunctionsDto implements Serializable {

    private Integer functionId;
    private String functionName;
    private String functionCode;
}
