package com.olak.goaml.userMGT.dto.other;

import com.olak.goaml.userMGT.dto.master.UsMBranchDto;
import com.olak.goaml.userMGT.dto.master.UsMDepartmentDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AuthenticationResponse {
    private String accessToken;
    private int expiresIn;
    private Integer userId;
    private String userName;
    private String userRoleName;
    private Integer userRoleId;
    private List<UsMBranchDto> branchList;
    private List<UsMDepartmentDto> departmentList;
    private String firstName;
    private String lastName;
    private String email;
    private String mobile;
    private Integer userResetDateCount;
    private Integer passwordResetDateCount;
    private Boolean isFirstLogin;
    private Integer userTypeId;
    private String userType;
    private Integer companyId;
    private Boolean isExistingCompany;
    private String referenceNo;
    private Integer businessTypeId;
}
