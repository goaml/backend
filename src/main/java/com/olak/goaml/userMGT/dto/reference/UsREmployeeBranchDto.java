package com.olak.goaml.userMGT.dto.reference;

import com.olak.goaml.userMGT.dto.master.UsMBranchDto;
import com.olak.goaml.userMGT.dto.master.UsMUserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsREmployeeBranchDto {


    private int employeeBranchId;
    private Date from;
    private Date to;
    private Integer statusId;
    private Integer userId;
    private Integer BranchId;

    private UsMBranchDto usMBranch;
    private UsMUserDto usMUser;
    private UsRStatusDetailDto usRUserStatusDetail;


}
