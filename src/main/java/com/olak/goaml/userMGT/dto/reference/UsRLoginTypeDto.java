package com.olak.goaml.userMGT.dto.reference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsRLoginTypeDto {
    private Integer loginTypeId;
    private String typeCode;
    private String typeDescription;
    private Boolean isActive;
}
