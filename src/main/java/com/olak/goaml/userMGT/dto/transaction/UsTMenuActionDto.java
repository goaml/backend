package com.olak.goaml.userMGT.dto.transaction;

import com.olak.goaml.userMGT.dto.master.UsMActionDto;
import com.olak.goaml.userMGT.dto.master.UsMMenuDto;
import com.olak.goaml.userMGT.dto.master.UsMModuleDto;
import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsTMenuActionDto {

    private Integer menuActionId;
    private String name;
    private String url;
    private Boolean isMenu;
    private String moduleName;
    private String statusName;
    private String actionName;

    private Integer menuId;
    private Integer moduleId;
    private Integer statusId;
    private Integer actionId;

    private UsMActionDto usMAction;
    private UsMMenuDto usMMenu;
    private UsMModuleDto usMModule;
    private UsRStatusDetailDto usRUserStatusDetail;

}
