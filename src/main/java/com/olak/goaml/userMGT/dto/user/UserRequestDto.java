package com.olak.goaml.userMGT.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserRequestDto {

    private String description;
    private String email;
    private String mobileNo;
    private String firstName;
    private String lastName;
    private Integer userRoleId;
    private Integer branchId;
    private Integer departmentId;
    private String nic;
    private String referenceNo;

    private Integer userTypeId;
    private Integer comId;
    private Boolean isExistingCompany;


}