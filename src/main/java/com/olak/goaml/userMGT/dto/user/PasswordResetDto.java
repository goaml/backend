package com.olak.goaml.userMGT.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PasswordResetDto {
    private String currentPassword;
    private String newPassword;
    private String reNewPassword;
}
