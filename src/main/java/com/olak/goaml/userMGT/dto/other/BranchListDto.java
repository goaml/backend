package com.olak.goaml.userMGT.dto.other;

import com.olak.goaml.userMGT.dto.reference.UsRBrachDepartmentDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BranchListDto {
    private String message;
    private List<UsRBrachDepartmentDto> usRBrachDepartmentDto;
}
