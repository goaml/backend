package com.olak.goaml.userMGT.dto.master;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlobalDetailsDto {

    private String created_by;
    private Timestamp created_on;
    private String updated_by;
    private Timestamp updated_on;
    private Integer globalID;
    private String frontURL;
    private String domainDestription;
    private String coreURL;
    private LocalDateTime datetime;
    private Integer userExpirationDays;
    private Integer userLoginAttempts;
    private String encryptKey;
    private String companyName;
    private String footerTitle;
    private String versionNumber;
    private String logoPath;
    private String userMalePath;
    private String userFemalePath;
    private Integer status;
    private LocalTime plocaltime;
    private LocalDate plocaldate;
    private LocalDateTime createOn;
    private LocalDateTime lastChanged;
    private Integer userExpDate;
    private Integer passwordExpDate;
    private Integer resetDateCount;
    private LocalDate currentWorkingDate;
    private LocalDate nextWorkingDate;  // S || E || B
    private String userNotificationMode;
    private Integer passwordMinLength;
    private Integer passwordMaxLength;
    private Integer passwordNumOfSpecialCharacter;
    private Integer passwordNumOfCapitalLetters;
    private Integer passwordNumOfSimpleLetters;
    private Integer passwordNumOfDigits;
    private Boolean isMaintenanceOn;

    private String bankName;
    private String swiftCode;
    private Boolean nonBankInstitution;
    private LocalDate reportGenDate;

}
