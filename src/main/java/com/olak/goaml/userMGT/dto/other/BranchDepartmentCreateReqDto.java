package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BranchDepartmentCreateReqDto {
    @NotNull
    private Integer branchId;
    @NotNull
    private Integer departmentId;
}