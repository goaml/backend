package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MenuGroupDto {
    private String menuGroupName;
    private Integer seqNo;
    private List<SubMenuGroupDto> subGroup;
}
