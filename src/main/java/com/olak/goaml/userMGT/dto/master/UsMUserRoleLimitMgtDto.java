package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsMUserRoleLimitMgtDto {

    private String created_by;
    private Timestamp created_on;
    private String updated_by;
    private Timestamp updated_on;
    private Integer roleLimitId;
    private Double limit;

    private Integer branchId;
    private Integer menuAllowMapId;
    private Integer statusId;

    private UsMBranchDto usMBranch;
    private UsMMenuAllowedMapDto usMMenuAllowedMap;
    private UsRStatusDetailDto usRStatusDetail;

}
