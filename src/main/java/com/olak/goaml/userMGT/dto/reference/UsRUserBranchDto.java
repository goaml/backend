package com.olak.goaml.userMGT.dto.reference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsRUserBranchDto {
    private Integer employeeBranchId;
    private LocalDate from;
    private Integer isDefualtDept;
    private LocalDate to;

    private Integer userId;
    private Integer brachDeptId;
    private Integer statusId;

    private String branchName;
    private String deptName;
    private Boolean isActive;

}
