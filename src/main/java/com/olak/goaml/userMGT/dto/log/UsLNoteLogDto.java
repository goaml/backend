package com.olak.goaml.userMGT.dto.log;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsLNoteLogDto {

    private Integer noteId;
    private String menuName;
    private String actionName;
    private String username;
    private LocalDate systemDate;
    private LocalDate currentDate;
    private LocalTime time;
    private String companyId;
    private String branchName;
    private String departmentName;
    private Integer keyValueId;
    private String keyValue;
    private String comment;

    private Integer userId;
    private Integer menuId;
    private Integer actionId;
    private Integer branchId;
    private Integer departmentId;

    private Boolean isActive;

//    private AdMUserDto adMUser;
//    private UsMMenuDto usMMenu;
//    private UsMActionDto usMAction;
//    private UsMBranchDto usMBranch;
//    private UsMDepartmentDto usMDepartment;

}