package com.olak.goaml.userMGT.dto.organization;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Ad m branch contact dto.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdMBranchContactDto {

    private Integer branchContactId;
    private Integer type;
    private String number;
    private Boolean isPrimary;
    private Boolean isActive;
    private Integer branchId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdMBranchContactDto that = (AdMBranchContactDto) o;

        if (getBranchContactId() != null ? !getBranchContactId().equals(that.getBranchContactId()) : that.getBranchContactId() != null)
            return false;
        if (getType() != null ? !getType().equals(that.getType()) : that.getType() != null) return false;
        if (getNumber() != null ? !getNumber().equals(that.getNumber()) : that.getNumber() != null) return false;
        if (getIsPrimary() != null ? !getIsPrimary().equals(that.getIsPrimary()) : that.getIsPrimary() != null)
            return false;
        if (getIsActive() != null ? !getIsActive().equals(that.getIsActive()) : that.getIsActive() != null)
            return false;
        return getBranchId() != null ? getBranchId().equals(that.getBranchId()) : that.getBranchId() == null;
    }

    @Override
    public int hashCode() {
        int result = getBranchContactId() != null ? getBranchContactId().hashCode() : 0;
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getNumber() != null ? getNumber().hashCode() : 0);
        result = 31 * result + (getIsPrimary() != null ? getIsPrimary().hashCode() : 0);
        result = 31 * result + (getIsActive() != null ? getIsActive().hashCode() : 0);
        result = 31 * result + (getBranchId() != null ? getBranchId().hashCode() : 0);
        return result;
    }
}
