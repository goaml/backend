package com.olak.goaml.userMGT.dto.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsMCompanyDetailDto {
    private Integer comId;
    private String applicantName;
    private String businessRegNo;
    private String boiRegNo;
    private String companyCode;
    private String contactNo;
    private Boolean isActive;
}