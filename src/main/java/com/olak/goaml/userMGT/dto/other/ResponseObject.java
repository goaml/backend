package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseObject {
    private String code;
    private String status;
    private String description;
    private Object instructions;

    public ResponseObject(String code, String status, String description) {
        this.code = code;
        this.status = status;
        this.description = description;
    }
}
