package com.olak.goaml.userMGT.dto.user;

import com.olak.goaml.userMGT.dto.master.UsMBranchDto;
import com.olak.goaml.userMGT.dto.master.UsMDepartmentDto;
import com.olak.goaml.userMGT.dto.master.UsMUserRoleDto;
import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import com.olak.goaml.userMGT.dto.reference.UsRUserIdTypeDto;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class AdMUserDto implements Serializable {

    private static final long serialVersionUID = -2002437233789421599L;

    private Integer userId;
    private String userName;
    private String description;
    private Boolean isFirstLogin;
    private Boolean isActive;
    private String email;
    private String mobileNo;
    private LocalDate userExpDate;
    private LocalDate passwordExpDate;
    private String firstName;
    private String lastName;
    private String password;
    private String secureKey;
    private String nic;
    private String referenceNo;
    private Boolean isExistingCompany;

    @NonNull
    private Integer userRoleId;
    private Integer statusId;
    @NonNull
    private Integer branchId;
    @NonNull
    private Integer departmentId;
    @NonNull
    private Integer userTypeId;

    private Integer comId;

    private UsRUserIdTypeDto usRUserIdType;
    private UsMUserRoleDto usMUserRole;
    private UsRStatusDetailDto usRStatusDetail;
    private UsMBranchDto usMBranch;
    private UsMDepartmentDto usMDepartment;

}
