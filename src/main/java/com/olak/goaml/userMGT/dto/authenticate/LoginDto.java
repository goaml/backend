package com.olak.goaml.userMGT.dto.authenticate;

import lombok.*;

/**
 * The type Login dto.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class LoginDto {
    private String userName;
    private String password;
}
