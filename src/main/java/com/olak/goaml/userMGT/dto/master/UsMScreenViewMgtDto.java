package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.transaction.UsTMenuActionDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsMScreenViewMgtDto {

    private Integer screenViewId;
    private Boolean isVisible;

    private Integer menuActionId;
    private Integer screenAttributeId;

    private UsTMenuActionDto usTMenuAction;
    private UsMScreenAttributesDto usMScreenAttributes;

}
