package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AdMUserDetailsDto {

    private Integer userId;
    private String userName;
    private String description;
    private Boolean isFirstLogin;
    private Boolean isActive;
    private String email;
    private String mobileNo;
    private String securityKey;
    private LocalDate userExpDate;
    private LocalDate passwordExpDate;
    private Integer userRoleId;
    private Integer statusId;
    private Integer branchId;
    private Integer departmentId;
    private String defaultBranch;

}