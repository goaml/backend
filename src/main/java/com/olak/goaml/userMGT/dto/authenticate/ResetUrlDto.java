package com.olak.goaml.userMGT.dto.authenticate;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ResetUrlDto {

    private Integer userId;
    private String token;
    private String email;
}
