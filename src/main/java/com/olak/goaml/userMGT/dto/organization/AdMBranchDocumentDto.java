package com.olak.goaml.userMGT.dto.organization;

import lombok.*;

/**
 * The type Ad m branch document dto.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AdMBranchDocumentDto {
    private Integer branchDocumentId;
    private Integer funcDocumentId;
    private Integer documentTypeId;
    private Integer functionId;
    private Integer moduleId;
    private String documentPath;
    private Boolean isActive;
    private Integer branchId;
}
