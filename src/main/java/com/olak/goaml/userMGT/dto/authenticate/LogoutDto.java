package com.olak.goaml.userMGT.dto.authenticate;

import lombok.*;

/**
 * The type Logout dto.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class LogoutDto {

    private String refreshToken;
    private Boolean logoutStatus;
}
