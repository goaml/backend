package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import com.olak.goaml.userMGT.models.master.UsMBranch;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsMLocationDetailDto {

    private String created_by;
    private Timestamp created_on;
    private String updated_by;
    private Timestamp updated_on;
    private Integer locationDetailsId;
    private LocalDate endDate;
    private String locationType;
    private LocalDate startDate;
    private UsMBranch usMBranch;

    private Integer statusId;

    private UsRStatusDetailDto usRUserStatusDetail;

}