package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsMMenuDto {

    private String created_by;
    private Timestamp created_on;
    private String updated_by;
    private Timestamp updated_on;
    private Integer menuId;
    private String menuDesc;
    private String menuHashcode;
    private String menuName;
    private String menuUrl;
    private String menuIcon;
    private Integer seqNo;

    private Integer statusId;
    private Integer subGroupId;

    private UsRStatusDetailDto usRStatusDetail;
    private UsMMenuSubGroupDto usMMenuSubGroup;

}