package com.olak.goaml.userMGT.dto.authenticate;

import lombok.*;

/**
 * The type Login url dto.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class LoginUrlDto {

    private Integer userId;
    private String token;
}
