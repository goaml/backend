package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsMUserRoleDto {

    private Integer userRoleId;
    private Integer cCode;
    private String roleApprvReason;
    private String roleInBussiness;
    private String statusFlag;
    private String userRoleDesc;
    private String userRoleName;

    private Integer statusId;

    private UsRStatusDetailDto usRStatusDetail;

}
