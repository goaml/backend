package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import com.olak.goaml.userMGT.dto.transaction.UsTMenuActionDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsMMenuAllowedMapDto {

    private Integer menuAllowMapId;

    private Integer userRoleId;
    private Integer statusId;
    private Integer menuActionId;

    private UsMUserRoleDto usMUserRole;
    private UsRStatusDetailDto usRStatusDetail;
    private UsTMenuActionDto usTMenuAction;

}
