package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SideMenuDto {

    private String group;
    private Integer groupSeqNo;

    private String subGroup;
    private Integer subGroupSeqNo;

    private Integer id;
    private String title;
    private Integer menuSeqNo;
    private String url;
    private String icon;

}