package com.olak.goaml.userMGT.dto.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsMMenuSubGroupDto {
    private Integer subGroupId;
    private String subGroupName;
    private Integer seqNo;

    private Integer menuGroupId;

    private UsMMenuGroupDto usMMenuGroup;
}