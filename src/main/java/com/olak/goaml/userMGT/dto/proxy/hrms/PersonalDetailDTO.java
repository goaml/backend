package com.olak.goaml.userMGT.dto.proxy.hrms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Lob;
import java.time.LocalDate;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PersonalDetailDTO {
    private Long id;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private Long personalDetailEmpId;
    private String middleName;
    private String initials;
    private String namesDenotedByInitials;
    private String fullName;
    private String otherNames;
    private String nicOrPassport;
    private LocalDate nicOrPassportIssueDate;
    private String nicOrPassportIssuePlace;
    private LocalDate dob;
    private Integer age;
    private LocalDate marriedDate;
    private LocalDate divorcedDate;
    private String race;
    private String religion;
    private String nationality;
    private String bloodGroup;
    private String civilStatus;
    private String gender;
    private String accountStatus;
    @Lob
    private String imageUrl;
    private String placeOfBirth;

    private String epfNo;

    // new Item added =============================
    private String surName;
    private String salName;
    private String nicNo;
    private LocalDate nicIssuedDate;
    private String passportNo;
    private String passportIssuedDate;
    private LocalDate passportExpireDate;
    private String drivingLicenceNumber;
    private LocalDate drivingLicenceNumberIssuedDate;
    private LocalDate drivingLicenceExpireDate;
    private String spouseName;
    private String idNumber;
    private String address;
    private String addressTwo;
    private String addressThree;
    private String title;
    private String currentDepartment;
    // this fields not belong to this class
    private String location;
    private String designation;
    private String currentBranch;
    private String currentDept;
    private String userId;
    private String empId;
    private String isSupervisorApproved;

}
