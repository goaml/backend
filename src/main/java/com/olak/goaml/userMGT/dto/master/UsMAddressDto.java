package com.olak.goaml.userMGT.dto.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsMAddressDto {

    private String created_by;
    private Timestamp created_on;
    private String updated_by;
    private Timestamp updated_on;
    private Integer addressId;
    private String adNum;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String addressType;
    private String city;

    private Integer userId;

    private UsMUserDto usMUser;

}
