package com.olak.goaml.userMGT.dto.miscellaneous;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OtpValidateResponseDto {

    private String mobileNumber;
    private String status;
    private String statusCode;

}