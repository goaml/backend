package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserRestrictionDto {

    private Integer menuActionId;
    private String menuActionName;

    private Integer moduleId;
    private String moduleName;

    private Integer menuId;
    private String menuName;

    private Integer actionId;
    private String actionName;

    private Integer statusId;

//    private Integer employeeBranchId;
//    private LocalDate from;
//    private LocalDate to;

}
