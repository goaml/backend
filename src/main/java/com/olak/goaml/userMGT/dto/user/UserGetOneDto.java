package com.olak.goaml.userMGT.dto.user;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserGetOneDto {

    private Integer userId;
    private String userName;
    private String password;
    private String rePassword;
    private Boolean accountOptionLink;
    private Boolean accountOptionManual;
    private Boolean forceChangePassword;
    private Boolean generateOnetimePassword;
    private Boolean isActive;
    private Integer staffId;
    private Character userStatus;
}
