package com.olak.goaml.userMGT.dto.organization;

import lombok.*;

/**
 * The type Ad m branch email dto.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AdMBranchEmailDto {

    private Integer branchEmailId;
    private Integer type;
    private String email;
    private Boolean isPrimary;
    private Boolean isReceiveNotification;
    private Boolean isActive;
    private Integer branchId;
}
