package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RoleOptionsDto {

    private Integer menuActionId;
    private Boolean status;
    private String menuActionName;
    private String menuName;
    private String url;

}