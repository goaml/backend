package com.olak.goaml.userMGT.dto.reference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserIdTypeReqDto {
    @NotNull
    private String userCategory;
    @NotNull
    private Integer loginTypeId;
}
