package com.olak.goaml.userMGT.dto.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsMUserDto {

    private Integer userId;
    private String companyId;
    private LocalDate expireDate;
    private String otp;
    private String password;
    private Integer roleOwnerId;
    private String userApprvReason;
    private String username;

    private Integer employeeId;
    private Integer userRoleId;
    private Integer statusId;

//    private UsMEmployeeDto usMEmployee;
//    private UsMUserRoleDto usMUserRole;
//    private UsRStatusDetailDto usRStatusDetail;

}