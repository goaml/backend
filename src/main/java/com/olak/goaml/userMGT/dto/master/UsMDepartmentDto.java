package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsMDepartmentDto {

    //    private String created_by;
//    private Timestamp created_on;
//    private String updated_by;
//    private Timestamp updated_on;
    private Integer departmentId;
    private String contactNumber;
    private String deptAddress;
    private String deptLocation;
    private String deptName;
    private String desc;
    private String email;

    private Integer statusId;

    private UsRStatusDetailDto usRStatusDetail;

}
