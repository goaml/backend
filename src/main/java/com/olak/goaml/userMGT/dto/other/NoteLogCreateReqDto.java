package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NoteLogCreateReqDto {
    private Integer keyValueId;
    private String keyValue;
    private String comment;
    @NotNull
    private Integer menuId;
    @NotNull
    private Integer actionId;
}
