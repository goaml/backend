package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsMUserLimitMgtDto {

    private String created_by;
    private Timestamp created_on;
    private String updated_by;
    private Timestamp updated_on;
    private Integer userLimitId;
    private Double limit;

    private Integer menuId;
    private Integer userId;
    private Integer statusId;

    private UsMMenuDto usMMenu;
    private UsMUserDto usMUser;
    private UsRStatusDetailDto usRUserStatusDetail;

}
