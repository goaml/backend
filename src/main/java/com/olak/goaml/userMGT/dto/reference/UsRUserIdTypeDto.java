package com.olak.goaml.userMGT.dto.reference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsRUserIdTypeDto {
    private Integer userTypeId;
    private String userCategory;
    private Boolean isActive;
    private Integer loginTypeId;
    private UsRLoginTypeDto usRLoginType;

}
