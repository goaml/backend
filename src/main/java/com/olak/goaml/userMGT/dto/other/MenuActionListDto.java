package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuActionListDto {
    private String userRoleDesc;
    private Integer userRoleId;
    private Integer statusId;
    private String userRoleName;
    private Boolean hasAccess;
    private List<RoleOptionsDto> roleOptions;
}
