package com.olak.goaml.userMGT.dto.module;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * The type Function dto.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SyRFunctionDto implements Serializable {
    private static final long serialVersionUID = 4694807787777252830L;
    private Integer functionId;
    private String functionCode;
    private String functionName;
    private Boolean isActive;
    private Integer subModuleId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SyRFunctionDto that = (SyRFunctionDto) o;

        if (getFunctionId() != null ? !getFunctionId().equals(that.getFunctionId()) : that.getFunctionId() != null)
            return false;
        if (getFunctionCode() != null ? !getFunctionCode().equals(that.getFunctionCode()) : that.getFunctionCode() != null)
            return false;
        if (getFunctionName() != null ? !getFunctionName().equals(that.getFunctionName()) : that.getFunctionName() != null)
            return false;
        if (getIsActive() != null ? !getIsActive().equals(that.getIsActive()) : that.getIsActive() != null)
            return false;
        return getSubModuleId() != null ? getSubModuleId().equals(that.getSubModuleId()) : that.getSubModuleId() == null;
    }

    @Override
    public int hashCode() {
        int result = getFunctionId() != null ? getFunctionId().hashCode() : 0;
        result = 31 * result + (getFunctionCode() != null ? getFunctionCode().hashCode() : 0);
        result = 31 * result + (getFunctionName() != null ? getFunctionName().hashCode() : 0);
        result = 31 * result + (getIsActive() != null ? getIsActive().hashCode() : 0);
        result = 31 * result + (getSubModuleId() != null ? getSubModuleId().hashCode() : 0);
        return result;
    }
}
