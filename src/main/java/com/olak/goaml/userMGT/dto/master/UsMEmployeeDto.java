package com.olak.goaml.userMGT.dto.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsMEmployeeDto {

    private Integer employeeId;
    private String address;
    private String desc;
    private String empEmail;
    private String empMobileNumber;
    private String empNic;
    private String empNumber;
    private String firstName;
    private String gender;
    private String lastName;

    private Integer designationId;
    private Integer statusId;

//    private Designation designation;
//    private UsRStatusDetail usRStatusDetail;

}
