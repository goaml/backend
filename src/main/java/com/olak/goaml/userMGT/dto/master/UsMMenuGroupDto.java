package com.olak.goaml.userMGT.dto.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsMMenuGroupDto {
    private Integer menuGroupId;
    private String menuGroupName;
    private Integer seqNo;
    private Integer moduleId;

    private UsMModuleDto usMModule;
}
