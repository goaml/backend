package com.olak.goaml.userMGT.dto.log;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
public class UsLActivityLogDto implements Serializable {
    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;
    private Integer activityId;
    @NotNull(message = "Menu id is required.")
    private Integer menuId;
    @NotNull(message = "Menu name is required.")
    private String menuName;
    @NotNull(message = "Action id is required.")
    private Integer actionId;
    @NotNull(message = "Action name is required.")
    private String actionName;
    private String userName; // from backend
    private LocalDate systemDate; // from backend
    private LocalDate currentDate; // from backend
    private LocalTime serverTime; // from backend
    @NotNull(message = "Company Id is required.")
    private Integer companyId;
    @NotNull(message = "Logged user branch id is required.")
    private Integer branchId;
    private String branchName;
    private Integer deptId;
    private String deptName;
    private Integer keyValueId;
    private String keyValue;
    private String description;
    private Boolean isActive;
}