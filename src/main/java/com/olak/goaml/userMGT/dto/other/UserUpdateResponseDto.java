package com.olak.goaml.userMGT.dto.other;

import com.olak.goaml.userMGT.dto.reference.UsRUserBranchDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserUpdateResponseDto {

    private Integer userId;
    private String userName;
    private String description;
    //    private String wfProcessDefId;
//    private String keycloakUserId;
//    private Boolean isApprovalRequired;
//    private Boolean isManualPasswordEntry;
//    private Boolean isSendOneTimeLoginLink;
//    private Boolean isForceLogin;
//    private Boolean isFirstLogin;
//    private Boolean isGenerateOneTimePassword;
    private Boolean isActive;
    private String empEmail;
    private String empMobileNumber;
    //    private String stringPassword;
//    private LocalDate userExpDate;
//    private LocalDate passwordExpDate;
    private Integer userRoleId;
    private Integer statusId;
    private String firstName;
    private String lastName;
    private List<UsRUserBranchDto> branchList;

}
