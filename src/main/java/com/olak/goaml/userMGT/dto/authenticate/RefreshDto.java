package com.olak.goaml.userMGT.dto.authenticate;


import lombok.*;

/**
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class RefreshDto {
    private String refreshToken;
}
