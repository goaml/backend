package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GetUserResponseDto {
    private String description;
    private String empEmail;
    private String empMobileNumber;
    private String firstName;
    private String lastName;
    private Integer userRoleId;
    private Integer statusId;
    private Integer userId;
    private String userName;
    private Boolean isActive;
    private String nic;
    private String referenceNo;
    private Integer departmentId;
    private Integer branchId;
    private String statusName;
    private String userRoleName;
    private String branchName;
    private String departmentName;
    private Integer userTypeId;
    private String userTypeName;
    private Integer companyId;

}
