package com.olak.goaml.userMGT.dto.miscellaneous;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmsDto implements Serializable {

    private static final long serialVersionUID = 2249538763037905908L;

    private String mask;
    private String msg;
    private String numbers;

}
