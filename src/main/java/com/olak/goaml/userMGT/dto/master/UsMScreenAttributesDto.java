package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.transaction.UsTMenuActionDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsMScreenAttributesDto {

    private Integer screenAttributeId;
    private String attributeName;
    private Boolean isVisible;
    @NotNull
    private Integer menuActionId;

    private UsTMenuActionDto usTMenuAction;

}
