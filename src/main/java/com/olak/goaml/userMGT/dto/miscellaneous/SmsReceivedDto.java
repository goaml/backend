package com.olak.goaml.userMGT.dto.miscellaneous;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmsReceivedDto implements Serializable {

    private static final long serialVersionUID = -6060519372751524248L;

    private String reason;
    private String status;

}
