package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BranchCreateReqDto {
    private String branchAddress;
    //    @NotNull
//    @NotEmpty
//    @NotBlank
    private String branchLocation;
    @NotNull
    @NotEmpty
    @NotBlank
    private String branchName;
    private String contactNumber;
    @NotNull
    @NotEmpty
    @NotBlank
    private String desc;
    private String email;
    //    @NotNull
    private BigDecimal limitAmt;
}
