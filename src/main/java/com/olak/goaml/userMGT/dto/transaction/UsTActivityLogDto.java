package com.olak.goaml.userMGT.dto.transaction;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import com.olak.goaml.userMGT.dto.user.AdMUserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsTActivityLogDto {


    private Integer activityLogId;
    private Integer activityAffectedUser;
    private Integer activityModuleId;
    private String activityName;

    private Integer userId;
    private Integer statusId;

    private AdMUserDto adMUser;
    private UsRStatusDetailDto usRUserStatusDetail;


}
