package com.olak.goaml.userMGT.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PasswordConfirmationRequestDto {
    private String password;
    private String rePassword;

}
