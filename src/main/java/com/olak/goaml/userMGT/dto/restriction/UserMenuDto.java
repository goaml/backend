package com.olak.goaml.userMGT.dto.restriction;

import com.olak.goaml.userMGT.dto.transaction.UsTMenuActionDto;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserMenuDto {

    private UsTMenuActionDto menuAction;
    private Double transactionLimit;
}
