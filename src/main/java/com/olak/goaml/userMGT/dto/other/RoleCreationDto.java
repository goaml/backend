package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleCreationDto {

    private List<RoleOptionsDto> roleOptions;
    private String userRoleName;
    private String userRoleDescription;
    private Integer statusId;
    private String roleInBussiness;
    private Integer cCode;
    private String roleApproveReason;

}
