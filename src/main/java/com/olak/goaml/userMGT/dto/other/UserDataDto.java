package com.olak.goaml.userMGT.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDataDto {
    private Integer branchId;
    private Integer departmentId;
    private String branchName;
    private String departmentName;
    private String username;
    private String displayName;
    private String roleName;
    private String roleId;
}
