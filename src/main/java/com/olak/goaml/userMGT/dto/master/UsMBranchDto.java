package com.olak.goaml.userMGT.dto.master;

import com.olak.goaml.userMGT.dto.reference.UsRStatusDetailDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsMBranchDto {

    private String created_by;
    private Timestamp created_on;
    private String updated_by;
    private Timestamp updated_on;
    private Integer branchId;
    private String branchAddress;
    private String branchLocation;
    private String branchName;
    private String contactNumber;
    private String desc;
    private String email;
    private BigDecimal limitAmt;

    private Integer statusId;

    private UsRStatusDetailDto usRStatusDetail;
}
