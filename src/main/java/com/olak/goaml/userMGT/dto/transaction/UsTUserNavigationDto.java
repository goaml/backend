package com.olak.goaml.userMGT.dto.transaction;

import com.olak.goaml.userMGT.dto.master.UsMMenuDto;
import com.olak.goaml.userMGT.dto.user.AdMUserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsTUserNavigationDto {

    private Integer userNavigationId;
    private String remoteIp;

    private Integer menuId;
    private Integer userId;

    private UsMMenuDto usMMenu;
    private AdMUserDto adMUser;
}
