package com.olak.goaml.userMGT.dto.organization;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * The type Ad m branch dto.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AdMBranchDto implements Serializable {
    private Integer branchId;
    private String branchCode;
    private String branchName;
    private String shortName;
    private String businessRegNo;
    private String businessDescription;
    private String addressLine1;
    private String addressLine2;
    private String contactType;
    private String contactNumber;
    private String emailType;
    private String email;
    private Integer cityId;
    private String postalCodeOrZip;
    private Integer countryId;
    private Integer stateId;
    private Integer suburbOrDistrictId;
    private String timezone;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private String functionType;
    private String currencyCode;
    private String websiteUrl;
    private String wfProcessId;
    private Integer managerId;
    private Boolean isActive;
    private Integer parentBranchId;
    private Integer levelId;
    private Integer themeId;
    private String verticalBranchPath;
    private List<AdMBranchContactDto> contactList;
    private List<AdMBranchEmailDto> emailList;
    private HashMap<String, Boolean> function;
    private List<AdMBranchCurrencyDto> currencyList;
    private AdMBranchDocumentDto branchLogoDetail;

}
