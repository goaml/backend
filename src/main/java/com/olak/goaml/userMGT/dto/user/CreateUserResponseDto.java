package com.olak.goaml.userMGT.dto.user;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CreateUserResponseDto {

    private Integer userId;
    private String userName;
    private Integer userTypeId;
    private String userType;
    private Boolean isActive;
    private Integer userStatusId;
    private String userStatus;
    private Integer employeeId;
    private Integer userRoleId;
    private Integer statusId;
    private Integer branchId;
    private Integer departmentId;
}
