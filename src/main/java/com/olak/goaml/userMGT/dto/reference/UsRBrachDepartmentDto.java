package com.olak.goaml.userMGT.dto.reference;

import com.olak.goaml.userMGT.dto.master.UsMBranchDto;
import com.olak.goaml.userMGT.dto.master.UsMDepartmentDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsRBrachDepartmentDto {

    private Integer brachDeptId;

    private Integer branchId;
    private Integer departmentId;
    private String branchName;
    private Boolean isActive;
    private UsMBranchDto usMBranch;
    private UsMDepartmentDto usMDepartment;

}
