package com.olak.goaml.userMGT.dto.user;

import lombok.*;

/**
 * The type User functions details dto.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserFunctionsDetailsDto {

    private Integer functionId;
    private String functionName;
    private String functionCode;
    private Boolean isApproved;
}
