package com.olak.goaml.userMGT.proxy;

import com.olak.goaml.userMGT.dto.miscellaneous.SmsDto;
import org.json.JSONObject;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class SmsSend {

    public HttpURLConnection smsIntegration(SmsDto smsDto) {
        HttpURLConnection connection = null;
        URL tokenurl = null;
        JSONObject ob = new JSONObject();
        JSONObject dataSet = new JSONObject();

        try {
            tokenurl = new URL("https://etraders.lk:9777/eAlerts/smsintegrator/smssend");
            connection = (HttpURLConnection) tokenurl.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            dataSet.put("mask", smsDto.getMask());
            dataSet.put("msg", smsDto.getMsg());
            dataSet.put("numbers", smsDto.getNumbers());


            System.out.println(dataSet);
            try (OutputStream os = connection.getOutputStream()) {
                byte[] input = dataSet.toString().getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(ob.toString());
            out.flush();
            out.close();

        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return connection;
    }

}
