package com.olak.goaml.userMGT.proxy;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
@RestController
public class OTP {

    public HttpURLConnection sendOTP(String mobileNo) {
        HttpURLConnection connection = null;
        URL tokenurl = null;
        try {
            tokenurl = new URL("https://uat.appiaz.com:9777/eAlerts/smsintegrator/" + mobileNo + "/otp");
            connection = (HttpURLConnection) tokenurl.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json;");
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.flush();
            out.close();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return connection;

    }

    public StringBuffer validateOTP(String mobileNo, String otp) {
        try {
            String url = "https://uat.appiaz.com:9777/eAlerts/smsintegrator/" + mobileNo + "/otp/" + otp;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response;
        } catch (Exception e) {
            e.fillInStackTrace();
            return null;
        }
    }

}
