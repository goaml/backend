package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TAddress;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TPerson;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TPersonIdentification;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TPhone;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAddressDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonIdentificationDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPhoneDto;

import java.util.ArrayList;
import java.util.List;

public class TPersonService {

    public TPerson generateTPersonTag(xmlTPersonDto tPersonDto) {
        TPerson tPerson = new TPerson();

        if (tPersonDto.getGender() != null)
            tPerson.setGender(tPersonDto.getGender());

        if (tPersonDto.getTitle() != null)
            tPerson.setTitle(XmlUtils.createJAXBElement("title", tPersonDto.getTitle()));

        if (tPersonDto.getFirstName() != null)
            tPerson.setFirstName(tPersonDto.getFirstName());

//        if (tPersonDto.getMiddleName() != null)
//            tPerson.setMiddleName(XmlUtils.createJAXBElement("middle_name", tPersonDto.getMiddleName()));
//
//        if (tPersonDto.getPrefix() != null)
//            tPerson.setPrefix(tPersonDto.getPrefix());

        if (tPersonDto.getLastName() != null)
            tPerson.setLastName(tPersonDto.getLastName());

        if (tPersonDto.getBirthdate() != null)
            tPerson.setBirthdate(XmlUtils.createXMLGregorianCalendar(tPersonDto.getBirthdate().toString()));

//        if (tPersonDto.getBirthPlace() != null)
//            tPerson.setBirthPlace(tPersonDto.getBirthPlace());
//
//        if (tPersonDto.getMothersName() != null)
//            tPerson.setMothersName(tPersonDto.getMothersName());
//
//        if (tPersonDto.getAlias() != null)
//            tPerson.setAlias(tPersonDto.getAlias());

        if (tPersonDto.getSsn() != null)
            tPerson.setSsn(tPersonDto.getSsn());

        if (tPersonDto.getPassportNumber() != null)
            tPerson.setPassportNumber(tPersonDto.getPassportNumber());

        if (tPersonDto.getPassportCountry() != null)
            tPerson.setPassportCountry(tPersonDto.getPassportCountry());

        if (tPersonDto.getIdNumber() != null)
            tPerson.setIdNumber(tPersonDto.getIdNumber());

        if (tPersonDto.getNationality1() != null)
            tPerson.setNationality1(tPersonDto.getNationality1());

        if (tPersonDto.getNationality2() != null)
            tPerson.setNationality2(tPersonDto.getNationality2());

        if (tPersonDto.getNationality3() != null)
            tPerson.setNationality3(tPersonDto.getNationality3());

        if (tPersonDto.getResidence() != null)
            tPerson.setResidence(tPersonDto.getResidence());


        if (tPersonDto.getPhones() != null) {
            if (tPersonDto.getPhones().getPhone() != null && !tPersonDto.getPhones().getPhone().isEmpty()){
                TPerson.Phones phones = new TPerson.Phones();
                List<TPhone> phoneList = new ArrayList<>();
                for (xmlTPhoneDto phone : tPersonDto.getPhones().getPhone()){
                    TPhoneService phoneService = new TPhoneService();
                    phoneList.add(phoneService.generateTPhoneTag(phone));
                }
                phones.setPhone(phoneList);
                tPerson.setPhones(phones);
            }
        }


        if (tPersonDto.getAddresses() != null) {
            if (tPersonDto.getAddresses().getAddress() != null && !tPersonDto.getAddresses().getAddress().isEmpty()) {
                TPerson.Addresses addresses = new TPerson.Addresses();
                List<TAddress> addressList = new ArrayList<>();
                for (xmlTAddressDto address : tPersonDto.getAddresses().getAddress()) {
                    TAddressService addressService = new TAddressService();
                    addressList.add(addressService.generateTAddressTag(address));
                }
                addresses.setAddress(addressList);
                tPerson.setAddresses(addresses);
            }
        }


        if (tPersonDto.getEmail() != null && !tPersonDto.getEmail().isEmpty()) {
            List<String> emailList = new ArrayList<>();
            for (String email : tPersonDto.getEmail())
                emailList.add(email);

            tPerson.setEmail(emailList);
        }

        if (tPersonDto.getOccupation() != null)
            tPerson.setOccupation(tPersonDto.getOccupation());

//        if (tPersonDto.getEmployerName() != null)
//            tPerson.setEmployerName(tPersonDto.getEmployerName());
//
//        if (tPersonDto.getEmployerAddressId() != null) {
//            TAddressService employerAddressService = new TAddressService();
//            tPerson.setEmployerAddressId(employerAddressService.generateTAddressTag(tPersonDto.getEmployerAddressId()));
//        }
//
//        if (tPersonDto.getEmployerPhoneId() != null) {
//            TPhoneService employerPhoneService = new TPhoneService();
//            tPerson.setEmployerPhoneId(employerPhoneService.generateTPhoneTag(tPersonDto.getEmployerPhoneId()));
//        }
//
//        if (tPersonDto.getIdentification() != null && !tPersonDto.getIdentification().isEmpty()) {
//            List<TPersonIdentification> identificationList = new ArrayList<>();
//            for (xmlTPersonIdentificationDto identification : tPersonDto.getIdentification()) {
//                TPersonIdentificationService personIdentificationService = new TPersonIdentificationService();
//                identificationList.add(personIdentificationService.generateTPersonIdentificationTag(identification));
//            }
//            tPerson.setIdentification(identificationList);
//        }
//
//
//        if (tPersonDto.getDeceased() != null)
//            tPerson.setDeceased(tPersonDto.getDeceased());
//
//        if (tPersonDto.getDateDeceased() != null)
//            tPerson.setDateDeceased(XmlUtils.createXMLGregorianCalendar(tPersonDto.getDateDeceased().toString()));
//
//        if (tPersonDto.getTaxNumber() != null)
//            tPerson.setTaxNumber(tPersonDto.getTaxNumber());
//
//        if (tPersonDto.getTaxRegNumber() != null)
//            tPerson.setTaxRegNumber(tPersonDto.getTaxRegNumber());
//
//        if (tPersonDto.getSourceOfWealth() != null)
//            tPerson.setSourceOfWealth(tPersonDto.getSourceOfWealth());

        if (tPersonDto.getComments() != null)
            tPerson.setComments(tPersonDto.getComments());

        return tPerson;
    }
}
