package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.*;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTFromMyClientDto;

public class TFromMyClientService {

    public Report.Transaction.TFromMyClient generateTFromMyClientTag(xmlTFromMyClientDto tFromMyClientDto) {
        Report.Transaction.TFromMyClient tFromMyClient = new Report.Transaction.TFromMyClient();

        if (tFromMyClientDto.getFromFundsCode() != null)
            tFromMyClient.setFromFundsCode(tFromMyClientDto.getFromFundsCode());

        if (tFromMyClientDto.getFromFundsComment() != null)
            tFromMyClient.setFromFundsComment(tFromMyClientDto.getFromFundsComment());

        if (tFromMyClientDto.getFromForeignCurrency() != null) {
            TForeignCurrencyService tForeignCurrencyService = new TForeignCurrencyService();
            tFromMyClient.setFromForeignCurrency(tForeignCurrencyService.generateTForeignCurrencyTag(tFromMyClientDto.getFromForeignCurrency()));
        }

        if (tFromMyClientDto.getTConductor() != null) {
            TPersonMyClientService tConductorService = new TPersonMyClientService();
            tFromMyClient.setTConductor(tConductorService.generateTPersonMyClientTag(tFromMyClientDto.getTConductor()));
        }

        if (tFromMyClientDto.getFromAccount() != null) {
            TAccountMyClientService tAccountMyClientService = new TAccountMyClientService();
            tFromMyClient.setFromAccount(tAccountMyClientService.generateTAccountMyClientTag(tFromMyClientDto.getFromAccount()));
        }

        if (tFromMyClientDto.getFromPerson() != null) {
            TPersonMyClientService tPersonMyClientService = new TPersonMyClientService();
            tFromMyClient.setFromPerson(tPersonMyClientService.generateTPersonMyClientTag(tFromMyClientDto.getFromPerson()));
        }

        if (tFromMyClientDto.getFromEntity() != null) {
            TEntityMyClientService tEntityMyClientService = new TEntityMyClientService();
            tFromMyClient.setFromEntity(tEntityMyClientService.generateTEntityMyClientTag(tFromMyClientDto.getFromEntity()));
        }

        if (tFromMyClientDto.getFromCountry() != null)
            tFromMyClient.setFromCountry(tFromMyClientDto.getFromCountry());

        return tFromMyClient;
    }
}
