package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TPersonIdentification;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonIdentificationDto;


public class TPersonIdentificationService {

    public TPersonIdentification generateTPersonIdentificationTag(xmlTPersonIdentificationDto tPersonIdentificationDto) {
        TPersonIdentification tPersonIdentification = new TPersonIdentification();

        if (tPersonIdentificationDto.getType() != null)
            tPersonIdentification.setType(tPersonIdentificationDto.getType());

        if (tPersonIdentificationDto.getNumber() != null)
            tPersonIdentification.setNumber(tPersonIdentificationDto.getNumber());

        if (tPersonIdentificationDto.getIssueDate() != null)
            tPersonIdentification.setIssueDate(XmlUtils.createXMLGregorianCalendar(tPersonIdentificationDto.getIssueDate().toString()));

        if (tPersonIdentificationDto.getExpiryDate() != null)
            tPersonIdentification.setExpiryDate(XmlUtils.createXMLGregorianCalendar(tPersonIdentificationDto.getExpiryDate().toString()));

        if (tPersonIdentificationDto.getIssuedBy() != null)
            tPersonIdentification.setIssuedBy(tPersonIdentificationDto.getIssuedBy());

        if (tPersonIdentificationDto.getIssueCountry() != null)
            tPersonIdentification.setIssueCountry(tPersonIdentificationDto.getIssueCountry());

        if (tPersonIdentificationDto.getComments() != null)
            tPersonIdentification.setComments(tPersonIdentificationDto.getComments());

        return tPersonIdentification;
    }
}
