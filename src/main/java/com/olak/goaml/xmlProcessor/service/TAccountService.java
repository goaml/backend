package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.CurrencyType;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TAccount;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlAccountSignatoryDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAccountDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TAccountService {

    public TAccount generateTAccountTag(xmlTAccountDto tAccountDto) {
        TAccount tAccount = new TAccount();

        if (tAccountDto.getInstitutionName() != null)
            tAccount.setInstitutionName(tAccountDto.getInstitutionName());

        if (tAccountDto.getSwift() != null)
            tAccount.setSwift(tAccountDto.getSwift());

        if (tAccountDto.getNonBankInstitution() != null)
            tAccount.setNonBankInstitution(tAccountDto.getNonBankInstitution());

        if (tAccountDto.getBranch() != null)
            tAccount.setBranch(tAccountDto.getBranch());

        if (tAccountDto.getAccount() != null)
            tAccount.setAccount(tAccountDto.getAccount());

        if (tAccountDto.getCurrencyCode() != null)
            tAccount.setCurrencyCode(CurrencyType.fromValue(tAccountDto.getCurrencyCode().getCurrencyCode()));

        if (tAccountDto.getAccountName() != null)
            tAccount.setAccountName(tAccountDto.getAccountName());

        if (tAccountDto.getIban() != null)
            tAccount.setIban(tAccountDto.getIban());

        if (tAccountDto.getClientNumber() != null)
            tAccount.setClientNumber(tAccountDto.getClientNumber());

        if (tAccountDto.getPersonalAccountType() != null)
            tAccount.setPersonalAccountType(tAccountDto.getPersonalAccountType());

        if (tAccountDto.getTEntity() != null) {
            TEntityService tEntityService = new TEntityService();
            tAccount.setTEntity(tEntityService.generateTEntityTag(tAccountDto.getTEntity()));
        }

        if (tAccountDto.getSignatory() != null) {
            List<TAccount.Signatory> signatoryList = new ArrayList<>();
            for (xmlAccountSignatoryDto signatoryDto : tAccountDto.getSignatory()) {
                TAccountSignatoryService tAccountSignatoryService = new TAccountSignatoryService();
                signatoryList.add(tAccountSignatoryService.generateSignatoryTag(signatoryDto));
            }
            tAccount.setSignatory(signatoryList);
        }

        if (tAccountDto.getOpened() != null)
            tAccount.setOpened(XmlUtils.createXMLGregorianCalendar(tAccountDto.getOpened().toString()));

        if (tAccountDto.getClosed() != null)
            tAccount.setClosed(XmlUtils.createXMLGregorianCalendar(tAccountDto.getClosed().toString()));

        if (tAccountDto.getBalance() != null)
            tAccount.setBalance(tAccountDto.getBalance());

        if (tAccountDto.getDateBalance() != null)
            tAccount.setDateBalance(XmlUtils.createXMLGregorianCalendar(tAccountDto.getDateBalance().toString()));

        if (tAccountDto.getStatusCode() != null)
            tAccount.setStatusCode(tAccountDto.getStatusCode());

        if (tAccountDto.getBeneficiary() != null)
            tAccount.setBeneficiary(XmlUtils.createJAXBElement("beneficiary", tAccountDto.getBeneficiary()));

        if (tAccountDto.getBeneficiaryComment() != null)
            tAccount.setBeneficiaryComment(XmlUtils.createJAXBElement("beneficiary_comment", tAccountDto.getBeneficiaryComment()));

        if (tAccountDto.getComments() != null)
            tAccount.setComments(tAccountDto.getComments());


        return tAccount;
    }
}
