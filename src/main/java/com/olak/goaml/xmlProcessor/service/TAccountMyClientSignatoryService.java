package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TAccountMyClient;
import com.olak.goaml.xmlProcessor.xmlDto.xmlAccountMyClientSignatoryDto;

public class TAccountMyClientSignatoryService {

    public TAccountMyClient.Signatory generateSignatoryTag(xmlAccountMyClientSignatoryDto signatoryDto) {
        TAccountMyClient.Signatory signatory = new TAccountMyClient.Signatory();

        if (signatoryDto.getIsPrimary() != null)
            signatory.setIsPrimary(signatoryDto.getIsPrimary());

        if (signatoryDto.getTPerson() != null) {
            TPersonMyClientService tPersonMyClientService = new TPersonMyClientService();
            signatory.setTPerson(tPersonMyClientService.generateTPersonMyClientTag(signatoryDto.getTPerson()));
        }

        if (signatoryDto.getRole() != null)
            signatory.setRole(signatoryDto.getRole());

        return signatory;
    }
}
