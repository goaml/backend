package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.Report;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TParty;
import com.olak.goaml.xmlProcessor.xmlDto.xmlInvolvedPartiesDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPartyDto;

import java.util.ArrayList;
import java.util.List;

public class InvolvedPartiesService {

    public Report.Transaction.InvolvedParties generateInvolvedPartiesTag(xmlInvolvedPartiesDto involvedPartiesDto) {
        Report.Transaction.InvolvedParties involvedParties = new Report.Transaction.InvolvedParties();

        if (involvedPartiesDto.getParty() != null && !involvedPartiesDto.getParty().isEmpty()) {
            List<TParty> partyList = new ArrayList<>();
            for (xmlTPartyDto tPartyDto : involvedPartiesDto.getParty()) {
                TPartyService tPartyService = new TPartyService();
                partyList.add(tPartyService.generateTPartyTag(tPartyDto));
            }
            involvedParties.setParty(partyList);
        }

        return involvedParties;
    }
}
