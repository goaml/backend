package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.CurrencyType;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TForeignCurrency;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTForeignCurrencyDto;

import java.math.BigDecimal;

public class TForeignCurrencyService {

    public TForeignCurrency generateTForeignCurrencyTag(xmlTForeignCurrencyDto foreignCurrencyDto) {
        TForeignCurrency tForeignCurrency = new TForeignCurrency();

        if (foreignCurrencyDto.getForeignCurrencyCode() != null)
            tForeignCurrency.setForeignCurrencyCode(CurrencyType.fromValue(foreignCurrencyDto.getForeignCurrencyCode().getCurrencyCode()));

        if (foreignCurrencyDto.getForeignAmount() != null)
            tForeignCurrency.setForeignAmount(foreignCurrencyDto.getForeignAmount());

        if (foreignCurrencyDto.getForeignExchangeRate() != null)
            tForeignCurrency.setForeignExchangeRate(foreignCurrencyDto.getForeignExchangeRate());

        return tForeignCurrency;
    }
}
