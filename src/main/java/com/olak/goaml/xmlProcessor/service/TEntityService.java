package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TAddress;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TEntity;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TPersonMyClient;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TPhone;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlEntityDirectorIdDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAddressDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTEntityDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPhoneDto;

import java.util.ArrayList;
import java.util.List;

public class TEntityService {

    public TEntity generateTEntityTag(xmlTEntityDto tEntityDto) {
        TEntity tEntity = new TEntity();

        if (tEntityDto.getName() != null)
            tEntity.setName(tEntityDto.getName());

        if (tEntityDto.getCommercialName() != null)
            tEntity.setCommercialName(tEntityDto.getCommercialName());

        if (tEntityDto.getIncorporationLegalForm() != null)
            tEntity.setIncorporationLegalForm(tEntityDto.getIncorporationLegalForm());

        if (tEntityDto.getIncorporationNumber() != null)
            tEntity.setIncorporationNumber(tEntityDto.getIncorporationNumber());

        if (tEntityDto.getBusiness() != null)
            tEntity.setBusiness(tEntityDto.getBusiness());


        if (tEntityDto.getPhones() != null) {
            if (tEntityDto.getPhones().getPhone() != null && !tEntityDto.getPhones().getPhone().isEmpty()){
                TEntity.Phones phones = new TEntity.Phones();
                List<TPhone> phoneList = new ArrayList<>();
                for (xmlTPhoneDto phone : tEntityDto.getPhones().getPhone()){
                    TPhoneService phoneService = new TPhoneService();
                    phoneList.add(phoneService.generateTPhoneTag(phone));
                }
                phones.setPhone(phoneList);
                tEntity.setPhones(phones);
            }
        }


        if (tEntityDto.getAddresses() != null) {
            if (tEntityDto.getAddresses().getAddress() != null && !tEntityDto.getAddresses().getAddress().isEmpty()) {
                TEntity.Addresses addresses = new TEntity.Addresses();
                List<TAddress> addressList = new ArrayList<>();
                for (xmlTAddressDto address : tEntityDto.getAddresses().getAddress()) {
                    TAddressService addressService = new TAddressService();
                    addressList.add(addressService.generateTAddressTag(address));
                }
                addresses.setAddress(addressList);
                tEntity.setAddresses(addresses);
            }
        }

        if (tEntityDto.getEmail() != null)
            tEntity.setEmail(tEntityDto.getEmail());

        if (tEntityDto.getUrl() != null)
            tEntity.setUrl(XmlUtils.createJAXBElement("url",tEntityDto.getUrl()));

        if (tEntityDto.getIncorporationState() != null)
            tEntity.setIncorporationState(tEntityDto.getIncorporationState());

        if (tEntityDto.getIncorporationCountryCode() != null)
            tEntity.setIncorporationCountryCode(tEntityDto.getIncorporationCountryCode());

        if (tEntityDto.getDirectorId() != null) {
            List<TEntity.DirectorId> directorIdList = new ArrayList<>();
            for (xmlEntityDirectorIdDto directorId : tEntityDto.getDirectorId()) {
                EntityDirectorIdService entityDirectorIdService = new EntityDirectorIdService();
                directorIdList.add(entityDirectorIdService.generateEntityDirectorIdTag(directorId));
            }
            tEntity.setDirectorId(directorIdList);
        }

        if (tEntityDto.getIncorporationDate() != null)
            tEntity.setIncorporationDate(XmlUtils.createXMLGregorianCalendar(tEntityDto.getIncorporationDate().toString()));

        if (tEntityDto.getBusinessClosed() != null)
            tEntity.setBusinessClosed(tEntityDto.getBusinessClosed());

        if (tEntityDto.getDateBusinessClosed() != null)
            tEntity.setDateBusinessClosed(XmlUtils.createXMLGregorianCalendar(tEntityDto.getDateBusinessClosed().toString()));

        if (tEntityDto.getTaxNumber() != null)
            tEntity.setTaxNumber(tEntityDto.getTaxNumber());

        if (tEntityDto.getTaxRegNumber() != null)
            tEntity.setTaxRegNumber(tEntityDto.getTaxRegNumber());

        if (tEntityDto.getComments() != null)
            tEntity.setComments(tEntityDto.getComments());

        return tEntity;
    }
}
