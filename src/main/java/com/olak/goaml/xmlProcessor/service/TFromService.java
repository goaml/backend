package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.*;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTFromDto;

public class TFromService {

    public Report.Transaction.TFrom generateTFromTag(xmlTFromDto tFromDto) {
        Report.Transaction.TFrom tFrom = new Report.Transaction.TFrom();

        if (tFromDto.getFromFundsCode() != null)
            tFrom.setFromFundsCode(tFromDto.getFromFundsCode());

        if (tFromDto.getFromFundsComment() != null)
            tFrom.setFromFundsComment(tFromDto.getFromFundsComment());

        if (tFromDto.getFromForeignCurrency() != null) {
            TForeignCurrencyService tForeignCurrencyService = new TForeignCurrencyService();
            tFrom.setFromForeignCurrency(tForeignCurrencyService.generateTForeignCurrencyTag(tFromDto.getFromForeignCurrency()));
        }

        if (tFromDto.getTConductor() != null) {
            TPersonService tConductorService = new TPersonService();
            tFrom.setTConductor(tConductorService.generateTPersonTag(tFromDto.getTConductor()));
        }

        if (tFromDto.getFromAccount() != null) {
            TAccountService tAccountService = new TAccountService();
            tFrom.setFromAccount(tAccountService.generateTAccountTag(tFromDto.getFromAccount()));
        }

        if (tFromDto.getFromPerson() != null) {
            TPersonService tPersonService = new TPersonService();
            tFrom.setFromPerson(tPersonService.generateTPersonTag(tFromDto.getFromPerson()));
        }

        if (tFromDto.getFromEntity() != null) {
            TEntityService tEntityService = new TEntityService();
            tFrom.setFromEntity(tEntityService.generateTEntityTag(tFromDto.getFromEntity()));
        }

        if (tFromDto.getFromCountry() != null)
            tFrom.setFromCountry(tFromDto.getFromCountry());


        return tFrom;
    }
}
