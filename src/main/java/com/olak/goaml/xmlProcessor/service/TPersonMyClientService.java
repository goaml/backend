package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.*;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAddressDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonIdentificationDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonMyClientDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPhoneDto;

import java.util.ArrayList;
import java.util.List;

public class TPersonMyClientService {

    public TPersonMyClient generateTPersonMyClientTag(xmlTPersonMyClientDto tPersonMyClientDto) {
        TPersonMyClient tPersonMyClient = new TPersonMyClient();

        if (tPersonMyClientDto.getGender() != null)
            tPersonMyClient.setGender(tPersonMyClientDto.getGender());

        if (tPersonMyClientDto.getTitle() != null)
            tPersonMyClient.setTitle(XmlUtils.createJAXBElement("title", tPersonMyClientDto.getTitle()));

        if (tPersonMyClientDto.getFirstName() != null)
            tPersonMyClient.setFirstName(tPersonMyClientDto.getFirstName());

//        if (tPersonMyClientDto.getMiddleName() != null)
//            tPersonMyClient.setMiddleName(XmlUtils.createJAXBElement("middle_name", tPersonMyClientDto.getMiddleName()));
//
//        if (tPersonMyClientDto.getPrefix() != null)
//            tPersonMyClient.setPrefix(tPersonMyClientDto.getPrefix());

        if (tPersonMyClientDto.getLastName() != null)
            tPersonMyClient.setLastName(tPersonMyClientDto.getLastName());

        if (tPersonMyClientDto.getBirthdate() != null)
            tPersonMyClient.setBirthdate(XmlUtils.createXMLGregorianCalendar(tPersonMyClientDto.getBirthdate().toString()));

//        if (tPersonMyClientDto.getBirthPlace() != null)
//            tPersonMyClient.setBirthPlace(tPersonMyClientDto.getBirthPlace());
//
//        if (tPersonMyClientDto.getMothersName() != null)
//            tPersonMyClient.setMothersName(tPersonMyClientDto.getMothersName());
//
//        if (tPersonMyClientDto.getAlias() != null)
//            tPersonMyClient.setAlias(tPersonMyClientDto.getAlias());

        if (tPersonMyClientDto.getSsn() != null)
            tPersonMyClient.setSsn(tPersonMyClientDto.getSsn());

        if (tPersonMyClientDto.getPassportNumber() != null)
            tPersonMyClient.setPassportNumber(tPersonMyClientDto.getPassportNumber());

        if (tPersonMyClientDto.getPassportCountry() != null)
            tPersonMyClient.setPassportCountry(tPersonMyClientDto.getPassportCountry());

        if (tPersonMyClientDto.getIdNumber() != null)
            tPersonMyClient.setIdNumber(tPersonMyClientDto.getIdNumber());

        if (tPersonMyClientDto.getNationality1() != null)
            tPersonMyClient.setNationality1(tPersonMyClientDto.getNationality1());

        if (tPersonMyClientDto.getNationality2() != null)
            tPersonMyClient.setNationality2(tPersonMyClientDto.getNationality2());

        if (tPersonMyClientDto.getNationality3() != null)
            tPersonMyClient.setNationality3(tPersonMyClientDto.getNationality3());

        if (tPersonMyClientDto.getResidence() != null)
            tPersonMyClient.setResidence(tPersonMyClientDto.getResidence());

        if (tPersonMyClientDto.getPhones() != null) {
            if (tPersonMyClientDto.getPhones().getPhone() != null && !tPersonMyClientDto.getPhones().getPhone().isEmpty()){
                TPersonMyClient.Phones phones = new TPersonMyClient.Phones();
                List<TPhone> phoneList = new ArrayList<>();
                for (xmlTPhoneDto phone : tPersonMyClientDto.getPhones().getPhone()){
                    TPhoneService phoneService = new TPhoneService();
                    phoneList.add(phoneService.generateTPhoneTag(phone));
                }
                phones.setPhone(phoneList);
                tPersonMyClient.setPhones(phones);
            }
        }

        if (tPersonMyClientDto.getAddresses() != null) {
            if (tPersonMyClientDto.getAddresses().getAddress() != null && !tPersonMyClientDto.getAddresses().getAddress().isEmpty()) {
                TPersonMyClient.Addresses addresses = new TPersonMyClient.Addresses();
                List<TAddress> addressList = new ArrayList<>();
                for (xmlTAddressDto address : tPersonMyClientDto.getAddresses().getAddress()) {
                    TAddressService addressService = new TAddressService();
                    addressList.add(addressService.generateTAddressTag(address));
                }
                addresses.setAddress(addressList);
                tPersonMyClient.setAddresses(addresses);
            }
        }


        if (tPersonMyClientDto.getEmail() != null && !tPersonMyClientDto.getEmail().isEmpty()) {
            List<String> emailList = new ArrayList<>();
            for (String email : tPersonMyClientDto.getEmail())
                emailList.add(email);

            tPersonMyClient.setEmail(emailList);
        }

        if (tPersonMyClientDto.getOccupation() != null)
            tPersonMyClient.setOccupation(tPersonMyClientDto.getOccupation());
//
//        if (tPersonMyClientDto.getEmployerName() != null)
//            tPersonMyClient.setEmployerName(tPersonMyClientDto.getEmployerName());
//
//
//        if (tPersonMyClientDto.getEmployerAddressId() != null) {
//            TAddressService employerAddressService = new TAddressService();
//            tPersonMyClient.setEmployerAddressId(employerAddressService.generateTAddressTag(tPersonMyClientDto.getEmployerAddressId()));
//        }
//
//
//        if (tPersonMyClientDto.getEmployerPhoneId() != null) {
//            TPhoneService employerPhoneService = new TPhoneService();
//            tPersonMyClient.setEmployerPhoneId(employerPhoneService.generateTPhoneTag(tPersonMyClientDto.getEmployerPhoneId()));
//        }
//
//
//        if (tPersonMyClientDto.getIdentification() != null && !tPersonMyClientDto.getIdentification().isEmpty()) {
//            List<TPersonIdentification> identificationList = new ArrayList<>();
//            for (xmlTPersonIdentificationDto identification : tPersonMyClientDto.getIdentification()) {
//                TPersonIdentificationService personIdentificationService = new TPersonIdentificationService();
//                identificationList.add(personIdentificationService.generateTPersonIdentificationTag(identification));
//            }
//            tPersonMyClient.setIdentification(identificationList);
//        }
//
//
//        if (tPersonMyClientDto.getDeceased() != null)
//            tPersonMyClient.setDeceased(tPersonMyClientDto.getDeceased());
//
//        if (tPersonMyClientDto.getDateDeceased() != null)
//            tPersonMyClient.setDateDeceased(XmlUtils.createXMLGregorianCalendar(tPersonMyClientDto.getDateDeceased().toString()));
//
//        if (tPersonMyClientDto.getTaxNumber() != null)
//            tPersonMyClient.setTaxNumber(tPersonMyClientDto.getTaxNumber());
//
//        if (tPersonMyClientDto.getTaxRegNumber() != null)
//            tPersonMyClient.setTaxRegNumber(tPersonMyClientDto.getTaxRegNumber());
//
//        if (tPersonMyClientDto.getSourceOfWealth() != null)
//            tPersonMyClient.setSourceOfWealth(tPersonMyClientDto.getSourceOfWealth());

        if (tPersonMyClientDto.getComments() != null)
            tPersonMyClient.setComments(tPersonMyClientDto.getComments());

        return tPersonMyClient;

    }
}
