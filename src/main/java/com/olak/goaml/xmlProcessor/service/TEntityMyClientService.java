package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TAddress;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TEntity;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TEntityMyClient;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TPhone;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlEntityMyClientDirectorIdDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAddressDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTEntityMyClientDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPhoneDto;

import java.util.ArrayList;
import java.util.List;

public class TEntityMyClientService {

    public TEntityMyClient generateTEntityMyClientTag(xmlTEntityMyClientDto xmlTEntityMyClientDto) {
        TEntityMyClient tEntityMyClient = new TEntityMyClient();

        if (xmlTEntityMyClientDto.getName() != null)
            tEntityMyClient.setName(xmlTEntityMyClientDto.getName());

        if (xmlTEntityMyClientDto.getCommercialName() != null)
            tEntityMyClient.setCommercialName(xmlTEntityMyClientDto.getCommercialName());

        if (xmlTEntityMyClientDto.getIncorporationLegalForm() != null)
            tEntityMyClient.setIncorporationLegalForm(xmlTEntityMyClientDto.getIncorporationLegalForm());

        if (xmlTEntityMyClientDto.getIncorporationNumber() != null)
            tEntityMyClient.setIncorporationNumber(xmlTEntityMyClientDto.getIncorporationNumber());

        if (xmlTEntityMyClientDto.getBusiness() != null)
            tEntityMyClient.setBusiness(xmlTEntityMyClientDto.getBusiness());


        if (xmlTEntityMyClientDto.getPhones() != null) {
            if (xmlTEntityMyClientDto.getPhones().getPhone() != null && !xmlTEntityMyClientDto.getPhones().getPhone().isEmpty()){
                TEntityMyClient.Phones phones = new TEntityMyClient.Phones();
                List<TPhone> phoneList = new ArrayList<>();
                for (xmlTPhoneDto phone : xmlTEntityMyClientDto.getPhones().getPhone()){
                    TPhoneService phoneService = new TPhoneService();
                    phoneList.add(phoneService.generateTPhoneTag(phone));
                }
                phones.setPhone(phoneList);
                tEntityMyClient.setPhones(phones);
            }
        }


        if (xmlTEntityMyClientDto.getAddresses() != null) {
            if (xmlTEntityMyClientDto.getAddresses().getAddress() != null && !xmlTEntityMyClientDto.getAddresses().getAddress().isEmpty()) {
                TEntityMyClient.Addresses addresses = new TEntityMyClient.Addresses();
                List<TAddress> addressList = new ArrayList<>();
                for (xmlTAddressDto address : xmlTEntityMyClientDto.getAddresses().getAddress()) {
                    TAddressService addressService = new TAddressService();
                    addressList.add(addressService.generateTAddressTag(address));
                }
                addresses.setAddress(addressList);
                tEntityMyClient.setAddresses(addresses);
            }
        }


        if (xmlTEntityMyClientDto.getEmail() != null)
            tEntityMyClient.setEmail(xmlTEntityMyClientDto.getEmail());

//        if (xmlTEntityMyClientDto.getUrl() != null)
//            tEntityMyClient.setUrl(XmlUtils.createJAXBElement("url",xmlTEntityMyClientDto.getUrl()));
//
//        if (xmlTEntityMyClientDto.getIncorporationState() != null)
//            tEntityMyClient.setIncorporationState(xmlTEntityMyClientDto.getIncorporationState());

        if (xmlTEntityMyClientDto.getIncorporationCountryCode() != null)
            tEntityMyClient.setIncorporationCountryCode(xmlTEntityMyClientDto.getIncorporationCountryCode());


        if (xmlTEntityMyClientDto.getDirectorId() != null) {
            List<TEntityMyClient.DirectorId> directorIdList = new ArrayList<>();
            for (xmlEntityMyClientDirectorIdDto directorId : xmlTEntityMyClientDto.getDirectorId()) {
                EntityMyClientDirectorIdService directorIdService = new EntityMyClientDirectorIdService();
                directorIdList.add(directorIdService.generateEntityMyClientDirectorIdTag(directorId));
            }
            tEntityMyClient.setDirectorId(directorIdList);
        }


        if (xmlTEntityMyClientDto.getIncorporationDate() != null)
            tEntityMyClient.setIncorporationDate(XmlUtils.createXMLGregorianCalendar(xmlTEntityMyClientDto.getIncorporationDate().toString()));

        if (xmlTEntityMyClientDto.getBusinessClosed() != null)
            tEntityMyClient.setBusinessClosed(xmlTEntityMyClientDto.getBusinessClosed());

        if (xmlTEntityMyClientDto.getDateBusinessClosed() != null)
            tEntityMyClient.setDateBusinessClosed(XmlUtils.createXMLGregorianCalendar(xmlTEntityMyClientDto.getDateBusinessClosed().toString()));

        if (xmlTEntityMyClientDto.getTaxNumber() != null)
            tEntityMyClient.setTaxNumber(xmlTEntityMyClientDto.getTaxNumber());

        if (xmlTEntityMyClientDto.getTaxRegNumber() != null)
            tEntityMyClient.setTaxRegNumber(xmlTEntityMyClientDto.getTaxRegNumber());

        if (xmlTEntityMyClientDto.getComments() != null)
            tEntityMyClient.setComments(xmlTEntityMyClientDto.getComments());

        return tEntityMyClient;
    }
}
