package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.*;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlEntityDirectorIdDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAddressDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonIdentificationDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPhoneDto;

import java.util.ArrayList;
import java.util.List;

public class EntityDirectorIdService {

    public TEntity.DirectorId generateEntityDirectorIdTag(xmlEntityDirectorIdDto entityDirectorIdDto) {

        TEntity.DirectorId directorId = new TEntity.DirectorId();

        if (entityDirectorIdDto.getGender() != null)
            directorId.setGender(entityDirectorIdDto.getGender());

        if (entityDirectorIdDto.getTitle() != null)
            directorId.setTitle(XmlUtils.createJAXBElement("title", entityDirectorIdDto.getTitle()));

        if (entityDirectorIdDto.getFirstName() != null)
            directorId.setFirstName(entityDirectorIdDto.getFirstName());

//        if (entityDirectorIdDto.getMiddleName() != null)
//            directorId.setMiddleName(XmlUtils.createJAXBElement("middle_name", entityDirectorIdDto.getMiddleName()));
//
//        if (entityDirectorIdDto.getPrefix() != null)
//            directorId.setPrefix(entityDirectorIdDto.getPrefix());

        if (entityDirectorIdDto.getLastName() != null)
            directorId.setLastName(entityDirectorIdDto.getLastName());

        if (entityDirectorIdDto.getBirthdate() != null)
            directorId.setBirthdate(XmlUtils.createXMLGregorianCalendar(entityDirectorIdDto.getBirthdate().toString()));

//        if (entityDirectorIdDto.getBirthPlace() != null)
//            directorId.setBirthPlace(entityDirectorIdDto.getBirthPlace());
//
//        if (entityDirectorIdDto.getMothersName() != null)
//            directorId.setMothersName(entityDirectorIdDto.getMothersName());
//
//        if (entityDirectorIdDto.getAlias() != null)
//            directorId.setAlias(entityDirectorIdDto.getAlias());

        if (entityDirectorIdDto.getSsn() != null)
            directorId.setSsn(entityDirectorIdDto.getSsn());

        if (entityDirectorIdDto.getPassportNumber() != null)
            directorId.setPassportNumber(entityDirectorIdDto.getPassportNumber());

        if (entityDirectorIdDto.getPassportCountry() != null)
            directorId.setPassportCountry(entityDirectorIdDto.getPassportCountry());

        if (entityDirectorIdDto.getIdNumber() != null)
            directorId.setIdNumber(entityDirectorIdDto.getIdNumber());

        if (entityDirectorIdDto.getNationality1() != null)
            directorId.setNationality1(entityDirectorIdDto.getNationality1());

        if (entityDirectorIdDto.getNationality2() != null)
            directorId.setNationality2(entityDirectorIdDto.getNationality2());

        if (entityDirectorIdDto.getNationality3() != null)
            directorId.setNationality3(entityDirectorIdDto.getNationality3());

        if (entityDirectorIdDto.getResidence() != null)
            directorId.setResidence(entityDirectorIdDto.getResidence());


        if (entityDirectorIdDto.getPhones() != null) {
            if (entityDirectorIdDto.getPhones().getPhone() != null && !entityDirectorIdDto.getPhones().getPhone().isEmpty()){
                TPerson.Phones phones = new TPerson.Phones();
                List<TPhone> phoneList = new ArrayList<>();
                for (xmlTPhoneDto phone : entityDirectorIdDto.getPhones().getPhone()){
                    TPhoneService phoneService = new TPhoneService();
                    phoneList.add(phoneService.generateTPhoneTag(phone));
                }
                phones.setPhone(phoneList);
                directorId.setPhones(phones);
            }
        }


        if (entityDirectorIdDto.getAddresses() != null) {
            if (entityDirectorIdDto.getAddresses().getAddress() != null && !entityDirectorIdDto.getAddresses().getAddress().isEmpty()) {
                TPerson.Addresses addresses = new TPerson.Addresses();
                List<TAddress> addressList = new ArrayList<>();
                for (xmlTAddressDto address : entityDirectorIdDto.getAddresses().getAddress()) {
                    TAddressService addressService = new TAddressService();
                    addressList.add(addressService.generateTAddressTag(address));
                }
                addresses.setAddress(addressList);
                directorId.setAddresses(addresses);
            }
        }


        if (entityDirectorIdDto.getEmail() != null && !entityDirectorIdDto.getEmail().isEmpty()) {
            List<String> emailList = new ArrayList<>();
            for (String email : entityDirectorIdDto.getEmail())
                emailList.add(email);

            directorId.setEmail(emailList);
        }

        if (entityDirectorIdDto.getOccupation() != null)
            directorId.setOccupation(entityDirectorIdDto.getOccupation());

//        if (entityDirectorIdDto.getEmployerName() != null)
//            directorId.setEmployerName(entityDirectorIdDto.getEmployerName());
//
//        if (entityDirectorIdDto.getEmployerAddressId() != null) {
//            TAddressService employerAddressService = new TAddressService();
//            directorId.setEmployerAddressId(employerAddressService.generateTAddressTag(entityDirectorIdDto.getEmployerAddressId()));
//        }

//        if (entityDirectorIdDto.getEmployerPhoneId() != null) {
//            TPhoneService employerPhoneService = new TPhoneService();
//            directorId.setEmployerPhoneId(employerPhoneService.generateTPhoneTag(entityDirectorIdDto.getEmployerPhoneId()));
//        }

//        if (entityDirectorIdDto.getIdentification() != null && !entityDirectorIdDto.getIdentification().isEmpty()) {
//            List<TPersonIdentification> identificationList = new ArrayList<>();
//            for (xmlTPersonIdentificationDto identification : entityDirectorIdDto.getIdentification()) {
//                TPersonIdentificationService personIdentificationService = new TPersonIdentificationService();
//                identificationList.add(personIdentificationService.generateTPersonIdentificationTag(identification));
//            }
//            directorId.setIdentification(identificationList);
//        }


//        if (entityDirectorIdDto.getDeceased() != null)
//            directorId.setDeceased(entityDirectorIdDto.getDeceased());
//
//        if (entityDirectorIdDto.getDateDeceased() != null)
//            directorId.setDateDeceased(XmlUtils.createXMLGregorianCalendar(entityDirectorIdDto.getDateDeceased().toString()));
//
//        if (entityDirectorIdDto.getTaxNumber() != null)
//            directorId.setTaxNumber(entityDirectorIdDto.getTaxNumber());
//
//        if (entityDirectorIdDto.getTaxRegNumber() != null)
//            directorId.setTaxRegNumber(entityDirectorIdDto.getTaxRegNumber());

//        if (entityDirectorIdDto.getSourceOfWealth() != null)
//            directorId.setSourceOfWealth(entityDirectorIdDto.getSourceOfWealth());

        if (entityDirectorIdDto.getComments() != null)
            directorId.setComments(entityDirectorIdDto.getComments());

        if (entityDirectorIdDto.getRole() != null)
            directorId.setRole(entityDirectorIdDto.getRole());


        return directorId;
    }
}
