package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TAddress;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAddressDto;

public class TAddressService {

    public TAddress generateTAddressTag(xmlTAddressDto tAddressDto) {
        TAddress tAddress = new TAddress();

        if (tAddressDto.getAddressType() != null)
            tAddress.setAddressType(tAddressDto.getAddressType());

        if (tAddressDto.getAddress() != null)
            tAddress.setAddress(tAddressDto.getAddress());

        if (tAddressDto.getTown() != null)
            tAddress.setTown(tAddressDto.getTown());

        if (tAddressDto.getCity() != null)
            tAddress.setCity(tAddressDto.getCity());

        if (tAddressDto.getZip() != null)
            tAddress.setZip(tAddressDto.getZip());

        if (tAddressDto.getCountryCode() != null)
            tAddress.setCountryCode(tAddressDto.getCountryCode());

        if (tAddressDto.getState() != null)
            tAddress.setState(tAddressDto.getState());

        if (tAddressDto.getComments() != null)
            tAddress.setComments(tAddressDto.getComments());

        return tAddress;
    }
}
