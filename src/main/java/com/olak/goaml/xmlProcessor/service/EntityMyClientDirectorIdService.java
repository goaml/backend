package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.*;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlEntityMyClientDirectorIdDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAddressDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPersonIdentificationDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPhoneDto;

import java.util.ArrayList;
import java.util.List;

public class EntityMyClientDirectorIdService {
    public TEntityMyClient.DirectorId generateEntityMyClientDirectorIdTag(xmlEntityMyClientDirectorIdDto xmlEntityMyClientDirectorIdDto) {

        TEntityMyClient.DirectorId directorId = new TEntityMyClient.DirectorId();

        if (xmlEntityMyClientDirectorIdDto.getGender() != null)
            directorId.setGender(xmlEntityMyClientDirectorIdDto.getGender());

        if (xmlEntityMyClientDirectorIdDto.getTitle() != null)
            directorId.setTitle(XmlUtils.createJAXBElement("title", xmlEntityMyClientDirectorIdDto.getTitle()));

        if (xmlEntityMyClientDirectorIdDto.getFirstName() != null)
            directorId.setFirstName(xmlEntityMyClientDirectorIdDto.getFirstName());

//        if (xmlEntityMyClientDirectorIdDto.getMiddleName() != null)
//            directorId.setMiddleName(XmlUtils.createJAXBElement("middle_name", xmlEntityMyClientDirectorIdDto.getMiddleName()));
//
//        if (xmlEntityMyClientDirectorIdDto.getPrefix() != null)
//            directorId.setPrefix(xmlEntityMyClientDirectorIdDto.getPrefix());

        if (xmlEntityMyClientDirectorIdDto.getLastName() != null)
            directorId.setLastName(xmlEntityMyClientDirectorIdDto.getLastName());

        if (xmlEntityMyClientDirectorIdDto.getBirthdate() != null)
            directorId.setBirthdate(XmlUtils.createXMLGregorianCalendar(xmlEntityMyClientDirectorIdDto.getBirthdate().toString()));

//        if (xmlEntityMyClientDirectorIdDto.getBirthPlace() != null)
//            directorId.setBirthPlace(xmlEntityMyClientDirectorIdDto.getBirthPlace());
//
//        if (xmlEntityMyClientDirectorIdDto.getMothersName() != null)
//            directorId.setMothersName(xmlEntityMyClientDirectorIdDto.getMothersName());
//
//        if (xmlEntityMyClientDirectorIdDto.getAlias() != null)
//            directorId.setAlias(xmlEntityMyClientDirectorIdDto.getAlias());

        if (xmlEntityMyClientDirectorIdDto.getSsn() != null)
            directorId.setSsn(xmlEntityMyClientDirectorIdDto.getSsn());

        if (xmlEntityMyClientDirectorIdDto.getPassportNumber() != null)
            directorId.setPassportNumber(xmlEntityMyClientDirectorIdDto.getPassportNumber());

        if (xmlEntityMyClientDirectorIdDto.getPassportCountry() != null)
            directorId.setPassportCountry(xmlEntityMyClientDirectorIdDto.getPassportCountry());

        if (xmlEntityMyClientDirectorIdDto.getIdNumber() != null)
            directorId.setIdNumber(xmlEntityMyClientDirectorIdDto.getIdNumber());

        if (xmlEntityMyClientDirectorIdDto.getNationality1() != null)
            directorId.setNationality1(xmlEntityMyClientDirectorIdDto.getNationality1());

        if (xmlEntityMyClientDirectorIdDto.getNationality2() != null)
            directorId.setNationality2(xmlEntityMyClientDirectorIdDto.getNationality2());

        if (xmlEntityMyClientDirectorIdDto.getNationality3() != null)
            directorId.setNationality3(xmlEntityMyClientDirectorIdDto.getNationality3());

        if (xmlEntityMyClientDirectorIdDto.getResidence() != null)
            directorId.setResidence(xmlEntityMyClientDirectorIdDto.getResidence());

        if (xmlEntityMyClientDirectorIdDto.getPhones() != null) {
            if (xmlEntityMyClientDirectorIdDto.getPhones().getPhone() != null && !xmlEntityMyClientDirectorIdDto.getPhones().getPhone().isEmpty()){
                TPersonMyClient.Phones phones = new TPersonMyClient.Phones();
                List<TPhone> phoneList = new ArrayList<>();
                for (xmlTPhoneDto phone : xmlEntityMyClientDirectorIdDto.getPhones().getPhone()){
                    TPhoneService phoneService = new TPhoneService();
                    phoneList.add(phoneService.generateTPhoneTag(phone));
                }
                phones.setPhone(phoneList);
                directorId.setPhones(phones);
            }
        }

        if (xmlEntityMyClientDirectorIdDto.getAddresses() != null) {
            if (xmlEntityMyClientDirectorIdDto.getAddresses().getAddress() != null && !xmlEntityMyClientDirectorIdDto.getAddresses().getAddress().isEmpty()) {
                TPersonMyClient.Addresses addresses = new TPersonMyClient.Addresses();
                List<TAddress> addressList = new ArrayList<>();
                for (xmlTAddressDto address : xmlEntityMyClientDirectorIdDto.getAddresses().getAddress()) {
                    TAddressService addressService = new TAddressService();
                    addressList.add(addressService.generateTAddressTag(address));
                }
                addresses.setAddress(addressList);
                directorId.setAddresses(addresses);
            }
        }


        if (xmlEntityMyClientDirectorIdDto.getEmail() != null && !xmlEntityMyClientDirectorIdDto.getEmail().isEmpty()) {
            List<String> emailList = new ArrayList<>();
            for (String email : xmlEntityMyClientDirectorIdDto.getEmail())
                emailList.add(email);

            directorId.setEmail(emailList);
        }

        if (xmlEntityMyClientDirectorIdDto.getOccupation() != null)
            directorId.setOccupation(xmlEntityMyClientDirectorIdDto.getOccupation());

//        if (xmlEntityMyClientDirectorIdDto.getEmployerName() != null)
//            directorId.setEmployerName(xmlEntityMyClientDirectorIdDto.getEmployerName());
//
//
//        if (xmlEntityMyClientDirectorIdDto.getEmployerAddressId() != null) {
//            TAddressService employerAddressService = new TAddressService();
//            directorId.setEmployerAddressId(employerAddressService.generateTAddressTag(xmlEntityMyClientDirectorIdDto.getEmployerAddressId()));
//        }
//
//
//        if (xmlEntityMyClientDirectorIdDto.getEmployerPhoneId() != null) {
//            TPhoneService employerPhoneService = new TPhoneService();
//            directorId.setEmployerPhoneId(employerPhoneService.generateTPhoneTag(xmlEntityMyClientDirectorIdDto.getEmployerPhoneId()));
//        }
//
//
//        if (xmlEntityMyClientDirectorIdDto.getIdentification() != null && !xmlEntityMyClientDirectorIdDto.getIdentification().isEmpty()) {
//            List<TPersonIdentification> identificationList = new ArrayList<>();
//            for (xmlTPersonIdentificationDto identification : xmlEntityMyClientDirectorIdDto.getIdentification()) {
//                TPersonIdentificationService personIdentificationService = new TPersonIdentificationService();
//                identificationList.add(personIdentificationService.generateTPersonIdentificationTag(identification));
//            }
//            directorId.setIdentification(identificationList);
//        }
//
//
//        if (xmlEntityMyClientDirectorIdDto.getDeceased() != null)
//            directorId.setDeceased(xmlEntityMyClientDirectorIdDto.getDeceased());
//
//        if (xmlEntityMyClientDirectorIdDto.getDateDeceased() != null)
//            directorId.setDateDeceased(XmlUtils.createXMLGregorianCalendar(xmlEntityMyClientDirectorIdDto.getDateDeceased().toString()));
//
//        if (xmlEntityMyClientDirectorIdDto.getTaxNumber() != null)
//            directorId.setTaxNumber(xmlEntityMyClientDirectorIdDto.getTaxNumber());
//
//        if (xmlEntityMyClientDirectorIdDto.getTaxRegNumber() != null)
//            directorId.setTaxRegNumber(xmlEntityMyClientDirectorIdDto.getTaxRegNumber());
//
//        if (xmlEntityMyClientDirectorIdDto.getSourceOfWealth() != null)
//            directorId.setSourceOfWealth(xmlEntityMyClientDirectorIdDto.getSourceOfWealth());

        if (xmlEntityMyClientDirectorIdDto.getComments() != null)
            directorId.setComments(xmlEntityMyClientDirectorIdDto.getComments());

        if (xmlEntityMyClientDirectorIdDto.getRole() != null)
            directorId.setRole(xmlEntityMyClientDirectorIdDto.getRole());


        return directorId;
    }
}
