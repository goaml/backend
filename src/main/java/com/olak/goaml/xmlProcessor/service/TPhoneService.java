package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TPhone;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPhoneDto;


public class TPhoneService {

    public TPhone generateTPhoneTag(xmlTPhoneDto tPhoneDto) {
        TPhone tPhone = new TPhone();

        if (tPhoneDto.getTphContactType() != null)
            tPhone.setTphContactType(tPhoneDto.getTphContactType());

        if (tPhoneDto.getTphCommunicationType() != null)
            tPhone.setTphCommunicationType(tPhoneDto.getTphCommunicationType());

        if (tPhoneDto.getTphCountryPrefix() != null)
            tPhone.setTphCountryPrefix(tPhoneDto.getTphCountryPrefix());

        if (tPhoneDto.getTphNumber() != null)
            tPhone.setTphNumber(tPhoneDto.getTphNumber());

        if (tPhoneDto.getTphExtension() != null)
            tPhone.setTphExtension(tPhoneDto.getTphExtension());

        if (tPhoneDto.getComments() != null)
            tPhone.setComments(tPhoneDto.getComments());

        return tPhone;
    }
}
