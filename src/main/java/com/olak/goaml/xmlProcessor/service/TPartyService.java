package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.*;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTPartyDto;

public class TPartyService {

    public TParty generateTPartyTag(xmlTPartyDto tPartyDto) {
        TParty tParty = new TParty();

        if (tPartyDto.getRole() != null)
            tParty.setRole(tPartyDto.getRole());

        if (tPartyDto.getPerson() != null) {
            TPersonService tPersonService = new TPersonService();
            tParty.setPerson(tPersonService.generateTPersonTag(tPartyDto.getPerson()));
        }

        if (tPartyDto.getPersonMyClient() != null) {
            TPersonMyClientService tPersonMyClientService = new TPersonMyClientService();
            tParty.setPersonMyClient(tPersonMyClientService.generateTPersonMyClientTag(tPartyDto.getPersonMyClient()));
        }

        if (tPartyDto.getAccount() != null) {
            TAccountService tAccountService = new TAccountService();
            tParty.setAccount(tAccountService.generateTAccountTag(tPartyDto.getAccount()));
        }

        if (tPartyDto.getAccountMyClient() != null) {
            TAccountMyClientService tAccountMyClientService = new TAccountMyClientService();
            tParty.setAccountMyClient(tAccountMyClientService.generateTAccountMyClientTag(tPartyDto.getAccountMyClient()));
        }

        if (tPartyDto.getEntity() != null) {
            TEntityService tEntityService = new TEntityService();
            tParty.setEntity(tEntityService.generateTEntityTag(tPartyDto.getEntity()));
        }

        if (tPartyDto.getEntityMyClient() != null) {
            TEntityMyClientService tEntityMyClientService = new TEntityMyClientService();
            tParty.setEntityMyClient(tEntityMyClientService.generateTEntityMyClientTag(tPartyDto.getEntityMyClient()));
        }

        if (tPartyDto.getFundsCode() != null)
            tParty.setFundsCode(tPartyDto.getFundsCode());

        if (tPartyDto.getFundsComment() != null)
            tParty.setFundsComment(tPartyDto.getFundsComment());

        if (tPartyDto.getForeignCurrency() != null){
            TForeignCurrencyService tForeignCurrencyService = new TForeignCurrencyService();
            tParty.setForeignCurrency(tForeignCurrencyService.generateTForeignCurrencyTag(tPartyDto.getForeignCurrency()));
        }

        if (tPartyDto.getCountry() != null)
            tParty.setCountry(tPartyDto.getCountry());

        if (tPartyDto.getSignificance() != null)
            tParty.setSignificance(tPartyDto.getSignificance());

        if (tPartyDto.getComments() != null)
            tParty.setComments(tPartyDto.getComments());


        return tParty;
    }
}
