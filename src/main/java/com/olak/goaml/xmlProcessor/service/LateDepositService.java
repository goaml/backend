package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.Report;
import com.olak.goaml.xmlProcessor.xmlDto.xmlLateDepositDto;

public class LateDepositService {

    public Report.Transaction.LateDeposit generateLateDepositTag(xmlLateDepositDto lateDepositReq) {
        Report.Transaction.LateDeposit lateDeposit = new Report.Transaction.LateDeposit();
        lateDeposit.setValue(lateDepositReq.isValue());
        return lateDeposit;
    }
}
