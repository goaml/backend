package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.*;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTToMyClientDto;

public class TToMyClientService {

    public Report.Transaction.TToMyClient generateTToMyClientTag(xmlTToMyClientDto xmlTToMyClientDto) {
        Report.Transaction.TToMyClient tToMyClient = new Report.Transaction.TToMyClient();

        if (xmlTToMyClientDto.getToFundsCode() != null)
            tToMyClient.setToFundsCode(xmlTToMyClientDto.getToFundsCode());

        if (xmlTToMyClientDto.getToFundsComment() != null)
            tToMyClient.setToFundsComment(xmlTToMyClientDto.getToFundsComment());

        if (xmlTToMyClientDto.getToForeignCurrency() != null) {
            TForeignCurrencyService tForeignCurrencyService = new TForeignCurrencyService();
            tToMyClient.setToForeignCurrency(tForeignCurrencyService.generateTForeignCurrencyTag(xmlTToMyClientDto.getToForeignCurrency()));
        }

        if (xmlTToMyClientDto.getToAccount() != null) {
            TAccountMyClientService tAccountMyClientService = new TAccountMyClientService();
            tToMyClient.setToAccount(tAccountMyClientService.generateTAccountMyClientTag(xmlTToMyClientDto.getToAccount()));
        }

        if (xmlTToMyClientDto.getToPerson() != null) {
            TPersonMyClientService tPersonMyClientService = new TPersonMyClientService();
            tToMyClient.setToPerson(tPersonMyClientService.generateTPersonMyClientTag(xmlTToMyClientDto.getToPerson()));
        }

        if (xmlTToMyClientDto.getToEntity() != null) {
            TEntityMyClientService tEntityMyClientService = new TEntityMyClientService();
            tToMyClient.setToEntity(tEntityMyClientService.generateTEntityMyClientTag(xmlTToMyClientDto.getToEntity()));
        }

        if (xmlTToMyClientDto.getToCountry() != null)
            tToMyClient.setToCountry(xmlTToMyClientDto.getToCountry());

        return tToMyClient;
    }
}
