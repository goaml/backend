package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.Report;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTransactionDto;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.math.BigDecimal;

public class TransactionService {

    public Report.Transaction generateTransactionTag(xmlTransactionDto transactionDto) {

        Report.Transaction transaction = new Report.Transaction();
        if (transactionDto.getTransactionnumber() != null)
            transaction.setTransactionnumber(transactionDto.getTransactionnumber());

        if (transactionDto.getInternalRefNumber() != null)
            transaction.setInternalRefNumber(transactionDto.getInternalRefNumber());

        if (transactionDto.getTransactionLocation() != null)
            transaction.setTransactionLocation(transactionDto.getTransactionLocation());

        if (transactionDto.getTransactionDescription() != null)
            transaction.setTransactionDescription(transactionDto.getTransactionDescription());

        if (transactionDto.getDateTransaction() != null)
            transaction.setDateTransaction(XmlUtils.createXMLGregorianCalendar(transactionDto.getDateTransaction().toString()));

        if (transactionDto.getLateDeposit() != null) {
            LateDepositService lateDepositService = new LateDepositService();
            transaction.setLateDeposit(lateDepositService.generateLateDepositTag(transactionDto.getLateDeposit()));
        }

        if (transactionDto.getDatePosting() != null)
            transaction.setDatePosting(XmlUtils.createXMLGregorianCalendar(transactionDto.getDatePosting().toString()));

        if (transactionDto.getValueDate() != null)
            transaction.setValueDate(XmlUtils.createXMLGregorianCalendar(transactionDto.getValueDate().toString()));

        if (transactionDto.getTransmodeCode() != null)
            transaction.setTransmodeCode(transactionDto.getTransmodeCode());

        if (transactionDto.getTransmodeComment() != null)
            transaction.setTransmodeComment(XmlUtils.createJAXBElement("transmode_comment", transactionDto.getTransmodeComment()));

        if (transactionDto.getAmountLocal() != null)
            transaction.setAmountLocal(transactionDto.getAmountLocal());

        if (transactionDto.getInvolvedParties() != null) {
            InvolvedPartiesService involvedPartiesService = new InvolvedPartiesService();
            transaction.setInvolvedParties(involvedPartiesService.generateInvolvedPartiesTag(transactionDto.getInvolvedParties()));
        }

        if (transactionDto.getTFromMyClient() != null) {
            TFromMyClientService tFromMyClientService = new TFromMyClientService();
            transaction.setTFromMyClient(tFromMyClientService.generateTFromMyClientTag(transactionDto.getTFromMyClient()));
        }

        if (transactionDto.getTFrom() != null) {
            TFromService tFromService = new TFromService();
            transaction.setTFrom(tFromService.generateTFromTag(transactionDto.getTFrom()));
        }

        if (transactionDto.getTToMyClient() != null) {
            TToMyClientService tToMyClientService = new TToMyClientService();
            transaction.setTToMyClient(tToMyClientService.generateTToMyClientTag(transactionDto.getTToMyClient()));
        }

        if (transactionDto.getTTo() != null) {
            TToService tToService = new TToService();
            transaction.setTTo(tToService.generateTToTag(transactionDto.getTTo()));
        }

//        transaction.setGoodsServices(new Report.Transaction.GoodsServices());

        if (transactionDto.getComments() != null)
            transaction.setComments(transactionDto.getComments());

        return transaction;
    }
}
