package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.CurrencyType;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TAccountMyClient;
import com.olak.goaml.xmlProcessor.utils.XmlUtils;
import com.olak.goaml.xmlProcessor.xmlDto.xmlAccountMyClientSignatoryDto;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTAccountMyClientDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TAccountMyClientService {

    public TAccountMyClient generateTAccountMyClientTag(xmlTAccountMyClientDto xmlTAccountMyClientDto) {
        TAccountMyClient tAccountMyClient = new TAccountMyClient();

        if (xmlTAccountMyClientDto.getInstitutionName() != null)
            tAccountMyClient.setInstitutionName(xmlTAccountMyClientDto.getInstitutionName());

        if (xmlTAccountMyClientDto.getSwift() != null)
            tAccountMyClient.setSwift(xmlTAccountMyClientDto.getSwift());

        if (xmlTAccountMyClientDto.getNonBankInstitution() != null)
            tAccountMyClient.setNonBankInstitution(xmlTAccountMyClientDto.getNonBankInstitution());

        if (xmlTAccountMyClientDto.getBranch() != null)
            tAccountMyClient.setBranch(xmlTAccountMyClientDto.getBranch());

        if (xmlTAccountMyClientDto.getAccount() != null)
            tAccountMyClient.setAccount(xmlTAccountMyClientDto.getAccount());

        if (xmlTAccountMyClientDto.getCurrencyCode() != null)
            tAccountMyClient.setCurrencyCode(CurrencyType.fromValue(xmlTAccountMyClientDto.getCurrencyCode().getCurrencyCode()));

        if (xmlTAccountMyClientDto.getAccountName() != null)
            tAccountMyClient.setAccountName(xmlTAccountMyClientDto.getAccountName());

        if (xmlTAccountMyClientDto.getIban() != null)
            tAccountMyClient.setIban(xmlTAccountMyClientDto.getIban());

        if (xmlTAccountMyClientDto.getClientNumber() != null)
            tAccountMyClient.setClientNumber(xmlTAccountMyClientDto.getClientNumber());

        if (xmlTAccountMyClientDto.getPersonalAccountType() != null)
            tAccountMyClient.setPersonalAccountType(xmlTAccountMyClientDto.getPersonalAccountType());

        if (xmlTAccountMyClientDto.getTEntity() != null) {
            TEntityMyClientService tEntityMyClientService = new TEntityMyClientService();
            tAccountMyClient.setTEntity(tEntityMyClientService.generateTEntityMyClientTag(xmlTAccountMyClientDto.getTEntity()));
        }

        if (xmlTAccountMyClientDto.getAccount() != null) {
            List<TAccountMyClient.Signatory> signatoryList = new ArrayList<>();
            if (xmlTAccountMyClientDto.getSignatory() != null && !xmlTAccountMyClientDto.getSignatory().isEmpty()) {
                for (xmlAccountMyClientSignatoryDto signatoryDto : xmlTAccountMyClientDto.getSignatory()) {
                    TAccountMyClientSignatoryService tAccountMyClientSignatoryService = new TAccountMyClientSignatoryService();
                    signatoryList.add(tAccountMyClientSignatoryService.generateSignatoryTag(signatoryDto));
                }
            }
            tAccountMyClient.setSignatory(signatoryList);
        }

        if (xmlTAccountMyClientDto.getOpened() != null)
            tAccountMyClient.setOpened(XmlUtils.createXMLGregorianCalendar(xmlTAccountMyClientDto.getOpened().toString()));

        if (xmlTAccountMyClientDto.getClosed() != null)
            tAccountMyClient.setClosed(XmlUtils.createXMLGregorianCalendar(xmlTAccountMyClientDto.getClosed().toString()));

        if (xmlTAccountMyClientDto.getBalance() != null)
            tAccountMyClient.setBalance(xmlTAccountMyClientDto.getBalance());

        if (xmlTAccountMyClientDto.getDateBalance() != null)
            tAccountMyClient.setDateBalance(XmlUtils.createXMLGregorianCalendar(xmlTAccountMyClientDto.getDateBalance().toString()));

        if (xmlTAccountMyClientDto.getStatusCode() != null)
            tAccountMyClient.setStatusCode(xmlTAccountMyClientDto.getStatusCode());

        if (xmlTAccountMyClientDto.getBeneficiary() != null)
            tAccountMyClient.setBeneficiary(XmlUtils.createJAXBElement("beneficiary", xmlTAccountMyClientDto.getBeneficiary()));

        if (xmlTAccountMyClientDto.getBeneficiaryComment() != null)
            tAccountMyClient.setBeneficiaryComment(XmlUtils.createJAXBElement("beneficiary_comment", xmlTAccountMyClientDto.getBeneficiaryComment()));

        if (xmlTAccountMyClientDto.getComments() != null)
            tAccountMyClient.setComments(xmlTAccountMyClientDto.getComments());


        return tAccountMyClient;
    }
}
