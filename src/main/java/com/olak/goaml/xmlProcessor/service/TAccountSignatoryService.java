package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TAccount;
import com.olak.goaml.xmlProcessor.xmlDto.xmlAccountSignatoryDto;

public class TAccountSignatoryService {

    public TAccount.Signatory generateSignatoryTag(xmlAccountSignatoryDto signatoryDto) {
        TAccount.Signatory signatory = new TAccount.Signatory();

        if (signatoryDto.getIsPrimary() != null)
            signatory.setIsPrimary(signatoryDto.getIsPrimary());

        if (signatoryDto.getTPerson() != null) {
            TPersonService tPersonService = new TPersonService();
            signatory.setTPerson(tPersonService.generateTPersonTag(signatoryDto.getTPerson()));
        }

        if (signatoryDto.getRole() != null)
            signatory.setRole(signatoryDto.getRole());

        return signatory;
    }
}
