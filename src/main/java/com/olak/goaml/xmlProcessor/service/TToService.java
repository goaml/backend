package com.olak.goaml.xmlProcessor.service;

import com.olak.goaml.xmlProcessor.xmlConfigDto.*;
import com.olak.goaml.xmlProcessor.xmlDto.xmlTToDto;

public class TToService {

    public Report.Transaction.TTo generateTToTag(xmlTToDto xmlTToDto) {
        Report.Transaction.TTo tTo = new Report.Transaction.TTo();

        if (xmlTToDto.getToFundsCode() != null)
            tTo.setToFundsCode(xmlTToDto.getToFundsCode());

        if (xmlTToDto.getToFundsComment() != null)
            tTo.setToFundsComment(xmlTToDto.getToFundsComment());

        if (xmlTToDto.getToForeignCurrency() != null) {
            TForeignCurrencyService tForeignCurrencyService = new TForeignCurrencyService();
            tTo.setToForeignCurrency(tForeignCurrencyService.generateTForeignCurrencyTag(xmlTToDto.getToForeignCurrency()));
        }

        if (xmlTToDto.getToAccount() != null) {
            TAccountService tAccountService = new TAccountService();
            tTo.setToAccount(tAccountService.generateTAccountTag(xmlTToDto.getToAccount()));
        }

        if (xmlTToDto.getToPerson() != null) {
            TPersonService tPersonService = new TPersonService();
            tTo.setToPerson(tPersonService.generateTPersonTag(xmlTToDto.getToPerson()));
        }

        if (xmlTToDto.getToEntity() != null) {
            TEntityService tEntityService = new TEntityService();
            tTo.setToEntity(tEntityService.generateTEntityTag(xmlTToDto.getToEntity()));
        }

        if (xmlTToDto.getToCountry() != null)
            tTo.setToCountry(xmlTToDto.getToCountry());

        return tTo;
    }
}
