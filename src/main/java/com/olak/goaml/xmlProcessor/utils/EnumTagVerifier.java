package com.olak.goaml.xmlProcessor.utils;

import com.olak.goaml.xmlProcessor.enums.*;

public class EnumTagVerifier {
    public static boolean isTagValid(String enumTagName, String scenario) {
        switch (scenario) {
//            case "GNCB1":
//                TagEnum enumValueS1 = getEnumValue(enumTagName, GNCB1.class);
//                return enumValueS1.isRepeatable();
            case "GNCB2":
                TagEnum enumValueS2 = getEnumValue(enumTagName, GNCB2.class);
                return enumValueS2.isRepeatable();
            case "GNCB3":
                TagEnum enumValueS3 = getEnumValue(enumTagName, GNCB3.class);
                return enumValueS3.isRepeatable();
            case "GNCB4":
                TagEnum enumValueS4 = getEnumValue(enumTagName, GNCB4.class);
                return enumValueS4.isRepeatable();
            case "GNCB5":
                TagEnum enumValueS5 = getEnumValue(enumTagName, GNCB5.class);
                return enumValueS5.isRepeatable();
            case "GNCB6":
                TagEnum enumValueS6 = getEnumValue(enumTagName, GNCB6.class);
                return enumValueS6.isRepeatable();
            case "GNCB7":
                TagEnum enumValueS7 = getEnumValue(enumTagName, GNCB7.class);
                return enumValueS7.isRepeatable();
            case "GNCB8":
                TagEnum enumValueS8 = getEnumValue(enumTagName, GNCB8.class);
                return enumValueS8.isRepeatable();
            case "GNCB9":
                TagEnum enumValueS9 = getEnumValue(enumTagName, GNCB9.class);
                return enumValueS9.isRepeatable();
            default:
                return false;
        }
    }

    public static <T extends Enum<T> & TagEnum> T getEnumValue(String enumConstant, Class<T> enumClass) {
        return Enum.valueOf(enumClass, enumConstant);
    }

}


