package com.olak.goaml.xmlProcessor.enums;

public interface TagEnum {
    boolean isRepeatable();
    String getTag();
}
