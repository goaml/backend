package com.olak.goaml.xmlProcessor.xmlDto;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TAccount;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TEntity;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TForeignCurrency;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TPerson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTFromDto {
    protected String fromFundsCode;
    protected String fromFundsComment;
    protected xmlTForeignCurrencyDto fromForeignCurrency;
    protected xmlTPersonDto tConductor;
    protected xmlTAccountDto fromAccount;
    protected xmlTPersonDto fromPerson;
    protected xmlTEntityDto fromEntity;
    protected String fromCountry;
}
