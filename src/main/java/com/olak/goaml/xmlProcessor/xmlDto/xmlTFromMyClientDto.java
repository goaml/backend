package com.olak.goaml.xmlProcessor.xmlDto;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TAccountMyClient;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TEntityMyClient;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TForeignCurrency;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TPersonMyClient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTFromMyClientDto {
    protected String fromFundsCode;
    protected String fromFundsComment;
    protected xmlTForeignCurrencyDto fromForeignCurrency;
    protected xmlTPersonMyClientDto tConductor;
    protected xmlTAccountMyClientDto fromAccount;
    protected xmlTPersonMyClientDto fromPerson;
    protected xmlTEntityMyClientDto fromEntity;
    protected String fromCountry;
}
