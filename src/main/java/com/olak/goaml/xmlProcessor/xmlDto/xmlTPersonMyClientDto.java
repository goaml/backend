package com.olak.goaml.xmlProcessor.xmlDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTPersonMyClientDto {

    protected String gender;

    protected String title;

    protected String firstName;
    protected String middleName;

    protected String prefix;

    protected String lastName;

    protected LocalDateTime birthdate;

    protected String birthPlace;
    protected String mothersName;

    protected String alias;

    protected String ssn;
    protected String passportNumber;
    protected String passportCountry;
    protected String idNumber;

    protected String nationality1;

    protected String nationality2;

    protected String nationality3;

    protected String residence;

    protected xmlPhonesDto phones;

    protected xmlAddressesDto addresses;

    protected List<String> email;

    protected String occupation;
    protected String employerName;
    protected xmlTAddressDto employerAddressId;

    protected xmlTPhoneDto employerPhoneId;

    protected List<xmlTPersonIdentificationDto> identification;

    protected Boolean deceased;
    protected LocalDateTime dateDeceased;

    protected String taxNumber;

    protected String taxRegNumber;
    protected String sourceOfWealth;

    protected String comments;
}
