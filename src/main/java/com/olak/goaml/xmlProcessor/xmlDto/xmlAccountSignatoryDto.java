package com.olak.goaml.xmlProcessor.xmlDto;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TPerson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlAccountSignatoryDto {

    protected Boolean isPrimary;
    protected xmlTPersonDto tPerson;

    protected String role;
}
