package com.olak.goaml.xmlProcessor.xmlDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTForeignCurrencyDto {
    protected xmlCurrencyTypeDto foreignCurrencyCode;
    protected BigDecimal foreignAmount;
    protected BigDecimal foreignExchangeRate;
}
