package com.olak.goaml.xmlProcessor.xmlDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTAddressDto {
    protected String addressType;

    protected String address;
    protected String town;
    protected String city;
    protected String zip;
    protected String countryCode;
    protected String state;

    protected String comments;
}
