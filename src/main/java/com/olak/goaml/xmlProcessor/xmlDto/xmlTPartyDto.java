package com.olak.goaml.xmlProcessor.xmlDto;

import com.olak.goaml.xmlProcessor.xmlConfigDto.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTPartyDto {

    protected String role;

    protected xmlTPersonDto person;
    protected xmlTPersonMyClientDto personMyClient;

    protected xmlTAccountDto account;
    protected xmlTAccountMyClientDto accountMyClient;

    protected xmlTEntityDto entity;
    protected xmlTEntityMyClientDto entityMyClient;
    protected String fundsCode;
    protected String fundsComment;
    protected xmlTForeignCurrencyDto foreignCurrency;
    protected String country;
    protected Integer significance;

    protected String comments;
}
