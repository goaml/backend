package com.olak.goaml.xmlProcessor.xmlDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlAccountMyClientSignatoryDto {

    protected Boolean isPrimary;
    protected xmlTPersonMyClientDto tPerson;

    protected String role;
}
