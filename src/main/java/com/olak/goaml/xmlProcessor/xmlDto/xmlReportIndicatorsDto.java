package com.olak.goaml.xmlProcessor.xmlDto;

import com.olak.goaml.xmlProcessor.xmlConfigDto.ReportIndicatorType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlReportIndicatorsDto {
    protected List<xmlReportIndicatorTypeDto> indicator;
}
