package com.olak.goaml.xmlProcessor.xmlDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTEntityMyClientDto {

    protected String name;
    protected String commercialName;
    protected String incorporationLegalForm;
    protected String incorporationNumber;

    protected String business;

    protected xmlPhonesDto phones;

    protected xmlAddressesDto addresses;

    protected String email;

    protected String url;

    protected String incorporationState;

    protected String incorporationCountryCode;

    protected List<xmlEntityMyClientDirectorIdDto> directorId;

    protected LocalDateTime incorporationDate;

    protected Boolean businessClosed;

    protected LocalDateTime dateBusinessClosed;

    protected String taxNumber;

    protected String taxRegNumber;

    protected String comments;
}
