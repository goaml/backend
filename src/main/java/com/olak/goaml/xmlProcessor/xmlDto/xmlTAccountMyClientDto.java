package com.olak.goaml.xmlProcessor.xmlDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTAccountMyClientDto {

    protected String institutionName;
    protected String swift;
    protected Boolean nonBankInstitution;

    protected String branch;

    protected String account;
    protected xmlCurrencyTypeDto currencyCode;

    protected String accountName;
    protected String iban;

    protected String clientNumber;

    protected String personalAccountType;

    protected xmlTEntityMyClientDto tEntity;

    protected List<xmlAccountMyClientSignatoryDto> signatory;

    protected LocalDateTime opened;
    protected LocalDateTime closed;
    protected BigDecimal balance;

    protected LocalDateTime dateBalance;

    protected String statusCode;

    protected String beneficiary;

    protected String beneficiaryComment;

    protected String comments;
}
