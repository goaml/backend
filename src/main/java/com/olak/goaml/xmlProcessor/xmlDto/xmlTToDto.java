package com.olak.goaml.xmlProcessor.xmlDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTToDto {
    protected String toFundsCode;
    protected String toFundsComment;
    protected xmlTForeignCurrencyDto toForeignCurrency;
    protected xmlTAccountDto toAccount;
    protected xmlTPersonDto toPerson;

    protected xmlTEntityDto toEntity;
    protected String toCountry;
}
