package com.olak.goaml.xmlProcessor.xmlDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlActivityDto {

    protected String activityCode;
    protected String activityDescription;
}
