package com.olak.goaml.xmlProcessor.xmlDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTPhoneDto {

    protected String tphContactType;
    protected String tphCommunicationType;
    protected String tphCountryPrefix;
    protected String tphNumber;
    protected String tphExtension;

    protected String comments;
}
