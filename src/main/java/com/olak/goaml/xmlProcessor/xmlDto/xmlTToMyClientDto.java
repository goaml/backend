package com.olak.goaml.xmlProcessor.xmlDto;

import com.olak.goaml.xmlProcessor.xmlConfigDto.TAccountMyClient;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TEntityMyClient;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TForeignCurrency;
import com.olak.goaml.xmlProcessor.xmlConfigDto.TPersonMyClient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class xmlTToMyClientDto {
    protected String toFundsCode;
    protected String toFundsComment;
    protected xmlTForeignCurrencyDto toForeignCurrency;
    protected xmlTAccountMyClientDto toAccount;
    protected xmlTPersonMyClientDto toPerson;

    protected xmlTEntityMyClientDto toEntity;
    protected String toCountry;
}
