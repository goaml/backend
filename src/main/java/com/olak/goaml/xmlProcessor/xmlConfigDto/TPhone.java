//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2024.06.10 at 08:34:16 PM IST 
//


package com.olak.goaml.xmlProcessor.xmlConfigDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_phone complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_phone"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tph_contact_type" type="{}contact_type"/&gt;
 *         &lt;element name="tph_communication_type" type="{}communication_type"/&gt;
 *         &lt;element name="tph_country_prefix" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="4"/&gt;
 *               &lt;minLength value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="tph_number"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="50"/&gt;
 *               &lt;minLength value="1"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="tph_extension" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="10"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="comments" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="0"/&gt;
 *               &lt;maxLength value="4000"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_phone", propOrder = {
    "tphContactType",
    "tphCommunicationType",
    "tphCountryPrefix",
    "tphNumber",
    "tphExtension",
    "comments"
})
public class TPhone {

    @XmlElement(name = "tph_contact_type", required = true)
    protected String tphContactType;
    @XmlElement(name = "tph_communication_type", required = true)
    protected String tphCommunicationType;
    @XmlElement(name = "tph_country_prefix")
    protected String tphCountryPrefix;
    @XmlElement(name = "tph_number", required = true)
    protected String tphNumber;
    @XmlElement(name = "tph_extension")
    protected String tphExtension;
    protected String comments;

    /**
     * Gets the value of the tphContactType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTphContactType() {
        return tphContactType;
    }

    /**
     * Sets the value of the tphContactType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTphContactType(String value) {
        this.tphContactType = value;
    }

    /**
     * Gets the value of the tphCommunicationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTphCommunicationType() {
        return tphCommunicationType;
    }

    /**
     * Sets the value of the tphCommunicationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTphCommunicationType(String value) {
        this.tphCommunicationType = value;
    }

    /**
     * Gets the value of the tphCountryPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTphCountryPrefix() {
        return tphCountryPrefix;
    }

    /**
     * Sets the value of the tphCountryPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTphCountryPrefix(String value) {
        this.tphCountryPrefix = value;
    }

    /**
     * Gets the value of the tphNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTphNumber() {
        return tphNumber;
    }

    /**
     * Sets the value of the tphNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTphNumber(String value) {
        this.tphNumber = value;
    }

    /**
     * Gets the value of the tphExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTphExtension() {
        return tphExtension;
    }

    /**
     * Sets the value of the tphExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTphExtension(String value) {
        this.tphExtension = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

}
