package com.olak.goaml.xmlProcessor.xmlConfigDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rentityId",
    "rentityBranch",
    "submissionCode",
    "reportCode",
    "entityReference",
    "fiuRefNumber",
    "submissionDate",
    "currencyCodeLocal",
    "reportingPerson",
    "location",
    "reason",
    "action",
    "transaction",
    "activity",
    "reportIndicators"
})
@XmlRootElement(name = "report")
public class Report {

    @XmlElement(name = "rentity_id")
    protected int rentityId;
    @XmlElement(name = "rentity_branch")
    protected String rentityBranch;
    @XmlElement(name = "submission_code", required = true)
    protected String submissionCode;
    @XmlElement(name = "report_code", required = true)
    @XmlSchemaType(name = "string")
    protected ReportType reportCode;
    @XmlElementRef(name = "entity_reference", type = JAXBElement.class, required = false)
    protected JAXBElement<String> entityReference;
    @XmlElementRef(name = "fiu_ref_number", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fiuRefNumber;
    @XmlElement(name = "submission_date", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar submissionDate;
    @XmlElement(name = "currency_code_local", required = true)
    @XmlSchemaType(name = "string")
    protected CurrencyType currencyCodeLocal;
    @XmlElement(name = "reporting_person")
    protected TPersonRegistrationInReport reportingPerson;
    protected TAddress location;
    protected String reason;
    protected String action;
    protected List<Transaction> transaction;
    protected Activity activity;
    @XmlElement(name = "report_indicators", required = true)
    protected ReportIndicators reportIndicators;


    public int getRentityId() {
        return rentityId;
    }
    public void setRentityId(int value) {
        this.rentityId = value;
    }

    public String getRentityBranch() {
        return rentityBranch;
    }

    public void setRentityBranch(String value) {
        this.rentityBranch = value;
    }

    public String getSubmissionCode() {
        return submissionCode;
    }

    public void setSubmissionCode(String value) {
        this.submissionCode = value;
    }

    public ReportType getReportCode() {
        return reportCode;
    }

    public void setReportCode(ReportType value) {
        this.reportCode = value;
    }

    public JAXBElement<String> getEntityReference() {
        return entityReference;
    }

    public void setEntityReference(JAXBElement<String> value) {
        this.entityReference = value;
    }

    public JAXBElement<String> getFiuRefNumber() {
        return fiuRefNumber;
    }

    public void setFiuRefNumber(JAXBElement<String> value) {
        this.fiuRefNumber = value;
    }

    public XMLGregorianCalendar getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(XMLGregorianCalendar value) {
        this.submissionDate = value;
    }

    public CurrencyType getCurrencyCodeLocal() {
        return currencyCodeLocal;
    }
    public void setCurrencyCodeLocal(CurrencyType value) {
        this.currencyCodeLocal = value;
    }

    public TPersonRegistrationInReport getReportingPerson() {
        return reportingPerson;
    }

    public void setReportingPerson(TPersonRegistrationInReport value) {
        this.reportingPerson = value;
    }

    public TAddress getLocation() {
        return location;
    }
    public void setLocation(TAddress value) {
        this.location = value;
    }
    public String getReason() {
        return reason;
    }
    public void setReason(String value) {
        this.reason = value;
    }
    public String getAction() {
        return action;
    }
    public void setAction(String value) {
        this.action = value;
    }

    public List<Transaction> getTransaction() {
        if (transaction == null) {
            transaction = new ArrayList<Transaction>();
        }
        return this.transaction;
    }
    public void setTransaction(List<Transaction> value) {
        this.transaction = value;
    }
    public Activity getActivity() {
        return activity;
    }
    public void setActivity(Activity value) {
        this.activity = value;
    }
    public ReportIndicators getReportIndicators() {
        return reportIndicators;
    }
    public void setReportIndicators(ReportIndicators value) {
        this.reportIndicators = value;
    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reportParties",
        "goodsServices"
    })
    public static class Activity {

        @XmlElement(name = "report_parties", required = true)
        protected ReportParties reportParties;
        @XmlElement(name = "goods_services")
        protected GoodsServices goodsServices;

        public ReportParties getReportParties() {
            return reportParties;
        }
        public void setReportParties(ReportParties value) {
            this.reportParties = value;
        }
        public GoodsServices getGoodsServices() {
            return goodsServices;
        }
        public void setGoodsServices(GoodsServices value) {
            this.goodsServices = value;
        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "item"
        })
        public static class GoodsServices {

            @XmlElement(required = true)
            protected List<TTransItem> item;

            public List<TTransItem> getItem() {
                if (item == null) {
                    item = new ArrayList<TTransItem>();
                }
                return this.item;
            }

        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "reportParty"
        })
        public static class ReportParties {

            @XmlElement(name = "report_party", required = true)
            protected List<ReportPartyType> reportParty;

            public List<ReportPartyType> getReportParty() {
                if (reportParty == null) {
                    reportParty = new ArrayList<ReportPartyType>();
                }
                return this.reportParty;
            }

        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "indicator"
    })
    public static class ReportIndicators {

        @XmlElement(required = true)
        @XmlSchemaType(name = "string")
        protected List<ReportIndicatorType> indicator;

        public List<ReportIndicatorType> getIndicator() {
            if (indicator == null) {
                indicator = new ArrayList<ReportIndicatorType>();
            }
            return this.indicator;
        }

        public void setIndicator(List<ReportIndicatorType> indicator) {
            this.indicator = indicator;
        }
    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transactionnumber",
        "internalRefNumber",
        "transactionLocation",
        "transactionDescription",
        "dateTransaction",
        "lateDeposit",
        "datePosting",
        "valueDate",
        "transmodeCode",
        "transmodeComment",
        "amountLocal",
        "involvedParties",
        "tFromMyClient",
        "tFrom",
        "tToMyClient",
        "tTo",
        "goodsServices",
        "comments"
    })
    public static class Transaction {

        @XmlElement(required = true)
        protected String transactionnumber;
        @XmlElement(name = "internal_ref_number")
        protected String internalRefNumber;
        @XmlElement(name = "transaction_location")
        protected String transactionLocation;
        @XmlElement(name = "transaction_description", required = true)
        protected String transactionDescription;
        @XmlElement(name = "date_transaction", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar dateTransaction;
        @XmlElement(name = "late_deposit")
        protected LateDeposit lateDeposit;
        @XmlElement(name = "date_posting")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar datePosting;
        @XmlElement(name = "value_date")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar valueDate;
        @XmlElement(name = "transmode_code", required = true)
        protected String transmodeCode;
        @XmlElementRef(name = "transmode_comment", type = JAXBElement.class, required = false)
        protected JAXBElement<String> transmodeComment;
        @XmlElement(name = "amount_local", required = true)
        protected BigDecimal amountLocal;
        @XmlElement(name = "involved_parties")
        protected InvolvedParties involvedParties;
        @XmlElement(name = "t_from_my_client")
        protected TFromMyClient tFromMyClient;
        @XmlElement(name = "t_from")
        protected TFrom tFrom;
        @XmlElement(name = "t_to_my_client")
        protected TToMyClient tToMyClient;
        @XmlElement(name = "t_to")
        protected TTo tTo;
        @XmlElement(name = "goods_services")
        protected GoodsServices goodsServices;
        protected String comments;

        public String getTransactionnumber() {
            return transactionnumber;
        }

        public void setTransactionnumber(String value) {
            this.transactionnumber = value;
        }

        public String getInternalRefNumber() {
            return internalRefNumber;
        }

        public void setInternalRefNumber(String value) {
            this.internalRefNumber = value;
        }
        public String getTransactionLocation() {
            return transactionLocation;
        }
        public void setTransactionLocation(String value) {
            this.transactionLocation = value;
        }
        public String getTransactionDescription() {
            return transactionDescription;
        }
        public void setTransactionDescription(String value) {
            this.transactionDescription = value;
        }
        public XMLGregorianCalendar getDateTransaction() {
            return dateTransaction;
        }
        public void setDateTransaction(XMLGregorianCalendar value) {
            this.dateTransaction = value;
        }
        public LateDeposit getLateDeposit() {
            return lateDeposit;
        }
        public void setLateDeposit(LateDeposit value) {
            this.lateDeposit = value;
        }
        public XMLGregorianCalendar getDatePosting() {
            return datePosting;
        }
        public void setDatePosting(XMLGregorianCalendar value) {
            this.datePosting = value;
        }
        public XMLGregorianCalendar getValueDate() {
            return valueDate;
        }
        public void setValueDate(XMLGregorianCalendar value) {
            this.valueDate = value;
        }
        public String getTransmodeCode() {
            return transmodeCode;
        }
        public void setTransmodeCode(String value) {
            this.transmodeCode = value;
        }
        public JAXBElement<String> getTransmodeComment() {
            return transmodeComment;
        }
        public void setTransmodeComment(JAXBElement<String> value) {
            this.transmodeComment = value;
        }
        public BigDecimal getAmountLocal() {
            return amountLocal;
        }
        public void setAmountLocal(BigDecimal value) {
            this.amountLocal = value;
        }
        public InvolvedParties getInvolvedParties() {
            return involvedParties;
        }
        public void setInvolvedParties(InvolvedParties value) {
            this.involvedParties = value;
        }
        public TFromMyClient getTFromMyClient() {
            return tFromMyClient;
        }
        public void setTFromMyClient(TFromMyClient value) {
            this.tFromMyClient = value;
        }
        public TFrom getTFrom() {
            return tFrom;
        }
        public void setTFrom(TFrom value) {
            this.tFrom = value;
        }
        public TToMyClient getTToMyClient() {
            return tToMyClient;
        }
        public void setTToMyClient(TToMyClient value) {
            this.tToMyClient = value;
        }
        public TTo getTTo() {
            return tTo;
        }
        public void setTTo(TTo value) {
            this.tTo = value;
        }
        public GoodsServices getGoodsServices() {
            return goodsServices;
        }
        public void setGoodsServices(GoodsServices value) {
            this.goodsServices = value;
        }
        public String getComments() {
            return comments;
        }
        public void setComments(String value) {
            this.comments = value;
        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "item"
        })
        public static class GoodsServices {

            @XmlElement(required = true)
            protected List<TTransItem> item;

            public List<TTransItem> getItem() {
                if (item == null) {
                    item = new ArrayList<TTransItem>();
                }
                return this.item;
            }

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "party"
        })
        public static class InvolvedParties {

            @XmlElement(required = true)
            protected List<TParty> party;

            public List<TParty> getParty() {
                if (party == null) {
                    party = new ArrayList<TParty>();
                }
                return this.party;
            }

            public void setParty(List<TParty> party) {
                this.party = party;
            }
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class LateDeposit {

            @XmlValue
            protected boolean value;

            public boolean isValue() {
                return value;
            }

            public void setValue(boolean value) {
                this.value = value;
            }

        }


        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fromFundsCode",
            "fromFundsComment",
            "fromForeignCurrency",
            "tConductor",
            "fromAccount",
            "fromPerson",
            "fromEntity",
            "fromCountry"
        })
        public static class TFrom {

            @XmlElement(name = "from_funds_code", required = true)
            protected String fromFundsCode;
            @XmlElement(name = "from_funds_comment")
            protected String fromFundsComment;
            @XmlElement(name = "from_foreign_currency")
            protected TForeignCurrency fromForeignCurrency;
            @XmlElement(name = "t_conductor")
            protected TPerson tConductor;
            @XmlElement(name = "from_account")
            protected TAccount fromAccount;
            @XmlElement(name = "from_person")
            protected TPerson fromPerson;
            @XmlElement(name = "from_entity")
            protected TEntity fromEntity;
            @XmlElement(name = "from_country", required = true)
            protected String fromCountry;

            public String getFromFundsCode() {
                return fromFundsCode;
            }

            public void setFromFundsCode(String value) {
                this.fromFundsCode = value;
            }

            public String getFromFundsComment() {
                return fromFundsComment;
            }

            public void setFromFundsComment(String value) {
                this.fromFundsComment = value;
            }

            public TForeignCurrency getFromForeignCurrency() {
                return fromForeignCurrency;
            }

            public void setFromForeignCurrency(TForeignCurrency value) {
                this.fromForeignCurrency = value;
            }

            public TPerson getTConductor() {
                return tConductor;
            }

            public void setTConductor(TPerson value) {
                this.tConductor = value;
            }

            public TAccount getFromAccount() {
                return fromAccount;
            }

            public void setFromAccount(TAccount value) {
                this.fromAccount = value;
            }

            
            public TPerson getFromPerson() {
                return fromPerson;
            }

            
            public void setFromPerson(TPerson value) {
                this.fromPerson = value;
            }

            
            public TEntity getFromEntity() {
                return fromEntity;
            }

            
            public void setFromEntity(TEntity value) {
                this.fromEntity = value;
            }

            
            public String getFromCountry() {
                return fromCountry;
            }

            
            public void setFromCountry(String value) {
                this.fromCountry = value;
            }

        }


        
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fromFundsCode",
            "fromFundsComment",
            "fromForeignCurrency",
            "tConductor",
            "fromAccount",
            "fromPerson",
            "fromEntity",
            "fromCountry"
        })
        public static class TFromMyClient {

            @XmlElement(name = "from_funds_code", required = true)
            protected String fromFundsCode;
            @XmlElement(name = "from_funds_comment")
            protected String fromFundsComment;
            @XmlElement(name = "from_foreign_currency")
            protected TForeignCurrency fromForeignCurrency;
            @XmlElement(name = "t_conductor")
            protected TPersonMyClient tConductor;
            @XmlElement(name = "from_account")
            protected TAccountMyClient fromAccount;
            @XmlElement(name = "from_person")
            protected TPersonMyClient fromPerson;
            @XmlElement(name = "from_entity")
            protected TEntityMyClient fromEntity;
            @XmlElement(name = "from_country", required = true)
            protected String fromCountry;

            
            public String getFromFundsCode() {
                return fromFundsCode;
            }

            public void setFromFundsCode(String value) {
                this.fromFundsCode = value;
            }

            public String getFromFundsComment() {
                return fromFundsComment;
            }

            public void setFromFundsComment(String value) {
                this.fromFundsComment = value;
            }
            
            public TForeignCurrency getFromForeignCurrency() {
                return fromForeignCurrency;
            }
            
            public void setFromForeignCurrency(TForeignCurrency value) {
                this.fromForeignCurrency = value;
            }

            public TPersonMyClient getTConductor() {
                return tConductor;
            }

            public void setTConductor(TPersonMyClient value) {
                this.tConductor = value;
            }
            
            public TAccountMyClient getFromAccount() {
                return fromAccount;
            }

            public void setFromAccount(TAccountMyClient value) {
                this.fromAccount = value;
            }
            
            public TPersonMyClient getFromPerson() {
                return fromPerson;
            }

            public void setFromPerson(TPersonMyClient value) {
                this.fromPerson = value;
            }

            public TEntityMyClient getFromEntity() {
                return fromEntity;
            }
            
            public void setFromEntity(TEntityMyClient value) {
                this.fromEntity = value;
            }

            public String getFromCountry() {
                return fromCountry;
            }

            public void setFromCountry(String value) {
                this.fromCountry = value;
            }

        }


        
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "toFundsCode",
            "toFundsComment",
            "toForeignCurrency",
            "toAccount",
            "toPerson",
            "toEntity",
            "toCountry"
        })
        public static class TTo {

            @XmlElement(name = "to_funds_code", required = true)
            protected String toFundsCode;
            @XmlElement(name = "to_funds_comment")
            protected String toFundsComment;
            @XmlElement(name = "to_foreign_currency")
            protected TForeignCurrency toForeignCurrency;
            @XmlElement(name = "to_account")
            protected TAccount toAccount;
            @XmlElement(name = "to_person")
            protected TPerson toPerson;
            @XmlElement(name = "to_entity")
            protected TEntity toEntity;
            @XmlElement(name = "to_country", required = true)
            protected String toCountry;

            
            public String getToFundsCode() {
                return toFundsCode;
            }

            public void setToFundsCode(String value) {
                this.toFundsCode = value;
            }

            public String getToFundsComment() {
                return toFundsComment;
            }

            public void setToFundsComment(String value) {
                this.toFundsComment = value;
            }

            public TForeignCurrency getToForeignCurrency() {
                return toForeignCurrency;
            }
            
            public void setToForeignCurrency(TForeignCurrency value) {
                this.toForeignCurrency = value;
            }

            public TAccount getToAccount() {
                return toAccount;
            }
            
            public void setToAccount(TAccount value) {
                this.toAccount = value;
            }
            
            public TPerson getToPerson() {
                return toPerson;
            }
            
            public void setToPerson(TPerson value) {
                this.toPerson = value;
            }
            
            public TEntity getToEntity() {
                return toEntity;
            }
            
            public void setToEntity(TEntity value) {
                this.toEntity = value;
            }
            
            public String getToCountry() {
                return toCountry;
            }
            
            public void setToCountry(String value) {
                this.toCountry = value;
            }

        }


        
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "toFundsCode",
            "toFundsComment",
            "toForeignCurrency",
            "toAccount",
            "toPerson",
            "toEntity",
            "toCountry"
        })
        public static class TToMyClient {

            @XmlElement(name = "to_funds_code", required = true)
            protected String toFundsCode;
            @XmlElement(name = "to_funds_comment")
            protected String toFundsComment;
            @XmlElement(name = "to_foreign_currency")
            protected TForeignCurrency toForeignCurrency;
            @XmlElement(name = "to_account")
            protected TAccountMyClient toAccount;
            @XmlElement(name = "to_person")
            protected TPersonMyClient toPerson;
            @XmlElement(name = "to_entity")
            protected TEntityMyClient toEntity;
            @XmlElement(name = "to_country", required = true)
            protected String toCountry;

            
            public String getToFundsCode() {
                return toFundsCode;
            }

            
            public void setToFundsCode(String value) {
                this.toFundsCode = value;
            }

            
            public String getToFundsComment() {
                return toFundsComment;
            }

            
            public void setToFundsComment(String value) {
                this.toFundsComment = value;
            }

            
            public TForeignCurrency getToForeignCurrency() {
                return toForeignCurrency;
            }

            
            public void setToForeignCurrency(TForeignCurrency value) {
                this.toForeignCurrency = value;
            }

            
            public TAccountMyClient getToAccount() {
                return toAccount;
            }

            
            public void setToAccount(TAccountMyClient value) {
                this.toAccount = value;
            }

            
            public TPersonMyClient getToPerson() {
                return toPerson;
            }

            
            public void setToPerson(TPersonMyClient value) {
                this.toPerson = value;
            }

            
            public TEntityMyClient getToEntity() {
                return toEntity;
            }

            
            public void setToEntity(TEntityMyClient value) {
                this.toEntity = value;
            }

            
            public String getToCountry() {
                return toCountry;
            }

            
            public void setToCountry(String value) {
                this.toCountry = value;
            }

        }

    }

}
