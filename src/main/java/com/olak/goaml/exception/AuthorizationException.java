package com.olak.goaml.exception;

public class AuthorizationException extends RuntimeException {
    private String i18nKey;
    private int httpCode = 401;

    public AuthorizationException(String msg) {
        super(msg);
    }

    public AuthorizationException(String msg, Exception e) {
        super(msg, e);
    }

    public String getI18nKey() {
        return i18nKey;
    }

    public void setI18nKey(String i18nKey) {
        this.i18nKey = i18nKey;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }
}
