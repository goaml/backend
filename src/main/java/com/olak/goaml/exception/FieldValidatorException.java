package com.olak.goaml.exception;

public class FieldValidatorException extends RuntimeException {
    private String fieldName;
    private String i18nKey;

    public FieldValidatorException() {
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getI18nKey() {
        return i18nKey;
    }

    public void setI18nKey(String i18nKey) {
        this.i18nKey = i18nKey;
    }
}
