package com.olak.goaml.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.logging.Logger;

@ControllerAdvice
@ResponseBody
public class ExceptionHandlerAdvice {
    private static final Logger LOGGER = Logger.getLogger("global.exception");
    private static final Integer ACCESS_DENIED_HASH = 544818101;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({FieldValidatorException.class})
    public ErrorDto handleFieldValidatorException(FieldValidatorException e) {
        LOGGER.info(e.getMessage());
        return new ErrorDto(HttpStatus.BAD_REQUEST.value(), e.getI18nKey(), e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({BusinessRuleException.class})
    public ErrorDto businessRuleException(BusinessRuleException e) {
        LOGGER.info(e.getMessage());
        return new ErrorDto(HttpStatus.BAD_REQUEST.value(), e.getI18nKey(), e.getMessage());
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler({AuthorizationException.class})
    public ErrorDto authorizationException(AuthorizationException e) {
        LOGGER.info(e.getMessage());
        return new ErrorDto(HttpStatus.BAD_REQUEST.value(), e.getI18nKey(), e.getMessage());
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({AuthenticationException.class})
    public ErrorDto handleAuthenticationException(AuthenticationException e) {
        LOGGER.info(e.getMessage());
        return new ErrorDto(HttpStatus.BAD_REQUEST.value(), e.getI18nKey(), e.getMessage());
    }

}