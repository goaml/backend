package com.olak.goaml.exception;

import java.util.HashMap;
import java.util.Map;

public class RuleException extends RuntimeException {
    private Map response = new HashMap();

    public RuleException() {
    }

    public Map getResponse() {
        return response;
    }

    public void setResponse(Map response) {
        this.response = response;
    }
}
