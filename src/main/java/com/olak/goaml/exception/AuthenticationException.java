package com.olak.goaml.exception;

public class AuthenticationException extends RuntimeException {
    private String i18nKey;
    private int httpCode = 401;

    public AuthenticationException(String msg) {
        super(msg);
    }

    public AuthenticationException(String msg, Exception e) {
        super(msg, e);
    }

    public int getHttpCode() {
        return this.httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public String getI18nKey() {
        return this.i18nKey;
    }

    public void setI18nKey(String i18nKey) {
        this.i18nKey = i18nKey;
    }

}