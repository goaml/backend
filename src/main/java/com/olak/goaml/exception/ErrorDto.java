package com.olak.goaml.exception;

public class ErrorDto {
    private int httpCode;
    private String errorCode;
    private String description;

    public ErrorDto(int httpCode, String errorCode, String description) {
        this.httpCode = httpCode;
        this.errorCode = errorCode;
        this.description = description;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
