package com.olak.goaml.exception;

import lombok.Getter;

@Getter
public class BusinessRuleException extends RuntimeException {

    private String i18nKey;
    private String message;

    public BusinessRuleException() {
    }

    public String getI18nKey() {
        return i18nKey;
    }

    public void setI18nKey(String i18nKey) {
        this.i18nKey = i18nKey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
