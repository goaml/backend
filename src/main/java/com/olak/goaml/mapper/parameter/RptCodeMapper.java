package com.olak.goaml.mapper.parameter;

import com.olak.goaml.dto.master.RptCodeDto;
import com.olak.goaml.models.master.RptCode;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RptCodeMapper {

    @Mappings({
            @Mapping(target = "goamlTrxCodeId", source = "goAmlTrxCodes.goamlTrxCodeId"),
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    RptCodeDto toDto(RptCode entity);

    @Mappings({
            @Mapping(source = "goamlTrxCodeId", target = "goAmlTrxCodes.goamlTrxCodeId"),
            @Mapping(source = "statusId", target = "rStatus.statusId")
    })
    RptCode toEntity(RptCodeDto dto);

    @Mappings({
            @Mapping(target = "goamlTrxCodeId", source = "goAmlTrxCodes.goamlTrxCodeId"),
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<RptCodeDto> listToDto(List<RptCode> entities);


}