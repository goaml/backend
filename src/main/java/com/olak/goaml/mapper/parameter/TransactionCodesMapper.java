package com.olak.goaml.mapper.parameter;

import com.olak.goaml.dto.master.TransactionCodesDto;
import com.olak.goaml.models.master.TransactionCodes;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TransactionCodesMapper {
    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    TransactionCodesDto toDto(TransactionCodes entity);

    @Mappings({
            @Mapping(source = "statusId", target = "rStatus.statusId")
    })
    TransactionCodes toEntity(TransactionCodesDto dto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<TransactionCodesDto> listToDto(List<TransactionCodes> entities);




}