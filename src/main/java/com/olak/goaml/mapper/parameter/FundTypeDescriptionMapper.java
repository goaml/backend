package com.olak.goaml.mapper.parameter;

import com.olak.goaml.dto.master.FundTypeDescriptionDto;

import com.olak.goaml.models.master.FundTypeDescription;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FundTypeDescriptionMapper {

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    FundTypeDescriptionDto toDto(FundTypeDescription entity);

    @Mappings({
            @Mapping(source = "statusId", target = "rStatus.statusId")
    })
    FundTypeDescription toEntity(FundTypeDescriptionDto dto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<FundTypeDescriptionDto> listToDto(List<FundTypeDescription> entities);

}