package com.olak.goaml.mapper.parameter;

import com.olak.goaml.dto.master.ReportTypeDto;
import com.olak.goaml.models.master.ReportType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ReportTypeMapper {

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    ReportTypeDto toDto(ReportType entity);

    @Mappings({
            @Mapping(source = "statusId", target = "rStatus.statusId")
    })
    ReportType toEntity(ReportTypeDto dto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<ReportTypeDto> listToDto(List<ReportType> entities);


}