package com.olak.goaml.mapper.parameter;

import com.olak.goaml.dto.master.FundTypeDto;
import com.olak.goaml.models.master.FundType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FundTypeMapper {

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    FundTypeDto toDto(FundType entity);

    @Mappings({
            @Mapping(source = "statusId", target = "rStatus.statusId")
    })
    FundType toEntity(FundTypeDto dto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<FundTypeDto> listToDto(List<FundType> entities);


}