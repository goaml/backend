package com.olak.goaml.mapper.parameter;

import com.olak.goaml.dto.master.*;
import com.olak.goaml.models.master.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper {
    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    CategoryDto toDto(Category entity);

    @Mappings({
            @Mapping(source = "statusId", target = "rStatus.statusId")
    })
    Category toEntity(CategoryDto dto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<CategoryDto> listToDto(List<Category> entities);




}