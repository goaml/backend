package com.olak.goaml.mapper.parameter;

import com.olak.goaml.dto.master.MultiPartyInvolvePartyDto;
import com.olak.goaml.models.master.MultiPartyInvolveParty;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MultiPartyInvolvePartyMapper {

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    MultiPartyInvolvePartyDto toDto(MultiPartyInvolveParty entity);

    @Mappings({
            @Mapping(source = "statusId", target = "rStatus.statusId")
    })
    MultiPartyInvolveParty toEntity(MultiPartyInvolvePartyDto dto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<MultiPartyInvolvePartyDto> listToDto(List<MultiPartyInvolveParty> entities);


}