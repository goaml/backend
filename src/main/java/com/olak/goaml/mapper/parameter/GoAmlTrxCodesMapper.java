package com.olak.goaml.mapper.parameter;

import com.olak.goaml.dto.master.GoAmlTrxCodesDto;
import com.olak.goaml.models.master.GoAmlTrxCodes;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GoAmlTrxCodesMapper {

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    GoAmlTrxCodesDto toDto(GoAmlTrxCodes entity);

    @Mappings({
            @Mapping(source = "statusId", target = "rStatus.statusId")
    })
    GoAmlTrxCodes toEntity(GoAmlTrxCodesDto dto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<GoAmlTrxCodesDto> listToDto(List<GoAmlTrxCodes> entities);

}