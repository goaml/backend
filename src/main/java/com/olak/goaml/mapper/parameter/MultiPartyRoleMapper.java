package com.olak.goaml.mapper.parameter;

import com.olak.goaml.dto.master.MultiPartyRoleDto;
import com.olak.goaml.models.master.MultiPartyRole;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MultiPartyRoleMapper {

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    MultiPartyRoleDto toDto(MultiPartyRole entity);

    @Mappings({
            @Mapping(source = "statusId", target = "rStatus.statusId")
    })
    MultiPartyRole toEntity(MultiPartyRoleDto dto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<MultiPartyRoleDto> listToDto(List<MultiPartyRole> entities);


}