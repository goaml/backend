package com.olak.goaml.mapper.parameter;

import com.olak.goaml.dto.master.MultiPartyFundCodeDto;
import com.olak.goaml.models.master.MultiPartyFundsCode;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MultiPartyFundsCodeMapper {

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    MultiPartyFundCodeDto toDto(MultiPartyFundsCode entity);

    @Mappings({
            @Mapping(source = "statusId", target = "rStatus.statusId")
    })
    MultiPartyFundsCode toEntity(MultiPartyFundCodeDto dto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<MultiPartyFundCodeDto> listToDto(List<MultiPartyFundsCode> entities);


}