package com.olak.goaml.mapper.master;

import com.olak.goaml.dto.master.TemplateModelDto;
import com.olak.goaml.models.master.TemplateModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TemplateMapper {

    TemplateModelDto toDto(TemplateModel templateModel);

    TemplateModel toEntity(TemplateModelDto templateModelDto);

}