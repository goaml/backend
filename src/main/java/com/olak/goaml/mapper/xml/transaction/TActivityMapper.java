package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TActivityDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TActivity;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TActivityMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    TActivity toEntity(TActivityDto TActivityDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    TActivityDto toDto(TActivity TActivity);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<TActivityDto> listToDto(List<TActivity> entities);
}