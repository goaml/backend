package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.dto.xml.transaction.TToDto;
import com.olak.goaml.models.xml.transaction.TReport;
import com.olak.goaml.models.xml.transaction.TTo;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TToMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "fundTypeId", target = "RFundsType.fundTypeId"),
            @Mapping(source = "countryCodeId", target = "RCountryCodes.countryCodeId"),
            @Mapping(source = "transactionId", target = "TTransaction.transactionId")
    })
    TTo toEntity(TToDto TToDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "fundTypeId", source = "RFundsType.fundTypeId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId"),
            @Mapping(target = "transactionId", source = "TTransaction.transactionId")
    })
    TToDto toDto(TTo TTo);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "fundTypeId", source = "RFundsType.fundTypeId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId"),
            @Mapping(target = "transactionId", source = "TTransaction.transactionId")
    })
    List<TToDto> listToDto(List<TTo> entities);
}