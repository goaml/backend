package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TPersonMyClientDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TPersonMyClient;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TPersonMyClientMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "clientTypeId", target = "RClientType.clientTypeId")
    })
    TPersonMyClient toEntity(TPersonMyClientDto TPersonMyClientDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "clientTypeId", source = "RClientType.clientTypeId")
    })
    TPersonMyClientDto toDto(TPersonMyClient TPersonMyClient);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "clientTypeId", source = "RClientType.clientTypeId")
    })
    List<TPersonMyClientDto> listToDto(List<TPersonMyClient> entities);
}