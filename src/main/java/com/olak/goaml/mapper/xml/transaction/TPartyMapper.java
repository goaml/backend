package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TPartyDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TParty;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TPartyMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "partyTypeId", target = "RPartyType.partyTypeId"),
            @Mapping(source = "fundTypeId", target = "RFundsType.fundTypeId"),
            @Mapping(source = "transactionId", target = "TTransaction.transactionId"),
            @Mapping(source = "countryCodeId", target = "RCountryCodes.countryCodeId")
    })
    TParty toEntity(TPartyDto TPartyDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "partyTypeId", source = "RPartyType.partyTypeId"),
            @Mapping(target = "fundTypeId", source = "RFundsType.fundTypeId"),
            @Mapping(target = "transactionId", source = "TTransaction.transactionId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId")
    })
    TPartyDto toDto(TParty TParty);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "partyTypeId", source = "RPartyType.partyTypeId"),
            @Mapping(target = "fundTypeId", source = "RFundsType.fundTypeId"),
            @Mapping(target = "transactionId", source = "TTransaction.transactionId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId")
    })
    List<TPartyDto> listToDto(List<TParty> entities);
}