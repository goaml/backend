package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TForeignCurrencyDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TForeignCurrency;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TForeignCurrencyMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "currenciesId", target = "RCurrency.currenciesId")
    })
    TForeignCurrency toEntity(TForeignCurrencyDto TForeignCurrencyDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "currenciesId", source = "RCurrency.currenciesId")
    })
    TForeignCurrencyDto toDto(TForeignCurrency TForeignCurrency);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "currenciesId", source = "RCurrency.currenciesId")
    })
    List<TForeignCurrencyDto> listToDto(List<TForeignCurrency> entities);
}