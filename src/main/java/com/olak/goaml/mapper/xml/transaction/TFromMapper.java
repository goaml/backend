package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TFromDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.models.xml.transaction.TFrom;
import com.olak.goaml.models.xml.transaction.TReport;
import com.olak.goaml.models.xml.transaction.TTransaction;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TFromMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "transactionId", target = "TTransaction.transactionId"),
            @Mapping(source = "countryCodeId", target = "RCountryCodes.countryCodeId"),
            @Mapping(source = "fundTypeId", target = "RFundsType.fundTypeId")

    })
    TFrom toEntity(TFromDto TFromDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "transactionId", source = "TTransaction.transactionId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId"),
            @Mapping(target = "fundTypeId", source = "RFundsType.fundTypeId")
    })
    TFromDto toDto(TFrom TFrom);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "transactionId", source = "TTransaction.transactionId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId"),
            @Mapping(target = "fundTypeId", source = "RFundsType.fundTypeId")
    })
    List<TFromDto> listToDto(List<TFrom> entities);
}