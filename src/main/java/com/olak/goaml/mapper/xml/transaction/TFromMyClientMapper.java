package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TFromMyClientDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TFromMyClient;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TFromMyClientMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "transactionId", target = "TTransaction.transactionId"),
            @Mapping(source = "countryCodeId", target = "RCountryCodes.countryCodeId"),
            @Mapping(source = "fundTypeId", target = "RFundsType.fundTypeId")
    })
    TFromMyClient toEntity(TFromMyClientDto TFromMyClientDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "transactionId", source = "TTransaction.transactionId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId"),
            @Mapping(target = "fundTypeId", source = "RFundsType.fundTypeId")
    })
    TFromMyClientDto toDto(TFromMyClient TFromMyClient);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "transactionId", source = "TTransaction.transactionId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId"),
            @Mapping(target = "fundTypeId", source = "RFundsType.fundTypeId")
    })
    List<TFromMyClientDto> listToDto(List<TFromMyClient> entities);
}