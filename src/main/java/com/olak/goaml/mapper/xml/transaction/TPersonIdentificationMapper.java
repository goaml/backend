package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TPersonIdentificationDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TPersonIdentification;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TPersonIdentificationMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "identifierTypeId", target = "RIdentifierType.identifierTypeId")
    })
    TPersonIdentification toEntity(TPersonIdentificationDto TPersonIdentificationDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "identifierTypeId", source = "RIdentifierType.identifierTypeId")
    })
    TPersonIdentificationDto toDto(TPersonIdentification TPersonIdentification);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "identifierTypeId", source = "RIdentifierType.identifierTypeId")
    })
    List<TPersonIdentificationDto> listToDto(List<TPersonIdentification> entities);
}