package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TPhoneDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TPhone;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TPhoneMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "communicationTypeId", target = "RCommunicationType.communicationTypeId"),
            @Mapping(source = "contactTypeId", target = "RContactType.contactTypeId")
    })
    TPhone toEntity(TPhoneDto TPhoneDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "communicationTypeId", source = "RCommunicationType.communicationTypeId"),
            @Mapping(target = "contactTypeId", source = "RContactType.contactTypeId")
    })
    TPhoneDto toDto(TPhone TPhone);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "communicationTypeId", source = "RCommunicationType.communicationTypeId"),
            @Mapping(target = "contactTypeId", source = "RContactType.contactTypeId")
    })
    List<TPhoneDto> listToDto(List<TPhone> entities);
}