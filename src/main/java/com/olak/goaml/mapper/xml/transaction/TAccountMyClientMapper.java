package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TAccountMyClientDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TAccountMyClient;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TAccountMyClientMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "clientTypeId", target = "RClientType.clientTypeId")
    })
    TAccountMyClient toEntity(TAccountMyClientDto TAccountMyClientDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "clientTypeId", source = "RClientType.clientTypeId")
    })
    TAccountMyClientDto toDto(TAccountMyClient TAccountMyClient);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "clientTypeId", source = "RClientType.clientTypeId")
    })
    List<TAccountMyClientDto> listToDto(List<TAccountMyClient> entities);
}