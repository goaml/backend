package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.dto.xml.transaction.TToMyClientDto;
import com.olak.goaml.models.xml.transaction.TReport;
import com.olak.goaml.models.xml.transaction.TToMyClient;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TToMyClientMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "fundTypeId", target = "RFundsType.fundTypeId"),
            @Mapping(source = "countryCodeId", target = "RCountryCodes.countryCodeId"),
            @Mapping(source = "transactionId", target = "TTransaction.transactionId")
    })
    TToMyClient toEntity(TToMyClientDto TToMyClientDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "fundTypeId", source = "RFundsType.fundTypeId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId"),
            @Mapping(target = "transactionId", source = "TTransaction.transactionId")
    })
    TToMyClientDto toDto(TToMyClient TToMyClient);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "fundTypeId", source = "RFundsType.fundTypeId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId"),
            @Mapping(target = "transactionId", source = "TTransaction.transactionId")
    })
    List<TToMyClientDto> listToDto(List<TToMyClient> entities);
}