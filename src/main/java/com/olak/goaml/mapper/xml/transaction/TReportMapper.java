package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TReportMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    TReport toEntity(TReportDto TReportDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    TReportDto toDto(TReport TReport);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId")
    })
    List<TReportDto> listToDto(List<TReport> entities);
}