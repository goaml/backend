package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TAddressDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TAddress;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TAddressMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "countryCodeId", target = "RCountryCodes.countryCodeId"),
            @Mapping(source = "contactTypeId", target = "RContactType.contactTypeId"),
            @Mapping(source = "cityId", target = "RCityList.cityId")
    })
    TAddress toEntity(TAddressDto TAddressDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId"),
            @Mapping(target = "contactTypeId", source = "RContactType.contactTypeId"),
            @Mapping(target = "cityId", source = "RCityList.cityId")
    })
    TAddressDto toDto(TAddress TAddress);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "countryCodeId", source = "RCountryCodes.countryCodeId"),
            @Mapping(target = "contactTypeId", source = "RContactType.contactTypeId"),
            @Mapping(target = "cityId", source = "RCityList.cityId")
    })
    List<TAddressDto> listToDto(List<TAddress> entities);
}