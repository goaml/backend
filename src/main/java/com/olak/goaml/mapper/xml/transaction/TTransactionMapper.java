package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TTransactionDto;
import com.olak.goaml.models.xml.transaction.TTransaction;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TTransactionMapper {

    @Mappings({
            @Mapping(target = "RConductionType.conductionTypeId", source = "conductionTypeId"),
            @Mapping(target = "RStatus.statusId", source = "statusId"),
            @Mapping(target = "TReport.reportId", source = "reportId")
//            @Mapping(target = "TFroms", source = "TFroms"),
//            @Mapping(target = "TFromMyClients", source = "TFromMyClients"),
//            @Mapping(target = "TTos", source = "TTos"),
//            @Mapping(target = "TToMyClients", source = "TToMyClients")
    })
    TTransaction toEntity(TTransactionDto TTransactionDto);

    @Mappings({
            @Mapping(source = "RConductionType.conductionTypeId", target = "conductionTypeId"),
            @Mapping(source = "RStatus.statusId", target = "statusId"),
            @Mapping(source = "TReport.reportId", target = "reportId")
//            @Mapping(source = "TFroms", target = "TFroms"),
//            @Mapping(source = "TFromMyClients", target = "TFromMyClients"),
//            @Mapping(source = "TTos", target = "TTos"),
//            @Mapping(source = "TToMyClients", target = "TToMyClients")
    })
    TTransactionDto toDto(TTransaction TTransaction);

    @Mappings({
            @Mapping(source = "RConductionType.conductionTypeId", target = "conductionTypeId"),
            @Mapping(source = "RStatus.statusId", target = "statusId"),
            @Mapping(source = "TReport.reportId", target = "reportId")
//            @Mapping(source = "TFroms", target = "TFroms"),
//            @Mapping(source = "TFromMyClients", target = "TFromMyClients"),
//            @Mapping(source = "TTos", target = "TTos"),
//            @Mapping(source = "TToMyClients", target = "TToMyClients")
    })
    List<TTransactionDto> listToDto(List<TTransaction> TTransactionList);
}