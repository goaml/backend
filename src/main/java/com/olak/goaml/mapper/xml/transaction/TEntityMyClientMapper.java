package com.olak.goaml.mapper.xml.transaction;

import com.olak.goaml.dto.xml.transaction.TEntityMyClientDto;
import com.olak.goaml.dto.xml.transaction.TReportDto;
import com.olak.goaml.models.xml.transaction.TEntityMyClient;
import com.olak.goaml.models.xml.transaction.TReport;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TEntityMyClientMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId"),
            @Mapping(source = "clientTypeId", target = "RClientType.clientTypeId")
    })
    TEntityMyClient toEntity(TEntityMyClientDto TEntityMyClientDto);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "clientTypeId", source = "RClientType.clientTypeId")
    })
    TEntityMyClientDto toDto(TEntityMyClient TEntityMyClient);

    @Mappings({
            @Mapping(target = "statusId", source = "RStatus.statusId"),
            @Mapping(target = "clientTypeId", source = "RClientType.clientTypeId")
    })
    List<TEntityMyClientDto> listToDto(List<TEntityMyClient> entities);
}