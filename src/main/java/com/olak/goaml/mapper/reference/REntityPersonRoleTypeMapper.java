package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.REntityPersonRoleTypeDto;
import com.olak.goaml.models.reference.REntityPersonRoleType;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface REntityPersonRoleTypeMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    REntityPersonRoleType toEntity(REntityPersonRoleTypeDto REntityPersonRoleTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target =  "statusId")
    })
    REntityPersonRoleTypeDto toDto(REntityPersonRoleType REntityPersonRoleType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    REntityPersonRoleType partialUpdate(REntityPersonRoleTypeDto REntityPersonRoleTypeDto, @MappingTarget REntityPersonRoleType REntityPersonRoleType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target =  "statusId")
    })
    List<REntityPersonRoleTypeDto> listToDto(List<REntityPersonRoleType> resultList);
}