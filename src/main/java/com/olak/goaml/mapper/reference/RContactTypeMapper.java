package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RContactTypeDto;
import com.olak.goaml.models.reference.RContactType;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RContactTypeMapper {

    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RContactType toEntity(RContactTypeDto rContactTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RContactTypeDto toDto(RContactType RContactType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RContactType partialUpdate(RContactTypeDto rContactTypeDto, @MappingTarget RContactType rContactType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RContactTypeDto> listToDto(List<RContactType> resultList);
}
