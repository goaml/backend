package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RFundsTypeDto;
import com.olak.goaml.models.reference.RFundsType;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RFundsTypeMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RFundsType toEntity(RFundsTypeDto RFundsTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RFundsTypeDto toDto(RFundsType RFundsType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RFundsType partialUpdate(RFundsTypeDto RFundsTypeDto, @MappingTarget RFundsType RFundsType);

    @Mappings({
            @Mapping(target = "statusId", source = "ldRStatus.statusId")
    })
    List<RFundsTypeDto> listToDto(List<RFundsType> entities);
}