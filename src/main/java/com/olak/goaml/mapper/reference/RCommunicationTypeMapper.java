package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RCommunicationTypeDto;
import com.olak.goaml.models.reference.RCommunicationType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RCommunicationTypeMapper {

    @Mappings({
            @Mapping(target = "RStatus.statusId", source = "statusId")
    })
    RCommunicationType toEntity(RCommunicationTypeDto RCommunicationTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RCommunicationTypeDto toDto(RCommunicationType RCommunicationType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RCommunicationTypeDto> listToDto(List<RCommunicationType> RCommunicationType);
}