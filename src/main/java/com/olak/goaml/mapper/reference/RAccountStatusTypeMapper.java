package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RAccountStatusTypeDto;
import com.olak.goaml.models.reference.RAccountStatusType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RAccountStatusTypeMapper {

    @Mappings({
            @Mapping(target = "RStatus.statusId", source = "statusId")
    })
    RAccountStatusType toEntity(RAccountStatusTypeDto RAccountStatusTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RAccountStatusTypeDto toDto(RAccountStatusType RAccountStatusType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RAccountStatusTypeDto> listToDto(List<RAccountStatusType> RAccountStatusTypeList);


}