package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RCityListDto;
import com.olak.goaml.models.reference.RCityList;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RCityListMapper {

    @Mappings({
            @Mapping(target = "RStatus.statusId", source = "statusId")
    })
    RCityList toEntity(RCityListDto RCityListDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RCityListDto toDto(RCityList RCityList);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RCityListDto> listToDto(List<RCityList> RCityList);

}