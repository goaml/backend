package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RReportIndicatorTypeDto;
import com.olak.goaml.models.reference.RReportIndicatorType;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RReportIndicatorTypeMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RReportIndicatorType toEntity(RReportIndicatorTypeDto RReportIndicatorTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RReportIndicatorTypeDto toDto(RReportIndicatorType RReportIndicatorType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RReportIndicatorType partialUpdate(RReportIndicatorTypeDto RReportIndicatorTypeDto, @MappingTarget RReportIndicatorType RReportIndicatorType);

    @Mappings({
            @Mapping(target = "statusId", source = "ldRStatus.statusId")
    })
    List<RReportIndicatorTypeDto> listToDto(List<RReportIndicatorType> resultList);
}