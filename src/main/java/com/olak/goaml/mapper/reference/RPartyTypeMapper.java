package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RPartyTypeDto;
import com.olak.goaml.models.reference.RPartyType;

import java.util.List;

import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RPartyTypeMapper {

    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RPartyType toEntity(RPartyTypeDto rPartyTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RPartyTypeDto toDto(RPartyType RPartyType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RPartyType partialUpdate(RPartyTypeDto rPartyTypeDto, @MappingTarget RPartyType rPartyType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RPartyTypeDto> listToDto(List<RPartyType> resultList);
}
