package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RAccountTypeDto;
import com.olak.goaml.models.reference.RAccountType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RAccountTypeMapper {

    @Mappings({
            @Mapping(target = "RStatus.statusId", source = "statusId")
    })
    RAccountType toEntity(RAccountTypeDto RAccountTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RAccountTypeDto toDto(RAccountType RAccountType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RAccountTypeDto> listToDto(List<RAccountType> RAccountTypeList);

}