package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RCurrencyDto;
import com.olak.goaml.models.reference.RCurrency;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RCurrencyMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RCurrency toEntity(RCurrencyDto RCurrencyDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RCurrencyDto toDto(RCurrency RCurrency);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RCurrency partialUpdate(RCurrencyDto RCurrencyDto, @MappingTarget RCurrency RCurrency);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RCurrencyDto> listToDto(List<RCurrency> resultList);
}

