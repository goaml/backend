package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RSubmissionTypeDto;
import com.olak.goaml.models.reference.RSubmissionType;

import java.util.List;

import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RSubmissionTypeMapper {

    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RSubmissionType toEntity(RSubmissionTypeDto rSubmissionTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RSubmissionTypeDto toDto(RSubmissionType RSubmissionType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RSubmissionType partialUpdate(RSubmissionTypeDto rSubmissionTypeDto,@MappingTarget RSubmissionType rSubmissionType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RSubmissionTypeDto> listToDto(List<RSubmissionType> resultList);
}