package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.models.reference.RStatus;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RStatusMapper {
    RStatus toEntity(RStatusDto RStatusDto);

    RStatusDto toDto(RStatus RStatus);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RStatus partialUpdate(RStatusDto RStatusDto, @MappingTarget RStatus RStatus);

    List<RStatusDto> listToDto(List<RStatus> resultList);
}