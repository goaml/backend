package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RAccountPersonRoleTypeDto;
import com.olak.goaml.models.reference.RAccountPersonRoleType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RAccountPersonRoleTypeMapper {

    @Mappings({
            @Mapping(target = "RStatus.statusId", source = "statusId")
    })
    RAccountPersonRoleType toEntity(RAccountPersonRoleTypeDto RAccountPersonRoleTypeDto);

    @Mappings({
            @Mapping( source= "RStatus.statusId",  target= "statusId")
    })
    RAccountPersonRoleTypeDto toDto(RAccountPersonRoleType RAccountPersonRoleType);

    @Mappings({
            @Mapping( source= "RStatus.statusId",  target= "statusId")
    })
    List<RAccountPersonRoleTypeDto> listToDto(List<RAccountPersonRoleType> RAccountPersonRoleTypelIST);
}
