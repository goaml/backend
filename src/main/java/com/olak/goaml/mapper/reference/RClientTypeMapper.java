package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RClientTypeDto;
import com.olak.goaml.dto.reference.RCommunicationTypeDto;
import com.olak.goaml.models.reference.RClientType;
import com.olak.goaml.models.reference.RCommunicationType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RClientTypeMapper {
    @Mappings({
            @Mapping(target = "RStatus.statusId", source = "statusId")
    })
    RClientType toEntity(RClientTypeDto rClientTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RClientTypeDto toDto(RClientType rClientType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RClientTypeDto> listToDto(List<RClientType> rClientType);
}
