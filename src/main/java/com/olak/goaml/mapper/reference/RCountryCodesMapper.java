package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RContactTypeDto;
import com.olak.goaml.dto.reference.RCountryCodesDto;
import com.olak.goaml.models.reference.RContactType;
import com.olak.goaml.models.reference.RCountryCodes;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RCountryCodesMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RCountryCodes toEntity(RCountryCodesDto RCountryCodesDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RCountryCodesDto toDto(RCountryCodes RCountryCodes);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RCountryCodes partialUpdate(RCountryCodesDto RCountryCodesDto, @MappingTarget RCountryCodes RCountryCodes);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RCountryCodesDto> listToDto(List<RCountryCodes> resultList);
}