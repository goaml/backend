package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RGenderTypeDto;
import com.olak.goaml.models.reference.RGenderType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RGenderTypeMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RGenderType toEntity(RGenderTypeDto RGenderTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RGenderTypeDto toDto(RGenderType RGenderType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RGenderType partialUpdate(RGenderTypeDto RGenderTypeDto, @MappingTarget RGenderType RGenderType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RGenderTypeDto> listToDto(List<RGenderType> resultList);
}