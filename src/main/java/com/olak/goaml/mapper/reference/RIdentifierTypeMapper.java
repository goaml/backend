package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RIdentifierTypeDto;
import com.olak.goaml.models.reference.RIdentifierType;
import org.mapstruct.*;
import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RIdentifierTypeMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RIdentifierType toEntity(RIdentifierTypeDto RIdentifierTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RIdentifierTypeDto toDto(RIdentifierType RIdentifierType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RIdentifierType partialUpdate(RIdentifierTypeDto RIdentifierTypeDto, @MappingTarget RIdentifierType RIdentifierType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RIdentifierTypeDto> listToDto(List<RIdentifierType> resultList);



}