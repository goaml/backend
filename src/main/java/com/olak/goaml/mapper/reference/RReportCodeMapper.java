package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RFundsTypeDto;
import com.olak.goaml.dto.reference.RReportCodeDto;
import com.olak.goaml.models.reference.RFundsType;
import com.olak.goaml.models.reference.RReportCode;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RReportCodeMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RReportCode toEntity(RReportCodeDto RReportCodeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RReportCodeDto toDto(RReportCode RReportCode);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RReportCode partialUpdate(RReportCodeDto RReportCodeDto, @MappingTarget RReportCode RReportCode);

    @Mappings({
            @Mapping(target = "statusId", source = "ldRStatus.statusId")
    })
    List<RReportCodeDto> listToDto(List<RReportCode> entities);
}