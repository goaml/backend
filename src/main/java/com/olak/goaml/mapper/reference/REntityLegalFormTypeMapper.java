package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.REntityLegalFormTypeDto;
import com.olak.goaml.models.reference.REntityLegalFormType;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface REntityLegalFormTypeMapper {
    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    REntityLegalFormType toEntity(REntityLegalFormTypeDto REntityLegalFormTypeDto);


    @Mappings({
            @Mapping(source = "RStatus.statusId", target =  "statusId")
    })
    REntityLegalFormTypeDto toDto(REntityLegalFormType REntityLegalFormType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    REntityLegalFormType partialUpdate(REntityLegalFormTypeDto REntityLegalFormTypeDto, @MappingTarget REntityLegalFormType REntityLegalFormType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target =  "statusId")
    })
    List<REntityLegalFormTypeDto> listToDto(List<REntityLegalFormType> entities);
}