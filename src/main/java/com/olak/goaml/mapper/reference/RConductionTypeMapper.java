package com.olak.goaml.mapper.reference;

import com.olak.goaml.dto.reference.RConductionTypeDto;
import com.olak.goaml.models.reference.RConductionType;

import java.util.List;

import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RConductionTypeMapper {

    @Mappings({
            @Mapping(source = "statusId", target = "RStatus.statusId")
    })
    RConductionType toEntity(RConductionTypeDto rConductionTypeDto);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    RConductionTypeDto toDto(RConductionType rConductionType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    RConductionType partialUpdate(RConductionTypeDto rConductionTypeDto, @MappingTarget RConductionType rConductionType);

    @Mappings({
            @Mapping(source = "RStatus.statusId", target = "statusId")
    })
    List<RConductionTypeDto> listToDto(List<RConductionType> resultList);
}
