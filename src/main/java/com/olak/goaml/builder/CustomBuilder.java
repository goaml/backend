package com.olak.goaml.builder;


import com.olak.goaml.exception.BusinessRuleException;

/**
 * The type Exception builder.
 *
 * @author sheharaj
 * @Date 6 /7/21
 */
public class CustomBuilder {
    private CustomBuilder() {
    }

    /**
     * Construct business rule exception business rule exception.
     *
     * @param i18nKey the 18 n key
     * @param message the message
     * @return the business rule exception
     */
    public static BusinessRuleException buildBusinessRuleException(String i18nKey, String message) {
        BusinessRuleException brex = new BusinessRuleException();
        brex.setI18nKey(i18nKey);
        brex.setMessage(message);
        return brex;
    }

}
