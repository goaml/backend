package com.olak.goaml.config;

import com.olak.goaml.error.AuthorizationAlertException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Component
@Slf4j
public class JwtInterceptor implements HandlerInterceptor {
    private final JwtUtils jwtUtils;

    public JwtInterceptor(JwtUtils jwtUtils) {
        this.jwtUtils = jwtUtils;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("{}", request.getRequestURI());
        String auth = request.getHeader("authorization");
        try {
            jwtUtils.TokenVerification(auth);
            return HandlerInterceptor.super.preHandle(request, response, handler);
        } catch (Exception e) {
            throw new AuthorizationAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }
}
