package com.olak.goaml.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.olak.goaml.error.AuthorizationAlertException;
import org.springframework.stereotype.Service;

import java.util.Date;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@Service
public class JwtUtils {

    public static final long EXPIRATION_TIME = 86400000;
    private static final String SECRET = "OLAK_secret";
    private static final String ISSUER = "Token Provider";

    public static String generateToken(String username) {
        Date expirationDate = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
        Algorithm algorithm = Algorithm.HMAC256(SECRET);

        String token = JWT.create()
                .withIssuer(ISSUER)
                .withExpiresAt(expirationDate)
                .withSubject(username)
                .sign(algorithm);

        return token;
    }

    public static Boolean validateToken(String token) {
        if (token == null || token.isEmpty()) {
            return false;
        }
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer(ISSUER).build();

            verifier.verify(token);
            return true;
        } catch (JWTVerificationException e) {
            return false;
        }
    }

    public static String getUsernameFromToken(String token) {
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getSubject();
    }

    public static boolean isTokenExpired(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(ISSUER)
                    .build();
            verifier.verify(token);
            return false;
        } catch (JWTVerificationException e) {
            return true;
        }
    }

    public void TokenVerification(String authorizationHeader) {
        try {
            if (authorizationHeader == "" || authorizationHeader == null) {
                throw new AuthorizationAlertException("Authorization token required.", ENTITY_NAME, "error");
            }
            String[] parts = authorizationHeader.split(" ");
            String token;
            try {
                if (parts.length < 2) {
                    token = parts[0];
                } else {
                    token = parts[1];
                }
            } catch (Exception e) {
                throw new AuthorizationAlertException("Invalid Token", ENTITY_NAME, "error");
            }
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer(ISSUER).build();
            verifier.verify(token);
        } catch (Exception e) {
            e.printStackTrace();
            throw new AuthorizationAlertException(e.getMessage(), ENTITY_NAME, "error");
        }
    }

}