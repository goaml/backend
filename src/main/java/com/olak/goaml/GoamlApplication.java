package com.olak.goaml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class GoamlApplication {

	public static void main(String[] args) {

		SpringApplication.run(GoamlApplication.class, args);
	}

}
