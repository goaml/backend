package com.olak.goaml.audit;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.olak.goaml.config.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Component
public class AuditUtils {

    @Autowired
    private Environment env;
    @Autowired
    private JwtUtils jwtUtils;

    public static String GetAuditUserName() {
        String auth = getRequest1().getHeader("authorization");
        if (auth != null) {
            String[] parts = auth.split(" ");
            DecodedJWT jwt = JWT.decode(parts[1]);
            return jwt.getSubject();
        }
        return "System";
    }

    private static HttpServletRequest getRequest1() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }

    private String getToken() {
        String auth = getRequest().getHeader("authorization");
        String[] parts = auth.split(" ");
        return parts[1];
    }

    @Bean
    @Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public AuditUser getAuditUser() {
        return new AuditUser(jwtUtils.getUsernameFromToken(getToken()));
    }

//    private String getAuditUserName(String token) {
//        try {
//            String authorization = token;
//            String[] bearers = authorization.split("Bearer ");
//            String jwtToken = bearers[1];
//            String[] split_string = jwtToken.split("\\.");
//            String base64EncodedBody = split_string[1];
//            Base64 base64Url = new Base64(true);
//            String body = new String(base64Url.decode(base64EncodedBody));
//            JSONObject jsonObject = new JSONObject(body);
//            String user = (String) jsonObject.get("preferred_username");
//            return user;
//        } catch (Exception e) {
//            throw new RuntimeException("cannot determine a user", e);
//        }
//    }

    @Bean
    @Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public AuditUserToken getAuditUserToken() {
        return new AuditUserToken(getToken());
    }

    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }
}
