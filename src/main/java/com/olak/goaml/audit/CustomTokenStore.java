package com.olak.goaml.audit;

public class CustomTokenStore {

    private static String token;

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        CustomTokenStore.token = token;
    }
}
