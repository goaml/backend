package com.olak.goaml.audit;


import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

public class AuditListener {

    @PrePersist
    public void setCreatedOn(AuditModel entity) {
        entity.setCreatedOn(new Date());
        entity.setCreatedBy(AuditUtils.GetAuditUserName()); // You can obtain the current user from your authentication context
    }

    @PreUpdate
    public void setUpdatedOn(AuditModel entity) {
        entity.setUpdatedOn(new Date());
        entity.setUpdatedBy(AuditUtils.GetAuditUserName());
    }

}
