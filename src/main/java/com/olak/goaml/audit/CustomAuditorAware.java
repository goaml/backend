package com.olak.goaml.audit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CustomAuditorAware implements AuditorAware<String> {

    @Autowired
    private AuditUtils auditUtils;

    public Optional<String> getCurrentAuditor() {
        return Optional.of(this.auditUtils.getAuditUser().getUserName());
    }
}
