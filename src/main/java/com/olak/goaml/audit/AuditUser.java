package com.olak.goaml.audit;

public class AuditUser {

    private String userName;

    public AuditUser(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
