package com.olak.goaml.rest.transaction;

import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.miscellaneous.TranRequestDto;
import com.olak.goaml.dto.xml.transaction.*;
import com.olak.goaml.models.xml.transaction.TAccountMyClient;
import com.olak.goaml.service.SaveDailyTranToTableService;
import com.olak.goaml.service.transaction.*;
import com.olak.goaml.userMGT.dto.other.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transaction")
@Slf4j
public class TransactionController {

    private final SaveDailyTranToTableService saveDailyTranToTableService;
    private final TReportModelService reportModelService;
    private final TTransactionModelService transactionModelService;
    private final TFromMyClientModelService tFromMyClientModelService;
    private final TFromModelService tFromModelService;
    private final TToMyClientModelService tToMyClientModelService;
    private final TToModelService tToModelService;
    private final TAccountMyClientModelService tAccountMyClientModelService;
    private final TPersonMyClientModelService tPersonMyClientModelService;
    private final TEntityMyClientModelService tEntityMyClientModelService;
    private final TPartyModelService tPartyModelService;
    private final TAddressModelService tAddressModelService;
    private final TPhoneModelService tPhoneModelService;
    private final TForeignCurrencyModelService tForeignCurrencyModelService;
    private final TPersonIdentificationModelService tPersonIdentificationModelService;

    public TransactionController(SaveDailyTranToTableService saveDailyTranToTableService, TReportModelService reportModelService, TTransactionModelService transactionModelService,
                                 TFromMyClientModelService tFromMyClientModelService, TFromModelService tFromModelService, TToMyClientModelService tToMyClientModelService, TToModelService tToModelService
                , TAccountMyClientModelService tAccountMyClientModelService, TPersonMyClientModelService tPersonMyClientModelService, TEntityMyClientModelService tEntityMyClientModelService,
                                 TPartyModelService tPartyModelService, TAddressModelService tAddressModelService, TPhoneModelService tPhoneModelService, TForeignCurrencyModelService tForeignCurrencyModelService, TPersonIdentificationModelService tPersonIdentificationModelService) {

        this.saveDailyTranToTableService = saveDailyTranToTableService;
        this.reportModelService = reportModelService;
        this.transactionModelService = transactionModelService;
        this.tFromMyClientModelService = tFromMyClientModelService;
        this.tFromModelService = tFromModelService;
        this.tToMyClientModelService = tToMyClientModelService;
        this.tToModelService = tToModelService;
        this.tAccountMyClientModelService = tAccountMyClientModelService;
        this.tPersonMyClientModelService = tPersonMyClientModelService;
        this.tEntityMyClientModelService = tEntityMyClientModelService;
        this.tPartyModelService = tPartyModelService;
        this.tAddressModelService = tAddressModelService;
        this.tPhoneModelService = tPhoneModelService;
        this.tForeignCurrencyModelService = tForeignCurrencyModelService;
        this.tPersonIdentificationModelService = tPersonIdentificationModelService;
    }

    @PostMapping("/save-daily-tran-to-table")
    public ResponseEntity<ResponseDto> saveDailyTranToTable(@RequestBody TranRequestDto tranRequestDto) {
        log.info("Request to save daily transaction to table");
        return ResponseEntity.ok(saveDailyTranToTableService.saveDailyTranToTable(tranRequestDto));
    }

    /**
     * Report Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param reportCode
     * @return
     */

    @GetMapping("/report/tbl")
    public ResponseEntity<ApiResponseDto<List<TReportDto>>> getAllReportDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "reportId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "", required = false) String reportCode
    ) {
        log.info("Inside BaseTransactionController: getAllReportDetails method");
        return reportModelService.getAllReportDetails(page, per_page, search, sort, direction, reportCode);
    }


    /**
     * Transaction Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param rptCode
     * @param trxNo
     * @return
     */

    @GetMapping("/transaction/tbl")
    public ResponseEntity<ApiResponseDto<List<TTransactionDto>>> getAllTransactionDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "transactionId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "", required = false) String rptCode,
            @RequestParam(defaultValue = "", required = false) String trxNo
    ) {
        log.info("Inside BaseTransactionController: getAllTransactionDetails method");
        return transactionModelService.getAllTransactionDetails(page, per_page, search, sort, direction, rptCode, trxNo);
    }


    /**
     * From My Client Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param trxId
     * @return
     */
    @GetMapping("/from-my-client/tbl")
    public ResponseEntity<ApiResponseDto<List<TFromMyClientDto>>> getAllFromMyClientDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "fromMyClientId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer trxId
    ) {
        log.info("Inside BaseTransactionController: getAllFromMyClientDetails method");
        return tFromMyClientModelService.getAllFromMyClientDetails(page, per_page, search, sort, direction, trxId);
    }


    /**
     * From Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param trxId
     * @return
     */
    @GetMapping("/from/tbl")
    public ResponseEntity<ApiResponseDto<List<TFromDto>>> getAllFromDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "fromId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer trxId
    ) {
        log.info("Inside BaseTransactionController: getAllFromDetails method");
        return tFromModelService.getAllFromDetails(page, per_page, search, sort, direction, trxId);
    }

    /**
     *  To My Client Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param trxId
     * @return
     */
    @GetMapping("/to-my-client/tbl")
    public ResponseEntity<ApiResponseDto<List<TToMyClientDto>>> getAllToMyClientDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "toMyClientId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer trxId
    ) {
        log.info("Inside BaseTransactionController: getAllToMyClientDetails method");
        return tToMyClientModelService.getAllToMyClientDetails(page, per_page, search, sort, direction, trxId);
    }

    /**
     *  To Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param trxId
     * @return
     */
    @GetMapping("/to/tbl")
    public ResponseEntity<ApiResponseDto<List<TToDto>>> getAllToDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "toId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer trxId
    ) {
        log.info("Inside BaseTransactionController: getAllToDetails method");
        return tToModelService.getAllToDetails(page, per_page, search, sort, direction, trxId);
    }


    /**
     *  Account My Client Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param accountMyClientId
     * @return
     */
    @GetMapping("/account-my-client/tbl")
    public ResponseEntity<ApiResponseDto<List<TAccountMyClientDto>>> getAllAccountMyClientDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "accountMyClientId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer accountMyClientId
    ) {
        log.info("Inside BaseTransactionController: getAllAccountMyClientDetails method");
        return tAccountMyClientModelService.getAllAccountMyClientDetails(page, per_page, search, sort, direction, accountMyClientId);
    }

    /**
     *  Person My Client Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param personMyClientId
     * @return
     */
    @GetMapping("/person-my-client/tbl")
    public ResponseEntity<ApiResponseDto<List<TPersonMyClientDto>>> getAllPersonMyClientDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "personMyClientId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer personMyClientId
    ) {
        log.info("Inside BaseTransactionController: getAllPersonMyClientDetails method");
        return tPersonMyClientModelService.getAllPersonMyClientDetails(page, per_page, search, sort, direction, personMyClientId);
    }


    /**
     *  Entity My Client Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param entityMyClientId
     * @return
     */
    @GetMapping("/entity-my-client/tbl")
    public ResponseEntity<ApiResponseDto<List<TEntityMyClientDto>>> getAllEntityMyClientDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "entityMyClientId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer entityMyClientId
    ) {
        log.info("Inside BaseTransactionController: getAllEntityMyClientDetails method");
        return tEntityMyClientModelService.getAllEntityMyClientDetails(page, per_page, search, sort, direction, entityMyClientId);
    }

    @GetMapping("/party/tbl")
    public ResponseEntity<ApiResponseDto<List<TPartyDto>>> getAllPartyDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "partyId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "", required = false) String rptCode,
            @RequestParam(defaultValue = "", required = false) String trxNo
    ) {
        log.info("Inside BaseTransactionController: getAllPartyDetails method");
        return tPartyModelService.getAllPartyDetails(page, per_page, search, sort, direction, rptCode, trxNo);
    }


    /**
     *  Address Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param addressId
     * @return
     */
    @GetMapping("/address/tbl")
    public ResponseEntity<ApiResponseDto<List<TAddressDto>>> getAllAddressDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "addressId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer addressId
    ) {
        log.info("Inside BaseTransactionController: getAllAddressDetails method");
        return tAddressModelService.getAllAddressDetails(page, per_page, search, sort, direction, addressId);
    }

    /**
     *  Phone Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param phoneId
     * @return
     */
    @GetMapping("/phone/tbl")
    public ResponseEntity<ApiResponseDto<List<TPhoneDto>>> getAllPhoneDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "phoneId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer phoneId
    ) {
        log.info("Inside BaseTransactionController: getAllPhoneDetails method");
        return tPhoneModelService.getAllPhoneDetails(page, per_page, search, sort, direction, phoneId);
    }

    /**
     *  Foreign Currency Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param foreignCurrencyId
     * @return
     */
    @GetMapping("/foreign-currency/tbl")
    public ResponseEntity<ApiResponseDto<List<TForeignCurrencyDto>>> getAllForeignCurrencyDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "foreignCurrencyId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer foreignCurrencyId
    ) {
        log.info("Inside BaseTransactionController: getAllForeignCurrencyDetails method");
        return tForeignCurrencyModelService.getAllForeignCurrencyDetails(page, per_page, search, sort, direction, foreignCurrencyId);
    }

    /**
     *  Person Identification Details
     * @param page
     * @param per_page
     * @param search
     * @param sort
     * @param direction
     * @param personIdentificationId
     * @return
     */
    @GetMapping("/person-identification/tbl")
    public ResponseEntity<ApiResponseDto<List<TPersonIdentificationDto>>> getAllPersonIdentificationDetails(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "personIdentificationId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction,
            @RequestParam(defaultValue = "0", required = false) Integer personIdentificationId
    ) {
        log.info("Inside BaseTransactionController: getAllPersonIdentificationDetails method");
        return tPersonIdentificationModelService.getAllPersonIdentificationDetails(page, per_page, search, sort, direction, personIdentificationId);
    }

}
