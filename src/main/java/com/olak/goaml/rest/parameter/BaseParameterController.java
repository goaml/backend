package com.olak.goaml.rest.parameter;

import com.olak.goaml.dto.master.*;
import com.olak.goaml.dto.miscellaneous.ApiResponseDto;
import com.olak.goaml.dto.reference.*;

import com.olak.goaml.service.reference.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/base-para")
public class BaseParameterController {

    private final CategoryService categoryService;
    private final FundTypeDescriptionService fundTypeDescriptionService;
    private final FundTypeService fundTypeService;
    private final GoAmlTrxCodeService goAmlTrxCodeService;
    private final MultiPartyFundCodeService multiPartyFundCodeService;
    private final MultiPartyInvolvePartyService multiPartyInvolvePartyService;
    private final MultiPartyRoleService multiPartyRoleService;
    private final ReportTypeService reportTypeService;
    private final RptCodeService rptCodeService;
    private final TransactionCodeService transactionCodeService;
    private final SubmissionTypeService submissionTypeService;
    private final PartyTypeService partyTypeService;
    private final AccountTypeService accountTypeService;
    private final AccountStatusTypeService accountStatusTypeService;
    private final IdentifierTypeService identifierTypeService;
    private final ConductionTypeService conductionTypeService;
    private final ContactTypeService contactTypeService;
    private final CommunicationTypeService communicationTypeService;
    private final CityListService cityListService;
    private final StatusService statusService;
    private final ReportIndicatorTypeService reportIndicatorTypeService;
    private final GenderTypeService genderTypeService;
    private final EntityPersonRoleTypeService entityPersonRoleTypeService;

    private final AccountPersonRoleTypeService accountPersonRoleTypeService;

    private final CurrencyService currencyService;

    private final EntityLegalFormTypeService entityLegalFormTypeService;
    private final RReportCodeService reportCodeService;



    public BaseParameterController(CategoryService categoryService, FundTypeDescriptionService fundTypeDescriptionService, FundTypeService fundTypeService, GoAmlTrxCodeService goAmlTrxCodeService, MultiPartyFundCodeService multiPartyFundCodeService, MultiPartyInvolvePartyService multiPartyInvolvePartyService, MultiPartyRoleService multiPartyRoleService, ReportTypeService reportTypeService, RptCodeService rptCodeService, TransactionCodeService transactionCodeService, SubmissionTypeService submissionTypeService, PartyTypeService partyTypeService, AccountTypeService accountTypeService, AccountStatusTypeService accountStatusTypeService, IdentifierTypeService identifierTypeService, ConductionTypeService conductionTypeService, ContactTypeService contactTypeService, CommunicationTypeService communicationTypeService, CityListService cityListService, StatusService statusService, ReportIndicatorTypeService reportIndicatorTypeService, GenderTypeService genderTypeService, EntityPersonRoleTypeService entityPersonRoleTypeService, AccountPersonRoleTypeService accountPersonRoleTypeService, CurrencyService currencyService, EntityLegalFormTypeService entityLegalFormTypeService, RReportCodeService reportCodeService) {

        this.categoryService = categoryService;
        this.fundTypeDescriptionService = fundTypeDescriptionService;
        this.fundTypeService = fundTypeService;
        this.goAmlTrxCodeService = goAmlTrxCodeService;
        this.multiPartyFundCodeService = multiPartyFundCodeService;
        this.multiPartyInvolvePartyService = multiPartyInvolvePartyService;
        this.multiPartyRoleService = multiPartyRoleService;
        this.reportTypeService = reportTypeService;
        this.rptCodeService = rptCodeService;
        this.transactionCodeService = transactionCodeService;
        this.submissionTypeService = submissionTypeService;
        this.partyTypeService = partyTypeService;
        this.accountTypeService = accountTypeService;
        this.accountStatusTypeService = accountStatusTypeService;
        this.identifierTypeService = identifierTypeService;
        this.conductionTypeService = conductionTypeService;
        this.contactTypeService = contactTypeService;
        this.communicationTypeService = communicationTypeService;
        this.cityListService = cityListService;
        this.statusService = statusService;
        this.reportIndicatorTypeService = reportIndicatorTypeService;
        this.genderTypeService = genderTypeService;
        this.entityPersonRoleTypeService = entityPersonRoleTypeService;
        this.accountPersonRoleTypeService = accountPersonRoleTypeService;
        this.currencyService = currencyService;
        this.entityLegalFormTypeService = entityLegalFormTypeService;
        this.reportCodeService = reportCodeService;
    }


    /**
     * Category Type Controller
     *
     * @return
     */

    @GetMapping("/category/all")
    public ResponseEntity<List<CategoryDto>> getAllCategory() {
        log.info("Inside BaseParameterController: getAllCategory method");
        return categoryService.getAllCategory();
    }

    @GetMapping("/category/tbl")
    public ResponseEntity<ApiResponseDto<List<CategoryDto>>> getCategoryWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "categoryId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getCategoryWithPagination method");
        return categoryService.getCategoryWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/category/add")
    public ResponseEntity<CategoryDto> addCategory(@Valid @RequestBody CategoryDto categoryDto) {
        log.info("Inside BaseParameterController: addCategory method");
        return categoryService.addCategory(categoryDto);
    }

    @GetMapping("/category/get/{id}")
    public ResponseEntity<CategoryDto> getCategoryById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: getCategoryById method");
        return categoryService.getCategoryById(id);
    }

    @PutMapping("/category/update/{id}")
    public ResponseEntity<CategoryDto> updateCategoryById(
            @PathVariable Long id,
            @RequestBody CategoryDto categoryDto
    ) {
        log.info("Inside BaseParameterController: updateCategoryById method");
        return categoryService.updateCategoryById(id, categoryDto);
    }

    @DeleteMapping("/category/delete/{id}")
    public ResponseEntity<CategoryDto> deleteCategoryById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: deleteCategoryById method");
        return categoryService.deleteCategoryById(id);
    }



    /**
     * Fund Type Description Controller
     *
     * @return
     */

    @GetMapping("/fund-type-description/all")
    public ResponseEntity<List<FundTypeDescriptionDto>> getAllFundTypeDescription() {
        log.info("Inside BaseParameterController: getAllFundTypeDescription method");
        return fundTypeDescriptionService.getAllFundTypeDescription();
    }

    @GetMapping("/fund-type-description/tbl")
    public ResponseEntity<ApiResponseDto<List<FundTypeDescriptionDto>>> getFundTypeDescriptionWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "fundTypeDescriptionId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getFundTypeDescriptionWithPagination method");
        return fundTypeDescriptionService.getFundTypeDescriptionWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/fund-type-description/add")
    public ResponseEntity<FundTypeDescriptionDto> addFundTypeDescription(@Valid @RequestBody FundTypeDescriptionDto fundTypeDescriptionDto) {
        log.info("Inside BaseParameterController: addFundTypeDescription method");
        return fundTypeDescriptionService.addFundTypeDescription(fundTypeDescriptionDto);
    }

    @GetMapping("/fund-type-description/get/{id}")
    public ResponseEntity<FundTypeDescriptionDto> getFundTypeDescriptionById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: getFundTypeDescriptionById method");
        return fundTypeDescriptionService.getFundTypeDescriptionById(id);
    }

    @PutMapping("/fund-type-description/update/{id}")
    public ResponseEntity<FundTypeDescriptionDto> updateFundTypeDescriptionById(
            @PathVariable Long id,
            @RequestBody FundTypeDescriptionDto fundTypeDescriptionDto
    ) {
        log.info("Inside BaseParameterController: updateFundTypeDescriptionById method");
        return fundTypeDescriptionService.updateFundTypeDescriptionById(id, fundTypeDescriptionDto);
    }

    @DeleteMapping("/fund-type-description/delete/{id}")
    public ResponseEntity<FundTypeDescriptionDto> deleteFundTypeDescriptionById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: deleteCategoryById method");
        return fundTypeDescriptionService.deleteFundTypeDescriptionById(id);
    }

    /**
     * Multi Party Fund Code Controller
     *
     * @return
     */

    @GetMapping("/multi-party-fund-code/all")
    public ResponseEntity<List<MultiPartyFundCodeDto>> getAllMultiPartyFundCode() {
        log.info("Inside BaseParameterController: getAllMultiPartyFundCode method");
        return multiPartyFundCodeService.getAllMultiPartyFundCode();
    }

    @GetMapping("/multi-party-fund-code/tbl")
    public ResponseEntity<ApiResponseDto<List<MultiPartyFundCodeDto>>> getMultiPartyFundCodeWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "multiPartyFundsCodeId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getMultiPartyFundCodeWithPagination method");
        return multiPartyFundCodeService.getMultiPartyFundCodeWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/multi-party-fund-code/add")
    public ResponseEntity<MultiPartyFundCodeDto> addMultiPartyFundCode(@Valid @RequestBody MultiPartyFundCodeDto multiPartyFundCodeDto) {
        log.info("Inside BaseParameterController: addMultiPartyFundCode method");
        return multiPartyFundCodeService.addMultiPartyFundCode(multiPartyFundCodeDto);
    }

    @GetMapping("/multi-party-fund-code/get/{id}")
    public ResponseEntity<MultiPartyFundCodeDto> getMultiPartyFundCodeById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: getMultiPartyFundCodeById method");
        return multiPartyFundCodeService.getMultiPartyFundCodeById(id);
    }

    @PutMapping("/multi-party-fund-code/update/{id}")
    public ResponseEntity<MultiPartyFundCodeDto> updateMultiPartyFundCodeById(
            @PathVariable Long id,
            @RequestBody MultiPartyFundCodeDto multiPartyFundCodeDto
    ) {
        log.info("Inside BaseParameterController: updateMultiPartyFundCodeById method");
        return multiPartyFundCodeService.updateMultiPartyFundCodeById(id, multiPartyFundCodeDto);
    }

    @DeleteMapping("/multi-party-fund-code/delete/{id}")
    public ResponseEntity<MultiPartyFundCodeDto> deleteMultiPartyFundCodeById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: deleteMultiPartyFundCodeById method");
        return multiPartyFundCodeService.deleteMultiPartyFundCodeById(id);
    }

    /**
     * Multi Party Involve Party Controller
     *
     * @return
     */

    @GetMapping("/multi-party-involve-party/all")
    public ResponseEntity<List<MultiPartyInvolvePartyDto>> getAllMultiPartyInvolveParty() {
        log.info("Inside BaseParameterController: getAllMultiPartyInvolveParty method");
        return multiPartyInvolvePartyService.getAllMultiPartyInvolveParty();
    }

    @GetMapping("/multi-party-involve-party/tbl")
    public ResponseEntity<ApiResponseDto<List<MultiPartyInvolvePartyDto>>> getMultiPartyInvolvePartyWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "multiPartyInvolvePartyId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getMultiPartyInvolvePartyWithPagination method");
        return multiPartyInvolvePartyService.getMultiPartyInvolvePartyWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/multi-party-involve-party/add")
    public ResponseEntity<MultiPartyInvolvePartyDto> addMultiPartyInvolveParty(@Valid @RequestBody MultiPartyInvolvePartyDto multiPartyInvolvePartyDto) {
        log.info("Inside BaseParameterController: addCategory method");
        return multiPartyInvolvePartyService.addMultiPartyInvolveParty(multiPartyInvolvePartyDto);
    }

    @GetMapping("/multi-party-involve-party/get/{id}")
    public ResponseEntity<MultiPartyInvolvePartyDto> getMultiPartyInvolvePartyById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: getMultiPartyInvolvePartyById method");
        return multiPartyInvolvePartyService.getMultiPartyInvolvePartyById(id);
    }

    @PutMapping("/multi-party-involve-party/update/{id}")
    public ResponseEntity<MultiPartyInvolvePartyDto> updateMultiPartyInvolvePartyById(
            @PathVariable Long id,
            @RequestBody MultiPartyInvolvePartyDto multiPartyInvolvePartyDto
    ) {
        log.info("Inside BaseParameterController: updateMultiPartyInvolvePartyById method");
        return multiPartyInvolvePartyService.updateMultiPartyInvolvePartyById(id, multiPartyInvolvePartyDto);
    }

    @DeleteMapping("/multi-party-involve-party/delete/{id}")
    public ResponseEntity<MultiPartyInvolvePartyDto> deleteMultiPartyInvolvePartyById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: deleteCategoryById method");
        return multiPartyInvolvePartyService.deleteMultiPartyInvolvePartyById(id);
    }

    /**
     * Multi Party Role Controller
     *
     * @return
     */

    @GetMapping("/multi-party-role/all")
    public ResponseEntity<List<MultiPartyRoleDto>> getAllMultiPartyRole() {
        log.info("Inside BaseParameterController: getAllMultiPartyRole method");
        return multiPartyRoleService.getAllMultiPartyRole();
    }

    @GetMapping("/multi-party-role/tbl")
    public ResponseEntity<ApiResponseDto<List<MultiPartyRoleDto>>> getMultiPartyRoleWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "multiPartyRoleId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getMultiPartyRoleWithPagination method");
        return multiPartyRoleService.getMultiPartyRoleWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/multi-party-role/add")
    public ResponseEntity<MultiPartyRoleDto> addMultiPartyRole(@Valid @RequestBody MultiPartyRoleDto multiPartyRoleDto) {
        log.info("Inside BaseParameterController: addMultiPartyRole method");
        return multiPartyRoleService.addMultiPartyRole(multiPartyRoleDto);
    }

    @GetMapping("/multi-party-role/get/{id}")
    public ResponseEntity<MultiPartyRoleDto> getMultiPartyRole(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: getMultiPartyRole method");
        return multiPartyRoleService.getMultiPartyRole(id);
    }

    @PutMapping("/multi-party-role/update/{id}")
    public ResponseEntity<MultiPartyRoleDto> updateMultiPartyRoleById(
            @PathVariable Long id,
            @RequestBody MultiPartyRoleDto multiPartyRoleDto
    ) {
        log.info("Inside BaseParameterController: updateMultiPartyRoleById method");
        return multiPartyRoleService.updateMultiPartyRoleById(id, multiPartyRoleDto);
    }

    @DeleteMapping("/multi-party-role/delete/{id}")
    public ResponseEntity<MultiPartyRoleDto> deleteMultiPartyRoleById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: deleteMultiPartyRoleById method");
        return multiPartyRoleService.deleteMultiPartyRoleById(id);
    }

    /**
     * Report Type Controller
     *
     * @return
     */

    @GetMapping("/report-type/all")
    public ResponseEntity<List<ReportTypeDto>> getAllReportType() {
        log.info("Inside BaseParameterController: getAllReportType method");
        return reportTypeService.getAllReportType();
    }

    @GetMapping("/report-type/tbl")
    public ResponseEntity<ApiResponseDto<List<ReportTypeDto>>> getAllReportTypeWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "reportTypeId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getAllReportTypeWithPagination method");
        return reportTypeService.getAllReportTypeWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/report-type/add")
    public ResponseEntity<ReportTypeDto> addReportType(@Valid @RequestBody ReportTypeDto reportTypeDto) {
        log.info("Inside BaseParameterController: addReportType method");
        return reportTypeService.addReportType(reportTypeDto);
    }

    @GetMapping("/report-type/get/{id}")
    public ResponseEntity<ReportTypeDto> getReportTypeById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: getReportTypeById method");
        return reportTypeService.getReportTypeById(id);
    }

    @PutMapping("/report-type/update/{id}")
    public ResponseEntity<ReportTypeDto> updateReportTypeById(
            @PathVariable Long id,
            @RequestBody ReportTypeDto reportTypeDto
    ) {
        log.info("Inside BaseParameterController: updateReportTypeById method");
        return reportTypeService.updateReportTypeById(id, reportTypeDto);
    }

    @DeleteMapping("/report-type/delete/{id}")
    public ResponseEntity<ReportTypeDto> deleteReportTypeById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: deleteReportTypeById method");
        return reportTypeService.deleteReportTypeById(id);
    }

    /**
     * Rpt Code Controller
     *
     * @return
     */

    @GetMapping("/rpt-code/all")
    public ResponseEntity<List<RptCodeDto>> getAllRptCode() {
        log.info("Inside BaseParameterController: getAllRptCode method");
        return rptCodeService.getAllRptCode();
    }

    @GetMapping("/rpt-code/tbl")
    public ResponseEntity<ApiResponseDto<List<RptCodeDto>>> getRptCodeWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "rptCodeId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getRptCodeWithPagination method");
        return rptCodeService.getRptCodeWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/rpt-code/add")
    public ResponseEntity<RptCodeDto> addRptCode(@Valid @RequestBody RptCodeDto rptCodeDto) {
        log.info("Inside BaseParameterController: addRptCode method");
        return rptCodeService.addRptCode(rptCodeDto);
    }

    @GetMapping("/rpt-code/get/{id}")
    public ResponseEntity<RptCodeDto> getRptCodeById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: getRptCodeById method");
        return rptCodeService.getRptCodeById(id);
    }

    @PutMapping("/rpt-code/update/{id}")
    public ResponseEntity<RptCodeDto> updateRptCodeById(
            @PathVariable Long id,
            @RequestBody RptCodeDto rptCodeDto
    ) {
        log.info("Inside BaseParameterController: updateRptCodeById method");
        return rptCodeService.updateRptCodeById(id, rptCodeDto);
    }

    @DeleteMapping("/rpt-code/delete/{id}")
    public ResponseEntity<RptCodeDto> deleteRptCodeById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: deleteRptCodeById method");
        return rptCodeService.deleteRptCodeById(id);
    }

    /**
     * Transaction Code Controller
     *
     * @return
     */

    @GetMapping("/transaction-code/all")
    public ResponseEntity<List<TransactionCodesDto>> getAllTransactionCodes() {
        log.info("Inside BaseParameterController: getAllTransactionCodes method");
        return transactionCodeService.getAllTransactionCodes();
    }

    @GetMapping("/transaction-code/tbl")
    public ResponseEntity<ApiResponseDto<List<TransactionCodesDto>>> getTransactionCodesWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "transactionCodeId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getTransactionCodesWithPagination method");
        return transactionCodeService.getTransactionCodesWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/transaction-code/add")
    public ResponseEntity<TransactionCodesDto> addTransactionCodes(@Valid @RequestBody TransactionCodesDto transactionCodesDto) {
        log.info("Inside BaseParameterController: addTransactionCodes method");
        return transactionCodeService.addTransactionCodes(transactionCodesDto);
    }

    @GetMapping("/transaction-code/get/{id}")
    public ResponseEntity<TransactionCodesDto> getTransactionCodesById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: getTransactionCodesById method");
        return transactionCodeService.getTransactionCodesById(id);
    }

    @PutMapping("/transaction-code/update/{id}")
    public ResponseEntity<TransactionCodesDto> updateTransactionCodesById(
            @PathVariable Long id,
            @RequestBody TransactionCodesDto transactionCodesDto
    ) {
        log.info("Inside BaseParameterController: updateTransactionCodesById method");
        return transactionCodeService.updateTransactionCodesById(id, transactionCodesDto);
    }

    @DeleteMapping("/transaction-code/delete/{id}")
    public ResponseEntity<TransactionCodesDto> deleteTransactionCodesById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: deleteTransactionCodesById method");
        return transactionCodeService.deleteTransactionCodesById(id);
    }

    /**
     * Go Aml Trx Code Controller
     *
     * @return
     */

    @GetMapping("/go-aml-trx-code/all")
    public ResponseEntity<List<GoAmlTrxCodesDto>> getAllGoAmlTrxCodes() {
        log.info("Inside BaseParameterController: getAllGoAmlTrxCodes method");
        return goAmlTrxCodeService.getAllGoAmlTrxCodes();
    }

    @GetMapping("/go-aml-trx-code/tbl")
    public ResponseEntity<ApiResponseDto<List<GoAmlTrxCodesDto>>> getGoAmlTrxCodesWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "goamlTrxCodeId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getGoAmlTrxCodesWithPagination method");
        return goAmlTrxCodeService.getGoAmlTrxCodesWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/go-aml-trx-code/add")
    public ResponseEntity<GoAmlTrxCodesDto> addGoAmlTrxCodes(@Valid @RequestBody GoAmlTrxCodesDto goAmlTrxCodesDto) {
        log.info("Inside BaseParameterController: addGoAmlTrxCodes method");
        return goAmlTrxCodeService.addGoAmlTrxCodes(goAmlTrxCodesDto);
    }

    @GetMapping("/go-aml-trx-code/get/{id}")
    public ResponseEntity<GoAmlTrxCodesDto> getGoAmlTrxCodesById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: getGoAmlTrxCodesById method");
        return goAmlTrxCodeService.getGoAmlTrxCodesById(id);
    }

    @PutMapping("/go-aml-trx-code/update/{id}")
    public ResponseEntity<GoAmlTrxCodesDto> updateGoAmlTrxCodesById(
            @PathVariable Long id,
            @RequestBody GoAmlTrxCodesDto goAmlTrxCodesDto
    ) {
        log.info("Inside BaseParameterController: updateGoAmlTrxCodesById method");
        return goAmlTrxCodeService.updateGoAmlTrxCodesById(id, goAmlTrxCodesDto);
    }

    @DeleteMapping("/go-aml-trx-code/delete/{id}")
    public ResponseEntity<GoAmlTrxCodesDto> deleteGoAmlTrxCodesById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: deleteGoAmlTrxCodesById method");
        return goAmlTrxCodeService.deleteGoAmlTrxCodesById(id);
    }


    /**
     * submission type Controller
     *
     * @return
     */



    @GetMapping("/submission-type")
    public ResponseEntity<List<RSubmissionTypeDto>> getAllSubmissionTypes() {
        log.info("Request to get all submission types");
        return submissionTypeService.getAllSubmissionTypes();
    }

    @GetMapping("/submission-type/pagination")
    public ResponseEntity<ApiResponseDto<List<RSubmissionTypeDto>>> getSubmissionTypesWithPagination(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer perPage,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "submissionTypeId") String sort,
            @RequestParam(defaultValue = "asc") String direction) {
        log.info("Request to get submission types with pagination");
        return submissionTypeService.getSubmissionTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/submission-type")
    public ResponseEntity<RSubmissionTypeDto> addSubmissionType(@Valid @RequestBody RSubmissionTypeDto submissionTypeDto) {
        log.info("Request to add new submission type");
        return submissionTypeService.addSubmissionType(submissionTypeDto);
    }

    @GetMapping("/submission-type/{id}")
    public ResponseEntity<RSubmissionTypeDto> getSubmissionTypeById(@PathVariable Integer id) {
        log.info("Request to get submission type by id: {}", id);
        return submissionTypeService.getSubmissionTypeById(id);
    }

    @PutMapping("/submission-type/{id}")
    public ResponseEntity<RSubmissionTypeDto> updateSubmissionTypeById(@PathVariable Integer id, @RequestBody RSubmissionTypeDto submissionTypeDto) {
        log.info("Request to update submission type by id: {}", id);
        return submissionTypeService.updateSubmissionTypeById(id, submissionTypeDto);
    }

    @DeleteMapping("/submission-type/{id}")
    public ResponseEntity<RSubmissionTypeDto> deleteSubmissionTypeById(@PathVariable Integer id) {
        log.info("Request to delete submission type by id: {}", id);
        return submissionTypeService.deleteSubmissionTypeById(id);
    }

    /**
     * party type Controller
     *
     * @return
     */

      @GetMapping("/party-type")
    public ResponseEntity<List<RPartyTypeDto>> getAllPartyTypes() {
        log.info("Request to get all party types");
        return partyTypeService.getAllPartyTypes();
    }

    @GetMapping("/party-type/tbl")
    public ResponseEntity<ApiResponseDto<List<RPartyTypeDto>>> getPartyTypesWithPagination(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer perPage,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "partyTypeId") String sort,
            @RequestParam(defaultValue = "asc") String direction) {
        log.info("Request to get party types with pagination");
        return partyTypeService.getPartyTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/party-type")
    public ResponseEntity<RPartyTypeDto> addPartyType(@Valid @RequestBody RPartyTypeDto partyTypeDto) {
        log.info("Request to add new party type");
        return partyTypeService.addPartyType(partyTypeDto);
    }

    @GetMapping("/party-type/{id}")
    public ResponseEntity<RPartyTypeDto> getPartyTypeById(@PathVariable Integer id) {
        log.info("Request to get party type by id: {}", id);
        return partyTypeService.getPartyTypeById(id);
    }

    @PutMapping("/party-type/{id}")
    public ResponseEntity<RPartyTypeDto> updatePartyTypeById(@PathVariable Integer id, @RequestBody RPartyTypeDto partyTypeDto) {
        log.info("Request to update party type by id: {}", id);
        return partyTypeService.updatePartyTypeById(id, partyTypeDto);
    }

    @DeleteMapping("/party-type/{id}")
    public ResponseEntity<RPartyTypeDto> deletePartyTypeById(@PathVariable Integer id) {
        log.info("Request to delete party type by id: {}", id);
        return partyTypeService.deletePartyTypeById(id);
    }

    /**
     * account type Controller
     *
     * @return
     */

    @GetMapping("/account-type")
    public ResponseEntity<List<RAccountTypeDto>> getAllAccountTypes() {
        log.info("Request to get all account types");
        return accountTypeService.getAllAccountTypes();
    }

    @GetMapping("/account-type/tbl")
    public ResponseEntity<ApiResponseDto<List<RAccountTypeDto>>> getAccountTypesWithPagination(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer perPage,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "accountTypeId") String sort,
            @RequestParam(defaultValue = "asc") String direction) {
        log.info("Request to get account types with pagination");
        return accountTypeService.getAccountTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/account-type")
    public ResponseEntity<RAccountTypeDto> addAccountType(@Valid @RequestBody RAccountTypeDto accountTypeDto) {
        log.info("Request to add new account type");
        return accountTypeService.addAccountType(accountTypeDto);
    }

    @GetMapping("/account-type/{id}")
    public ResponseEntity<RAccountTypeDto> getAccountTypeById(@PathVariable Integer id) {
        log.info("Request to get account type by id: {}", id);
        return accountTypeService.getAccountTypeById(id);
    }

    @PutMapping("/account-type/{id}")
    public ResponseEntity<RAccountTypeDto> updateAccountTypeById(@PathVariable Integer id, @RequestBody RAccountTypeDto accountTypeDto) {
        log.info("Request to update account type by id: {}", id);
        return accountTypeService.updateAccountTypeById(id, accountTypeDto);
    }

    @DeleteMapping("/account-type/{id}")
    public ResponseEntity<RAccountTypeDto> deleteAccountTypeById(@PathVariable Integer id) {
        log.info("Request to delete account type by id: {}", id);
        return accountTypeService.deleteAccountTypeById(id);
    }

    /**
     * account status type Controller
     *
     * @return
     */

    @GetMapping("/account-status-type")
    public ResponseEntity<List<RAccountStatusTypeDto>> getAllAccountStatusTypes() {
        log.info("Request to get all account status types");
        return accountStatusTypeService.getAllAccountStatusTypes();
    }

    @GetMapping("/account-status-type/tbl")
    public ResponseEntity<ApiResponseDto<List<RAccountStatusTypeDto>>> getAccountStatusTypesWithPagination(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer perPage,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "accountStatusTypeId") String sort,
            @RequestParam(defaultValue = "asc") String direction) {
        log.info("Request to get account status types with pagination");
        return accountStatusTypeService.getAccountStatusTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/account-status-type")
    public ResponseEntity<RAccountStatusTypeDto> addAccountStatusType(@Valid @RequestBody RAccountStatusTypeDto accountStatusTypeDto) {
        log.info("Request to add new account status type");
        return accountStatusTypeService.addAccountStatusType(accountStatusTypeDto);
    }

    @GetMapping("/account-status-type/{id}")
    public ResponseEntity<RAccountStatusTypeDto> getAccountStatusTypeById(@PathVariable Integer id) {
        log.info("Request to get account status type by id: {}", id);
        return accountStatusTypeService.getAccountStatusTypeById(id);
    }

    @PutMapping("/account-status-type/{id}")
    public ResponseEntity<RAccountStatusTypeDto> updateAccountStatusTypeById(@PathVariable Integer id, @RequestBody RAccountStatusTypeDto accountStatusTypeDto) {
        log.info("Request to update account status type by id: {}", id);
        return accountStatusTypeService.updateAccountStatusTypeById(id, accountStatusTypeDto);
    }

    @DeleteMapping("/account-status-type/{id}")
    public ResponseEntity<RAccountStatusTypeDto> deleteAccountStatusTypeById(@PathVariable Integer id) {
        log.info("Request to delete account status type by id: {}", id);
        return accountStatusTypeService.deleteAccountStatusTypeById(id);
    }

    /**contact-type
     * identifier type Controller
     *
     * @return
     */

    @GetMapping("/identifier-type")
    public ResponseEntity<List<RIdentifierTypeDto>> getAllIdentifierTypes() {
        log.info("REST request to get all Identifier Types");
        return identifierTypeService.getAllIdentifierTypes();
    }

    @GetMapping("/identifier-type/tbl")
    public ResponseEntity<ApiResponseDto<List<RIdentifierTypeDto>>> getIdentifierTypesWithPagination(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "perPage", defaultValue = "10") Integer perPage,
            @RequestParam(value = "search", defaultValue = "") String search,
            @RequestParam(value = "sort", defaultValue = "identifierTypeId") String sort,
            @RequestParam(value = "direction", defaultValue = "asc") String direction) {
        log.info("REST request to get Identifier Types with pagination");
        return identifierTypeService.getIdentifierTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/identifier-type")
    public ResponseEntity<RIdentifierTypeDto> addIdentifierType(@RequestBody RIdentifierTypeDto identifierTypeDto) {
        log.info("REST request to add a new Identifier Type");
        return identifierTypeService.addIdentifierType(identifierTypeDto);
    }

    @GetMapping("/identifier-type/{id}")
    public ResponseEntity<RIdentifierTypeDto> getIdentifierTypeById(@PathVariable Integer id) {
        log.info("REST request to get Identifier Type by id: {}", id);
        return identifierTypeService.getIdentifierTypeById(id);
    }

    @PutMapping("/identifier-type/{id}")
    public ResponseEntity<RIdentifierTypeDto> updateIdentifierTypeById(@PathVariable Integer id, @RequestBody RIdentifierTypeDto identifierTypeDto) {
        log.info("REST request to update Identifier Type by id: {}", id);
        return identifierTypeService.updateIdentifierTypeById(id, identifierTypeDto);
    }

    @DeleteMapping("/identifier-type/{id}")
    public ResponseEntity<RIdentifierTypeDto> deleteIdentifierTypeById(@PathVariable Integer id) {
        log.info("REST request to delete Identifier Type by id: {}", id);
        return identifierTypeService.deleteIdentifierTypeById(id);
    }

    /**
     * conduction type Controller
     *
     * @return
     */




    @GetMapping("/conduction-type")
    public ResponseEntity<List<RConductionTypeDto>> getAllConductionTypes() {
        log.info("REST request to get all Conduction Types");
        return conductionTypeService.getAllConductionTypes();
    }

    @GetMapping("/conduction-type/tbl")
    public ResponseEntity<ApiResponseDto<List<RConductionTypeDto>>> getConductionTypesWithPagination(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "perPage", defaultValue = "10") Integer perPage,
            @RequestParam(value = "search", defaultValue = "") String search,
            @RequestParam(value = "sort", defaultValue = "conductionTypeId") String sort,
            @RequestParam(value = "direction", defaultValue = "asc") String direction) {
        log.info("REST request to get Conduction Types with pagination");
        return conductionTypeService.getConductionTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/conduction-type")
    public ResponseEntity<RConductionTypeDto> addConductionType(@Valid @RequestBody RConductionTypeDto conductionTypeDto) {
        log.info("REST request to add a new Conduction Type");
        return conductionTypeService.addConductionType(conductionTypeDto);
    }

    @GetMapping("/conduction-type/{id}")
    public ResponseEntity<RConductionTypeDto> getConductionTypeById(@PathVariable Integer id) {
        log.info("REST request to get Conduction Type by id: {}", id);
        return conductionTypeService.getConductionTypeById(id);
    }

    @PutMapping("/conduction-type/{id}")
    public ResponseEntity<RConductionTypeDto> updateConductionTypeById(@PathVariable Integer id, @Valid @RequestBody RConductionTypeDto conductionTypeDto) {
        log.info("REST request to update Conduction Type by id: {}", id);
        return conductionTypeService.updateConductionTypeById(id, conductionTypeDto);
    }

    @DeleteMapping("/conduction-type/{id}")
    public ResponseEntity<RConductionTypeDto> deleteConductionTypeById(@PathVariable Integer id) {
        log.info("REST request to delete Conduction Type by id: {}", id);
        return conductionTypeService.deleteConductionTypeById(id);
    }

    /**
     * contact type Controller
     *
     * @return
     */


    @GetMapping("/contact-type")
    public ResponseEntity<List<RContactTypeDto>> getAllContactTypes() {
        log.info("Request to get all contact types");
        return contactTypeService.getAllContactTypes();
    }

    @GetMapping("/contact-type/tbl")
    public ResponseEntity<ApiResponseDto<List<RContactTypeDto>>> getContactTypesWithPagination(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer perPage,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "contactTypeId") String sort,
            @RequestParam(defaultValue = "asc") String direction) {
        log.info("Request to get contact types with pagination");
        return contactTypeService.getContactTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/contact-type")
    public ResponseEntity<RContactTypeDto> addContactType(@Valid @RequestBody RContactTypeDto contactTypeDto) {
        log.info("Request to add new contact type");
        return contactTypeService.addContactType(contactTypeDto);
    }

    @GetMapping("/contact-type/{id}")
    public ResponseEntity<RContactTypeDto> getContactTypeById(@PathVariable Integer id) {
        log.info("Request to get contact type by id: {}", id);
        return contactTypeService.getContactTypeById(id);
    }

    @PutMapping("/contact-type/{id}")
    public ResponseEntity<RContactTypeDto> updateContactTypeById(@PathVariable Integer id, @RequestBody RContactTypeDto contactTypeDto) {
        log.info("Request to update contact type by id: {}", id);
        return contactTypeService.updateContactTypeById(id, contactTypeDto);
    }

    @DeleteMapping("/contact-type/{id}")
    public ResponseEntity<RContactTypeDto> deleteContactTypeById(@PathVariable Integer id) {
        log.info("Request to delete contact type by id: {}", id);
        return contactTypeService.deleteContactTypeById(id);
    }


    /**
     * communication type Controller
     *
     * @return
     */

    @GetMapping("/communication-type")
    public ResponseEntity<List<RCommunicationTypeDto>> getAllCommunicationTypes() {
        log.info("Request to get all communication types");
        return communicationTypeService.getAllCommunicationTypes();
    }

    @GetMapping("/communication-type/tbl")
    public ResponseEntity<ApiResponseDto<List<RCommunicationTypeDto>>> getCommunicationTypesWithPagination(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer perPage,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "communicationTypeId") String sort,
            @RequestParam(defaultValue = "asc") String direction) {
        log.info("Request to get communication types with pagination");
        return communicationTypeService.getCommunicationTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/communication-type")
    public ResponseEntity<RCommunicationTypeDto> addCommunicationType(@Valid @RequestBody RCommunicationTypeDto communicationTypeDto) {
        log.info("Request to add new communication type");
        return communicationTypeService.addCommunicationType(communicationTypeDto);
    }

    @GetMapping("/communication-type/{id}")
    public ResponseEntity<RCommunicationTypeDto> getCommunicationTypeById(@PathVariable Integer id) {
        log.info("Request to get communication type by id: {}", id);
        return communicationTypeService.getCommunicationTypeById(id);
    }

    @PutMapping("/communication-type/{id}")
    public ResponseEntity<RCommunicationTypeDto> updateCommunicationTypeById(@PathVariable Integer id, @RequestBody RCommunicationTypeDto communicationTypeDto) {
        log.info("Request to update communication type by id: {}", id);
        return communicationTypeService.updateCommunicationTypeById(id, communicationTypeDto);
    }

    @DeleteMapping("/communication-type/{id}")
    public ResponseEntity<RCommunicationTypeDto> deleteCommunicationTypeById(@PathVariable Integer id) {
        log.info("Request to delete communication type by id: {}", id);
        return communicationTypeService.deleteCommunicationTypeById(id);
    }

    /**
     * citylist  Controller
     *
     * @return
     */

    @GetMapping("/city-list")
    public ResponseEntity<List<RCityListDto>> getAllCityLists() {
        log.info("REST request to get all City Lists");
        return cityListService.getAllCityLists();
    }

    @GetMapping("/city-list/tbl")
    public ResponseEntity<ApiResponseDto<List<RCityListDto>>> getCityListsWithPagination(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "perPage", defaultValue = "10") Integer perPage,
            @RequestParam(value = "search", defaultValue = "") String search,
            @RequestParam(value = "sort", defaultValue = "cityId") String sort,
            @RequestParam(value = "direction", defaultValue = "asc") String direction) {
        log.info("REST request to get City Lists with pagination");
        return cityListService.getCityListsWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/city-list")
    public ResponseEntity<RCityListDto> addCityList(@Valid @RequestBody RCityListDto cityListDto) {
        log.info("REST request to add a new City List");
        return cityListService.addCityList(cityListDto);
    }

    @GetMapping("/city-list/{id}")
    public ResponseEntity<RCityListDto> getCityListById(@PathVariable Integer id) {
        log.info("REST request to get City List by id: {}", id);
        return cityListService.getCityListById(id);
    }

    @PutMapping("/city-list/{id}")
    public ResponseEntity<RCityListDto> updateCityListById(@PathVariable Integer id, @Valid @RequestBody RCityListDto cityListDto) {
        log.info("REST request to update City List by id: {}", id);
        return cityListService.updateCityListById(id, cityListDto);
    }

    @DeleteMapping("/city-list/{id}")
    public ResponseEntity<RCityListDto> deleteCityListById(@PathVariable Integer id) {
        log.info("REST request to delete City List by id: {}", id);
        return cityListService.deleteCityListById(id);
    }


    /**
     * status  Controller
     *
     * @return
     */


    @GetMapping("/status")
    public ResponseEntity<List<RStatusDto>> getAllStatuses() {
        log.info("REST request to get all Statuses");
        return statusService.getAllStatuses();
    }

    @GetMapping("/status/tbl")
    public ResponseEntity<ApiResponseDto<List<RStatusDto>>> getStatusesWithPagination(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "perPage", defaultValue = "10") Integer perPage,
            @RequestParam(value = "search", defaultValue = "") String search,
            @RequestParam(value = "sort", defaultValue = "statusId") String sort,
            @RequestParam(value = "direction", defaultValue = "asc") String direction) {
        log.info("REST request to get Statuses with pagination");
        return statusService.getStatusesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/status")
    public ResponseEntity<RStatusDto> addStatus(@Valid @RequestBody RStatusDto statusDto) {
        log.info("REST request to add a new Status");
        return statusService.addStatus(statusDto);
    }

    @GetMapping("/status/{id}")
    public ResponseEntity<RStatusDto> getStatusById(@PathVariable Integer id) {
        log.info("REST request to get Status by id: {}", id);
        return statusService.getStatusById(id);
    }

    @PutMapping("/status/{id}")
    public ResponseEntity<RStatusDto> updateStatusById(@PathVariable Integer id, @Valid @RequestBody RStatusDto statusDto) {
        log.info("REST request to update Status by id: {}", id);
        return statusService.updateStatusById(id, statusDto);
    }

    @DeleteMapping("/status/{id}")
    public ResponseEntity<RStatusDto> deleteStatusById(@PathVariable Integer id) {
        log.info("REST request to delete Status by id: {}", id);
        return statusService.deleteStatusById(id);
    }


    /**
     * Report Indicator  Controller
     *
     * @return
     */




    @GetMapping("/report-indicator-type")
    public ResponseEntity<List<RReportIndicatorTypeDto>> getAllReportIndicatorTypes() {
        log.info("Request to get all report indicator types");
        return reportIndicatorTypeService.getAllReportIndicatorTypes();
    }

    @GetMapping("/report-indicator-type/tbl")
    public ResponseEntity<ApiResponseDto<List<RReportIndicatorTypeDto>>> getReportIndicatorTypesWithPagination(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer perPage,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "reportIndicatorTypeId") String sort,
            @RequestParam(defaultValue = "asc") String direction) {
        log.info("Request to get report indicator types with pagination");
        return reportIndicatorTypeService.getReportIndicatorTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/report-indicator-type")
    public ResponseEntity<RReportIndicatorTypeDto> addReportIndicatorType(@Valid @RequestBody RReportIndicatorTypeDto reportIndicatorTypeDto) {
        log.info("Request to add new report indicator type");
        return reportIndicatorTypeService.addReportIndicatorType(reportIndicatorTypeDto);
    }

    @GetMapping("/report-indicator-type/{id}")
    public ResponseEntity<RReportIndicatorTypeDto> getReportIndicatorTypeById(@PathVariable Integer id) {
        log.info("Request to get report indicator type by id: {}", id);
        return reportIndicatorTypeService.getReportIndicatorTypeById(id);
    }

    @PutMapping("/report-indicator-type/{id}")
    public ResponseEntity<RReportIndicatorTypeDto> updateReportIndicatorTypeById(@PathVariable Integer id, @RequestBody RReportIndicatorTypeDto reportIndicatorTypeDto) {
        log.info("Request to update report indicator type by id: {}", id);
        return reportIndicatorTypeService.updateReportIndicatorTypeById(id, reportIndicatorTypeDto);
    }

    @DeleteMapping("/report-indicator-type/{id}")
    public ResponseEntity<RReportIndicatorTypeDto> deleteReportIndicatorTypeById(@PathVariable Integer id) {
        log.info("Request to delete report indicator type by id: {}", id);
        return reportIndicatorTypeService.deleteReportIndicatorTypeById(id);
    }


    /**
     * Gender type  Controller
     *
     * @return
     */

    @GetMapping("/gender-type")
    public ResponseEntity<List<RGenderTypeDto>> getAllGenderTypes() {
        log.info("Request to get all gender types");
        return genderTypeService.getAllGenderTypes();
    }

    @GetMapping("/gender-type/tbl")
    public ResponseEntity<ApiResponseDto<List<RGenderTypeDto>>> getGenderTypesWithPagination(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer perPage,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "genderTypeId") String sort,
            @RequestParam(defaultValue = "asc") String direction) {
        log.info("Request to get gender types with pagination");
        return genderTypeService.getGenderTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/gender-type")
    public ResponseEntity<RGenderTypeDto> addGenderType(@Valid @RequestBody RGenderTypeDto genderTypeDto) {
        log.info("Request to add new gender type");
        return genderTypeService.addGenderType(genderTypeDto);
    }

    @GetMapping("/gender-type/{id}")
    public ResponseEntity<RGenderTypeDto> getGenderTypeById(@PathVariable Integer id) {
        log.info("Request to get gender type by id: {}", id);
        return genderTypeService.getGenderTypeById(id);
    }

    @PutMapping("/gender-type/{id}")
    public ResponseEntity<RGenderTypeDto> updateGenderTypeById(@PathVariable Integer id, @RequestBody RGenderTypeDto genderTypeDto) {
        log.info("Request to update gender type by id: {}", id);
        return genderTypeService.updateGenderTypeById(id, genderTypeDto);
    }

    @DeleteMapping("/gender-type/{id}")
    public ResponseEntity<RGenderTypeDto> deleteGenderTypeById(@PathVariable Integer id) {
        log.info("Request to delete gender type by id: {}", id);
        return genderTypeService.deleteGenderTypeById(id);



    }


    /**
     * Person Role  Controller
     *
     * @return
     */




    @GetMapping("/entity-person-role-types")
    public ResponseEntity<List<REntityPersonRoleTypeDto>> getAllEntityPersonRoleTypes() {
        log.info("Request to get all entity person role types");
        return entityPersonRoleTypeService.getAllEntityPersonRoleTypes();
    }

    @GetMapping("/entity-person-role-types/tbl")
    public ResponseEntity<ApiResponseDto<List<REntityPersonRoleTypeDto>>> getEntityPersonRoleTypesWithPagination(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer perPage,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "entityPersonRoleTypeId") String sort,
            @RequestParam(defaultValue = "asc") String direction) {
        log.info("Request to get entity person role types with pagination");
        return entityPersonRoleTypeService.getEntityPersonRoleTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/entity-person-role-types")
    public ResponseEntity<REntityPersonRoleTypeDto> addEntityPersonRoleType(@Valid @RequestBody REntityPersonRoleTypeDto entityPersonRoleTypeDto) {
        log.info("Request to add new entity person role type");
        return entityPersonRoleTypeService.addEntityPersonRoleType(entityPersonRoleTypeDto);
    }

    @GetMapping("/entity-person-role-types/{id}")
    public ResponseEntity<REntityPersonRoleTypeDto> getEntityPersonRoleTypeById(@PathVariable Integer id) {
        log.info("Request to get entity person role type by id: {}", id);
        return entityPersonRoleTypeService.getEntityPersonRoleTypeById(id);
    }

    @PutMapping("/entity-person-role-types/{id}")
    public ResponseEntity<REntityPersonRoleTypeDto> updateEntityPersonRoleTypeById(@PathVariable Integer id, @Valid @RequestBody REntityPersonRoleTypeDto entityPersonRoleTypeDto) {
        log.info("Request to update entity person role type by id: {}", id);
        return entityPersonRoleTypeService.updateEntityPersonRoleTypeById(id, entityPersonRoleTypeDto);
    }

    @DeleteMapping("/entity-person-role-types/{id}")
    public ResponseEntity<REntityPersonRoleTypeDto> deleteEntityPersonRoleTypeById(@PathVariable Integer id) {
        log.info("Request to delete entity person role type by id: {}", id);
        return entityPersonRoleTypeService.deleteEntityPersonRoleTypeById(id);
    }


    /**
     * AccountPerson Role  Controller
     *
     * @return
     */


    @GetMapping("/account-person-role-types")
    public ResponseEntity<List<RAccountPersonRoleTypeDto>> getAllAccountPersonRoleTypes() {
        log.info("Request to get all account person role types");
        return accountPersonRoleTypeService.getAllAccountPersonRoleTypes();
    }

    @GetMapping("/account-person-role-types/tbl")
    public ResponseEntity<ApiResponseDto<List<RAccountPersonRoleTypeDto>>> getAccountPersonRoleTypesWithPagination(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer perPage,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "accountPersonRoleTypeId") String sort,
            @RequestParam(defaultValue = "asc") String direction) {
        log.info("Request to get account person role types with pagination");
        return accountPersonRoleTypeService.getAccountPersonRoleTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/account-person-role-types")
    public ResponseEntity<RAccountPersonRoleTypeDto> addAccountPersonRoleType(@Valid @RequestBody RAccountPersonRoleTypeDto accountPersonRoleTypeDto) {
        log.info("Request to add new account person role type");
        return accountPersonRoleTypeService.addAccountPersonRoleType(accountPersonRoleTypeDto);
    }

    @GetMapping("/account-person-role-types/{id}")
    public ResponseEntity<RAccountPersonRoleTypeDto> getAccountPersonRoleTypeById(@PathVariable Integer id) {
        log.info("Request to get account person role type by id: {}", id);
        return accountPersonRoleTypeService.getAccountPersonRoleTypeById(id);
    }

    @PutMapping("/account-person-role-types/{id}")
    public ResponseEntity<RAccountPersonRoleTypeDto> updateAccountPersonRoleTypeById(@PathVariable Integer id, @Valid @RequestBody RAccountPersonRoleTypeDto accountPersonRoleTypeDto) {
        log.info("Request to update account person role type by id: {}", id);
        return accountPersonRoleTypeService.updateAccountPersonRoleTypeById(id, accountPersonRoleTypeDto);
    }

    @DeleteMapping("/account-person-role-types/{id}")
    public ResponseEntity<RAccountPersonRoleTypeDto> deleteAccountPersonRoleTypeById(@PathVariable Integer id) {
        log.info("Request to delete account person role type by id: {}", id);
        return accountPersonRoleTypeService.deleteAccountPersonRoleTypeById(id);
    }


    /**
     * Currency  Controller
     *
     * @return
     */



    @GetMapping("/currency")
    public ResponseEntity<List<RCurrencyDto>> getAllCurrencies() {
        log.info("Inside CurrencyController: getAllCurrencies method");
        return currencyService.getAllCurrencies();
    }

    @GetMapping("/currency/tbl")
    public ResponseEntity<ApiResponseDto<List<RCurrencyDto>>> getCurrenciesWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer perPage,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "currenciesId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction) {
        log.info("Inside CurrencyController: getCurrenciesWithPagination method");
        return currencyService.getCurrenciesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/currency")
    public ResponseEntity<RCurrencyDto> addCurrency(@Valid @RequestBody RCurrencyDto currencyDto) {
        log.info("Inside CurrencyController: addCurrency method");
        return currencyService.addCurrency(currencyDto);
    }

    @GetMapping("/currency/{id}")
    public ResponseEntity<RCurrencyDto> getCurrencyById(@PathVariable Integer id) {
        log.info("Inside CurrencyController: getCurrencyById method");
        return currencyService.getCurrencyById(id);
    }

    @PutMapping("/currency/{id}")
    public ResponseEntity<RCurrencyDto> updateCurrencyById(@PathVariable Integer id, @Valid @RequestBody RCurrencyDto currencyDto) {
        log.info("Inside CurrencyController: updateCurrencyById method");
        return currencyService.updateCurrencyById(id, currencyDto);
    }

    @DeleteMapping("/currency/{id}")
    public ResponseEntity<RCurrencyDto> deleteCurrencyById(@PathVariable Integer id) {
        log.info("Inside CurrencyController: deleteCurrencyById method");
        return currencyService.deleteCurrencyById(id);
    }

    /**
     * Entity Legal Form Type  Controller
     *
     * @return
     */


    @GetMapping("/entity-legal-form-type")
    public ResponseEntity<List<REntityLegalFormTypeDto>> getAllEntityLegalFormTypes() {
        log.info("Inside EntityLegalFormTypeController: getAllEntityLegalFormTypes method");
        return entityLegalFormTypeService.getAllEntityLegalFormTypes();
    }

    @GetMapping("/entity-legal-form-type/tbl")
    public ResponseEntity<ApiResponseDto<List<REntityLegalFormTypeDto>>> getEntityLegalFormTypesWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer perPage,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "entityLegalFormTypeId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction) {
        log.info("Inside EntityLegalFormTypeController: getEntityLegalFormTypesWithPagination method");
        return entityLegalFormTypeService.getEntityLegalFormTypesWithPagination(page, perPage, search, sort, direction);
    }

    @PostMapping("/entity-legal-form-type")
    public ResponseEntity<REntityLegalFormTypeDto> addEntityLegalFormType(@Valid @RequestBody REntityLegalFormTypeDto entityLegalFormTypeDto) {
        log.info("Inside EntityLegalFormTypeController: addEntityLegalFormType method");
        return entityLegalFormTypeService.addEntityLegalFormType(entityLegalFormTypeDto);
    }



    @GetMapping("/entity-legal-form-type/{id}")
    public ResponseEntity<REntityLegalFormTypeDto> getEntityLegalFormTypeById(@PathVariable Integer id) {
        log.info("Inside EntityLegalFormTypeController: getEntityLegalFormTypeById method");
        return entityLegalFormTypeService.getEntityLegalFormTypeById(id);
    }

    @PutMapping("/entity-legal-form-type/{id}")
    public ResponseEntity<REntityLegalFormTypeDto> updateEntityLegalFormTypeById(@PathVariable Integer id, @Valid @RequestBody REntityLegalFormTypeDto entityLegalFormTypeDto) {
        log.info("Inside EntityLegalFormTypeController: updateEntityLegalFormTypeById method");
        return entityLegalFormTypeService.updateEntityLegalFormTypeById(id, entityLegalFormTypeDto);
    }

    @DeleteMapping("/entity-legal-form-type/{id}")
    public ResponseEntity<REntityLegalFormTypeDto> deleteEntityLegalFormTypeById(@PathVariable Integer id) {
        log.info("Inside EntityLegalFormTypeController: deleteEntityLegalFormTypeById method");
        return entityLegalFormTypeService.deleteEntityLegalFormTypeById(id);
    }




    /**
     * Fund Type Controller
     *
     * @return
     */

    @GetMapping("/fund-type/all")
    public ResponseEntity<List<RFundsTypeDto>> getAllFundType() {
        log.info("Inside BaseParameterController: getAllFundType method");
        return fundTypeService.getAllFundType();
    }

    @GetMapping("/fund-type/tbl")
    public ResponseEntity<ApiResponseDto<List<RFundsTypeDto>>> getFundTypeWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "fundTypeId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getFundTypeWithPagination method");
        return fundTypeService.getFundTypeWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/fund-type/add")
    public ResponseEntity<RFundsTypeDto> addFundType(@Valid @RequestBody RFundsTypeDto fundTypeDto) {
        log.info("Inside BaseParameterController: addFundType method");
        return fundTypeService.addFundType(fundTypeDto);
    }

    @GetMapping("/fund-type/get/{id}")
    public ResponseEntity<RFundsTypeDto> getFundTypeById(
            @PathVariable Long id
    ) {
        log.info("Inside BaseParameterController: getFundTypeById method");
        return fundTypeService.getFundTypeById(id);
    }

    @PutMapping("/fund-type/update/{id}")
    public ResponseEntity<RFundsTypeDto> updateFundTypeById(
            @PathVariable Long id,
            @RequestBody RFundsTypeDto fundTypeDto
    ) {
        log.info("Inside BaseParameterController: updateFundTypeById method");
        return fundTypeService.updateFundTypeById(id, fundTypeDto);
    }

    @DeleteMapping("/fund-type/delete/{id}")
    public ResponseEntity<RFundsTypeDto> deleteFundTypeById(
            @PathVariable Integer id
    ) {
        log.info("Inside BaseParameterController: deleteFundTypeById method");
        return fundTypeService.deleteFundTypeById(id);
    }




    /**
     * Report Code Controller
     *
     * @return
     */

    @GetMapping("/report-code/all")
    public ResponseEntity<List<RReportCodeDto>> getAllReportCode() {
        log.info("Inside BaseParameterController: getAllReportCode method");
        return reportCodeService.getAllReportCode();
    }

    @GetMapping("/report-code/tbl")
    public ResponseEntity<ApiResponseDto<List<RReportCodeDto>>> getReportCodeWithPagination(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "10", required = false) Integer per_page,
            @RequestParam(defaultValue = "", required = false) String search,
            @RequestParam(defaultValue = "reportCodeId", required = false) String sort,
            @RequestParam(defaultValue = "asc", required = false) String direction
    ) {
        log.info("Inside BaseParameterController: getReportCodeWithPagination method");
        return reportCodeService.getReportCodeWithPagination(page, per_page, search, sort, direction);
    }

    @PostMapping("/report-code/add")
    public ResponseEntity<RReportCodeDto> addReportCode(@Valid @RequestBody RReportCodeDto reportCodeDto) {
        log.info("Inside BaseParameterController: addReportCode method");
        return reportCodeService.addReportCode(reportCodeDto);
    }

    @GetMapping("/report-code/get/{id}")
    public ResponseEntity<RReportCodeDto> getReportCodeById(
            @PathVariable Integer id
    ) {
        log.info("Inside BaseParameterController: getReportCodeById method");
        return reportCodeService.getReportCodeById(id);
    }

    @PutMapping("/report-code/update/{id}")
    public ResponseEntity<RReportCodeDto> updateReportCodeById(
            @PathVariable Integer id,
            @RequestBody RReportCodeDto reportCodeDto
    ) {
        log.info("Inside BaseParameterController: updateReportCodeById method");
        return reportCodeService.updateReportCodeById(id, reportCodeDto);
    }

    @DeleteMapping("/report-code/delete/{id}")
    public ResponseEntity<RReportCodeDto> deleteReportCodeById(
            @PathVariable Integer id
    ) {
        log.info("Inside BaseParameterController: deleteReportCodeById method");
        return reportCodeService.deleteReportCodeById(id);
    }


}



