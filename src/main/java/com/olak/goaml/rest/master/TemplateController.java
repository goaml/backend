package com.olak.goaml.rest.master;

import com.olak.goaml.dto.master.TemplateModelDto;
import com.olak.goaml.service.TemplateService;
import com.olak.goaml.userMGT.service.other.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("template")
public class TemplateController {

    private final TemplateService templateService;
    @Autowired
    private MailService mailService;

    public TemplateController(TemplateService templateService) {
        this.templateService = templateService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<TemplateModelDto> getTemplate(@PathVariable Long id) {
        return templateService.getTemplate(id);
    }

    @GetMapping("/test/email")
    public ResponseEntity<String> testEmail() {
        mailService.sendEMail("chamodishankha@gmail.com", "Test Email", "This is a test email");
        return new ResponseEntity<>("Done", HttpStatus.OK);
    }

}