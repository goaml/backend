package com.olak.goaml.constants;

public class HardCodeConstants {

    public static final Integer GLOBAL_DETAILS_ID = 1;
    public static final Integer DEFAULT_COMPANY_ID = 1;
    public static final int OTP_EXPIRE_TIME = 5;
    public static final String SMS_MASK = "CarMarket";
    public static final int FILE_UPLOAD_TYPE_NFS = 1;
    public static final int FILE_UPLOAD_TYPE_BASE64 = 2;
    public static Integer DOCUMENT_PENDING_STATUS_ID = 2;
    public static Integer NEW_STATUS_ID = 1;
    public static Integer PENDING_AUTHORIZATION_STATUS_ID = 2;
    public static Integer ACTIVE_AUTHORIZED_STATUS_ID = 3;
    public static Integer RESTRICTED_STATUS_ID = 4;
    public static Integer EDITED_STATUS_ID = 5;
    public static Integer ADMIN_ROLE = 1;
    public static Integer USER_ROLE = 2;
    public static Integer USER = 14;
    public static Integer COMPANY = 32;
    public static Integer MEMBER = 28;
    public static Integer DEFAULT_DEPARTMENT_ID = 1;
    public static Integer DEFAULT_BRANCH_ID = 1;
    public static Integer STAFF_USER_TYPE_ID = 1;
    public static Integer COMPANY_USER_TYPE_ID = 2;
    public static Integer MEMBER_USER_TYPE_ID = 3;
    public static Integer LOGIN_TYPE_NIC_ID = 1;
    public static Integer LOGIN_TYPE_MOBILE_ID = 2;
    public static Integer LOGIN_TYPE_EMAIL_ID = 3;
    public static Integer LOGIN_TYPE_REFERENCE_NO_ID = 4;


}
