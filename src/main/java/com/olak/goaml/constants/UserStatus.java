package com.olak.goaml.constants;

/**
 * The enum Active status.
 */
public enum UserStatus {
    /**
     * ACTIVE USER status constants.
     */
    ACTIVE('A'),
    /**
     * INACTIVE USER status constants.
     */
    INACTIVE('I'),
    /**
     * RESIGNED USER status constants.
     */
    RESIGNED('R'),
    /**
     * EXPIRED USER status constants.
     */
    EXPIRED('E'),
    /**
     * TERMINATED USER status constants.
     */
    TERMINATED('T'),
    /**
     * BLACKLISTED USER status constants.
     */
    BLACKLISTED('B');

    private final Character value;

    UserStatus(final Character newValue) {
        value = newValue;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public Character getValue() {
        return value;
    }

}
