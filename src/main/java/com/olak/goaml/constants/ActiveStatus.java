package com.olak.goaml.constants;



/**
 * The enum Active status.
 */
public enum ActiveStatus {
    /**
     * Inactive travel status constants.
     */
    INACTIVE(false),
    /**
     * Active travel status constants.
     */
    ACTIVE(true);

    private final Boolean value;

    ActiveStatus(final Boolean newValue) {
        value = newValue;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public Boolean getValue() {
        return value;
    }

}
