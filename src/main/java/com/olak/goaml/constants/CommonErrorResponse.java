package com.olak.goaml.constants;

public enum CommonErrorResponse {

    /**
     * Id exists common response.
     */
    ID_EXISTS("ID_EXISTS"),
    /**
     * Id null common response.
     */
    ID_NULL("ID_NULL"),
    /**
     * Invalid id common response.
     */
    INVALID_ID("INVALID_ID"),
    /**
     * Id not found common response.
     */
    ID_NOT_FOUND("ID_NOT_FOUND"),
    /**
     * The Null id found.
     */
    NULL_ID_FOUND("Null Id Found"),
    /**
     * The Invalid id found.
     */
    INVALID_ID_FOUND("Invalid Id Found"),
    /**
     * The Entity not found.
     */
    ENTITY_NOT_FOUND("Entity not found");

    private final String response;


    /**
     * @param response
     */
    CommonErrorResponse(final String response) {
        this.response = response;
    }

    /**
     * Entity error string.
     *
     * @param clazz the clazz
     * @return the string
     */
    public static String entityError(Class<?> clazz) {
        return String.format("A new %s cannot already have an ID", clazz.getSimpleName());
    }

    /**
     * (non-Javadoc)
     *
     * @see Enum#toString()
     */
    @Override
    public String toString() {
        return response;
    }
}
