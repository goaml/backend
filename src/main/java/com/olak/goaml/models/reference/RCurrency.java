package com.olak.goaml.models.reference;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "R_CURRENCY", indexes = {
        @Index(name = "R_CURRENCY_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class RCurrency extends AuditModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "R_CURRENCY", allocationSize = 1, sequenceName = "R_CURRENCY_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "R_CURRENCY")
    @Column(name = "CURRENCIES_ID", nullable = false, length = 8)
    private Integer currenciesId;

    @Column(name = "CURRENCIES_CODE", length = 10)
    private String currenciesCode;

    @Column(name = "DESCRIPTION", length = 200)
    private String description;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;
}