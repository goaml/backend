package com.olak.goaml.models.reference;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "R_PARTY_TYPE", indexes = {
        @Index(name = "R_PARTY_TYPE_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class RPartyType extends AuditModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "R_PARTY_TYPE", allocationSize = 1, sequenceName = "R_PARTY_TYPE_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "R_PARTY_TYPE")
    @Column(name = "PARTY_TYPE_ID", nullable = false, length = 8)
    private Integer partyTypeId;

    @Column(name = "PARTY_TYPE_CODE", length = 10)
    private String partyTypeCode;

    @Column(name = "DESCRIPTION", length = 200)
    private String description;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;
}