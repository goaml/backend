package com.olak.goaml.models.reference;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "R_REPORT_CODE", indexes = {
        @Index(name = "R_REPORT_CODE_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class RReportCode extends AuditModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "R_REPORT_CODE", allocationSize = 1, sequenceName = "R_REPORT_CODE_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "R_REPORT_CODE")
    @Column(name = "REPORT_CODE_ID", nullable = false, length = 8)
    private Integer reportCodeId;

    @Column(name = "REPORT_CODE", length = 10)
    private String reportCode;

    @Column(name = "DESCRIPTION", length = 200)
    private String description;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;
}