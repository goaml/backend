package com.olak.goaml.models.transaction;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DailyTranDetailTableTempId implements Serializable {

    @Column(name = "tran_id", length = 9)
    private String tranId;

    @Column(name = "part_tran_type", length = 10)
    private String partTranType;

    // Default constructor
    public DailyTranDetailTableTempId() {}

    // Parameterized constructor
    public DailyTranDetailTableTempId(String tranId, String partTranType) {
        this.tranId = tranId;
        this.partTranType = partTranType;
    }

    // Getters and setters
    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getPartTranType() {
        return partTranType;
    }

    public void setPartTranType(String partTranType) {
        this.partTranType = partTranType;
    }

    // Override equals and hashCode
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DailyTranDetailTableTempId that = (DailyTranDetailTableTempId) o;

        if (!tranId.equals(that.tranId)) return false;
        return partTranType.equals(that.partTranType);
    }

    @Override
    public int hashCode() {
        int result = tranId.hashCode();
        result = 31 * result + partTranType.hashCode();
        return result;
    }
}
