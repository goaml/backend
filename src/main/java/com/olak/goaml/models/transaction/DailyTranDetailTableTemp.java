package com.olak.goaml.models.transaction;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "DAILY_TRAN_DETAIL_TABLE", indexes = {
})
public class DailyTranDetailTableTemp {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "tran_date")
    private LocalDate tranDate;

    @Column(name = "tran_id", length = 9)
    private String tranId;

    @Column(name = "part_tran_srl_num", length = 4)
    private String partTranSrlNum;

    @Column(name = "del_flg", length = 1)
    private String delFlg;

    @Column(name = "tran_type", length = 1)
    private String tranType;

    @Column(name = "tran_sub_type", length = 2)
    private String tranSubType;

    @Column(name = "part_tran_type", length = 1)
    private String partTranType;

    @Column(name = "gl_sub_head_code", length = 5)
    private String glSubHeadCode;

    @Column(name = "acid", length = 11)
    private String acid;

    @Column(name = "value_date")
    private LocalDate valueDate;

    @Column(name = "tran_amt", precision = 24, scale = 4)
    private BigDecimal tranAmt;

    @Column(name = "tran_particular", length = 50)
    private String tranParticular;

    @Column(name = "entry_user_id", length = 15)
    private String entryUserId;

    @Column(name = "pstd_user_id", length = 15)
    private String pstdUserId;

    @Column(name = "vfd_user_id", length = 15)
    private String vfdUserId;

    @Column(name = "entry_date")
    private LocalDate entryDate;

    @Column(name = "pstd_date")
    private LocalDate pstdDate;

    @Column(name = "vfd_date")
    private LocalDate vfdDate;

    @Column(name = "rpt_code", length = 5)
    private String rptCode;

    @Column(name = "ref_num", length = 20)
    private String refNum;

    @Column(name = "instrmnt_type", length = 5)
    private String instrmntType;

    @Column(name = "instrmnt_date")
    private LocalDate instrmntDate;

    @Column(name = "instrmnt_num", length = 16)
    private String instrmntNum;

    @Column(name = "instrmnt_alpha", length = 6)
    private String instrmntAlpha;

    @Column(name = "tran_rmks", length = 30)
    private String tranRmks;

    @Column(name = "pstd_flg", length = 1)
    private String pstdFlg;

    @Column(name = "prnt_advc_ind", length = 1)
    private String prntAdvcInd;

    @Column(name = "amt_reservation_ind", length = 1)
    private String amtReservationInd;

    @Column(name = "reservation_amt", precision = 24, scale = 4)
    private BigDecimal reservationAmt;

    @Column(name = "restrict_modify_ind", length = 1)
    private String restrictModifyInd;

    @Column(name = "lchg_user_id", length = 15)
    private String lchgUserId;

    @Column(name = "lchg_time")
    private LocalDate lchgTime;

    @Column(name = "rcre_user_id", length = 15)
    private String rcreUserId;

    @Column(name = "rcre_time")
    private LocalDate rcreTime;

    @Column(name = "cust_id", length = 9)
    private String custId;

    @Column(name = "voucher_print_flg", length = 1)
    private String voucherPrintFlg;

    @Column(name = "module_id", length = 3)
    private String moduleId;

    @Column(name = "br_code", length = 6)
    private String brCode;

    @Column(name = "fx_tran_amt", precision = 24, scale = 4)
    private BigDecimal fxTranAmt;

    @Column(name = "rate_code", length = 5)
    private String rateCode;

    @Column(name = "rate", precision = 31, scale = 10)
    private BigDecimal rate;

    @Column(name = "crncy_code", length = 3)
    private String crncyCode;

    @Column(name = "navigation_flg", length = 1)
    private String navigationFlg;

    @Column(name = "tran_crncy_code", length = 3)
    private String tranCrncyCode;

    @Column(name = "ref_crncy_code", length = 3)
    private String refCrncyCode;

    @Column(name = "ref_amt", precision = 24, scale = 4)
    private BigDecimal refAmt;

    @Column(name = "sol_id", length = 8)
    private String solId;

    @Column(name = "bank_code", length = 6)
    private String bankCode;

    @Column(name = "trea_ref_num", length = 16)
    private String treaRefNum;

    @Column(name = "trea_rate", precision = 31, scale = 10)
    private BigDecimal treaRate;

    @Column(name = "ts_cnt", precision = 10, scale = 2)
    private BigDecimal tsCnt;

    @Column(name = "gst_upd_flg", length = 1)
    private String gstUpdFlg;

    @Column(name = "iso_flg", length = 1)
    private String isoFlg;

    @Column(name = "eabfab_upd_flg", length = 1)
    private String eabfabUpdFlg;

    @Column(name = "lift_lien_flg", length = 1)
    private String liftLienFlg;

    @Column(name = "proxy_post_ind", length = 1)
    private String proxyPostInd;

    @Column(name = "si_srl_num", length = 12)
    private String siSrlNum;

    @Column(name = "si_org_exec_date")
    private LocalDate siOrgExecDate;

    @Column(length = 9, name = "pr_srl_num")
    private String prSrlNum;

    @Column(name = "serial_num", length = 4)
    private String serialNum;

    @Column(name = "del_memo_pad", length = 1)
    private String delMemoPad;

    @Column(name = "uad_module_id", length = 6)
    private String uadModuleId;

    @Column(name = "uad_module_key", length = 256)
    private String uadModuleKey;

    @Column(name = "reversal_date")
    private LocalDate reversalDate;

    @Column(name = "reversal_value_date")
    private LocalDate reversalValueDate;

    @Column(name = "pttm_event_type", length = 5)
    private String pttmEventType;

    @Column(name = "proxy_acid", length = 11)
    private String proxyAcid;

    @Column(name = "tod_entity_type", length = 5)
    private String todEntityType;

    @Column(name = "tod_entity_id", length = 50)
    private String todEntityId;

    @Column(name = "dth_init_sol_id", length = 8)
    private String dthInitSolId;

    @Column(name = "regularization_amt", precision = 24, scale = 4)
    private BigDecimal regularizationAmt;

    @Column(name = "principal_portion_amt", precision = 24, scale = 4)
    private BigDecimal principalPortionAmt;

    @Column(name = "tf_entity_sol_id", length = 8)
    private String tfEntitySolId;

    @Column(name = "tran_particular_2", length = 50)
    private String tranParticular2;

    @Column(name = "tran_particular_code", length = 5)
    private String tranParticularCode;

    @Column(name = "tr_status", length = 1)
    private String trStatus;

    @Column(name = "svs_tran_id", length = 25)
    private String svsTranId;

    @Column(name = "crncy_hol_chk_done_flg", length = 1)
    private String crncyHolChkDoneFlg;

    @Column(name = "referral_id", length = 12)
    private String referralId;

    @Column(name = "party_code", length = 9)
    private String partyCode;

    @Column(name = "gl_date")
    private LocalDate glDate;

    @Column(name = "bkdt_tran_flg", length = 1)
    private String bkdtTranFlg;

    @Column(name = "bank_id", length = 8)
    private String bankId;

    @Column(name = "impl_cash_part_tran_flg", length = 1)
    private String implCashPartTranFlg;

    @Column(name = "ptran_chrg_exists_flg", length = 1)
    private String ptranChrgExistsFlg;

    @Column(name = "mud_pool_bal_build_flg", length = 1)
    private String mudPoolBalBuildFlg;

    @Column(name = "gl_segment_string", length = 50)
    private String glSegmentString;

    @Column(name = "sys_part_tran_code", length = 10)
    private String sysPartTranCode;

    @Column(name = "user_part_tran_code", length = 10)
    private String userPartTranCode;

    @Column(name = "tran_free_code1", length = 5)
    private String tranFreeCode1;

    @Column(name = "tran_free_code2", length = 5)
    private String tranFreeCode2;

}
