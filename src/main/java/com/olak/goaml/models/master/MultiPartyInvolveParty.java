package com.olak.goaml.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.models.reference.RStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "P_MULTI_PARTY_INVOLVE_PARTY", indexes = {
        @Index(name = "P_MULTI_PARTY_INVOLVE_PARTY_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class MultiPartyInvolveParty extends AuditModel implements Serializable {

    @Id
    @SequenceGenerator(name = "P_MULTI_PARTY_INVOLVE_PARTY", sequenceName = "P_MULTI_PARTY_INVOLVE_PARTY_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "P_MULTI_PARTY_INVOLVE_PARTY")
    @Column(name = "MULTI_PARTY_INVOLVE_PARTY_ID")
    private Long multiPartyInvolvePartyId;

    @Column(name = "MULTI_PARTY_INVOLVE_PARTY_NAME")
    private String multiPartyInvolvePartyName;

    @Column(name = "MULTI_PARTY_INVOLVE_PARTY_DESCRIPTION")
    private String multiPartyInvolvePartyDescription;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;
}