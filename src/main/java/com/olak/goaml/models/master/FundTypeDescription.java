package com.olak.goaml.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.models.reference.RStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "P_FUND_TYPE_DESCRIPTION", indexes = {
        @Index(name = "P_FUND_TYPE_DESCRIPTION_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class FundTypeDescription extends AuditModel implements Serializable {

    @Id
    @SequenceGenerator(name = "P_FUND_TYPE_DESCRIPTION", sequenceName = "P_FUND_TYPE_DESCRIPTION_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "P_FUND_TYPE_DESCRIPTION")
    @Column(name = "FUND_TYPE_DESCRIPTION_ID")
    private Long fundTypeDescriptionId;

    @Column(name = "FUND_TYPE_DESCRIPTION")
    private String fundTypeDescription;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;

}