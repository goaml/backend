package com.olak.goaml.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.models.reference.RStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "P_MULTI_PARTY_ROLE", indexes = {
        @Index(name = "P_MULTI_PARTY_ROLE_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class MultiPartyRole extends AuditModel implements Serializable {

    @Id
    @SequenceGenerator(name = "P_MULTI_PARTY_ROLE", sequenceName = "P_MULTI_PARTY_ROLE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "P_MULTI_PARTY_ROLE")
    @Column(name = "MULTI_PARTY_ROLE_ID")
    private Long multiPartyRoleId;

    @Column(name = "MULTI_PARTY_ROLE_NAME")
    private String multiPartyRoleName;

    @Column(name = "MULTI_PARTY_ROLE_DESCRIPTION")
    private String multiPartyRoleDescription;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;
}