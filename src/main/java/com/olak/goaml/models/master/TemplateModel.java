package com.olak.goaml.models.master;

import lombok.*;

import javax.persistence.*;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "template")
public class TemplateModel {

    @Id
    @SequenceGenerator(name = "template", sequenceName = "template_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "template")
    @Column(name = "TEMPLATE_ID")
    private Long templateId;

    @Column(name = "TEMPLATE_NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;
}