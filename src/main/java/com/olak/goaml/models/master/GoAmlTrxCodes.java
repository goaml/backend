package com.olak.goaml.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.models.reference.RStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "P_GOAML_TRX_CODE", indexes = {
        @Index(name = "P_GOAML_TRX_CODE_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class GoAmlTrxCodes extends AuditModel implements Serializable {

    @Id
    @SequenceGenerator(name = "P_GOAML_TRX_CODE", sequenceName = "P_GOAML_TRX_CODE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "P_GOAML_TRX_CODE")
    @Column(name = "GOAML_TRX_CODE_ID")
    private Long goamlTrxCodeId;

    @Column(name = "R_CATEGORY_ID")
    private Long categoryId;

    @Column(name = "P_CATEGORY_NAME")
    private String categoryName;

    @Column(name = "FROM_FUND_TYPE_ID")
    private Long fromFundTypeId;

    @Column(name = "FROM_FUND_TYPE_NAME")
    private String fromFundTypeName;

    @Column(name = "FROM_FUND_TYPE_DESCRIPTION_ID")
    private Long fromFundTypeDescriptionId;

    @Column(name = "FROM_FUND_TYPE_DESCRIPTION")
    private String fromFundTypeDescription;

    @Column(name = "TO_FUND_TYPE_ID")
    private Long toFundTypeId;

    @Column(name = "TO_FUND_TYPE_NAME")
    private String toFundTypeName;

    @Column(name = "TO_FUND_TYPE_DESCRIPTION_ID")
    private Long toFundTypeDescriptionId;

    @Column(name = "TO_FUND_TYPE_DESCRIPTION")
    private String toFundTypeDescription;

    @Column(name = "MULTI_PARTY_FUNDS_CODE_ID")
    private Long multiPartyFundsCodeId;

    @Column(name = "MULTI_PARTY_FUNDS_CODE_NAME")
    private String multiPartyFundsCodeName;

    @Column(name = "MULTI_PARTY_INVOLVE_PARTY_ID")
    private Long multiPartyInvolvePartyId;

    @Column(name = "MULTI_PARTY_INVOLVE_PARTY_NAME")
    private String multiPartyInvolvePartyName;

    @Column(name = "MULTI_PARTY_ROLE_ID")
    private Long multiPartyRoleId;

    @Column(name = "MULTI_PARTY_ROLE_NAME")
    private String multiPartyRoleName;

    @Column(name = "REPORT_TYPE_ID")
    private Long reportTypeId;

    @Column(name = "REPORT_TYPE_NAME")
    private String reportTypeName;

    @Column(name = "RPT_CODE_ID")
    private Long rptCodeId;

    @Column(name = "RPT_CODE_NAME")
    private String rptCodeName;

    @Column(name = "TRANSACTION_CODE_ID")
    private Long transactionCodeId;

    @Column(name = "TRANSACTION_CODE_NAME")
    private String transactionCodeName;

    @Column(name = "GOAML_TRX_CODE_SCENARIO")
    private String goamlTrxCodeScenario;

    @Column(name = "GOAML_TRX_CODE_MULTI_PARTY")
    private String goamlTrxCodeMultiParty;

    @Column(name = "GOAML_TRX_CODE_BI_PARTY")
    private String goamlTrxCodeBiParty;

    @Column(name = "GOAML_TRX_CODE_FROM_FUND_TYPE")
    private String goamlTrxCodeFromFundType;

    @Column(name = "GOAML_TRX_CODE_FROM")
    private String goamlTrxCodeFrom;

    @Column(name = "GOAML_TRX_CODE_TO_FUND_TYPE")
    private String goamlTrxCodeToFundType;

    @Column(name = "GOAML_TRX_CODE_TO")
    private String goamlTrxCodeTo;

    @Column(name = "GOAML_TRX_CODE_COMMENTS")
    private String goamlTrxCodeComments;

    @Column(name = "GOAML_TRX_CODE_EXAMPLE")
    private String goamlTrxCodeExample;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;
    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;


}