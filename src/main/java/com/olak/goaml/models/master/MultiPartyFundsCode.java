package com.olak.goaml.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.models.reference.RStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "P_MULTI_PARTY_FUNDS_CODE", indexes = {
        @Index(name = "P_MULTI_PARTY_FUNDS_CODE_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class MultiPartyFundsCode extends AuditModel implements Serializable {

    @Id
    @SequenceGenerator(name = "P_MULTI_PARTY_FUNDS_CODE", sequenceName = "P_MULTI_PARTY_FUNDS_CODE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "P_MULTI_PARTY_FUNDS_CODE")
    @Column(name = "MULTI_PARTY_FUNDS_CODE_ID")
    private Long multiPartyFundsCodeId;

    @Column(name = "MULTI_PARTY_FUNDS_CODE_NAME")
    private String multiPartyFundsCodeName;

    @Column(name = "MULTI_PARTY_FUNDS_CODE_DESCRIPTION")
    private String multiPartyFundsCodeDescription;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;
}