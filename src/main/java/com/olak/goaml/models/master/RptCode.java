package com.olak.goaml.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.models.reference.RStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "P_RPT_CODE", indexes = {
        @Index(name = "P_RPT_CODE_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class RptCode extends AuditModel implements Serializable {

    @Id
    @SequenceGenerator(name = "P_RPT_CODE", sequenceName = "P_RPT_CODE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "P_RPT_CODE")
    @Column(name = "RPT_CODE_ID")
    private Long rptCodeId;

    @Column(name = "RPT_CODE_NAME")
    private String rptCodeName;

    @Column(name = "RPT_CODE_DESCRIPTION")
    private String rptCodeDescription;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GOAML_TRX_CODE_ID", referencedColumnName = "GOAML_TRX_CODE_ID", nullable = false)
    private GoAmlTrxCodes goAmlTrxCodes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;
}