package com.olak.goaml.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.models.reference.RStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "P_TRANSACTION_CODE", indexes = {
        @Index(name = "P_TRANSACTION_CODE_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class TransactionCodes extends AuditModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "P_TRANSACTION_CODE", sequenceName = "P_TRANSACTION_CODE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "P_TRANSACTION_CODE")
    @Column(name = "TRANSACTION_CODE_ID")
    private Long transactionCodeId;

    @Column(name = "TRANSACTION_CODE_NAME")
    private String transactionCodeName;

    @Column(name = "TRANSACTION_CODE_DESCRIPTION")
    private String transactionCodeDescription;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;

}