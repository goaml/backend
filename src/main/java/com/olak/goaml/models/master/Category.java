package com.olak.goaml.models.master;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.models.reference.RStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "P_CATEGORY", indexes = {
        @Index(name = "P_CATEGORY_R_STATUS1_IDX", columnList = "STATUS_ID")
})
public class Category extends AuditModel implements Serializable {

    @Id
    @SequenceGenerator(name = "P_CATEGORY", sequenceName = "P_CATEGORY_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "P_CATEGORY")
    @Column(name = "P_CATEGORY_ID")
    private Long categoryId;

    @Column(name = "P_CATEGORY_NAME")
    private String categoryName;

    @Column(name = "P_CATEGORY_DESCRIPTION")
    private String categoryDescription;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
    private RStatus rStatus;

}