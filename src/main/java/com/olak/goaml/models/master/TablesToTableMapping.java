package com.olak.goaml.models.master;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "TABLES_TO_TABLE_MAPPING")
public class TablesToTableMapping {

    @Id
    @SequenceGenerator(name = "TABLES_TO_TABLE_MAPPING", sequenceName = "TABLES_TO_TABLE_MAPPING_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TABLES_TO_TABLE_MAPPING")
    @Column(name = "TABLES_TO_TABLE_MAPPING_ID")
    private Integer tableToTableMappingId;

    @Column(name = "TABLE_NAME")
    private String tableName;
    @Column(name = "COLUMN_NAME")
    private String columnName;
    @Column(name = "MAPPING_TABLE_NAME")
    private String mappingTableName;
    @Column(name = "MAPPING_COLUMN_NAME")
    private String mappingColumnName;
}
