package com.olak.goaml.models.xml.transaction;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.olak.goaml.models.reference.*;


/**
 * The persistent class for the t_person_my_client database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_PERSON_MY_CLIENT")
public class TPersonMyClient extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_PERSON_MY_CLIENT", allocationSize = 1, sequenceName="T_PERSON_MY_CLIENT_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_PERSON_MY_CLIENT")
	@Column(name="PERSON_MY_CLIENT_ID")
	private Integer personMyClientId;

	@Column(name = "ADDRESSES")
	private String addresses;

	@Column(name="ALIAS")
	private String alias;

	@Column(name="BIRTH_PLACE")
	private String birthPlace;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BIRTH_DATE")
	private Date birthdate;

	@Column(name="COMMENTS")
	private String comments;

	@Column(name="DECEASED")
	private Boolean deceased;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DECEASED_DATE")
	private Date deceasedDate;

	@Column(name="EMAIL")
	private String email;

	@Column(name="EMPLOYER_NAME")
	private String employerName;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="ID_NUMBER")
	private String idNumber;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="MIDDLE_NAME")
	private String middleName;

	@Column(name="MOTHERS_NAME")
	private String mothersName;

	@Column(name="OCCUPATION")
	private String occupation;

	@Column(name="PASSPORT_NUMBER")
	private String passportNumber;

	@Column(name = "PHONES")
	private String phones;

	@Column(name="PREFIX")
	private String prefix;

	@Column(name="SOURCE_OF_WEALTH")
	private String sourceOfWealth;

	@Column(name = "SSN")
	private String ssn;

	@Column(name="TAX_NUMBER")
	private String taxNumber;

	@Column(name="TAX_REG_NUMBER")
	private String taxRegNumber;

	@Column(name = "TITLE")
	private String title;


	//bi-directional many-to-one association to TActivity
	@OneToMany(mappedBy="TPersonMyClient")
	private List<TActivity> TActivities;




	//Relationship Removed
	@Column(name="GENDER_TYPE_ID")
	private Integer genderTypeId;
	@Column(name="PASSPORT_COUNTRY_Id")
	private Integer passportCountryId;
	@Column(name="NATIONALITY_COUNTRY_Id")
	private Integer nationalityCountryId;
	@Column(name="NATIONALITY_COUNTRY2_Id")
	private Integer nationalityCountry2Id;
	@Column(name="NATIONALITY_COUNTRY3_Id")
	private Integer nationalityCountry3Id;
	@Column(name="RESIDENCE_COUNTRY_Id")
	private Integer residenceCountryId;
	@Column(name="PHONE_ID")
	private Integer phoneId;
	@Column(name="ADDRESS_Id")
	private Integer addressId;
	@Column(name="EMPLOYER_PHONE_ID")
	private Integer employerPhoneId;
	@Column(name="EMPLOYER_ADDRESS_Id")
	private Integer employerAddressId;
	@Column(name="PERSON_IDENTIFICATION_ID")
	private Integer personIdentificationId;
	@Column(name="entity_Person_Role_Type_Id")
	private Integer entityPersonRoleTypeId;





//	//bi-directional many-to-one association to RGenderType
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="GENDER_TYPE_ID", referencedColumnName = "GENDER_TYPE_ID", nullable = false)
//	private RGenderType RGenderType;
//
//
//	//bi-directional many-to-one association to RCountryCode
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="PASSPORT_COUNTRY", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
//	private RCountryCodes RCountryCodes1;
//
//	//bi-directional many-to-one association to RCountryCode
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="NATIONALITY1", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
//	private RCountryCodes RCountryCodes2;
//
//	//bi-directional many-to-one association to RCountryCodes
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="NATIONALITY2", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
//	private RCountryCodes RCountryCodes3;
//
//	//bi-directional many-to-one association to RCountryCodes
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="NATIONALITY3", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
//	private RCountryCodes RCountryCodes4;
//
//	//bi-directional many-to-one association to RCountryCodes
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="RESIDENCE", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
//	private RCountryCodes RCountryCodes5;
//
//	//bi-directional many-to-one association to TPhone
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="PHONE", referencedColumnName = "PHONE_ID", nullable = false)
//	private TPhone TPhone1;
//
//	//bi-directional many-to-one association to TAddress
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ADDRESS", referencedColumnName = "ADDRESS_ID", nullable = false)
//	private TAddress TAddress1;
//
//
//	//bi-directional many-to-one association to TAddress
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="EMPLOYER_ADDRESS_ID", referencedColumnName = "ADDRESS_ID", nullable = false)
//	private TAddress TAddress2;
//
//
//	//bi-directional many-to-one association to TPhone
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="EMPLOYER_PHONE_ID", referencedColumnName = "PHONE_ID", nullable = false)
//	private TPhone TPhone2;
//
//	//bi-directional many-to-one association to TPersonIdentification
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="PERSON_IDENTIFICATION_ID", referencedColumnName = "PERSON_IDENTIFICATION_ID", nullable = false)
//	private TPersonIdentification TPersonIdentification;



	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;

	//bi-directional many-to-one association to RAccountStatusType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CLIENT_TYPE_ID", referencedColumnName = "CLIENT_TYPE_ID", nullable = false)
	private RClientType RClientType;

}