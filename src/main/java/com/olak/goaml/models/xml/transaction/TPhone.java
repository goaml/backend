package com.olak.goaml.models.xml.transaction;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

import com.olak.goaml.models.reference.*;


/**
 * The persistent class for the t_phone database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_PHONE")
public class TPhone extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_PHONE", allocationSize = 1, sequenceName="T_PHONE_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_PHONE")
	@Column(name="PHONE_ID")
	private Integer phoneId;

	@Column(name = "COMMENTS")
	private String comments;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;

	@Column(name="TPH_COUNTRY_PREFIX")
	private String tphCountryPrefix;

	@Column(name="TPH_EXTENSION")
	private String tphExtension;

	@Column(name="TPH_NUMBER")
	private String tphNumber;

	//bi-directional many-to-one association to RContactType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TPH_CONTACT_TYPE_ID", referencedColumnName = "CONTACT_TYPE_ID", nullable = false)
	private RContactType RContactType;

	//bi-directional many-to-one association to RCommunicationType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TPH_COMMUNICATION_TYPE_ID", referencedColumnName = "COMMUNICATION_TYPE_ID", nullable = false)
	private RCommunicationType RCommunicationType;

	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;

}