package com.olak.goaml.models.xml.transaction;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

import com.olak.goaml.models.reference.*;

/**
 * The persistent class for the t_person_identification database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_PERSON_IDENTIFICATION")
public class TPersonIdentification extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_PERSON_IDENTIFICATION", allocationSize = 1, sequenceName="T_PERSON_IDENTIFICATION_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_PERSON_IDENTIFICATION")
	@Column(name="PERSON_IDENTIFICATION_ID")
	private Integer personIdentificationId;

	@Column(name = "COMMENTS")
	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="EXPIRY_DATE")
	private Date expiryDate;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ISSUE_DATE")
	private Date issueDate;

	@Column(name="ISSUED_BY")
	private String issuedBy;

	@Column(name = "NUMBER")
	private String number;


	//Relationship Removed
	@Column(name="ISSUE_COUNTRY_CODE_ID")
	private Integer issueCountryCodeId;


	//bi-directional many-to-one association to RIdentifierType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDENTIFIER_TYPE_ID", referencedColumnName = "IDENTIFIER_TYPE_ID", nullable = false)
	private RIdentifierType RIdentifierType;

//	//bi-directional many-to-one association to RCountryCode
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ISSUE_COUNTRY_CODE_ID", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
//	private RCountryCodes RCountryCodes;


	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;


}