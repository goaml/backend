package com.olak.goaml.models.xml.transaction;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

import com.olak.goaml.models.reference.*;


/**
 * The persistent class for the t_address database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_ADDRESS", indexes = {
		@Index(name = "T_ADDRESS_R_CITY_LIST1_idx", columnList = "CITY_ID"),
		@Index(name = "T_ADDRESS_R_CONTACT_TYPE1_idx", columnList = "CONTACT_TYPE_ID"),
		@Index(name = "T_ADDRESS_R_COUNTRY_CODES1_idx", columnList = "COUNTRY_CODE_ID")
})
public class TAddress extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_ADDRESS", allocationSize = 1, sequenceName="T_ADDRESS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_ADDRESS")
	@Column(name="ADDRESS_ID")
	private Integer addressId;

	@Column(name="ADDRESS")
	private String address;

	@Column(name="COMMENTS")
	private String comments;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;

	@Column(name="STATE")
	private String state;

	@Column(name="TOWN")
	private String town;

	@Column(name="ZIP")
	private String zip;

	//bi-directional many-to-one association to RCityList
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CITY_ID", referencedColumnName = "CITY_ID", nullable = false)
	private RCityList RCityList;

	//bi-directional many-to-one association to RContactType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONTACT_TYPE_ID", referencedColumnName = "CONTACT_TYPE_ID", nullable = false)
	private RContactType RContactType;

	//bi-directional many-to-one association to RCountryCode
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COUNTRY_CODE_ID", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
	private RCountryCodes RCountryCodes;

	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;

}