package com.olak.goaml.models.xml.transaction;

import java.io.Serializable;
import javax.persistence.*;

import com.olak.goaml.audit.AuditModel;
import com.olak.goaml.models.reference.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the t_party database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_PARTY")
public class TParty extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_PARTY",allocationSize = 1, sequenceName="T_PARTY_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_PARTY")
	@Column(name="party_id")
	private Integer partyId;

	@Column(name = "COMMENTS")
	private String comments;

	@Column(name="COUNTRY")
	private String country;

	@Column(name="FUNDS_COMMENT")
	private String fundsComment;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;

	@Column(name="SIGNIFICANCE")
	private Integer significance;


	//bi-directional many-to-one association to RPartyType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ROLE_PARTY_TYPE_ID", referencedColumnName = "PARTY_TYPE_ID", nullable = false)
	private RPartyType RPartyType;

	//bi-directional many-to-one association to RFundsType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FUND_TYPE_ID", referencedColumnName = "FUND_TYPE_ID", nullable = false)
	private RFundsType RFundsType;

	//bi-directional many-to-one association to RCountryCode
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SIGNIFICANCE_COUNTRY_CODE_ID", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
	private RCountryCodes RCountryCodes;



	//Relationship Removed
	@Column(name="PERSON_ID")
	private Integer personId;
	@Column(name="PERSON_MY_CLIENT_ID")
	private Integer personMyClientId;
	@Column(name="ACCOUNT_ID")
	private Integer accountId;
	@Column(name="ACCOUNT_MY_CLIENT_ID")
	private Integer accountMyClientId;
	@Column(name="ENTITY_ID")
	private Integer entityId;
	@Column(name="ENTITY_MY_CLIENT_ID")
	private Integer entityMyClientId;
	@Column(name="FOREIGN_CURRENCY_ID")
	private Integer foreignCurrencyId;


//	//bi-directional many-to-one association to TPersonMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="PERSON", referencedColumnName = "PERSON_MY_CLIENT_ID", nullable = false)
//	private TPersonMyClient TPersonMyClient1;
//
//	//bi-directional many-to-one association to TPersonMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="PERSON_MY_CLIENT", referencedColumnName = "PERSON_MY_CLIENT_ID", nullable = false)
//	private TPersonMyClient TPersonMyClient2;
//
//	//bi-directional many-to-one association to TAccountMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ACCOUNT", referencedColumnName = "ACCOUNT_MY_CLIENT_ID", nullable = false)
//	private TAccountMyClient TAccountMyClient1;
//
//	//bi-directional many-to-one association to TAccountMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ACCOUNT_MY_CLIENT", referencedColumnName = "ACCOUNT_MY_CLIENT_ID", nullable = false)
//	private TAccountMyClient TAccountMyClient2;
//
//	//bi-directional many-to-one association to TEntityMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ENTITY", referencedColumnName = "ENTITY_MY_CLIENT_ID", nullable = false)
//	private TEntityMyClient TEntityMyClient1;
//
//	//bi-directional many-to-one association to TEntityMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ENTITY_MY_CLIENT", referencedColumnName = "ENTITY_MY_CLIENT_ID", nullable = false)
//	private TEntityMyClient TEntityMyClient2;
//
//
//	//bi-directional many-to-one association to TForeignCurrency
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="FOREIGN_CURRENCY_ID", referencedColumnName = "FOREIGN_CURRENCY_ID", nullable = false)
//	private TForeignCurrency TForeignCurrency;






	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;

	//bi-directional many-to-one association to TTransaction
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TRANSACTION_ID", referencedColumnName = "TRANSACTION_ID", nullable = false)
	private TTransaction TTransaction;
}