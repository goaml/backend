package com.olak.goaml.models.xml.transaction;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.*;

import com.olak.goaml.models.reference.*;


/**
 * The persistent class for the t_from_my_client database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_FROM_MY_CLIENT", indexes = {
		@Index(name = "T_FROM_MY_CLIENT_T_TRANSACTION1_idx", columnList = "TRANSACTION_ID"),
		@Index(name = "T_FROM_MY_CLIENT_R_FUNDS_TYPE1_idx", columnList = "FROM_FUND_TYPE_ID"),
		@Index(name = "T_FROM_MY_CLIENT_R_COUNTRY_CODES1_idx", columnList = "FROM_COUNTRY_CODE_ID")
})
public class TFromMyClient extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_FROM_MY_CLIENT", allocationSize = 1, sequenceName="T_FROM_MY_CLIENT_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_FROM_MY_CLIENT")
	@Column(name="FROM_MY_CLIENT_ID")
	private Integer fromMyClientId;

	@Column(name="FROM_FUNDS_COMMENT")
	private String fromFundsComment;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;



	//Relationship Removed
	@Column(name="FROM_FOREIGN_CURRENCY_ID")
	private Integer fromForeignCurrencyId;

	@Column(name="CONDUCTOR_PERSON_MY_CLIENT_ID")
	private Integer conductorPersonMyClientId;

	@Column(name="FROM_ACCOUNT_MY_CLIENT_ID")
	private Integer fromAccountMyClientId;

	@Column(name="FROM_PERSON_MY_CLIENT_ID")
	private Integer fromPersonMyClientId;

	@Column(name="FROM_ENTITY_MY_CLIENT_ID")
	private Integer fromEntityMyClientId;



//	//bi-directional many-to-one association to TForeignCurrency
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="FROM_FOREIGN_CURRENCY_ID", referencedColumnName = "FOREIGN_CURRENCY_ID", nullable = false)
//	private TForeignCurrency TForeignCurrency;
//
//
//	//bi-directional many-to-one association to TPersonMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="T_CONDUCTOR_PERSON_MY_CLIENT_ID", referencedColumnName = "PERSON_MY_CLIENT_ID", nullable = false)
//	private TPersonMyClient TPersonMyClient1;
//
//
//	//bi-directional many-to-one association to TAccountMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="FROM_ACCOUNT_MY_CLIENT_ID", referencedColumnName = "ACCOUNT_MY_CLIENT_ID", nullable = false)
//	private TAccountMyClient TAccountMyClient;
//
//
//	//bi-directional many-to-one association to TPersonMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="FROM_PERSON_MY_CLIENT_ID", referencedColumnName = "PERSON_MY_CLIENT_ID", nullable = false)
//	private TPersonMyClient TPersonMyClient2;
//
//
//	//bi-directional many-to-one association to TEntityMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="FROM_ENTITY_MY_CLIENT_ID", referencedColumnName = "ENTITY_MY_CLIENT_ID", nullable = false)
//	private TEntityMyClient TEntityMyClient;



	//bi-directional many-to-one association to RFundsType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FROM_FUND_TYPE_ID", referencedColumnName = "FUND_TYPE_ID", nullable = false)
	private RFundsType RFundsType;

	//bi-directional many-to-one association to RCountryCode
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FROM_COUNTRY_CODE_ID", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
	private RCountryCodes RCountryCodes;


	//bi-directional many-to-one association to TTransaction
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TRANSACTION_ID", referencedColumnName = "TRANSACTION_ID", nullable = false)
	private TTransaction TTransaction;


	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;
}