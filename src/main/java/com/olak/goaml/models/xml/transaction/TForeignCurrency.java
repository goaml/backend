package com.olak.goaml.models.xml.transaction;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

import com.olak.goaml.models.reference.*;

/**
 * The persistent class for the t_foreign_currency database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_FOREIGN_CURRENCY", indexes = {
		@Index(name = "T_FOREIGN_CURRENCY_R_CURRENCY1_idx", columnList = "FOREIGN_CURRENCIES_ID")
})
public class TForeignCurrency extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_FOREIGN_CURRENCY", allocationSize = 1, sequenceName="T_FOREIGN_CURRENCY_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_FOREIGN_CURRENCY")
	@Column(name="FOREIGN_CURRENCY_ID")
	private Integer foreignCurrencyId;

	@Column(name="FOREIGN_AMOUNT")
	private BigDecimal foreignAmount;

	@Column(name="FOREIGN_EXCHANGE_RATE")
	private BigDecimal foreignExchangeRate;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;



	//bi-directional many-to-one association to RCurrency
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FOREIGN_CURRENCIES_ID", referencedColumnName = "CURRENCIES_ID", nullable = false)
	private RCurrency RCurrency;

	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;

}