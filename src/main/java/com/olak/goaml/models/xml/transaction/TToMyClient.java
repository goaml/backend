package com.olak.goaml.models.xml.transaction;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.*;

import com.olak.goaml.models.reference.*;


/**
 * The persistent class for the t_to_my_client database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_TO_MY_CLIENT", indexes = {
		@Index(name = "T_TO_MY_CLIENT_T_TRANSACTION1_idx", columnList = "TRANSACTION_ID"),
		@Index(name = "T_TO_MY_CLIENT_R_FUNDS_TYPE1_idx", columnList = "TO_FUND_TYPE_ID"),
		@Index(name = "T_TO_MY_CLIENT_R_COUNTRY_CODES1_idx", columnList = "TO_COUNTRY")
})
public class TToMyClient extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_TO_MY_CLIENT", allocationSize = 1, sequenceName="T_TO_MY_CLIENT_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_TO_MY_CLIENT")
	@Column(name="TO_MY_CLIENT_ID")
	private Integer toMyClientId;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;

	@Column(name="TO_FUNDS_COMMENT")
	private String toFundsComment;




	//Relationship Removed
	@Column(name="TO_FOREIGN_CURRENCY_ID")
	private Integer toForeignCurrencyId;

	@Column(name="TO_ACCOUNT_MY_CLIENT_ID")
	private Integer toAccountMyClientId;

	@Column(name="TO_PERSON_MY_CLIENT_ID")
	private Integer toPersonMyClientId;

	@Column(name="TO_ENTITY_MY_CLIENT_ID")
	private Integer toEntityMyClientId;


//	//bi-directional many-to-one association to TForeignCurrency
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="TO_FOREIGN_CURRENCY", referencedColumnName = "FOREIGN_CURRENCY_ID", nullable = false)
//	private TForeignCurrency TForeignCurrency;
//
//
//	//bi-directional many-to-one association to TAccountMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="TO_ACCOUNT", referencedColumnName = "ACCOUNT_MY_CLIENT_ID", nullable = false)
//	private TAccountMyClient TAccountMyClient;
//
//	//bi-directional many-to-one association to TPersonMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="TO_PERSON", referencedColumnName = "PERSON_MY_CLIENT_ID", nullable = false)
//	private TPersonMyClient TPersonMyClient;
//
//
//	//bi-directional many-to-one association to TEntityMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="TO_ENTITY", referencedColumnName = "ENTITY_MY_CLIENT_ID", nullable = false)
//	private TEntityMyClient TEntityMyClient;


	//bi-directional many-to-one association to RFundsType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TO_FUND_TYPE_ID", referencedColumnName = "FUND_TYPE_ID", nullable = false)
	private RFundsType RFundsType;

	//bi-directional many-to-one association to RCountryCode
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TO_COUNTRY", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
	private RCountryCodes RCountryCodes;

	//bi-directional many-to-one association to TTransaction
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="TRANSACTION_ID", referencedColumnName = "TRANSACTION_ID", nullable = false)
	private TTransaction TTransaction;

	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;

}