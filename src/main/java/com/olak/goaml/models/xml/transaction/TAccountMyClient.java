package com.olak.goaml.models.xml.transaction;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.olak.goaml.models.reference.*;


/**
 * The persistent class for the t_account_my_client database table.
 * 
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_ACCOUNT_MY_CLIENT")
public class TAccountMyClient extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_ACCOUNT_MY_CLIENT", allocationSize = 1, sequenceName="T_ACCOUNT_MY_CLIENT_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_ACCOUNT_MY_CLIENT")
	@Column(name="ACCOUNT_MY_CLIENT_ID")
	private Integer accountMyClientId;

	@Column(name = "ACCOUNT")
	private String account;

	@Column(name="ACCOUNT_NAME")
	private String accountName;

	@Column(name="BALANCE")
	private BigDecimal balance;

	@Column(name="BENEFICIARY")
	private String beneficiary;

	@Column(name="BENEFICIARY_COMMENT")
	private String beneficiaryComment;

	@Column(name="BRANCH")
	private String branch;

	@Column(name="CLIENT_NUMBER")
	private String clientNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CLOSED")
	private Date closed;

	@Column(name="COMMENTS")
	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATE_BALANCE")
	private Date dateBalance;

	@Column(name="IBAN")
	private String iban;

	@Column(name="INSTITUTION_CODE")
	private String institutionCode;

	@Column(name="INSTITUTION_NAME")
	private String institutionName;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;

	@Column(name="IS_PRIMARY")
	private Boolean isPrimary;

	@Column(name="NON_BANK_INSTITUTION")
	private Boolean nonBankInstitution;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="OPENED")
	private Date opened;

	@Column(name="SIGNATORY")
	private String signatory;

	@Column(name="SWIFT")
	private String swift;

	//bi-directional many-to-one association to TActivity
	@OneToMany(mappedBy="TAccountMyClient")
	private List<TActivity> TActivities;


	//Relationship Removed
	@Column(name="CURRENCIES_ID")
	private Integer currenciesId;
	@Column(name="ACCOUNT_TYPE_ID")
	private Integer accountTypeId;
	@Column(name="ENTITY_MY_CLIENT_ID")
	private Integer entityMyClientId;
	@Column(name="PERSON_MY_CLIENT_ID")
	private Integer personMyClientId;
	@Column(name="ACCOUNT_PERSON_ROLE_TYPE_ID")
	private Integer accountPersonRoleTypeId;
	@Column(name="ACCOUNT_STATUS_TYPE_ID")
	private Integer accountStatusTypeId;


//
//	//bi-directional many-to-one association to RCurrency
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="CURRENCIES_ID", referencedColumnName = "CURRENCIES_ID", nullable = false)
//	private RCurrency RCurrency;
//
//
//	//bi-directional many-to-one association to RAccountType
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ACCOUNT_TYPE_ID", referencedColumnName = "ACCOUNT_TYPE_ID", nullable = false)
//	private RAccountType RAccountType;
//
//
//	//bi-directional many-to-one association to TEntityMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ENTITY_MY_CLIENT_ID", referencedColumnName = "ENTITY_MY_CLIENT_ID", nullable = false)
//	private TEntityMyClient TEntityMyClient;
//
//
//	//bi-directional many-to-one association to TPersonMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="PERSON_MY_CLIENT_ID", referencedColumnName = "PERSON_MY_CLIENT_ID", nullable = false)
//	private TPersonMyClient TPersonMyClient;
//
//
//	//bi-directional many-to-one association to RAccountPersonRoleType
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ACCOUNT_PERSON_ROLE_TYPE_ID", referencedColumnName = "ACCOUNT_PERSON_ROLE_TYPE_ID", nullable = false)
//	private RAccountPersonRoleType RAccountPersonRoleType;
//
//
//	//bi-directional many-to-one association to RAccountStatusType
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ACCOUNT_STATUS_TYPE_ID", referencedColumnName = "ACCOUNT_STATUS_TYPE_ID", nullable = false)
//	private RAccountStatusType RAccountStatusType;





	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;

	//bi-directional many-to-one association to RAccountStatusType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CLIENT_TYPE_ID", referencedColumnName = "CLIENT_TYPE_ID", nullable = false)
	private RClientType RClientType;

}