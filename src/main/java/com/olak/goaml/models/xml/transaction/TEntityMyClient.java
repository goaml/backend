package com.olak.goaml.models.xml.transaction;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.olak.goaml.models.reference.*;


/**
 * The persistent class for the t_entity_my_client database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_ENTITY_MY_CLIENT")
public class TEntityMyClient extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_ENTITY_MY_CLIENT", allocationSize = 1, sequenceName="T_ENTITY_MY_CLIENT_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_ENTITY_MY_CLIENT")
	@Column(name="ENTITY_MY_CLIENT_ID")
	private Integer entityMyClientId;

	@Column(name="ADDRESSES")
	private String addresses;

	@Column(name="BUSINESS")
	private String business;

	@Column(name="BUSINESS_CLOSED")
	private Boolean businessClosed;

	@Column(name="COMMENTS")
	private String comments;

	@Column(name="COMMERCIAL_NAME")
	private String commercialName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATE_BUSINESS_CLOSED")
	private Date dateBusinessClosed;

	@Column(name="EMAIL")
	private String email;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="INCORPORATION_DATE")
	private Date incorporationDate;

	@Column(name="INCORPORATION_NUMBER")
	private String incorporationNumber;

	@Column(name="INCORPORATION_STATE")
	private String incorporationState;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;

	@Column(name="NAME")
	private String name;

	@Column(name="PHONES")
	private String phones;

	@Column(name="TAX_NUMBER")
	private String taxNumber;

	@Column(name="TAX_REG_NUMBER")
	private String taxRegNumber;

	@Column(name="URL")
	private String url;



	//bi-directional many-to-one association to TActivity
	@OneToMany(mappedBy="TEntityMyClient")
	private List<TActivity> TActivities;





	//Relationship Removed
	@Column(name="ENTITY_LEGAL_FORM_TYPE_ID")
	private Integer entityLegalFormTypeId;
	@Column(name="PHONE_ID")
	private Integer phoneId;
	@Column(name="ADDRESS_Id")
	private Integer addressId;
	@Column(name="COUNTRY_CODE_ID")
	private Integer countryCodeId;
	@Column(name="DIRECTOR_PERSON_ID")
	private Integer directorPersonId;
	@Column(name="ENTITY_PERSON_ROLE_TYPE_ID")
	private Integer entityPersonRoleTypeId;



	//bi-directional many-to-one association to REntityLegalFormType
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ENTITY_LEGAL_FORM_TYPE_ID", referencedColumnName = "ENTITY_LEGAL_FORM_TYPE_ID", nullable = false)
//	private REntityLegalFormType REntityLegalFormType;
//
//	//bi-directional many-to-one association to TPhone
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="PHONE_ID", referencedColumnName = "PHONE_ID", nullable = false)
//	private TPhone TPhone;
//
//	//bi-directional many-to-one association to TAddress
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ADDRESS_ID", referencedColumnName = "ADDRESS_ID", nullable = false)
//	private TAddress TAddress;
//
//	//bi-directional many-to-one association to RCountryCode
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="COUNTRY_CODE_ID", referencedColumnName = "COUNTRY_CODE_ID", nullable = false)
//	private RCountryCodes RCountryCodes;
//
//	//bi-directional many-to-one association to TPersonMyClient
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="DIRECTOR_ID", referencedColumnName = "PERSON_MY_CLIENT_ID", nullable = false)
//	private TPersonMyClient TPersonMyClient;
//
//
//	//bi-directional many-to-one association to REntityPersonRoleType
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ENTITY_PERSON_ROLE_TYPE_ID", referencedColumnName = "ENTITY_PERSON_ROLE_TYPE_ID", nullable = false)
//	private REntityPersonRoleType REntityPersonRoleType;





	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;

	//bi-directional many-to-one association to RAccountStatusType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CLIENT_TYPE_ID", referencedColumnName = "CLIENT_TYPE_ID", nullable = false)
	private RClientType RClientType;

}