package com.olak.goaml.models.xml.transaction;

import com.olak.goaml.audit.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import javax.persistence.*;
import com.olak.goaml.models.reference.*;


/**
 * The persistent class for the t_activity database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="T_ACTIVITY")
public class TActivity extends AuditModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="T_ACTIVITY", allocationSize = 1, sequenceName="T_ACTIVITY_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="T_ACTIVITY")
	@Column(name="ACTIVITY_ID")
	private Integer activityId;

	@Column(name="COMMENTS")
	private String comments;

	@Column(name="IS_ACTIVE")
	private Boolean isActive;

	@Column(name="REASON")
	private String reason;

	@Column(name="REPORT_PARTIES")
	private String reportParties;

	@Column(name="REPORT_PARTY")
	private String reportParty;

	@Column(name="SIGNIFICANCE")
	private Integer significance;

	//bi-directional many-to-one association to RStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATUS_ID", referencedColumnName = "STATUS_ID", nullable = false)
	private RStatus RStatus;

	//bi-directional many-to-one association to TAccountMyClient
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ACCOUNT_MY_CLIENT_ID", referencedColumnName = "ACCOUNT_MY_CLIENT_ID", nullable = false)
	private TAccountMyClient TAccountMyClient;

	//bi-directional many-to-one association to TEntityMyClient
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ENTITY_MY_CLIENT_ID", referencedColumnName = "ENTITY_MY_CLIENT_ID", nullable = false)
	private TEntityMyClient TEntityMyClient;

	//bi-directional many-to-one association to TPersonMyClient
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PERSON_MY_CLIENT_ID", referencedColumnName = "PERSON_MY_CLIENT_ID", nullable = false)
	private TPersonMyClient TPersonMyClient;

	//bi-directional many-to-one association to TReport
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPORT_ID", referencedColumnName = "REPORT_ID", nullable = false)
	private TReport TReport;

}