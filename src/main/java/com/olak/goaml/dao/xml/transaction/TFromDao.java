package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TForeignCurrency;
import com.olak.goaml.models.xml.transaction.TFrom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TFromDao extends JpaRepository<TFrom, Integer> , JpaSpecificationExecutor<TFrom> {
    List<TFrom> findByTTransaction_TransactionIdAndIsActive(Integer transactionId, boolean b);
}