package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TReport;
import com.olak.goaml.models.xml.transaction.TTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TTransactionDao extends JpaRepository<TTransaction, Integer>, JpaSpecificationExecutor<TTransaction> {
}