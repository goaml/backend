package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TParty;
import com.olak.goaml.models.xml.transaction.TPersonIdentification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TPersonIdentificationDao extends JpaRepository<TPersonIdentification, Integer> , JpaSpecificationExecutor<TPersonIdentification> {
    Optional<TPersonIdentification> findByPersonIdentificationIdAndIsActive(Integer id, boolean b);
}