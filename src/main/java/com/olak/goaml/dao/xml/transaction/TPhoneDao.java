package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TPersonMyClient;
import com.olak.goaml.models.xml.transaction.TPhone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TPhoneDao extends JpaRepository<TPhone, Integer> , JpaSpecificationExecutor<TPhone> {
    Optional<TPhone> findByPhoneIdAndIsActive(Integer id, boolean b);
}