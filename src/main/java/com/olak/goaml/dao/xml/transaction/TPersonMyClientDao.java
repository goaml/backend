package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TPersonIdentification;
import com.olak.goaml.models.xml.transaction.TPersonMyClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TPersonMyClientDao extends JpaRepository<TPersonMyClient, Integer> , JpaSpecificationExecutor<TPersonMyClient> {
    Optional<TPersonMyClient> findByPersonMyClientIdAndIsActive(Integer id, boolean b);

    Optional<TPersonMyClient> findByPersonMyClientIdAndRClientType_ClientTypeIdAndIsActive(Integer id, Integer clientType, boolean b);
}