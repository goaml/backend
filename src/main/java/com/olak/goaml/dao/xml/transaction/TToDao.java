package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TTo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TToDao extends JpaRepository<TTo, Integer> , JpaSpecificationExecutor<TTo> {
}