package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TPhone;
import com.olak.goaml.models.xml.transaction.TToMyClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TToMyClientDao extends JpaRepository<TToMyClient, Integer> , JpaSpecificationExecutor<TToMyClient> {
}