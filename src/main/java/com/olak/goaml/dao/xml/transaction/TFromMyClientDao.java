package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TFrom;
import com.olak.goaml.models.xml.transaction.TFromMyClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TFromMyClientDao extends JpaRepository<TFromMyClient, Integer> , JpaSpecificationExecutor<TFromMyClient> {
}