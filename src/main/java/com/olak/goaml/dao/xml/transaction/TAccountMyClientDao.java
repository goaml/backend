package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TAccountMyClient;
import com.olak.goaml.models.xml.transaction.TTo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TAccountMyClientDao extends JpaRepository<TAccountMyClient, Integer> , JpaSpecificationExecutor<TAccountMyClient> {
    Optional<TAccountMyClient> findByAccountMyClientIdAndIsActive(Integer id, boolean b);

    Optional<TAccountMyClient> findByAccountMyClientIdAndRClientType_ClientTypeIdAndIsActive(Integer id, Integer clientType, boolean b);
}