package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TAccountMyClient;
import com.olak.goaml.models.xml.transaction.TActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TActivityDao extends JpaRepository<TActivity, Integer> , JpaSpecificationExecutor<TActivity> {
}