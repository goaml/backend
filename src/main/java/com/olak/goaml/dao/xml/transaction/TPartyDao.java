package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TFromMyClient;
import com.olak.goaml.models.xml.transaction.TParty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TPartyDao extends JpaRepository<TParty, Integer> , JpaSpecificationExecutor<TParty> {
}