package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TActivity;
import com.olak.goaml.models.xml.transaction.TAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TAddressDao extends JpaRepository<TAddress, Integer> , JpaSpecificationExecutor<TAddress> {
    Optional<TAddress> findByAddressIdAndIsActive(Integer id, boolean b);
}