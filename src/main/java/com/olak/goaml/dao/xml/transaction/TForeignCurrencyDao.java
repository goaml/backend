package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TEntityMyClient;
import com.olak.goaml.models.xml.transaction.TForeignCurrency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TForeignCurrencyDao extends JpaRepository<TForeignCurrency, Integer> , JpaSpecificationExecutor<TForeignCurrency> {
    Optional<TForeignCurrency> findByForeignCurrencyIdAndIsActive(Integer id, boolean b);
}