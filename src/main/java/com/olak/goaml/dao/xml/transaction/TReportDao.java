package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TReportDao extends JpaRepository<TReport, Integer>, JpaSpecificationExecutor<TReport> {
    Optional<TReport> findByReportCodeAndIsActive(String reportCode, boolean b);

    Optional<TReport> findByReportCodeAndIsActiveAndTTransactions_IsActive(String reportCode, boolean b, boolean b1);

    @Query("SELECT tr FROM TReport tr JOIN tr.TTransactions tt WHERE tr.reportCode = :reportCode AND tr.isActive = true AND tt.isActive = true")
    Optional<TReport> findByReportCodeAndIsActiveAndTTransactions_IsActive1(@Param("reportCode") String reportCode);
}