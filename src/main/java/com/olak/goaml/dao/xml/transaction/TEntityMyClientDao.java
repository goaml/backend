package com.olak.goaml.dao.xml.transaction;

import com.olak.goaml.models.xml.transaction.TAddress;
import com.olak.goaml.models.xml.transaction.TEntityMyClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TEntityMyClientDao extends JpaRepository<TEntityMyClient, Integer> , JpaSpecificationExecutor<TEntityMyClient> {
    Optional<TEntityMyClient> findByEntityMyClientIdAndIsActive(Integer id, boolean b);

    Optional<TEntityMyClient> findByEntityMyClientIdAndRClientType_ClientTypeIdAndIsActive(Integer id, Integer clientId, boolean b);
}