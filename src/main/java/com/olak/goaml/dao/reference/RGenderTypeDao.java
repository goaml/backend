package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RGenderType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RGenderTypeDao extends JpaRepository<RGenderType, Integer> {
    List<RGenderType> findByIsActive(boolean b);

    Optional<RGenderType> findByGenderTypeCodeAndIsActive(String genderTypeCode, boolean b);

    Optional<RGenderType> findByGenderTypeIdAndIsActive(Integer genderTypeId, boolean b);

    boolean existsByGenderTypeCodeAndIsActiveAndGenderTypeIdNot(String genderTypeCode, boolean b, Integer genderTypeId);
//    Optional<RGenderType> findByGenderTypeIdAndIsActive(Integer genderTypeId, boolean b);
}
