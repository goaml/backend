package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RReportIndicatorType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RReportIndicatorTypeDao extends JpaRepository<RReportIndicatorType, Integer> {

    List<RReportIndicatorType> findByIsActive(boolean b);

    Optional<RReportIndicatorType> findByReportIndicatorTypeCodeAndIsActive(String reportIndicatorTypeCode, boolean b);

    Optional<RReportIndicatorType> findByReportIndicatorTypeIdAndIsActive(Integer id, boolean b);

    boolean existsByReportIndicatorTypeCodeAndIsActiveAndReportIndicatorTypeIdNot(String reportIndicatorTypeCode, boolean b,
                                                                                  Integer id);


}
