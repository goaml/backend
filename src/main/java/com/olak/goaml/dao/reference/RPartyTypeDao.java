package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RPartyType;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RPartyTypeDao extends JpaRepository<RPartyType, Integer> {

    List<RPartyType> findByIsActive(boolean b);


    Optional<RPartyType> findByPartyTypeIdAndIsActive(Integer id, boolean b);

    boolean existsByPartyTypeCodeAndIsActiveAndPartyTypeIdNot(String partyTypeCode, boolean b, Integer id);

    Optional<RPartyType> findByPartyTypeCodeAndIsActive(String partyTypeCode, boolean b);
}
