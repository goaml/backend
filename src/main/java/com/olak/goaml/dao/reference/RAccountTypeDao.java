package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RAccountType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RAccountTypeDao extends JpaRepository<RAccountType, Integer> {

    List<RAccountType> findByIsActive(boolean b);

    Optional<RAccountType> findByAccountTypeCodeAndIsActive(String accountTypeCode, boolean b);

    Optional<RAccountType> findByAccountTypeIdAndIsActive(Integer accountTypeId, boolean b);

    boolean existsByAccountTypeCodeAndIsActiveAndAccountTypeIdNot(String accountTypeCode, boolean b, Integer accountTypeId);
//    Optional<RAccountType> findByAccountTypeIdAndIsActive(Integer accountTypeId, boolean b);
}
