package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RCityList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RCityListDao extends JpaRepository<RCityList, Integer> {

    List<RCityList> findByIsActive(boolean b);

    Optional<RCityList> findByCityCodeAndIsActive(String cityCode, boolean b);

    Optional<RCityList> findByCityIdAndIsActive(Integer id, boolean b);

    boolean existsByCityCodeAndIsActiveAndCityIdNot(String cityCode, boolean b, Integer id);
}
