package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RCountryCodes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RCountryCodesDao extends JpaRepository<RCountryCodes, Integer> {
    Optional<RCountryCodes> findByCountryCodeIdAndIsActive(Integer passportCountryId, boolean b);
}