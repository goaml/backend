package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RAccountPersonRoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RAccountPersonRoleTypeDao extends JpaRepository<RAccountPersonRoleType, Integer> {
    List<RAccountPersonRoleType> findByIsActive(boolean isActive);
    Optional<RAccountPersonRoleType> findByAccountPersonRoleTypeCodeAndIsActive(String code, boolean isActive);
    Optional<RAccountPersonRoleType> findByAccountPersonRoleTypeIdAndIsActive(Integer id, boolean isActive);
    boolean existsByAccountPersonRoleTypeCodeAndIsActiveAndAccountPersonRoleTypeIdNot(String code, boolean isActive, Integer id);
}
