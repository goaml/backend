package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.REntityLegalFormType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface REntityLegalFormTypeDao extends JpaRepository<REntityLegalFormType, Integer> {
    List<REntityLegalFormType> findByIsActive(boolean isActive);

    Optional<REntityLegalFormType> findByEntityLegalFormTypeCodeAndIsActive(String entityLegalFormTypeCode, boolean isActive);

    Optional<REntityLegalFormType> findByEntityLegalFormTypeIdAndIsActive(Integer id, boolean isActive);

    boolean existsByEntityLegalFormTypeCodeAndIsActiveAndEntityLegalFormTypeIdNot(String entityLegalFormTypeCode, boolean isActive, Integer id);
//    Optional<REntityLegalFormType> findByEntityLegalFormTypeIdAndIsActive(Integer entityLegalFormTypeId, boolean b);
}