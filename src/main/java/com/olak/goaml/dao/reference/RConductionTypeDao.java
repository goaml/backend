package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RConductionType;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RConductionTypeDao extends JpaRepository<RConductionType, Integer> {

    List<RConductionType> findByIsActive(boolean b);

    Optional<RConductionType> findByConductionTypeCodeAndIsActive(String conductionTypeCode, boolean b);

    Optional<RConductionType> findByConductionTypeIdAndIsActive(Integer id, boolean b);

    boolean existsByConductionTypeCodeAndIsActiveAndConductionTypeIdNot(String conductionTypeCode, boolean b,
                                                                        Integer id);
}
