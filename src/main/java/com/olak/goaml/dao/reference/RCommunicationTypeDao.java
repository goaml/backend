package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RCommunicationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RCommunicationTypeDao extends JpaRepository<RCommunicationType, Integer> {

    List<RCommunicationType> findByIsActive(boolean b);

    Optional<RCommunicationType> findByCommunicationTypeCodeAndIsActive(String communicationTypeCode, boolean b);

    Optional<RCommunicationType> findByCommunicationTypeIdAndIsActive(Integer id, boolean b);

    boolean existsByCommunicationTypeCodeAndIsActiveAndCommunicationTypeIdNot(String communicationTypeCode, boolean b, Integer id);
}
