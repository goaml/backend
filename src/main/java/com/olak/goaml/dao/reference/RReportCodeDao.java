package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RReportCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RReportCodeDao extends JpaRepository<RReportCode, Integer> {
    List<RReportCode> findByIsActive(boolean b);

    Optional<RReportCode> findByReportCodeAndIsActive(String reportCode, boolean b);

    Optional<RReportCode> findByReportCodeIdAndIsActive(Integer id, boolean b);

    boolean existsByReportCodeAndIsActiveAndReportCodeIdNot(String reportCode, boolean b, Integer id);
}