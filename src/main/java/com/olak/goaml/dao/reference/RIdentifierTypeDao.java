package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RIdentifierType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RIdentifierTypeDao extends JpaRepository<RIdentifierType, Integer> {

    List<RIdentifierType> findByIsActive(boolean b);

    Optional<RIdentifierType> findByIdentifierTypeCodeAndIsActive(String identifierTypeCode, boolean b);

    Optional<RIdentifierType> findByIdentifierTypeIdAndIsActive(Integer id, boolean b);

    boolean existsByIdentifierTypeCodeAndIsActiveAndIdentifierTypeIdNot(String identifierTypeCode, boolean b, Integer id);
}
