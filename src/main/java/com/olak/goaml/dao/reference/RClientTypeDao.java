package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RClientType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RClientTypeDao extends JpaRepository<RClientType, Integer> {
}
