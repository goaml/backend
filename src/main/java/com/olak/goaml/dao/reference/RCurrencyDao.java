package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RCurrency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RCurrencyDao extends JpaRepository<RCurrency, Integer> {

    List<RCurrency> findByIsActive(boolean isActive);

    Optional<RCurrency> findByCurrenciesCodeAndIsActive(String currenciesCode, boolean isActive);

    Optional<RCurrency> findByCurrenciesIdAndIsActive(Integer id, boolean isActive);

    boolean existsByCurrenciesCodeAndIsActiveAndCurrenciesIdNot(String currenciesCode, boolean isActive, Integer id);
//    Optional<RCurrency> findByCurrenciesIdAndIsActive(Integer currenciesId, boolean b);
}
