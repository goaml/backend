package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RSubmissionType;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RSubmissionTypeDao extends JpaRepository<RSubmissionType, Integer> {

    List<RSubmissionType> findByIsActive(boolean b);

    Optional<RSubmissionType> findBySubmissionTypeCodeAndIsActive(String submissionTypeCode, boolean b);

    Optional<RSubmissionType> findBySubmissionTypeIdAndIsActive(Integer id, boolean b);

    boolean existsBySubmissionTypeCodeAndIsActiveAndSubmissionTypeIdNot(String submissionTypeCode, boolean b,
            Integer id);
}