package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.REntityPersonRoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface REntityPersonRoleTypeDao extends JpaRepository<REntityPersonRoleType, Integer> {

    List<REntityPersonRoleType> findByIsActive(boolean isActive);

    Optional<REntityPersonRoleType> findByEntityPersonRoleTypeIdAndIsActive(Integer id, boolean isActive);

    Optional<REntityPersonRoleType> findByEntityPersonRoleTypeCodeAndIsActive(String code, boolean isActive);

    boolean existsByEntityPersonRoleTypeCodeAndIsActiveAndEntityPersonRoleTypeIdNot(String code, boolean isActive, Integer id);
}
