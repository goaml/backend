package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RAccountStatusType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RAccountStatusTypeDao extends JpaRepository<RAccountStatusType, Integer> {

    List<RAccountStatusType> findByIsActive(boolean b);

    Optional<RAccountStatusType> findByAccountStatusTypeCodeAndIsActive(String accountStatusTypeCode,boolean b);

    Optional<RAccountStatusType> findByAccountStatusTypeIdAndIsActive(Integer id, boolean b);

    boolean existsByAccountStatusTypeCodeAndIsActiveAndAccountStatusTypeIdNot(String accountStatusTypeCode, boolean b,
                                                                              Integer id);
//    Optional<RAccountStatusType> findByAccountStatusTypeIdAndIsActive(Integer accountStatusTypeId, boolean b);
}
