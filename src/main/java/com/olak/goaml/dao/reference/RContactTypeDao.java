package com.olak.goaml.dao.reference;

import com.olak.goaml.models.reference.RContactType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RContactTypeDao extends JpaRepository<RContactType, Integer> {

    List<RContactType> findByIsActive(boolean isActive);

    Optional<RContactType> findByContactTypeCodeAndIsActive(String contactTypeCode, boolean isActive);

    Optional<RContactType> findByContactTypeIdAndIsActive(Integer id, boolean isActive);

    boolean existsByContactTypeCodeAndIsActiveAndContactTypeIdNot(String contactTypeCode, boolean isActive, Integer id);
}
