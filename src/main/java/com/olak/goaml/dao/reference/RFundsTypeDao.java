package com.olak.goaml.dao.reference;

import com.olak.goaml.models.master.FundType;
import com.olak.goaml.models.reference.RFundsType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RFundsTypeDao extends JpaRepository<RFundsType, Integer> {
    List<RFundsType> findByIsActive(boolean b);

    Optional<RFundsType> findByFundTypeCodeAndIsActive(String fundTypeCode, boolean b);

    Optional<RFundsType> findByFundTypeIdAndIsActive(Long id, boolean b);

    boolean existsByFundTypeCodeAndIsActiveAndFundTypeIdNot(String fundTypeCode, boolean b, Long id);
}