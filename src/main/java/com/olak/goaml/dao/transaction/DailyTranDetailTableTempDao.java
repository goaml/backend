package com.olak.goaml.dao.transaction;

import com.olak.goaml.models.transaction.DailyTranDetailTableTemp;
import com.olak.goaml.models.transaction.DailyTranDetailTableTempId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Stream;

@Repository
public interface DailyTranDetailTableTempDao extends JpaRepository<DailyTranDetailTableTemp, Long> {


//    @Query("SELECT d FROM DailyTranDetailTableTemp d WHERE (d.tranDate BETWEEN ?1 AND ?2) AND d.rptCode IN ?3")
//    List<DailyTranDetailTableTemp> findAllByTranDateBetweenAndRptCodeIn(
//            LocalDate startDate,
//            LocalDate endDate,
//            List<String> rptCodes);

//    @Query("SELECT DISTINCT d FROM DailyTranDetailTableTemp d WHERE (d.tranDate BETWEEN ?1 AND ?2) AND d.rptCode IN ?3")
//    List<DailyTranDetailTableTemp> findAllByTranDateBetweenAndRptCodeIn(
//            LocalDate startDate,
//            LocalDate endDate,
//            List<String> rptCodes);

//    @Query("SELECT DISTINCT d FROM DailyTranDetailTableTemp d WHERE (d.tranDate BETWEEN ?1 AND ?2) AND d.rptCode IN ?3")
//    Stream<DailyTranDetailTableTemp> findAllByTranDateBetweenAndRptCodeIn(
//            LocalDate startDate,
//            LocalDate endDate,
//            List<String> rptCodes);
//
//    @Query("SELECT DISTINCT d FROM DailyTranDetailTableTemp d WHERE d.tranDate BETWEEN ?1 AND ?2")
//    List<DailyTranDetailTableTemp> findAllByTranDateBetween(LocalDate startDate, LocalDate endDate);

//    @Query("SELECT d FROM DailyTranDetailTableTemp d WHERE d.tranDate BETWEEN ?1 AND ?2")
//    Stream<DailyTranDetailTableTemp> findAllByTranDateBetween(LocalDate startDate, LocalDate endDate);

    @Query("SELECT DISTINCT d FROM DailyTranDetailTableTemp d WHERE (d.tranDate BETWEEN ?1 AND ?2) AND d.rptCode IN ?3")
    Page<DailyTranDetailTableTemp> findAllByTranDateBetweenAndRptCodeIn(
            LocalDate startDate,
            LocalDate endDate,
            List<String> rptCodes,
            Pageable pageable);

    @Query("SELECT d FROM DailyTranDetailTableTemp d WHERE d.tranDate BETWEEN ?1 AND ?2")
    Page<DailyTranDetailTableTemp> findAllByTranDateBetween(LocalDate startDate, LocalDate endDate,
                                                            Pageable pageable);

}