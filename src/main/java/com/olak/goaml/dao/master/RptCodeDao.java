package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.ReportType;
import com.olak.goaml.models.master.RptCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RptCodeDao extends JpaRepository<RptCode, Long> {
    List<RptCode> findByIsActive(boolean b);

    Optional<RptCode> findByRptCodeNameAndIsActive(String rptCodeName, boolean b);

    Optional<RptCode> findByRptCodeDescriptionAndIsActive(String rptCodeDescription, boolean b);

    Optional<RptCode> findByRptCodeIdAndIsActive(Long id, boolean b);

    boolean existsByRptCodeNameAndIsActiveAndRptCodeIdNot(String rptCodeName, boolean b, Long id);
}
