package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryDao extends JpaRepository<Category, Long> {
    List<Category> findByIsActive(boolean b);

    Optional<Category> findByCategoryNameAndIsActive(String categoryName, boolean b);

    Optional<Category> findByCategoryIdAndIsActive(Long id, boolean b);

    boolean existsByCategoryNameAndIsActiveAndCategoryIdNot(String categoryName, boolean b, Long id);

    Optional<Category> findByCategoryDescriptionAndIsActive(String categoryDescription, boolean b);
}
