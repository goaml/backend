package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.RptCode;
import com.olak.goaml.models.master.TransactionCodes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TransactionCodesDao extends JpaRepository<TransactionCodes, Long> {
    List<TransactionCodes> findByIsActive(boolean b);

    Optional<TransactionCodes> findByTransactionCodeNameAndIsActive(String transactionCodeName, boolean b);

    Optional<TransactionCodes> findByTransactionCodeDescriptionAndIsActive(String transactionCodeDescription, boolean b);

    Optional<TransactionCodes> findByTransactionCodeIdAndIsActive(Long id, boolean b);

    boolean existsByTransactionCodeNameAndIsActiveAndTransactionCodeIdNot(String transactionCodeName, boolean b, Long id);
}
