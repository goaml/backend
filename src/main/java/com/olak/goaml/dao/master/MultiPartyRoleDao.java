package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.MultiPartyInvolveParty;
import com.olak.goaml.models.master.MultiPartyRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MultiPartyRoleDao extends JpaRepository<MultiPartyRole, Long> {
    List<MultiPartyRole> findByIsActive(boolean b);

    Optional<MultiPartyRole> findByMultiPartyRoleIdAndIsActive(Long id, boolean b);

    boolean existsByMultiPartyRoleNameAndIsActiveAndMultiPartyRoleIdNot(String multiPartyRoleName, boolean b, Long id);

    Optional<MultiPartyRole> findByMultiPartyRoleNameAndIsActive(String multiPartyRoleName, boolean b);

    Optional<MultiPartyRole> findByMultiPartyRoleDescriptionAndIsActive(String multiPartyRoleDescription, boolean b);
}
