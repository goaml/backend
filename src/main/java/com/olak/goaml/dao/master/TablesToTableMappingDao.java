package com.olak.goaml.dao.master;

import com.olak.goaml.models.master.TablesToTableMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TablesToTableMappingDao extends JpaRepository<TablesToTableMapping, Integer> {
}