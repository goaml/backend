package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.GoAmlTrxCodes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GoAmlTrxCodeDao extends JpaRepository<GoAmlTrxCodes, Long> {
    List<GoAmlTrxCodes> findByIsActive(boolean b);

    Optional<GoAmlTrxCodes> findByGoamlTrxCodeIdAndIsActive(Long id, boolean b);
}
