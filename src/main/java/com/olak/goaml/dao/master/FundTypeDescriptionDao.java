package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.FundTypeDescription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FundTypeDescriptionDao extends JpaRepository<FundTypeDescription, Long> {
    List<FundTypeDescription> findByIsActive(boolean b);

    Optional<FundTypeDescription> findByFundTypeDescriptionAndIsActive(String fundTypeDescription, boolean b);

    Optional<FundTypeDescription> findByFundTypeDescriptionIdAndIsActive(Long id, boolean b);

    boolean existsByFundTypeDescriptionAndIsActiveAndFundTypeDescriptionIdNot(String fundTypeDescription, boolean b, Long id);
}
