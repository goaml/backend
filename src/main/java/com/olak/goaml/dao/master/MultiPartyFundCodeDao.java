package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.FundTypeDescription;
import com.olak.goaml.models.master.MultiPartyFundsCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MultiPartyFundCodeDao extends JpaRepository<MultiPartyFundsCode, Long> {
    List<MultiPartyFundsCode> findByIsActive(boolean b);

    Optional<MultiPartyFundsCode> findByMultiPartyFundsCodeNameAndIsActive(String multiPartyFundsCodeName, boolean b);

    Optional<MultiPartyFundsCode> findByMultiPartyFundsCodeDescriptionAndIsActive(String multiPartyFundsCodeDescription, boolean b);

    Optional<MultiPartyFundsCode> findByMultiPartyFundsCodeIdAndIsActive(Long id, boolean b);
}
