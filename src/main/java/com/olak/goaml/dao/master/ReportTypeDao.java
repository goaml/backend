package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.MultiPartyRole;
import com.olak.goaml.models.master.ReportType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReportTypeDao extends JpaRepository<ReportType, Long> {
    List<ReportType> findByIsActive(boolean b);

    Optional<ReportType> findByReportTypeNameAndIsActive(String reportTypeName, boolean b);

    Optional<ReportType> findByReportTypeDescriptionAndIsActive(String reportTypeDescription, boolean b);

    Optional<ReportType> findByReportTypeIdAndIsActive(Long id, boolean b);

    boolean existsByReportTypeNameAndIsActiveAndReportTypeIdNot(String reportTypeName, boolean b, Long id);
}
