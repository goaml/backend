package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.MultiPartyFundsCode;
import com.olak.goaml.models.master.MultiPartyInvolveParty;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MultiPartyInvolvePartyDao extends JpaRepository<MultiPartyInvolveParty, Long> {
    List<MultiPartyInvolveParty> findByIsActive(boolean b);

    Optional<MultiPartyInvolveParty> findByMultiPartyInvolvePartyDescriptionAndIsActive(String multiPartyInvolvePartyDescription, boolean b);

    Optional<MultiPartyInvolveParty> findByMultiPartyInvolvePartyNameAndIsActive(String multiPartyInvolvePartyName, boolean b);

    Optional<MultiPartyInvolveParty> findByMultiPartyInvolvePartyIdAndIsActive(Long id, boolean b);

    boolean existsByMultiPartyInvolvePartyNameAndIsActiveAndMultiPartyInvolvePartyIdNot(String multiPartyInvolvePartyName, boolean b, Long id);

    boolean existsByMultiPartyInvolvePartyDescriptionAndIsActiveAndMultiPartyInvolvePartyIdNot(String multiPartyInvolvePartyDescription, boolean b, Long id);
}
