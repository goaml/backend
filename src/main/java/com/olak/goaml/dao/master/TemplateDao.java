package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.TemplateModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemplateDao extends JpaRepository<TemplateModel, Long> {
}
