package com.olak.goaml.dao.master;


import com.olak.goaml.models.master.FundType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FundTypeDao extends JpaRepository<FundType, Long> {
    List<FundType> findByIsActive(boolean b);

    Optional<FundType> findByFundTypeIdAndIsActive(Long id, boolean b);

    Optional<FundType> findByFundTypeNameAndIsActive(String fundTypeName, boolean b);

    boolean existsByFundTypeNameAndIsActiveAndFundTypeIdNot(String fundTypeName, boolean b, Long id);
}
