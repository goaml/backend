package com.olak.goaml.dto.master;

import com.olak.goaml.dto.reference.RStatusDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CategoryDto implements Serializable {

    private Long categoryId;
    private String categoryName;
    private String categoryDescription;
    private Boolean isActive;
    private RStatusDto rStatus;
    private Integer statusId;

}