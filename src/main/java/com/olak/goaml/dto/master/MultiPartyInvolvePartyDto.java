package com.olak.goaml.dto.master;

import com.olak.goaml.dto.reference.RStatusDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MultiPartyInvolvePartyDto implements Serializable {

    private Long multiPartyInvolvePartyId;
    private String multiPartyInvolvePartyName;
    private String multiPartyInvolvePartyDescription;
    private Boolean isActive;
    private RStatusDto rStatus;
    private Integer statusId;

}