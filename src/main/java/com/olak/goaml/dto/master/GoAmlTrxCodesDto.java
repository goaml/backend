package com.olak.goaml.dto.master;

import com.olak.goaml.dto.reference.RStatusDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GoAmlTrxCodesDto implements Serializable {

    private Long goamlTrxCodeId;
    private Long categoryId;
    private String categoryName;
    private Long fromFundTypeId;
    private String fromFundTypeName;
    private Long fromFundTypeDescriptionId;
    private String fromFundTypeDescription;
    private Long toFundTypeId;
    private String toFundTypeName;
    private Long toFundTypeDescriptionId;
    private String toFundTypeDescription;
    private Long multiPartyFundsCodeId;
    private String multiPartyFundsCodeName;
    private Long multiPartyInvolvePartyId;
    private String multiPartyInvolvePartyName;
    private Long multiPartyRoleId;
    private String multiPartyRoleName;
    private Long reportTypeId;
    private String reportTypeName;
    private Long rptCodeId;
    private String rptCodeName;
    private Long transactionCodeId;
    private String transactionCodeName;
    private String description;

    private String goamlTrxCodeScenario;
    private String goamlTrxCodeMultiParty;
    private String goamlTrxCodeBiParty;
    private String goamlTrxCodeFromFundType;
    private String goamlTrxCodeFrom;
    private String goamlTrxCodeToFundType;
    private String goamlTrxCodeTo;
    private String goamlTrxCodeComments;
    private String goamlTrxCodeExample;

    private Boolean isActive;
    private RStatusDto rStatus;
    private Integer statusId;
}