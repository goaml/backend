package com.olak.goaml.dto.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * DTO for {@link com.olak.goaml.models.master.TablesToTableMapping}
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TablesToTableMappingDto implements Serializable {
    private Integer tableToTableMappingId;
    private String tableName;
    private String columnName;
    private String mappingTableName;
    private String mappingColumnName;
}