package com.olak.goaml.dto.master;

import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.models.master.GoAmlTrxCodes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RptCodeDto implements Serializable {

    private Long rptCodeId;
    private String rptCodeName;
    private String rptCodeDescription;
    private Boolean isActive;
    private GoAmlTrxCodesDto goAmlTrxCodes;
    private RStatusDto rStatus;
    private Long goamlTrxCodeId;
    private Integer statusId;

}