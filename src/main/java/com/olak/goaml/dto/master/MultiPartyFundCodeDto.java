package com.olak.goaml.dto.master;

import com.olak.goaml.dto.reference.RStatusDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MultiPartyFundCodeDto implements Serializable {

    private Long multiPartyFundsCodeId;
    private String multiPartyFundsCodeName;
    private String multiPartyFundsCodeDescription;
    private Boolean isActive;
    private RStatusDto rStatus;
    private Integer statusId;

}