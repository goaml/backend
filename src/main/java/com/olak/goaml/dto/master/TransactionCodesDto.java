package com.olak.goaml.dto.master;

import com.olak.goaml.dto.reference.RStatusDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TransactionCodesDto implements Serializable {

    private Long transactionCodeId;
    private String transactionCodeName;
    private String transactionCodeDescription;
    private Boolean isActive;
    private RStatusDto rStatus;
    private Integer statusId;

}