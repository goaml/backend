package com.olak.goaml.dto.master;

import com.olak.goaml.dto.reference.RStatusDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReportTypeDto implements Serializable {

    private Long reportTypeId;
    private String reportTypeName;
    private String reportTypeDescription;
    private Boolean isActive;
    private RStatusDto rStatus;
    private Integer statusId;

}