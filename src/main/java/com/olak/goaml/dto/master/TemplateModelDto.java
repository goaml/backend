package com.olak.goaml.dto.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TemplateModelDto {

    private Long templateId;
    private String name;
    private String description;

}