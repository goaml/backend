package com.olak.goaml.dto.dtolist;

import com.olak.goaml.dto.master.CategoryDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CategoryListDto {

    List<CategoryDto> data;

}