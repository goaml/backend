package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.dto.reference.RCommunicationTypeDto;
import com.olak.goaml.dto.reference.RContactTypeDto;
import com.olak.goaml.models.reference.RCommunicationType;
import com.olak.goaml.models.reference.RContactType;
import com.olak.goaml.models.reference.RStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TPhone}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TPhoneDto implements Serializable {
    private Integer phoneId;
    private String comments;
    private Boolean isActive;
    private String tphCountryPrefix;
    private String tphExtension;
    private String tphNumber;
    private RCommunicationTypeDto RCommunicationType;
    private RContactTypeDto RContactType;
    private RStatusDto RStatus;

    private Integer communicationTypeId;
    private Integer contactTypeId;
    private Integer statusId;

}