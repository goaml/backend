package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.dto.reference.RCityListDto;
import com.olak.goaml.dto.reference.RContactTypeDto;
import com.olak.goaml.dto.reference.RCountryCodesDto;
import com.olak.goaml.models.reference.RCityList;
import com.olak.goaml.models.reference.RContactType;
import com.olak.goaml.models.reference.RCountryCodes;
import com.olak.goaml.models.reference.RStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TAddress}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TAddressDto implements Serializable {
    private Integer addressId;
    private String address;
    private String comments;
    private Boolean isActive;
    private String state;
    private String town;
    private String zip;

    private RCityListDto RCityList;
    private RContactTypeDto RContactType;
    private RCountryCodesDto RCountryCodes;
    private RStatusDto RStatus;

    private Integer cityId;
    private Integer contactTypeId;
    private Integer countryCodeId;
    private Integer statusId;

}