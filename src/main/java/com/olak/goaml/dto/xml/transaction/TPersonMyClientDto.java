package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.RClientTypeDto;
import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.dto.reference.RCountryCodesDto;
import com.olak.goaml.dto.reference.RGenderTypeDto;
import com.olak.goaml.models.reference.RStatus;
import com.olak.goaml.models.xml.transaction.TActivity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TPersonMyClient}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TPersonMyClientDto implements Serializable {
    private Integer personMyClientId;
    private String addresses;
    private String alias;
    private String birthPlace;
    private Date birthdate;
    private String comments;
    private Boolean deceased;
    private Date deceasedDate;
    private String email;
    private String employerName;
    private String firstName;
    private String idNumber;
    private Boolean isActive;
    private String lastName;
    private String middleName;
    private String mothersName;
    private String occupation;
    private String passportNumber;
    private String phones;
    private String prefix;
    private String sourceOfWealth;
    private String ssn;
    private String taxNumber;
    private String taxRegNumber;
    private String title;
    private List<TActivityDto> TActivities;

    private Integer genderTypeId;
    private Integer passportCountryId;
    private Integer nationalityCountryId;
    private Integer nationalityCountry2Id;
    private Integer nationalityCountry3Id;
    private Integer residenceCountryId;
    private Integer phoneId;
    private Integer addressId;
    private Integer employerPhoneId;
    private Integer employerAddressId;
    private Integer personIdentificationId;

    private RStatusDto RStatus;
    private Integer statusId;
    private Integer clientTypeId;


    // Additional fields
    private RCountryCodesDto passportCountry;
    private RCountryCodesDto nationalityCountry;
    private RCountryCodesDto nationalityCountry2;
    private RCountryCodesDto nationalityCountry3;
    private RCountryCodesDto residenceCountry;
    private RGenderTypeDto RGenderType;
    private TAddressDto TAddress1;
    private TAddressDto TAddress2;
    private TPersonIdentificationDto TPersonIdentification;
    private TPhoneDto TPhone1;
    private TPhoneDto TPhone2;
    private RClientTypeDto RClientType;
}