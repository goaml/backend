package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.RStatusDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TActivity}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TActivityDto implements Serializable {
    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;
    private Integer activityId;
    private String comments;
    private Boolean isActive;
    private String reason;
    private String reportParties;
    private String reportParty;
    private Integer significance;
    private RStatusDto RStatus;
    private TAccountMyClientDto TAccountMyClient;
    private TEntityMyClientDto TEntityMyClient;
    private TPersonMyClientDto TPersonMyClient;
    private TReportDto TReport;

    private Integer statusId;
    private Integer accountMyClientId;
    private Integer entityMyClientId;
    private Integer personMyClientId;
    private Integer reportId;
}