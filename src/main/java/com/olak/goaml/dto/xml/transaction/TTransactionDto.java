package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.dto.reference.RConductionTypeDto;
import com.olak.goaml.models.xml.transaction.TFrom;
import com.olak.goaml.models.xml.transaction.TFromMyClient;
import com.olak.goaml.models.xml.transaction.TTo;
import com.olak.goaml.models.xml.transaction.TToMyClient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TTransaction}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TTransactionDto implements Serializable {
//    private String createdBy;
//    private Date createdOn;
//    private String updatedBy;
//    private Date updatedOn;
    private Integer transactionId;
    private BigDecimal amountLocal;
    private String authorized;
    private String comments;
    private LocalDate datePosting;
    private LocalDate dateTransaction;
    private String goodsServices;
    private String internalRefNumber;
    private Boolean isActive;
    private Boolean lateDeposit;
    private String teller;
    private String transactionDescription;
    private String transactionLocation;
    private String transactionNumber;
    private String transmodeComment;
    private LocalDate valueDate;
    private String trxNo;
    private String rptCode;
    private RConductionTypeDto RConductionType;
    private RStatusDto RStatus;
    private TReportDto TReport;

//    private List<TFromDto> TFroms;
//    private List<TFromMyClientDto> TFromMyClients;
//    private List<TToDto> TTos;
//    private List<TToMyClientDto> TToMyClients;

    private Integer conductionTypeId;
    private Integer statusId;
    private Integer reportId;
    private String custId;
    private String acid ;
}