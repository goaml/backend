package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.models.reference.RStatus;
import com.olak.goaml.models.xml.transaction.TActivity;
import com.olak.goaml.models.xml.transaction.TTransaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TReport}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TReportDto implements Serializable {
    private Integer reportId;
    private String action;
    private String currencyCodeLocal;
    private String entityReference;
    private String fiuRefNumber;
    private Boolean isActive;
    private String location;
    private String reason;
    private String rentityBranch;
    private Integer rentityId;
    private String reportCode;
    private String reportingPerson;
    private String submissionCode;
    private Date submissionDate;
    private Integer noOfTransactions;
    private Date lastTrxUpdatedDate;

    private List<TActivity> TActivities;
    private RStatusDto RStatus;
    private List<TTransactionDto> transactions; // <transaction>

    private Integer statusId;
}