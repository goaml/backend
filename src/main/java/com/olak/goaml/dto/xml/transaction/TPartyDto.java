package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.dto.reference.RCountryCodesDto;
import com.olak.goaml.dto.reference.RFundsTypeDto;
import com.olak.goaml.dto.reference.RPartyTypeDto;
import com.olak.goaml.models.reference.RCountryCodes;
import com.olak.goaml.models.reference.RFundsType;
import com.olak.goaml.models.reference.RPartyType;
import com.olak.goaml.models.reference.RStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TParty}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TPartyDto implements Serializable {
    private Integer partyId;
    private String comments;
    private String country;
    private String fundsComment;
    private Boolean isActive;
    private Integer personId;
    private Integer personMyClientId;
    private Integer accountId;
    private Integer accountMyClientId;
    private Integer entityId;
    private Integer entityMyClientId;
    private Integer foreignCurrencyId;

    private RCountryCodesDto RCountryCodes;
    private RFundsTypeDto RFundsType;
    private RPartyTypeDto RPartyType;
    private RStatusDto RStatus;

    private Integer countryCodeId;
    private Integer fundTypeId;
    private Integer partyTypeId;
    private Integer statusId;
    private Integer transactionId;
    private TTransactionDto TTransaction;



    // Additional fields
    private TAccountMyClientDto TAccountMyClient1;
    private TAccountMyClientDto TAccountMyClient2;
    private TEntityMyClientDto TEntityMyClient1;
    private TEntityMyClientDto TEntityMyClient2;
    private TForeignCurrencyDto TForeignCurrency;
    private TPersonMyClientDto TPersonMyClient1;
    private TPersonMyClientDto TPersonMyClient2;

}