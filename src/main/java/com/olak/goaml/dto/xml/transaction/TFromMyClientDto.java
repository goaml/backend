package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.dto.reference.RCountryCodesDto;
import com.olak.goaml.dto.reference.RFundsTypeDto;
import com.olak.goaml.models.reference.RCountryCodes;
import com.olak.goaml.models.reference.RFundsType;
import com.olak.goaml.models.reference.RStatus;
import com.olak.goaml.models.xml.transaction.TTransaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TFromMyClient}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TFromMyClientDto implements Serializable {
    private Integer fromMyClientId;
    private String fromFundsComment;
    private Boolean isActive;
    private Integer fromForeignCurrencyId;
    private Integer conductorPersonMyClientId;
    private Integer fromAccountMyClientId;
    private Integer fromPersonMyClientId;
    private Integer fromEntityMyClientId;
    private RCountryCodesDto RCountryCodes;
    private RFundsTypeDto RFundsType;
    private RStatusDto RStatus;
    private TTransactionDto TTransaction;

    private Integer countryCodeId;
    private Integer fundTypeId;
    private Integer statusId;
    private Integer transactionId;


    // Additional fields
    private TAccountMyClientDto TAccountMyClient;
    private TEntityMyClientDto TEntityMyClient;
    private TForeignCurrencyDto TForeignCurrency;
    private TPersonMyClientDto TPersonMyClient1;
    private TPersonMyClientDto TPersonMyClient2;
}