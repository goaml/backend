package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.*;
import com.olak.goaml.models.reference.RStatus;
import com.olak.goaml.models.xml.transaction.TActivity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TEntityMyClient}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TEntityMyClientDto implements Serializable {
    private Integer entityMyClientId;
    private String addresses;
    private String business;
    private Boolean businessClosed;
    private String comments;
    private String commercialName;
    private Date dateBusinessClosed;
    private String email;
    private Date incorporationDate;
    private String incorporationNumber;
    private String incorporationState;
    private Boolean isActive;
    private String name;
    private String phones;
    private String taxNumber;
    private String taxRegNumber;
    private String url;

    private List<TActivity> TActivities;

    private Integer entityLegalFormTypeId;
    private Integer phoneId;
    private Integer addressId;
    private Integer countryCodeId;
    private Integer directorPersonId;
    private Integer entityPersonRoleTypeId;

    private RStatusDto RStatus;
    private Integer statusId;
    private Integer clientTypeId;


    // Additional fields
    private RCountryCodesDto RCountryCodes;
    private REntityLegalFormTypeDto REntityLegalFormType;
    private REntityPersonRoleTypeDto REntityPersonRoleType;
    private TAddressDto TAddress;
    private TPersonMyClientDto TPersonMyClient;
    private TPhoneDto TPhone;

    private RClientTypeDto RClientType;
}