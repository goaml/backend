package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.dto.reference.RCurrencyDto;
import com.olak.goaml.models.reference.RCurrency;
import com.olak.goaml.models.reference.RStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TForeignCurrency}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TForeignCurrencyDto implements Serializable {
    private Integer foreignCurrencyId;
    private BigDecimal foreignAmount;
    private BigDecimal foreignExchangeRate;
    private Boolean isActive;
    private RCurrencyDto RCurrency;
    private RStatusDto RStatus;

    private Integer currenciesId;
    private Integer statusId;
}