package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.RStatusDto;
import com.olak.goaml.dto.reference.RCountryCodesDto;
import com.olak.goaml.dto.reference.RIdentifierTypeDto;
import com.olak.goaml.models.reference.RIdentifierType;
import com.olak.goaml.models.reference.RStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TPersonIdentification}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TPersonIdentificationDto implements Serializable {
    private Integer personIdentificationId;
    private String comments;
    private Date expiryDate;
    private Boolean isActive;
    private Date issueDate;
    private String issuedBy;
    private String number;
    private Integer issueCountryCodeId;

    private RIdentifierTypeDto RIdentifierType;
    private RStatusDto RStatus;
    private Integer identifierTypeId;
    private Integer statusId;



    // Additional fields
    private RCountryCodesDto RCountryCodes;
}