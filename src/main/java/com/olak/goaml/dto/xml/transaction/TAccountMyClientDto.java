package com.olak.goaml.dto.xml.transaction;

import com.olak.goaml.dto.reference.*;
import com.olak.goaml.models.reference.RStatus;
import com.olak.goaml.models.xml.transaction.TActivity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * DTO for {@link com.olak.goaml.models.xml.transaction.TAccountMyClient}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TAccountMyClientDto implements Serializable {
    private Integer accountMyClientId;
    private String account;
    private String accountName;
    private BigDecimal balance;
    private String beneficiary;
    private String beneficiaryComment;
    private String branch;
    private String clientNumber;
    private Date closed;
    private String comments;
    private Date dateBalance;
    private String iban;
    private String institutionCode;
    private String institutionName;
    private Boolean isActive;
    private Boolean isPrimary;
    private Boolean nonBankInstitution;
    private Date opened;
    private String signatory;
    private String swift;
    private List<TActivity> TActivities;
    private Integer currenciesId;
    private Integer accountTypeId;
    private Integer entityMyClientId;
    private Integer personMyClientId;
    private Integer accountPersonRoleTypeId;
    private Integer accountStatusTypeId;
    private RStatusDto RStatus;
    private Integer statusId;
    private Integer clientTypeId;



    // Additional fields
    private RAccountPersonRoleTypeDto RAccountPersonRoleType;
    private RAccountStatusTypeDto RAccountStatusType;
    private RAccountTypeDto RAccountType;
    private RCurrencyDto RCurrency;
    private TEntityMyClientDto TEntityMyClient;
    private TPersonMyClientDto TPersonMyClient;
    private RClientTypeDto RClientType;
}