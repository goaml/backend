package com.olak.goaml.dto.reference;

import com.olak.goaml.dto.reference.RStatusDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO for {@link com.olak.goaml.models.reference.RAccountPersonRoleType}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RAccountPersonRoleTypeDto implements Serializable {
    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;
    private Integer accountPersonRoleTypeId;
    private String accountPersonRoleTypeCode;
    private String description;
    private Boolean isActive;
    private RStatusDto rStatus;

    private Integer statusId;
}