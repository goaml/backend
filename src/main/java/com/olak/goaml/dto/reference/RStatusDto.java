package com.olak.goaml.dto.reference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RStatusDto implements Serializable {
    private Integer statusId;
    @Size(max = 150, message = "Description length must less than 150 characters")
    private String description;
    private Boolean isActive;
    private String statusCode;
}
