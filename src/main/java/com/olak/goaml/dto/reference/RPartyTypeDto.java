package com.olak.goaml.dto.reference;

import com.olak.goaml.dto.reference.RStatusDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO for {@link com.olak.goaml.models.reference.RPartyType}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RPartyTypeDto implements Serializable {
    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;
    private Integer partyTypeId;
    private String partyTypeCode;
    private String description;
    private Boolean isActive;
    private RStatusDto rStatus;

    private Integer statusId;
}