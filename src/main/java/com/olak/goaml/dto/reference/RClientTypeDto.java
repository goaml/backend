package com.olak.goaml.dto.reference;

import com.olak.goaml.models.reference.RStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RClientTypeDto implements Serializable {

    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;
    private Integer clientTypeId;
    private String clientTypeCode;
    private String description;
    private Boolean isActive;
    private RStatusDto rStatus;
    private Integer statusId;
}
