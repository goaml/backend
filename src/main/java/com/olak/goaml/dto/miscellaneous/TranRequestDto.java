package com.olak.goaml.dto.miscellaneous;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TranRequestDto {

    private LocalDate startDate;
    private LocalDate endDate;
    private List<String> rtpCode;

}
