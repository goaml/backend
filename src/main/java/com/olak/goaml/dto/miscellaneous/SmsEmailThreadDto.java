package com.olak.goaml.dto.miscellaneous;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmsEmailThreadDto implements Serializable {
    private static final long serialVersionUID = 2492942573192525990L;

    private String name;
    private String mobileNumber;

}
