package com.olak.goaml.dto.transaction;

import com.olak.goaml.models.transaction.DailyTranDetailTableTempId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

/**
 * DTO for {@link com.olak.goaml.models.transaction.DailyTranDetailTableTemp}
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DailyTranDetailTableTempDto implements Serializable {
    private Long id;
    private OffsetDateTime tranDate;
    private String tranId;
    private String partTranSrlNum;
    private String delFlg;
    private String tranType;
    private String tranSubType;
    private String partTranType;
    private String glSubHeadCode;
    private String acid;
    private OffsetDateTime valueDate;
    private BigDecimal tranAmt;
    private String tranParticular;
    private String entryUserId;
    private String pstdUserId;
    private String vfdUserId;
    private OffsetDateTime entryDate;
    private OffsetDateTime pstdDate;
    private OffsetDateTime vfdDate;
    private String rptCode;
    private String refNum;
    private String instrmntType;
    private OffsetDateTime instrmntDate;
    private String instrmntNum;
    private String instrmntAlpha;
    private String tranRmks;
    private String pstdFlg;
    private String prntAdvcInd;
    private String amtReservationInd;
    private BigDecimal reservationAmt;
    private String restrictModifyInd;
    private String lchgUserId;
    private OffsetDateTime lchgTime;
    private String rcreUserId;
    private OffsetDateTime rcreTime;
    private String custId;
    private String voucherPrintFlg;
    private String moduleId;
    private String brCode;
    private BigDecimal fxTranAmt;
    private String rateCode;
    private BigDecimal rate;
    private String crncyCode;
    private String navigationFlg;
    private String tranCrncyCode;
    private String refCrncyCode;
    private BigDecimal refAmt;
    private String solId;
    private String bankCode;
    private String treaRefNum;
    private BigDecimal treaRate;
    private BigDecimal tsCnt;
    private String gstUpdFlg;
    private String isoFlg;
    private String eabfabUpdFlg;
    private String liftLienFlg;
    private String proxyPostInd;
    private String siSrlNum;
    private OffsetDateTime siOrgExecDate;
    private String prSrlNum;
    private String serialNum;
    private String delMemoPad;
    private String uadModuleId;
    private String uadModuleKey;
    private OffsetDateTime reversalDate;
    private OffsetDateTime reversalValueDate;
    private String pttmEventType;
    private String proxyAcid;
    private String todEntityType;
    private String todEntityId;
    private String dthInitSolId;
    private BigDecimal regularizationAmt;
    private BigDecimal principalPortionAmt;
    private String tfEntitySolId;
    private String tranParticular2;
    private String tranParticularCode;
    private String trStatus;
    private String svsTranId;
    private String crncyHolChkDoneFlg;
    private String referralId;
    private String partyCode;
    private OffsetDateTime glDate;
    private String bkdtTranFlg;
    private String bankId;
    private String implCashPartTranFlg;
    private String ptranChrgExistsFlg;
    private String mudPoolBalBuildFlg;
    private String glSegmentString;
    private String sysPartTranCode;
    private String userPartTranCode;
    private String tranFreeCode1;
    private String tranFreeCode2;
}