package com.olak.goaml.dto.transaction;

import java.io.Serializable;

public class DailyTranDetailTableTempIdDto implements Serializable {

    private String tranId;
    private String partTranType;

    // Default constructor
    public DailyTranDetailTableTempIdDto() {}

    // Parameterized constructor
    public DailyTranDetailTableTempIdDto(String tranId, String partTranType) {
        this.tranId = tranId;
        this.partTranType = partTranType;
    }

    // Getters and setters
    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getPartTranType() {
        return partTranType;
    }

    public void setPartTranType(String partTranType) {
        this.partTranType = partTranType;
    }

    // Override equals and hashCode if necessary
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DailyTranDetailTableTempIdDto that = (DailyTranDetailTableTempIdDto) o;

        if (!tranId.equals(that.tranId)) return false;
        return partTranType.equals(that.partTranType);
    }

    @Override
    public int hashCode() {
        int result = tranId.hashCode();
        result = 31 * result + partTranType.hashCode();
        return result;
    }
}
